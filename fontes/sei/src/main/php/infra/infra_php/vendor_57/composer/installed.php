<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '5bd0e8b9053fa6bb061442dab0cfb970ad31dac4',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '5bd0e8b9053fa6bb061442dab0cfb970ad31dac4',
    ),
    'beberlei/assert' => 
    array (
      'pretty_version' => 'v3.2.2',
      'version' => '3.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5547e7d03f8c6be121b8b9db6d6ed5a22ffdcb01',
    ),
    'ircmaxell/password-compat' => 
    array (
      'pretty_version' => 'v1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c5cde8822a69545767f7c7f3058cb15ff84614c',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '55af0dc01992b4d0da7f6372e2eac097bbbaffdb',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v2.0.12',
      'version' => '2.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '258c89a6b97de7dfaf5b8c7607d0478e236b04fb',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f066a26a962dbe58ddea9f72a4e82874a3975a4',
    ),
    'spomky-labs/otphp' => 
    array (
      'pretty_version' => 'v10.0.3',
      'version' => '10.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9784d9f7c790eed26e102d6c78f12c754036c366',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => 'v4.0.8',
      'version' => '4.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd58df88e3cfdb2702f5fd8cd67c33961c2539e0c',
    ),
    'symfony/polyfill' => 
    array (
      'pretty_version' => 'v1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '86e27771f290e6af6d95c1c256f6d6d15ac9e22e',
    ),
    'symfony/polyfill-apcu' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-iconv' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-php54' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-php55' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-php56' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-php70' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-php71' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-php72' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-util' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'symfony/polyfill-xml' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.7.0',
      ),
    ),
    'thecodingmachine/safe' => 
    array (
      'pretty_version' => 'v1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8ab0876305a4cdaef31b2350fcb9811b5608dbc',
    ),
    'tuupola/base32' => 
    array (
      'pretty_version' => '0.2.0',
      'version' => '0.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '43a783c6eb3f125a5efdc9e22a97f6cc604f345e',
    ),
    'voku/anti-xss' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cde2346113c3d84495ddb2d1fe511ca9c7c0db3c',
    ),
    'voku/portable-utf8' => 
    array (
      'pretty_version' => '3.1.30',
      'version' => '3.1.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '162de9c40d8a7dff84697296f375b424aef53df8',
    ),
  ),
);
