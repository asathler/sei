<?

try {
  require_once __DIR__.'/../web/SEI.php';

  if (!method_exists('InfraUtil', 'compararVersoes')) {
    throw new InfraException('VERSAO DO FRAMEWORK PHP DEVE SER IGUAL OU SUPERIOR A 1.598.0 (REFERENTE A VERSAO SEI 4.0.0)');
  }

  $arrModulos = ConfiguracaoSEI::getInstance()->getValor('SEI', 'Modulos', false);
  if (!isset($arrModulos['MdJulgarIntegracao'])) {
    throw new InfraException('MODULO NAO FOI CONFIGURADO NO ARQUIVO ConfiguracaoSEI.php');
  }

  class MdJulgarVersaoSeiRN extends InfraScriptVersao
  {

    public function __construct()
    {
      parent::__construct();
    }

    protected function inicializarObjInfraIBanco()
    {
      return BancoSEI::getInstance();
    }

    public function versao_1_0_0($strVersaoAtual)
    {
      try {

        $objInfraMetaBD = new InfraMetaBD(BancoSEI::getInstance());

        BancoSEI::getInstance()->executarSql('CREATE TABLE algoritmo (
 id_algoritmo         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
 id_colegiado         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
 id_usuario           '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
 id_origem            '.$objInfraMetaBD->tipoNumero().'  NULL ,
 sta_algoritmo        '.$objInfraMetaBD->tipoTextoFixo(1).'  NOT NULL,
 contador             '.$objInfraMetaBD->tipoNumero().'  NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('algoritmo', 'pk_algoritmo', array('id_algoritmo'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE andamento_sessao (
 id_andamento_sessao  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
 dth_execucao         '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
 id_tarefa_sessao     '.$objInfraMetaBD->tipoNumero().' NOT NULL,
 id_sessao_julgamento '.$objInfraMetaBD->tipoNumero().' NOT NULL,
 id_unidade           '.$objInfraMetaBD->tipoNumero().' NULL,
 id_usuario           '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('andamento_sessao', 'pk_andamento_sessao', array('id_andamento_sessao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE ausencia_sessao (
 id_ausencia_sessao   '.$objInfraMetaBD->tipoNumero().' NOT NULL,
 id_usuario           '.$objInfraMetaBD->tipoNumero().' NOT NULL,
 id_sessao_julgamento '.$objInfraMetaBD->tipoNumero().' NOT NULL,
 id_motivo_ausencia   '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('ausencia_sessao', 'pk_ausencia_sessao', array('id_ausencia_sessao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE atributo_andamento_sessao
      (
        id_atributo_andamento_sessao  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        chave                 '.$objInfraMetaBD->tipoTextoVariavel(50).' NOT NULL,
        valor                 '.$objInfraMetaBD->tipoTextoVariavel(250).' NULL,
        id_origem             '.$objInfraMetaBD->tipoTextoVariavel(50).' NULL,
        id_andamento_sessao   '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('atributo_andamento_sessao', 'pk_atributo_andamento_sessao', array('id_atributo_andamento_sessao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE colegiado
      (
        id_colegiado          '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_tipo_procedimento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario_secretario  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario_presidente  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade_responsavel '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sigla                 '.$objInfraMetaBD->tipoTextoVariavel(30).' NOT NULL,
        nome                  '.$objInfraMetaBD->tipoTextoVariavel(100).' NOT NULL,
        quorum_minimo         '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sta_algoritmo_distribuicao  '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('colegiado', 'pk_colegiado', array('id_colegiado'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE colegiado_composicao
      (
        id_colegiado_composicao  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_tipo_membro_colegiado  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_colegiado_versao   '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        ordem                 '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sin_rodada            '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        peso                  '.$objInfraMetaBD->tipoNumeroDecimal(2, 1).' NOT NULL,
        id_cargo              '.$objInfraMetaBD->tipoNumero().' NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('colegiado_composicao', 'pk_colegiado_composicao', array('id_colegiado_composicao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE colegiado_versao
      (
        id_colegiado_versao   '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_colegiado          '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        dth_versao            '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        sin_ultima            '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        sin_editavel          '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        id_unidade            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_tipo_procedimento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario_secretario  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario_presidente  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade_responsavel '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sigla                 '.$objInfraMetaBD->tipoTextoVariavel(30).' NOT NULL,
        nome                  '.$objInfraMetaBD->tipoTextoVariavel(100).' NOT NULL,
        quorum_minimo         '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sta_algoritmo_distribuicao  '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('colegiado_versao', 'pk_colegiado_versao', array('id_colegiado_versao'));
        $objInfraMetaBD->criarIndice('colegiado_versao', 'i04_colegiado_versao', array('sin_ultima'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE destaque
      (
        id_destaque           '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_item_sessao_julgamento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sta_tipo              '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(4000).' NOT NULL,
        dth_destaque          '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        sin_bloqueado         '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('destaque', 'pk_destaque', array('id_destaque'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE distribuicao
      (
        id_distribuicao       '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_distribuicao_agrupador '.$objInfraMetaBD->tipoNumero().' NULL,
        id_colegiado_versao   '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_procedimento       '.$objInfraMetaBD->tipoNumeroGrande().' NOT NULL,
        id_motivo_prevencao   '.$objInfraMetaBD->tipoNumero().' NULL,
        id_usuario_relator    '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade_relator    '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario_relator_acordao  '.$objInfraMetaBD->tipoNumero().' NULL,
        id_unidade_relator_acordao  '.$objInfraMetaBD->tipoNumero().' NULL,
        id_documento_distribuicao  '.$objInfraMetaBD->tipoNumeroGrande().' NOT NULL,
        sta_distribuicao      '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        sta_distribuicao_anterior  '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        sin_prevencao         '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        dth_distribuicao      '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        dth_situacao_atual    '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        sta_ultimo            '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_motivo_canc_distribuicao  '.$objInfraMetaBD->tipoNumero().' NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('distribuicao', 'pk_distribuicao', array('id_distribuicao'));
        $objInfraMetaBD->criarIndice('distribuicao', 'i06_distribuicao', array('id_procedimento', 'sta_distribuicao'));
        $objInfraMetaBD->criarIndice('distribuicao', 'i10_distribuicao', array('sta_ultimo'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE impedimento
      (
        id_impedimento        '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_distribuicao       '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_motivo_impedimento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('impedimento', 'pk_impedimento', array('id_impedimento'));


        BancoSEI::getInstance()->executarSql('CREATE TABLE item_sessao_documento
      (
        id_item_sessao_documento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_documento              '.$objInfraMetaBD->tipoNumeroGrande().' NOT NULL,
        id_item_sessao_julgamento '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario                '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade                '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('item_sessao_documento', 'pk_item_sessao_documento', array('id_item_sessao_documento'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE item_sessao_julgamento
      (
        id_item_sessao_julgamento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_sessao_julgamento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_distribuicao       '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_documento          '.$objInfraMetaBD->tipoNumeroGrande().' NULL,
        id_documento_julgamento  '.$objInfraMetaBD->tipoNumeroGrande().' NULL,
        id_usuario_presidente  '.$objInfraMetaBD->tipoNumero().' NULL,
        id_usuario_sessao      '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade_sessao      '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sta_item_sessao_julgamento  '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        dispositivo           '.$objInfraMetaBD->tipoTextoGrande().' NULL,
        dth_inclusao          '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        ordem                 '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sta_situacao          '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        sin_manual            '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        id_motivo_mesa        '.$objInfraMetaBD->tipoNumero().' NULL,
        provimento            '.$objInfraMetaBD->tipoTextoVariavel(4000).' NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('item_sessao_julgamento', 'pk_item_sessao_julgamento', array('id_item_sessao_julgamento'));
        $objInfraMetaBD->criarIndice('item_sessao_julgamento', 'i06_item_sessao_julgamento', array('sta_situacao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE julgamento_parte
      (
        id_julgamento_parte   '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_item_sessao_julgamento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(100).' NOT NULL,
        id_usuario_desempate  '.$objInfraMetaBD->tipoNumero().' NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('julgamento_parte', 'pk_julgamento_parte', array('id_julgamento_parte'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE motivo_ausencia
      (
        id_motivo_ausencia    '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(100).' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('motivo_ausencia', 'pk_motivo_ausencia', array('id_motivo_ausencia'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE motivo_canc_distribuicao
      (
        id_motivo_canc_distribuicao  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(250).' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('motivo_canc_distribuicao', 'pk_motivo_canc_distribuicao', array('id_motivo_canc_distribuicao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE motivo_distribuicao
      (
        id_motivo_distribuicao  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(250).' NOT NULL,
        sta_tipo              '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        id_colegiado          '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('motivo_distribuicao', 'pk_motivo_distribuicao', array('id_motivo_distribuicao'));
        $objInfraMetaBD->criarIndice('motivo_distribuicao', 'i02_motivo_distribuicao', array('sta_tipo'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE motivo_mesa
      (
        id_motivo_mesa        '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(250).' NOT NULL,
        ordem                 '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('motivo_mesa', 'pk_motivo_mesa', array('id_motivo_mesa'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE parte_procedimento
      (
        id_parte_procedimento '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_procedimento       '.$objInfraMetaBD->tipoNumeroGrande().' NOT NULL,
        id_contato            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_qualificacao_parte '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade            '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('parte_procedimento', 'pk_parte_procedimento', array('id_parte_procedimento'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE pedido_vista
      (
        id_pedido_vista       '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_distribuicao       '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        dth_pedido            '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        dth_devolucao         '.$objInfraMetaBD->tipoDataHora().' NULL,
        sin_pendente          '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_unidade            '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('pedido_vista', 'pk_pedido_vista', array('id_pedido_vista'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE presenca_sessao
      (
        id_presenca_sessao    '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_sessao_julgamento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        dth_entrada           '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        dth_saida             '.$objInfraMetaBD->tipoDataHora().' NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('presenca_sessao', 'pk_presenca_sessao', array('id_presenca_sessao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE provimento
      (
        id_provimento         '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        conteudo              '.$objInfraMetaBD->tipoTextoVariavel(4000).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('provimento', 'pk_provimento', array('id_provimento'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE qualificacao_parte
      (
        id_qualificacao_parte '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(100).' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('qualificacao_parte', 'pk_qualificacao_parte', array('id_qualificacao_parte'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE rel_colegiado_usuario
      (
        id_colegiado           '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('rel_colegiado_usuario', 'pk_rel_colegiado_usuario', array('id_colegiado', 'id_usuario'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE rel_destaque_usuario
      (
        id_destaque           '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('rel_destaque_usuario', 'pk_rel_destaque_usuario', array('id_destaque', 'id_usuario'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE rel_proced_tipo_materia
      (
        id_procedimento       '.$objInfraMetaBD->tipoNumeroGrande().' NOT NULL,
        id_tipo_materia       '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('rel_proced_tipo_materia', 'pk_rel_proced_tipo_materia', array('id_procedimento', 'id_tipo_materia'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE sessao_julgamento
      (
        id_sessao_julgamento  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_procedimento       '.$objInfraMetaBD->tipoNumeroGrande().' NULL,
        id_colegiado          '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_colegiado_versao   '.$objInfraMetaBD->tipoNumero().' NULL,
        id_usuario_presidente '.$objInfraMetaBD->tipoNumero().' NULL,
        id_documento_sessao   '.$objInfraMetaBD->tipoNumeroGrande().' NULL,
        dth_sessao            '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        sta_situacao          '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        sta_tipo              '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        dth_inicio            '.$objInfraMetaBD->tipoDataHora().' NULL,
        dth_fim               '.$objInfraMetaBD->tipoDataHora().' NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('sessao_julgamento', 'pk_sessao_julgamento', array('id_sessao_julgamento'));
        $objInfraMetaBD->criarIndice('sessao_julgamento', 'i05_sessao_julgamento', array('dth_sessao'));
        $objInfraMetaBD->criarIndice('sessao_julgamento', 'i06_sessao_julgamento', array('sta_situacao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE sustentacao_oral
      (
        id_sustentacao_oral       '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_item_sessao_julgamento '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_qualificacao_parte     '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_contato                '.$objInfraMetaBD->tipoNumero().' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('sustentacao_oral', 'pk_sustentacao_oral', array('id_sustentacao_oral'));


        BancoSEI::getInstance()->executarSql('CREATE TABLE tarefa_sessao
      (
        id_tarefa_sessao      '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        nome                  '.$objInfraMetaBD->tipoTextoVariavel(250).' NOT NULL,
        sin_historico_resumido '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('tarefa_sessao', 'pk_tarefa_sessao', array('id_tarefa_sessao'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE tipo_materia
      (
        id_tipo_materia       '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        descricao             '.$objInfraMetaBD->tipoTextoVariavel(250).' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('tipo_materia', 'pk_tipo_materia', array('id_tipo_materia'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE tipo_membro_colegiado
      (
        id_tipo_membro_colegiado  '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        nome                  '.$objInfraMetaBD->tipoTextoVariavel(50).' NOT NULL,
        sin_ativo             '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('tipo_membro_colegiado', 'pk_tipo_membro_colegiado', array('id_tipo_membro_colegiado'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE voto_parte
      (
        id_voto_parte         '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_voto_parte_associado  '.$objInfraMetaBD->tipoNumero().' NULL,
        id_julgamento_parte   '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_sessao_voto        '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_documento          '.$objInfraMetaBD->tipoNumeroGrande().' NULL,
        id_usuario            '.$objInfraMetaBD->tipoNumero().' NOT NULL,
        id_provimento         '.$objInfraMetaBD->tipoNumero().' NULL,
        complemento           '.$objInfraMetaBD->tipoTextoVariavel(4000).' NULL,
        ressalva              '.$objInfraMetaBD->tipoTextoVariavel(1000).' NULL,
        sta_voto_parte        '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL,
        dth_voto              '.$objInfraMetaBD->tipoDataHora().' NOT NULL,
        sin_vencedor          '.$objInfraMetaBD->tipoTextoFixo(1).' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('voto_parte', 'pk_voto_parte', array('id_voto_parte'));

        $objInfraMetaBD->adicionarChaveEstrangeira('fk_algoritmo_usuario', 'algoritmo', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_algoritmo_colegiado', 'algoritmo', array('id_colegiado'), 'colegiado', array('id_colegiado'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_andamento_sess_tarefa_sess', 'andamento_sessao', array('id_tarefa_sessao'), 'tarefa_sessao', array('id_tarefa_sessao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_andamento_sess_sess_julg', 'andamento_sessao', array('id_sessao_julgamento'), 'sessao_julgamento', array('id_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_andamento_sessao_unidade', 'andamento_sessao', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_andamento_sessao_usuario', 'andamento_sessao', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_atrib_and_sessao_and_sessao', 'atributo_andamento_sessao', array('id_andamento_sessao'), 'andamento_sessao', array('id_andamento_sessao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_ausencia_sessao_usuario', 'ausencia_sessao', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_ausencia_sessao_sessao', 'ausencia_sessao', array('id_sessao_julgamento'), 'sessao_julgamento', array('id_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_ausencia_sessao_motivo_aus', 'ausencia_sessao', array('id_motivo_ausencia'), 'motivo_ausencia', array('id_motivo_ausencia'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_colegiado_tipo_procedimento', 'colegiado', array('id_tipo_procedimento'), 'tipo_procedimento', array('id_tipo_procedimento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_colegiado_secretario', 'colegiado', array('id_usuario_secretario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_colegiado_presidente', 'colegiado', array('id_usuario_presidente'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_colegiado_unidade', 'colegiado', array('id_unidade_responsavel'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_composic_cargo', 'colegiado_composicao', array('id_cargo'), 'cargo', array('id_cargo'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_composic_coleg_versao', 'colegiado_composicao', array('id_colegiado_versao'), 'colegiado_versao', array('id_colegiado_versao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_composic_unidade', 'colegiado_composicao', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_composic_usuario', 'colegiado_composicao', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_comp_tipo_membro', 'colegiado_composicao', array('id_tipo_membro_colegiado'), 'tipo_membro_colegiado', array('id_tipo_membro_colegiado'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_colegiado_versao_colegiado', 'colegiado_versao', array('id_colegiado'), 'colegiado', array('id_colegiado'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_versao_unidade', 'colegiado_versao', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_versao_usuario', 'colegiado_versao', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_versao_usuario_secr', 'colegiado_versao', array('id_usuario_secretario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_versao_usuario_presid', 'colegiado_versao', array('id_usuario_presidente'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_versao_unidade_resp', 'colegiado_versao', array('id_unidade_responsavel'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_coleg_versao_tipo_proced', 'colegiado_versao', array('id_tipo_procedimento'), 'tipo_procedimento', array('id_tipo_procedimento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_destaque_unidade', 'destaque', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_destaque_usuario', 'destaque', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_destaque_item_sessao_julg', 'destaque', array('id_item_sessao_julgamento'), 'item_sessao_julgamento', array('id_item_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distrib_usuario_relat_acord', 'distribuicao', array('id_usuario_relator_acordao'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distribuicao_distribuicao', 'distribuicao', array('id_distribuicao_agrupador'), 'distribuicao', array('id_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distribuicao_usuario', 'distribuicao', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distrib_unidade_relat_acord', 'distribuicao', array('id_unidade_relator_acordao'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distribuicao_unidade', 'distribuicao', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distrib_mot_canc_distrib', 'distribuicao', array('id_motivo_canc_distribuicao'), 'motivo_canc_distribuicao', array('id_motivo_canc_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distrib_usuario_relator', 'distribuicao', array('id_usuario_relator'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distribuicao_procedimento', 'distribuicao', array('id_procedimento'), 'procedimento', array('id_procedimento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distribuicao_coleg_versao', 'distribuicao', array('id_colegiado_versao'), 'colegiado_versao', array('id_colegiado_versao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distrib_unidade_relator', 'distribuicao', array('id_unidade_relator'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distribuicao_motivo_distrib', 'distribuicao', array('id_motivo_prevencao'), 'motivo_distribuicao', array('id_motivo_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_distribuicao_documento', 'distribuicao', array('id_documento_distribuicao'), 'documento', array('id_documento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_impedimento_usuario', 'impedimento', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_impedimento_distribuicao', 'impedimento', array('id_distribuicao'), 'distribuicao', array('id_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_impedimento_motivo_distrib', 'impedimento', array('id_motivo_impedimento'), 'motivo_distribuicao', array('id_motivo_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sessao_motivo_mesa', 'item_sessao_julgamento', array('id_motivo_mesa'), 'motivo_mesa', array('id_motivo_mesa'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_it_sess_doc_it_sess_julg', 'item_sessao_documento', array('id_item_sessao_julgamento'), 'item_sessao_julgamento', array('id_item_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sessao_doc_usuario', 'item_sessao_documento', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sessao_doc_documento', 'item_sessao_documento', array('id_documento'), 'documento', array('id_documento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sessao_doc_unidade', 'item_sessao_documento', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sess_julg_sess_julg', 'item_sessao_julgamento', array('id_sessao_julgamento'), 'sessao_julgamento', array('id_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sess_julg_distribuicao', 'item_sessao_julgamento', array('id_distribuicao'), 'distribuicao', array('id_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sess_julg_documento', 'item_sessao_julgamento', array('id_documento'), 'documento', array('id_documento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sess_julg_presidente', 'item_sessao_julgamento', array('id_usuario_presidente'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sess_julg_usuario_sess', 'item_sessao_julgamento', array('id_usuario_sessao'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sess_julg_unidade_sess', 'item_sessao_julgamento', array('id_unidade_sessao'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_item_sess_julg_doc_julgamen', 'item_sessao_julgamento', array('id_documento_julgamento'), 'documento', array('id_documento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_julg_parte_usu_desempate', 'julgamento_parte', array('id_usuario_desempate'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_julg_parte_item_sess_julg', 'julgamento_parte', array('id_item_sessao_julgamento'), 'item_sessao_julgamento', array('id_item_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_motivo_distrib_colegiado', 'motivo_distribuicao', array('id_colegiado'), 'colegiado', array('id_colegiado'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_parte_proced_contato', 'parte_procedimento', array('id_contato'), 'contato', array('id_contato'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_parte_proced_procedimento', 'parte_procedimento', array('id_procedimento'), 'procedimento', array('id_procedimento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_parte_proced_qualif_parte', 'parte_procedimento', array('id_qualificacao_parte'), 'qualificacao_parte', array('id_qualificacao_parte'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_parte_proced_unidade', 'parte_procedimento', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_pedido_vista_distribuicao', 'pedido_vista', array('id_distribuicao'), 'distribuicao', array('id_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_pedido_vista_usuario', 'pedido_vista', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_pedido_vista_unidade', 'pedido_vista', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_presenca_sessao_usuario', 'presenca_sessao', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_presenca_sessao_sess_julg', 'presenca_sessao', array('id_sessao_julgamento'), 'sessao_julgamento', array('id_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_coleg_usuario_colegiado', 'rel_colegiado_usuario', array('id_colegiado'), 'colegiado', array('id_colegiado'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_coleg_usuario_usuario', 'rel_colegiado_usuario', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_destaque_usuario_destaq', 'rel_destaque_usuario', array('id_destaque'), 'destaque', array('id_destaque'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_destaque_usuario_usuari', 'rel_destaque_usuario', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_proced_tipo_mat_proced', 'rel_proced_tipo_materia', array('id_procedimento'), 'procedimento', array('id_procedimento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_proced_tipo_mat_tp_mat', 'rel_proced_tipo_materia', array('id_tipo_materia'), 'tipo_materia', array('id_tipo_materia'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_julgamento_colegiado', 'sessao_julgamento', array('id_colegiado'), 'colegiado', array('id_colegiado'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_julgamento_coleg_ver', 'sessao_julgamento', array('id_colegiado_versao'), 'colegiado_versao', array('id_colegiado_versao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_julgamento_proced', 'sessao_julgamento', array('id_procedimento'), 'procedimento', array('id_procedimento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_julgamento_presid', 'sessao_julgamento', array('id_usuario_presidente'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_julgamento_documento', 'sessao_julgamento', array('id_documento_sessao'), 'documento', array('id_documento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sustent_oral_item_sess_julg', 'sustentacao_oral', array('id_item_sessao_julgamento'), 'item_sessao_julgamento', array('id_item_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sustent_oral_qualif_parte', 'sustentacao_oral', array('id_qualificacao_parte'), 'qualificacao_parte', array('id_qualificacao_parte'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sustent_oral_contato', 'sustentacao_oral', array('id_contato'), 'contato', array('id_contato'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_parte_julgamento_parte', 'voto_parte', array('id_julgamento_parte'), 'julgamento_parte', array('id_julgamento_parte'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_item_usuario', 'voto_parte', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_parte_voto_parte', 'voto_parte', array('id_voto_parte_associado'), 'voto_parte', array('id_voto_parte'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_parte_documento', 'voto_parte', array('id_documento'), 'documento', array('id_documento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_parte_sessao_julg', 'voto_parte', array('id_sessao_voto'), 'sessao_julgamento', array('id_sessao_julgamento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_parte_provimento', 'voto_parte', array('id_provimento'), 'provimento', array('id_provimento'));


        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIES_JULGAMENTO\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_CERTIDAO_CANCELAMENTO_SESSAO\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_EDITAL_CANCELAMENTO_SESSAO\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_JULGAMENTO_RELATORIOS\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_JULGAMENTO_VOTOS\',\'\')');

        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_CERTIDAO_DISTRIBUICAO\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_CERTIDAO_JULGAMENTO\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_CERTIDAO_REDISTRIBUICAO\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_JULGAMENTO_ATA\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_JULGAMENTO_PAUTA\',\'\')');
        BancoSEI::getInstance()->executarSql('insert into infra_parametro (nome, valor) values (\'ID_SERIE_JULGAMENTO_DISPONIBILIZAVEIS\',\'\')');

        BancoSEI::getInstance()->executarSql('insert into tipo_membro_colegiado (id_tipo_membro_colegiado, nome, sin_ativo) values (1,\'Titular\',\'S\')');
        BancoSEI::getInstance()->executarSql('insert into tipo_membro_colegiado (id_tipo_membro_colegiado, nome, sin_ativo) values (2,\'Suplente\',\'S\')');


        if (BancoSEI::getInstance() instanceof InfraMySql) {

          BancoSEI::getInstance()->executarSql('drop table seq_tarefa');

          $rs = BancoSEI::getInstance()->consultarSql('select max(id_tarefa) as ultimo from tarefa');

          $ultimo = $rs[0]['ultimo'];
          if ($ultimo == null) {
            $numInicial = 1;
          } else if ($ultimo <= 1000) {
            $numInicial = 1001;
          } else {
            $numInicial = $ultimo + 1;
          }

          BancoSEI::getInstance()->criarSequencialNativa('seq_tarefa', $numInicial);
        }


        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo distribu�do (@COLEGIADO@, @RELATOR@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_PROCESSO_DISTRIBUIDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo redistribu�do (@COLEGIADO@, @RELATOR@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_PROCESSO_REDISTRIBUIDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo inclu�do em pauta (@COLEGIADO@, @SESSAO@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_INCLUSAO_PAUTA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo retirado da pauta (@COLEGIADO@).'."\n".'@MOTIVO@\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_RETIRADA_PAUTA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo inclu�do em mesa (@COLEGIADO@, @SESSAO@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_INCLUSAO_MESA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo retirado da mesa (@COLEGIADO@).'."\n".'@MOTIVO@\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_RETIRADA_MESA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo inclu�do para referendo (@COLEGIADO@, @SESSAO@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_INCLUSAO_REFERENDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo retirado referendo (@COLEGIADO@).'."\n".'@MOTIVO@\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_RETIRADO_REFERENDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo com julgamento adiado (@COLEGIADO@).'."\n".'@MOTIVO@\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_ADIADO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Pedido de Vista (@COLEGIADO@ por @USUARIO@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_PEDIDO_VISTA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo julgado (@COLEGIADO@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_ENCERRADA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Pedido de Vista cancelado (@COLEGIADO@ por @USUARIO@)\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_PEDIDO_VISTA_CANCELADO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Processo com pedido de vista devolvido para a Secretaria.'."\n".'@MOTIVO@\',\'S\',\'S\',\'N\',\'S\',\'S\',\'TRF4_SESSAO_JULGAMENTO_DEVOLUCAO_SECRETARIA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Distribui��o cancelada (@COLEGIADO@)'."\n".'@MOTIVO@\',\'N\',\'S\',\'N\',\'N\',\'S\',\'TRF4_SESSAO_JULGAMENTO_CANCELAMENTO_DISTRIBUICAO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Sess�o cancelada (@COLEGIADO@)\',\'N\',\'S\',\'S\',\'S\',\'N\',\'TRF4_SESSAO_JULGAMENTO_CANCELADA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa (id_tarefa,nome,sin_historico_resumido,sin_historico_completo,sin_lancar_andamento_fechado,sin_permite_processo_fechado,sin_fechar_andamentos_abertos,id_tarefa_modulo) VALUES ('.BancoSEI::getInstance()->getValorSequencia('seq_tarefa').',\'Julgamento do Processo convertido em Dilig�ncia (@COLEGIADO@)\',\'N\',\'S\',\'N\',\'N\',\'S\',\'TRF4_SESSAO_JULGAMENTO_CONVERSAO_DILIGENCIA\')');

        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (1,\'@SITUACAO@\',\'S\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (2,\'Incluido processo @PROCESSO@ (@TIPO@)\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (3,\'Retirado o processo @PROCESSO@ (@TIPO@).'."\n".'@MOTIVO@\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (4,\'Processo @PROCESSO@ julgado\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (5,\'Processo @PROCESSO@ adiado.'."\n".'@MOTIVO@\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (6,\'Alterada presid�ncia da sess�o para @PRESIDENTE@\',\'S\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (7,\'Disponibilizado documento @DOCUMENTO@ do processo @PROCESSO@\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (8,\'Cancelada a disponibiliza��o do documento @DOCUMENTO@ do processo @PROCESSO@\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (9,\'Pedido de Vista no processo @PROCESSO@ por @USUARIO@\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (10,\'Pedido de Vista no processo @PROCESSO@ cancelado\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (11,\'Julgamento do processo @PROCESSO@ convertido em dilig�ncia\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (12,\'Cancelada a convers�o em dilig�ncia do processo @PROCESSO@\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (13,\'Cancelado o adiamento do processo @PROCESSO@\',\'N\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (14,\'Julgamento do processo @PROCESSO@ convertido para manual\',\'N\')');

        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (1,\'DECIDIU DEFERIR O PEDIDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (2,\'DECIDIU INDEFERIR O PEDIDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (3,\'DECIDIU DAR PROVIMENTO AO RECURSO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (4,\'DECIDIU NEGAR PROVIMENTO AO RECURSO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (5,\'DECIDIU CONVERTER JULGAMENTO EM DILIG�NCIA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (6,\'DECIDIU DEFERIR EM PARTE O PEDIDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (7,\'DECIDIU DAR PARCIAL PROVIMENTO AO RECURSO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (8,\'DECIDIU INDEFERIR O PEDIDO DE RECONSIDERA��O E NEGAR PROVIMENTO AO RECURSO ADMINISTRATIVO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (9,\'REFERENDOU A DECIS�O / EDITAL\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (10,\'DECIDIU JULGAR PREJUDICADO O PEDIDO\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (11,\'DECIDIU APROVAR A PORTARIA / RESOLU��O / EDITAL / PROPOSTA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (12,\'DECIDIU HOMOLOGAR O RESULTADO FINAL DO CERTAME\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (13,\'DECIDIU DEFERIR AS REMO��ES\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (14,\'DECIDIU SUBMETER O PROCESSO AO CJF / CORTE ESPECIAL ADMINISTRATIVA\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (15,\'DECIDIU ACOLHER A QUEST�O DE ORDEM\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO provimento (id_provimento,conteudo) VALUES (16,\'DECIDIU REJEITAR A QUEST�O DE ORDEM\')');

        BancoSEI::getInstance()->executarSql('INSERT INTO qualificacao_parte (id_qualificacao_parte,descricao,sin_ativo) VALUES (1,\'Interessado\',\'S\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO qualificacao_parte (id_qualificacao_parte,descricao,sin_ativo) VALUES (4,\'Advogado\',\'S\')');

        BancoSEI::getInstance()->criarSequencialNativa('seq_algoritmo', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_andamento_sessao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_atributo_andamento_sessao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_ausencia_sessao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_colegiado', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_colegiado_composicao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_colegiado_versao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_destaque', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_distribuicao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_impedimento', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_item_sessao_documento', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_item_sessao_julgamento', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_julgamento_item', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_julgamento_parte', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_motivo_ausencia', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_motivo_canc_distribuicao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_motivo_distribuicao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_motivo_mesa', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_parte_procedimento', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_pedido_vista', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_presenca_sessao', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_provimento', 17);
        BancoSEI::getInstance()->criarSequencialNativa('seq_qualificacao_parte', 5);
        BancoSEI::getInstance()->criarSequencialNativa('seq_sessao_julgamento', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_sustentacao_oral', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_tarefa_sessao', 12);
        BancoSEI::getInstance()->criarSequencialNativa('seq_tipo_materia', 1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_voto_parte', 1);

      } catch (Exception $e) {
        throw new InfraException('Erro atualizando vers�o 1.0.0 do m�dulo SEI Julgar.', $e);
      }
    }

    public function versao_1_1_0($strVersaoAtual)
    {
      try {

        $objInfraMetaBD = new InfraMetaBD(BancoSEI::getInstance());

        $objInfraMetaBD->excluirColuna('item_sessao_julgamento', 'provimento');
        $objInfraMetaBD->excluirChaveEstrangeira('voto_parte', 'fk_voto_parte_documento');
        $objInfraMetaBD->excluirIndice('voto_parte','fk_voto_parte_documento');
        $objInfraMetaBD->excluirColuna('voto_parte', 'id_documento');

        $objInfraMetaBD->adicionarColuna('destaque', 'sta_acesso', $objInfraMetaBD->tipoTextoFixo(1), 'NULL');
        BancoSEI::getInstance()->executarSql('UPDATE destaque set sta_acesso=sta_tipo');
        $objInfraMetaBD->alterarColuna('destaque', 'sta_acesso', $objInfraMetaBD->tipoTextoFixo(1), 'NOT NULL');
        $objInfraMetaBD->alterarColuna('destaque', 'descricao', $objInfraMetaBD->tipoTextoVariavel(4000), 'NULL');
        $objInfraMetaBD->alterarColuna('destaque', 'sta_tipo', $objInfraMetaBD->tipoTextoFixo(1), 'NULL');
        BancoSEI::getInstance()->executarSql('UPDATE destaque set sta_tipo=\'C\'');

        BancoSEI::getInstance()->executarSql('CREATE TABLE autuacao (
 id_autuacao         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
 id_procedimento         '.$objInfraMetaBD->tipoNumeroGrande().'  NOT NULL ,
 id_tipo_materia           '.$objInfraMetaBD->tipoNumero().' NULL ,
 descricao        '.$objInfraMetaBD->tipoTextoVariavel(1000).'  NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('autuacao', 'pk_autuacao', array('id_autuacao'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_autuacao', 1);
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_autuacao_procedimento', 'autuacao', array('id_procedimento'), 'procedimento', array('id_procedimento'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_autuacao_tipo_materia', 'autuacao', array('id_tipo_materia'), 'tipo_materia', array('id_tipo_materia'));

        //migrar registros de rel_proced_tipo_materia para autuacao
        $objAutuacaoRN = new AutuacaoRN();
        $rs = BancoSEI::getInstance()->executarSql('select * from rel_proced_tipo_materia');
        if (is_array($rs)) {
          foreach ($rs as $registro) {
            $objAutuacaoDTO = new AutuacaoDTO();
            $objAutuacaoDTO->setNumIdTipoMateria($registro['id_tipo_materia']);
            $objAutuacaoDTO->setDblIdProcedimento(BancoSEI::getInstance()->formatarLeituraDbl($registro['id_procedimento']));
            $objAutuacaoRN->cadastrar($objAutuacaoDTO);
          }
        }
        BancoSEI::getInstance()->executarSql('drop table rel_proced_tipo_materia');


      } catch (Exception $e) {
        throw new InfraException('Erro atualizando vers�o 1.1.0 do m�dulo SEI Julgar.', $e);
      }
    }

    public function versao_1_2_0($strVersaoAtual)
    {
      try {

        $objInfraMetaBD = new InfraMetaBD(BancoSEI::getInstance());

        //incluir novo tipo_membro_colegiado
        BancoSEI::getInstance()->executarSql("insert into tipo_membro_colegiado values (3,'Eventual','S')");

        //inclui campo para habilitar/desabilitar membro
        $objInfraMetaBD->adicionarColuna('colegiado_composicao', 'sin_habilitado', 'char(1)', 'null');
        BancoSEI::getInstance()->executarSql("update colegiado_composicao set sin_habilitado='N'");
        BancoSEI::getInstance()->executarSql("update colegiado_composicao set sin_habilitado='S' where id_tipo_membro_colegiado=1");
        $objInfraMetaBD->alterarColuna('colegiado_composicao', 'sin_habilitado', 'char(1)', 'not null');

        BancoSEI::getInstance()->executarSql('CREATE TABLE rel_motivo_distr_colegiado (
            id_motivo_distribuicao         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            id_colegiado         '.$objInfraMetaBD->tipoNumero().'  NOT NULL)');
        $objInfraMetaBD->adicionarChavePrimaria('rel_motivo_distr_colegiado', 'pk_rel_motivo_distr_colegiado', array('id_motivo_distribuicao', 'id_colegiado'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_motiv_distr_motiv_distr', 'rel_motivo_distr_colegiado', array('id_motivo_distribuicao'), 'motivo_distribuicao', array('id_motivo_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_motiv_distr_colegiado', 'rel_motivo_distr_colegiado', array('id_colegiado'), 'colegiado', array('id_colegiado'));


        //migrar registros de motivo_distribuicao para rel_distribuicao
        BancoSEI::getInstance()->executarSql("insert into  rel_motivo_distr_colegiado (id_motivo_distribuicao,id_colegiado) select id_motivo_distribuicao,id_colegiado from motivo_distribuicao where sin_ativo='S'");
        //remover duplicatas do motivo_distribuicao
        $rs = BancoSEI::getInstance()->executarSql('select * from motivo_distribuicao order by sta_tipo asc,descricao asc,sin_ativo desc');
        if (is_array($rs)) {
          $tipo = '';
          $descricao = '';
          $idMotivo = null;
          foreach ($rs as $registro) {
            if ($tipo != $registro['sta_tipo'] || $descricao != $registro['descricao']) {
              $tipo = $registro['sta_tipo'];
              $descricao = $registro['descricao'];
              $idMotivo = $registro['id_motivo_distribuicao'];
              continue;
            }
            $idMotivoEliminar = $registro['id_motivo_descricao'];
            if ($tipo == MotivoDistribuicaoRN::$TIPO_PREVENCAO) {
              BancoSEI::getInstance()->executarSql("update distribuicao set id_motivo_prevencao=$idMotivo where id_motivo_prevencao=$idMotivoEliminar");
            } else if ($tipo == MotivoDistribuicaoRN::$TIPO_IMPEDIMENTO) {
              BancoSEI::getInstance()->executarSql("update impedimento set id_motivo_impedimento=$idMotivo where id_motivo_impedimento=$idMotivoEliminar");
            }
            BancoSEI::getInstance()->executarSql("delete from motivo_distribuicao where id_motivo_distribuicao=$idMotivoEliminar");
          }
        }
        $objInfraMetaBD->excluirChaveEstrangeira('motivo_distribuicao', 'fk_motivo_distrib_colegiado');
        $objInfraMetaBD->excluirIndice('motivo_distribuicao', 'fk_motivo_distrib_colegiado');
        $objInfraMetaBD->excluirColuna('motivo_distribuicao', 'id_colegiado');


        //incluir campo ordem nas fra��es de julgamento
        $objInfraMetaBD->adicionarColuna('julgamento_parte', 'ordem', 'int', 'null');

        $objJulgamentoParteDTO = new JulgamentoParteDTO();
        $objJulgamentoParteRN = new JulgamentoParteRN();
        $objJulgamentoParteDTO->setNumOrdem(null);
        $objJulgamentoParteDTO->retNumIdJulgamentoParte();
        $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
        $objJulgamentoParteDTO->setOrdNumIdItemSessaoJulgamento(InfraDTO::$TIPO_ORDENACAO_ASC);
        $objJulgamentoParteDTO->setOrdNumIdJulgamentoParte(InfraDTO::$TIPO_ORDENACAO_ASC);
        $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
        if (count($arrObjJulgamentoParteDTO) > 0) {
          $idItem = null;
          $ordem = 1;
          foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
            if ($idItem == $objJulgamentoParteDTO->getNumIdItemSessaoJulgamento()) {
              $ordem++;
            } else {
              $ordem = 1;
              $idItem = $objJulgamentoParteDTO->getNumIdItemSessaoJulgamento();
            }
            $numParte = $objJulgamentoParteDTO->getNumIdJulgamentoParte();
            BancoSEI::getInstance()->executarSql("update julgamento_parte set ordem=$ordem where id_julgamento_parte=$numParte");
          }
        }
        $objInfraMetaBD->alterarColuna('julgamento_parte', 'ordem', 'int', 'not null');
        $objInfraMetaBD->alterarColuna('julgamento_parte', 'descricao', $objInfraMetaBD->tipoTextoVariavel(4000), 'not null');
        $objInfraMetaBD->alterarColuna('autuacao', 'descricao', $objInfraMetaBD->tipoTextoVariavel(2000), 'null');

      } catch (Exception $e) {
        throw new InfraException('Erro atualizando vers�o 1.2.0 do m�dulo SEI Julgar.', $e);
      }
    }

    public function versao_1_3_0($strVersaoAtual)
    {
      try {

        $objInfraMetaBD = new InfraMetaBD(BancoSEI::getInstance());

        BancoSEI::getInstance()->executarSql('CREATE TABLE bloqueio_item_sess_unidade (
            id_bloqueio_item_sess_unidade         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            id_distribuicao         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            id_documento         '.$objInfraMetaBD->tipoNumeroGrande().'  NULL ,
            id_unidade         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            sta_tipo         '.$objInfraMetaBD->tipoTextoFixo(1).'  NOT NULL)');
        $objInfraMetaBD->adicionarChavePrimaria('bloqueio_item_sess_unidade', 'pk_bloqueio_item_sess_unidade', array('id_bloqueio_item_sess_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_bloqueio_item_unidade', 'bloqueio_item_sess_unidade', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_bloqueio_item_distrib', 'bloqueio_item_sess_unidade', array('id_distribuicao'), 'distribuicao', array('id_distribuicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_bloqueio_item_documento', 'bloqueio_item_sess_unidade', array('id_documento'), 'documento', array('id_documento'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_bloqueio_item_sess_unidade', 1);

//        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_motiv_distr_motiv_distr', 'rel_motivo_distr_colegiado', array('id_motivo_distribuicao'), 'motivo_distribuicao', array('id_motivo_distribuicao'));
//        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_motiv_distr_colegiado', 'rel_motivo_distr_colegiado', array('id_colegiado'), 'colegiado', array('id_colegiado'));

        $objInfraMetaBD->adicionarColuna('colegiado', 'artigo', $objInfraMetaBD->tipoTextoFixo(1), 'null');

        if (BancoSEI::getInstance() instanceof InfraOracle) {
          BancoSEI::getInstance()->executarSql('alter table destaque rename column descricao to descricao_old');
          $objInfraMetaBD->adicionarColuna('destaque', 'descricao', $objInfraMetaBD->tipoTextoGrande(), 'null');
          BancoSEI::getInstance()->executarSql('UPDATE destaque SET descricao = descricao_old');
          $objInfraMetaBD->excluirColuna('destaque','descricao_old');
        } else {
          $objInfraMetaBD->alterarColuna('destaque', 'descricao', $objInfraMetaBD->tipoTextoGrande(), 'null');
        }



        BancoSEI::getInstance()->executarSql('CREATE TABLE revisao_item (
            id_revisao_item         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            id_item_sessao_julgamento         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            id_usuario         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            id_unidade         '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            dth_revisao         '.$objInfraMetaBD->tipoDataHora().'  NOT NULL ,
            sin_revisado         '.$objInfraMetaBD->tipoTextoFixo(1).'  NOT NULL)');
        $objInfraMetaBD->adicionarChavePrimaria('revisao_item', 'pk_revisao_item', array('id_revisao_item'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_revisao_item_unidade', 'revisao_item', array('id_unidade'), 'unidade', array('id_unidade'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_revisao_item_usuario', 'revisao_item', array('id_usuario'), 'usuario', array('id_usuario'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_revisao_item_item_sess_julg', 'revisao_item', array('id_item_sessao_julgamento'), 'item_sessao_julgamento', array('id_item_sessao_julgamento'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_revisao_item', 1);

        if(!(BancoSEI::getInstance() instanceof InfraMySqli)){
          $objInfraMetaBD->excluirIndice('andamento_sessao','fk_andamento_sessao_unidade');
        }
        $objInfraMetaBD->alterarColuna('andamento_sessao', 'id_unidade', $objInfraMetaBD->tipoNumero(), 'not null');
        if(!(BancoSEI::getInstance() instanceof InfraMySqli)) {
          $objInfraMetaBD->criarIndice('andamento_sessao', 'fk_andamento_sessao_unidade', ['id_unidade']);
        }

        $this->fix_sessao_cancelada();

      } catch (Exception $e) {
        throw new InfraException('Erro atualizando vers�o 1.3.0 do m�dulo SEI Julgar.', $e);
      }
    }

    public function versao_1_4_0($strVersaoAtual)
    {
      try {

        $objInfraMetaBD = new InfraMetaBD(BancoSEI::getInstance());

        BancoSEI::getInstance()->executarSql('CREATE TABLE rel_autuacao_tipo_materia
      (
        id_autuacao           ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL,
        id_tipo_materia       ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL,
        ordem                 ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL
)');
        $objInfraMetaBD->adicionarChavePrimaria('rel_autuacao_tipo_materia', 'pk_rel_autuacao_tipo_materia', array('id_autuacao', 'id_tipo_materia'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_aut_tipo_mat_tipo_mat', 'rel_autuacao_tipo_materia', array('id_tipo_materia'), 'tipo_materia', array('id_tipo_materia'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_rel_aut_tipo_mat_autuacao', 'rel_autuacao_tipo_materia', array('id_autuacao'), 'autuacao', array('id_autuacao'));

        //atualizacao
        $sql='INSERT INTO rel_autuacao_tipo_materia (id_autuacao,id_tipo_materia,ordem) select a.id_autuacao,a.id_tipo_materia,1 from autuacao a where a.id_tipo_materia is not null';
        BancoSEI::getInstance()->executarSql($sql);

        $objInfraMetaBD->adicionarColuna('autuacao','idx_autuacao',$objInfraMetaBD->tipoTextoVariavel(4000),'null');

        $this->fix_idx_autuacao();
        $this->fix_sessao_cancelada();

        $objInfraMetaBD->adicionarColuna('sessao_julgamento','id_documento_pauta',$objInfraMetaBD->tipoNumeroGrande(),'null');
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_julgamento_doc_pauta','sessao_julgamento',['id_documento_pauta'],'documento',['id_documento']);

        $objInfraMetaBD->alterarColuna('atributo_andamento_sessao','valor',$objInfraMetaBD->tipoTextoVariavel(4000),'null');

        $objInfraMetaBD->adicionarColuna('provimento','sin_ativo',$objInfraMetaBD->tipoTextoFixo(1),'null');
        BancoSEI::getInstance()->executarSql("update provimento set sin_ativo='S'");
        $objInfraMetaBD->alterarColuna('provimento','sin_ativo',$objInfraMetaBD->tipoTextoFixo(1),'not null');

        BancoSEI::getInstance()->executarSql('CREATE TABLE eleicao (
            id_eleicao           '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            id_item_sessao_julgamento '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            identificacao        '.$objInfraMetaBD->tipoTextoVariavel(250).'  NOT NULL ,
            descricao            '.$objInfraMetaBD->tipoTextoVariavel(4000).'  NULL ,
            sta_situacao         '.$objInfraMetaBD->tipoTextoFixo(1).'  NOT NULL ,
            quantidade           '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
            ordem                '.$objInfraMetaBD->tipoNumero().'  NOT NULL,
            sin_secreta          '.$objInfraMetaBD->tipoTextoFixo(1).'  NOT NULL
             
          )');

        $objInfraMetaBD->adicionarChavePrimaria('eleicao', 'pk_eleicao', array('id_eleicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_eleicao_item_sessao_julg', 'eleicao', array('id_item_sessao_julgamento'), 'item_sessao_julgamento', array('id_item_sessao_julgamento'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_eleicao', 1);

        BancoSEI::getInstance()->executarSql('CREATE TABLE opcao_eleicao (
          id_opcao_eleicao     '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
          id_eleicao           '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
          identificacao        '.$objInfraMetaBD->tipoTextoVariavel(4000).'  NOT NULL ,
          ordem                '.$objInfraMetaBD->tipoNumero().'  NOT NULL 
        )');

        $objInfraMetaBD->adicionarChavePrimaria('opcao_eleicao', 'pk_opcao_eleicao', array('id_opcao_eleicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_opcao_eleicao_eleicao', 'opcao_eleicao', array('id_eleicao'), 'eleicao', array('id_eleicao'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_opcao_eleicao', 1);

        BancoSEI::getInstance()->executarSql('CREATE TABLE voto_eleicao(
          id_voto_eleicao      '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
          id_eleicao           '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
          id_usuario           '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
          dth_voto_eleicao     '.$objInfraMetaBD->tipoDataHora().'  NOT NULL,
          ordem                '.$objInfraMetaBD->tipoNumero().'  NOT NULL
        )');

        $objInfraMetaBD->adicionarChavePrimaria('voto_eleicao', 'pk_voto_eleicao', array('id_voto_eleicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_eleicao_eleicao', 'voto_eleicao', array('id_eleicao'), 'eleicao', array('id_eleicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_eleicao_usuario', 'voto_eleicao', array('id_usuario'), 'usuario', array('id_usuario'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_voto_eleicao', 1);

        BancoSEI::getInstance()->executarSql('CREATE TABLE voto_opcao_eleicao (
          id_voto_opcao_eleicao '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
          id_voto_eleicao      '.$objInfraMetaBD->tipoNumero().'  NULL ,
          id_opcao_eleicao     '.$objInfraMetaBD->tipoNumero().'  NOT NULL ,
          ordem                '.$objInfraMetaBD->tipoNumero().'  NOT NULL 
        )');

        $objInfraMetaBD->adicionarChavePrimaria('voto_opcao_eleicao', 'pk_voto_opcao_eleicao', array('id_voto_opcao_eleicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_opcao_eleicao_voto', 'voto_opcao_eleicao', array('id_voto_eleicao'), 'voto_eleicao', array('id_voto_eleicao'));
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_voto_opcao_eleicao_opcao', 'voto_opcao_eleicao', array('id_opcao_eleicao'), 'opcao_eleicao', array('id_opcao_eleicao'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_voto_opcao_eleicao', 1);

        BancoSEI::getInstance()->executarSql('INSERT INTO tarefa_sessao (id_tarefa_sessao,nome,sin_historico_resumido) VALUES (16,\'Escrut�nio Eletr�nico "@ELEICAO@" @SITUACAO@ no processo @PROCESSO@\',\'N\')');
      } catch (Exception $e) {
        throw new InfraException('Erro atualizando vers�o 1.4.0 do m�dulo SEI Julgar.', $e);
      }
    }
    public function versao_2_0_0($strVersaoAtual)
    {
      try {

        $objInfraMetaBD = new InfraMetaBD(BancoSEI::getInstance());
        BancoSEI::getInstance()->executarSql("update destaque set sta_tipo='T' where sta_acesso='R'");

        BancoSEI::getInstance()->executarSql('CREATE TABLE tipo_sessao (
            id_tipo_sessao         ' . $objInfraMetaBD->tipoNumero() . '  NOT NULL ,
            descricao         ' . $objInfraMetaBD->tipoTextoVariavel(50) . '  NOT NULL ,
            sin_virtual         ' . $objInfraMetaBD->tipoTextoFixo(1) . '  NOT NULL ,
            sin_ativo         ' . $objInfraMetaBD->tipoTextoFixo(1) . '  NOT NULL)');
        $objInfraMetaBD->adicionarChavePrimaria('tipo_sessao', 'pk_tipo_sessao', array('id_tipo_sessao'));
        BancoSEI::getInstance()->criarSequencialNativa('seq_tipo_sessao',6);

        BancoSEI::getInstance()->executarSql('INSERT INTO tipo_sessao (id_tipo_sessao,descricao,sin_virtual,sin_ativo) values (1,\'Ordin�ria\',\'N\',\'S\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tipo_sessao (id_tipo_sessao,descricao,sin_virtual,sin_ativo) values (2,\'Extraordin�ria\',\'N\',\'S\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tipo_sessao (id_tipo_sessao,descricao,sin_virtual,sin_ativo) values (3,\'Comemorativa\',\'N\',\'S\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tipo_sessao (id_tipo_sessao,descricao,sin_virtual,sin_ativo) values (4,\'Solene\',\'N\',\'S\')');
        BancoSEI::getInstance()->executarSql('INSERT INTO tipo_sessao (id_tipo_sessao,descricao,sin_virtual,sin_ativo) values (5,\'Virtual\',\'S\',\'S\')');


        $objInfraMetaBD->adicionarColuna('sessao_julgamento','id_tipo_sessao',$objInfraMetaBD->tipoNumero(),'null');
        BancoSEI::getInstance()->executarSql("UPDATE sessao_julgamento set id_tipo_sessao=1 where sta_tipo='O'");
        BancoSEI::getInstance()->executarSql("UPDATE sessao_julgamento set id_tipo_sessao=2 where sta_tipo='E'");
        BancoSEI::getInstance()->executarSql("UPDATE sessao_julgamento set id_tipo_sessao=3 where sta_tipo='C'");
        BancoSEI::getInstance()->executarSql("UPDATE sessao_julgamento set id_tipo_sessao=4 where sta_tipo='S'");
        BancoSEI::getInstance()->executarSql("UPDATE sessao_julgamento set id_tipo_sessao=5 where sta_tipo='V'");
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_julg_tipo_sessao', 'sessao_julgamento', array('id_tipo_sessao'), 'tipo_sessao', array('id_tipo_sessao'));

        $objInfraMetaBD->excluirColuna('sessao_julgamento','sta_tipo');


        BancoSEI::getInstance()->executarSql('CREATE TABLE sessao_bloco (
            id_sessao_bloco      ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL ,
            descricao            ' . $objInfraMetaBD->tipoTextoVariavel(50) . ' NOT NULL,
            ordem                ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL ,
            sin_agrupar_membro   ' . $objInfraMetaBD->tipoTextoFixo(1) . ' NOT NULL ,
            sta_tipo_item        ' . $objInfraMetaBD->tipoTextoFixo(1) . ' NOT NULL ,
            id_sessao_julgamento ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL)');

        $objInfraMetaBD->adicionarChavePrimaria('sessao_bloco', 'pk_sessao_bloco', ['id_sessao_bloco']);
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_bloco_sessao_julg', 'sessao_bloco', array('id_sessao_julgamento'), 'sessao_julgamento', array('id_sessao_julgamento'));

        BancoSEI::getInstance()->executarSql('CREATE TABLE tipo_sessao_bloco (
            id_tipo_sessao_bloco ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL ,
            descricao            ' . $objInfraMetaBD->tipoTextoVariavel(50) . ' NOT NULL,
            ordem                ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL ,
            sin_agrupar_membro   ' . $objInfraMetaBD->tipoTextoFixo(1) . ' NOT NULL ,
            sta_tipo_item        ' . $objInfraMetaBD->tipoTextoFixo(1) . ' NOT NULL ,
            id_tipo_sessao       ' . $objInfraMetaBD->tipoNumero() . ' NOT NULL)');

        $objInfraMetaBD->adicionarChavePrimaria('tipo_sessao_bloco', 'pk_tipo_sessao_bloco', ['id_tipo_sessao_bloco']);
        $objInfraMetaBD->adicionarChaveEstrangeira('fk_sessao_bloco_tipo_sessao', 'tipo_sessao_bloco', array('id_tipo_sessao'), 'tipo_sessao', array('id_tipo_sessao'));

        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (1,'Pauta','1','S','1',1)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (2,'Pauta','1','S','1',2)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (3,'Pauta','1','S','1',3)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (4,'Pauta','1','S','1',4)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (5,'Pauta','1','S','1',5)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (6,'Mesa','2','S','2',1)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (7,'Mesa','2','S','2',2)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (8,'Mesa','2','S','2',3)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (9,'Mesa','2','S','2',4)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (10,'Mesa','2','S','2',5)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (11,'Referendo','3','N','3',1)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (12,'Referendo','3','N','3',2)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (13,'Referendo','3','N','3',3)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (14,'Referendo','3','N','3',4)");
        BancoSEI::getInstance()->executarSql("INSERT INTO tipo_sessao_bloco (id_tipo_sessao_bloco,descricao,ordem,sin_agrupar_membro,sta_tipo_item,id_tipo_sessao) values (15,'Referendo','3','N','3',5)");

        $objInfraMetaBD->adicionarColuna('item_sessao_julgamento','id_sessao_bloco',$objInfraMetaBD->tipoNumero(),'null');


        BancoSEI::getInstance()->criarSequencialNativa('seq_sessao_bloco',1);
        BancoSEI::getInstance()->criarSequencialNativa('seq_tipo_sessao_bloco',16);


        $this->fix_sessao_bloco();
        $this->fix_parametros_modulo();

        $objInfraMetaBD->alterarColuna('item_sessao_julgamento','id_sessao_bloco',$objInfraMetaBD->tipoNumero(),'not null');
        $objInfraMetaBD->excluirColuna('item_sessao_julgamento','sta_item_sessao_julgamento');

      } catch (Exception $e) {
        throw new InfraException('Erro atualizando vers�o 2.0.0 do m�dulo SEI Julgar.', $e);
      }
    }

    public function fix_idx_autuacao()
    {
      $objAutuacaoRN = new AutuacaoRN();
      $objAutuacaoDTO = new AutuacaoDTO();
      $objAutuacaoDTO->setStrDescricao(null, InfraDTO::$OPER_DIFERENTE);
      $objAutuacaoDTO->setStrIdxAutuacao(null);
      $objAutuacaoDTO->retNumIdAutuacao();

      $arrObjAutuacaoDTO = $objAutuacaoRN->listar($objAutuacaoDTO);
      $numRegistros = count($arrObjAutuacaoDTO);
      if ($numRegistros>0) {
        $objAutuacaoDTO2 = new AutuacaoDTO();
        $objAutuacaoDTO2->setNumIdAutuacao(InfraArray::converterArrInfraDTO($arrObjAutuacaoDTO, 'IdAutuacao'));
        $objAutuacaoRN->montarIndexacao($objAutuacaoDTO2);
      }
    }

    public function fix_sessao_cancelada()
    {
      $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO->setStrStaSituacao(SessaoJulgamentoRN::$ES_CANCELADA);
      $objSessaoJulgamentoDTO->retNumIdColegiado();
      $objSessaoJulgamentoDTO->retNumIdColegiadoVersao();
      $objSessaoJulgamentoDTO->retNumIdUsuarioPresidente();
      $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objSessaoJulgamentoDTO->retDthSessao();
      $objSessaoJulgamentoDTO->adicionarCriterio(
          ['IdColegiadoVersao','IdUsuarioPresidente'],
          [InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL],
          [null,null],
          InfraDTO::$OPER_LOGICO_OR
      );
      $arrObjSessaoJulgamentoDTO=$objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);

      $objColegiadoVersaoRN=new ColegiadoVersaoRN();
      $objColegiadoVersaoDTO=new ColegiadoVersaoDTO();
      $objColegiadoVersaoDTO->retNumIdColegiadoVersao();
      $objColegiadoVersaoDTO->retNumIdUsuarioPresidente();
      $objColegiadoVersaoDTO->setNumMaxRegistrosRetorno(1);
      $objColegiadoVersaoDTO->setOrdDthVersao(InfraDTO::$TIPO_ORDENACAO_DESC);

      foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {

        $objColegiadoVersaoDTO->setNumIdColegiado($objSessaoJulgamentoDTO->getNumIdColegiado());
        $objColegiadoVersaoDTO->setDthVersao($objSessaoJulgamentoDTO->getDthSessao(),InfraDTO::$OPER_MENOR_IGUAL);

        $objColegiadoVersaoDTOBanco=$objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);

        $sql="UPDATE sessao_julgamento set id_colegiado_versao=". BancoSEI::getInstance()->formatarGravacaoNum($objColegiadoVersaoDTOBanco->getNumIdColegiadoVersao());
        $sql.=", id_usuario_presidente=".BancoSEI::getInstance()->formatarGravacaoNum($objColegiadoVersaoDTOBanco->getNumIdUsuarioPresidente());
        $sql.=" where id_sessaao_julgamento=".BancoSEI::getInstance()->formatarGravacaoNum($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        BancoSEI::getInstance()->executarSql($sql);
      }


    }
    public function fix_documento_pauta(){
      $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
      $objSessaoJulgamentoRN = new SessaoJulgamentoRN();

      $objSessaoJulgamentoDTO->setStrStaSituacao([SessaoJulgamentoRN::$ES_CANCELADA, SessaoJulgamentoRN::$ES_PAUTA_ABERTA], InfraDTO::$OPER_NOT_IN);
      $objSessaoJulgamentoDTO->setDblIdDocumentoPauta(null);
      $objSessaoJulgamentoDTO->setDblIdProcedimento(null, InfraDTO::$OPER_DIFERENTE);

      $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objSessaoJulgamentoDTO->retDblIdProcedimento();
      $objSessaoJulgamentoDTO->retStrSiglaColegiado();
      $objSessaoJulgamentoDTO->retStrProtocoloFormatadoDocumentoPauta();
      $objSessaoJulgamentoDTO->retDthSessao();

      $arrObjSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);
      if (count($arrObjSessaoJulgamentoDTO)>0) {
        $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
        $idTipoDocumentoPauta = $objInfraParametro->getValor('ID_SERIE_JULGAMENTO_PAUTA', null);
        if ($idTipoDocumentoPauta==null) {
          return;
        }
        /** @var SessaoJulgamentoDTO[] $arrObjSessaoJulgamentoDTO */
        foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {
          $objDocumentoDTO = new DocumentoDTO();
          $objDocumentoDTO->setDblIdProcedimento($objSessaoJulgamentoDTO->getDblIdProcedimento());
          $objDocumentoDTO->setNumIdSerie($idTipoDocumentoPauta);
          $objDocumentoDTO->setStrStaEstadoProtocolo(ProtocoloRN::$CA_DOCUMENTO_CANCELADO, InfraDTO::$OPER_DIFERENTE);
          $objDocumentoDTO->setOrdDblIdDocumento(InfraDTO::$TIPO_ORDENACAO_DESC);
          $objDocumentoDTO->setNumMaxRegistrosRetorno(1);

          $objDocumentoDTO->retStrProtocoloDocumentoFormatado();
          $objDocumentoDTO->retDblIdDocumento();

          $objDocumentoRN = new DocumentoRN();
          /** @var DocumentoDTO $objDocumentoDTO */
          $objDocumentoDTO = $objDocumentoRN->consultarRN0005($objDocumentoDTO);
          if ($objDocumentoDTO) {
            $objSessaoJulgamentoDTO->setDblIdDocumentoPauta($objDocumentoDTO->getDblIdDocumento());
            $objSessaoJulgamentoRN->associarDocumentoPauta($objSessaoJulgamentoDTO);
            InfraDebug::getInstance()->gravar('Associando doc. pauta (' . $objDocumentoDTO->getStrProtocoloDocumentoFormatado() . ') � sess�o: ' . $objSessaoJulgamentoDTO->getStrSiglaColegiado() . ' de ' . $objSessaoJulgamentoDTO->getDthSessao());
          } else {
            InfraDebug::getInstance()->gravar('Doc. pauta n�o encontrado para a sess�o: ' . $objSessaoJulgamentoDTO->getStrSiglaColegiado() . ' de ' . $objSessaoJulgamentoDTO->getDthSessao());
          }
        }
      }
    }
    public function fix_sessao_bloco(){
      $objTipoSessaoBlocoDTO=new TipoSessaoBlocoDTO();
      $objTipoSessaoBlocoRN=new TipoSessaoBlocoRN();
      $objTipoSessaoBlocoDTO->retTodos();
      $arrObjTipoSessaoBlocoDTO=$objTipoSessaoBlocoRN->listar($objTipoSessaoBlocoDTO);
      $arrObjTipoSessaoBlocoDTO=InfraArray::indexarArrInfraDTO($arrObjTipoSessaoBlocoDTO,['IdTipoSessao','StaTipoItem']);

      $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objSessaoJulgamentoDTO->retNumIdTipoSessao();
      $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
      $arrObjSessaoJulgamentoDTO=$objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);

      $objSessaoBlocoRN=new SessaoBlocoRN();
      foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {
        $numIdSessao=$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento();
        $arrIdSessaoBloco=[];
        foreach ($arrObjTipoSessaoBlocoDTO[$objSessaoJulgamentoDTO->getNumIdTipoSessao()] as $objTipoSessaoBlocoDTO) {
          $objSessaoBlocoDTO=new SessaoBlocoDTO();
          $objSessaoBlocoDTO->setNumIdSessaoJulgamento($numIdSessao);
          $objSessaoBlocoDTO->setStrDescricao($objTipoSessaoBlocoDTO->getStrDescricao());
          $objSessaoBlocoDTO->setNumOrdem($objTipoSessaoBlocoDTO->getNumOrdem());
          $objSessaoBlocoDTO->setStrStaTipoItem($objTipoSessaoBlocoDTO->getStrStaTipoItem());
          $objSessaoBlocoDTO->setStrSinAgruparMembro($objTipoSessaoBlocoDTO->getStrSinAgruparMembro());
          $objSessaoBlocoDTO=$objSessaoBlocoRN->cadastrar($objSessaoBlocoDTO);
          $arrIdSessaoBloco[$objTipoSessaoBlocoDTO->getStrStaTipoItem()]=$objSessaoBlocoDTO->getNumIdSessaoBloco();
        }
        foreach ($arrIdSessaoBloco as $chave => $valor) {
          BancoSEI::getInstance()->executarSql("UPDATE item_sessao_julgamento set id_sessao_bloco=$valor where id_sessao_julgamento=$numIdSessao and sta_item_sessao_julgamento='$chave'");
        }
      }
    }
    public function fix_parametros_modulo(){

      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO' WHERE NOME='ID_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_CERTIDAO_CANCELAMENTO_SESSAO' WHERE NOME='ID_SERIE_CERTIDAO_CANCELAMENTO_SESSAO'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_CERTIDAO_DISTRIBUICAO' WHERE NOME='ID_SERIE_CERTIDAO_DISTRIBUICAO'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_CERTIDAO_JULGAMENTO' WHERE NOME='ID_SERIE_CERTIDAO_JULGAMENTO'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_CERTIDAO_REDISTRIBUICAO' WHERE NOME='ID_SERIE_CERTIDAO_REDISTRIBUICAO'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_ATA_SESSAO' WHERE NOME='ID_SERIE_JULGAMENTO_ATA'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_PAUTA_SESSAO' WHERE NOME='ID_SERIE_JULGAMENTO_PAUTA'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_EDITAL_CANCELAMENTO_SESSAO' WHERE NOME='ID_SERIE_EDITAL_CANCELAMENTO_SESSAO'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_ACORDAO' WHERE NOME='ID_SERIE_ACORDAO'");
      BancoSEI::getInstance()->executarSql("UPDATE infra_parametro set nome='MD_JULGAR_ID_SERIE_DISPONIBILIZAVEIS' WHERE NOME='ID_SERIE_JULGAMENTO_DISPONIBILIZAVEIS'");
    }


    public function identificarVersao(): ?string
    {
      try {
        $objInfraMetaBD = new InfraMetaBD(BancoSEI::getInstance());

        BancoSEI::getInstance()->abrirConexao();
        $arrTabelas = array();
        $ret = $objInfraMetaBD->obterTabelas();
        BancoSEI::getInstance()->fecharConexao();
        foreach ($ret as $tabela) {
          $arrTabelas[$tabela['table_name']] = 1;
        }
        if (isset($arrTabelas['tipo_sessao'])) {
          return '2.0.0';
        }
        if (isset($arrTabelas['rel_autuacao_tipo_materia'])) {
          return '1.4.0';
        }
        if (isset($arrTabelas['bloqueio_item_sess_unidade'])) {
          return '1.3.0';
        }
        if (isset($arrTabelas['rel_motivo_distr_colegiado'])) {
          return '1.2.0';
        }
        if (isset($arrTabelas['autuacao'])) {
          return '1.1.0';
        }
        if (isset($arrTabelas['algoritmo'])) {
          return '1.0.0';
        }

        return null;

      } catch (Exception $e) {
        throw new InfraException('Erro identificando vers�o do m�dulo SEI Julgar.', $e);
      }


    }
  }


  if (!isset($_SESSION)) {
    session_start();
  }

  SessaoSEI::getInstance(false);
  BancoSEI::getInstance()->setBolScript(true);

  $objMdJulgarVersaoSeiRN = new MdJulgarVersaoSeiRN();
  $objMdJulgarVersaoSeiRN->setStrNome('MODULO SEI JULGAR');
  $objMdJulgarVersaoSeiRN->setStrVersaoAtual('2.0.3');
  $objMdJulgarVersaoSeiRN->setStrParametroVersao('MD_JULGAR_VERSAO');
  $objMdJulgarVersaoSeiRN->setArrVersoes(array('1.0.*' => 'versao_1_0_0', '1.1.*' => 'versao_1_1_0', '1.2.*' => 'versao_1_2_0', '1.3.*' => 'versao_1_3_0', '1.4.*' => 'versao_1_4_0', '2.0.*' => 'versao_2_0_0'));
  $objMdJulgarVersaoSeiRN->setStrVersaoInfra('1.598.0');
  $objMdJulgarVersaoSeiRN->setBolMySql(true);
  $objMdJulgarVersaoSeiRN->setBolOracle(true);
  $objMdJulgarVersaoSeiRN->setBolSqlServer(true);
  $objMdJulgarVersaoSeiRN->setBolPostgreSql(true);
  $objMdJulgarVersaoSeiRN->setBolErroVersaoInexistente(false);
  $objMdJulgarVersaoSeiRN->setStrClasseModulo('MdJulgarIntegracao');

  $strVersaoInstalada = $objMdJulgarVersaoSeiRN->identificarVersao();

  if ($strVersaoInstalada != null) {
    $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
    $objInfraParametro->setValor('MD_JULGAR_VERSAO', $strVersaoInstalada);
  }

  if (InfraUtil::compararVersoes(SEI_VERSAO, '<', '4.0.0')) {
    $objMdJulgarVersaoSeiRN->processarErro('VERSAO DO SEI DEVE SER IGUAL OU SUPERIOR A 4.0.0');
  }

  $objMdJulgarVersaoSeiRN->atualizarVersao();

} catch (Exception $e) {
  echo(InfraException::inspecionar($e));
  try {
    LogSEI::getInstance()->gravar(InfraException::inspecionar($e));
  } catch (Exception $e) {
  }
  exit(1);
}
?>