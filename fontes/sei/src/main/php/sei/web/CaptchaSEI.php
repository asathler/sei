<?
/*
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 03/05/2021 - criado por MGA
 *
 */

require_once dirname(__FILE__).'/SEI.php';

class CaptchaSEI extends InfraCaptcha {

  private static $instance = null;

  public static function getInstance()
  {
    if (CaptchaSEI::$instance == null) {
      CaptchaSEI::$instance = new CaptchaSEI(BancoSEI::getInstance());
    }
    return CaptchaSEI::$instance;
  }

  public function configurarCaptcha($strIdentificacao){
    try {

      $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
      $tipoCaptcha = $objInfraParametro->getValor('SEI_TIPO_CAPTCHA', false);

      if ($tipoCaptcha == null) {
        $tipoCaptcha = CaptchaSEI::$TIPO_INFRA_V2;
      }

      switch ($tipoCaptcha) {
        case CaptchaSEI::$TIPO_INFRA:
          $this->configurarInfra($strIdentificacao,true);
          break;

        case CaptchaSEI::$TIPO_INFRA_V2:
          $this->configurarInfraV2($strIdentificacao);
          break;

        default:
          throw new InfraException('Tipo de Captcha inv�lido.');
      }

    }catch(Exception $e){
      throw new InfraException('Erro configurando captcha.', $e);
    }
  }
}
?>