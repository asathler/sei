<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento'));

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  $arrComandos = array();

  $bolEscolherEleicao = true;
  $bolEscolherMembro = false;
  $numIdEleicao = null;
  $numEscolhas = 0;
  $strAviso = '';
  $strCss = '';
  $strJs = '';
  $strHtml = '';
  $strItensSelEleicao = '';
  $strResultado = '';
  
  switch($_GET['acao']) {
    case 'eleicao_votar':

      $strTitulo = 'Escrut�nios Eletr�nicos';

      //recuperar sessao, distribuicao e colegiado
      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdUnidadeResponsavelColegiado();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTO->retNumIdUsuarioPresidente();
      $objSessaoJulgamentoDTO->retNumIdUnidadeResponsavelColegiado();
      $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());

      $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);

      $bolAdmin = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') && SessaoSEI::getInstance()->getNumIdUnidadeAtual()==$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado();
      $bolPresidente = (SessaoSEI::getInstance()->getNumIdUsuario() == $objSessaoJulgamentoDTO->getNumIdUsuarioPresidente());
      $bolVotante = false;

      //recuperar a unidade do membro que esta logado
      $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoDTO->retNumIdUnidade();
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());

      $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
      $objColegiadoComposicaoDTO = $objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);

      if ($objColegiadoComposicaoDTO != null){

        $objBloqueioItemSessUnidadeDTO = new BloqueioItemSessUnidadeDTO();
        $objBloqueioItemSessUnidadeDTO->retNumIdBloqueioItemSessUnidade();
        $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
        $objBloqueioItemSessUnidadeDTO->setNumIdUnidade($objColegiadoComposicaoDTO->getNumIdUnidade());
        $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);

        $objBloqueioItemSessUnidadeRN = new BloqueioItemSessUnidadeRN();
        if ($objBloqueioItemSessUnidadeRN->consultar($objBloqueioItemSessUnidadeDTO)==null){

          $objPresencaSessaoDTO = new PresencaSessaoDTO();
          $objPresencaSessaoDTO->setNumMaxRegistrosRetorno(1);
          $objPresencaSessaoDTO->retNumIdPresencaSessao();
          $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
          $objPresencaSessaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
          $objPresencaSessaoDTO->setDthSaida(null);

          $objPresencaSessaoRN = new PresencaSessaoRN();
          if ($objPresencaSessaoRN->consultar($objPresencaSessaoDTO)!=null){
            $bolVotante = true;
          }
        }
      }


      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->retNumIdEleicao();
      $objEleicaoDTO->retStrIdentificacao();
      $objEleicaoDTO->retStrStaSituacao();
      $objEleicaoDTO->retNumQuantidade();
      $objEleicaoDTO->retStrSinSecreta();
      $objEleicaoDTO->setStrStaSituacao(array(EleicaoRN::$SE_LIBERADA,EleicaoRN::$SE_CONCLUIDA,EleicaoRN::$SE_FINALIZADA), InfraDTO::$OPER_IN);
      $objEleicaoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      $objEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

      $objEleicaoRN = new EleicaoRN();
      $arrObjEleicaoDTO = $objEleicaoRN->listar($objEleicaoDTO);

      if (count($arrObjEleicaoDTO)==0){

          $strAviso = 'Nenhum escrut�nio dispon�vel.';

      }else {

        $objEleicaoDTO = null;
        if (!isset($_POST['selEleicao']) && count($arrObjEleicaoDTO) == 1) {
          $objEleicaoDTO = $arrObjEleicaoDTO[0];
        } else if ($_POST['selEleicao'] != '' && $_POST['hdnAtualizar']=='') {
          foreach ($arrObjEleicaoDTO as $dto) {
            if ($dto->getNumIdEleicao() == $_POST['selEleicao']) {
              $objEleicaoDTO = $dto;
              break;
            }
          }
        }

        if ($objEleicaoDTO != null) {

          $numIdEleicao = $objEleicaoDTO->getNumIdEleicao();
          $numEscolhas = $objEleicaoDTO->getNumQuantidade();

          if ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_LIBERADA) {
            $strTitulo = 'Votar no Escrut�nio Eletr�nico';
          } else if ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_CONCLUIDA) {
            $strTitulo = 'Escrut�nio Eletr�nico Conclu�do';
          } else if ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_FINALIZADA) {
            $strTitulo = 'Extrato de Vota��o';
          }

          $objVotoEleicaoDTO = new VotoEleicaoDTO();
          $objVotoEleicaoDTO->retNumIdVotoEleicao();
          $objVotoEleicaoDTO->retDthVotoEleicao();
          $objVotoEleicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
          $objVotoEleicaoDTO->setNumIdEleicao($numIdEleicao);

          $objVotoEleicaoRN = new VotoEleicaoRN();
          $objVotoEleicaoDTO = $objVotoEleicaoRN->consultar($objVotoEleicaoDTO);


          if ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_LIBERADA) {

            if ($objVotoEleicaoDTO == null) {

              if (isset($_POST['sbmVotar'])) {
                try {

                  $arrObjOpcaoEleicaoDTO = array();
                  for ($i = 1; $i < ($numEscolhas + 1); $i++) {
                    $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
                    $objOpcaoEleicaoDTO->setNumIdOpcaoEleicao($_POST['selOpcao'.$i]);
                    $objOpcaoEleicaoDTO->setNumOrdem($i);
                    $arrObjOpcaoEleicaoDTO[] = $objOpcaoEleicaoDTO;
                  }

                  $objEleicaoDTOVotacao = new EleicaoDTO();
                  $objEleicaoDTOVotacao->setNumIdEleicao($_POST['selEleicao']);
                  $objEleicaoDTOVotacao->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
                  $objEleicaoDTOVotacao->setArrObjOpcaoEleicaoDTO($arrObjOpcaoEleicaoDTO);

                  $objEleicaoRN = new EleicaoRN();
                  $objVotoEleicaoDTO = $objEleicaoRN->votar($objEleicaoDTOVotacao);

                } catch (Exception $e) {
                  PaginaSEI::getInstance()->processarExcecao($e);
                }
              }
            }

            if ($objVotoEleicaoDTO != null) {

              $strAviso = 'Voto lan�ado em '.substr($objVotoEleicaoDTO->getDthVotoEleicao(), 0, 16).'.';

            } else {

              if ($bolVotante) {

                if ($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){

                  $strAviso = 'N�o � poss�vel lan�ar o voto neste momento porque a sess�o n�o est� aberta.';

                }else {
                  $arrComandos[] = '<button type="submit" accesskey="V" name="sbmVotar" value="Votar" class="infraButton">Votar</button>';

                  for ($i = 1; $i < ($numEscolhas + 1); $i++) {

                    $strCss .= '#lblOpcao'.$i.' {position:absolute;left:0%;top:0%;width:93%;}'."\n";
                    $strCss .= '#selOpcao'.$i.' {position:absolute;left:0%;top:38%;width:93%;}'."\n\n";

                    $strJs .= 'if (!infraSelectSelecionado(\'selOpcao'.$i.'\')){'."\n";

                    if ($numEscolhas == 1) {
                      $strJs .= '  alert(\'Selecione uma op��o.\');'."\n";
                    } else {
                      $strJs .= '  alert(\'Selecione Op��o '.$i.'.\');'."\n";
                    }

                    $strJs .= '  document.getElementById(\'selOpcao'.$i.'\').focus();'."\n";
                    $strJs .= '  return false;'."\n";
                    $strJs .= '}'."\n\n";

                    $strHtml .= '<div class="infraAreaDados" style="height:5em">'."\n";
                    $strHtml .= '<label id="lblOpcao'.$i.'" for="selOpcao'.$i.'" class="infraLabelObrigatorio">Op��o'.($numEscolhas == 1 ? '' : ' '.$i).':</label>'."\n";
                    $strHtml .= '<select id="selOpcao'.$i.'" name="selOpcao'.$i.'" class="infraSelect">'."\n";
                    $strHtml .= OpcaoEleicaoINT::montarSelectIdentificacao('null', '&nbsp;', $_POST['selOpcao'.$i], $numIdEleicao);
                    $strHtml .= '</select>'."\n";
                    $strHtml .= '</div>'."\n";
                  }

                  $strJs .= "\n";
                  $strJs .= 'if (!confirm("Confirma a vota��o no Escrut�nio Eletr�nico?\n\nAp�s a confirma��o n�o ser� poss�vel alterar o voto lan�ado.")) {'."\n";
                  $strJs .= '  return false;'."\n";
                  $strJs .= '}'."\n\n";
                }
              } else {
                $strTitulo = 'Escrut�nio Eletr�nico Liberado';
                $strAviso = 'Colhendo votos dos membros do colegiado.';
              }
            }

          } else if ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_CONCLUIDA) {

            if (!$bolAdmin && !$bolPresidente) {

              $strAviso = 'Aguardando finaliza��o.';

            } else {

              $arrComandos[] = '<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="imprimir();" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir</button>';

              /*
              if ($bolPresidente) {
                $strTitulo .= ' (Presidente)';
              }else {
                $strTitulo .= ' (Secretaria)';
              }
              */

              $numIdVotoEleicao = null;
              if (isset($_POST['selUsuario']) && $_POST['selUsuario'] != '') {
                $numIdVotoEleicao = $_POST['selUsuario'];
              }

              $objVotoOpcaoEleicaoDTO = new VotoOpcaoEleicaoDTO();
              $objVotoOpcaoEleicaoDTO->retNumIdOpcaoEleicao();
              $objVotoOpcaoEleicaoDTO->retStrIdentificacaoOpcaoEleicao();
              $objVotoOpcaoEleicaoDTO->retNumIdUsuarioVotoEleicao();
              $objVotoOpcaoEleicaoDTO->retStrSiglaUsuario();
              $objVotoOpcaoEleicaoDTO->retStrNomeUsuario();
              $objVotoOpcaoEleicaoDTO->setNumIdEleicaoOpcaoEleicao($numIdEleicao);

              if ($numIdVotoEleicao != null) {
                $objVotoOpcaoEleicaoDTO->setNumTipoFkVotoEleicao(InfraDTO::$TIPO_FK_OBRIGATORIA);
                $objVotoOpcaoEleicaoDTO->setNumIdVotoEleicao($numIdVotoEleicao);
              }

              $objVotoOpcaoEleicaoDTO->setOrdNumOrdemVotoEleicao(InfraDTO::$TIPO_ORDENACAO_ASC);
              $objVotoOpcaoEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

              $objVotoOpcaoEleicaoRN = new VotoOpcaoEleicaoRN();
              $arrObjVotoOpcaoEleicaoDTO = $objVotoOpcaoEleicaoRN->listar($objVotoOpcaoEleicaoDTO);

              $numRegistros = count($arrObjVotoOpcaoEleicaoDTO);

              if ($numRegistros) {

                $objVotoEleicaoDTO = new VotoEleicaoDTO();
                $objVotoEleicaoDTO->setNumIdEleicao($numIdEleicao);
                if ($numIdVotoEleicao != null) {
                  $objVotoEleicaoDTO->setNumIdVotoEleicao($numIdVotoEleicao);
                }

                $objVotoEleicaoRN = new VotoEleicaoRN();
                $numVotantes = $objVotoEleicaoRN->contar($objVotoEleicaoDTO);

                PaginaSEI::getInstance()->getThCheck();

                $strResultado = '';
                $strResultado .= '<table width="99%" id="tblEleicao" class="infraTable'.($numEscolhas > 1 ?' tabelaEleicao':'').'" summary="Tabela de votos do Escrut�nio Eletr�nico.">'."\n";
                $strResultado .= '<caption class="infraCaption">Votos ('.$numVotantes.' '.($numVotantes == 1 ? 'votante' : 'votantes').'):</caption>';

                $strCssTr = '';

                $strResultado .= '<tr>';
                $strResultado .= '<th align="center" width="40%" class="infraTh">Membro</th>'."\n";
                if ($numEscolhas > 1) {
                  $strResultado .= '<th align="center" width="10%" class="infraTh">Ordem</th>'."\n";
                }
                $strResultado .= '<th align="center" class="infraTh">Op��o</th>'."\n";
                $strResultado .= '</tr>';

                $arrObjVotoOpcaoEleicaoDTO = InfraArray::indexarArrInfraDTO($arrObjVotoOpcaoEleicaoDTO, 'IdUsuarioVotoEleicao', true);

                foreach ($arrObjVotoOpcaoEleicaoDTO as $numIdUsuarioVotoEleicao => $arrObjVotoOpcaoEleicaoDTOUsuario) {

                  $strCssTr = ($strCssTr == 'infraTrClara') ? 'infraTrEscura' : 'infraTrClara';
                  $strResultado .= '<tr name="trVoto'.$numIdUsuarioVotoEleicao.'" class="'.$strCssTr.($numEscolhas > 1 ?' linhaEleicao':'').'">'."\n";
                  $strResultado .= '<td rowspan="'.$numEscolhas.'">'.$arrObjVotoOpcaoEleicaoDTOUsuario[0]->getStrNomeUsuario().'</td>'."\n";

                  $n = 1;
                  foreach ($arrObjVotoOpcaoEleicaoDTOUsuario as $objVotoOpcaoEleicaoDTO) {
                    if ($numEscolhas > 1) {
                      if ($n > 1) {
                        $strResultado .= '<tr name="trVoto'.$numIdUsuarioVotoEleicao.'" class="'.$strCssTr.'">'."\n";
                      }
                      $strResultado .= '<td align="center">'.($n++).'</td>';
                    }
                    $strResultado .= '<td>'.$objVotoOpcaoEleicaoDTO->getStrIdentificacaoOpcaoEleicao().'</td>'."\n";
                    $strResultado .= '</tr>'."\n";
                  }
                }

                $strResultado .= '</table>'."\n";
              }

              $bolEscolherMembro = true;
              $strItensSelUsuario = VotoEleicaoINT::montarSelectVotantes('', 'Todos', $_POST['selUsuario'], $numIdEleicao);
            }

          } else if ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_FINALIZADA) {


            $arrComandos[] = '<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="imprimir();" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir</button>';

            $objVotoOpcaoEleicaoDTO = new VotoOpcaoEleicaoDTO();
            $objVotoOpcaoEleicaoDTO->retNumIdOpcaoEleicao();
            $objVotoOpcaoEleicaoDTO->retStrIdentificacaoOpcaoEleicao();
            $objVotoOpcaoEleicaoDTO->retNumIdUsuarioVotoEleicao();
            $objVotoOpcaoEleicaoDTO->retStrSiglaUsuario();
            $objVotoOpcaoEleicaoDTO->retStrNomeUsuario();
            $objVotoOpcaoEleicaoDTO->setNumIdEleicaoOpcaoEleicao($numIdEleicao);
            $objVotoOpcaoEleicaoDTO->setOrdNumOrdemVotoEleicao(InfraDTO::$TIPO_ORDENACAO_ASC);
            $objVotoOpcaoEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

            $objVotoOpcaoEleicaoRN = new VotoOpcaoEleicaoRN();
            $arrObjVotoOpcaoEleicaoDTO = $objVotoOpcaoEleicaoRN->listar($objVotoOpcaoEleicaoDTO);

            $numRegistros = count($arrObjVotoOpcaoEleicaoDTO);

            if ($numRegistros) {

              $objVotoEleicaoDTO = new VotoEleicaoDTO();
              $objVotoEleicaoDTO->setNumIdEleicao($numIdEleicao);

              $objVotoEleicaoRN = new VotoEleicaoRN();
              $numVotantes = $objVotoEleicaoRN->contar($objVotoEleicaoDTO);

              $strResultado = '';
              $strResultado .= '<table width="99%" id="tblExtrato" class="infraTable" summary="Tabela de resultado do Escrut�nio Eletr�nico.">'."\n";
              $strResultado .= '<caption class="infraCaption">Resultado ('.$numVotantes.' '.($numVotantes == 1 ? 'votante' : 'votantes').'):</caption>';
              $strResultado .= '<tr>';
              $strResultado .= '<th align="center" width="15%" class="infraTh">Votos</th>'."\n";
              $strResultado .= '<th class="infraTh" width="40%">Op��o</th>'."\n";

              if ($objEleicaoDTO->getStrSinSecreta() == 'N') {
                $strResultado .= '<th class="infraTh">Membros</th>'."\n";
              }

              $strResultado .= '</tr>'."\n";

              PaginaSEI::getInstance()->getThCheck();

              $arrObjVotoOpcaoEleicaoDTOExtrato = InfraArray::indexarArrInfraDTO($arrObjVotoOpcaoEleicaoDTO, 'IdOpcaoEleicao', true);

              $arrOrdenacao = array();
              foreach ($arrObjVotoOpcaoEleicaoDTOExtrato as $numIdOpcaoEleicao => $arrVotos) {
                $arrOrdenacao[$numIdOpcaoEleicao] = count($arrVotos);
              }
              arsort($arrOrdenacao);

              $strCssTr = '<tr class="infraTrEscura">';
              $n = 1;
              foreach ($arrOrdenacao as $numIdOpcaoEleicao => $numVotos) {

                $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
                $strResultado .= $strCssTr."\n";

                $strPercentual = round(($numVotos / $numVotantes) * 100, 1);
                if (strpos($strPercentual, '.') === false) {
                  $strPercentual .= '.0';
                }
                $strResultado .= '<td align="center">'.$numVotos.' ('.$strPercentual.'%)</td>'."\n";

                $strResultado .= '<td>'.$arrObjVotoOpcaoEleicaoDTOExtrato[$numIdOpcaoEleicao][0]->getStrIdentificacaoOpcaoEleicao().'</td>'."\n";

                if ($objEleicaoDTO->getStrSinSecreta() == 'N') {
                  $strResultado .= '<td>'."\n";
                  foreach ($arrObjVotoOpcaoEleicaoDTOExtrato[$numIdOpcaoEleicao] as $objVotoOpcaoEleicaoDTO) {
                    $strResultado .= '<li>'.$objVotoOpcaoEleicaoDTO->getStrNomeUsuario().'</li>'."\n";
                  }
                  $strResultado .= '</td>'."\n";
                }
                $strResultado .= '</tr>'."\n";
              }
              $strResultado .= '</table>'."\n";

              if ($objEleicaoDTO->getStrSinSecreta() == 'N') {

                $strResultado .= '<br><br>';

                $strResultado .= '<table width="99%" id="tblEleicao" class="infraTable'.($numEscolhas > 1 ?' tabelaEleicao':'').'" cellspacing="0" summary="Tabela de votos do Escrut�nio Eletr�nico.">'."\n";
                $strResultado .= '<caption class="infraCaption">Votos ('.$numVotantes.' '.($numVotantes == 1 ? 'votante' : 'votantes').'):</caption>';

                $strCssTr = '';

                $strResultado .= '<tr>';
                $strResultado .= '<th align="center" width="40%" class="infraTh">Membro</th>'."\n";
                if ($numEscolhas > 1) {
                  $strResultado .= '<th align="center" width="10%" class="infraTh">Ordem</th>'."\n";
                }
                $strResultado .= '<th align="center" class="infraTh">Op��o</th>'."\n";
                $strResultado .= '</tr>';

                $arrObjVotoOpcaoEleicaoDTO = InfraArray::indexarArrInfraDTO($arrObjVotoOpcaoEleicaoDTO, 'IdUsuarioVotoEleicao', true);

                foreach ($arrObjVotoOpcaoEleicaoDTO as $numIdUsuarioVotoEleicao => $arrObjVotoOpcaoEleicaoDTOUsuario) {

                  $strCssTr = ($strCssTr == 'infraTrClara') ? 'infraTrEscura' : 'infraTrClara';

                  $strResultado .= '<tr name="trVoto'.$numIdUsuarioVotoEleicao.'" class="'.$strCssTr.($numEscolhas > 1 ?' linhaEleicao':'').'">'."\n";
                  $strResultado .= '<td rowspan="'.$numEscolhas.'">'.$arrObjVotoOpcaoEleicaoDTOUsuario[0]->getStrNomeUsuario().'</td>'."\n";

                  $n = 1;
                  foreach ($arrObjVotoOpcaoEleicaoDTOUsuario as $objVotoOpcaoEleicaoDTO) {
                    if ($numEscolhas > 1) {
                      if ($n > 1) {
                        $strResultado .= '<tr name="trVoto'.$numIdUsuarioVotoEleicao.'" class="'.$strCssTr.'">'."\n";
                      }
                      $strResultado .= '<td align="center">'.($n++).'</td>';
                    }
                    $strResultado .= '<td>'.$objVotoOpcaoEleicaoDTO->getStrIdentificacaoOpcaoEleicao().'</td>'."\n";
                    $strResultado .= '</tr>'."\n";
                  }
                }

                $strResultado .= '</table>'."\n";
              }
            }
          }
        }

        //$bolEscolherEleicao = true;

        $arrIdEleicoesComVoto = array();
        if ($bolVotante && count($arrObjEleicaoDTO)) {
          $objVotoEleicaoDTO = new VotoEleicaoDTO();
          $objVotoEleicaoDTO->retNumIdEleicao();
          $objVotoEleicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
          $objVotoEleicaoDTO->setNumIdEleicao(InfraArray::converterArrInfraDTO($arrObjEleicaoDTO, 'IdEleicao'), InfraDTO::$OPER_IN);

          $objVotoEleicaoRN = new VotoEleicaoRN();
          $arrIdEleicoesComVoto = InfraArray::converterArrInfraDTO($objVotoEleicaoRN->listar($objVotoEleicaoDTO),'IdEleicao');
        }

        //if ($bolVotante) {
          foreach ($arrObjEleicaoDTO as $objEleicaoDTO) {
            if ($objEleicaoDTO->getNumIdEleicao() != $numIdEleicao && $objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_LIBERADA && !in_array($objEleicaoDTO->getNumIdEleicao(), $arrIdEleicoesComVoto)) {
              $objEleicaoDTO->setStrIdentificacao($objEleicaoDTO->getStrIdentificacao().' (LIBERADO)');
            }
          }
        //}

        $strItensSelEleicao = InfraINT::montarSelectArrInfraDTO('null', '&nbsp;', $numIdEleicao, $arrObjEleicaoDTO, 'IdEleicao', 'Identificacao');

        //if ($bolVotante) {
          foreach ($arrObjEleicaoDTO as $objEleicaoDTO) {
            if ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_LIBERADA && !in_array($objEleicaoDTO->getNumIdEleicao(), $arrIdEleicoesComVoto)) {
              $strItensSelEleicao = str_replace(' value="'.$objEleicaoDTO->getNumIdEleicao().'"', ' value="'.$objEleicaoDTO->getNumIdEleicao().'" style="background-color:#81c784;"', $strItensSelEleicao);
            }
          }
        //}
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>

@media print {
  select {
    appearance: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    outline: none !important;
    border:0 !important;
  }

  #divInfraAreaTelaD{
    display:block;
    min-height:100%;
    width: auto !important;
    height: auto !important;
    overflow: visible !important;
  }
}

li {list-style-position: inside;}

table.tabelaEleicao{
  border-spacing: 0;
  border-bottom: 1px solid #ccc;
}

tr.linhaEleicao td {border-top:1px solid #ccc;}



<? if ($bolEscolherEleicao){ ?>
#lblEleicao {position:absolute;left:0%;top:0%;width:93%;}
#selEleicao {position:absolute;left:0%;top:38%;width:93%;}
#imgAtualizar {position:absolute;left:94%;top:43%;}
<? } ?>

<? if ($bolEscolherMembro){ ?>
#lblUsuario {position:absolute;left:0%;top:0%;width:70%;}
#selUsuario {position:absolute;left:0%;top:38%;width:70%;}
<? } ?>

<?=$strCss?>

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  <? if ($bolEscolherEleicao){ ?>
  document.getElementById('selEleicao').focus();
  infraEfeitoTabelas();
  prepararTrs();
  <?}?>
}

function validarCadastro() {

  if (!infraSelectSelecionado('selEleicao')) {
    alert('Selecione um Escrut�nio Eletr�nico.');
    document.getElementById('selEleicao').focus();
    return false;
  }

  <? if ($bolEscolherMembro){ ?>
  if (!infraSelectSelecionado('selUsuario')) {
    alert('Selecione um Usu�rio.');
    document.getElementById('selUsuario').focus();
    return false;
  }
  <? } ?>

  <?=$strJs?>

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

  function prepararTrs(){

    var i;
    var tab = document.getElementById('tblEleicao');

    if (tab != null){

      var trs = tab.getElementsByTagName("tr");

      for(i=0;i < trs.length;i++){

        if (i) {

          trs[i].onmouseover = function () {
            var arrTr = document.getElementsByName(this.getAttribute('name'));
            for (var j = 0; j<arrTr.length; j++) {
              $(arrTr[j]).addClass("infraTrSelecionada");
            }
          };

          trs[i].onmouseout = function () {
            var arrTr = document.getElementsByName(this.getAttribute('name'));
            for (var j = 0; j<arrTr.length; j++) {
              $(arrTr[j]).removeClass("infraTrSelecionada");
            }
          };
        }
      }
    }
  }

  function imprimir() {
    var div=$('#divInfraBarraComandosSuperior').hide();
    var img=$('#imgAtualizar').hide();
    window.print();
    img.show();
    div.show();
  }

  function atualizar(){
    document.getElementById('hdnAtualizar').value = '1';
    document.getElementById('frmVotoEleicao').submit();
  }

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmVotoEleicao" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();

if ($bolEscolherEleicao) {
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblEleicao" for="selEleicao" accesskey="" class="infraLabelObrigatorio">Escrut�nio Eletr�nico:</label>
  <select id="selEleicao" name="selEleicao" onchange="this.form.submit()" class="infraSelect"
          tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelEleicao ?>
  </select>
  <img id="imgAtualizar" src="<?=MdJulgarIcone::ATUALIZAR_ELEICOES?>" onclick="atualizar()" title="Atualizar" alt="Atualizar" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
}

if ($strAviso!=''){
  PaginaSEI::getInstance()->abrirAreaDados();
?>
    <br>
  <label id="lblAviso" class="infraLabelObrigatorio"><?=$strAviso?></label>
<?
  PaginaSEI::getInstance()->fecharAreaDados();
}

if ($bolEscolherMembro) {
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblUsuario" for="selUsuario" class="infraLabelObrigatorio">Membro:</label>
  <select id="selUsuario" name="selUsuario" onchange="this.form.submit()" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelUsuario ?>
  </select>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
}

echo $strHtml;

if ($strResultado!=''){
  echo '<br>';
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
}

PaginaSEI::getInstance()->montarAreaDebug();
//PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
?>

<input id="hdnAtualizar" name="hdnAtualizar" type="hidden" value="" />

</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
