<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class JulgamentoParteINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdItemSessaoJulgamento=''){
    $objJulgamentoParteDTO = new JulgamentoParteDTO();
    $objJulgamentoParteDTO->retNumIdJulgamentoParte();
    $objJulgamentoParteDTO->retStrDescricao();

    if ($numIdItemSessaoJulgamento!==''){
      $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    }

    $objJulgamentoParteDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objJulgamentoParteRN = new JulgamentoParteRN();
    $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjJulgamentoParteDTO, 'IdJulgamentoParte', 'Descricao');
  }

  public static function montarTdVotos($arrObjVotoParteDTO,$numIdUsuarioRelator){
    if(InfraArray::contar($arrObjVotoParteDTO)==0){
      return 'Nenhum voto registrado.';
    }
    $arrObjVotoParteDTO=InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO,'IdUsuario');
    $arrVotosProvimento=array();
    $arrVotosAcompanhamento=array();
    $arrVotos[$numIdUsuarioRelator]=array();
    /** @var VotoParteDTO $objVotoParteDTO */
    foreach ($arrObjVotoParteDTO as $idUsuario=>$objVotoParteDTO) {
      if($objVotoParteDTO->getNumIdProvimento()!=null){
        $arrVotosProvimento[$idUsuario]=$objVotoParteDTO;
        continue;
      }
      $idAssociado=$objVotoParteDTO->getNumIdUsuarioVotoParte();
      if($idUsuario!=$numIdUsuarioRelator && $objVotoParteDTO->getStrStaVotoParte()==VotoParteRN::$STA_ACOMPANHA){
        if($idAssociado==null){
          $idAssociado=$numIdUsuarioRelator;
        }
        if(!isset($arrVotosAcompanhamento[$idAssociado])){
          $arrVotosAcompanhamento[$idAssociado]=array();
        }
        $arrVotosAcompanhamento[$idAssociado][]=$objVotoParteDTO;
      }

    }
    $strVotos='';
    $objVotoParteDTO=$arrVotosProvimento[$numIdUsuarioRelator];
    $strVotos.='Relator ('.$objVotoParteDTO->getStrConteudoProvimento();
    $complemento=$objVotoParteDTO->getStrComplemento();
    if($complemento!=null){
      $strVotos.=' '.$complemento;
    }
    $strVotos.=')';
    if(isset($arrVotosAcompanhamento[$numIdUsuarioRelator])){
      if(InfraArray::contar($arrVotosAcompanhamento[$numIdUsuarioRelator])==1){
        $strVotos.=' Acompanha: ';
      } else {
        $strVotos.=' Acompanham: ';
      }
      $strConcat='';
      foreach ($arrVotosAcompanhamento[$numIdUsuarioRelator] as $objVotoParteDTO2) {
        $strVotos.=$strConcat.$objVotoParteDTO2->getStrNomeUsuario();
        $strConcat=', ';
      }
    }
    unset($arrVotosProvimento[$numIdUsuarioRelator]);
    foreach ($arrVotosProvimento as $objVotoParteDTO) {
      $strVotos.='; ';
      $strVotos.=$objVotoParteDTO->getStrNomeUsuario().' ('.$objVotoParteDTO->getStrConteudoProvimento();
      $complemento=$objVotoParteDTO->getStrComplemento();
      if($complemento!=null){
        $strVotos.=' '.$complemento;
      }
      $strVotos.=')';
      if(isset($arrVotosAcompanhamento[$objVotoParteDTO->getNumIdUsuario()])){
        if(InfraArray::contar($arrVotosAcompanhamento[$objVotoParteDTO->getNumIdUsuario()])==1){
          $strVotos.=' Acompanha: ';
        } else {
          $strVotos.=' Acompanham: ';
        }
        $strConcat='';
        foreach ($arrVotosAcompanhamento[$objVotoParteDTO->getNumIdUsuario()] as $objVotoParteDTO2) {
          $strVotos.=$strConcat.$objVotoParteDTO2->getStrNomeUsuario();
          $strConcat=', ';
        }
      }

    }

    return $strVotos;

  }
}
?>