<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ImpedimentoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdDistribuicao(ImpedimentoDTO $objImpedimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objImpedimentoDTO->getNumIdDistribuicao())){
      $objInfraException->adicionarValidacao('Distribui��o n�o informada.');
    }
  }

  private function validarNumIdUsuario(ImpedimentoDTO $objImpedimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objImpedimentoDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Membro do Colegiado n�o informado.');
    }
  }

  private function validarNumIdMotivoImpedimento(ImpedimentoDTO $objImpedimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objImpedimentoDTO->getNumIdMotivoImpedimento())){
      $objInfraException->adicionarValidacao('Motivo de Impedimento n�o informado.');
    }else{
      $objMotivoDistribuicaoDTO=new MotivoDistribuicaoDTO();
      $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($objImpedimentoDTO->getNumIdMotivoImpedimento());
      $objMotivoDistribuicaoDTO->setStrStaTipo(MotivoDistribuicaoRN::$TIPO_IMPEDIMENTO);
      $objMotivoDistribuicaoRN=new MotivoDistribuicaoRN();
      if ($objMotivoDistribuicaoRN->contar($objMotivoDistribuicaoDTO)==0) {
        $objInfraException->adicionarValidacao('Motivo de Impedimento n�o encontrado.');
      }
    }
  }

  protected function cadastrarControlado(ImpedimentoDTO $objImpedimentoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('impedimento_cadastrar',__METHOD__,$objImpedimentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdDistribuicao($objImpedimentoDTO, $objInfraException);
      $this->validarNumIdUsuario($objImpedimentoDTO, $objInfraException);
      $this->validarNumIdMotivoImpedimento($objImpedimentoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objImpedimentoBD = new ImpedimentoBD($this->getObjInfraIBanco());
      $ret = $objImpedimentoBD->cadastrar($objImpedimentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Impedimento.',$e);
    }
  }

  protected function alterarControlado(ImpedimentoDTO $objImpedimentoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('impedimento_alterar',__METHOD__,$objImpedimentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objImpedimentoDTO->isSetNumIdDistribuicao()){
        $this->validarNumIdDistribuicao($objImpedimentoDTO, $objInfraException);
      }
      if ($objImpedimentoDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objImpedimentoDTO, $objInfraException);
      }
      if ($objImpedimentoDTO->isSetNumIdMotivoImpedimento()){
        $this->validarNumIdMotivoImpedimento($objImpedimentoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objImpedimentoBD = new ImpedimentoBD($this->getObjInfraIBanco());
      $objImpedimentoBD->alterar($objImpedimentoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Impedimento.',$e);
    }
  }

  protected function excluirControlado($arrObjImpedimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('impedimento_excluir',__METHOD__,$arrObjImpedimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objImpedimentoBD = new ImpedimentoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjImpedimentoDTO);$i++){
        $objImpedimentoBD->excluir($arrObjImpedimentoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Impedimento.',$e);
    }
  }

  protected function consultarConectado(ImpedimentoDTO $objImpedimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('impedimento_consultar',__METHOD__,$objImpedimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objImpedimentoBD = new ImpedimentoBD($this->getObjInfraIBanco());
      $ret = $objImpedimentoBD->consultar($objImpedimentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Impedimento.',$e);
    }
  }

  protected function listarConectado(ImpedimentoDTO $objImpedimentoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('impedimento_listar',__METHOD__,$objImpedimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objImpedimentoBD = new ImpedimentoBD($this->getObjInfraIBanco());
      $ret = $objImpedimentoBD->listar($objImpedimentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Impedimentos.',$e);
    }
  }

  protected function contarConectado(ImpedimentoDTO $objImpedimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('impedimento_listar',__METHOD__,$objImpedimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objImpedimentoBD = new ImpedimentoBD($this->getObjInfraIBanco());
      $ret = $objImpedimentoBD->contar($objImpedimentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Impedimentos.',$e);
    }
  }
}
?>