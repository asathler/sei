<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 14/10/2014 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.12.0
 */

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->salvarCamposPost(array('selColegiado','txtDataFinal','txtDataInicial','selTipoProcedimento','chkSinResumo'));

  switch($_GET['acao']){

    case 'relatorio_distribuicao':
      $strTitulo = 'Relat�rio de Distribui��es';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


  $objDistribuicaoRN=new DistribuicaoRN();
  $objDistribuicaoDTO=new DistribuicaoDTO();

  $arrComandos = array();

  $idColegiado = PaginaSEI::getInstance()->recuperarCampo('selColegiado');
  $strDataFinal = PaginaSEI::getInstance()->recuperarCampo('txtDataFinal');
  $strDataInicial = PaginaSEI::getInstance()->recuperarCampo('txtDataInicial');
  $numIdTipoProcedimento = PaginaSEI::getInstance()->recuperarCampo('selTipoProcedimento', 'null');
  $strSinResumo = PaginaSEI::getInstance()->getCheckbox(PaginaSEI::getInstance()->recuperarCampo('chkSinResumo'));

  $bolGerarPlanilha=isset($_POST['sbmGerar']);

  if ($idColegiado=='null') {
    $idColegiado=null;
  }

  $arrObjEstadoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
  $arrObjEstadoDistribuicaoDTO=InfraArray::indexarArrInfraDTO($arrObjEstadoDistribuicaoDTO,'StaDistribuicao');


  $arrComandos[] = '<button type="submit" accesskey="P" id="sbmPesquisar" name="sbmPesquisar" value="Pesquisar" class="infraButton"><span class="infraTeclaAtalho">P</span>esquisar Distribui��es</button>';
  $arrComandos[] = '<button type="button" accesskey="L" id="btnLimpar" name="btnLimpar" onclick="limpar();" value="Limpar" class="infraButton"><span class="infraTeclaAtalho">L</span>impar Crit�rios</button>';
  $arrComandos[] = '<button type="submit" accesskey="G" name="sbmGerar" value="sbmGerar" class="infraButton"><span class="infraTeclaAtalho">G</span>erar Planilha</button>';

  if(!InfraString::isBolVazia($strDataInicial)) {
    $objDistribuicaoDTO->adicionarCriterio(array('Distribuicao'),array(InfraDTO::$OPER_MAIOR_IGUAL),array($strDataInicial),null);
  }
  if(!InfraString::isBolVazia($strDataFinal)) {
    $objDistribuicaoDTO->adicionarCriterio(array('Distribuicao'),array(InfraDTO::$OPER_MENOR_IGUAL),array($strDataFinal.' 23:59:59'),null);
  }
  if($numIdTipoProcedimento!='null'){
    $objDistribuicaoDTO->setNumIdTipoProcedimento($numIdTipoProcedimento);
  }
  $objDistribuicaoDTO->retNumIdUnidadeRelator();
  $objDistribuicaoDTO->setNumIdUnidadeResponsavelColegiado(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
  if ($idColegiado!=null){
    $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($idColegiado);
  }
//  $objDistribuicaoDTO->retStrSiglaUnidadeRelator();
//  $objDistribuicaoDTO->retStrDescricaoUnidadeRelator();
  $objDistribuicaoDTO->retStrStaDistribuicao();
//  $objDistribuicaoDTO->retNumIdUnidadeRelator();
  $objDistribuicaoDTO->retNumIdDistribuicao();
  $objDistribuicaoDTO->retStrNomeColegiado();
  $objDistribuicaoDTO->retStrSiglaColegiado();
  $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
  $objDistribuicaoDTO->retStrStaNivelAcessoGlobal();
  $objDistribuicaoDTO->retDblIdProcedimento();
  $objDistribuicaoDTO->retStrNomeTipoProcedimento();
  $objDistribuicaoDTO->retStrProtocoloFormatado();
  $objDistribuicaoDTO->retDthSituacaoAtual();
  $objDistribuicaoDTO->retDthDistribuicao();
  $objDistribuicaoDTO->retStrNomeUsuarioDistribuicao();
  $objDistribuicaoDTO->retStrNomeUsuarioRelator();
  $objDistribuicaoDTO->retStrStaUltimo();


  $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
  $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();

  if($idColegiado!=null){
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($idColegiado);
  }
  $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
  $objColegiadoComposicaoDTO->retNumIdUnidade();
  $objColegiadoComposicaoDTO->retNumIdUsuario();
  $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
  $objColegiadoComposicaoDTO->setDistinct(true);
  $objColegiadoComposicaoDTO->setOrdNumIdColegiadoColegiadoVersao(InfraDTO::$TIPO_ORDENACAO_ASC);

  $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
  $arrColegiados=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdColegiadoColegiadoVersao',true);
  foreach ($arrColegiados as $id=>$arrComposicao) {
    $arrColegiados[$id]=InfraArray::converterArrInfraDTO($arrComposicao,'IdUsuario','IdUnidade');
  }

  if($bolGerarPlanilha || $strSinResumo=='N') {
    $numRegistros = 0;
    $strResultado = '';
    $strSumarioTabela = 'Tabela de Distribui��es.';
    $strCaptionTabela = 'Distribui��es';
    $strResultado .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";


    $objDistribuicaoRN = new DistribuicaoRN();

    if(!$bolGerarPlanilha){
      PaginaSEI::getInstance()->prepararOrdenacao($objDistribuicaoDTO, 'Distribuicao', InfraDTO::$TIPO_ORDENACAO_DESC);
      PaginaSEI::getInstance()->prepararPaginacao($objDistribuicaoDTO);
    }
    $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);

    $arrIdProtocolosPermitidos = array();
    $arrIdProtocolosSigilosos = array();
    $arrIdDistribuicaoVista=[];
    $arrIdDistribuicao=[];

    /** @var DistribuicaoDTO $objDistribuicaoDTOBanco */
    foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTOBanco) {
      if ($objDistribuicaoDTOBanco->getStrStaNivelAcessoGlobal() === ProtocoloRN::$NA_SIGILOSO) {
        $arrIdProtocolosSigilosos[] = $objDistribuicaoDTOBanco->getDblIdProcedimento();
      } else {
        $arrIdProtocolosPermitidos[]=$objDistribuicaoDTOBanco->getDblIdProcedimento();
      }
      $numIdDistribuicao=$objDistribuicaoDTOBanco->getNumIdDistribuicao();
      if($objDistribuicaoDTOBanco->getStrStaDistribuicao() == DistribuicaoRN::$STA_PEDIDO_VISTA){
        $arrIdDistribuicaoVista[]=$numIdDistribuicao;
      }
    }
    if(count($arrIdDistribuicaoVista)){
      $objPedidoVistaDTO=new PedidoVistaDTO();
      $objPedidoVistaDTO->setNumIdDistribuicao($arrIdDistribuicaoVista,InfraDTO::$OPER_IN);
      $objPedidoVistaDTO->retNumIdDistribuicao();
      $objPedidoVistaDTO->retStrNomeUsuario();
      $objPedidoVistaRN=new PedidoVistaRN();
      $arrObjPedidoVistaDTO=$objPedidoVistaRN->listar($objPedidoVistaDTO);
      $arrObjPedidoVistaDTO=InfraArray::indexarArrInfraDTO($arrObjPedidoVistaDTO,'IdDistribuicao');
    }
    if (count($arrIdProtocolosSigilosos)) {
      $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
      $objPesquisaProtocoloDTO->setDblIdProtocolo($arrIdProtocolosSigilosos);
      $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_PROCEDIMENTOS);
      $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_AUTORIZADO);
      $objProtocoloRN = new ProtocoloRN();
      $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);
      foreach ($arrObjProtocoloDTO as $objProtocoloDTO) {
        if ($objProtocoloDTO->getNumCodigoAcesso() > 0) {
          $arrIdProtocolosPermitidos[$objProtocoloDTO->getDblIdProtocolo()] = 1;
        }
      }
    }
    if(InfraArray::contar($arrIdProtocolosPermitidos)>0){
      $objParticipanteDTO = new ParticipanteDTO();
      $objParticipanteDTO->retDblIdProtocolo();
      $objParticipanteDTO->retStrNomeContato();
      $objParticipanteDTO->setDblIdProtocolo($arrIdProtocolosPermitidos,InfraDTO::$OPER_IN);
      $objParticipanteDTO->setStrStaParticipacao(ParticipanteRN::$TP_INTERESSADO);
      $objParticipanteDTO->setOrdNumSequencia(InfraDTO::$TIPO_ORDENACAO_ASC);

      $objParticipanteRN = new ParticipanteRN();
      $arrObjParticipanteDTO = $objParticipanteRN->listarRN0189($objParticipanteDTO);
      $arrObjParticipanteDTO=InfraArray::indexarArrInfraDTO($arrObjParticipanteDTO,'IdProtocolo',true);
      $objParteProcedimentoDTO=new ParteProcedimentoDTO();
      $objParteProcedimentoRN=new ParteProcedimentoRN();
      $objParteProcedimentoDTO->retTodos();
      $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
      $objParteProcedimentoDTO->retStrNomeContato();
      $objParteProcedimentoDTO->setDblIdProcedimento($arrIdProtocolosPermitidos,InfraDTO::$OPER_IN);
      $arrObjParteProcedimentoDTO=$objParteProcedimentoRN->listar($objParteProcedimentoDTO);
      $arrObjParteProcedimentoDTO=InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO,'IdProcedimento',true);
    }

    $arrObjProtocoloDTO = array();
    $baseDTO = new ProtocoloDTO();
    $baseDTO->setDblIdProtocolo(null);
    $baseDTO->setStrStaNivelAcessoGlobal(null);
    foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO2) {
      /** @var DistribuicaoDTO $objDistribuicaoDTO2 */
      $objProtocoloDTO = clone $baseDTO;
      $objProtocoloDTO->setDblIdProtocolo($objDistribuicaoDTO2->getDblIdProcedimento());
      $objProtocoloDTO->setStrStaNivelAcessoGlobal($objDistribuicaoDTO2->getStrStaNivelAcessoGlobal());
      $arrObjProtocoloDTO[] = $objProtocoloDTO;
    }
    $objAnotacaoRN = new AnotacaoRN();
    $objAnotacaoRN->complementar($arrObjProtocoloDTO);

    $arrObjProtocoloDTO = InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO, 'IdProtocolo');
    $numRegistros = InfraArray::contar($arrObjDistribuicaoDTO);
    if($bolGerarPlanilha) {
      $objPHPExcel = new PHPExcel();
      //
      //// Set document properties
      $objPHPExcel->getProperties()->setCreator("SEI")
          ->setLastModifiedBy("SEI")
          ->setTitle(utf8_encode("Relatorio de Distribui��es"));
      //
      //// Add some data
      $objPlanilhaAtiva=$objPHPExcel->setActiveSheetIndex(0);
      $objPlanilhaAtiva->setCellValue('A1', 'Colegiado');
      $objPlanilhaAtiva->setCellValue('B1', utf8_encode('Data da Distribui��o'));
      $objPlanilhaAtiva->setCellValue('C1', 'Processo');
      $objPlanilhaAtiva->setCellValue('D1', 'Tipo do Processo');
      $objPlanilhaAtiva->setCellValue('E1', 'Interessados');
      $objPlanilhaAtiva->setCellValue('F1', 'Relator');
      $objPlanilhaAtiva->setCellValue('G1', utf8_encode('Situa��o Atual'));
      $objPlanilhaAtiva->getColumnDimension('A')->setWidth(10);
      $objPlanilhaAtiva->getColumnDimension('B')->setWidth(20);
      $objPlanilhaAtiva->getColumnDimension('C')->setWidth(25);
      $objPlanilhaAtiva->getColumnDimension('D')->setWidth(46);
      $objPlanilhaAtiva->getColumnDimension('E')->setWidth(55);
      $objPlanilhaAtiva->getColumnDimension('F')->setWidth(35);
      $objPlanilhaAtiva->getColumnDimension('G')->setWidth(55);
      $arrEstiloCabecalho = [
          'font' => ['bold' => true],
          'fill' =>[
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => ['argb' => 'FFA0A0A0']],
          'borders' => ['bottom' => ['style' => PHPExcel_Style_Border::BORDER_THIN]],
          'alignment' => ['horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER]
          ];
      $objPlanilhaAtiva->getStyle('A1:G1')->applyFromArray($arrEstiloCabecalho);
      $arrEstiloSigiloso=[
          'fill' =>[
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => ['argb' => PHPExcel_Style_Color::COLOR_BLACK]],
          'font' => ['color'=>['argb'=>PHPExcel_Style_Color::COLOR_WHITE]]
      ];
      $arrEstiloInativo=[
          'fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => ['argb' => 'FFFFBFAA']]
      ];
      $arrEstiloEscuro=[
          'fill' =>['type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => ['argb' => 'FFD0D0D0']]
      ];

      $numLinhaAtual=1;
      foreach ($arrObjDistribuicaoDTO as $i => $objDistribuicaoDTOBanco) {
        $numLinhaAtual++;
        $objPlanilhaAtiva->getStyle('A'.$numLinhaAtual.':G'.$numLinhaAtual)->applyFromArray(['borders' => ['bottom' => ['style' => PHPExcel_Style_Border::BORDER_THIN]]]);
        if($numLinhaAtual%2==1){
          $objPlanilhaAtiva->getStyle('A'.$numLinhaAtual.':G'.$numLinhaAtual)->applyFromArray($arrEstiloEscuro);
        }
        /* @var $objDistribuicaoDTOBanco DistribuicaoDTO */
        $numIdColegiado = $objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao();
        $dblIdProcedimento = $objDistribuicaoDTOBanco->getDblIdProcedimento();
        if ($objDistribuicaoDTOBanco->getStrStaUltimo()==DistribuicaoRN::$SD_REDISTRIBUIDO || $objDistribuicaoDTOBanco->getStrStaDistribuicao()==DistribuicaoRN::$STA_CANCELADO) {
          $objPlanilhaAtiva->getStyle('A'.$numLinhaAtual.':G'.$numLinhaAtual)->applyFromArray($arrEstiloInativo);
        }
        $objPlanilhaAtiva->setCellValueByColumnAndRow(0,$numLinhaAtual,utf8_encode($objDistribuicaoDTOBanco->getStrSiglaColegiado()));
        $objPlanilhaAtiva->setCellValueByColumnAndRow(1,$numLinhaAtual,$objDistribuicaoDTOBanco->getDthDistribuicao());

        //processo
        if ($objDistribuicaoDTOBanco->getStrStaNivelAcessoGlobal()===ProtocoloRN::$NA_SIGILOSO) {
          $objPlanilhaAtiva->getStyleByColumnAndRow(2,$numLinhaAtual)->applyFromArray($arrEstiloSigiloso);
        }
        $strStaDistribuicao = $objDistribuicaoDTOBanco->getStrStaDistribuicao();
        $objPlanilhaAtiva->setCellValueByColumnAndRow(2,$numLinhaAtual,$objDistribuicaoDTOBanco->getStrProtocoloFormatado());
        $objPlanilhaAtiva->setCellValueByColumnAndRow(3,$numLinhaAtual,utf8_encode($objDistribuicaoDTOBanco->getStrNomeTipoProcedimento()));

        $strInteressados='';
        if (isset($arrObjParteProcedimentoDTO[$dblIdProcedimento])) {
          $j = InfraArray::contar($arrObjParteProcedimentoDTO[$dblIdProcedimento]);
          foreach ($arrObjParteProcedimentoDTO[$dblIdProcedimento] as $objParteProcedimentoDTO) {
            $strInteressados .= PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrNomeContato()) . ' (' . PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrDescricaoQualificacaoParte()) . ')';
            if (-- $j>0) {
              $strInteressados .= "\n";
            }
          }
        } else if (isset($arrObjParticipanteDTO[$dblIdProcedimento])) {
          $strSeparador = '';
          foreach ($arrObjParticipanteDTO[$dblIdProcedimento] as $objParticipanteDTO) {
            $strInteressados .= $strSeparador . PaginaSEI::tratarHTML($objParticipanteDTO->getStrNomeContato());
            $strSeparador = "\n";
          }
        }
        $objPlanilhaAtiva->setCellValueByColumnAndRow(4,$numLinhaAtual,utf8_encode($strInteressados));
        $objPlanilhaAtiva->setCellValueByColumnAndRow(5,$numLinhaAtual,utf8_encode($objDistribuicaoDTOBanco->getStrNomeUsuarioRelator()));
        //situacao
        $strSituacao=PaginaSEI::tratarHTML($arrObjEstadoDistribuicaoDTO[$strStaDistribuicao]->getStrDescricao());
        if($objDistribuicaoDTOBanco->getStrStaUltimo()==DistribuicaoRN::$SD_REDISTRIBUIDO){

          $objRichText = new PHPExcel_RichText();
          $objTachado = $objRichText->createTextRun(utf8_encode($strSituacao));
          $objTachado->getFont()->setStrikethrough(true);
          $objRichText->createText(utf8_encode(" **houve nova distribui��o"));
          $objPlanilhaAtiva->setCellValueByColumnAndRow(6,$numLinhaAtual,$objRichText);
        } else {
          if($strStaDistribuicao==DistribuicaoRN::$STA_PEDIDO_VISTA){
            $strSituacao.= ' por ';
            $numIdDistribuicao=$objDistribuicaoDTOBanco->getNumIdDistribuicao();
            if(isset($arrObjPedidoVistaDTO[$numIdDistribuicao])){
              $strSituacao.=$arrObjPedidoVistaDTO[$numIdDistribuicao]->getStrNomeUsuario();
            }
          }
          $objPlanilhaAtiva->setCellValueByColumnAndRow(6,$numLinhaAtual,utf8_encode($strSituacao));
        }
      }

      $objPlanilhaAtiva->getStyle('A2:G'.$numLinhaAtual)->getAlignment()->setWrapText(true)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

      $objPlanilhaAtiva->getStyle('A2:C'.$numLinhaAtual)->getAlignment()->setWrapText(true)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      $objPlanilhaAtiva->getStyle('G2:G'.$numLinhaAtual)->getAlignment()->setWrapText(true)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// Save Excel 2007 file
      $objAnexoRN = new AnexoRN();
      $strArquivoTemp = $objAnexoRN->gerarNomeArquivoTemporario().'.xlsx';
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save(DIR_SEI_TEMP.'/'.$strArquivoTemp);
      $strNomeDownload = 'SEI-Relatorio-Distribuicao-'.str_replace(array('/',' ',':'),'-',InfraData::getStrDataHoraAtual()).'.xlsx';

      $bolGeracaoOK = true;


    } else {
      PaginaSEI::getInstance()->processarPaginacao($objDistribuicaoDTO);


      $strResultado .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistros) . '</caption>';
      $strResultado .= '<tr>';

      if ($numRegistros>0) {

        //$bolAcaoImprimir = true;
        //if ($bolAcaoImprimir) {
        //  $bolCheck = true;
        //  $arrComandos[] = '<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="infraImprimirTabela();" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir</button>';
        //}

        $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO, 'Colegiado', 'SiglaColegiado', $arrObjDistribuicaoDTO) . '</th>' . "\n";
        $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO, 'Data da Distribui��o', 'Distribuicao', $arrObjDistribuicaoDTO) . '</th>' . "\n";
        $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO, 'Processo', 'IdProcedimento', $arrObjDistribuicaoDTO) . '</th>' . "\n";
        $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO, 'Tipo do Processo', 'NomeTipoProcedimento', $arrObjDistribuicaoDTO) . '</th>' . "\n";
        $strResultado .= '<th class="infraTh">Interessados</th>' . "\n";
        $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO, 'Relator', 'NomeUsuarioRelator', $arrObjDistribuicaoDTO) . '</th>' . "\n";
        $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO, 'Situa��o Atual', 'StaDistribuicao', $arrObjDistribuicaoDTO) . '</th>' . "\n";

        $strResultado .= '</tr>' . "\n";
        $strCssTr = '';
        foreach ($arrObjDistribuicaoDTO as $i => $objDistribuicaoDTOBanco) {
          /* @var $objDistribuicaoDTOBanco DistribuicaoDTO */
          $strCssTr = ($strCssTr=='<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
          $numIdColegiado = $objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao();
          $dblIdProcedimento = $objDistribuicaoDTOBanco->getDblIdProcedimento();
          if ($objDistribuicaoDTOBanco->getStrStaUltimo()==DistribuicaoRN::$SD_REDISTRIBUIDO || $objDistribuicaoDTOBanco->getStrStaDistribuicao()==DistribuicaoRN::$STA_CANCELADO) {
            $strResultado .= '<tr class="trVermelha">';
          } else {
            $strResultado .= $strCssTr;
          }
          $strResultado .= '<td style="text-align: center"><a alt="' . PaginaSEI::tratarHTML($objDistribuicaoDTOBanco->getStrNomeColegiado()) . '" title="' . PaginaSEI::tratarHTML($objDistribuicaoDTOBanco->getStrNomeColegiado()) . '" class="ancoraSigla">' . PaginaSEI::tratarHTML($objDistribuicaoDTOBanco->getStrSiglaColegiado()) . '</a></td>';
          $strResultado .= '<td style="text-align: center">' . $objDistribuicaoDTOBanco->getDthDistribuicao() . '</td>';

          //processo
          $strResultado .= '<td style="text-align: center">';
          if ($objDistribuicaoDTOBanco->getStrStaNivelAcessoGlobal()===ProtocoloRN::$NA_SIGILOSO) {
            $class = 'processoVisualizadoSigiloso';
            $bolLink = isset($arrIdProtocolosPermitidos[$dblIdProcedimento]);
          } else {
            $class = 'protocoloNormal';
            $bolLink = true;
          }
          $strLink = $bolLink ? 'href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $dblIdProcedimento) . '" target="_blank"' : 'href="javascript:void(0);"';
          $strStaDistribuicao = $objDistribuicaoDTOBanco->getStrStaDistribuicao();
          $strTipoProcedimento = PaginaSEI::tratarHTML($objDistribuicaoDTOBanco->getStrNomeTipoProcedimento());
//        $strResultado .= AnotacaoINT::montarIconeAnotacao($arrObjProtocoloDTO[$objDistribuicaoDTOBanco->getDblIdProcedimento()]->getObjAnotacaoDTO(), $bolAcaoRegistrarAnotacao, $objDistribuicaoDTOBanco->getDblIdProcedimento(), $strParametros);
          $strResultado .= '<a ' . $strLink . ' tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" alt="' . $strTipoProcedimento . '" title="' . $strTipoProcedimento . '" class="' . $class . '">' . PaginaSEI::tratarHTML($objDistribuicaoDTOBanco->getStrProtocoloFormatado()) . '</a>';
          $strResultado .= '</td>';
          $strResultado .= '<td>' . $objDistribuicaoDTOBanco->getStrNomeTipoProcedimento() . '</td>';
          //interessados
          $strResultado .= '<td>';


          if (isset($arrObjParteProcedimentoDTO[$dblIdProcedimento])) {
            $j = InfraArray::contar($arrObjParteProcedimentoDTO[$dblIdProcedimento]);
            foreach ($arrObjParteProcedimentoDTO[$dblIdProcedimento] as $objParteProcedimentoDTO) {
              $strResultado .= PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrNomeContato()) . ' (' . PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrDescricaoQualificacaoParte()) . ')';
              if (-- $j>0) {
                $strResultado .= $j>1 ? ', ' : ' e ';
              }
            }
            $strResultado .= "</div></div>\n";
          } else if (isset($arrObjParticipanteDTO[$dblIdProcedimento])) {
            $strSeparador = '';
            foreach ($arrObjParticipanteDTO[$dblIdProcedimento] as $objParticipanteDTO) {
              $strResultado .= $strSeparador . PaginaSEI::tratarHTML($objParticipanteDTO->getStrNomeContato());
              $strSeparador = ', ';
            }
          }


          $strResultado .= '</td>';
          $strResultado .= '<td>' . $objDistribuicaoDTOBanco->getStrNomeUsuarioRelator() . '</td>';
          //situacao
          $strSituacao = PaginaSEI::tratarHTML($arrObjEstadoDistribuicaoDTO[$strStaDistribuicao]->getStrDescricao());
          $strResultado .= '<td style="text-align: center">';
          if ($objDistribuicaoDTOBanco->getStrStaUltimo()==DistribuicaoRN::$SD_REDISTRIBUIDO) {
            $strResultado .= '<s>' . $strSituacao . '</s>';
            $strResultado .= '<br>*houve nova distribui��o';
          } else {
            $strResultado .= $strSituacao;
          }
          if ($strStaDistribuicao==DistribuicaoRN::$STA_PEDIDO_VISTA) {
            $strResultado .= ' por ';
            $numIdDistribuicao = $objDistribuicaoDTOBanco->getNumIdDistribuicao();
            if (isset($arrObjPedidoVistaDTO[$numIdDistribuicao])) {
              $strResultado .= $arrObjPedidoVistaDTO[$numIdDistribuicao]->getStrNomeUsuario();
            }

          }
          $strResultado .= '</td>';

//        $strResultado .= '<td style="text-align: center">' . $objDistribuicaoDTOBanco->getStrStaUltimo() . '</td>';
          $strResultado .= '</tr>' . "\n";
        }
        $strResultado .= '</table>';
      }
    }
  }
  else {//resumo por membros
    $numRegistros = 0;
    $strResultado = '';



    $objDistribuicaoRN = new DistribuicaoRN();
    $objDistribuicaoDTO->setOrdStrSiglaColegiado(InfraDTO::$TIPO_ORDENACAO_ASC);
    $objDistribuicaoDTO->setOrdStrNomeUsuarioRelator(InfraDTO::$TIPO_ORDENACAO_ASC);

    $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);

    $arrObjDistribuicaoDTO=InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO,['SiglaColegiado','NomeUsuarioRelator'],true);


    foreach ($arrObjDistribuicaoDTO as $strSiglaColegiado=>$arrObjDistribuicaoDTORelator) {
      $numDistribuicoesColegiado=0;
      $numDistribuicoesCanceladasColegiado=0;
      $numRedistribuicoesColegiado=0;
      $arrTabelaColegiado=[];
      $strNomeColegiado='';
      foreach ($arrObjDistribuicaoDTORelator as $strNomeRelator=>$arrObjDistribuicaoDTO2) {
        $numDistribuicoesRelator=0;
        $numDistribuicoesCanceladasRelator=0;
        $numRedistribuicoesRelator=0;
        foreach ($arrObjDistribuicaoDTO2 as $objDistribuicaoDTO) {
          $strNomeColegiado=$objDistribuicaoDTO->getStrNomeColegiado();
          ++$numDistribuicoesRelator;
          if($objDistribuicaoDTO->getStrStaUltimo()==DistribuicaoRN::$SD_REDISTRIBUIDO){
            ++$numRedistribuicoesRelator;
          } elseif($objDistribuicaoDTO->getStrStaDistribuicao()==DistribuicaoRN::$STA_CANCELADO){
            ++$numDistribuicoesCanceladasRelator;
          }
        }
        $saldo=$numDistribuicoesRelator-$numDistribuicoesCanceladasRelator-$numRedistribuicoesRelator;
        $numDistribuicoesColegiado+=$numDistribuicoesRelator;
        $numRedistribuicoesColegiado+=$numRedistribuicoesRelator;
        $numDistribuicoesCanceladasColegiado+=$numDistribuicoesCanceladasRelator;
        $arrTabelaColegiado[]=[$strNomeRelator,$numDistribuicoesRelator,-$numDistribuicoesCanceladasRelator,-$numRedistribuicoesRelator,$saldo];

      }
      $strSumarioTabela = 'Tabela de Resumo de Distribui��es.';
      $strCaptionTabela = 'Resumo de Distribui��es';
      $strResultado .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
      $numRegistros = InfraArray::contar($arrTabelaColegiado);
      $strResultado .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistros) . '</caption>';
      $strResultado .= '<tr>';

      $strResultado .= '<th style="width: 10%" class="infraTh">Colegiado</th>' . "\n";
      $strResultado .= '<th style="width: 30%" class="infraTh">Relator</th>' . "\n";
      $strResultado .= '<th class="infraTh">N� Distribui��es</th>' . "\n";
      $strResultado .= '<th class="infraTh">N� Distribui��es Canceladas</th>' . "\n";
      $strResultado .= '<th class="infraTh">N� Redistribui��es</th>' . "\n";
      $strResultado .= '<th class="infraTh">Saldo</th>' . "\n";
      $strResultado .= '</tr>' . "\n";

      $strCssTr = '';
      foreach ($arrTabelaColegiado as $arrResultado) {
        $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
        $strResultado .= $strCssTr;
        $strResultado .= '<td style="text-align: center"><a alt="' . PaginaSEI::tratarHTML($strSiglaColegiado) . '" title="' . PaginaSEI::tratarHTML($strNomeColegiado) . '" class="ancoraSigla">' . PaginaSEI::tratarHTML($strSiglaColegiado) . '</a></td>';
        $strResultado .= '<td style="text-align: center">' . $arrResultado[0] . '</td>';
        $strResultado .= '<td style="text-align: center">' . ($arrResultado[1]==0?'':$arrResultado[1]) . '</td>';
        $strResultado .= '<td style="text-align: center">' . ($arrResultado[2]==0?'':$arrResultado[2]) . '</td>';
        $strResultado .= '<td style="text-align: center">' . ($arrResultado[3]==0?'':$arrResultado[3]) . '</td>';
        $strResultado .= '<td style="text-align: center;color: red;font-weight: bold ">' . $arrResultado[4] . '</td>';
        $strResultado .= '</tr>' . "\n";
      }
      $strResultado .= '<tr class="infraTh">';
      $strResultado .= '<td style="text-align: center;font-weight: bold"><a alt="' . PaginaSEI::tratarHTML($strSiglaColegiado) . '" title="' . PaginaSEI::tratarHTML($strNomeColegiado) . '" class="ancoraSigla">' . PaginaSEI::tratarHTML($strSiglaColegiado) . '</a></td>';
      $strResultado .= '<td style="text-align: center;font-weight: bold">Total do Colegiado</td>';
      $strResultado .= '<td style="text-align: center;font-weight: bold">' . $numDistribuicoesColegiado . '</td>';
      $strResultado .= '<td style="text-align: center;font-weight: bold">' . (-$numDistribuicoesCanceladasColegiado) . '</td>';
      $strResultado .= '<td style="text-align: center;font-weight: bold">' . (-$numRedistribuicoesColegiado) . '</td>';
      $strResultado .= '<td style="text-align: center;color: red;font-weight: bold">' . ($numDistribuicoesColegiado-$numDistribuicoesCanceladasColegiado-$numRedistribuicoesColegiado) . '</td>';
      $strResultado .= '</tr>' . "\n";
      $strResultado .= '</table><br>';


    }



  }

  $strItensSelTipoProcedimento 	= DistribuicaoINT::montarSelectNomeTipoProcedimento('null','Todos',$numIdTipoProcedimento,$idColegiado);
  $strItensSelColegiado=ColegiadoINT::montarSelectNomeAdministrados('null','Todos',$idColegiado);

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
  #lblDataInicial {position:absolute;left:0%;top:15%;width:10%;}
  #txtDataInicial {position:absolute;left:14%;top:0%;width:10%;}
  #imgCalDataInicial {position:absolute;left:25%;top:10%;}

  #lblDataFinal {position:absolute;left:29%;top:15%;width:10%;}
  #txtDataFinal {position:absolute;left:34%;top:0%;width:10%;}
  #imgCalDataFinal {position:absolute;left:45%;top:10%;}

  #lblColegiado {position:absolute;left:0%;top:15%;width:9%;}
  #selColegiado {position:absolute;left:14%;top:0%;width:40%;}

  #lblTipoProcedimento {position:absolute;left:0%;top:15%;width:20%;}
  #selTipoProcedimento {position:absolute;left:14%;top:0%;width:40%;}

  #divSinResumo {position:absolute;left:0;top:40%;}
<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

function inicializar() {

  <?if ($bolGeracaoOK){ ?>
  window.open('<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=exibir_arquivo&nome_arquivo='.$strArquivoTemp.'&nome_download='.InfraUtil::formatarNomeArquivo($strNomeDownload).'&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']);?>');
  <?}?>

  infraEfeitoTabelas();
}

function limpar() {
  document.getElementById('txtDataInicial').value = '';
  document.getElementById('txtDataFinal').value = '';
  document.getElementById('selColegiado').selectedIndex = -1;
}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
  <form id="frmRelatorioDistribuicao"  method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
    <?
    //PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?>

    <label id="lblDataInicial" for="txtDataInicial" accesskey="" class="infraLabelOpcional">Per�odo:</label>
    <input type="text" id="txtDataInicial" name="txtDataInicial" class="infraText" value="<?=$strDataInicial?>" onkeypress="return infraMascaraData(this, event)" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <img id="imgCalDataInicial" title="Selecionar Data Inicial" alt="Selecionar Data Inicial" src="<?=PaginaSEI::getInstance()->getIconeCalendario()?>" class="infraImg" onclick="infraCalendario('txtDataInicial',this);" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

    <label id="lblDataFinal" for="txtDataFinal" accesskey="" class="infraLabelOpcional">at�</label>
    <input type="text" id="txtDataFinal" name="txtDataFinal" class="infraText" value="<?=$strDataFinal?>" onkeypress="return infraMascaraData(this, event)" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <img id="imgCalDataFinal" title="Selecionar Data Final" alt="Selecionar Data Final" src="<?=PaginaSEI::getInstance()->getIconeCalendario()?>" class="infraImg" onclick="infraCalendario('txtDataFinal',this);" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

     <?
    PaginaSEI::getInstance()->fecharAreaDados();
     PaginaSEI::getInstance()->abrirAreaDados('3em');
     ?>
    <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelOpcional">Colegiado:</label>
    <select id="selColegiado" name="selColegiado" onchange="this.form.submit();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
      <?=$strItensSelColegiado?>
    </select>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?>
    <label id="lblTipoProcedimento" for="selTipoProcedimento" accesskey="" class="infraLabelOpcional">Tipo de Processo:</label>
    <select id="selTipoProcedimento" name="selTipoProcedimento" onchange="this.form.submit();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
      <?=$strItensSelTipoProcedimento?>
    </select>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?>
    <div id="divSinResumo" class="infraDivCheckbox">
      <input type="checkbox" id="chkSinResumo" name="chkSinResumo" class="infraCheckbox" <?=PaginaSEI::getInstance()->setCheckbox($strSinResumo)?>  tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
      <label id="chkSinResumo" for="chkSinResumo" accesskey="" class="infraLabelCheckbox">Resumo por membro</label>
    </div>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
    PaginaSEI::getInstance()->montarAreaDebug();
    //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);

    ?>
  </form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>