<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ImpedimentoINT extends InfraINT {

  public static function montarSelectIdImpedimento($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdDistribuicao='', $numIdUsuario=''){
    $objImpedimentoDTO = new ImpedimentoDTO();
    $objImpedimentoDTO->retNumIdImpedimento();
    $objImpedimentoDTO->retNumIdImpedimento();

    if ($numIdDistribuicao!==''){
      $objImpedimentoDTO->setNumIdDistribuicao($numIdDistribuicao);
    }

    if ($numIdUsuario!==''){
      $objImpedimentoDTO->setNumIdUsuario($numIdUsuario);
    }

    $objImpedimentoDTO->setOrdNumIdImpedimento(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objImpedimentoRN = new ImpedimentoRN();
    $arrObjImpedimentoDTO = $objImpedimentoRN->listar($objImpedimentoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjImpedimentoDTO, 'IdImpedimento', 'IdImpedimento');
  }
}
?>