<?
/*
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/04/2014 - criado por MGA
*
*/

class MdJulgarIntegracao extends SeiIntegracao
{
  public const DIRETORIO_MODULO ='modulos/trf4/julgamento';

  private static $arrDistribuicoes=[];
  private static $arrIdColegiadosUnidade=null;
  private static $arrIdColegiadosAdministrados=null;

  /**
   * @return DistribuicaoDTO[]
   */
  private static function getArrDistribuicoes($dblIdProcedimento): array
  {
    if(!isset(self::$arrDistribuicoes[$dblIdProcedimento])){
      $objDistribuicaoDTO = new DistribuicaoDTO();
      $objDistribuicaoDTO->setDblIdProcedimento($dblIdProcedimento);
      $objDistribuicaoDTO->setStrStaUltimo(array(DistribuicaoRN::$SD_ULTIMA, DistribuicaoRN::$SD_ULTIMA_JULGADO), InfraDTO::$OPER_IN);
      $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_CANCELADO, InfraDTO::$OPER_DIFERENTE);
      $objDistribuicaoDTO->retNumIdDistribuicao();
      $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
      $objDistribuicaoDTO->retStrStaDistribuicao();
      $objDistribuicaoDTO->retNumIdUnidadeResponsavelColegiado();
      $objDistribuicaoDTO->retNumIdUnidadeRelator();
      $objDistribuicaoDTO->retNumIdUsuarioRelator();
      $objDistribuicaoDTO->retDthSituacaoAtual();
      $objDistribuicaoDTO->retStrSiglaColegiado();
      $objDistribuicaoDTO->setOrdDthSituacaoAtual(InfraDTO::$TIPO_ORDENACAO_ASC);

      $objDistribuicaoRN = new DistribuicaoRN();
      self::$arrDistribuicoes[$dblIdProcedimento] = $objDistribuicaoRN->listar($objDistribuicaoDTO);

    }
    return self::$arrDistribuicoes[$dblIdProcedimento];
  }

  /**
   * @return array
   * @throws InfraException
   */
  private static function getArrIdColegiadosUnidade(): array
  {
    if(self::$arrIdColegiadosUnidade===null){
      $objColegiadoRN=new ColegiadoRN();
      self::$arrIdColegiadosUnidade=$objColegiadoRN->listarColegiadosUnidadeAtual();
    }
    return self::$arrIdColegiadosUnidade;
  }

  /**
   * @return array
   * @throws InfraException
   */
  private static function getArrIdColegiadosAdministrados(): array
  {
    if(self::$arrIdColegiadosAdministrados===null){
      $objColegiadoRN=new ColegiadoRN();
      self::$arrIdColegiadosAdministrados=$objColegiadoRN->listarColegiadosAdministrados();
    }
    return self::$arrIdColegiadosAdministrados;
  }

  public function getNome(): string
  {
    return 'SEI Julgar';
  }

  public function getVersao(): string
  {
    return '2.0.3';
  }

  public function getInstituicao(): string
  {
    return 'TRF4 - Tribunal Regional Federal da 4� Regi�o';
  }

  public function obterDiretorioIconesMenu(): string
  {
    return self::DIRETORIO_MODULO.'/menu';
  }

  public function montarIconeControleProcessos($arrObjProcedimentoAPI): array
  {

    $arrRet = array();

    //if (!SessaoSEI::getInstance()->verificarPermissao('distribuicao_consultar')) {
    //  return array();
    //}


    //$arrProcedimentos = InfraArray::converterArrInfraDTO($arrObjProcedimentoDTO, 'IdProcedimento');
    $arrProcedimentos = array();
    foreach ($arrObjProcedimentoAPI as $objProcedimentoAPI){
      $arrProcedimentos[] = $objProcedimentoAPI->getIdProcedimento();
    }

    if (count($arrProcedimentos)) {

      $objColegiadoRN=new ColegiadoRN();
      //se unidade atual n�o pertence a nenhum colegiado n�o exibe martelos
      if (!$objColegiadoRN->verificaUnidadeAtual(null)){
        return $arrRet;
      }

      $objDistribuicaoDTO = new DistribuicaoDTO();
      $objDistribuicaoDTO->setDblIdProcedimento($arrProcedimentos, InfraDTO::$OPER_IN);
      $objDistribuicaoDTO->retDblIdProcedimento();
      $objDistribuicaoDTO->retNumIdDistribuicao();
      $objDistribuicaoDTO->setStrStaUltimo(array(DistribuicaoRN::$SD_ULTIMA,DistribuicaoRN::$SD_ULTIMA_JULGADO),InfraDTO::$OPER_IN);
      $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_CANCELADO,InfraDTO::$OPER_DIFERENTE);
      $objDistribuicaoDTO->retStrStaDistribuicao();
      $objDistribuicaoDTO->retStrNomeUsuarioRelator();
      $objDistribuicaoDTO->retStrSiglaColegiado();
      $objDistribuicaoDTO->retDthSituacaoAtual();
      $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
      $objDistribuicaoDTO->setOrdDblIdProcedimento(InfraDTO::$TIPO_ORDENACAO_ASC);
      $objDistribuicaoDTO->setOrdDthSituacaoAtual(InfraDTO::$TIPO_ORDENACAO_DESC);
      $objDistribuicaoRN = new DistribuicaoRN();
      $objPedidoVistaRN=new PedidoVistaRN();
      $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);
      //deixar somente um martelo por colegiado por processo
      $idProcedimentoAtual=null;
      $arrColegiados=array();
      foreach ($arrObjDistribuicaoDTO as $key=>$objDistribuicaoDTO) {
        if($objDistribuicaoDTO->getDblIdProcedimento()!=$idProcedimentoAtual){
          $idProcedimentoAtual=$objDistribuicaoDTO->getDblIdProcedimento();
          $arrColegiados=array();
        }
        $idColegiado=$objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao();
        if(isset($arrColegiados[$idColegiado])){
          unset($arrObjDistribuicaoDTO[$key]);
          continue;
        }
        $arrColegiados[$idColegiado]=1;
      }
      $arrObjDistribuicaoDTO=array_values($arrObjDistribuicaoDTO);
      InfraArray::ordenarArrInfraDTO($arrObjDistribuicaoDTO,'SituacaoAtual',InfraArray::$TIPO_ORDENACAO_ASC);

      $arrIdDistribuicao=InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO,'IdDistribuicao','StaDistribuicao',true);
      $arrObjPedidoVistaDTO=array();
      if(isset($arrIdDistribuicao[DistribuicaoRN::$STA_PEDIDO_VISTA])){
        $objPedidoVistaDTO=new PedidoVistaDTO();
        $objPedidoVistaDTO->setNumIdDistribuicao($arrIdDistribuicao[DistribuicaoRN::$STA_PEDIDO_VISTA],InfraDTO::$OPER_IN);
        $objPedidoVistaDTO->setDthDevolucao(null);
        $objPedidoVistaDTO->retStrNomeUsuario();
        $objPedidoVistaDTO->retNumIdDistribuicao();
        $arrObjPedidoVistaDTO=$objPedidoVistaRN->listar($objPedidoVistaDTO);
        $arrObjPedidoVistaDTO=InfraArray::indexarArrInfraDTO($arrObjPedidoVistaDTO,'IdDistribuicao');
      }
      //buscar data da sessao
      $arrIdDistribuicaoBusca=array();
      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
        if(in_array($objDistribuicaoDTO->getStrStaDistribuicao(),array(DistribuicaoRN::$STA_JULGADO,DistribuicaoRN::$STA_PAUTADO,DistribuicaoRN::$STA_EM_MESA,DistribuicaoRN::$STA_PARA_REFERENDO))){
          $arrIdDistribuicaoBusca[]=$objDistribuicaoDTO->getNumIdDistribuicao();
        }
      }
      if(count($arrIdDistribuicaoBusca)){
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
        $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($arrIdDistribuicaoBusca,InfraDTO::$OPER_IN);
        $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
        $objItemSessaoJulgamentoDTO->retDthSessaoSessaoJulgamento();
        $objItemSessaoJulgamentoDTO->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_DESC);
        $objItemSessaoJulgamentoDTO->retDthInclusao();
        $objItemSessaoJulgamentoDTO->retStrSinVirtualTipoSessao();
        $objItemSessaoJulgamentoDTO->retStrDescricaoSessaoBloco();
        $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);
        $arrObjItemSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdDistribuicao',true);
      }
      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
        $numIdDistribuicao=$objDistribuicaoDTO->getNumIdDistribuicao();
        $strComplementoTitulo=' ('.$objDistribuicaoDTO->getStrSiglaColegiado(). ')';
        if(isset($arrObjItemSessaoJulgamentoDTO[$numIdDistribuicao])){
          $strComplementoTitulo=' ('.$objDistribuicaoDTO->getStrSiglaColegiado(). ' - '.substr($arrObjItemSessaoJulgamentoDTO[$numIdDistribuicao][0]->getDthSessaoSessaoJulgamento(),0,-3).')';
        }
        $strTexto= 'Relator: ' .$objDistribuicaoDTO->getStrNomeUsuarioRelator();
        switch ($objDistribuicaoDTO->getStrStaDistribuicao()) {
          case DistribuicaoRN::$STA_JULGADO:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Julgado'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::MARTELO_PR.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_PEDIDO_VISTA:
            if(isset($arrObjPedidoVistaDTO[$objDistribuicaoDTO->getNumIdDistribuicao()])){
              $strTexto.="\nVista: ".$arrObjPedidoVistaDTO[$objDistribuicaoDTO->getNumIdDistribuicao()]->getStrNomeUsuario();
            }
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Pedido de Vista'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::MARTELO_AZ.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_PAUTADO:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Pautado'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_EM_MESA:
            $strMesa=$arrObjItemSessaoJulgamentoDTO[$numIdDistribuicao][0]->getStrDescricaoSessaoBloco();
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,$strMesa.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_PARA_REFERENDO:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Para Referendo'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_CANCELADO:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Distribui��o cancelada.'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::MARTELO_VM.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_ADIADO:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Adiado'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_DILIGENCIA:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Em Dilig�ncia'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_REDISTRIBUIDO:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Redistribu�do'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
            break;
          case DistribuicaoRN::$STA_RETIRADO_PAUTA:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Retirado de pauta'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
            break;

          default:
            $icone='<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($strTexto,'Distribu�do'.$strComplementoTitulo) . '><img src="'.MdJulgarIcone::JULGAMENTO.'" class="imagemStatus" /></a>';
        }
        if(!isset($arrRet[$objDistribuicaoDTO->getDblIdProcedimento()])){
          $arrRet[$objDistribuicaoDTO->getDblIdProcedimento()] = array($icone);
        } else {
          $arrRet[$objDistribuicaoDTO->getDblIdProcedimento()][] = $icone;
        }
      }
    }
    return $arrRet;
  }

  public function montarBotaoControleProcessos(): array
  {
    $arrAcoes = array();

    if (SessaoSEI::getInstance()->verificarPermissao('julgamento_listar')) {
      $arrAcoes[] = '<a href="#" onclick="return acaoControleProcessos(\''  . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=julgamento_listar&acao_origem=procedimento_controlar&acao_retorno=procedimento_controlar') . '\', false, false);" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="'.MdJulgarIcone::JULGAMENTO.'" alt="Processos em Julgamento" title="Processos em Julgamento" /></a>';
      $arrAcoes[] = '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_consultar&acao_origem=procedimento_controlar&acao_retorno=procedimento_controlar') . '" target="janelaSessaoJulgamento"  tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="'.MdJulgarIcone::SESSAO_JULGAMENTO.'" alt="Sess�o de Julgamento" title="Sess�o de Julgamento" /></a>';
    }
    if (SessaoSEI::getInstance()->verificarPermissao('distribuicao_gerar')) {
      $arrAcoes[] = '<a href="#" onclick="return acaoControleProcessos(\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=distribuicao_gerar&acao_origem=procedimento_controlar&acao_retorno=procedimento_controlar') . '\', true, true);" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="'.MdJulgarIcone::DISTRIBUICAO.'" alt="Gerar Distribui��o" title="Gerar Distribui��o" /></a>';
    }
    return $arrAcoes;
  }

  public function montarBotaoProcesso(ProcedimentoAPI $objProcedimentoAPI): array
  {
    if (!SessaoSEI::getInstance()->verificarPermissao('distribuicao_consultar')) {
      return array();
    }
    $arrAcoes = array();
    $numIdUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();
    $bolAdmin=SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar');
    $bolAutuar=$bolAdmin;
    $bolAberto=($objProcedimentoAPI->getSinAberto() == 'S');
    $arrColegiadosUnidade=self::getArrIdColegiadosUnidade();
    $arrColegiadosAdmin=self::getArrIdColegiadosAdministrados();
    if(count($arrColegiadosAdmin)==0){
      $bolAdmin=false;
    }

    $dblIdProcedimento = $objProcedimentoAPI->getIdProcedimento();
    $arrObjDistribuicaoDTO = self::getArrDistribuicoes($dblIdProcedimento);
    $arrObjDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO,'IdColegiadoColegiadoVersao');

    //secretaria: gerar / cancelar distribui��o
    if ($bolAdmin && $bolAberto) {
      $bolGerar=false;
      $bolCancelar=false;
      foreach ($arrColegiadosAdmin as $idColegiadoAdministrado) {
        if ($bolGerar || !isset($arrObjDistribuicaoDTO[$idColegiadoAdministrado]) || !in_array($arrObjDistribuicaoDTO[$idColegiadoAdministrado]->getStrStaDistribuicao(), array(DistribuicaoRN::$STA_PAUTADO, DistribuicaoRN::$STA_EM_MESA, DistribuicaoRN::$STA_PARA_REFERENDO, DistribuicaoRN::$STA_PEDIDO_VISTA),true)){
          $bolGerar=true;
        }
        if ($bolCancelar||($arrObjDistribuicaoDTO[$idColegiadoAdministrado] && !in_array($arrObjDistribuicaoDTO[$idColegiadoAdministrado]->getStrStaDistribuicao(), array(DistribuicaoRN::$STA_PAUTADO, DistribuicaoRN::$STA_EM_MESA, DistribuicaoRN::$STA_PARA_REFERENDO, DistribuicaoRN::$STA_JULGADO, DistribuicaoRN::$STA_CANCELADO, DistribuicaoRN::$STA_PEDIDO_VISTA), true))) {
          $bolCancelar=true;
        }
      }
      if($bolGerar) {
        $arrAcoes[] = '<a href="#" onclick="location.href=\\\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=distribuicao_gerar&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_procedimento=' . $objProcedimentoAPI->getIdProcedimento() . '&arvore=1') . '\\\';" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="' . MdJulgarIcone::DISTRIBUICAO . '" alt="Gerar Distribui��o" title="Gerar Distribui��o" /></a>';
      }
      if ($bolCancelar) {
        $arrAcoes[] = '<a href="#" onclick="location.href=\\\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=distribuicao_cancelar&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_procedimento=' . $objProcedimentoAPI->getIdProcedimento() . '&arvore=1') . '\\\';" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="'.MdJulgarIcone::DISTRIBUICAO_CANCELAMENTO.'" alt="Cancelar Distribui��o" title="Cancelar Distribui��o" /></a>';
      }
    }

    $bolGerarDoc=false;
    $arrDistribuicaoPautar=[];
    $arrDistribuicaoDevolver=[];
    $arrItemSessaoFracionar=[];

    /** @var DistribuicaoDTO $objDistribuicaoDTO */
    foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
      $bolRelatorComposicaoAtual=false;
      $numIdColegiado=$objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao();
      //verifica se unidade atual faz parte do colegiado
      $bolUsuarioColegiado=in_array($numIdColegiado,$arrColegiadosUnidade);
      $numIdDistribuicao=$objDistribuicaoDTO->getNumIdDistribuicao();
      $staDistribuicao = $objDistribuicaoDTO->getStrStaDistribuicao();
      if ($bolUsuarioColegiado) {

        //verifica se relator ainda faz parte do colegiado
        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
        $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
        $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
        $objColegiadoComposicaoDTO->setStrSinHabilitado('S');
        $objColegiadoComposicaoDTO->retNumIdColegiadoComposicao();
        $objColegiadoComposicaoDTO->setNumIdUsuario($objDistribuicaoDTO->getNumIdUsuarioRelator());
        $bolRelatorComposicaoAtual = ($objColegiadoComposicaoRN->contar($objColegiadoComposicaoDTO)==1);
      }
      //se for relator ou admin do colegiado libera autua��o
      if(!$bolAutuar && ($objDistribuicaoDTO->getNumIdUnidadeRelator()==$numIdUnidadeAtual || ($bolAdmin && $numIdUnidadeAtual==$objDistribuicaoDTO->getNumIdUnidadeResponsavelColegiado()))){
        $bolAutuar=true;
      }

      if ($bolAberto && $bolUsuarioColegiado) {
        $bolGerarDoc=true;
        switch ($staDistribuicao) {
          case DistribuicaoRN::$STA_DISTRIBUIDO:
          case DistribuicaoRN::$STA_REDISTRIBUIDO:
          case DistribuicaoRN::$STA_RETIRADO_PAUTA:
          case DistribuicaoRN::$STA_ADIADO:
          case DistribuicaoRN::$STA_DILIGENCIA:
            if ($bolRelatorComposicaoAtual && $objDistribuicaoDTO->getNumIdUnidadeRelator()==$numIdUnidadeAtual) {
              $arrDistribuicaoPautar[$numIdDistribuicao] = $numIdDistribuicao;
            }
            break;
          case DistribuicaoRN::$STA_PEDIDO_VISTA:
            $objPedidoVistaDTO = new PedidoVistaDTO();
            $objPedidoVistaDTO->setNumIdDistribuicao($numIdDistribuicao);
            $objPedidoVistaDTO->setDthDevolucao(null);
            $objPedidoVistaDTO->setNumIdUnidade($numIdUnidadeAtual);
            $objPedidoVistaDTO->retNumIdPedidoVista();
            $objPedidoVistaDTO->retNumIdUsuario();
            $objPedidoVistaDTO->setNumMaxRegistrosRetorno(1);
            $objPedidoVistaDTO->setOrdNumIdPedidoVista(InfraDTO::$TIPO_ORDENACAO_DESC);

            $objPedidoVistaRN = new PedidoVistaRN();
            $objPedidoVistaDTO = $objPedidoVistaRN->consultar($objPedidoVistaDTO);
            if ($objPedidoVistaDTO!=null && $objPedidoVistaDTO->getNumIdUsuario()==ColegiadoComposicaoINT::getIdUsuarioMembroUnidadeAtual($objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao())) {
              $arrDistribuicaoPautar[$numIdDistribuicao] = $numIdDistribuicao;
              $arrDistribuicaoDevolver[$numIdDistribuicao] = $numIdDistribuicao;
            }
            break;
          default:
            $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
            $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
            $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($numIdDistribuicao);
            $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
            $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
            $objItemSessaoJulgamentoDTO->setOrdNumIdItemSessaoJulgamento(InfraDTO::$TIPO_ORDENACAO_DESC);
            $objItemSessaoJulgamentoDTO->setNumMaxRegistrosRetorno(1);
            $objItemSessaoJulgamentoDTO->retStrSinManual();
            $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
            if ($objItemSessaoJulgamentoDTO) {
              $arrDistribuicaoPautar[$numIdDistribuicao] = $numIdDistribuicao;
              if ($objItemSessaoJulgamentoDTO->getStrSinManual()!='S' && $objDistribuicaoDTO->getNumIdUnidadeRelator()==$numIdUnidadeAtual) {
                $arrItemSessaoFracionar[] = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();

              }
            }
        }
      }
    }

    if($bolGerarDoc){
      $arrAcoes[] .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=documento_escolher_tipo_julgamento&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_procedimento=' . $dblIdProcedimento . '&arvore=1') . '" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="'.MdJulgarIcone::INCLUIR_DOCUMENTO_JULGAMENTO.'" alt="Incluir Documento para Sess�o de Julgamento" title="Incluir Documento para Sess�o de Julgamento"/></a>';
    }
    if (count($arrDistribuicaoPautar)){
      $arrAcoes[] = '<a href="#" onclick="location.href=\\\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_pautar&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_distribuicao=' . implode(',',$arrDistribuicaoPautar). '&id_procedimento_sessao=' . $dblIdProcedimento . '&arvore=1') . '\\\';" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="'.MdJulgarIcone::SESSAO_JULGAMENTO.'" alt="Sess�o de Julgamento" title="Sess�o de Julgamento" /></a>';
    }
    if (count($arrDistribuicaoDevolver)) {
      $arrAcoes[] = '<a href="#" onclick="location.href=\\\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=pedido_vista_devolver_secretaria&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_distribuicao=' . implode(',',$arrDistribuicaoDevolver).'&id_procedimento_sessao=' . $dblIdProcedimento . '&arvore=1') . '\\\';" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="' . MdJulgarIcone::DISTRIBUICAO_DEVOLVER . '" alt="Devolver para a Secretaria" title="Devolver para a Secretaria" /></a>';
    }
    if(count($arrItemSessaoFracionar)) {
      $arrAcoes[] .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=julgamento_parte_cadastrar&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_item_sessao_julgamento=' . implode(',',$arrItemSessaoFracionar). '&arvore=1') . '" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="' . MdJulgarIcone::FRACIONAR_JULGAMENTO . '" alt="Definir Partes de Julgamento" title="Definir Partes de Julgamento"/></a>';
    }

    if($bolAutuar){
      $arrAcoes[] = '<a href="#" onclick="location.href=\\\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_autuacao_gerenciar&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_procedimento=' . $objProcedimentoAPI->getIdProcedimento() . '&arvore=1') . '\\\';" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="'.MdJulgarIcone::AUTUACAO.'" alt="Autua��o" title="Autua��o" /></a>';
    } else  if (count($arrObjDistribuicaoDTO)>0 && SessaoSEI::getInstance()->verificarPermissao('procedimento_autuacao_consultar')) {
      $objAutuacaoDTO=new AutuacaoDTO();
      $objAutuacaoRN=new AutuacaoRN();
      $objAutuacaoDTO->setDblIdProcedimento($dblIdProcedimento);
      if($objAutuacaoRN->contar($objAutuacaoDTO)) {
        $arrAcoes[] = '<a href="#" onclick="location.href=\\\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_autuacao_consultar&acao_origem=arvore_visualizar&acao_retorno=arvore_visualizar&id_procedimento=' . $objProcedimentoAPI->getIdProcedimento() . '&arvore=1') . '\\\';" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="' . MdJulgarIcone::AUTUACAO . '" alt="Consultar Autua��o" title="Consultar Autua��o" /></a>';
      }
    }

    return $arrAcoes;
  }

  public function montarIconeProcesso(ProcedimentoAPI $objProcedimentoAPI): array
  {
//    if (!SessaoSEI::getInstance()->verificarPermissao('julgamento_listar')) {
//      return array();
//    }
    $ret = array();

    if ($objProcedimentoAPI->getCodigoAcesso()> 0) {
      //se unidade atual n�o pertence a nenhum colegiado n�o exibe martelos
      if(count(self::getArrIdColegiadosUnidade())==0) {
        return $ret;
      }

      $dblIdProcedimento = $objProcedimentoAPI->getIdProcedimento();
      $arrObjDistribuicaoDTO = self::getArrDistribuicoes($dblIdProcedimento);
      InfraArray::ordenarArrInfraDTO($arrObjDistribuicaoDTO,'SituacaoAtual',InfraArray::$TIPO_ORDENACAO_DESC);

      $arrColegiados=array();
      foreach ($arrObjDistribuicaoDTO as $key=>$objDistribuicaoDTO) {
        $strSiglaColegiado=$objDistribuicaoDTO->getStrSiglaColegiado();
        if(isset($arrColegiados[$strSiglaColegiado])){
          unset($arrObjDistribuicaoDTO[$key]);
          continue;
        }
        $arrColegiados[$strSiglaColegiado]=1;
      }

      $arrObjDistribuicaoDTO2=array();
      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
        $objArvoreAcaoItemAPI = new ArvoreAcaoItemAPI();
        $objArvoreAcaoItemAPI->setTipo('JULGAMENTO');
        $objArvoreAcaoItemAPI->setId('J');
        $objArvoreAcaoItemAPI->setIdPai($dblIdProcedimento);
        //$objArvoreAcaoItemAPI->setHref(SessaoSEI::getInstance()->assinarLink('controlador.php?acao=julgamento_listar&acao_origem=procedimento_visualizar&id_procedimento='.$dblIdProcedimento.'&arvore=1'));
        $objArvoreAcaoItemAPI->setHref('javascript:void');
        $objArvoreAcaoItemAPI->setTarget('ifrVisualizacao');
        switch ($objDistribuicaoDTO->getStrStaDistribuicao()) {
          case DistribuicaoRN::$STA_JULGADO:
            $objArvoreAcaoItemAPI->setTitle('Processo julgado (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::MARTELO_PR);
            break;
          case DistribuicaoRN::$STA_PEDIDO_VISTA:
            $objArvoreAcaoItemAPI->setTitle('Processo com Pedido de Vista (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::MARTELO_AZ);
            break;
          case DistribuicaoRN::$STA_PAUTADO:
            $objArvoreAcaoItemAPI->setTitle('Processo pautado (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::JULGAMENTO);
            break;
          case DistribuicaoRN::$STA_EM_MESA:
            $arrObjDistribuicaoDTO2[]=$objDistribuicaoDTO;
            continue 2;
          case DistribuicaoRN::$STA_PARA_REFERENDO:
            $objArvoreAcaoItemAPI->setTitle('Processo para referendo (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::JULGAMENTO);
            break;
          case DistribuicaoRN::$STA_CANCELADO:
            $objArvoreAcaoItemAPI->setTitle('Processo com Distribui��o cancelada (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::MARTELO_VM);
            break;
          case DistribuicaoRN::$STA_ADIADO:
            $objArvoreAcaoItemAPI->setTitle('Processo adiado (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::JULGAMENTO);
            break;
          case DistribuicaoRN::$STA_DILIGENCIA:
            $objArvoreAcaoItemAPI->setTitle('Processo em dilig�ncia (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::JULGAMENTO);
            break;
          default:
            $objArvoreAcaoItemAPI->setTitle('Processo distribu�do para julgamento (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
            $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::JULGAMENTO);
        }

        $objArvoreAcaoItemAPI->setSinHabilitado('S');

        $ret[] = $objArvoreAcaoItemAPI;
      }
      if (count($arrObjDistribuicaoDTO2)){
        $arrNumIdDistribuicao=InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO2,'IdDistribuicao');
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
        $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($arrNumIdDistribuicao,InfraDTO::$OPER_IN);
        $objItemSessaoJulgamentoDTO->setStrStaTipoItemSessaoBloco(TipoSessaoBlocoRN::$STA_MESA);
        $objItemSessaoJulgamentoDTO->retStrDescricaoSessaoBloco();
        $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
        $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);
        $arrObjItemSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdDistribuicao');
        foreach ($arrObjDistribuicaoDTO2 as $objDistribuicaoDTO) {
          $objArvoreAcaoItemAPI = new ArvoreAcaoItemAPI();
          $objArvoreAcaoItemAPI->setTipo('JULGAMENTO');
          $objArvoreAcaoItemAPI->setId('J');
          $objArvoreAcaoItemAPI->setIdPai($dblIdProcedimento);
          $objArvoreAcaoItemAPI->setHref('javascript:void');
          $objArvoreAcaoItemAPI->setTarget('ifrVisualizacao');
          $objArvoreAcaoItemAPI->setTitle('Processo em ' . $arrObjItemSessaoJulgamentoDTO[$objDistribuicaoDTO->getNumIdDistribuicao()]->getStrDescricaoSessaoBloco() . ' (' . $objDistribuicaoDTO->getStrSiglaColegiado() . ')');
          $objArvoreAcaoItemAPI->setIcone(MdJulgarIcone::JULGAMENTO);
          $objArvoreAcaoItemAPI->setSinHabilitado('S');
          $ret[] = $objArvoreAcaoItemAPI;
        }
      }
    }

    return $ret;

  }

  public function montarIconeAcompanhamentoEspecial($arrObjProcedimentoAPI): array
  {
    return $this->montarIconeControleProcessos($arrObjProcedimentoAPI);
  }

  public function montarBotaoDocumento(ProcedimentoAPI $objProcedimentoAPI, $arrObjDocumentoAPI): array
  {
    $arrAcoes = [];
    if($objProcedimentoAPI->getSinAberto() == 'N'){
      return $arrAcoes;
    }

    $bolDisponibilizar=SessaoSEI::getInstance()->verificarPermissao('doc_sessao_julgamento_disponibilizar');
    $bolCancelarDisp=SessaoSEI::getInstance()->verificarPermissao('doc_sessao_julgamento_indisponibilizar');
    if (!$bolDisponibilizar && !$bolCancelarDisp) {
      return $arrAcoes;
    }

    $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();

    $dblIdProcedimento = $objProcedimentoAPI->getIdProcedimento();
    $arrIdColegiadosUnidadeAtual = self::getArrIdColegiadosUnidade();
    if (InfraArray::contar($arrIdColegiadosUnidadeAtual)==0) {
      return $arrAcoes;
    }

    $arrObjDistribuicaoDTO = self::getArrDistribuicoes($dblIdProcedimento);
    $arrObjDistribuicaoDTO2 = [];
    $arrIdDistribuicao = [];
    foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
      if (in_array($objDistribuicaoDTO->getStrStaDistribuicao(), [DistribuicaoRN::$STA_PAUTADO, DistribuicaoRN::$STA_EM_MESA, DistribuicaoRN::$STA_PARA_REFERENDO]) &&
          in_array($objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao(),$arrIdColegiadosUnidadeAtual)) {
        $arrObjDistribuicaoDTO2[] = $objDistribuicaoDTO;
        $arrIdDistribuicao[] = $objDistribuicaoDTO->getNumIdDistribuicao();
      }
    }

    if (count($arrObjDistribuicaoDTO2)==0) {
      return $arrAcoes;
    }

    //busca os item_sessao_julgamento em sess�o
    $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
    $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
    if (count($arrIdDistribuicao)==1) {
      $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($arrIdDistribuicao[0]);
    } else {
      $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($arrIdDistribuicao, InfraDTO::$OPER_IN);
    }
    $objItemSessaoJulgamentoDTO->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_ASC);
    $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento([SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA],InfraDTO::$OPER_IN);
    $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retStrSinVirtualTipoSessao();
    $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdUnidadeSessao();
    $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
    $arrObjItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);
    if(count($arrObjItemSessaoJulgamentoDTO)==0){
      return $arrAcoes;
    }
    $arrObjItemSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdDistribuicao');

    $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
    $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
    if (count($arrObjItemSessaoJulgamentoDTO)==1) {
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento(array_values($arrObjItemSessaoJulgamentoDTO)[0]->getNumIdItemSessaoJulgamento());
    } else {
      $arrIdItemSessaoJulgamento = InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento');
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento, InfraDTO::$OPER_IN);
    }
    $objItemSessaoDocumentoDTO->setNumIdUnidade($numIdUnidadeAtual);
    $objItemSessaoDocumentoDTO->retDblIdDocumento();
    $objItemSessaoDocumentoDTO->retNumIdItemSessaoJulgamento();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
    if ($arrObjItemSessaoDocumentoDTO) {
      foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTOBanco) {
        $numIdItemSessaoJulgamento=$objItemSessaoDocumentoDTOBanco->getNumIdItemSessaoJulgamento();
        $numIdDocumento=$objItemSessaoDocumentoDTOBanco->getDblIdDocumento();
        $arrDblIdDocumento[$numIdDocumento][$numIdItemSessaoJulgamento]=$numIdItemSessaoJulgamento;
      }
    } else {
      $arrDblIdDocumento = [];
    }

    $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
    $arrIdSeriesJulgamento = explode(',', $objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_DOCUMENTOS_DISPONIBILIZAVEIS));

    $arrAcaoDisponibilizar=[];
    $arrAcaoIndisponibilizar=[];

    //efetuar processamento para cada distribui��o/colegiado
    foreach ($arrObjDistribuicaoDTO2 as $objDistribuicaoDTO) {
      $objItemSessaoJulgamentoDTO = $arrObjItemSessaoJulgamentoDTO[$objDistribuicaoDTO->getNumIdDistribuicao()];
      if($objItemSessaoJulgamentoDTO==null){
        continue;
      }
      $bolSessaoVirtual = $objItemSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S';
      $numIdItemSessaoJulgamento = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
      $staDistribuicao = $objDistribuicaoDTO->getStrStaDistribuicao();

      $bolReferendo = $staDistribuicao==DistribuicaoRN::$STA_PARA_REFERENDO;
      if ($bolReferendo && $objDistribuicaoDTO->getNumIdUnidadeRelator()!=$numIdUnidadeAtual) {
        continue; //somente unidade relator pode incluir doc em referendo
      }

      foreach ($arrObjDocumentoAPI as $objDocumentoAPI) {

        $dblIdDocumento = $objDocumentoAPI->getIdDocumento();
        //tem permiss�o
        if ($bolDisponibilizar && !isset($arrDblIdDocumento[$dblIdDocumento][$numIdItemSessaoJulgamento])) { //doc n�o est� disponibilizado nesta sess�o e icone ainda n�o foi liberado
          if ($bolReferendo && $objDocumentoAPI->getSubTipo()==DocumentoRN::$TD_EDITOR_INTERNO) { //referendo permite todos os doc internos
            $arrAcaoDisponibilizar[$dblIdDocumento][$numIdItemSessaoJulgamento] = $numIdItemSessaoJulgamento;
          }
          if (!$bolReferendo //nenhum documento disponibilizado pela unidade (multiplos doc s� em referendos)
              && $objDocumentoAPI->getIdUnidadeGeradora()==$numIdUnidadeAtual //somente gerados pela unidade atual
              && in_array($objDocumentoAPI->getIdSerie(), $arrIdSeriesJulgamento) //series permitidas
          ) {
            $arrAcaoDisponibilizar[$dblIdDocumento][$numIdItemSessaoJulgamento] = $numIdItemSessaoJulgamento;
          }
        }

        if ($bolCancelarDisp && isset($arrDblIdDocumento[$dblIdDocumento][$numIdItemSessaoJulgamento])) { //doc est� disponibilizado nesta sess�o e icone ainda n�o foi liberado
          //#197968 - permitir indisponibilizar em caso de sess�o virtual (Aberta|Suspensa) somente unidade que colocou em sess�o o processo
          if (!$bolSessaoVirtual
              || in_array($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento(),[SessaoJulgamentoRN::$ES_SUSPENSA,SessaoJulgamentoRN::$ES_ABERTA])
              || $objItemSessaoJulgamentoDTO->getNumIdUnidadeSessao()==$numIdUnidadeAtual) {
            $arrAcaoIndisponibilizar[$dblIdDocumento][$numIdItemSessaoJulgamento] = $numIdItemSessaoJulgamento;
          }
        }
      }
    }
    foreach ($arrObjDocumentoAPI as $objDocumentoAPI) {
      $dblIdDocumento = $objDocumentoAPI->getIdDocumento();
      if (isset($arrAcaoDisponibilizar[$dblIdDocumento])) {
        $strLink=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=doc_sessao_julgamento_disponibilizar&acao_origem=procedimento_visualizar&acao_retorno=procedimento_visualizar&id_procedimento=' . $dblIdProcedimento . '&id_documento=' . $dblIdDocumento . '&id_item_sessao_julgamento='.implode(',',$arrAcaoDisponibilizar[$dblIdDocumento]). '&arvore=1');
        $arrAcoes[$dblIdDocumento][] = '<a href="' . $strLink . '" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="' . MdJulgarIcone::DISPONIBILIZAR . '" alt="Disponibilizar para a Sess�o" title="Disponibilizar para a Sess�o" /></a>';
      }
      if (isset($arrAcaoIndisponibilizar[$dblIdDocumento])) {
        $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=doc_sessao_julgamento_indisponibilizar&acao_origem=procedimento_visualizar&acao_retorno=procedimento_visualizar&id_procedimento=' . $dblIdProcedimento . '&id_documento=' . $dblIdDocumento . '&id_item_sessao_julgamento='.implode(',',$arrAcaoIndisponibilizar[$dblIdDocumento]). '&arvore=1');
        $arrAcoes[$dblIdDocumento][] = '<a href="' . $strLink . '" tabindex="' . PaginaSEI::getInstance()->getProxTabBarraComandosSuperior() . '"><img src="' . MdJulgarIcone::DISPONIBILIZAR_CANCELAMENTO . '" alt="Cancelar Disponibiliza��o na Sess�o" title="Cancelar Disponibiliza��o na Sess�o" /></a>';
      }
    }

    return $arrAcoes;
  }

  public function alterarIconeArvoreDocumento(ProcedimentoAPI $objProcedimentoAPI, $arrObjDocumentoAPI): array
  {
    $arrIcones = array();

    $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
    $arrSeriesDistribuicao = null;
    $arrSeriesCancelamento = null;

    foreach ($arrObjDocumentoAPI as $objDocumentoAPI) {

      if ($objDocumentoAPI->getSubTipo()==DocumentoRN::$TD_FORMULARIO_AUTOMATICO) {

        //buscar somente se encontrou um formulario
        if ($arrSeriesDistribuicao==null) {
          $arrParametrosModulo=CacheSessaoJulgamentoRN::getArrParametrosModulo();
          $arrSeriesDistribuicao=array();
          $arrSeriesDistribuicao[] = $arrParametrosModulo[MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_DISTRIBUICAO];
          $arrSeriesDistribuicao[] = $arrParametrosModulo[MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_REDISTRIBUICAO];
          $arrSeriesDistribuicao[] = $arrParametrosModulo[MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_JULGAMENTO];
          $arrSeriesCancelamento = array();
          $arrSeriesCancelamento[] = $arrParametrosModulo[MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO];
          $arrSeriesCancelamento[] = $arrParametrosModulo[MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_CANCELAMENTO_SESSAO];

        }

        if (in_array($objDocumentoAPI->getIdSerie(), $arrSeriesDistribuicao)) {
          $arrIcones[$objDocumentoAPI->getIdDocumento()] = MdJulgarIcone::CERTIDAO_DISTRIBUICAO;
        } elseif (in_array($objDocumentoAPI->getIdSerie(), $arrSeriesCancelamento)) {
          $arrIcones[$objDocumentoAPI->getIdDocumento()] = MdJulgarIcone::CERTIDAO_DISTRIBUICAO_CANCELAMENTO;
        }
      }
    }

    return $arrIcones;
  }

  public function excluirDocumento(DocumentoAPI $objDocumentoAPI){
    $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
    $objSessaoJulgamentoDTO->setDblIdDocumentoSessao($objDocumentoAPI->getIdDocumento());
    if ($objSessaoJulgamentoRN->contar($objSessaoJulgamentoDTO)>0){
      $objInfraException = new InfraException();
      $objInfraException->lancarValidacao('N�o � poss�vel excluir este documento porque ele est� associado com uma sess�o de julgamento finalizada.');
    }
    $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO->setDblIdDocumentoPauta($objDocumentoAPI->getIdDocumento());
    if ($objSessaoJulgamentoRN->contar($objSessaoJulgamentoDTO)>0){
      $objInfraException = new InfraException();
      $objInfraException->lancarValidacao('N�o � poss�vel excluir este documento porque ele est� associado com uma sess�o de julgamento como pauta da sess�o.');
    }

    $this->validarDocumentoSessao($objDocumentoAPI);
  }

  public function cancelarDocumento(DocumentoAPI $objDocumentoAPI){
    $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
    $objSessaoJulgamentoDTO->setDblIdDocumentoSessao($objDocumentoAPI->getIdDocumento());
    if ($objSessaoJulgamentoRN->contar($objSessaoJulgamentoDTO)>0){
      $objInfraException = new InfraException();
      $objInfraException->lancarValidacao('N�o � poss�vel cancelar este documento porque ele est� associado com uma sess�o de julgamento finalizada.');
    }
    $this->validarDocumentoSessao($objDocumentoAPI);
  }

  //vou alterar as valida��es. hoje exclui algumas situa��es da sess�o e do item. aquela situa��o eles devem ter pautado, disponibilizado, e depois de publicado retiraram. permanecendo o item na sess�o, para n�o quebrar a ordem da pauta
  //nesses casos ao inv�s de bloquear a exclus�o, posso excluir o registro, se for s� esse
  private function validarDocumentoSessao(DocumentoAPI $objDocumentoAPI){
    $objItemSessaoDocumentoDTO=new ItemSessaoDocumentoDTO();
    $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
    $objItemSessaoDocumentoDTO->setDblIdDocumento($objDocumentoAPI->getIdDocumento());
    $objItemSessaoDocumentoDTO->retStrStaSituacaoSessaoJulgamento();
    $objItemSessaoDocumentoDTO->retStrStaSituacaoItem();
    $objItemSessaoDocumentoDTO->retStrNomeColegiado();
    $objItemSessaoDocumentoDTO->retDthSessaoSessaoJulgamento();
    $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
    if (InfraArray::contar($arrObjItemSessaoDocumentoDTO)> 0) {
      /** @var ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO */
      foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
        if ($objItemSessaoDocumentoDTO->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_CANCELADA||
            in_array($objItemSessaoDocumentoDTO->getStrStaSituacaoItem(),array(ItemSessaoJulgamentoRN::$STA_IGNORADA,ItemSessaoJulgamentoRN::$STA_ADIADO,ItemSessaoJulgamentoRN::$STA_DILIGENCIA,ItemSessaoJulgamentoRN::$STA_RETIRADO,ItemSessaoJulgamentoRN::$STA_SESSAO_CANCELADA))){
          $objItemSessaoDocumentoRN->excluir(array($objItemSessaoDocumentoDTO));
          continue;
        }
        $objInfraException = new InfraException();
        $objItemSessaoJulgamentoDTO = $arrObjItemSessaoDocumentoDTO[0];
        $objInfraException->lancarValidacao('Documento disponibilizado para Sess�o de Julgamento (' . $objItemSessaoJulgamentoDTO->getStrNomeColegiado() . ' - ' . substr($objItemSessaoJulgamentoDTO->getDthSessaoSessaoJulgamento(),0,-3) . ').');
        break;
      }

    }
  }

  public function verificarAcessoProtocoloExterno($arrObjProcedimentoAPI, $arrObjDocumentoAPI) : ?array
  {
//    if (SessaoSEI::getInstance()->isBolHabilitada() || SessaoSEIExterna::getInstance()->getNumIdUsuarioExterno()==null) {
//      return null;
//    }
    //verifica para observador
    $objRelColegiadoUsuarioDTO = new RelColegiadoUsuarioDTO();
    $objRelColegiadoUsuarioRN = new RelColegiadoUsuarioRN();
    $objRelColegiadoUsuarioDTO->setNumIdUsuario(SessaoSEIExterna::getInstance()->getNumIdUsuarioExterno());
    $objRelColegiadoUsuarioDTO->retNumIdColegiado();
    $arrObjRelColegiadoUsuarioDTO = $objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO);
    if (count($arrObjRelColegiadoUsuarioDTO)==0) {
      return [];
    }



    $arrIdColegiadosObservador = InfraArray::converterArrInfraDTO($arrObjRelColegiadoUsuarioDTO, 'IdColegiado');

    $ret = [];

    $arrProcedimentos = array();

    foreach ($arrObjProcedimentoAPI as $objProcedimentoAPI) {
      $arrProcedimentos[$objProcedimentoAPI->getIdProcedimento()] = $objProcedimentoAPI->getNivelAcesso();
    }

    foreach ($arrObjDocumentoAPI as $objDocumentoAPI) {
      $arrProcedimentos[$objDocumentoAPI->getIdProcedimento()] = $objDocumentoAPI->getNivelAcesso();
    }


    $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
    $bolPermiteVisualizacaoPautaFechada = ($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_ACESSO_OBSERVADOR_EXTERNO, false)==1);

    $arrSituacaoPermitida = array(SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_SUSPENSA);
    if ($bolPermiteVisualizacaoPautaFechada) {
      $arrSituacaoPermitida[] = SessaoJulgamentoRN::$ES_PAUTA_FECHADA;
    }

    //busca itens correspondentes das sess�es de julgamento
    $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
    $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
    $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
    if($bolPermiteVisualizacaoPautaFechada){
      //libera acesso para SESSAO_ABERTA, SUSPENSA e PAUTA_FECHADA
      $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento($arrSituacaoPermitida, InfraDTO::$OPER_IN);
    } else {
      //libera acesso para SESSAO_ABERTA e SUSPENSA + (PAUTA_FECHADA se for referendo)
      $objItemSessaoJulgamentoDTO->adicionarCriterio(['StaSituacaoSessaoJulgamento'],[InfraDTO::$OPER_IN], [$arrSituacaoPermitida],null,'situacao');
      $objItemSessaoJulgamentoDTO->adicionarCriterio(
          ['StaSituacaoSessaoJulgamento','StaTipoItemSessaoBloco'],
          [InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL],
          [SessaoJulgamentoRN::$ES_PAUTA_FECHADA,TipoSessaoBlocoRN::$STA_REFERENDO],
          InfraDTO::$OPER_LOGICO_AND,'referendo');
      $objItemSessaoJulgamentoDTO->agruparCriterios(['situacao','referendo'],InfraDTO::$OPER_LOGICO_OR);
    }

    $objItemSessaoJulgamentoDTO->setNumIdColegiadoSessaoJulgamento($arrIdColegiadosObservador, InfraDTO::$OPER_IN);
    $objItemSessaoJulgamentoDTO->setDblIdProcedimentoDistribuicao(array_keys($arrProcedimentos), InfraDTO::$OPER_IN);

    $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
    $arrObjItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);
    if(count($arrObjItemSessaoJulgamentoDTO)==0){
      return null;
    }
    $arrObjItemSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdProcedimentoDistribuicao');

    foreach ($arrProcedimentos as $dblIdProcedimento => $strStaNivelAcesso) {
      if ($strStaNivelAcesso!==ProtocoloRN::$NA_SIGILOSO && isset($arrObjItemSessaoJulgamentoDTO[$dblIdProcedimento])) {
        $ret[$dblIdProcedimento] = SeiIntegracao::$TAM_PERMITIDO;
      }
    }

    if(count($arrObjDocumentoAPI)>0){
      $arrIdItemSessaoJulgamento=InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdItemSessaoJulgamento');
      //busca documentos disponibilizados
      $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoDTO->setDistinct(true);
      $objItemSessaoDocumentoDTO->retDblIdDocumento();
      $objItemSessaoDocumentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoDocumentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento, InfraDTO::$OPER_IN);

      $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
      $arrObjItemSessaoDocumentoDTO = InfraArray::indexarArrInfraDTO($objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO), 'IdDocumento', true);
      foreach ($arrObjDocumentoAPI as $objDocumentoAPI) {
        if (isset($arrObjItemSessaoDocumentoDTO[$objDocumentoAPI->getIdDocumento()])) {
          $ret[$objDocumentoAPI->getIdDocumento()] = SeiIntegracao::$TAM_PERMITIDO;
        }
      }
    }

    return $ret;
  }
  public function verificarAcessoProtocolo($arrObjProcedimentoAPI, $arrObjDocumentoAPI) : ?array
  {
    if (!SessaoSEI::getInstance()->verificarPermissao('colegiado_composicao_listar')) {
      return [];
    }

//    if (!SessaoSEI::getInstance()->isBolHabilitada() && SessaoSEIExterna::getInstance()->getNumIdUsuarioExterno()!=null) {
//     return null;
//    }

    $ret = [];

    $arrProcedimentos = array();
    $arrIdColegiadosUsuario = array();
    $bolSigiloso = false;

    foreach ($arrObjProcedimentoAPI as $objProcedimentoAPI) {

      $arrProcedimentos[$objProcedimentoAPI->getIdProcedimento()] = $objProcedimentoAPI->getNivelAcesso();

      if (!$bolSigiloso && $objProcedimentoAPI->getNivelAcesso()==ProtocoloRN::$NA_SIGILOSO) {
        $bolSigiloso = true;
      }
    }

    foreach ($arrObjDocumentoAPI as $objDocumentoAPI) {
      //se processo j� estiver inclu�do est� sobrescrevendo
      $arrProcedimentos[$objDocumentoAPI->getIdProcedimento()] = $objDocumentoAPI->getNivelAcesso();

      if (!$bolSigiloso && $objDocumentoAPI->getNivelAcesso()==ProtocoloRN::$NA_SIGILOSO) {
        $bolSigiloso = true;
      }
    }

    //se n�o tem processos retorna
    if (!InfraArray::contar($arrProcedimentos)) {
      return [];
    }

    $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();


    //verifica se term permiss�o para administrar colegiado e quais colegiados s�o administrados por esta unidade
    $arrIdColegiadosAdministrados = self::getArrIdColegiadosAdministrados();
    $bolAdministradorSessao = count($arrIdColegiadosAdministrados)>0;
    //verifica se a unidade faz parte de pelo menos um colegiado
    //busca colegiados da unidade atual
    $arrIdColegiadosUnidade = self::getArrIdColegiadosUnidade();
    if (!$bolAdministradorSessao && count($arrIdColegiadosUnidade)==0) {
      return [];
    }


    //busca itens correspondentes das sess�es de julgamento
    $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
    $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
    $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento(SessaoJulgamentoRN::$ES_CANCELADA, InfraDTO::$OPER_DIFERENTE);

    if ($bolAdministradorSessao) {
      $objItemSessaoJulgamentoDTO->setNumIdColegiadoSessaoJulgamento(array_merge($arrIdColegiadosUnidade, $arrIdColegiadosAdministrados), InfraDTO::$OPER_IN);
    }  else {
      $objItemSessaoJulgamentoDTO->setNumIdColegiadoSessaoJulgamento($arrIdColegiadosUnidade, InfraDTO::$OPER_IN);
    }

    $objItemSessaoJulgamentoDTO->setDblIdProcedimentoDistribuicao(array_keys($arrProcedimentos), InfraDTO::$OPER_IN);

    $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
    $arrObjItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

    //se nao tiverem itens a processar retorna - processo precisa estar em sess�o
    if (count($arrObjItemSessaoJulgamentoDTO)==0) {
      return [];
    }

    $arrIdItemSessaoJulgamento = InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento');
    $arrIdDistribuicao = InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdDistribuicao');
    $arrObjItemSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdProcedimentoDistribuicao', true);


    //busca documentos disponibilizados
    $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
    $objItemSessaoDocumentoDTO->setDistinct(true);
    $objItemSessaoDocumentoDTO->retDblIdDocumento();
    $objItemSessaoDocumentoDTO->retNumIdColegiadoSessaoJulgamento();
    $objItemSessaoDocumentoDTO->retStrStaSituacaoSessaoJulgamento();
    $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento, InfraDTO::$OPER_IN);

    $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
    $arrObjItemSessaoDocumentoDTO = InfraArray::indexarArrInfraDTO($objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO), 'IdDocumento', true);

    //blusca bloqueios
    $objBloqueioItemSessUnidadeDTO = new BloqueioItemSessUnidadeDTO();
    $objBloqueioItemSessUnidadeRN = new BloqueioItemSessUnidadeRN();
    $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($arrIdDistribuicao, InfraDTO::$OPER_IN);
    $objBloqueioItemSessUnidadeDTO->setStrStaDistribuicaoDistribuicao(DistribuicaoRN::$STA_CANCELADO, InfraDTO::$OPER_DIFERENTE);
    $objBloqueioItemSessUnidadeDTO->setStrStaUltimoDistribuicao(DistribuicaoRN::$SD_ULTIMA);
    $objBloqueioItemSessUnidadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objBloqueioItemSessUnidadeDTO->retDblIdDocumento();
    $objBloqueioItemSessUnidadeDTO->retDblIdProcedimentoDistribuicao();
    $arrObjBloqueioItemSessUnidadeDTO = $objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);

    $arrDocumentosBloqueados = array();
    $arrProcessosBloqueados = array();
    foreach ($arrObjBloqueioItemSessUnidadeDTO as $objBloqueioItemSessUnidadeDTO) {
      $dblIdDocumento = $objBloqueioItemSessUnidadeDTO->getDblIdDocumento();
      if ($dblIdDocumento!=null) {
        $arrDocumentosBloqueados[$dblIdDocumento] = $dblIdDocumento;
      } else {
        $dblIdProcedimento = $objBloqueioItemSessUnidadeDTO->getDblIdProcedimentoDistribuicao();
        $arrProcessosBloqueados[$dblIdProcedimento] = $dblIdProcedimento;
      }
    }

    if ($bolSigiloso) {
      //busca colegiados do usuario atual
      $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
      $objColegiadoComposicaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objColegiadoComposicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objColegiadoComposicaoDTO->setStrSinHabilitado('S');
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $arrIdColegiadosUsuario = InfraArray::converterArrInfraDTO($objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO), 'IdColegiadoColegiadoVersao');

    }

    $arrSessaoSituacaoPermitidaSigilosos = array(SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_SUSPENSA);

    foreach ($arrProcedimentos as $dblIdProcedimento => $strStaNivelAcesso) {
      if (isset($arrObjItemSessaoJulgamentoDTO[$dblIdProcedimento]) && !isset($arrProcessosBloqueados[$dblIdProcedimento])) {
        if ($strStaNivelAcesso==ProtocoloRN::$NA_RESTRITO) {
          $ret[$dblIdProcedimento] = SeiIntegracao::$TAM_PERMITIDO;
        } else if ($strStaNivelAcesso==ProtocoloRN::$NA_SIGILOSO) {
          foreach ($arrObjItemSessaoJulgamentoDTO[$dblIdProcedimento] as $objItemSessaoJulgamentoDTO) {
            //acesso do membro titular ou habilitado
            if (in_array($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento(), $arrIdColegiadosUsuario)
                && in_array($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento(), $arrSessaoSituacaoPermitidaSigilosos)) {
              $ret[$dblIdProcedimento] = SeiIntegracao::$TAM_PERMITIDO;
              break;
            }
          }
        }
      }
    }


    /** @var DocumentoAPI $objDocumentoAPI */
    foreach ($arrObjDocumentoAPI as $objDocumentoAPI) {
      //se processo estiver com bloqueio n�o libera os documentos tamb�m
      if (isset($arrProcessosBloqueados[$objDocumentoAPI->getIdProcedimento()])) {
        continue;
      }
      if (!isset($arrObjItemSessaoDocumentoDTO[$objDocumentoAPI->getIdDocumento()])) {
        continue;
      }

      if (!isset($arrDocumentosBloqueados[$objDocumentoAPI->getIdDocumento()])) {
        if ($objDocumentoAPI->getNivelAcesso()!=ProtocoloRN::$NA_SIGILOSO) {
          $ret[$objDocumentoAPI->getIdDocumento()] = SeiIntegracao::$TAM_PERMITIDO;
        } else {
          $arrObjItemSessaoDocumentoDTO2 = $arrObjItemSessaoDocumentoDTO[$objDocumentoAPI->getIdDocumento()];
          foreach ($arrObjItemSessaoDocumentoDTO2 as $objItemSessaoDocumentoDTO) {
            //secretaria
            //documento disponibilizado em sess�o aberta ou suspensa (de colegiado administrado) permite acesso ao documento
            if ($bolAdministradorSessao &&
                in_array($objItemSessaoDocumentoDTO->getNumIdColegiadoSessaoJulgamento(), $arrIdColegiadosAdministrados) &&
                in_array($objItemSessaoDocumentoDTO->getStrStaSituacaoSessaoJulgamento(), array(SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_SUSPENSA))) {
              $ret[$objDocumentoAPI->getIdDocumento()] = SeiIntegracao::$TAM_PERMITIDO;
              break;
            }
            //membro habilitado no colegiado que foi disponibilizado o documento e sess�o em situa��o permitida (pauta fechada, aberta, suspensa)
            if (in_array($objItemSessaoDocumentoDTO->getNumIdColegiadoSessaoJulgamento(), $arrIdColegiadosUnidade) &&
                in_array($objItemSessaoDocumentoDTO->getStrStaSituacaoSessaoJulgamento(), $arrSessaoSituacaoPermitidaSigilosos)) {
              $ret[$objDocumentoAPI->getIdDocumento()] = SeiIntegracao::$TAM_PERMITIDO;
              break;
            }
          }
        }
      }
    }

    return $ret;
  }

  public function permitirAndamentoConcluido(AndamentoAPI $objAndamentoAPI): bool
  {
    if ($objAndamentoAPI->getIdTarefa()==TarefaRN::$TI_GERACAO_DOCUMENTO && SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar')) {

      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_ENCERRADA),InfraDTO::$OPER_IN);
      $objItemSessaoJulgamentoDTO->setDblIdProcedimentoDistribuicao($objAndamentoAPI->getIdProtocolo());
      $objItemSessaoJulgamentoDTO->setNumMaxRegistrosRetorno(1);

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      if ($objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO) != null) {
        return true;
      }
    }
    return false;
  }

  public function montarMenuUsuarioExterno(): ?array
  {

    $objRelColegiadoUsuarioDTO=new RelColegiadoUsuarioDTO();
    $objRelColegiadoUsuarioRN=new RelColegiadoUsuarioRN();
    $objRelColegiadoUsuarioDTO->setNumIdUsuario(SessaoSEIExterna::getInstance()->getNumIdUsuarioExterno());
    $objRelColegiadoUsuarioDTO->retNumIdColegiado();
    $arrObjRelColegiadoUsuarioDTO=$objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO);

    if(InfraArray::contar($arrObjRelColegiadoUsuarioDTO)>0){
      $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTO->setNumIdColegiado(InfraArray::converterArrInfraDTO($arrObjRelColegiadoUsuarioDTO,'IdColegiado'),InfraDTO::$OPER_IN);
      $objSessaoJulgamentoDTO->setStrStaSituacao(array(SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_SUSPENSA),InfraDTO::$OPER_IN);
      if($objSessaoJulgamentoRN->contar($objSessaoJulgamentoDTO)>0){
        $arrMenu = array();
        $arrMenu[] = '-^controlador_externo.php?acao=sessao_julgamento_observar^^Sess�es de Julgamento^';
        return $arrMenu;
      }
    }
    return null;

  }

  public function excluirUsuario($arrObjUsuarioAPI){

    if (InfraArray::contar($arrObjUsuarioAPI)==0) {
      return null;
    }
    
    $arrIdUsuario = array();
    foreach($arrObjUsuarioAPI as $objUsuarioAPI){
      $arrIdUsuario[] = $objUsuarioAPI->getIdUsuario();
    }
    
    $objRelColegiadoUsuarioDTO=new RelColegiadoUsuarioDTO();
    $objRelColegiadoUsuarioRN=new RelColegiadoUsuarioRN();
    
    $objRelColegiadoUsuarioDTO->setNumIdUsuario($arrIdUsuario,InfraDTO::$OPER_IN);
    $objRelColegiadoUsuarioDTO->retStrSiglaUsuario();
    $objRelColegiadoUsuarioDTO->retStrSiglaColegiado();
    $arrObjRelColegiadoUsuarioDTO=$objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO);

    $objInfraException=new InfraException();
    foreach ($arrObjRelColegiadoUsuarioDTO as $objRelColegiadoUsuarioDTO) {
      $objInfraException->adicionarValidacao('O usu�rio "'.$objRelColegiadoUsuarioDTO->getStrSiglaUsuario().'" � observador no Colegiado "'.$objRelColegiadoUsuarioDTO->getStrSiglaColegiado().'".');
    }

    $objInfraException->lancarValidacoes();

    return null;
  }

  public function processarControlador($strAcao): bool
  {

    switch($strAcao) {

      case 'ausencia_sessao_registrar':
        require_once __DIR__.'/ausencia_sessao_cadastro.php';
        return true;

      case 'destaque_listar':
      case 'destaque_excluir':
      case 'destaque_marcar_leitura':
      case 'destaque_marcar_leitura_todos':
      case 'destaque_desmarcar_leitura':
        require_once __DIR__.'/destaque_lista.php';
        return true;
      case 'destaque_cadastrar':
      case 'destaque_alterar':
        require_once __DIR__.'/destaque_cadastro.php';
        return true;

      case 'julgamento_configurar':
        require_once __DIR__ . '/MdJulgarConfiguracao.php';
        return true;

      case 'voto_parte_cadastrar':
        require_once __DIR__.'/voto_parte_cadastro.php';
        return true;

      case 'voto_parte_alterar_ressalva':
        require_once __DIR__.'/voto_parte_alterar_ressalva.php';
        return true;

      case 'pedido_vista_devolver_secretaria':
        require_once __DIR__.'/pedido_vista_devolucao.php';
        return true;
      case 'colegiado_cadastrar':
      case 'colegiado_alterar':
      case 'colegiado_consultar':
        require_once __DIR__.'/colegiado_cadastro.php';
        return true;
      case 'colegiado_listar':
      case 'colegiado_excluir':
      case 'colegiado_desativar':
      case 'colegiado_reativar':
      case 'colegiado_selecionar':
        require_once __DIR__.'/colegiado_lista.php';
        return true;

      case 'julgamento_parte_cadastrar':
        require_once __DIR__.'/julgamento_parte_cadastro.php';
        return true;
      case 'julgamento_parte_desempatar':
        require_once __DIR__.'/julgamento_parte_desempate.php';
        return true;

      case 'documento_escolher_tipo_julgamento':
        require_once __DIR__.'/documento_escolher_tipo_julgamento.php';
        return true;

      case 'doc_sessao_julgamento_disponibilizar':
      case 'provimento_disponibilizado_alterar':
        require_once __DIR__ . '/documento_disponibilizar.php';
        return true;

      case 'doc_sessao_julgamento_indisponibilizar':
        require_once __DIR__ . '/documento_indisponibilizar.php';
        return true;

      case 'item_sessao_julgamento_retirar':
      case 'item_sessao_julgamento_adiar':
        require_once __DIR__ . '/item_sessao_julgamento_retirada.php';
        return true;

      case 'item_sessao_julgamento_ordenar':
        require_once __DIR__ . '/item_sessao_julgamento_ordenar.php';
        return true;

      case 'item_sessao_julgamento_julgar':
        require_once __DIR__ . '/item_sessao_julgamento_julgar.php';
        return true;

      case 'item_sessao_julgamento_manual':
        require_once __DIR__ . '/item_sessao_julgamento_manual.php';
        return true;

      case 'item_sessao_julgamento_julgar_multiplo':
        require_once __DIR__ . '/item_sessao_julgamento_multiplo.php';
        return true;

      case 'item_sessao_julgamento_finalizar':
        require_once __DIR__ . '/item_sessao_julgamento_finalizar.php';
        return true;
      case 'sessao_julgamento_cadastrar':
      case 'sessao_julgamento_alterar':
        require_once __DIR__.'/sessao_julgamento_cadastro.php';
        return true;
      case 'sessao_julgamento_historico':
        require_once __DIR__.'/sessao_julgamento_historico.php';
        return true;
      case 'sessao_julgamento_alterar_presidente':
        require_once __DIR__.'/sessao_julgamento_alterar_presidente.php';
        return true;

      case 'sessao_julgamento_listar':
      case 'sessao_julgamento_excluir':
      case 'sessao_julgamento_selecionar':
        require_once __DIR__.'/sessao_julgamento_lista.php';
        return true;

      case 'motivo_distribuicao_cadastrar':
      case 'motivo_distribuicao_alterar':
      case 'motivo_distribuicao_consultar':
        require_once __DIR__.'/motivo_distribuicao_cadastro.php';
        return true;

      case 'motivo_distribuicao_listar':
      case 'motivo_distribuicao_excluir':
      case 'motivo_distribuicao_selecionar':
      case 'motivo_distribuicao_desativar':
      case 'motivo_distribuicao_reativar':
        require_once __DIR__.'/motivo_distribuicao_lista.php';
        return true;
      
      case 'motivo_ausencia_cadastrar':
      case 'motivo_ausencia_alterar':
      case 'motivo_ausencia_consultar':
        require_once __DIR__.'/motivo_ausencia_cadastro.php';
        return true;

      case 'motivo_ausencia_listar':
      case 'motivo_ausencia_excluir':
      case 'motivo_ausencia_selecionar':
      case 'motivo_ausencia_desativar':
      case 'motivo_ausencia_reativar':
        require_once __DIR__.'/motivo_ausencia_lista.php';
        return true;
      case 'motivo_canc_distribuicao_cadastrar':
      case 'motivo_canc_distribuicao_alterar':
      case 'motivo_canc_distribuicao_consultar':
        require_once __DIR__.'/motivo_canc_distribuicao_cadastro.php';
        return true;

      case 'motivo_canc_distribuicao_listar':
      case 'motivo_canc_distribuicao_excluir':
      case 'motivo_canc_distribuicao_selecionar':
      case 'motivo_canc_distribuicao_desativar':
      case 'motivo_canc_distribuicao_reativar':
        require_once __DIR__.'/motivo_canc_distribuicao_lista.php';
        return true;

      case 'motivo_mesa_cadastrar':
      case 'motivo_mesa_alterar':
      case 'motivo_mesa_consultar':
        require_once __DIR__.'/motivo_mesa_cadastro.php';
        return true;

      case 'motivo_mesa_listar':
      case 'motivo_mesa_excluir':
      case 'motivo_mesa_selecionar':
      case 'motivo_mesa_desativar':
      case 'motivo_mesa_reativar':
        require_once __DIR__.'/motivo_mesa_lista.php';
        return true;

      case 'sessao_julgamento_pautar':
        require_once __DIR__.'/item_sessao_julgamento_cadastro.php';
        return true;

      case 'item_sessao_julgamento_diligencia_cancelar':
      case 'item_sessao_julgamento_adiamento_cancelar':
      case 'item_sessao_julgamento_diligenciar':
        require_once __DIR__.'/confirmacao.php';
        return true;


      case 'presenca_sessao_registrar':
      case 'sessao_julgamento_acompanhar':
      case 'sessao_julgamento_consultar':
      case 'sessao_julgamento_cancelar':
      case 'sessao_julgamento_encerrar':
      case 'sessao_julgamento_finalizar':
        require_once __DIR__.'/sessao_julgamento_abertura.php';
        return true;

      case 'colegiado_versao_listar':
      case 'colegiado_versao_excluir':
      case 'colegiado_versao_selecionar':
        require_once __DIR__.'/colegiado_versao_lista.php';
        return true;

      case 'distribuicao_gerar':
        require_once __DIR__.'/distribuicao_geracao.php';
        return true;
      case 'distribuicao_cancelar':
        require_once __DIR__.'/distribuicao_cancelar.php';
        return true;
      case 'distribuicao_simular':
        require_once __DIR__.'/distribuicao_simular.php';
        return true;
      case 'distribuicao_listar':
        require_once __DIR__.'/distribuicao_lista.php';
        return true;

      case 'julgamento_listar':
        require_once __DIR__.'/procedimento_julgamento_lista.php';
        return true;

      case 'provimento_cadastrar':
      case 'provimento_alterar':
      case 'provimento_consultar':
        require_once __DIR__.'/provimento_cadastro.php';
        return true;

      case 'provimento_listar':
      case 'provimento_excluir':
      case 'provimento_selecionar':
      case 'provimento_desativar':
      case 'provimento_reativar':
        require_once __DIR__.'/provimento_lista.php';
        return true;

      case 'painel_distribuicao_listar':
      case 'painel_distribuicao_gerar_grafico':
        require_once __DIR__.'/painel_distribuicao.php';
        return true;
      case 'painel_distribuicao_detalhar':
        require_once __DIR__.'/painel_distribuicao_detalhe.php';
        return true;
      case 'relatorio_distribuicao':
        require_once __DIR__.'/relatorio_distribuicao.php';
        return true;
      case 'resumo_pauta':
        require_once __DIR__.'/resumo_pauta.php';
        return true;
      case 'unidade_selecionar_colegiado':
        require_once __DIR__.'/unidade_colegiado_selecao.php';
        return true;
      case 'procedimento_autuacao_consultar':
      case 'procedimento_autuacao_gerenciar':
        require_once __DIR__.'/procedimento_autuacao.php';
        return true;

      case 'revisao_item_cadastrar':
        require_once __DIR__.'/revisao_item_cadastro.php';
        return true;

      case 'sustentacao_oral_consultar':
      case 'sustentacao_oral_cadastrar':
      case 'sustentacao_oral_excluir':
        require_once __DIR__.'/sustentacao_oral_cadastro.php';
        return true;

      case 'tipo_materia_cadastrar':
      case 'tipo_materia_consultar':
      case 'tipo_materia_alterar':
        require_once __DIR__.'/tipo_materia_cadastro.php';
        return true;
      case 'tipo_materia_listar':
      case 'tipo_materia_excluir':
      case 'tipo_materia_selecionar':
      case 'tipo_materia_desativar':
      case 'tipo_materia_reativar':
        require_once __DIR__.'/tipo_materia_lista.php';
        return true;
      case 'qualificacao_parte_cadastrar':
      case 'qualificacao_parte_consultar':
      case 'qualificacao_parte_alterar':
        require_once __DIR__.'/qualificacao_parte_cadastro.php';
        return true;
      case 'qualificacao_parte_listar':
      case 'qualificacao_parte_excluir':
      case 'qualificacao_parte_selecionar':
      case 'qualificacao_parte_desativar':
      case 'qualificacao_parte_reativar':
        require_once __DIR__.'/qualificacao_parte_lista.php';
        return true;

      case 'bloqueio_item_sess_unidade_alterar':
        require_once __DIR__.'/item_sessao_restricao.php';
        return true;

      case 'eleicao_cadastrar':
      case 'eleicao_alterar':
      case 'eleicao_consultar':
      case 'eleicao_liberar':
      case 'eleicao_concluir':
      case 'eleicao_finalizar':
      case 'eleicao_anular':
        require_once __DIR__.'/eleicao_cadastro.php';
        return true;

      case 'eleicao_listar':
      case 'eleicao_excluir':
        require_once __DIR__.'/eleicao_lista.php';
        return true;

      case 'eleicao_clonar':
        require_once __DIR__.'/eleicao_clonar.php';
        return true;

      case 'eleicao_votar':
        require_once __DIR__.'/eleicao_votacao.php';
        return true;
      case 'tipo_sessao_cadastrar':
      case 'tipo_sessao_consultar':
      case 'tipo_sessao_alterar':
        require_once __DIR__.'/tipo_sessao_cadastro.php';
        return true;
      case 'tipo_sessao_listar':
      case 'tipo_sessao_excluir':
      case 'tipo_sessao_selecionar':
      case 'tipo_sessao_desativar':
      case 'tipo_sessao_reativar':
        require_once __DIR__.'/tipo_sessao_lista.php';
        return true;
    }
    return false;
  }

  /**
   * @param $strAcaoAjax
   * @return string|null
   * @throws InfraException
   */
  public function processarControladorAjax($strAcaoAjax): ?string
  {

    $xml = null;

    switch($strAcaoAjax) {

      case 'usuario_unidade_julgamento':
        $strOptions = UsuarioINT::montarSelectNomePorUnidade('null',' ',$_POST['IdUsuario'],$_POST['IdUnidade']);
        $xml = InfraAjax::gerarXMLSelect($strOptions);
        break;

      case 'sessao_julgamento':
        $staSessao='';
        $strOptions='';

        if (isset($_POST['StaSituacao'])){
          $staSessao=$_POST['StaSituacao'];
        }
        $idColegiado=$_POST['IdColegiado'];

        if ($idColegiado!='') {
          if (isset($_POST['BolInclusao']) && $_POST['BolInclusao']=='true'){
            $strOptions = SessaoJulgamentoINT::montarSelectDthSessaoInclusao('null', ' ', $_POST['IdSessaoJulgamento'], $idColegiado);
          } else {
            $strOptions = SessaoJulgamentoINT::montarSelectDthSessao('null', ' ', $_POST['IdSessaoJulgamento'], $idColegiado, $staSessao);
          }
        }
        $xml = InfraAjax::gerarXMLSelect($strOptions);
        break;

      case 'eventos_sessao':
        $objEventoSessaoDTO=new EventoSessaoDTO();
        $objEventoSessaoDTO->setNumIdSessaoJulgamento($_GET['id_sessao_julgamento']);
        $objEventoSessaoDTO->setNumSeqEvento($_POST['UltimoEvento']);
        $json=EventoSessaoRN::listarJSON($objEventoSessaoDTO);

        InfraAjax::enviarJSON($json);
        die;

      case 'motivo_distribuicao_auto_completar':
        $arrObjRelMotivoDistrColegiadoDTO=RelMotivoDistrColegiadoINT::autoCompletarRelMotivoDistribuicaoColegiado($_POST['id_colegiado'],$_POST['sta_tipo'],$_POST['palavras_pesquisa']);
        $xml = InfraAjax::gerarXMLItensArrInfraDTO($arrObjRelMotivoDistrColegiadoDTO,'IdMotivoDistribuicao', 'DescricaoMotivoDistribuicao');
        break;

      case 'dispositivo_item':
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_POST['id_item_sessao_julgamento']);
        $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
        $objItemSessaoJulgamentoDTO->retStrDispositivo();
        $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
        if($objItemSessaoJulgamentoDTO){
          $objItemSessaoJulgamentoDTO->setStrDispositivo(base64_encode($objItemSessaoJulgamentoDTO->getStrDispositivo()));
        }
        $xml=InfraAjax::gerarXMLComplementosArrInfraDTO($objItemSessaoJulgamentoDTO,array('IdItemSessaoJulgamento','Dispositivo'));
        break;
    }
    return $xml;
  }

  public function processarControladorExterno($strAcao): bool
  {

    switch($strAcao) {

      case 'sessao_julgamento_observar':
        require_once __DIR__.'/observador_sessao_julgamento.php';
        return true;
      case 'documento_sessao_consulta_externa':
        require_once __DIR__.'/observador_documento_sessao.php';
        return true;
      case 'pauta_sessao_listar':
        require_once __DIR__.'/pauta_sessao_listar.php';
        return true;
      case 'pauta_sessao_visualizar':
        require_once __DIR__.'/pauta_sessao_visualizar.php';
        return true;
    }
    return false;
  }

  public function atualizarConteudoDocumento(DocumentoAPI $objDocumentoAPI){
    $objColegiadoRN=new ColegiadoRN();
    if (SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') || $objColegiadoRN->verificaUnidadeAtual(new ColegiadoDTO())) {
      $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
      if ($objItemSessaoDocumentoRN->verificaDocumentoBloqueado($objDocumentoAPI)) {
        $objInfraException = new InfraException();
        $objInfraException->lancarValidacao('Documento em uso em sess�o de julgamento');
      }
    }
  }

  public function processarVariaveisEditor(DocumentoAPI $objDocumentoAPI): ?array
  {
    if (!SessaoSEI::getInstance()->verificarPermissao('distribuicao_consultar')) {
      return null;
    }

    if($objDocumentoAPI->getIdProcedimento()==null){
      return null;
    }

    $ret=array();

    $objDistribuicaoRN=new DistribuicaoRN();
    $objDistribuicaoDTO=new DistribuicaoDTO();
    $objDistribuicaoDTO->setDblIdProcedimento($objDocumentoAPI->getIdProcedimento());
    $objDistribuicaoDTO->setStrStaUltimo('N',InfraDTO::$OPER_DIFERENTE);
    $objDistribuicaoDTO->setOrdDthSituacaoAtual(InfraDTO::$TIPO_ORDENACAO_DESC);
    $objDistribuicaoDTO->setNumMaxRegistrosRetorno(1);

    $objDistribuicaoDTO->retNumIdUsuarioRelator();
    $objDistribuicaoDTO->retNumIdUsuarioRelatorAcordao();
    $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
    $objDistribuicaoDTO->retNumIdColegiadoVersao();
    $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);

    if($objDistribuicaoDTO==null) {
      return null;
    }

    $objColegiadoRN=new ColegiadoRN();
    $objColegiadoDTO=new ColegiadoDTO();
    $objColegiadoDTO->setNumIdColegiado($objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao());
    $objColegiadoDTO->retStrNome();
    $objColegiadoDTO->retStrSigla();
    $objColegiadoDTO->retNumIdUsuarioPresidente();
    $objColegiadoDTO->retNumIdUsuarioSecretario();
    $objColegiadoDTO->retStrNomeSecretario();
    $objColegiadoDTO->retStrSiglaSecretario();
    $objColegiadoDTO=$objColegiadoRN->consultar($objColegiadoDTO);

    $ret['md_julgar_sigla_colegiado']=$objColegiadoDTO->getStrSigla();
    $ret['md_julgar_nome_colegiado']=$objColegiadoDTO->getStrNome();
    $ret['md_julgar_sigla_secretario_colegiado']=$objColegiadoDTO->getStrSiglaSecretario();
    $ret['md_julgar_nome_secretario_colegiado']=$objColegiadoDTO->getStrNomeSecretario();


    $arrIdUsuario=array();
    $arrIdUsuario[$objColegiadoDTO->getNumIdUsuarioPresidente()]=1;
    $arrIdUsuario[$objDistribuicaoDTO->getNumIdUsuarioRelator()]=1;
    if($objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao()!=null) {
      $arrIdUsuario[$objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao()] = 1;
    }

    $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao());
    $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
    $objColegiadoComposicaoDTO->setNumIdUsuario(array_keys($arrIdUsuario),InfraDTO::$OPER_IN);
    $objColegiadoComposicaoDTO->retNumIdUsuario();
    $objColegiadoComposicaoDTO->retStrNomeUsuario();
    $objColegiadoComposicaoDTO->retStrSiglaUsuario();
    $objColegiadoComposicaoDTO->retStrSiglaUnidade();
    $objColegiadoComposicaoDTO->retStrDescricaoUnidade();
    $objColegiadoComposicaoDTO->retStrExpressaoCargo();
    $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
    $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUsuario');

    $objColegiadoComposicaoDTO=$arrObjColegiadoComposicaoDTO[$objDistribuicaoDTO->getNumIdUsuarioRelator()];
    $ret['md_julgar_sigla_relator']=$objColegiadoComposicaoDTO->getStrSiglaUsuario();
    $ret['md_julgar_nome_relator']=$objColegiadoComposicaoDTO->getStrNomeUsuario();
    $ret['md_julgar_cargo_relator']=$objColegiadoComposicaoDTO->getStrExpressaoCargo();
    $ret['md_julgar_sigla_unidade_relator']=$objColegiadoComposicaoDTO->getStrSiglaUnidade();
    $ret['md_julgar_descricao_unidade_relator']=$objColegiadoComposicaoDTO->getStrDescricaoUnidade();

    if($objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao()!=null){
      $objColegiadoComposicaoDTO=$arrObjColegiadoComposicaoDTO[$objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao()];
    }
    $ret['md_julgar_sigla_relator_acordao']=$objColegiadoComposicaoDTO->getStrSiglaUsuario();
    $ret['md_julgar_nome_relator_acordao']=$objColegiadoComposicaoDTO->getStrNomeUsuario();
    $ret['md_julgar_cargo_relator_acordao']=$objColegiadoComposicaoDTO->getStrExpressaoCargo();
    $ret['md_julgar_sigla_unidade_relator_acordao']=$objColegiadoComposicaoDTO->getStrSiglaUnidade();
    $ret['md_julgar_descricao_unidade_relator_acordao']=$objColegiadoComposicaoDTO->getStrDescricaoUnidade();

    $objColegiadoComposicaoDTO=$arrObjColegiadoComposicaoDTO[$objColegiadoDTO->getNumIdUsuarioPresidente()];
    $ret['md_julgar_sigla_presidente_colegiado']=$objColegiadoComposicaoDTO->getStrSiglaUsuario();
    $ret['md_julgar_nome_presidente_colegiado']=$objColegiadoComposicaoDTO->getStrNomeUsuario();
    $ret['md_julgar_cargo_presidente_colegiado']=$objColegiadoComposicaoDTO->getStrExpressaoCargo();

    return $ret;
  }

  public function obterRelacaoVariaveisEditor(): array
  {

    $ret=array();
    $ret['md_julgar_sigla_colegiado']='Sigla do Colegiado';
    $ret['md_julgar_nome_colegiado']='Nome do Colegiado';
    $ret['md_julgar_cargo_presidente_colegiado']='Cargo do Presidente do Colegiado';
    $ret['md_julgar_sigla_presidente_colegiado']='Sigla do Presidente do Colegiado';
    $ret['md_julgar_nome_presidente_colegiado']='Nome do Presidente do Colegiado';
    $ret['md_julgar_sigla_secretario_colegiado']='Sigla do Secret�rio do Colegiado';
    $ret['md_julgar_nome_secretario_colegiado']='Nome do Secret�rio do Colegiado';
    $ret['md_julgar_sigla_relator']='Sigla do Relator';
    $ret['md_julgar_nome_relator']='Nome do Relator';
    $ret['md_julgar_cargo_relator']='Cargo do Relator';
    $ret['md_julgar_sigla_unidade_relator']='Sigla da Unidade do Relator';
    $ret['md_julgar_descricao_unidade_relator']='Descri��o da Unidade do Relator';
    $ret['md_julgar_sigla_relator_acordao']='Sigla do Relator do Ac�rd�o';
    $ret['md_julgar_nome_relator_acordao']='Nome do Relator do Ac�rd�o';
    $ret['md_julgar_cargo_relator_acordao']='Cargo do Relator do Ac�rd�o';
    $ret['md_julgar_sigla_unidade_relator_acordao']='Sigla da Unidade do Relator do Ac�rd�o';
    $ret['md_julgar_descricao_unidade_relator_acordao']='Descri��o da Unidade do Relator do Ac�rd�o';

    return $ret;
  }

  public function obterAcoesExternasSemLogin(): array
  {
    return array('pauta_sessao_listar','pauta_sessao_visualizar');
  }

  public function excluirProcesso(ProcedimentoAPI $objProcedimentoAPI)
  {
    $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
    $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO->setDblIdProcedimento($objProcedimentoAPI->getIdProcedimento());
    $objSessaoJulgamentoDTO->retStrSiglaColegiado();
    $objSessaoJulgamentoDTO->retDthSessao();
    $objSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
    $objSessaoJulgamentoDTO=$objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);

    if($objSessaoJulgamentoDTO){
      $objInfraException=new InfraException();
      $objInfraException->lancarValidacao('O processo "'.$objSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo().
          '" est� vinculado � sess�o de julgamento do dia "'.$objSessaoJulgamentoDTO->getDthSessao().
          '" do Colegiado "'.$objSessaoJulgamentoDTO->getStrSiglaColegiado().'".');
    }

    //excluir autua��o e partes do processo
    $objParteProcedimentoDTO=new ParteProcedimentoDTO();
    $objParteProcedimentoRN=new ParteProcedimentoRN();
    $objParteProcedimentoDTO->setDblIdProcedimento($objProcedimentoAPI->getIdProcedimento());
    $objParteProcedimentoDTO->retTodos();
    $arrObjParteProcedimentoDTO=$objParteProcedimentoRN->listar($objParteProcedimentoDTO);
    if($arrObjParteProcedimentoDTO){
      $objParteProcedimentoRN->excluir($arrObjParteProcedimentoDTO);
    }

    $objAutuacaoDTO=new AutuacaoDTO();
    $objAutuacaoRN=new AutuacaoRN();
    $objAutuacaoDTO->setDblIdProcedimento($objProcedimentoAPI->getIdProcedimento());
    $objAutuacaoDTO->retTodos();
    $objAutuacaoDTO=$objAutuacaoRN->consultar($objAutuacaoDTO);
    if($objAutuacaoDTO){
      $objAutuacaoRN->excluir(array($objAutuacaoDTO));
    }

    return null;
  }

  public function inicializar($strVersaoSEI)
  {
    if (InfraUtil::compararVersoes(SEI_VERSAO, '<', '4.0.0')){
      die('SEI! Julgar: VERSAO DO SEI DEVE SER IGUAL OU SUPERIOR A 4.0.0');
    }
  }
  public function tratarLinkSemAssinatura($strLink): bool
  {
    return preg_match('/^controlador.php\?acao=sessao_julgamento_acompanhar&id_sessao_julgamento=\d+$/', $strLink)===1;
  }

}
?>