<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class ParteProcedimentoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'parte_procedimento';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdParteProcedimento','id_parte_procedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL,'IdProcedimento','id_procedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdContato','id_contato');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdQualificacaoParte','id_qualificacao_parte');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidade','id_unidade');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeContato','nome','contato');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoQualificacaoParte','descricao','qualificacao_parte');

    $this->configurarPK('IdParteProcedimento',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdQualificacaoParte', 'qualificacao_parte', 'id_qualificacao_parte');
    $this->configurarFK('IdContato', 'contato', 'id_contato');
  }
}
?>