<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  ////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);



  $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
  $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();

  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '';
  $mensagem='';
  if(isset($_GET['arvore'])){
    $strParametros .= '&arvore='.$_GET['arvore'];
  }
  if (isset($_GET['id_procedimento'])){
    $strParametros .= '&id_procedimento=' .$_GET['id_procedimento'];
  }
  if (isset($_GET['id_documento'])){
    $strParametros .= '&id_documento=' .$_GET['id_documento'];
  }
  if (isset($_GET['id_sessao_julgamento'])){
    $strParametros.='&id_sessao_julgamento='.$_GET['id_sessao_julgamento'];
  }
  if (isset($_GET['id_item_sessao_julgamento'])){
    $strParametros.='&id_item_sessao_julgamento='.$_GET['id_item_sessao_julgamento'];
  }
  if (isset($_GET['id_colegiado'])){
    $strParametros.='&id_colegiado='.$_GET['id_colegiado'];
  }
  if (isset($_GET['id_distribuicao'])){
    $strParametros.='&id_distribuicao='.$_GET['id_distribuicao'];
  }
  $arrComandos = array();
  $arrIdItemSessaoJulgamento = explode(',', $_GET['id_item_sessao_julgamento']);
  $bolMultiplasSessoes = count($arrIdItemSessaoJulgamento)>1;
  if (isset($_POST['selItemSessaoJulgamento'])){
    $numIdItemSessaoJulgamento=$_POST['selItemSessaoJulgamento'];
  } else if($bolMultiplasSessoes){
    $numIdItemSessaoJulgamento = null;
  } else {
    $numIdItemSessaoJulgamento=$arrIdItemSessaoJulgamento[0];
  }


  $bolExecucaoOK=false;
  switch($_GET['acao']){
    case 'doc_sessao_julgamento_indisponibilizar':
      $strTitulo = 'Cancelar Disponibiliza��o do Documento para Sess�o de Julgamento';
      if($numIdItemSessaoJulgamento) {
        $arrComandos[] = '<button type="submit" accesskey="" name="sbmIndisponibilizar" value="Salvar" class="infraButton">Confirmar</button>';
        $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
        if (isset($_POST['sbmIndisponibilizar'])) {
          try {
            $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
            $objItemSessaoDocumentoDTO->setDblIdDocumento($_GET['id_documento']);
            $objItemSessaoDocumentoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
            $objItemSessaoDocumentoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_CANCELADA, SessaoJulgamentoRN::$ES_ENCERRADA, SessaoJulgamentoRN::$ES_FINALIZADA), InfraDTO::$OPER_NOT_IN);
            $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
            $objItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->consultar($objItemSessaoDocumentoDTO);
            if ($objItemSessaoDocumentoDTO==null) {
              throw new InfraException('Documento n�o est� disponibilizado na sess�o');
            }
            $objItemSessaoDocumentoRN->indisponibilizar($objItemSessaoDocumentoDTO);
            PaginaSEI::getInstance()->adicionarMensagem('Documento indisponibilizado com sucesso.');
            header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=arvore_visualizar&acao_origem=' . $_GET['acao'] . $strParametros . '&atualizar_arvore=1'));
            die;

          } catch (Exception $e) {
            PaginaSEI::getInstance()->processarExcecao($e);
          }
        }
      } else {
        $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento,InfraDTO::$OPER_IN);
      }
      break;

    default:
      throw new InfraException("A��o '${_GET['acao']}' n�o reconhecida.");
  }

  $objItemSessaoDocumentoDTO->setDblIdDocumento($_GET['id_documento']);
  $objItemSessaoDocumentoDTO->retTodos();
  $objItemSessaoDocumentoDTO->retStrNomeColegiado();
  $objItemSessaoDocumentoDTO->retStrStaSituacaoSessaoJulgamento();
  $objItemSessaoDocumentoDTO->retStrSinVirtualTipoSessao();
  $objItemSessaoDocumentoDTO->retDthSessaoSessaoJulgamento();



  $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
  $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
  $objItemSessaoDocumentoDTO=null;
  $arrMensagem=[];
  foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTOBanco) {
    $strSessao=$objItemSessaoDocumentoDTOBanco->getStrNomeColegiado().' - '.substr($objItemSessaoDocumentoDTOBanco->getDthSessaoSessaoJulgamento(),0,16);
    $objItemSessaoDocumentoDTOBanco->setStrNomeColegiado($strSessao);
    if($numIdItemSessaoJulgamento==$objItemSessaoDocumentoDTOBanco->getNumIdItemSessaoJulgamento()){
      $objItemSessaoDocumentoDTO=$objItemSessaoDocumentoDTOBanco;
    }
  }
  if ($bolMultiplasSessoes) {
    $strItensSelSessaoJulgamento = InfraINT::montarSelectArrInfraDTO('null', '&nbsp;', $numIdItemSessaoJulgamento, $arrObjItemSessaoDocumentoDTO, 'IdItemSessaoJulgamento', 'NomeColegiado');
  } else {
    $strItensSelSessaoJulgamento = InfraINT::montarSelectArrInfraDTO(null, '', $numIdItemSessaoJulgamento, $arrObjItemSessaoDocumentoDTO, 'IdItemSessaoJulgamento', 'NomeColegiado');
  }
  $strMensagem='';
  if($objItemSessaoDocumentoDTO && $objItemSessaoDocumentoDTO->getStrSinVirtualTipoSessao()=='S' && $objItemSessaoDocumentoDTO->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_ABERTA){
    $strMensagem='ATEN��O: N�o ser� poss�vel disponibilizar novamente o documento. Confirma?';
  }

  $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}
$strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&montar_visualizacao=1'.$strParametros);
$strLinkArvore=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_visualizar&acao_origem='.$_GET['acao'].$strParametros.'&montar_visualizacao=1');

$acaoRetorno=PaginaSEI::getInstance()->getAcaoRetorno();


PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
  #lblAviso {position:absolute;left:0%;top:0%;width:95%;}
  #lblItemSessaoJulgamento {position:absolute;left:0%;top:0%;width:50%;}
  #selItemSessaoJulgamento {position:absolute;left:0%;top:38%;width:50%;}
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
function inicializar(){

}
function onSubmitForm(){
  return true;
}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmItemSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblItemSessaoJulgamento" for="selItemSessaoJulgamento" accesskey="" class="infraLabelOpcional">Sess�o de Julgamento:</label>
  <select id="selItemSessaoJulgamento" name="selItemSessaoJulgamento" onchange="this.form.submit();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelSessaoJulgamento ?>
  </select>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('10em');
?>

  <label id="lblAviso" class="infraLabelObrigatorio" style="color:red;"><?=$strMensagem?></label>

  <?
  PaginaSEI::getInstance()->fecharAreaDados();

  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>