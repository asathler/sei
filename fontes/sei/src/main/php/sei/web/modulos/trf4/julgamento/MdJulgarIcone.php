<?

require_once __DIR__ .'/../../../SEI.php';

abstract class MdJulgarIcone
{

  public const VERSAO = '5';

  public const ACESSO_RESTRITO1 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/acesso_restrito1.svg?'.self::VERSAO;
  public const ACESSO_RESTRITO2 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/acesso_restrito2.svg?'.self::VERSAO;
  public const ADIAR_JULGAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/adiar_julgamento.svg?'.self::VERSAO;
  public const ADIAR_JULGAMENTO_CANCELAR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/adiar_julgamento_cancelar.svg?'.self::VERSAO;
  public const ATUALIZAR_DISPOSITIVO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/atualizar_dispositivo.svg?'.self::VERSAO;
  public const ATUALIZAR_ELEICOES = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/atualizar_eleicoes.svg?'.self::VERSAO;
  public const AUSENTE = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/ausente.svg?'.self::VERSAO;
  public const AUTUACAO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/autuacao.svg?'.self::VERSAO;
  public const BALAO1 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/balao1.svg?'.self::VERSAO;
  public const BALAO2 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/balao2.svg?'.self::VERSAO;
  public const CERTIDAO_DISTRIBUICAO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/certidao_distribuicao.svg?'.self::VERSAO;
  public const CERTIDAO_DISTRIBUICAO_CANCELAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/certidao_distribuicao_cancelamento.svg?'.self::VERSAO;
  public const SELECIONAR_RESTANTES = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/selecionar_restantes.svg?'.self::VERSAO;
  public const COLEGIADO_VERSOES = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/colegiado_versoes.svg?'.self::VERSAO;
  public const COMENTARIOS_INTERNOS = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/comentario_interno.svg?'.self::VERSAO;
  public const CONVERTER_DILIGENCIA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/converter_diligencia.svg?'.self::VERSAO;
  public const CONVERTER_DILIGENCIA_CANCELAR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/converter_diligencia_cancelar.svg?'.self::VERSAO;
  public const DESTAQUE_INCLUIR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_inclusao.svg?'.self::VERSAO;
  public const DESTAQUE_ABSTER = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_abster.svg?'.self::VERSAO;
  public const DESTAQUE_ACOMPANHA_RELATOR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_acomp_relator.svg?'.self::VERSAO;
  public const DESTAQUE_ACOMPANHA_DIVERGENCIA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_acomp_diverg.svg?'.self::VERSAO;
  public const DESTAQUE_COMENTARIO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_comentario.svg?'.self::VERSAO;
  public const DESTAQUE_DISCORDA_JULG_VIRT = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_disc_virtual.svg?'.self::VERSAO;
  public const DESTAQUE_DIVERGENCIA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_divergencia.svg?'.self::VERSAO;
  public const DESTAQUE_IMPEDIMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_impedimento.svg?'.self::VERSAO;
  public const DESTAQUE_RETIRADA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_retirada.svg?'.self::VERSAO;
  public const DESTAQUE_VISTA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_vista.svg?'.self::VERSAO;
  public const DESTAQUE_AGUARDA_VISTA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_aguarda_vista.svg?'.self::VERSAO;
  public const DESTAQUE_SUSTENTACAO_ORAL = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_sustentacao_oral.svg?'.self::VERSAO;
  public const DESTAQUE_RESSALVA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/destaque_ressalva.svg?'.self::VERSAO;

  public const DISPONIBILIZAR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/disponibilizar.svg?'.self::VERSAO;
  public const DISPONIBILIZAR_CANCELAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/disponibilizar_cancelamento.svg?'.self::VERSAO;
  public const DISTRIBUICAO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/distribuicao.svg?'.self::VERSAO;
  public const DISTRIBUICAO_CANCELAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/distribuicao_cancelamento.svg?'.self::VERSAO;
  public const DISTRIBUICAO_DEVOLVER = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/distribuicao_devolver.svg?'.self::VERSAO;
  public const ELEICAO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/eleicao.svg?'.self::VERSAO;
  public const FRACIONAR_JULGAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/fracionar_julgamento.svg?'.self::VERSAO;
  public const INCLUIR_DOCUMENTO_JULGAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/incluir_documento_julgamento.svg?'.self::VERSAO;
  public const JULGAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/julgamento.svg?'.self::VERSAO;
  public const MARTELO_AM = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/martelo_am.svg?'.self::VERSAO;
  public const MARTELO_AZ = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/martelo_az.svg?'.self::VERSAO;
  public const MARTELO_M_AM = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/martelo_m_am.svg?'.self::VERSAO;
  public const MARTELO_M_VD = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/martelo_m_vd.svg?'.self::VERSAO;
  public const MARTELO_PR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/martelo_pr.svg?'.self::VERSAO;
  public const MARTELO_VD = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/martelo_vd.svg?'.self::VERSAO;
  public const MARTELO_VM = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/martelo_vm.svg?'.self::VERSAO;
  public const MEDALHA = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/medalha.svg?'.self::VERSAO;
  public const ORDENAR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/ordenar.svg?'.self::VERSAO;
  public const PRESENTE = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/presente.svg?'.self::VERSAO;
  public const SESSAO_JULGAMENTO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/sessao_julgamento.svg?'.self::VERSAO;
  public const USUARIO1 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/usuario1.svg?'.self::VERSAO;
  public const USUARIO2 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/usuario2.svg?'.self::VERSAO;
  public const VERIFICACAO1 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/verificacao1.svg?'.self::VERSAO;
  public const VERIFICACAO2 = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/verificacao2.svg?'.self::VERSAO;
  public const VOTACAO_EXCESSO = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/votacao_excesso.svg?'.self::VERSAO;
  public const VOTAR = MdJulgarIntegracao::DIRETORIO_MODULO.'/svg/votar.svg?'.self::VERSAO;

  public const COR_VERMELHO='#ff0000';
  public const COR_VERDE='#00a500';
  public const COR_AZUL='#0056ff';

  public static function getConteudoIcone($strArquivo){
    $posVersao = strpos($strArquivo,'?');
    if ($posVersao!==false){
      $strArquivo = substr($strArquivo,0,$posVersao);
    }
    $localFile=str_replace(MdJulgarIntegracao::DIRETORIO_MODULO,__DIR__,$strArquivo);
    return file_get_contents($localFile);
  }

  public static function prepararIconeBase64($strArquivo,$strCor,$strSpan=''): string
  {
    $img=self::getConteudoIcone($strArquivo);
    $img=str_replace('fill="#000000"','fill="'.$strCor.'"',$img);
    if($strSpan!==''){
      $img=str_replace('<tspan>0</tspan>','<tspan>'.$strSpan.'</tspan>',$img);
    }
    return 'data:image/svg+xml;base64,'.base64_encode($img);
  }


}
?>