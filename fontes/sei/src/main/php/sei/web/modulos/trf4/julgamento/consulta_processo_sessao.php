<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../SEI.php';

session_start();
SessaoSEI::getInstance(false);

function print_obj_infraDTO(InfraDTO $objDTO){
  $arr= $objDTO->getArrAtributos();
  echo '------ OBJ: '.get_class($objDTO)." -----\n";
  foreach ($arr as $atributo) {
    echo $atributo[InfraDTO::$POS_ATRIBUTO_PREFIXO];
    echo $atributo[InfraDTO::$POS_ATRIBUTO_NOME].' [';
    echo $atributo[InfraDTO::$POS_ATRIBUTO_FLAGS].']: ';
    echo $atributo[InfraDTO::$POS_ATRIBUTO_VALOR]??"NULL";
    echo PHP_EOL;
  }
  echo "----------\n";
}

$strProcessoFormatado = '0008078-89.2020.4.04.8000';

$objProtocoloDTO = new ProtocoloDTO();
$objProtocoloRN = new ProtocoloRN();

$objProtocoloDTO->setStrProtocoloFormatado($strProcessoFormatado);
$objProtocoloDTO->retStrStaProtocolo();
$objProtocoloDTO->retDblIdProtocolo();
$objProtocoloDTO->retStrStaNivelAcessoGlobal();

$objProtocoloDTO = $objProtocoloRN->consultarRN0186($objProtocoloDTO);
if ($objProtocoloDTO===null) {
  throw new InfraException('Protocolo n�o encontrado no SEI');
}
print_obj_infraDTO($objProtocoloDTO);
$dblIdProtocolo = $objProtocoloDTO->getDblIdProtocolo();
$strResultado = "<div>Protocolo: <div>$strProcessoFormatado</div></div>\n";
$objProcedimentoDTO = new ProcedimentoDTO();
$objProcedimentoRN = new ProcedimentoRN();
$objProcedimentoDTO->setDblIdProcedimento($dblIdProtocolo);
$objProcedimentoDTO->retStrNomeTipoProcedimento();
$objProcedimentoDTO = $objProcedimentoRN->consultarRN0201($objProcedimentoDTO);
if ($objProcedimentoDTO===null) {
  throw new InfraException('Processo n�o encontrado no SEI');
}
print_obj_infraDTO($objProcedimentoDTO);
$strResultado .= "<div>Procedimento - Tipo: <div>{$objProcedimentoDTO->getStrNomeTipoProcedimento()}</div></div>\n";
$objDistribuicaoDTO = new DistribuicaoDTO();
$objDistribuicaoRN = new DistribuicaoRN();
$objDistribuicaoDTO->setDblIdProcedimento($dblIdProtocolo);
$objDistribuicaoDTO->setOrdDthDistribuicao(InfraDTO::$TIPO_ORDENACAO_ASC);
$objDistribuicaoDTO->retTodos();
$objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
$objDistribuicaoDTO->retStrNomeColegiado();
$objDistribuicaoDTO->retStrNomeUsuarioRelator();
$arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);

$qtdDistribuicoes = count($arrObjDistribuicaoDTO);
$strResultado .= "<div>Total de distribui��es: $qtdDistribuicoes</div>\n\n";
$objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
/** @var DistribuicaoDTO[][] $idxObjDistribuicaoDTO */
$idxObjDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO, 'IdColegiadoColegiadoVersao', true);
foreach ($idxObjDistribuicaoDTO as $numIdColegiado => $arrObjDistribuicaoDTO) {
  $strResultado .= "\n<div>Distribui��es do colegiado $numIdColegiado: ({$arrObjDistribuicaoDTO[0]->getStrNomeColegiado()})</div>\n";
  $strResultado .= "<pre>";
  foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
    print_obj_infraDTO($objDistribuicaoDTO);
    $strResultado .= "ID=" . $objDistribuicaoDTO->getNumIdDistribuicao();
    $strResultado .= " Data:" . $objDistribuicaoDTO->getDthDistribuicao();
    $strResultado .= " Relator:" . $objDistribuicaoDTO->getStrNomeUsuarioRelator() . "\n";
    $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($objDistribuicaoDTO->getNumIdDistribuicao());
    $objItemSessaoJulgamentoDTO->retTodos();
    $objItemSessaoJulgamentoDTO->retStrStaTipoItemSessaoBloco();
    $arrObjItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

    /** @var ItemSessaoJulgamentoDTO[] $arrObjItemSessaoJulgamentoDTO */
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      print_obj_infraDTO($objItemSessaoJulgamentoDTO);
      $strResultado .= "- Item id: " . $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
      $strResultado .= "- Tipo Bloco: " . $objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco();
      $strResultado .= "- Data Inclusao: " . $objItemSessaoJulgamentoDTO->getDthInclusao();
      $strResultado .= "- Usuario Sessao: " . $objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao();
      $strResultado .= "- Sessao: " . $objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento() . "\n\n";

      $objAndamentoSessaoDTO=new AndamentoSessaoDTO();
      $objAndamentoSessaoRN=new AndamentoSessaoRN();
      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->retStrSiglaUnidade();
      $objAndamentoSessaoDTO->retNumIdAndamentoSessao();
      $objAndamentoSessaoDTO->retDthExecucao();
      $objAndamentoSessaoDTO->retStrNomeTarefaSessao();
      $objAndamentoSessaoDTO->setOrdNumIdAndamentoSessao(InfraDTO::$TIPO_ORDENACAO_ASC);
      $arrObjAndamentoSessaoDTO=$objAndamentoSessaoRN->listar($objAndamentoSessaoDTO);
      foreach ($arrObjAndamentoSessaoDTO as $objAndamentoSessaoDTO) {
        $nomeTarefa=$objAndamentoSessaoDTO->getStrNomeTarefaSessao();

      }

      $arrObjAndamentoSessaoDTO=InfraArray::indexarArrInfraDTO($arrObjAndamentoSessaoDTO,'IdAndamentoSessao');

      $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoRN=new AtributoAndamentoSessaoRN();
      $objAtributoAndamentoSessaoDTO->setNumIdAndamentoSessao(InfraArray::converterArrInfraDTO($arrObjAndamentoSessaoDTO,'IdAndamentoSessao'),InfraDTO::$OPER_IN);
      $objAtributoAndamentoSessaoDTO->retStrValor();
      $objAtributoAndamentoSessaoDTO->retStrChave();
      $objAtributoAndamentoSessaoDTO->retNumIdAndamentoSessao();

      $arrObjAtributoAndamentoSessaoDTO=$objAtributoAndamentoSessaoRN->listar($objAtributoAndamentoSessaoDTO);

      foreach ($arrObjAtributoAndamentoSessaoDTO as $objAtributoAndamentoSessaoDTO) {
        $idAndamento=$objAtributoAndamentoSessaoDTO->getNumIdAndamentoSessao();
        $nomeTarefa=$arrObjAndamentoSessaoDTO[$idAndamento]->getStrNomeTarefaSessao();
        if(strpos($nomeTarefa,'@PROCESSO@') && $objAtributoAndamentoSessaoDTO->getStrValor()!=$strProcessoFormatado){
          $arrObjAndamentoSessaoDTO[$idAndamento]->setStrNomeTarefaSessao(null);
        } else {
          $arrObjAndamentoSessaoDTO[$idAndamento]->setStrNomeTarefaSessao(str_replace('@' . $objAtributoAndamentoSessaoDTO->getStrChave() . '@', $objAtributoAndamentoSessaoDTO->getStrValor(), $nomeTarefa));
        }
      }


      foreach ($arrObjAndamentoSessaoDTO as $objAndamentoSessaoDTO) {
        if($objAndamentoSessaoDTO->getStrNomeTarefaSessao()==null){
          continue;
        }
        $strResultado .= '** '.$objAndamentoSessaoDTO->getDthExecucao();
        $strResultado .= ' - '.$objAndamentoSessaoDTO->getStrSiglaUnidade();
        $strResultado .= ' - '.$objAndamentoSessaoDTO->getStrNomeTarefaSessao()."\n";
      }




      $objItemSessaoDocumentoDTO=new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoRN=new ItemSessaoDocumentoRN();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoDocumentoDTO->retStrProtocoloDocumentoFormatado();
      $objItemSessaoDocumentoDTO->retStrNomeUsuario();
      $objItemSessaoDocumentoDTO->retStrNomeSerie();
      $arrObjItemSessaoDocumentoDTO=$objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
      /** @var ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO */
      foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
        $strResultado.= "-- DOC: ".$objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado();
        $strResultado.= "- Serie: ".$objItemSessaoDocumentoDTO->getStrNomeSerie();
        $strResultado.= "- Usuario: ".$objItemSessaoDocumentoDTO->getStrNomeUsuario()."\n";
      }

      $objJulgamentoParteDTO=new JulgamentoParteDTO();
      $objJulgamentoParteRN=new JulgamentoParteRN();
      $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objJulgamentoParteDTO->retTodos();
      $arrObjJulgamentoParteDTO=$objJulgamentoParteRN->listar($objJulgamentoParteDTO);
      /** @var JulgamentoParteDTO $objJulgamentoParteDTO */
      foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
        print_obj_infraDTO($objJulgamentoParteDTO);
        $strResultado.="-- parte de julgamento: ".$objJulgamentoParteDTO->getNumIdJulgamentoParte();
        $strResultado.="- ".$objJulgamentoParteDTO->getStrDescricao()."\n";

        $objVotoParteDTO=new VotoParteDTO();
        $objVotoParteRN=new VotoParteRN();
        $objVotoParteDTO->setNumIdJulgamentoParte($objJulgamentoParteDTO->getNumIdJulgamentoParte());
        $objVotoParteDTO->retTodos();
        $objVotoParteDTO->retStrNomeUsuario();
        $objVotoParteDTO->retStrNomeUsuarioAssociado();
        $arrObjVotoParteDTO=$objVotoParteRN->listar($objVotoParteDTO);
        /** @var VotoParteDTO $objVotoParteDTO */
        foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
          $strResultado.="--- voto: ".$objVotoParteDTO->getStrStaVotoParte();
          $strResultado.="- ".$objVotoParteDTO->getStrNomeUsuario();
          if($objVotoParteDTO->getStrNomeUsuarioAssociado()!=null){
            $strResultado.="- ".$objVotoParteDTO->getStrNomeUsuarioAssociado();
          }
          $strResultado.=" (".$objVotoParteDTO->getDthVoto().")";
          if($objVotoParteDTO->getNumIdSessaoVoto()!=$objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento()){
            $strResultado.=" [SESSAO ANTERIOR]";
          }
          $strResultado.="\n";
          print_obj_infraDTO($objVotoParteDTO);
        }

      }


    }


  }

}
echo $strResultado;
die;
PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - Busca de dados da sess�o');
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody('Busca de dados da sess�o');



PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>