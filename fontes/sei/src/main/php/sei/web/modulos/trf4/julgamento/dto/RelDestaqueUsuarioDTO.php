<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class RelDestaqueUsuarioDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'rel_destaque_usuario';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdDestaque','id_destaque');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUnidadeDestaque','id_unidade','destaque');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdItemSessaoJulgamentoDestaque','id_item_sessao_julgamento','destaque');

    $this->configurarPK('IdDestaque',InfraDTO::$TIPO_PK_INFORMADO);
    $this->configurarPK('IdUsuario',InfraDTO::$TIPO_PK_INFORMADO);

    $this->configurarFK('IdDestaque', 'destaque', 'id_destaque');

  }
}
?>