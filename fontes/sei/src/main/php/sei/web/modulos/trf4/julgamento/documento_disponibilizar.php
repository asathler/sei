<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__ . '/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  ////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);


  $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
  $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();

  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '';
  $mensagem = '';
  if (isset($_GET['arvore'])) {
    $strParametros .= '&arvore=' . $_GET['arvore'];
  }
  if (isset($_GET['id_procedimento'])) {
    $strParametros .= '&id_procedimento=' . $_GET['id_procedimento'];
  }
  if (isset($_GET['id_documento'])) {
    $strParametros .= '&id_documento=' . $_GET['id_documento'];
  }
  if (isset($_GET['id_item_sessao_julgamento'])) {
    $strParametros .= '&id_item_sessao_julgamento=' . $_GET['id_item_sessao_julgamento'];
  }
  $arrComandos = array();
  $arrIdItemSessaoJulgamento = explode(',', $_GET['id_item_sessao_julgamento']);
  $bolMultiplasSessoes = count($arrIdItemSessaoJulgamento)>1;
  if (isset($_POST['selItemSessaoJulgamento'])){
    $numIdItemSessaoJulgamento=$_POST['selItemSessaoJulgamento'];
  } else if($bolMultiplasSessoes){
    $numIdItemSessaoJulgamento = null;
  } else {
    $numIdItemSessaoJulgamento=$arrIdItemSessaoJulgamento[0];
  }


  $bolExecucaoOK = false;
  $bolExibirProvimento = false;
  $bolMultiplo = false;
  $strTitulo = 'Disponibiliza��o de Documento para Sess�o de Julgamento';

  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
  if(!$bolMultiplasSessoes){
    $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento[0]);
  } else {
    $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento,InfraDTO::$OPER_IN);
  }
  $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->retNumIdUnidadeSessao();
  $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->retStrNomeColegiado();
  $objItemSessaoJulgamentoDTO->retStrStaTipoItemSessaoBloco();
  $objItemSessaoJulgamentoDTO->retDthSessaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
  $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

  foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTOBanco) {
    $strSessao=$objItemSessaoJulgamentoDTOBanco->getStrNomeColegiado().' - '.substr($objItemSessaoJulgamentoDTOBanco->getDthSessaoSessaoJulgamento(),0,16);
    $objItemSessaoJulgamentoDTOBanco->setStrNomeColegiado($strSessao);
    if($numIdItemSessaoJulgamento==$objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento()){
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoDTOBanco;
    }
    $strProcesso = $objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo();
  }
  if ($bolMultiplasSessoes) {
    $strItensSelSessaoJulgamento = InfraINT::montarSelectArrInfraDTO('null', '&nbsp;', $numIdItemSessaoJulgamento, $arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento', 'NomeColegiado');
  } else {
    $strItensSelSessaoJulgamento = InfraINT::montarSelectArrInfraDTO(null, '', $numIdItemSessaoJulgamento, $arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento', 'NomeColegiado');
  }

  if($numIdItemSessaoJulgamento){
    $numIdUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();
    $numIdUsuario = ColegiadoINT::getUsuarioUnidadeAtualColegiado($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
    //verificar se pode disponibilizar provimento
    //deve ser usuario sessao
    //n�o ter voto ainda
    $objJulgamentoParteDTO = new JulgamentoParteDTO();
    $objJulgamentoParteRN = new JulgamentoParteRN();
    $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    $objJulgamentoParteDTO->retNumIdJulgamentoParte();
    $objJulgamentoParteDTO->retStrDescricao();
    $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
    if (InfraArray::contar($arrObjJulgamentoParteDTO)>1) {
      $bolMultiplo = true;
    }

    if (isset($_POST['sbmDisponibilizar']) || isset($_POST['sbmSalvarProvimento'])) {
      $arrObjVotoParteDTO = array();
      if ($bolMultiplo) {
        foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
          $numIdJulgamentoParte = $objJulgamentoParteDTO->getNumIdJulgamentoParte();

          $objVotoParteDTO = new VotoParteDTO();
          $objVotoParteDTO->setNumIdProvimento($_POST['selProvimento' . $numIdJulgamentoParte]);
          $objVotoParteDTO->setStrComplemento($_POST['txaComplemento' . $numIdJulgamentoParte]);
          $objVotoParteDTO->setNumIdJulgamentoParte($numIdJulgamentoParte);
          $objVotoParteDTO->setStrDescricaoJulgamentoParte($objJulgamentoParteDTO->getStrDescricao());
          $objVotoParteDTO->setNumIdUsuario($numIdUsuario);
          $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO);
          $arrObjVotoParteDTO[$numIdJulgamentoParte] = $objVotoParteDTO;
        }
      } else {
        $objVotoParteDTO = new VotoParteDTO();
        $objVotoParteDTO->setNumIdProvimento($_POST['selProvimento']);
        $objVotoParteDTO->setStrComplemento($_POST['txaComplemento']);
        $objVotoParteDTO->setNumIdJulgamentoParte($arrObjJulgamentoParteDTO[0]->getNumIdJulgamentoParte());
        $objVotoParteDTO->setNumIdUsuario($numIdUsuario);
        $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO);

        if ($_POST['selProvimento']!=null) {
          $arrObjVotoParteDTO[] = $objVotoParteDTO;
        }
      }

    }

    $objVotoParteRN = new VotoParteRN();
    $objVotoParteDTO = new VotoParteDTO();
    $objVotoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objVotoParteDTO->retTodos();
    $objVotoParteDTO->retStrConteudoProvimento();

  }


  switch ($_GET['acao']) {
    case 'doc_sessao_julgamento_disponibilizar':

      $arrComandos[] = '<button type="submit" accesskey="" name="sbmDisponibilizar" value="Salvar" class="infraButton">Disponibilizar</button>';

      $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
      $objItemSessaoDocumentoDTO->setDblIdDocumento($_GET['id_documento']);
      $objItemSessaoDocumentoDTO->setNumIdUsuario($numIdUsuario);
      $objItemSessaoDocumentoDTO->setNumIdUnidade($numIdUnidadeAtual);

      if ($numIdItemSessaoJulgamento && ($objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco()!=TipoSessaoBlocoRN::$STA_REFERENDO || $numIdUnidadeAtual==$objItemSessaoJulgamentoDTO->getNumIdUnidadeSessao())) {
        $objVotoParteDTO->adicionarCriterio(array('StaVotoParte', 'IdUsuario'),
            array(InfraDTO::$OPER_DIFERENTE, InfraDTO::$OPER_IGUAL), array(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO, $numIdUsuario), InfraDTO::$OPER_LOGICO_OR);
        if ($objVotoParteRN->contar($objVotoParteDTO)==0) {
          $bolExibirProvimento = true;
        }
      }


      if (isset($_POST['sbmDisponibilizar'])) {
        try {
          $objItemSessaoDocumentoDTO->setArrObjVotoParteDTO($arrObjVotoParteDTO);
          $objItemSessaoDocumentoRN->disponibilizar($objItemSessaoDocumentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Documento disponibilizado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=arvore_visualizar&acao_origem='.$_GET['acao'].$strParametros.'&atualizar_arvore=1'));
          die;

        } catch (Exception $e) {
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'provimento_disponibilizado_alterar':

      $arrComandos[] = '<button type="submit" accesskey="" name="sbmSalvarProvimento" value="Salvar" class="infraButton">Salvar</button>';

      $bolExibirProvimento = true;
      $strTitulo = 'Alterar Provimento';

      if ($objItemSessaoJulgamentoDTO->getNumIdUnidadeSessao()!=$numIdUnidadeAtual) {
        throw new InfraException('Provimento s� pode ser alterado pelo relator.');
      }

      $objVotoParteDTO->setNumIdUsuario($numIdUsuario, InfraDTO::$OPER_DIFERENTE);
      if ($objVotoParteRN->contar($objVotoParteDTO)!=0) {
        $objInfraException = new InfraException();
        $objInfraException->lancarValidacao('J� existem votos lan�ados. S� � poss�vel editar provimento na tela de vota��o.');
      }

      $objVotoParteDTO->setNumIdUsuario($numIdUsuario);
      $arrObjVotoParteDTOBanco = $objVotoParteRN->listar($objVotoParteDTO);
      if (InfraArray::contar($arrObjVotoParteDTOBanco)==0) {
        throw new InfraException('Voto de disponibiliza��o de provimento n�o encontrado.');
      }

      if (isset($_POST['sbmSalvarProvimento'])) {
        try {

          foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
            $objVotoParteDTO->setNumIdVotoParteAssociado(null);
            $objVotoParteDTO->setStrRessalva(null);
            $objVotoParteDTO->setDthVoto(InfraData::getStrDataHoraAtual());
            $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
            $objVotoParteDTO->setStrSinVencedor('N');
            $objVotoParteRN->registrar($objVotoParteDTO);
          }
          PaginaSEI::getInstance()->adicionarMensagem('Provimento alterado com sucesso.');
          $bolExecucaoOK = true;
        } catch (Exception $e) {
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      } else {
        $arrObjVotoParteDTO = $arrObjVotoParteDTOBanco;
      }

      break;
    default:
      throw new InfraException("A��o '" . $_GET['acao'] . '\' n�o reconhecida.');
  }
  if ($bolExibirProvimento) {
    if ($bolMultiplo) {
      $arrObjVotoParteDTO = InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO, 'IdJulgamentoParte');
      $strResultado = '';
      $numRegistros = InfraArray::contar($arrObjJulgamentoParteDTO);

      $strSumarioTabela = 'Tabela de Itens de Julgamento.';
      $strCaptionTabela = 'Itens de Julgamento';

      $strResultado .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
      $strResultado .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistros) . '</caption>';
      $strResultado .= '<thead><tr>';
      $strResultado .= '<th class="infraTh" style="width: 15%">Item de Julgamento</th>' . "\n";
      $strResultado .= '<th class="infraTh" style="width: 45%">Provimento</th>' . "\n";
      $strResultado .= '<th class="infraTh" style="width: 40%">Complemento</th>' . "\n";
      $strResultado .= '</tr></thead>' . "\n";
      $strCssTr = '';
      $strResultado .= '<tbody>';
      foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
        $numIdJulgamentoParte = $objJulgamentoParteDTO->getNumIdJulgamentoParte();
        if (isset($arrObjVotoParteDTO[$numIdJulgamentoParte])) {
          $objVotoParteDTO = $arrObjVotoParteDTO[$numIdJulgamentoParte];
        } else {
          $objVotoParteDTO = new VotoParteDTO();
          $objVotoParteDTO->setNumIdProvimento(null);
          $objVotoParteDTO->setStrComplemento('');
        }

        $strCssTr = ($strCssTr=='<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
        $strResultado .= $strCssTr;
        $strItensProvimento = ProvimentoINT::montarSelectConteudo('null', '&nbsp;', $objVotoParteDTO->getNumIdProvimento());
        $strResultado .= '<td style="text-align: center"><b>' . $objJulgamentoParteDTO->getStrDescricao() . '</b></td>';
        $strResultado .= '<td><select id="selProvimento' . $numIdJulgamentoParte . '" name="selProvimento' . $numIdJulgamentoParte . '" class="infraSelect" style="width:99%" tabindex="';
        $strResultado .= PaginaSEI::getInstance()->getProxTabDados() . '">' . $strItensProvimento . '</select>';
        $strResultado .= '</td>';
        $strResultado .= '<td><textarea id="txaComplemento' . $numIdJulgamentoParte . '" name="txaComplemento' . $numIdJulgamentoParte . '" rows="3"  style="width:99%" class="infraTextarea" tabindex="' . PaginaSEI::getInstance()->getProxTabDados() . '">' . PaginaSEI::tratarHTML($objVotoParteDTO->getStrComplemento()) . '</textarea>';
        $strResultado .= '</td></tr>' . "\n";
      }
      $strResultado .= '</tbody></table>' . "\n";

    } else {
      $objVotoParteDTO=$arrObjVotoParteDTO[0];
    }
  }

  $numIdProvimento=null;
  if($objVotoParteDTO && $objVotoParteDTO->isSetNumIdProvimento()){
    $numIdProvimento=$objVotoParteDTO->getNumIdProvimento();
  }
  $strItensProvimento=ProvimentoINT::montarSelectConteudo('null','&nbsp;',$numIdProvimento);

  if($objVotoParteDTO==null){
    $objVotoParteDTO=new VotoParteDTO();
    $objVotoParteDTO->setStrComplemento('');
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}
$acaoRetorno=PaginaSEI::getInstance()->getAcaoRetorno();
$strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$acaoRetorno.'&acao_origem='.$_GET['acao'].'&montar_visualizacao=1'.$strParametros);
$strLinkArvore=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_visualizar&acao_origem='.$_GET['acao'].$strParametros.'&montar_visualizacao=1');


PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
  #lblProvimento {position:absolute;left:0%;top:0%;width:95%;}
  #selProvimento {position:absolute;left:0%;top:13%;width:96%;}
  #lblComplemento {position:absolute;left:0%;top:33%;width:95%;}
  #txaComplemento {position:absolute;left:0%;top:46%;width:95%;}
  #lblItemSessaoJulgamento {position:absolute;left:0%;top:0%;width:50%;}
  #selItemSessaoJulgamento {position:absolute;left:0%;top:38%;width:50%;}
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
function inicializar(){
<? if ($bolExecucaoOK) { ?>
  fechar_pagina('<?=PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento'])?>','<?=$strLinkRetorno?>');
  <?}?>
}
  function validarCadastro() {
<?if ($bolExibirProvimento) {?>
    var selVazios=$('select').filter(function(){return this.value==='null'});
    if(selVazios.length>0){
      selVazios.focus();
      alert('Selecione um Provimento.');
      return false;
    }
<?}?>
    return true;
  }
function onSubmitForm(){
  return validarCadastro();
}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmItemSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
//PaginaSEI::getInstance()->abrirAreaDados('10em');
//if($bolMultiplasSessoes){
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblItemSessaoJulgamento" for="selItemSessaoJulgamento" accesskey="" class="infraLabelOpcional">Sess�o de Julgamento:</label>
  <select id="selItemSessaoJulgamento" name="selItemSessaoJulgamento" onchange="this.form.submit();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelSessaoJulgamento ?>
  </select>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
//}
  //PaginaSEI::getInstance()->fecharAreaDados();
if ($bolExibirProvimento) {
  if(!$bolMultiplo){
    PaginaSEI::getInstance()->abrirAreaDados('15em');
    ?>

    <label id="lblProvimento" for="selProvimento" accesskey="" class="infraLabelObrigatorio">Provimento:</label>
    <select id="selProvimento" name="selProvimento" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
      <?= $strItensProvimento ?>
    </select>
    <label id="lblComplemento" for="txaComplemento" accesskey="" class="infraLabelOpcional">Complemento:</label>
    <textarea id="txaComplemento" name="txaComplemento" rows="3" class="infraTextarea"
              tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?= PaginaSEI::tratarHTML($objVotoParteDTO->getStrComplemento()) ?></textarea>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();


  } else {
    PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  }

}
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>