<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4ª REGIÃO
*
* 16/07/2015 - criado por bcu
*
* Versão do Gerador de Código: 1.35.0
*
* Versão no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('motivo_canc_distribuicao_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  switch($_GET['acao']){
    case 'motivo_canc_distribuicao_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoCancDistribuicaoDTO = array();
        foreach ($arrStrIds as $strId) {
          $objMotivoCancDistribuicaoDTO = new MotivoCancDistribuicaoDTO();
          $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($strId);
          $arrObjMotivoCancDistribuicaoDTO[] = $objMotivoCancDistribuicaoDTO;
        }
        $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
        $objMotivoCancDistribuicaoRN->excluir($arrObjMotivoCancDistribuicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;


    case 'motivo_canc_distribuicao_desativar':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoCancDistribuicaoDTO = array();
        foreach ($arrStrIds as $strId) {
          $objMotivoCancDistribuicaoDTO = new MotivoCancDistribuicaoDTO();
          $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($strId);
          $arrObjMotivoCancDistribuicaoDTO[] = $objMotivoCancDistribuicaoDTO;
        }
        $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
        $objMotivoCancDistribuicaoRN->desativar($arrObjMotivoCancDistribuicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

    case 'motivo_canc_distribuicao_reativar':
      $strTitulo = 'Reativar Motivos de Cancelamento de Distribuição';
      if ($_GET['acao_confirmada']=='sim'){
        try{
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjMotivoCancDistribuicaoDTO = array();
          foreach ($arrStrIds as $strId) {
            $objMotivoCancDistribuicaoDTO = new MotivoCancDistribuicaoDTO();
            $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($strId);
            $arrObjMotivoCancDistribuicaoDTO[] = $objMotivoCancDistribuicaoDTO;
          }
          $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
          $objMotivoCancDistribuicaoRN->reativar($arrObjMotivoCancDistribuicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        } 
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
        die;
      } 
      break;


    case 'motivo_canc_distribuicao_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Motivo de Cancelamento de Distribuição','Selecionar Motivos de Cancelamento de Distribuição');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='motivo_canc_distribuicao_cadastrar'){
        if (isset($_GET['id_motivo_canc_distribuicao'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_motivo_canc_distribuicao']);
        }
      }
      break;

    case 'motivo_canc_distribuicao_listar':
      $strTitulo = 'Motivos de Cancelamento de Distribuição';
      break;

    default:
      throw new InfraException("Ação '".$_GET['acao']."' não reconhecida.");
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'motivo_canc_distribuicao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  if ($_GET['acao'] == 'motivo_canc_distribuicao_listar' || $_GET['acao'] == 'motivo_canc_distribuicao_selecionar'){
    $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_cadastrar');
    if ($bolAcaoCadastrar){
      $arrComandos[] = '<button type="button" accesskey="N" id="btnNovo" value="Novo" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_canc_distribuicao_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ovo</button>';
    }
  }

  $objMotivoCancDistribuicaoDTO = new MotivoCancDistribuicaoDTO();
  $objMotivoCancDistribuicaoDTO->retNumIdMotivoCancDistribuicao();
  $objMotivoCancDistribuicaoDTO->retStrDescricao();

  $objMotivoCancDistribuicaoDTO->setBolExclusaoLogica(false);
  $objMotivoCancDistribuicaoDTO->retStrSinAtivo();

  PaginaSEI::getInstance()->prepararOrdenacao($objMotivoCancDistribuicaoDTO, 'Descricao', InfraDTO::$TIPO_ORDENACAO_ASC);

  $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
  $arrObjMotivoCancDistribuicaoDTO = $objMotivoCancDistribuicaoRN->listar($objMotivoCancDistribuicaoDTO);

  $numRegistros = InfraArray::contar($arrObjMotivoCancDistribuicaoDTO);

  if ($numRegistros > 0){

    $bolCheck = false;

    if ($_GET['acao']=='motivo_canc_distribuicao_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_alterar');
      $bolAcaoImprimir = false;
      //$bolAcaoGerarPlanilha = false;
      $bolAcaoExcluir = false;
      $bolAcaoDesativar = false;
      $bolCheck = true;
    }else{
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_alterar');
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_excluir');
      $bolAcaoDesativar = SessaoSEI::getInstance()->verificarPermissao('motivo_canc_distribuicao_desativar');
    }

    
    if ($bolAcaoDesativar){
      //$bolCheck = true;
      //$arrComandos[] = '<button type="button" accesskey="t" id="btnDesativar" value="Desativar" onclick="acaoDesativacaoMultipla();" class="infraButton">Desa<span class="infraTeclaAtalho">t</span>ivar</button>';
      $strLinkDesativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_canc_distribuicao_desativar&acao_origem='.$_GET['acao']);
    }

    if ($bolAcaoReativar){
      //$bolCheck = true;
      //$arrComandos[] = '<button type="button" accesskey="R" id="btnReativar" value="Reativar" onclick="acaoReativacaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">R</span>eativar</button>';
      $strLinkReativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_canc_distribuicao_reativar&acao_origem='.$_GET['acao'].'&acao_confirmada=sim');
    }
    

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_canc_distribuicao_excluir&acao_origem='.$_GET['acao']);
    }

    /*
    if ($bolAcaoGerarPlanilha){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="P" id="btnGerarPlanilha" value="Gerar Planilha" onclick="infraGerarPlanilhaTabela(\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=infra_gerar_planilha_tabela').'\');" class="infraButton">Gerar <span class="infraTeclaAtalho">P</span>lanilha</button>';
    }
    */

    $strResultado = '';

      $strSumarioTabela = 'Tabela de Motivos de Cancelamento de Distribuição.';
      $strCaptionTabela = 'Motivos de Cancelamento de Distribuição';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objMotivoCancDistribuicaoDTO,'Descrição','Descricao',$arrObjMotivoCancDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="15%">Ações</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      if ($arrObjMotivoCancDistribuicaoDTO[$i]->getStrSinAtivo()=='S'){
        $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      }else{
        $strCssTr = '<tr class="trVermelha">';
      }

      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjMotivoCancDistribuicaoDTO[$i]->getNumIdMotivoCancDistribuicao(),$arrObjMotivoCancDistribuicaoDTO[$i]->getStrDescricao()).'</td>';
      }
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjMotivoCancDistribuicaoDTO[$i]->getStrDescricao()).'</td>';
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjMotivoCancDistribuicaoDTO[$i]->getNumIdMotivoCancDistribuicao());


      if ($bolAcaoAlterar && $arrObjMotivoCancDistribuicaoDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_canc_distribuicao_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_motivo_canc_distribuicao='.$arrObjMotivoCancDistribuicaoDTO[$i]->getNumIdMotivoCancDistribuicao()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Motivo de Cancelamento de Distribuição" alt="Alterar Motivo de Cancelamento de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoDesativar || $bolAcaoReativar || $bolAcaoExcluir){
        $strId = $arrObjMotivoCancDistribuicaoDTO[$i]->getNumIdMotivoCancDistribuicao();
        $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjMotivoCancDistribuicaoDTO[$i]->getStrDescricao());
      }

      if ($bolAcaoDesativar && $arrObjMotivoCancDistribuicaoDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoDesativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeDesativar().'" title="Desativar Motivo de Cancelamento de Distribuição" alt="Desativar Motivo de Cancelamento de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoReativar && $arrObjMotivoCancDistribuicaoDTO[$i]->getStrSinAtivo()=='N'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoReativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeReativar().'" title="Reativar Motivo de Cancelamento de Distribuição" alt="Reativar Motivo de Cancelamento de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoExcluir){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoExcluir(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Motivo de Cancelamento de Distribuição" alt="Excluir Motivo de Cancelamento de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'motivo_canc_distribuicao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='motivo_canc_distribuicao_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('btnFechar').focus();
  }
  infraEfeitoTabelas();
}

<? if ($bolAcaoDesativar){ ?>
function acaoDesativar(id,desc){
  if (confirm("Confirma desativação do Motivo de Cancelamento de Distribuição \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoCancDistribuicaoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoCancDistribuicaoLista').submit();
  }
}

function acaoDesativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Cancelamento de Distribuição selecionado.');
    return;
  }
  if (confirm("Confirma desativação dos Motivos de Cancelamento de Distribuição selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoCancDistribuicaoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoCancDistribuicaoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoReativar){ ?>
function acaoReativar(id,desc){
  if (confirm("Confirma reativação do Motivo de Cancelamento de Distribuição \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoCancDistribuicaoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoCancDistribuicaoLista').submit();
  }
}

function acaoReativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Cancelamento de Distribuição selecionado.');
    return;
  }
  if (confirm("Confirma reativação dos Motivos de Cancelamento de Distribuição selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoCancDistribuicaoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoCancDistribuicaoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclusão do Motivo de Cancelamento de Distribuição \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoCancDistribuicaoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoCancDistribuicaoLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Cancelamento de Distribuição selecionado.');
    return;
  }
  if (confirm("Confirma exclusão dos Motivos de Cancelamento de Distribuição selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoCancDistribuicaoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoCancDistribuicaoLista').submit();
  }
}
<? } ?>

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmMotivoCancDistribuicaoLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  //PaginaSEI::getInstance()->abrirAreaDados('5em');
  //PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>