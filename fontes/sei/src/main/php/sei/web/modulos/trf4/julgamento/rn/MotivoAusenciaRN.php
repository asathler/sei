<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MotivoAusenciaRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrDescricao(MotivoAusenciaDTO $objMotivoAusenciaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoAusenciaDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('descri��o n�o informada.');
    }else{
      $objMotivoAusenciaDTO->setStrDescricao(trim($objMotivoAusenciaDTO->getStrDescricao()));

      if (strlen($objMotivoAusenciaDTO->getStrDescricao())>100){
        $objInfraException->adicionarValidacao('descri��o possui tamanho superior a 100 caracteres.');
      }
      
      $dto = new MotivoAusenciaDTO();
      $dto->setBolExclusaoLogica(false);
      $dto->retStrSinAtivo();
      $dto->setNumIdMotivoAusencia($objMotivoAusenciaDTO->getNumIdMotivoAusencia(),InfraDTO::$OPER_DIFERENTE);
      $dto->setStrDescricao($objMotivoAusenciaDTO->getStrDescricao());
      $dto = $this->consultar($dto);

      if ($dto!=null) {
        if ($dto->getStrSinAtivo()=='S') {
          $objInfraException->adicionarValidacao('Existe outro motivo com esta Descri��o.');
        } else {
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa com esta Descri��o.');
        }
      }
    }
  }

  private function validarStrSinAtivo(MotivoAusenciaDTO $objMotivoAusenciaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoAusenciaDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objMotivoAusenciaDTO->getStrSinAtivo())){
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }

  protected function cadastrarControlado(MotivoAusenciaDTO $objMotivoAusenciaDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_cadastrar',__METHOD__,$objMotivoAusenciaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objMotivoAusenciaDTO, $objInfraException);
      $this->validarStrSinAtivo($objMotivoAusenciaDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      $ret = $objMotivoAusenciaBD->cadastrar($objMotivoAusenciaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Motivo de Aus�ncia.',$e);
    }
  }

  protected function alterarControlado(MotivoAusenciaDTO $objMotivoAusenciaDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_alterar',__METHOD__,$objMotivoAusenciaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objMotivoAusenciaDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objMotivoAusenciaDTO, $objInfraException);
      }
      if ($objMotivoAusenciaDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objMotivoAusenciaDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      $objMotivoAusenciaBD->alterar($objMotivoAusenciaDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Motivo de Aus�ncia.',$e);
    }
  }

  protected function excluirControlado($arrObjMotivoAusenciaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_excluir',__METHOD__,$arrObjMotivoAusenciaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();
      $arrId=InfraArray::converterArrInfraDTO($arrObjMotivoAusenciaDTO,'IdMotivoAusencia');
      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      
      if(InfraArray::contar($arrId)>0) {
        $objMotivoAusenciaDTO = new MotivoAusenciaDTO();
        $objMotivoAusenciaDTO->setNumIdMotivoAusencia($arrId, InfraDTO::$OPER_IN);
        $objMotivoAusenciaDTO->retNumIdMotivoAusencia();
        $objMotivoAusenciaDTO->setBolExclusaoLogica(false);
        $arrObjMotivoAusenciaDTO = $this->listar($objMotivoAusenciaDTO);
      }
      for($i=0;$i<InfraArray::contar($arrObjMotivoAusenciaDTO);$i++){
        $objAusenciaSessaoDTO=new AusenciaSessaoDTO();
        $objAusenciaSessaoRN=new AusenciaSessaoRN();
        $objAusenciaSessaoDTO->setNumIdMotivoAusencia($arrObjMotivoAusenciaDTO[$i]->getNumIdMotivoAusencia());
        if ($objAusenciaSessaoRN->contar($objAusenciaSessaoDTO)>0){
          $objInfraException->lancarValidacao('Motivo de aus�ncia j� foi utilizado.');
        }
        $objMotivoAusenciaBD->excluir($arrObjMotivoAusenciaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Motivo de Aus�ncia.',$e);
    }
  }

  protected function consultarConectado(MotivoAusenciaDTO $objMotivoAusenciaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_consultar',__METHOD__,$objMotivoAusenciaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      $ret = $objMotivoAusenciaBD->consultar($objMotivoAusenciaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Motivo de Aus�ncia.',$e);
    }
  }

  protected function listarConectado(MotivoAusenciaDTO $objMotivoAusenciaDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_listar',__METHOD__,$objMotivoAusenciaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      $ret = $objMotivoAusenciaBD->listar($objMotivoAusenciaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Motivos de Aus�ncia.',$e);
    }
  }

  protected function contarConectado(MotivoAusenciaDTO $objMotivoAusenciaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_listar',__METHOD__,$objMotivoAusenciaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      $ret = $objMotivoAusenciaBD->contar($objMotivoAusenciaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Motivos de Aus�ncia.',$e);
    }
  }

  protected function desativarControlado($arrObjMotivoAusenciaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_desativar',__METHOD__,$arrObjMotivoAusenciaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjMotivoAusenciaDTO);$i++){
        $objMotivoAusenciaBD->desativar($arrObjMotivoAusenciaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Motivo de Aus�ncia.',$e);
    }
  }

  protected function reativarControlado($arrObjMotivoAusenciaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_reativar',__METHOD__,$arrObjMotivoAusenciaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjMotivoAusenciaDTO);$i++){
        $objMotivoAusenciaBD->reativar($arrObjMotivoAusenciaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Motivo de Aus�ncia.',$e);
    }
  }

  protected function bloquearControlado(MotivoAusenciaDTO $objMotivoAusenciaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_ausencia_consultar',__METHOD__,$objMotivoAusenciaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoAusenciaBD = new MotivoAusenciaBD($this->getObjInfraIBanco());
      $ret = $objMotivoAusenciaBD->bloquear($objMotivoAusenciaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Motivo de Aus�ncia.',$e);
    }
  }


}
?>