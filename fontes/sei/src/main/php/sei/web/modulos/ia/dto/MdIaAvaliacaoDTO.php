<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 19/05/2023 - criado por neijobson
 *
 * Vers�o do Gerador de C�digo: 1.43.2
 */

require_once dirname(__FILE__) . '/../../../SEI.php';

class MdIaAvaliacaoDTO extends InfraDTO
{

    public function getStrNomeTabela()
    {
        return 'md_ia_avaliacao';
    }

    public function montar()
    {

        $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdMdIaAvaliacao', 'id_md_ia_avaliacao');

        $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL, 'IdProcedimento', 'id_procedimento');

        $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUnidade', 'id_unidade');

        $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUsuario', 'id_usuario');

        $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'Atual', 'dth_atual');

        $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Sugestao', 'sugestao');

        $this->configurarPK('IdMdIaAvaliacao', InfraDTO::$TIPO_PK_SEQUENCIAL);

    }
}
