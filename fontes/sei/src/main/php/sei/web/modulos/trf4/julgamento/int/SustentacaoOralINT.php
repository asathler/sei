<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 04/10/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class SustentacaoOralINT extends InfraINT {

  public static function montarSelectIdSustentacaoOral($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdItemSessaoJulgamento='', $numIdQualificacaoParte='', $numIdContato=''){
    $objSustentacaoOralDTO = new SustentacaoOralDTO();
    $objSustentacaoOralDTO->retNumIdSustentacaoOral();
    $objSustentacaoOralDTO->retNumIdSustentacaoOral();

    if ($numIdItemSessaoJulgamento!==''){
      $objSustentacaoOralDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    }

    if ($numIdQualificacaoParte!==''){
      $objSustentacaoOralDTO->setNumIdQualificacaoParte($numIdQualificacaoParte);
    }

    if ($numIdContato!==''){
      $objSustentacaoOralDTO->setNumIdContato($numIdContato);
    }

    $objSustentacaoOralDTO->setOrdNumIdSustentacaoOral(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objSustentacaoOralRN = new SustentacaoOralRN();
    $arrObjSustentacaoOralDTO = $objSustentacaoOralRN->listar($objSustentacaoOralDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjSustentacaoOralDTO, 'IdSustentacaoOral', 'IdSustentacaoOral');
  }

  public static function montarTextoDispositivo($idItemSessaoJulgamento)
  {
    $objSustentacaoOralDTO=new SustentacaoOralDTO();
    $objSustentacaoOralDTO->setNumIdItemSessaoJulgamento($idItemSessaoJulgamento);
    $objSustentacaoOralDTO->retStrNomeContato();
    $objSustentacaoOralDTO->setOrdStrNomeContato(InfraDTO::$TIPO_ORDENACAO_ASC);
    $objSustentacaoOralRN=new SustentacaoOralRN();
    $arrObjSustentacaoOralDTO=$objSustentacaoOralRN->listar($objSustentacaoOralDTO);

    $ret='';
    foreach ($arrObjSustentacaoOralDTO as $objSustentacaoOralDTO) {
      if($ret===''){
        $ret.='Sustenta��o Oral: ';
      } else {
        $ret.=', ';
      }
      $ret.=$objSustentacaoOralDTO->getStrNomeContato();
    }
    if($ret===''){
      $ret.="\n\n";
    }
    return $ret;

  }
}
?>