<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 07/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('motivo_mesa_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objMotivoMesaDTO = new MotivoMesaDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'motivo_mesa_cadastrar':
      $strTitulo = 'Novo Motivo de Mesa';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarMotivoMesa" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objMotivoMesaDTO->setNumIdMotivoMesa(null);
      $objMotivoMesaDTO->setStrDescricao($_POST['txtDescricao']);
      $objMotivoMesaDTO->setNumOrdem($_POST['txtOrdem']);
      $objMotivoMesaDTO->setStrSinAtivo('S');

      if (isset($_POST['sbmCadastrarMotivoMesa'])) {
        try{
          $objMotivoMesaRN = new MotivoMesaRN();
          $objMotivoMesaDTO = $objMotivoMesaRN->cadastrar($objMotivoMesaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo "'.$objMotivoMesaDTO->getStrDescricao().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_motivo_mesa='.$objMotivoMesaDTO->getNumIdMotivoMesa().PaginaSEI::getInstance()->montarAncora($objMotivoMesaDTO->getNumIdMotivoMesa())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_mesa_alterar':
      $strTitulo = 'Alterar Motivo de Mesa';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarMotivoMesa" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_motivo_mesa'])){
        $objMotivoMesaDTO->setNumIdMotivoMesa($_GET['id_motivo_mesa']);
        $objMotivoMesaDTO->retTodos();
        $objMotivoMesaRN = new MotivoMesaRN();
        $objMotivoMesaDTO = $objMotivoMesaRN->consultar($objMotivoMesaDTO);
        if ($objMotivoMesaDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objMotivoMesaDTO->setNumIdMotivoMesa($_POST['hdnIdMotivoMesa']);
        $objMotivoMesaDTO->setStrDescricao($_POST['txtDescricao']);
        $objMotivoMesaDTO->setNumOrdem($_POST['txtOrdem']);
        $objMotivoMesaDTO->setStrSinAtivo('S');
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoMesaDTO->getNumIdMotivoMesa())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarMotivoMesa'])) {
        try{
          $objMotivoMesaRN = new MotivoMesaRN();
          $objMotivoMesaRN->alterar($objMotivoMesaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo "'.$objMotivoMesaDTO->getStrDescricao().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoMesaDTO->getNumIdMotivoMesa())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_mesa_consultar':
      $strTitulo = 'Consultar Motivo de Mesa';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_motivo_mesa'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objMotivoMesaDTO->setNumIdMotivoMesa($_GET['id_motivo_mesa']);
      $objMotivoMesaDTO->setBolExclusaoLogica(false);
      $objMotivoMesaDTO->retTodos();
      $objMotivoMesaRN = new MotivoMesaRN();
      $objMotivoMesaDTO = $objMotivoMesaRN->consultar($objMotivoMesaDTO);
      if ($objMotivoMesaDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblDescricao {position:absolute;left:0%;top:0%;width:70%;}
#txtDescricao {position:absolute;left:0%;top:6%;width:70%;}

#lblOrdem {position:absolute;left:0%;top:16%;width:10%;}
#txtOrdem {position:absolute;left:0%;top:22%;width:10%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
//<script>
function inicializar(){
  if ('<?=$_GET['acao']?>'=='motivo_mesa_cadastrar'){
    document.getElementById('txtDescricao').focus();
  } else if ('<?=$_GET['acao']?>'=='motivo_mesa_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
}

function validarCadastro() {

  if (infraTrim(document.getElementById('txtDescricao').value)==''){
    alert('Informe a Descri��o.');
    document.getElementById('txtDescricao').focus();
    return false;
  }

  if (infraTrim(document.getElementById('txtOrdem').value)==''){
    alert('Informe a Ordem.');
    document.getElementById('txtOrdem').focus();
    return false;
  }


  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

//</script>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmMotivoMesaCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('30em');
?>
  <label id="lblDescricao" for="txtDescricao" class="infraLabelObrigatorio">Descri��o:</label>
  <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="<?=PaginaSEI::tratarHTML($objMotivoMesaDTO->getStrDescricao());?>" onkeypress="return infraMascaraTexto(this,event,250);" maxlength="250" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblOrdem" for="txtOrdem" class="infraLabelObrigatorio">Ordem:</label>
  <input type="text" id="txtOrdem" name="txtOrdem" onkeypress="return infraMascaraNumero(this, event)" class="infraText" value="<?=PaginaSEI::tratarHTML($objMotivoMesaDTO->getNumOrdem());?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <input type="hidden" id="hdnIdMotivoMesa" name="hdnIdMotivoMesa" value="<?=$objMotivoMesaDTO->getNumIdMotivoMesa();?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>