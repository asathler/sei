<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 13/03/2020 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class RelAutuacaoTipoMateriaDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'rel_autuacao_tipo_materia';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdAutuacao', 'id_autuacao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdTipoMateria', 'id_tipo_materia');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Ordem', 'ordem');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoTipoMateria','descricao','tipo_materia');

    $this->configurarFK('IdTipoMateria', 'tipo_materia', 'id_tipo_materia');
    $this->configurarFK('IdAutuacao', 'autuacao', 'id_autuacao');

    $this->configurarPK('IdAutuacao',InfraDTO::$TIPO_PK_INFORMADO);
    $this->configurarPK('IdTipoMateria',InfraDTO::$TIPO_PK_INFORMADO);

  }
}
?>