<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class VotoEleicaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdEleicao(VotoEleicaoDTO $objVotoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoEleicaoDTO->getNumIdEleicao())){
      $objInfraException->adicionarValidacao('Escrut�nio Eletr�nico n�o informado.');
    }
  }

  private function validarNumIdUsuario(VotoEleicaoDTO $objVotoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoEleicaoDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
    }
  }

  private function validarDthVotoEleicao(VotoEleicaoDTO $objVotoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoEleicaoDTO->getDthVotoEleicao())){
      $objInfraException->adicionarValidacao('Data/Hora n�o informada.');
    }else{
      if (!InfraData::validarDataHora($objVotoEleicaoDTO->getDthVotoEleicao())){
        $objInfraException->adicionarValidacao('Data/Hora inv�lida.');
      }
    }
  }

  private function validarNumOrdem(VotoEleicaoDTO $objVotoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoEleicaoDTO->getNumOrdem())){
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }

  protected function cadastrarControlado(VotoEleicaoDTO $objVotoEleicaoDTO) {
    try{

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_cadastrar', __METHOD__, $objVotoEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdEleicao($objVotoEleicaoDTO, $objInfraException);
      $this->validarNumIdUsuario($objVotoEleicaoDTO, $objInfraException);
      $this->validarDthVotoEleicao($objVotoEleicaoDTO, $objInfraException);
      $this->validarNumOrdem($objVotoEleicaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoEleicaoBD->cadastrar($objVotoEleicaoDTO);

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Voto no Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function alterarControlado(VotoEleicaoDTO $objVotoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_alterar', __METHOD__, $objVotoEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objVotoEleicaoDTOBanco = new VotoEleicaoDTO();
      $objVotoEleicaoDTOBanco->retNumIdEleicao();
      $objVotoEleicaoDTOBanco->retNumIdUsuario();
      $objVotoEleicaoDTOBanco->retDthVotoEleicao();
      $objVotoEleicaoDTOBanco->retNumOrdem();
      $objVotoEleicaoDTOBanco->setNumIdVotoEleicao($objVotoEleicaoDTO->getNumIdVotoEleicao());

      $objVotoEleicaoDTOBanco = $this->consultar($objVotoEleicaoDTOBanco);

      if ($objVotoEleicaoDTO->isSetNumIdEleicao() && $objVotoEleicaoDTO->getNumIdEleicao()!=$objVotoEleicaoDTOBanco->getNumIdEleicao()){
        $objInfraException->lancarValidacao('N�o � poss�vel alterar o Escrut�nio Eletr�nico de um voto.');
      }

      if ($objVotoEleicaoDTO->isSetNumIdUsuario() && $objVotoEleicaoDTO->getNumIdUsuario()!=$objVotoEleicaoDTOBanco->getNumIdUsuario()){
        $objInfraException->lancarValidacao('N�o � poss�vel alterar o usu�rio de um voto.');
      }

      if ($objVotoEleicaoDTO->isSetDthVotoEleicao() && $objVotoEleicaoDTO->getDthVotoEleicao()!=$objVotoEleicaoDTOBanco->getDthVotoEleicao()){
        $objInfraException->lancarValidacao('N�o � poss�vel alterar a data/hora de um voto.');
      }

      if ($objVotoEleicaoDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objVotoEleicaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      $objVotoEleicaoBD->alterar($objVotoEleicaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro alterando Voto no Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function excluirControlado($arrObjVotoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_excluir', __METHOD__, $arrObjVotoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjVotoEleicaoDTO);$i++){
        $objVotoEleicaoBD->excluir($arrObjVotoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Voto do Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function consultarConectado(VotoEleicaoDTO $objVotoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_consultar', __METHOD__, $objVotoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoEleicaoBD->consultar($objVotoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Voto no Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function listarConectado(VotoEleicaoDTO $objVotoEleicaoDTO) {
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_listar', __METHOD__, $objVotoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoEleicaoBD->listar($objVotoEleicaoDTO);

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Votos dos Escrut�nios Eletr�nicos.',$e);
    }
  }

  protected function contarConectado(VotoEleicaoDTO $objVotoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_listar', __METHOD__, $objVotoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoEleicaoBD->contar($objVotoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Votos dos Escrut�nios Eletr�nicos.',$e);
    }
  }
/* 
  protected function desativarControlado($arrObjVotoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_desativar', __METHOD__, $arrObjVotoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjVotoEleicaoDTO);$i++){
        $objVotoEleicaoBD->desativar($arrObjVotoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro desativando Voto no Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function reativarControlado($arrObjVotoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_reativar', __METHOD__, $arrObjVotoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjVotoEleicaoDTO);$i++){
        $objVotoEleicaoBD->reativar($arrObjVotoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro reativando Voto no Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function bloquearControlado(VotoEleicaoDTO $objVotoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_eleicao_consultar', __METHOD__, $objVotoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoEleicaoBD = new VotoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoEleicaoBD->bloquear($objVotoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Voto no Escrut�nio Eletr�nico.',$e);
    }
  }

 */
}
