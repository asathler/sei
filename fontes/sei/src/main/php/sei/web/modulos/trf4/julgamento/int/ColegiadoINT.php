<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoINT extends InfraINT {

  public static function montarSelectNome($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrIdsExcluidos=null){
    $objColegiadoDTO = new ColegiadoDTO();
    $objColegiadoDTO->retNumIdColegiado();
    $objColegiadoDTO->retStrNome();
    $objColegiadoDTO->retStrSigla();

    if ($arrIdsExcluidos!=null) {
      $objColegiadoDTO->setNumIdColegiado($arrIdsExcluidos,InfraDTO::$OPER_NOT_IN);
    }

    if ($strValorItemSelecionado!=null){
      $objColegiadoDTO->setBolExclusaoLogica(false);
      $objColegiadoDTO->adicionarCriterio(array('SinAtivo','IdColegiado'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objColegiadoDTO->setOrdStrSigla(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objColegiadoRN = new ColegiadoRN();
    $arrObjColegiadoDTO = $objColegiadoRN->listar($objColegiadoDTO);
    foreach ($arrObjColegiadoDTO as $objColegiadoDTO) {
      $objColegiadoDTO->setStrNome($objColegiadoDTO->getStrSigla().' - '.$objColegiadoDTO->getStrNome());
    }


    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjColegiadoDTO, 'IdColegiado', 'Nome');
  }
  public static function montarSelectNomePermitidos($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objColegiadoDTO = new ColegiadoDTO();
    $objColegiadoDTO->retNumIdColegiado();
    $objColegiadoDTO->retStrNome();
    $objColegiadoDTO->retStrSigla();
    $objColegiadoDTO->retNumIdUnidadeResponsavel();


    if ($strValorItemSelecionado!=null){
      $objColegiadoDTO->setBolExclusaoLogica(false);
      $objColegiadoDTO->adicionarCriterio(array('SinAtivo','IdColegiado'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objColegiadoDTO->setOrdStrSigla(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objColegiadoRN = new ColegiadoRN();
    $arrObjColegiadoDTO = $objColegiadoRN->listar($objColegiadoDTO);

    if ($arrObjColegiadoDTO==null){
      return null;
    }

    $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();

    $arrIdColegiado=InfraArray::converterArrInfraDTO($arrObjColegiadoDTO,'IdColegiado');
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
    $objColegiadoComposicaoDTO->setNumIdUnidade($numIdUnidadeAtual);
    $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($arrIdColegiado,InfraDTO::$OPER_IN);
    $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
    $objColegiadoComposicaoDTO->setDistinct(true);
    $objColegiadoComposicaoDTO->setStrSinHabilitado('S');
    $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
    $arrIdColegiado=InfraArray::converterArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdColegiadoColegiadoVersao');



    foreach ($arrObjColegiadoDTO as $chave=>$objColegiadoDTO) {
      if($objColegiadoDTO->getNumIdUnidadeResponsavel()!=$numIdUnidadeAtual && !in_array($objColegiadoDTO->getNumIdColegiado(),$arrIdColegiado)){
        unset($arrObjColegiadoDTO[$chave]);
        continue;
      }
      $objColegiadoDTO->setStrNome($objColegiadoDTO->getStrSigla().' - '.$objColegiadoDTO->getStrNome());
    }


    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjColegiadoDTO, 'IdColegiado', 'Nome');
  }
  public static function montarSelectNomeAdministrados($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrIdsExcluidos=null){
    $objColegiadoDTO = new ColegiadoDTO();
    $objColegiadoDTO->retNumIdColegiado();
    $objColegiadoDTO->retStrNome();
    $objColegiadoDTO->retStrSigla();
    $objColegiadoDTO->setNumIdUnidadeResponsavel(SessaoSEI::getInstance()->getNumIdUnidadeAtual());

    if ($arrIdsExcluidos!=null) {
      $objColegiadoDTO->setNumIdColegiado($arrIdsExcluidos,InfraDTO::$OPER_NOT_IN);
    }

    if ($strValorItemSelecionado!=null){
      $objColegiadoDTO->setBolExclusaoLogica(false);
      $objColegiadoDTO->adicionarCriterio(array('SinAtivo','IdColegiado'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objColegiadoDTO->setOrdStrSigla(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objColegiadoRN = new ColegiadoRN();
    $arrObjColegiadoDTO = $objColegiadoRN->listar($objColegiadoDTO);
    foreach ($arrObjColegiadoDTO as $objColegiadoDTO) {
      $objColegiadoDTO->setStrNome($objColegiadoDTO->getStrSigla().' - '.$objColegiadoDTO->getStrNome());
    }


    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjColegiadoDTO, 'IdColegiado', 'Nome');
  }
  public static function montarSelectNomeVersao($numIdColegiadoVersao){
    $objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
    $objColegiadoVersaoDTO->retNumIdColegiado();
    $objColegiadoVersaoDTO->retStrNome();
    $objColegiadoVersaoDTO->retStrSigla();
    $objColegiadoVersaoDTO->setNumIdColegiadoVersao($numIdColegiadoVersao);
    $objColegiadoVersaoDTO->retDthVersao();

    $objColegiadoVersaoRN = new ColegiadoVersaoRN();
    $objColegiadoVersaoDTO = $objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);
    $objColegiadoVersaoDTO->setStrNome($objColegiadoVersaoDTO->getStrSigla().' - '.$objColegiadoVersaoDTO->getStrNome() .' - Vers�o de '.$objColegiadoVersaoDTO->getDthVersao());

    return parent::montarSelectArrInfraDTO(null, null, null, array($objColegiadoVersaoDTO), 'IdColegiado', 'Nome');
  }
  public static function montarArrUnidadesColegiado($numIdColegiado=null){
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();

    if($numIdColegiado!=null){
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
    }
    $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
    $objColegiadoComposicaoDTO->retNumIdUnidade();
    $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
    $objColegiadoComposicaoDTO->setDistinct(true);
    $objColegiadoComposicaoDTO->setOrdNumIdColegiadoColegiadoVersao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
    if ($arrObjColegiadoComposicaoDTO!=null) {
      $arrResultado=InfraArray::converterArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUnidade','IdColegiadoColegiadoVersao',true);
    } else {
      $arrResultado=null;
    }

    return $arrResultado;
  }
  public static function getUsuarioUnidadeAtualColegiado($numIdColegiado){
    $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
    $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
    $objColegiadoComposicaoDTO->retNumIdUsuario();
    $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
    if(InfraArray::contar($arrObjColegiadoComposicaoDTO)!=1){
      return null;
    }
    return $arrObjColegiadoComposicaoDTO[0]->getNumIdUsuario();
  }
}
?>