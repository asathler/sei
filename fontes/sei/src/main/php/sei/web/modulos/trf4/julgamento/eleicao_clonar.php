<?
/*
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/06/2020 - criado por mga
*
*
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento'));

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  $arrComandos = array();

  switch($_GET['acao']){

    case 'eleicao_clonar':

      $strTitulo = 'Clonar Escrut�nio';

      $arrComandos[] = '<button type="submit" accesskey="" id="sbmClonar" name="sbmClonar" value="Clonar" onclick="clonar();" class="infraButton">Clonar</button>';

      $objEleicaoRN = new EleicaoRN();

      if (isset($_GET['id_eleicao'])) {
        $objEleicaoDTO = new EleicaoDTO();
        $objEleicaoDTO->retNumIdEleicao();
        $objEleicaoDTO->retStrIdentificacao();
        $objEleicaoDTO->setNumIdEleicao($_GET['id_eleicao']);
        $objEleicaoDTO = $objEleicaoRN->consultar($objEleicaoDTO);
        if ($objEleicaoDTO == null) {
          throw new InfraException("Registro n�o encontrado.");
        }
      }else {
        $objEleicaoDTO = new EleicaoDTO();
        $objEleicaoDTO->setNumIdEleicao($_POST['hdnIdEleicao']);
        $objEleicaoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
        $objEleicaoDTO->setStrIdentificacao($_POST['txtIdentificacao']);
      }

      $arrComandos[] = '<button type="button" accesskey="" name="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())).'\';" class="infraButton">Cancelar</button>';

      if (isset($_POST['sbmClonar'])){

        try{

          $objEleicaoRN = new EleicaoRN();
          $objEleicaoDTOClone = $objEleicaoRN->clonar($objEleicaoDTO);

          header('Location:' .SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTOClone->getNumIdEleicao())));
          die;

        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }

      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

#lblIdentificacao {position:absolute;left:0%;top:0%;}
#txtIdentificacao {position:absolute;left:0%;top:40%;width:70%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
  //<script>

  function inicializar(){
    document.getElementById('txtIdentificacao').focus();
  }

  function OnSubmitForm() {
    if (infraTrim(document.getElementById('txtIdentificacao').value)==''){
      alert('Informe a Identifica��o.');
      document.getElementById('txtIdentificacao').focus();
      return false;
    }
    return true;
  }
  //</script>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>

  <form id="frmEleicaoClonar" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].'&acao_retorno='.PaginaSEI::getInstance()->getAcaoRetorno().$strParametros)?>">

    <?
    //PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    //PaginaSEI::getInstance()->montarAreaValidacao();
    ?>

    <div class="infraAreaDados" style="height:5em">
      <label id="lblIdentificacao" for="txtIdentificacao" accesskey="" class="infraLabelObrigatorio">Identifica��o:</label>
      <input type="text" id="txtIdentificacao" name="txtIdentificacao" class="infraText" value="<?=PaginaSEI::tratarHTML($objEleicaoDTO->getStrIdentificacao());?>" onkeypress="return infraMascaraTexto(this,event,250);" maxlength="250" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    </div>

    <input type="hidden" id="hdnIdEleicao" name="hdnIdEleicao" value="<?=$objEleicaoDTO->getNumIdEleicao();?>" />

  </form>
<?
PaginaSEI::getInstance()->montarAreaDebug();
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>