<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  ////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);

  $objPedidoVistaDTO=new PedidoVistaDTO();
  $objPedidoVistaRN=new PedidoVistaRN();


  $numIdColegiadoVersao = null;
  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '&id_distribuicao='.$_GET['id_distribuicao'];
  $mensagem='';
  $arrComandos = array();
  $arrIdDistribuicao = explode(',', $_GET['id_distribuicao']);
  $bolMultiplasDistribuicoes = count($arrIdDistribuicao)>1;


  switch($_GET['acao']){
    case 'pedido_vista_devolver_secretaria':
      $strTitulo = 'Devolver Processo para a Secretaria';

      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmDevolverPedidoVista" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      if(PaginaSEI::getInstance()->getAcaoRetorno()=='painel_distribuicao_detalhar'){
        $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="window.parent.infraFecharJanelaModal();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
      }else {
        $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
      }


      if($bolMultiplasDistribuicoes){
        $objPedidoVistaDTO->setNumIdDistribuicao($arrIdDistribuicao,InfraDTO::$OPER_IN);
        $numIdColegiadoVersao = $_POST['selColegiado'] ?? null;
      } else {
        $objPedidoVistaDTO->setNumIdDistribuicao($arrIdDistribuicao[0]);
      }
      $objPedidoVistaDTO->retNumIdDistribuicao();
      $objPedidoVistaDTO->retNumIdUsuario();
      $objPedidoVistaDTO->retNumIdUnidade();
      $objPedidoVistaDTO->retStrNomeColegiadoVersao();
      $objPedidoVistaDTO->retNumIdColegiadoVersaoDistribuicao();
      $arrObjPedidoVistaDTO=$objPedidoVistaRN->listar($objPedidoVistaDTO);

      if(count($arrObjPedidoVistaDTO)==0){
        throw new InfraException('Pedido de vista n�o encontrado.');
      }

      if (isset($_POST['sbmDevolverPedidoVista'])) {
        try{
          $objPedidoVistaDTO=null;
          if($bolMultiplasDistribuicoes){
            foreach ($arrObjPedidoVistaDTO as $objPedidoVistaDTO2) {
              if($objPedidoVistaDTO2->getNumIdColegiadoVersaoDistribuicao()==$numIdColegiadoVersao){
                $objPedidoVistaDTO=$objPedidoVistaDTO2;
                break;
              }
            }
          } else {
            $objPedidoVistaDTO=$arrObjPedidoVistaDTO[0];
          }
          if($objPedidoVistaDTO==null){
            throw new InfraException('Pedido de vista n�o selecionado.');
          }

          $objPedidoVistaDTO->setStrMotivo($_POST['txaMotivo']);
          $objPedidoVistaDTO->setDthDevolucao(InfraData::getStrDataHoraAtual());
          $objPedidoVistaRN->devolverSecretaria($objPedidoVistaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Processo devolvido com sucesso.');

          if(PaginaSEI::getInstance()->getAcaoRetorno()=='painel_distribuicao_detalhar'){
            $bolFechar=true;
          } else {
            $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_controlar&acao_origem='.$_GET['acao'].'&montar_visualizacao=1');
            header('Location: '.$strLinkRetorno);
            die;
          }

        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  //
  if ($bolMultiplasDistribuicoes) {
    $strItensSelColegiadoVersao = InfraINT::montarSelectArrInfraDTO('null', '&nbsp;', $numIdColegiadoVersao, $arrObjPedidoVistaDTO, 'IdColegiadoVersaoDistribuicao', 'NomeColegiadoVersao');
  } else {
    $strItensSelColegiadoVersao = InfraINT::montarSelectArrInfraDTO(null, '', $numIdColegiadoVersao, $arrObjPedidoVistaDTO, 'IdColegiadoVersaoDistribuicao', 'NomeColegiadoVersao');
  }




}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
#lblMotivo {position:absolute;left:0%;top:0%;width:50%;}
#txaMotivo {position:absolute;left:0%;top:12%;width:90%;}
<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
function inicializar(){
<?
if($bolFechar) { ?>
  window.parent.infraFecharJanelaModal();
<? } ?>
  <? if(!$bolMultiplasDistribuicoes){?>
  document.getElementById('selColegiado').disabled = true;
  <?}?>
  document.getElementById('txaMotivo').focus();
  infraEfeitoTabelas();
}

function validarCadastro() {
  if (infraTrim(document.getElementById('txaMotivo').value)=='') {
    alert('Informe o Motivo.');
    document.getElementById('txaMotivo').focus();
    return false;
  }
  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

function finalizar() {

}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();" onunload="finalizar();"');
?>
<form id="frmItemSessaoJulgamentoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>

  <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <select id="selColegiado" name="selColegiado" onchange="this.form.submit();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelColegiadoVersao ?>
  </select>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('15em');
?>

  <label id="lblMotivo" for="txaMotivo" accesskey="" class="infraLabelObrigatorio">Motivo:</label>
  <textarea id="txaMotivo" name="txaMotivo" class="infraTextarea" rows="3" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" ></textarea>

  <input type="hidden" id="hdnIdItemSessaoJulgamento" name="hdnIdItemSessaoJulgamento" value="" />
<?
PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>