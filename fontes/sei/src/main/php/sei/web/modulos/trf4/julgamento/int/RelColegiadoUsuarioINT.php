<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 30/05/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class RelColegiadoUsuarioINT extends InfraINT {

  public static function montarSelectIdUsuario($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdUsuario='', $numIdColegiado=''){
    $objRelColegiadoUsuarioDTO = new RelColegiadoUsuarioDTO();
    $objRelColegiadoUsuarioDTO->retNumIdUsuario();
    $objRelColegiadoUsuarioDTO->retNumIdColegiado();
    $objRelColegiadoUsuarioDTO->retNumIdUsuario();

    if ($numIdUsuario!==''){
      $objRelColegiadoUsuarioDTO->setNumIdUsuario($numIdUsuario);
    }

    if ($numIdColegiado!==''){
      $objRelColegiadoUsuarioDTO->setNumIdColegiado($numIdColegiado);
    }

    $objRelColegiadoUsuarioDTO->setOrdNumIdUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objRelColegiadoUsuarioRN = new RelColegiadoUsuarioRN();
    $arrObjRelColegiadoUsuarioDTO = $objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjRelColegiadoUsuarioDTO, 'IdUsuario', 'IdUsuario');
  }
}
?>