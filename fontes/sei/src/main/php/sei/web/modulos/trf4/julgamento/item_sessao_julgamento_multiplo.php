<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);

  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();


  $strDesabilitar = '';
  $strParametros='';

  if (isset($_GET['id_sessao_julgamento'])){
    $idSessaoJulgamento=$_GET['id_sessao_julgamento'];
    $strParametros.='&id_sessao_julgamento='.$idSessaoJulgamento;
  } else {
    $idSessaoJulgamento=$_POST['selSessao'];
  }






  $arrComandos = array();
  $strTitulo = 'Julgamento de M�ltiplos Processos';
  $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarVotoParte" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
//  $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';



  switch($_GET['acao']){
    case 'item_sessao_julgamento_julgar_multiplo':
      if (isset($_POST['hdnIdItens'])){
        $arrIds=explode(',',$_POST['hdnIdItens']);
      } else if ($_POST['hdnInfraItemId']!='') {
        $arrIds = array($_POST['hdnInfraItemId']);
      } else {
          $arrTabelasCheck=explode(',',$_POST['hdnInfraSelecoes']);
          $arrIds=array();
          foreach ($arrTabelasCheck as $tabela) {
            if ($tabela!='Infra' && $tabela!='membros') $arrIds=array_merge($arrIds,PaginaSEI::getInstance()->getArrStrItensSelecionados($tabela));
          }
      }
      $_POST['hdnIdItens']=implode(',',$arrIds);

      if(InfraArray::contar($arrIds)==0) {
        throw new InfraException('Nenhum processo selecionado.');
      }

      $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
      $objSessaoJulgamentoDTO->retStrNomeColegiado();
      $objSessaoJulgamentoDTO=$objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);

      $dtoBase = new ItemSessaoJulgamentoDTO();
      $dtoBase->setNumIdProvimento($_POST['selProvimento']);
      $dtoBase->setStrComplemento($_POST['txaComplemento']);

      if(isset($_POST['sbmCadastrarVotoParte'])){
        try {
          $arrObjItemSessaoJulgamentoDTO = array();
          foreach ($arrIds as $id) {
            $objItemSessaoJulgamentoDTO=clone $dtoBase;
            $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($id);
            $arrObjItemSessaoJulgamentoDTO[] = $objItemSessaoJulgamentoDTO;
          }
          $objItemSessaoJulgamentoRN->julgarMultiplo($arrObjItemSessaoJulgamentoDTO);
          $bolExecucaoOK = true;
        }catch(Exception $e) {
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensProvimento=ProvimentoINT::montarSelectConteudo('null','&nbsp;',$_POST['selProvimento']);



}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

#lblColegiado {position:absolute;left:0%;top:0%;width:40%;}
#txtColegiado {position:absolute;left:0%;top:10%;width:40%;}

#divSinProvimento {position:absolute;left:0;top:25%;width:95%;}
  #lblProvimento {position:absolute;left:0%;top:39%;width:95%;}
  #selProvimento {position:absolute;left:0%;top:48%;width:95%;}
  #lblComplemento {position:absolute;left:0%;top:60%;width:95%;}
  #txaComplemento {position:absolute;left:0%;top:70%;width:95%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
function inicializar(){
  <?if ($bolExecucaoOK) {?>
  window.parent.infraFecharJanelaModal();
  window.parent.$('input[type=checkbox]:checked').click();
  window.parent.document.getElementById('frmSessaoJulgamentoCadastro').submit();
  <? } ?>

  document.getElementById('txtColegiado').disabled=true;
  // document.getElementById('btnCancelar').focus();
  exibeProvimento();
  infraEfeitoTabelas();
}
function exibeProvimento(){
  var chk=document.getElementById('chkSinProvimento').checked;
  if (chk){
    $('#divProvimento').hide();
    $('#txaComplemento').val('');
    $('#selProvimento').val(null);
  } else {
    $('#divProvimento').show();
  }
}
function onSubmitForm() {
  if(!infraSelectSelecionado('selProvimento')) {
    alert('Selecione um Provimento.');
    document.getElementById('selProvimento').focus();
    return false;
  }
  return true;
}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmItemSessaoJulgamentoJulgar" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('20em');
?>

  <label id="lblColegiado" for="txtColegiado" accesskey="o" class="infraLabelObrigatorio">C<span class="infraTeclaAtalho">o</span>legiado:</label>
  <input type="text" id="txtColegiado" name="txtColegiado" class="infraText" value="<?=PaginaSEI::tratarHTML($objSessaoJulgamentoDTO->getStrNomeColegiado()); ?>" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>

  <div id="divSinProvimento" class="infraDivCheckbox">
    <input type="checkbox" id="chkSinProvimento" name="chkSinProvimento" class="infraCheckbox" checked="checked" onchange="exibeProvimento()" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>"/>
    <label id="lblSinProvimento" for="chkSinProvimento" accesskey="" class="infraLabelCheckbox">Utilizar o provimento informado anteriormente</label>
  </div>

  <input type="hidden" id="hdnFinalizar" name="hdnFinalizar" value="" />
  <div id="divProvimento">
    <label id="lblProvimento" for="selProvimento" accesskey="" class="infraLabelObrigatorio">Provimento:</label>
    <select id="selProvimento" name="selProvimento" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
      <?= $strItensProvimento ?>
    </select>
    <label id="lblComplemento" for="txaComplemento" accesskey="" class="infraLabelOpcional">Complemento:</label>
    <textarea id="txaComplemento" name="txaComplemento" rows="3" class="infraTextarea"
              tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?= PaginaSEI::tratarHTML($_POST['txaComplemento']) ?></textarea>
  </div>


    <input type="hidden" id="hdnIdItens" name="hdnIdItens" value="<?=$_POST['hdnIdItens'];?>">
  <?
  PaginaSEI::getInstance()->fecharAreaDados();

  ?>
  <br />
  <?
//  PaginaSEI::getInstance()->montarAreaTabela($strResultado, $numRegistros);
  ?>

</form>
<?
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>

<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>