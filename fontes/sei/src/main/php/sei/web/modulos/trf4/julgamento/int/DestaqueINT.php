<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class DestaqueINT extends InfraINT {

  public static function montarSelectStaTipo($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado,$staItem=null,$bolAlterar=false): string
  {
    $objDestaqueRN = new DestaqueRN();

    $arrObjTipoDestaqueDTO = $objDestaqueRN->listarValoresTipoDestaque();

    if($staItem==TipoSessaoBlocoRN::$STA_REFERENDO){
      $arrObjTipoDestaqueDTO=InfraArray::indexarArrInfraDTO($arrObjTipoDestaqueDTO,'StaTipo');
      unset($arrObjTipoDestaqueDTO[DestaqueRN::$TD_VISTA], $arrObjTipoDestaqueDTO[DestaqueRN::$TD_AGUARDA_VISTA]);

    }

    $ret = '<option value="null" '.($strValorItemSelecionado===null?'selected="selected"':'').'>&nbsp;</option>'."\n";

    foreach ($arrObjTipoDestaqueDTO as $objTipoDestaqueDTO) {
      if($bolAlterar) {
        if (($strValorItemSelecionado==DestaqueRN::$TD_COMENTARIO_RESTRITO && $objTipoDestaqueDTO->getStrStaTipo()!=DestaqueRN::$TD_COMENTARIO_RESTRITO) ||
            ($strValorItemSelecionado!=DestaqueRN::$TD_COMENTARIO_RESTRITO && $objTipoDestaqueDTO->getStrStaTipo()==DestaqueRN::$TD_COMENTARIO_RESTRITO)) {
          continue;
        }
      }
      $ret .= '<option '.(($objTipoDestaqueDTO->getStrStaTipo()==$strValorItemSelecionado)?'selected="selected"':'').' value="' .$objTipoDestaqueDTO->getStrStaTipo() . '" data-imagesrc="'.$objTipoDestaqueDTO->getStrIcone().'">'.$objTipoDestaqueDTO->getStrDescricao().'</option>'."\n";
    }

    return $ret;

  }


  /**
   * Monta �cone de destaques, retorna string do �cone e se possui destaque nao lido
   *
   * @param DestaqueDTO[] $arrObjDestaqueDTO
   * @param int $numIdColegiado
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param int $numOrdemSequencial
   * @param int $numIdItemSessaoJulgamento
   * @param int $numQtdDestaques
   * @return string
   * @throws InfraException
   */
  public static function montarIconeDestaques(array $arrObjDestaqueDTO, int $numIdColegiado, SessaoJulgamentoDTO $objSessaoJulgamentoDTO, int $numOrdemSequencial, int $numIdItemSessaoJulgamento, &$numQtdDestaques): string
  {
    $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
    $strStaSituacaoSessaoJulgamento=$objSessaoJulgamentoDTO->getStrStaSituacao();
    $strDestaque = '';
    $numQtdDestaques = 0;
    if ($arrObjDestaqueDTO===null) {
      $arrObjDestaqueDTO = [];
    }
    $bolAcaoDestaques = ($strStaSituacaoSessaoJulgamento!==SessaoJulgamentoRN::$ES_CANCELADA && $strStaSituacaoSessaoJulgamento!==SessaoJulgamentoRN::$ES_PAUTA_ABERTA &&
        (!in_array($strStaSituacaoSessaoJulgamento, array(SessaoJulgamentoRN::$ES_ENCERRADA, SessaoJulgamentoRN::$ES_FINALIZADA)) || count($arrObjDestaqueDTO)>0));
    $arrIcones=[];
    $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
    if($objSessaoJulgamentoDTO->isSetArrObjColegiadoComposicaoDTO()){
      $arrObjColegiadoComposicaoDTO=$objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO();
    } else {
      $arrObjColegiadoComposicaoDTO=CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    }

    $objDestaqueRN=new DestaqueRN();
    $arrObjTipoDestaqueDTO=$objDestaqueRN->listarValoresTipoDestaque();
    $arrObjTipoDestaqueDTO=InfraArray::indexarArrInfraDTO($arrObjTipoDestaqueDTO,'StaTipo');
    $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUnidade');
    if ($bolAcaoDestaques) {
      if($objSessaoJulgamentoRN->permiteIncluirDestaque($objSessaoJulgamentoDTO)) {
        $arrIcones['@'] = ['usuarios'=>''];
      }
      /** @var DestaqueDTO[] $arrObjDestaqueDTO */
      foreach ($arrObjDestaqueDTO as $objDestaqueDTO) {
        $staTipoDestaque = $objDestaqueDTO->getStrStaTipo();
        if ($objDestaqueDTO->getNumIdUnidade()!=$numIdUnidadeAtual && $objDestaqueDTO->getStrStaAcesso()==DestaqueRN::$TA_RESTRITO) {
          continue;
        }
        if ($staTipoDestaque==DestaqueRN::$TD_DIVERGENCIA) {
          $numQtdDestaques ++;
        }

        if (!isset($arrIcones[$staTipoDestaque])) {
          $arrIcones[$staTipoDestaque] = ['visualizado'=>false,'usuarios'=>'','quantidade'=>1];
        } else {
          $arrIcones[$staTipoDestaque]['quantidade']++;
        }

        $separador='';
        if($arrIcones[$staTipoDestaque]['usuarios']!==''){
          $separador='<hr>';
        }
        $numIdUnidadeDestaque=$objDestaqueDTO->getNumIdUnidade();
        if($numIdUnidadeDestaque!=$numIdUnidadeAtual && isset($arrObjColegiadoComposicaoDTO[$numIdUnidadeDestaque])) {
          $arrIcones[$staTipoDestaque]['usuarios'] .= $separador .substr($objDestaqueDTO->getDthDestaque(),0,-3).' - '. $arrObjColegiadoComposicaoDTO[$numIdUnidadeDestaque]->getStrNomeUsuario() . ' (' . $objDestaqueDTO->getStrSiglaUnidade() . ')';
        } else {
          $arrIcones[$staTipoDestaque]['usuarios'] .= $separador .substr($objDestaqueDTO->getDthDestaque(),0,-3).' - '. $objDestaqueDTO->getStrNomeUsuario() . ' (' . $objDestaqueDTO->getStrSiglaUnidade() . ')';
        }

        $strTextoDestaque = trim($objDestaqueDTO->getStrDescricao());
        if ($strTextoDestaque!='') {
          if (strlen($strTextoDestaque) > 500) {
            $strTextoDestaque = substr($strTextoDestaque, 0, 500).'...';
          }
          $arrIcones[$staTipoDestaque]['usuarios'] .= "\n".$strTextoDestaque;
        }

        if ($objDestaqueDTO->getStrSinVisualizado()==='N') {
          $arrIcones[$staTipoDestaque]['visualizado'] = true;
        }
      }


      foreach ($arrIcones as $chave=>$valor) {
        if ($chave=='@') {
          $strImagem = MdJulgarIcone::DESTAQUE_INCLUIR;
          $strTooltip = 'Incluir Destaque';
        } else {
          $strTooltip = $arrObjTipoDestaqueDTO[$chave]->getStrTooltip();
          $strImagem = $arrObjTipoDestaqueDTO[$chave]->getStrIcone();
          if($valor['quantidade']>0){
            $strTooltip.= ' ('.$valor['quantidade'].')';
          }

          if ($valor['visualizado']) {
            $strImagem=MdJulgarIcone::prepararIconeBase64($strImagem,MdJulgarIcone::COR_VERMELHO);
          } else {
            $strImagem=MdJulgarIcone::prepararIconeBase64($strImagem,MdJulgarIcone::COR_VERDE);
          }
        }

//        if(strlen($valor['usuarios'])>500){
//          $valor['usuarios']=substr($valor['usuarios'],0,500).'...';
//        }
        $strLink = SessaoJulgamentoINT::montarLink('destaque_listar', $_GET['acao'], $numIdItemSessaoJulgamento, '&id_colegiado=' . $numIdColegiado . '&sta_tipo=' . $chave . '&num_seq=' . $numOrdemSequencial);
        $strDestaque .= '<a onclick="event.stopPropagation();" href="javascript:void(0)" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '">';
        $strDestaque .= '<img src="' . $strImagem . '" onclick="marcarTrAcessadaElemento(this);infraAbrirJanelaModal(\'' . $strLink . '\',800,600,false);" tabindex="' .
            PaginaSEI::getInstance()->getProxTabTabela() .'"'. PaginaSEI::montarTitleTooltip($valor['usuarios'], $strTooltip) .'style="vertical-align:middle" class="infraImg';
        $strDestaque .= '" />';
        $strDestaque .= '</a>&nbsp;';
      }
    }
    return $strDestaque;
  }

  /**
   * Monta �cone de coment�rios restritos, retorna string do �cone
   *
   * @param $strParametros
   * @param $arrObjDestaqueDTOPar
   * @param $numIdItemSessaoJulgamento
   * @param $numIdColegiado
   * @return string
   */
  public static function montarIconeComentariosRestritos($strParametros, $arrObjDestaqueDTOPar, $numIdItemSessaoJulgamento, $numIdColegiado): string
  {
    return '';
//    if($arrObjDestaqueDTOPar==null){
//      $arrObjDestaqueDTO=array();
//    } else {
//      $arrObjDestaqueDTO=InfraArray::filtrarArrInfraDTO($arrObjDestaqueDTOPar,'StaAcesso',DestaqueRN::$TA_RESTRITO);
//    }
//    $qtd = InfraArray::contar($arrObjDestaqueDTO);
//    $cor = '#0056ff';
//    if ($qtd>0) {
//
//      foreach ($arrObjDestaqueDTO as $objDestaqueDTO) {
//        if ($objDestaqueDTO->getStrSinVisualizado()==='N') {
//          $cor = 'red';
//          break;
//        }
//      }
//    }
//    $strLink = SessaoJulgamentoINT::montarLink('destaque_listar', $_GET['acao'], $numIdItemSessaoJulgamento, '&id_colegiado=' . $numIdColegiado . '&sta_acesso=' . DestaqueRN::$TA_RESTRITO . $strParametros);
//    $strDestaques = '<a class="espacamento" href="javascript:void(0);" onclick="event.stopPropagation();infraAbrirJanelaModal(\'' . $strLink . '\',800,600,false);" title="Coment�rios Restritos" alt="Coment�rios Restritos" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '">';
////    $strDestaques .= PaginaSEI::getInstance()->montarSpanNotificacao($qtd, $cor, 'style="top:-4px;" title="Coment�rios Restritos" alt="Coment�rios Restritos"');
//
//    $align=PaginaSEI::getInstance()->isBolNavegadorIE()?'middle':'baseline';
//    $cx=PaginaSEI::getInstance()->isBolNavegadorFirefox()?'47':'50';
//    $strSVG='<svg id="svgComentario'.$numIdItemSessaoJulgamento.'" width="100" height="100" viewBox="0 0 100 100" style="height: 24px;width: 24px;vertical-align: '.$align.';">
//  <g id="layer1">
//    <circle style="fill: '.$cor.';" cx="'.$cx.'" cy="50" r="45"></circle>
//    <text style="text-anchor: middle;font-size:48px;fill:#ffffff;" x="49%" y="67">'.$qtd.'</text>
//  </g>
//</svg>';
//    $strDestaques .= $strSVG.'</a>';
//    return $strDestaques;
  }
}
?>