<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 25/04/2012 - criado por mga
*
*
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();
 
  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////
	
  SessaoSEIExterna::getInstance()->validarLink();

  switch($_GET['acao']){

    case 'sessao_julgamento_observar':
      $strTitulo = 'Sess�es de Julgamento';
      break;

    default:
      throw new InfraException("A��o '${_GET['acao']}' n�o reconhecida.");
  }

  $idSessaoJulgamento=null;
  if(isset($_POST['selSessao'])){
    $idSessaoJulgamento=$_POST['selSessao'];
  }

  $arrTabelasPauta=array();
  $arrTabelasMesa=array();

  $objRelColegiadoUsuarioDTO=new RelColegiadoUsuarioDTO();
  $objRelColegiadoUsuarioRN=new RelColegiadoUsuarioRN();
  $objRelColegiadoUsuarioDTO->setNumIdUsuario(SessaoSEIExterna::getInstance()->getNumIdUsuarioExterno());
  $objRelColegiadoUsuarioDTO->retNumIdColegiado();
  $arrObjRelColegiadoUsuarioDTO=$objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO);

  $arrSessoes=array();
  if(InfraArray::contar($arrObjRelColegiadoUsuarioDTO)>0){
    $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
    $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO->setNumIdColegiado(InfraArray::converterArrInfraDTO($arrObjRelColegiadoUsuarioDTO,'IdColegiado'),InfraDTO::$OPER_IN);
    $objSessaoJulgamentoDTO->setStrStaSituacao(array(SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_SUSPENSA),InfraDTO::$OPER_IN);
    $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
    $objSessaoJulgamentoDTO->retNumIdColegiado();
    $objSessaoJulgamentoDTO->retNumIdColegiadoVersao();
    $objSessaoJulgamentoDTO->retStrSiglaColegiado();
    $objSessaoJulgamentoDTO->retStrNomeColegiado();
    $objSessaoJulgamentoDTO->retDthSessao();
    $objSessaoJulgamentoDTO->retStrStaSituacao();
    $objSessaoJulgamentoDTO->retNumIdUsuarioPresidenteColegiado();
    $arrObjSessaoJulgamentoDTO=$objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);
    if (InfraArray::contar($arrObjSessaoJulgamentoDTO)>0){
      $arrObjSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjSessaoJulgamentoDTO,'IdSessaoJulgamento');
    }
    foreach ($arrObjSessaoJulgamentoDTO as $id=>$objSessaoJulgamentoDTO) {

      $strSessao=$objSessaoJulgamentoDTO->getStrNomeColegiado().' - '.substr($objSessaoJulgamentoDTO->getDthSessao(),0,16);
      $arrSessoes[$id]=$strSessao;
    }
  }

  $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
  $bolPermiteVisualizacaoPautaFechada=($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_ACESSO_OBSERVADOR_EXTERNO,false)==1);

  $arrSituacaoPermitidaDocumento=array(SessaoJulgamentoRN::$ES_ABERTA.SessaoJulgamentoRN::$ES_SUSPENSA);
  if($bolPermiteVisualizacaoPautaFechada){
    $arrSituacaoPermitidaDocumento[]=SessaoJulgamentoRN::$ES_PAUTA_FECHADA;
  }


  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
  $numRegistros=0;
  if($idSessaoJulgamento!=null){
    $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
    if(isset($arrObjSessaoJulgamentoDTO[$idSessaoJulgamento])){

      $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
      $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
      $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinDocumentos('S');

      $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);

      $bolSessaoVirtual=$objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S';

      $arrObjItemSessaoJulgamentoDTO=$objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();


      $numRegistros=InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
      $idUsuarioPresidenteColegiado=$arrObjSessaoJulgamentoDTO[$idSessaoJulgamento]->getNumIdUsuarioPresidenteColegiado();
    } else {
      $idSessaoJulgamento=null;
    }
  }
  $strItensSelSessao=InfraINT::montarSelectArray('null','&nbsp;',$idSessaoJulgamento,$arrSessoes);
  $arrSituacaoItem=InfraArray::converterArrInfraDTO($objItemSessaoJulgamentoRN->listarValoresStaSituacao(),'Descricao','StaTipo');

  $objDistribuicaoRN=new DistribuicaoRN();
  $arrObjEstadoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
  $arrObjEstadoDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjEstadoDistribuicaoDTO, 'StaDistribuicao');

  $arrComandos = array();
  $arrComandos[] = '<button type="submit" id="btnAtualizar" name="btnAtualizar" value="Atualizar" class="infraButton">Atualizar</button>';

  $arrObjTabelaHtmlDTO=[];
  $arrObjSessaoBlocoDTO=CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($idSessaoJulgamento);
  if ($numRegistros){
    $arrObjProtocoloDTO=CacheSessaoJulgamentoRN::pesquisaAcessoDocumentos($arrObjItemSessaoJulgamentoDTO);

    $arrObjColegiadoComposicaoDTO=CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($idSessaoJulgamento);

    $arrObjItemSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdSessaoBloco', true);
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoDTO->setNumIdUsuario(-1);
    $objColegiadoComposicaoDTO->setStrNomeUsuario('Processos colocados em mesa ap�s a abertura da Sess�o');
    $objColegiadoComposicaoDTO->setNumIdUnidade(-1);
    $arrObjColegiadoComposicaoDTO[-1]=$objColegiadoComposicaoDTO;

    foreach ($arrObjItemSessaoJulgamentoDTO as $numIdSessaoBloco=>$arrObjItemSessaoJulgamentoDTO2) {
      $objSessaoBlocoDTO=$arrObjSessaoBlocoDTO[$numIdSessaoBloco];
      if ($objSessaoBlocoDTO->getStrSinAgruparMembro()=='S') {
        //s� separa os itens inclu�dos ap�s a abertura da sess�o se for mesa
        $dthInicioSessao = $objSessaoJulgamentoDTO->getDthInicio();
        $arrItensSessaoAberta = array();
        if ($dthInicioSessao!=null && $objSessaoBlocoDTO->getStrStaTipoItem()==TipoSessaoBlocoRN::$STA_MESA) {
          /**
           * @var ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
           */
          foreach ($arrObjItemSessaoJulgamentoDTO2 as $chave => $objItemSessaoJulgamentoDTO) {
            if (InfraData::compararDataHorasSimples($dthInicioSessao, $objItemSessaoJulgamentoDTO->getDthInclusao())>=1) {
              $objItemSessaoJulgamentoDTO->setStrNomeUsuarioRelator($arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()]->getStrNomeUsuario());
              $arrItensSessaoAberta[] = $objItemSessaoJulgamentoDTO;
              unset($arrObjItemSessaoJulgamentoDTO2[$chave]);
            }
          }
        }
        $arrObjItemSessaoJulgamentoDTO2 = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO2, 'IdUsuarioSessao', true);
        if (count($arrItensSessaoAberta)>0) {
          $arrObjItemSessaoJulgamentoDTO2[-1] = $arrItensSessaoAberta;
        }


        $numRegistrosBloco = 0;
        //pauta
        foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
          $numIdUsuario = $objColegiadoComposicaoDTO->getNumIdUsuario();
          if (isset($arrObjItemSessaoJulgamentoDTO2[$numIdUsuario])) {
            $arrObjItemSessaoJulgamentoDTO = $arrObjItemSessaoJulgamentoDTO2[$objColegiadoComposicaoDTO->getNumIdUsuario()];
            $idTabela = $numIdSessaoBloco . 'T' . $numIdUsuario;
            $numOrdemSequencial = $numRegistrosBloco;
            $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);

            $arrObjItemSessaoJulgamentoDTO[0]->setArrObjColegiadoComposicaoDTO(array($objColegiadoComposicaoDTO));
            $strResultado = SessaoJulgamentoINT::montarTabelaItemObservador($objSessaoJulgamentoDTO, $arrObjItemSessaoJulgamentoDTO, $numOrdemSequencial);

            $numRegistrosBloco += $numRegistros;
            $objTabelaHtmlDTO = new TabelaHtmlDTO();
            $objTabelaHtmlDTO->setNumIdSessaoBloco($numIdSessaoBloco);
            $objTabelaHtmlDTO->setNumRegistros($numRegistros);
            $objTabelaHtmlDTO->setStrHtml($strResultado);
            $objTabelaHtmlDTO->setStrIdTabela($idTabela);
            $arrObjTabelaHtmlDTO[] = $objTabelaHtmlDTO;
          }
        }

      } else {

        $numOrdemSequencial = 0;
        $numRegistrosBloco = InfraArray::contar($arrObjItemSessaoJulgamentoDTO2);

        $strResultado = SessaoJulgamentoINT::montarTabelaItemObservador($objSessaoJulgamentoDTO, $arrObjItemSessaoJulgamentoDTO2, $numOrdemSequencial);
        $objTabelaHtmlDTO = new TabelaHtmlDTO();
        $objTabelaHtmlDTO->setNumIdSessaoBloco($numIdSessaoBloco);
        $objTabelaHtmlDTO->setNumRegistros($numRegistrosBloco);
        $objTabelaHtmlDTO->setStrHtml($strResultado);
        $objTabelaHtmlDTO->setStrIdTabela($numIdSessaoBloco . 'T');
        $arrObjTabelaHtmlDTO[] = $objTabelaHtmlDTO;
      }
    }


    $arrObjTabelaHtmlDTO=InfraArray::indexarArrInfraDTO($arrObjTabelaHtmlDTO,'IdSessaoBloco',true);




  }

  SessaoSEIExterna::getInstance()->configurarAcessoExterno(null);

}catch(Exception $e){
  PaginaSEIExterna::getInstance()->processarExcecao($e);
} 


PaginaSEIExterna::getInstance()->montarDocType();
PaginaSEIExterna::getInstance()->abrirHtml();
PaginaSEIExterna::getInstance()->abrirHead();
PaginaSEIExterna::getInstance()->montarMeta();
PaginaSEIExterna::getInstance()->montarTitle(PaginaSEIExterna::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEIExterna::getInstance()->montarStyle();
PaginaSEIExterna::getInstance()->abrirStyle();
if(0){?><style><?}
?>
#lblSessao {position:absolute;top:0%;left:0;}
#selSessao {position:absolute;top:40%;left:0%;width:50%}
.resumo {float:right;color:white;font-size:0.9em;padding-right: 3px;position: relative;top:2px;}
#lblTabelaPauta {position:absolute;left:0;top:0;width:99%;}
#lblTabelaMesa {position:absolute;left:0;top:0;width:99%;}
#lblTabelaReferendo {position:absolute;left:0;top:0;width:99%;}

label.infraLabelTitulo {font-weight:normal;}

<?
if(0){?></style><?}
PaginaSEIExterna::getInstance()->fecharStyle();
PaginaSEIExterna::getInstance()->montarJavaScript();
PaginaSEIExterna::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

function inicializar(){
  infraEfeitoTabelas();
}
function exibe(sel,image) {
  var cookie = '<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>' + '_' + sel.substr(1);
  var img;
  $(sel).toggle();
  if (image) {
    img = $(image);
  } else {
    img = $(sel).prev().find('img');
  }

  if (img.attr('src') == '<?=PaginaSEI::getInstance()->getIconeExibir()?>') {
    img.attr('src', '<?=PaginaSEI::getInstance()->getIconeOcultar()?>');
    infraCriarCookie(cookie, "Fechar", 356);
  } else {
    img.attr('src', '<?=PaginaSEI::getInstance()->getIconeExibir()?>');
    infraRemoverCookie(cookie);
  }
}
<?
if(0){?></script><?}
PaginaSEIExterna::getInstance()->fecharJavaScript();
PaginaSEIExterna::getInstance()->fecharHead();
PaginaSEIExterna::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmUsuarioExternoControle" method="post" action="<?=SessaoSEIExterna::getInstance()->assinarLink('controlador_externo.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">

<?
PaginaSEIExterna::getInstance()->montarBarraComandosSuperior($arrComandos);
PaginaSEIExterna::getInstance()->abrirAreaDados('6em');
?>
  <label id="lblSessao" for="selSessao" accesskey="" class="infraLabelObrigatorio">Colegiado / Data da Sess�o:</label>
  <select id="selSessao" name="selSessao" onchange="this.form.submit();" class="infraSelect" tabindex="<?=      PaginaSEIExterna::getInstance()->getProxTabDados()?>" >
    <?=$strItensSelSessao?>
  </select>
  <?
PaginaSEIExterna::getInstance()->fecharAreaDados();
if($idSessaoJulgamento!=null) {
  // inicio blocos de sessao
  foreach ($arrObjSessaoBlocoDTO as $objSessaoBlocoDTO) {
    $idSessaoBloco=$objSessaoBlocoDTO->getNumIdSessaoBloco();
    $strDescricao=$objSessaoBlocoDTO->getStrDescricao();
    $numRegistrosBloco=0;
    if (isset($arrObjTabelaHtmlDTO[$idSessaoBloco])) {
      foreach ($arrObjTabelaHtmlDTO[$idSessaoBloco] as $objTabelaHtmlDTO) {
        $numRegistrosBloco+=$objTabelaHtmlDTO->getNumRegistros();
      }
    }
    PaginaSEI::getInstance()->abrirAreaDados('3.4em');

    echo "<label id='lblTabela$idSessaoBloco' style='display:block;position:absolute' class='infraLabelTitulo infraCorBarraSuperior'>\n";
    echo "<img onclick='exibe(\"#divBloco$idSessaoBloco\",this);' alt='Exibir/Ocultar Pauta' title='Exibir/Ocultar Pauta' src='".PaginaSEI::getInstance()->getIconeOcultar()."' />";


    echo ' '.$strDescricao.'<div class="resumo">';
    if ($numRegistrosBloco) {
      if ($numRegistrosBloco==1) {
        echo '1 Item';
      } else {
        echo $numRegistrosBloco . ' Itens';
      }
    }
    echo "</div></label>";
    PaginaSEI::getInstance()->fecharAreaDados();

    echo "<div id='divBloco$idSessaoBloco'>";

    /** @var TabelaHtmlDTO[][] $arrObjTabelaHtmlDTO */
    if (isset($arrObjTabelaHtmlDTO[$idSessaoBloco])) {
      foreach ($arrObjTabelaHtmlDTO[$idSessaoBloco] as $objTabelaHtmlDTO) {
        PaginaSEI::getInstance()->montarAreaTabela($objTabelaHtmlDTO->getStrHtml(), $objTabelaHtmlDTO->getNumRegistros(), true);
      }
    } else {
      PaginaSEI::getInstance()->montarAreaTabela('', null, true);
      echo '<br />';
    }
    echo '</div>';
  }

}
?>
</form>
<?
PaginaSEIExterna::getInstance()->montarAreaDebug();
PaginaSEIExterna::getInstance()->fecharBody();
PaginaSEIExterna::getInstance()->fecharHtml();
?>