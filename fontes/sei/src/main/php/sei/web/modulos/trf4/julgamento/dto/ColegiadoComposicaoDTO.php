<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoComposicaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'colegiado_composicao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdColegiadoComposicao','id_colegiado_composicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdColegiadoVersao','id_colegiado_versao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidade','id_unidade');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdTipoMembroColegiado','id_tipo_membro_colegiado');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinRodada','sin_rodada');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'Ordem','ordem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL,'Peso','peso');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdCargo','id_cargo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinHabilitado','sin_habilitado');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoColegiadoVersao','id_colegiado','colegiado_versao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SinUltimaColegiadoVersao','sin_ultima','colegiado_versao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeTipoMembroColegiado','nome','tipo_membro_colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'ExpressaoCargo','expressao','cargo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaGeneroCargo','sta_genero','cargo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUsuario','sigla','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SinAtivoColegiado','sin_ativo','colegiado');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinPresente');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'ComposicaoAnterior');

    $this->configurarPK('IdColegiadoComposicao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdColegiadoVersao', 'colegiado_versao', 'id_colegiado_versao');
    $this->configurarFK('IdColegiadoColegiadoVersao', 'colegiado', 'id_colegiado');
    $this->configurarFK('IdTipoMembroColegiado', 'tipo_membro_colegiado', 'id_tipo_membro_colegiado');
    $this->configurarFK('IdUnidade', 'unidade', 'id_unidade');
    $this->configurarFK('IdUsuario', 'usuario', 'id_usuario');
    $this->configurarFK('IdCargo', 'cargo', 'id_cargo',InfraDTO::$TIPO_FK_OPCIONAL);
  }
}
?>