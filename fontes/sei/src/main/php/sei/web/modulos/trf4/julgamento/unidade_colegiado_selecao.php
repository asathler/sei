<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2008 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.14.0
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__ .'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('unidade_selecionar_colegiado');

//  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  
  PaginaSEI::getInstance()->salvarCamposPost(array('txtSiglaUnidade','txtDescricaoUnidade'));


  switch($_GET['acao']){

    case 'unidade_selecionar_colegiado':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Unidade','Selecionar Unidades');
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();
  
  $arrComandos[] = '<input type="submit" id="btnPesquisar" value="Pesquisar" class="infraButton" />';  
  
  $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';

  $arrUnidadesNaoPermitidas=array(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
  $numIdItemSessaoJulgamento=$_GET['id_item_sessao_julgamento'];

  $strParametros='&id_item_sessao_julgamento='.$numIdItemSessaoJulgamento;

  if(isset($_GET['id_item_sessao_documento'])){
    $numIdItemSessaoDocumento=$_GET['id_item_sessao_documento'];
    $strParametros.='&id_item_sessao_documento='.$numIdItemSessaoDocumento;
//TODO: falta filtrar se for por documento
  } else{
    $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
    $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retNumIdUnidadeSessao();
    $objItemSessaoJulgamentoDTO->retNumIdUnidadeRelatorDistribuicao();
    $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
    $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

    $arrUnidadesNaoPermitidas[]=$objItemSessaoJulgamentoDTO->getNumIdUnidadeSessao();
    $arrUnidadesNaoPermitidas[]=$objItemSessaoJulgamentoDTO->getNumIdUnidadeRelatorDistribuicao();

    $objPedidoVistaDTO=new PedidoVistaDTO();
    $objPedidoVistaRN=new PedidoVistaRN();
    $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
    $objPedidoVistaDTO->setDthDevolucao(null);
    $objPedidoVistaDTO->retNumIdUnidade();
    $arrObjPedidoVistaDTO=$objPedidoVistaRN->listar($objPedidoVistaDTO);
    foreach ($arrObjPedidoVistaDTO as $objPedidoVistaDTO) {
      $arrUnidadesNaoPermitidas[]=$objPedidoVistaDTO->getNumIdUnidade();
    }

    $idColegiado=$objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento();
    $arrUnidadesNaoPermitidas=array_unique($arrUnidadesNaoPermitidas);
  }


  $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
  $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($idColegiado);
  $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
  $objColegiadoComposicaoDTO->setNumIdUnidade($arrUnidadesNaoPermitidas,InfraDTO::$OPER_NOT_IN);
  $objColegiadoComposicaoDTO->retNumIdUnidade();
  $objColegiadoComposicaoDTO->retStrSiglaUnidade();
  $objColegiadoComposicaoDTO->retStrDescricaoUnidade();
  $objColegiadoComposicaoDTO->retStrNomeTipoMembroColegiado();


  PaginaSEI::getInstance()->prepararOrdenacao($objColegiadoComposicaoDTO, 'SiglaUnidade', InfraDTO::$TIPO_ORDENACAO_ASC);
  PaginaSEI::getInstance()->prepararPaginacao($objColegiadoComposicaoDTO);

  $strSiglaPesquisa = PaginaSEI::getInstance()->recuperarCampo('txtSiglaUnidade');
  if ($strSiglaPesquisa!==''){
    $objColegiadoComposicaoDTO->setStrSiglaUnidade($strSiglaPesquisa);
  }
  
  $strDescricaoPesquisa = PaginaSEI::getInstance()->recuperarCampo('txtDescricaoUnidade');
  if ($strDescricaoPesquisa!==''){
    $objColegiadoComposicaoDTO->setStrDescricaoUnidade($strDescricaoPesquisa);
  }
  
  $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
  
  $arrObjColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);

  
  PaginaSEI::getInstance()->processarPaginacao($objColegiadoComposicaoDTO);
  $numRegistros = InfraArray::contar($arrObjColegiadoComposicaoDTO);

  if ($numRegistros > 0){

    $bolCheck = true;

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Unidades.';
    $strCaptionTabela = 'Unidades';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoComposicaoDTO,'Sigla','SiglaUnidade',$arrObjColegiadoComposicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoComposicaoDTO,'Descri��o','DescricaoUnidade',$arrObjColegiadoComposicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoComposicaoDTO,'Titularidade','NomeTipoMembroColegiado',$arrObjColegiadoComposicaoDTO).'</th>'."\n";
    //$strResultado .= '<th align="left" class="infraTh">Sigla</th>'."\n";
    $strResultado .= '<th class="infraTh">A��es</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjColegiadoComposicaoDTO[$i]->getNumIdUnidade(),UnidadeINT::formatarSiglaDescricao($arrObjColegiadoComposicaoDTO[$i]->getStrSiglaUnidade(),$arrObjColegiadoComposicaoDTO[$i]->getStrDescricaoUnidade())).'</td>';
      }
      $strResultado .= '<td width="15%">'.$arrObjColegiadoComposicaoDTO[$i]->getStrSiglaUnidade().'</td>';
      $strResultado .= '<td>'.$arrObjColegiadoComposicaoDTO[$i]->getStrDescricaoUnidade().'</td>';
      $strResultado .= '<td>'.$arrObjColegiadoComposicaoDTO[$i]->getStrNomeTipoMembroColegiado().'</td>';
      $strResultado .= '<td align="center">';
      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjColegiadoComposicaoDTO[$i]->getNumIdUnidade());
      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

#lblSiglaUnidade {position:absolute;left:0%;top:0%;width:20%;}
#txtSiglaUnidade {position:absolute;left:0%;top:40%;width:20%;}

#lblDescricaoUnidade {position:absolute;left:25%;top:0%;width:73%;}
#txtDescricaoUnidade {position:absolute;left:25%;top:40%;width:73%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>

function inicializar(){
  infraReceberSelecao();
  document.getElementById('btnFecharSelecao').focus();
  
  infraEfeitoTabelas();
}
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmUnidadeLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
  <?
  //PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblSiglaUnidade" for="txtSiglaUnidade" class="infraLabelOpcional">Sigla:</label>
  <input type="text" id="txtSiglaUnidade" name="txtSiglaUnidade" class="infraText" value="<?=$strSiglaPesquisa?>" maxlength="15" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  
  <label id="lblDescricaoUnidade" for="txtDescricaoUnidade" class="infraLabelOpcional">Descri��o:</label>
  <input type="text" id="txtDescricaoUnidade" name="txtDescricaoUnidade" class="infraText" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" value="<?=$strDescricaoPesquisa?>" />
  
  <?
  PaginaSEI::getInstance()->fecharAreaDados();  
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>