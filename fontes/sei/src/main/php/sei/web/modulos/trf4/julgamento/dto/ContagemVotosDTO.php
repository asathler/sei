<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ContagemVotosDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return null;
  }

  public function montar() {

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdUsuarioRelator');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjColegiadoComposicaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjVotoParteDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'IdUnidadesBloqueadas');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdSessaoAtual');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdUsuarioDesempate');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Presentes');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Votos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Ressalvas');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Acompanham');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Divergem');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'TitularesColegiado');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'QuorumMinimo');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'FaltamVotos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'FaltaQuorum');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'PedidoVista');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'PedidoVistaAnterior');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'Empate');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'Divergencia');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'UsuariosEmpate');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjResumoVotacaoDTO');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdUsuarioAcordao');



  }
}
?>