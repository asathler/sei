<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 07/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MdJulgarConfiguracaoRN extends ConfiguracaoRN {

  public static $EP_ASSINATURA=0;
  public static $EP_PUBLICACAO=1;

  public static $PAR_EVENTO_PAUTA_FECHADA='MD_JULGAR_EVENTO_PAUTA';
  public static $PAR_PRAZO_ABERTURA_SESSAO_VIRUTAL='MD_JULGAR_DIAS_INICIO_SESSAO_VIRTUAL';
  public static $PAR_ACESSO_OBSERVADOR_EXTERNO='MD_JULGAR_ACESSO_OBSERVADOR_EXTERNO';

  public static $PAR_SERIE_CERTIDAO_DISTRIBUICAO='MD_JULGAR_ID_SERIE_CERTIDAO_DISTRIBUICAO';
  public static $PAR_SERIE_CERTIDAO_REDISTRIBUICAO='MD_JULGAR_ID_SERIE_CERTIDAO_REDISTRIBUICAO';
  public static $PAR_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO='MD_JULGAR_ID_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO';
  public static $PAR_SERIE_PAUTA_SESSAO='MD_JULGAR_ID_SERIE_PAUTA_SESSAO';
  public static $PAR_SERIE_CERTIDAO_CANCELAMENTO_SESSAO='MD_JULGAR_ID_SERIE_CERTIDAO_CANCELAMENTO_SESSAO';
  public static $PAR_SERIE_EDITAL_CANCELAMENTO_SESSAO='MD_JULGAR_ID_SERIE_EDITAL_CANCELAMENTO_SESSAO';
  public static $PAR_SERIE_CERTIDAO_JULGAMENTO='MD_JULGAR_ID_SERIE_CERTIDAO_JULGAMENTO';
  public static $PAR_SERIE_ATA_SESSAO='MD_JULGAR_ID_SERIE_ATA_SESSAO';
  public static $PAR_SERIE_ACORDAO='MD_JULGAR_ID_SERIE_ACORDAO';
  public static $PAR_SERIE_DOCUMENTOS_DISPONIBILIZAVEIS='MD_JULGAR_ID_SERIE_DISPONIBILIZAVEIS';

  public function __construct(){
    parent::__construct();
  }
  public static function montarArrayRegraAcessoPauta(): array
  {
    return array(0 => 'Assinatura', 1 => 'Publica��o');
  }
  public function getArrParametrosConfiguraveis(): array
  {
    $arr = array();
    $arr[self::$PAR_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Cancelamento de Distribui��o');
    $arr[self::$PAR_SERIE_CERTIDAO_DISTRIBUICAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Distribui��o');
    $arr[self::$PAR_SERIE_CERTIDAO_JULGAMENTO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Julgamento');
    $arr[self::$PAR_SERIE_CERTIDAO_REDISTRIBUICAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Redistribui��o');
    $arr[self::$PAR_SERIE_ATA_SESSAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Ata de Sess�o de Julgamento');
    $arr[self::$PAR_SERIE_PAUTA_SESSAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Pauta de Sess�o de Julgamento');
    $arr[self::$PAR_SERIE_CERTIDAO_CANCELAMENTO_SESSAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Cancelamento de Sess�o');
    $arr[self::$PAR_SERIE_EDITAL_CANCELAMENTO_SESSAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Edital de Cancelamento de Sess�o');
    $arr[self::$PAR_SERIE_ACORDAO] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Ac�rd�o');
    $arr[self::$PAR_SERIE_DOCUMENTOS_DISPONIBILIZAVEIS] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_MULTIPLO=>true,ConfiguracaoRN::$POS_ROTULO=>'Documentos que podem ser disponibilizados na sess�o');
    $arr[self::$PAR_ACESSO_OBSERVADOR_EXTERNO]= array(ConfiguracaoRN::$TP_COMBO,'sel',ConfiguracaoRN::$POS_REGRA=>'Boolean',
        ConfiguracaoRN::$POS_ROTULO=>'Observador pode acessar documentos com pauta fechada');
    $arr[self::$PAR_EVENTO_PAUTA_FECHADA]= array(ConfiguracaoRN::$TP_COMBO,'sel',ConfiguracaoRN::$POS_REGRA=>'RegraAcessoPauta',
        ConfiguracaoRN::$POS_ROTULO=>'Crit�rio de libera��o de acesso externo � pauta');
//    $arr[self::$PAR_PRAZO_ABERTURA_SESSAO_VIRUTAL] = array(ConfiguracaoRN::$TP_NUMERICO,'txt',
//        ConfiguracaoRN::$POS_ROTULO=>'N�mero de dias �teis para abrir sess�o virtual ap�s pauta fechada');
    //
    return $arr;
  }
  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  protected function gravarControlado($arrObjInfraParametroDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_configurar',__METHOD__,$arrObjInfraParametroDTO);

      $arrParametrosConfiguracao=$this->getArrParametrosConfiguraveis();
      //Regras de Negocio
      $objInfraException = new InfraException();
      if(InfraArray::contar($arrObjInfraParametroDTO)!=InfraArray::contar($arrParametrosConfiguracao)){
        $objInfraException->lancarValidacao('N�o foram informados todos os par�metros da Sess�o de Julgamento.');
      }


      $objSerieDTO=new SerieDTO();
      $objSerieRN=new SerieRN();
      $objSerieDTO->setStrSinAtivo('S');
      $objSerieDTO->retStrStaAplicabilidade();
      $objSerieDTO->retStrNome();
      $objSerieDTO->retNumIdSerie();
      $arrObjSerieDTO=$objSerieRN->listarRN0646($objSerieDTO);
      $arrObjSerieDTO=InfraArray::indexarArrInfraDTO($arrObjSerieDTO,'IdSerie');


      foreach ($arrObjInfraParametroDTO as $objInfraParametroDTO) {
        $strNome=$objInfraParametroDTO->getStrNome();
        if (InfraString::isBolVazia($objInfraParametroDTO->getStrNome())){
          $objInfraException->lancarValidacao('Nome do par�metro n�o informado.');
          continue;
        }
        $strRotulo=$strNome;
        if (isset($arrParametrosConfiguracao[$strNome][ConfiguracaoRN::$POS_ROTULO])){
          $strRotulo=$arrParametrosConfiguracao[$strNome][ConfiguracaoRN::$POS_ROTULO];
        }
        if (InfraString::isBolVazia($objInfraParametroDTO->getStrValor())){
          $objInfraException->adicionarValidacao('Valor do par�metro ['.$strRotulo.'] n�o informado.');
          continue;
        }
        switch($objInfraParametroDTO->getStrNome()) {
          case self::$PAR_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO:
          case self::$PAR_SERIE_CERTIDAO_DISTRIBUICAO:
          case self::$PAR_SERIE_CERTIDAO_JULGAMENTO:
          case self::$PAR_SERIE_CERTIDAO_REDISTRIBUICAO:
          case self::$PAR_SERIE_ATA_SESSAO:
          case self::$PAR_SERIE_PAUTA_SESSAO:
          case self::$PAR_SERIE_CERTIDAO_CANCELAMENTO_SESSAO:
          case self::$PAR_SERIE_EDITAL_CANCELAMENTO_SESSAO:
          case self::$PAR_SERIE_ACORDAO:
            $valor = $objInfraParametroDTO->getStrValor();
            if (!isset($arrObjSerieDTO[$valor]) || !in_array($arrObjSerieDTO[$valor]->getStrStaAplicabilidade(), array(SerieRN::$TA_INTERNO, SerieRN::$TA_INTERNO_EXTERNO))) {
              $objInfraException->adicionarValidacao('Valor do par�metro [' . $strRotulo . '] n�o permitido.');
            }
            break;
          case self::$PAR_SERIE_DOCUMENTOS_DISPONIBILIZAVEIS:
            $valor = $objInfraParametroDTO->getStrValor();
            $arr = explode(',', $valor);
            foreach ($arr as $key => $valor2) {
              if (!isset($arrObjSerieDTO[$valor2])) {
                $objInfraException->adicionarValidacao('Valor do par�metro [' . $strRotulo . '] n�o permitido.');
              }
            }
            break;
          case self::$PAR_ACESSO_OBSERVADOR_EXTERNO:
          case self::$PAR_EVENTO_PAUTA_FECHADA:
            if(!in_array($objInfraParametroDTO->getStrValor(),array(0,1))){
              $objInfraException->adicionarValidacao('Valor do par�metro [' . $strRotulo . '] n�o permitido.');
            }
            break;
          case self::$PAR_PRAZO_ABERTURA_SESSAO_VIRUTAL:
            if($objInfraParametroDTO->getStrValor()<0){
              $objInfraException->adicionarValidacao('Valor do par�metro [' . $strRotulo . '] n�o permitido.');
            }
            break;
          default:
            $objInfraException->lancarValidacao('Configura��o do par�metro [' . $strRotulo . '] n�o permitida.');
        }
      }
      
      $objInfraException->lancarValidacoes();

      $objInfraParametro=new InfraParametro(BancoSEI::getInstance());
      foreach ($arrObjInfraParametroDTO as $objInfraParametroDTO) {
        $objInfraParametro->setValor($objInfraParametroDTO->getStrNome(),$objInfraParametroDTO->getStrValor());
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro configurando par�metros.',$e);
    }
  }


}
?>