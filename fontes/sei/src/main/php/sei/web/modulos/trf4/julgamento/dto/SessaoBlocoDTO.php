<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 03/07/2021 - criado por alv77
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class SessaoBlocoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'sessao_bloco';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdSessaoBloco', 'id_sessao_bloco');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Descricao', 'descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Ordem', 'ordem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'SinAgruparMembro', 'sin_agrupar_membro');
   // $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'SinPermitePautaFechada', 'sin_permite_pauta_fechada');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'StaTipoItem', 'sta_tipo_item');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdSessaoJulgamento', 'id_sessao_julgamento');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaSituacaoSessao','sta_situacao','sessao_julgamento');

    $this->configurarPK('IdSessaoBloco',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdSessaoJulgamento', 'sessao_julgamento', 'id_sessao_julgamento');
  }
}
?>