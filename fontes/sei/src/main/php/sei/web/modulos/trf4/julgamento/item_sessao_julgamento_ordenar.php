<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 20/01/2015 - criado por bcu
 */

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(false);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  $strParametros = '';
  if(isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
    $strParametros .= '&arvore='.$_GET['arvore'];
  }

  $strParametros .= "&id_sessao_julgamento=".$_GET['id_sessao_julgamento'];
  $strParametros .= "&id_sessao_bloco=".$_GET['id_sessao_bloco'];
  $strParametros .= "&id_usuario=".$_GET['id_usuario'];


  $arrComandos = array();

  switch($_GET['acao']){
    case 'item_sessao_julgamento_ordenar':

      $strTitulo = 'Ordenar Processos do Relator';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmSalvar" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
//      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" value="Cancelar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';


      $arrItemSessaoJulgamento = PaginaSEI::getInstance()->getArrValuesSelect($_POST['hdnIdItemSessaoJulgamento']);

      $arrObjItemSessaoJulgamentoDTO = array();
      foreach ($arrItemSessaoJulgamento as $i => $strId) {
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($strId);
        $objItemSessaoJulgamentoDTO->setNumOrdem($i+1);
        $arrObjItemSessaoJulgamentoDTO[] = $objItemSessaoJulgamentoDTO;
      }

      if (isset($_POST['sbmSalvar'])) {
        try{

          $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
          $objItemSessaoJulgamentoRN->alterarOrdem($arrObjItemSessaoJulgamentoDTO);
          $bolExecutouOK=true;

          //header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&resultado=1'.$strParametros));
          //die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensSelIdItemSessaoJulgamento = ItemSessaoJulgamentoINT::montarSelectItensOrdenacao($_GET['id_sessao_julgamento'],$_GET['id_sessao_bloco'],$_GET['id_usuario']);

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

  #lblItemSessaoJulgamento {position:absolute;left:0%;top:0%;width:90%;}
  #selItemSessaoJulgamento {position:absolute;left:0%;top:3.5%;width:90%;}

  #imgRelProtocoloProtocoloAcima {position:absolute;left:91%;top:3.5%;}
  #imgRelProtocoloProtocoloAbaixo {position:absolute;left:91%;top:9%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

  var objLupaRelProtocoloProtocolo = null;

  function inicializar(){
<?  if ($bolExecutouOK){ ?>
    fechar_pagina('','about:blank');
<?  } ?>

  objLupaRelProtocoloProtocolo = new infraLupaSelect('selItemSessaoJulgamento','hdnIdItemSessaoJulgamento', null);

  infraEfeitoTabelas();
  }

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
  <form id="frmArvoreOrdenar" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
    <?
    //PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    //PaginaSEI::getInstance()->montarAreaValidacao();
    PaginaSEI::getInstance()->abrirAreaDados('40em');
    ?>

    <label id="lblItemSessaoJulgamento" for="selItemSessaoJulgamento" accesskey="" class="infraLabelObrigatorio">Processos:</label>
    <select id="selItemSessaoJulgamento" name="selItemSessaoJulgamento" size="25" multiple="multiple" class="infraSelect">
      <?=$strItensSelIdItemSessaoJulgamento?>
    </select>
    <img id="imgRelProtocoloProtocoloAcima" onclick="objLupaRelProtocoloProtocolo.moverAcima();" src="<?=PaginaSEI::getInstance()->getIconeMoverAcima()?>" alt="Mover Acima Protocolo Selecionado" title="Mover Acima Protocolo Selecionado" class="infraImg" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <img id="imgRelProtocoloProtocoloAbaixo" onclick="objLupaRelProtocoloProtocolo.moverAbaixo();" src="<?=PaginaSEI::getInstance()->getIconeMoverAbaixo()?>" alt="Mover Abaixo Protocolo Selecionado" title="Mover Abaixo Protocolo Selecionado" class="infraImg" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

    <input type="hidden" id="hdnIdItemSessaoJulgamento" name="hdnIdItemSessaoJulgamento" value="<?=$_POST['hdnIdItemSessaoJulgamento']?>" />

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->montarAreaDebug();
    //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
    ?>
  </form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>