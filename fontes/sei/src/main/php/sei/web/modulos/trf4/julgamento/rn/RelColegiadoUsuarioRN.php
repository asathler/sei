<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 30/05/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class RelColegiadoUsuarioRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdUsuario(RelColegiadoUsuarioDTO $objRelColegiadoUsuarioDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelColegiadoUsuarioDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
    }
  }

  private function validarNumIdColegiado(RelColegiadoUsuarioDTO $objRelColegiadoUsuarioDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelColegiadoUsuarioDTO->getNumIdColegiado())){
      $objInfraException->adicionarValidacao('Colegiado n�o informado.');
    }
  }

  protected function cadastrarControlado(RelColegiadoUsuarioDTO $objRelColegiadoUsuarioDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_colegiado_usuario_cadastrar',__METHOD__,$objRelColegiadoUsuarioDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdUsuario($objRelColegiadoUsuarioDTO, $objInfraException);
      $this->validarNumIdColegiado($objRelColegiadoUsuarioDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objRelColegiadoUsuarioBD = new RelColegiadoUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelColegiadoUsuarioBD->cadastrar($objRelColegiadoUsuarioDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Usu�rio Observador.',$e);
    }
  }

  protected function alterarControlado(RelColegiadoUsuarioDTO $objRelColegiadoUsuarioDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('rel_colegiado_usuario_alterar',__METHOD__,$objRelColegiadoUsuarioDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objRelColegiadoUsuarioDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objRelColegiadoUsuarioDTO, $objInfraException);
      }
      if ($objRelColegiadoUsuarioDTO->isSetNumIdColegiado()){
        $this->validarNumIdColegiado($objRelColegiadoUsuarioDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objRelColegiadoUsuarioBD = new RelColegiadoUsuarioBD($this->getObjInfraIBanco());
      $objRelColegiadoUsuarioBD->alterar($objRelColegiadoUsuarioDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Usu�rio Observador.',$e);
    }
  }

  protected function excluirControlado($arrObjRelColegiadoUsuarioDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_colegiado_usuario_excluir',__METHOD__,$arrObjRelColegiadoUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelColegiadoUsuarioBD = new RelColegiadoUsuarioBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjRelColegiadoUsuarioDTO);$i++){
        $objRelColegiadoUsuarioBD->excluir($arrObjRelColegiadoUsuarioDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Usu�rio Observador.',$e);
    }
  }

  protected function consultarConectado(RelColegiadoUsuarioDTO $objRelColegiadoUsuarioDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_colegiado_usuario_consultar',__METHOD__,$objRelColegiadoUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelColegiadoUsuarioBD = new RelColegiadoUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelColegiadoUsuarioBD->consultar($objRelColegiadoUsuarioDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Usu�rio Observador.',$e);
    }
  }

  protected function listarConectado(RelColegiadoUsuarioDTO $objRelColegiadoUsuarioDTO) {
    try {

      //Valida Permissao
      //SessaoSEI::getInstance()->validarAuditarPermissao('rel_colegiado_usuario_listar',__METHOD__,$objRelColegiadoUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelColegiadoUsuarioBD = new RelColegiadoUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelColegiadoUsuarioBD->listar($objRelColegiadoUsuarioDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Usu�rios Observadores.',$e);
    }
  }

  protected function contarConectado(RelColegiadoUsuarioDTO $objRelColegiadoUsuarioDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_colegiado_usuario_listar',__METHOD__,$objRelColegiadoUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelColegiadoUsuarioBD = new RelColegiadoUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelColegiadoUsuarioBD->contar($objRelColegiadoUsuarioDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Usu�rios Observadores.',$e);
    }
  }
}
?>