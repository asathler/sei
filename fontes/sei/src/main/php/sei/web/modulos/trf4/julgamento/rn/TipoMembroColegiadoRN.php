<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class TipoMembroColegiadoRN extends InfraRN {

  public static $TMC_TITULAR = 1;
  public static $TMC_SUPLENTE = 2;
  public static $TMC_EVENTUAL = 3;


  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  protected function consultarConectado(TipoMembroColegiadoDTO $objTipoMembroColegiadoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_membro_colegiado_consultar',__METHOD__,$objTipoMembroColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMembroColegiadoBD = new TipoMembroColegiadoBD($this->getObjInfraIBanco());
      $ret = $objTipoMembroColegiadoBD->consultar($objTipoMembroColegiadoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Tipo de Membro de Colegiado.',$e);
    }
  }

  protected function listarConectado(TipoMembroColegiadoDTO $objTipoMembroColegiadoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_membro_colegiado_listar',__METHOD__,$objTipoMembroColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMembroColegiadoBD = new TipoMembroColegiadoBD($this->getObjInfraIBanco());
      $ret = $objTipoMembroColegiadoBD->listar($objTipoMembroColegiadoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Tipos de Membro de Colegiado.',$e);
    }
  }

  protected function contarConectado(TipoMembroColegiadoDTO $objTipoMembroColegiadoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_membro_colegiado_listar',__METHOD__,$objTipoMembroColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMembroColegiadoBD = new TipoMembroColegiadoBD($this->getObjInfraIBanco());
      $ret = $objTipoMembroColegiadoBD->contar($objTipoMembroColegiadoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Tipos de Membro de Colegiado.',$e);
    }
  }


}
?>