<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 04/07/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

try {
  require_once dirname(__FILE__).'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);


  switch($_GET['acao']){

    case 'distribuicao_listar':
      $strTitulo = 'Distribui��es do colegiado';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();

  $numIdColegiadoVersao=$_GET['id_colegiado_versao'];


  $objDistribuicaoDTO = new DistribuicaoDTO();
  $objDistribuicaoDTO->setNumIdColegiadoVersao($numIdColegiadoVersao);
  $objDistribuicaoDTO->retNumIdDistribuicao();
  $objDistribuicaoDTO->retStrStaDistribuicao();
  $objDistribuicaoDTO->retStrStaUltimo();
  $objDistribuicaoDTO->retStrProtocoloFormatado();
  $objDistribuicaoDTO->retStrNomeUsuarioRelator();
  $objDistribuicaoDTO->retStrSiglaUnidadeRelator();
  $objDistribuicaoDTO->retDthDistribuicao();


  PaginaSEI::getInstance()->prepararOrdenacao($objDistribuicaoDTO, 'IdDistribuicao', InfraDTO::$TIPO_ORDENACAO_ASC);
  PaginaSEI::getInstance()->prepararPaginacao($objDistribuicaoDTO);

  $objDistribuicaoRN = new DistribuicaoRN();
  $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);

  PaginaSEI::getInstance()->processarPaginacao($objDistribuicaoDTO);
  $numRegistros = InfraArray::contar($arrObjDistribuicaoDTO);

  if ($numRegistros > 0){

    $arrObjEstadoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
    $arrObjEstadoDistribuicao=InfraArray::indexarArrInfraDTO($arrObjEstadoDistribuicaoDTO,'StaDistribuicao');


    $bolCheck = false;

    $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('distribuicao_consultar');
    $bolAcaoImprimir = true;

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Distribui��es.';
    $strCaptionTabela = 'Distribui��es';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Processo','ProtocoloFormatado',$arrObjDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Data/Hora','Distribuicao',$arrObjDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Relator','NomeUsuarioRelator',$arrObjDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Unidade','SiglaUnidadeRelator',$arrObjDistribuicaoDTO).'</th>'."\n";
//    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Situa��o','StaDistribuicao',$arrObjDistribuicaoDTO).'</th>'."\n";
//    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Ativo','StaUltimo',$arrObjDistribuicaoDTO).'</th>'."\n";
//    $strResultado .= '<th class="infraTh">A��es</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      $strCssTr = ($strCssTr=='infraTrClara')?'infraTrEscura':'infraTrClara';
//      if($arrObjDistribuicaoDTO[$i]->getStrStaUltimo()==DistribuicaoRN::$SD_REDISTRIBUIDO) {
//        $strCssTr.=' inativo';
//      }
      $strResultado .= '<tr class="'.$strCssTr.'">';

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjDistribuicaoDTO[$i]->getNumIdDistribuicao(),$arrObjDistribuicaoDTO[$i]->getNumIdDistribuicao()).'</td>';
      }
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjDistribuicaoDTO[$i]->getStrProtocoloFormatado()).'</td>';
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjDistribuicaoDTO[$i]->getDthDistribuicao()).'</td>';
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjDistribuicaoDTO[$i]->getStrNomeUsuarioRelator()).'</td>';
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjDistribuicaoDTO[$i]->getStrSiglaUnidadeRelator()).'</td>';
//      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjEstadoDistribuicao[$arrObjDistribuicaoDTO[$i]->getStrStaDistribuicao()]->getStrDescricao()).'</td>';
//      if($arrObjDistribuicaoDTO[$i]->getStrStaUltimo()==DistribuicaoRN::$SD_REDISTRIBUIDO && $arrObjDistribuicaoDTO[$i]->getStrStaDistribuicao()!=DistribuicaoRN::$STA_CANCELADO){
//        $strStatus='**Redistribu�do**';
//      } else {
//        $strStatus='';
//      }
//      $strResultado .= '<td>'.PaginaSEI::tratarHTML($strStatus).'</td>';
//      $strResultado .= '<td align="center">';

//      if ($bolAcaoConsultar){
//        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=distribuicao_consultar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeConsultar().'" title="Consultar Distribui��o" alt="Consultar Distribui��o" class="infraImg" /></a>&nbsp;';
//      }
//
//      $strResultado .= '</td>';

      $strResultado.='</tr>'."\n";
    }
    $strResultado .= '</table>';
  }
}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
  .inativo td {color:dimgrey}
<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  document.getElementById('btnFechar').focus();
  infraEfeitoTabelas();
}


<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmDistribuicaoLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
//  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);

  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>