<?
/*
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/09/2008 - criado por marcio_db
*
*
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(false);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEIExterna::getInstance()->validarLink();


  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);
  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  SessaoSEIExterna::getInstance()->validarPermissao($_GET['acao']);

  $idItemSessaoJulgamento=$_GET['id_item_sessao_julgamento'];
  $idDocumento=$_GET['id_documento'];


  switch($_GET['acao']){

    case 'documento_sessao_consulta_externa':

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($idItemSessaoJulgamento);
      $objItemSessaoJulgamentoDTO->retStrStaTipoItemSessaoBloco();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
      $staItem=$objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco();

      //validar permiss�o...
      $objItemSessaoDocumentoDTO=new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoRN=new ItemSessaoDocumentoRN();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($idItemSessaoJulgamento);
      $objItemSessaoDocumentoDTO->setDblIdDocumento($idDocumento);
      $objItemSessaoDocumentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoDocumentoDTO->retStrStaSituacaoSessaoJulgamento();

      $objItemSessaoDocumentoDTO=$objItemSessaoDocumentoRN->consultar($objItemSessaoDocumentoDTO);

      if($objItemSessaoDocumentoDTO==null){
        //n�o encontrado
        throw new InfraException('Documento n�o disponibilizado neste item.');
      }

      $objRelColegiadoUsuarioDTO=new RelColegiadoUsuarioDTO();
      $objRelColegiadoUsuarioRN=new RelColegiadoUsuarioRN();
      $objRelColegiadoUsuarioDTO->setNumIdUsuario(SessaoSEIExterna::getInstance()->getNumIdUsuarioExterno());
      $objRelColegiadoUsuarioDTO->setNumIdColegiado($objItemSessaoDocumentoDTO->getNumIdColegiadoSessaoJulgamento());
      if($objRelColegiadoUsuarioRN->contar($objRelColegiadoUsuarioDTO)==0){
        //usu�rio n�o � observador do colegiado
        throw new InfraException('Sem permiss�o de acesso a este colegiado.');
      }


      $objDocumentoDTO = new DocumentoDTO();
      $objDocumentoDTO->retDblIdDocumento();
      $objDocumentoDTO->retStrNomeSerie();
      $objDocumentoDTO->retStrNumero();
      $objDocumentoDTO->retStrSiglaUnidadeGeradoraProtocolo();
      $objDocumentoDTO->retStrProtocoloDocumentoFormatado();
      $objDocumentoDTO->retStrStaProtocoloProtocolo();
      $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_EDITOR_INTERNO);
      $objDocumentoDTO->retDblIdDocumentoEdoc();
      $objDocumentoDTO->setDblIdDocumento($_GET['id_documento']);

      $objDocumentoRN = new DocumentoRN();
      $objDocumentoDTO = $objDocumentoRN->consultarRN0005($objDocumentoDTO);

      if ($objDocumentoDTO==null){
        die('Documento n�o encontrado.');
      }

      $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
      $bolPermiteVisualizacaoPautaFechada=($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_ACESSO_OBSERVADOR_EXTERNO,false)==1);

      $arrSituacaoPermitidaDocumento=array(SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA);
      if($bolPermiteVisualizacaoPautaFechada){
        $arrSituacaoPermitidaDocumento[]=SessaoJulgamentoRN::$ES_PAUTA_FECHADA;
      }
      if($staItem!=TipoSessaoBlocoRN::$STA_REFERENDO && !in_array($objItemSessaoDocumentoDTO->getStrStaSituacaoSessaoJulgamento(),$arrSituacaoPermitidaDocumento)){
        throw new InfraException('Situa��o da Sess�o de Julgamento n�o permite acesso ao documento.');
      }

        $strProtocoloDocumentoFormatado = $objDocumentoDTO->getStrProtocoloDocumentoFormatado();
        $strNomeSerie = $objDocumentoDTO->getStrNomeSerie();


        $objEditorDTO = new EditorDTO();
        $objEditorDTO->setDblIdDocumento($objDocumentoDTO->getDblIdDocumento());
        $objEditorDTO->setNumIdBaseConhecimento(null);
        $objEditorDTO->setStrSinCabecalho('S');
        $objEditorDTO->setStrSinRodape('S');
        $objEditorDTO->setStrSinCarimboPublicacao('S');
        $objEditorDTO->setStrSinIdentificacaoVersao('S');
        $objEditorDTO->setStrSinProcessarLinks('S');

        $objEditorRN = new EditorRN();
        $strResultado = $objEditorRN->consultarHtmlVersao($objEditorDTO);

        $objAuditoriaProtocoloDTO = new AuditoriaProtocoloDTO();
        $objAuditoriaProtocoloDTO->setStrRecurso($_GET['acao']);
        $objAuditoriaProtocoloDTO->setNumIdUsuario(SessaoSEIExterna::getInstance()->getNumIdUsuarioExterno());
        $objAuditoriaProtocoloDTO->setDblIdProtocolo($_GET['id_documento']);
        $objAuditoriaProtocoloDTO->setNumIdAnexo(null);
        $objAuditoriaProtocoloDTO->setDtaAuditoria(InfraData::getStrDataAtual());
        $objAuditoriaProtocoloDTO->setNumVersao($objEditorDTO->getNumVersao());

        $objAuditoriaProtocoloRN = new AuditoriaProtocoloRN();
        $objAuditoriaProtocoloRN->auditarVisualizacao($objAuditoriaProtocoloDTO);

        InfraPagina::montarHeaderDownload(null, null, 'Content-Type: text/html; charset=iso-8859-1');

        die($strResultado);


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

}catch(Exception $e){
  try{ LogSEI::getInstance()->gravar(InfraException::inspecionar($e)); }catch(Exception $e2){}
  die('Erro na visualiza��o do documento.');
}
?>