<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class TipoMembroColegiadoINT extends InfraINT {

  public static function montarSelectNome($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objTipoMembroColegiadoDTO = new TipoMembroColegiadoDTO();
    $objTipoMembroColegiadoDTO->retNumIdTipoMembroColegiado();
    $objTipoMembroColegiadoDTO->retStrNome();

    if ($strValorItemSelecionado!=null){
      $objTipoMembroColegiadoDTO->setBolExclusaoLogica(false);
      $objTipoMembroColegiadoDTO->adicionarCriterio(array('SinAtivo','IdTipoMembroColegiado'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objTipoMembroColegiadoDTO->setOrdStrNome(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objTipoMembroColegiadoRN = new TipoMembroColegiadoRN();
    $arrObjTipoMembroColegiadoDTO = $objTipoMembroColegiadoRN->listar($objTipoMembroColegiadoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjTipoMembroColegiadoDTO, 'IdTipoMembroColegiado', 'Nome');
  }
}
?>