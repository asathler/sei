<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoVersaoINT extends InfraINT {

  public static function montarSelectIdColegiadoVersao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdColegiado=''){
    $objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
    $objColegiadoVersaoDTO->retNumIdColegiadoVersao();
    $objColegiadoVersaoDTO->retNumIdColegiadoVersao();

    if ($numIdColegiado!==''){
      $objColegiadoVersaoDTO->setNumIdColegiado($numIdColegiado);
    }

    $objColegiadoVersaoDTO->setOrdNumIdColegiadoVersao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objColegiadoVersaoRN = new ColegiadoVersaoRN();
    $arrObjColegiadoVersaoDTO = $objColegiadoVersaoRN->listar($objColegiadoVersaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjColegiadoVersaoDTO, 'IdColegiadoVersao', 'IdColegiadoVersao');
  }
  public static function montarSelectVersao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdColegiado='', $numIdUnidade='', $numIdUsuario=''){
  	$objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
  	$objColegiadoVersaoDTO->retNumIdColegiadoVersao();
  	$objColegiadoVersaoDTO->retDthVersao();

  	if ($numIdColegiado!==''){
  		$objColegiadoVersaoDTO->setNumIdColegiado($numIdColegiado);
  	}

  	if ($numIdUnidade!==''){
  		$objColegiadoVersaoDTO->setNumIdUnidade($numIdUnidade);
  	}

  	if ($numIdUsuario!==''){
  		$objColegiadoVersaoDTO->setNumIdUsuario($numIdUsuario);
  	}

  	$objColegiadoVersaoDTO->setOrdDthVersao(InfraDTO::$TIPO_ORDENACAO_ASC);

  	$objColegiadoVersaoRN = new ColegiadoVersaoRN();
  	$arrObjColegiadoVersaoDTO = $objColegiadoVersaoRN->listar($objColegiadoVersaoDTO);

  	return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjColegiadoVersaoDTO, 'IdColegiadoVersao', 'Versao');
  }
}
?>