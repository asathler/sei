<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class AndamentoSessaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'andamento_sessao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdAndamentoSessao','id_andamento_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Execucao','dth_execucao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdTarefaSessao','id_tarefa_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamento','id_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidade','id_unidade');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeTarefaSessao','nome','tarefa_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SinHistoricoResumidoTarefaSessao','sin_historico_resumido','tarefa_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SinHistoricoResumido','sin_historico_resumido','tarefa_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUsuario','sigla','usuario');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjAtributoAndamentoSessaoDTO');

    $this->configurarPK('IdAndamentoSessao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdTarefaSessao', 'tarefa_sessao', 'id_tarefa_sessao');
    $this->configurarFK('IdUsuario', 'usuario', 'id_usuario');
    $this->configurarFK('IdUnidade', 'unidade', 'id_unidade');
  }
}
?>