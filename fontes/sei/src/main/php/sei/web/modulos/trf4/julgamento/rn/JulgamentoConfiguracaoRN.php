<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 07/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class JulgamentoConfiguracaoRN extends ConfiguracaoRN {

  public function __construct(){
    parent::__construct();
  }
  public static function montarArrayRegraAcessoPauta()
  {
    return array(0 => 'Assinatura', 1 => 'Publica��o');
  }
  public function getArrParametrosConfiguraveis()
  {
    $arr = array();
    $arr['ID_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Cancelamento de Distribui��o');
    $arr['ID_SERIE_CERTIDAO_DISTRIBUICAO'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Distribui��o');
    $arr['ID_SERIE_CERTIDAO_JULGAMENTO'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Julgamento');
    $arr['ID_SERIE_CERTIDAO_REDISTRIBUICAO'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Redistribui��o');
    $arr['ID_SERIE_JULGAMENTO_ATA'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Ata de Sess�o de Julgamento');
    $arr['ID_SERIE_JULGAMENTO_PAUTA'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Pauta de Sess�o de Julgamento');
    $arr['ID_SERIE_CERTIDAO_CANCELAMENTO_SESSAO'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Certid�o de Cancelamento de Sess�o');
    $arr['ID_SERIE_EDITAL_CANCELAMENTO_SESSAO'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_ROTULO=>'Edital de Cancelamento de Sess�o');
    $arr['ID_SERIE_JULGAMENTO_DISPONIBILIZAVEIS'] = array(ConfiguracaoRN::$TP_ID,'sel','serie',
        ConfiguracaoRN::$POS_MULTIPLO=>true,ConfiguracaoRN::$POS_ROTULO=>'Documentos que podem ser disponibilizados na sess�o');
    $arr['MD_JULGAR_ACESSO_OBSERVADOR_EXTERNO']= array(ConfiguracaoRN::$TP_COMBO,'sel',ConfiguracaoRN::$POS_REGRA=>'Boolean',
        ConfiguracaoRN::$POS_ROTULO=>'Obsevador pode acessar documentos com pauta fechada sess�o');
    $arr['MD_JULGAR_CONSULTA_EXTERNA_PAUTA']= array(ConfiguracaoRN::$TP_COMBO,'sel',ConfiguracaoRN::$POS_REGRA=>'RegraAcessoPauta',
        ConfiguracaoRN::$POS_ROTULO=>'Crit�rio de libera��o de acesso externo � pauta');
    //
    return $arr;
  }
  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  protected function gravarControlado($arrObjInfraParametroDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_configurar',__METHOD__,$arrObjInfraParametroDTO);

      $arrParametrosConfiguracao=$this->getArrParametrosConfiguraveis();
      //Regras de Negocio
      $objInfraException = new InfraException();
      if(InfraArray::contar($arrObjInfraParametroDTO)!=InfraArray::contar($arrParametrosConfiguracao)){
        $objInfraException->lancarValidacao('N�o foram informados todos os par�metros da Sess�o de Julgamento.');
      }


      $objSerieDTO=new SerieDTO();
      $objSerieRN=new SerieRN();
      $objSerieDTO->setStrSinAtivo('S');
      $objSerieDTO->retStrStaAplicabilidade();
      $objSerieDTO->retStrNome();
      $objSerieDTO->retNumIdSerie();
      $arrObjSerieDTO=$objSerieRN->listarRN0646($objSerieDTO);
      $arrObjSerieDTO=InfraArray::indexarArrInfraDTO($arrObjSerieDTO,'IdSerie');


      foreach ($arrObjInfraParametroDTO as $objInfraParametroDTO) {
        $strNome=$objInfraParametroDTO->getStrNome();
        if (InfraString::isBolVazia($objInfraParametroDTO->getStrNome())){
          $objInfraException->lancarValidacao('Nome do par�metro n�o informado.');
          continue;
        }
        $strRotulo=$strNome;
        if (isset($arrParametrosConfiguracao[$strNome][ConfiguracaoRN::$POS_ROTULO])){
          $strRotulo=$arrParametrosConfiguracao[$strNome][ConfiguracaoRN::$POS_ROTULO];
        }
        if (InfraString::isBolVazia($objInfraParametroDTO->getStrValor())){
          $objInfraException->adicionarValidacao('Valor do par�metro ['.$strRotulo.'] n�o informado.');
          continue;
        }
        switch($objInfraParametroDTO->getStrNome()) {
          case 'ID_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO':
          case 'ID_SERIE_CERTIDAO_DISTRIBUICAO':
          case 'ID_SERIE_CERTIDAO_JULGAMENTO':
          case 'ID_SERIE_CERTIDAO_REDISTRIBUICAO':
          case 'ID_SERIE_JULGAMENTO_ATA':
          case 'ID_SERIE_JULGAMENTO_PAUTA':
          case 'ID_SERIE_CERTIDAO_CANCELAMENTO_SESSAO':
          case 'ID_SERIE_EDITAL_CANCELAMENTO_SESSAO':
          case 'ID_SERIE_ACORDAO':
            $valor = $objInfraParametroDTO->getStrValor();
            if (!isset($arrObjSerieDTO[$valor]) || !in_array($arrObjSerieDTO[$valor]->getStrStaAplicabilidade(), array(SerieRN::$TA_INTERNO, SerieRN::$TA_INTERNO_EXTERNO))) {
              $objInfraException->adicionarValidacao('Valor do par�metro [' . $strRotulo . '] n�o permitido.');
            }
            break;
          case 'ID_SERIE_JULGAMENTO_DISPONIBILIZAVEIS':
            $valor = $objInfraParametroDTO->getStrValor();
            $arr = explode(',', $valor);
            foreach ($arr as $key => $valor2) {
              if (!isset($arrObjSerieDTO[$valor2])) {
                $objInfraException->adicionarValidacao('Valor do par�metro [' . $strRotulo . '] n�o permitido.');
              }
            }
            break;
          case 'MD_JULGAR_ACESSO_OBSERVADOR_EXTERNO':
          case 'MD_JULGAR_CONSULTA_EXTERNA_PAUTA':
            if(!in_array($objInfraParametroDTO->getStrValor(),array(0,1))){
              $objInfraException->adicionarValidacao('Valor do par�metro [' . $strRotulo . '] n�o permitido.');
            }
            break;

          default:
            $objInfraException->lancarValidacao('Configura��o do par�metro [' . $strRotulo . '] n�o permitida.');
        }
      }
      
      $objInfraException->lancarValidacoes();

      $objInfraParametro=new InfraParametro(BancoSEI::getInstance());
      foreach ($arrObjInfraParametroDTO as $objInfraParametroDTO) {
        $objInfraParametro->setValor($objInfraParametroDTO->getStrNome(),$objInfraParametroDTO->getStrValor());
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro configurando par�metros.',$e);
    }
  }


}
?>