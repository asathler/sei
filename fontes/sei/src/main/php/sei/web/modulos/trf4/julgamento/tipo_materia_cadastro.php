<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*
* Vers�o no SVN: $Id$
*/

try {
  require_once __DIR__ .'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('tipo_materia_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objTipoMateriaDTO = new TipoMateriaDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'tipo_materia_cadastrar':
      $strTitulo = 'Novo Tipo de Mat�ria';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarTipoMateria" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objTipoMateriaDTO->setNumIdTipoMateria(null);
      $objTipoMateriaDTO->setStrDescricao($_POST['txtDescricao']);
      $objTipoMateriaDTO->setStrSinAtivo('S');

      if (isset($_POST['sbmCadastrarTipoMateria'])) {
        try{
          $objTipoMateriaRN = new TipoMateriaRN();
          $objTipoMateriaDTO = $objTipoMateriaRN->cadastrar($objTipoMateriaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Tipo de Mat�ria "'.$objTipoMateriaDTO->getStrDescricao().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_tipo_materia='.$objTipoMateriaDTO->getNumIdTipoMateria().PaginaSEI::getInstance()->montarAncora($objTipoMateriaDTO->getNumIdTipoMateria())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'tipo_materia_alterar':
      $strTitulo = 'Alterar Tipo de Mat�ria';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarTipoMateria" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_tipo_materia'])){
        $objTipoMateriaDTO->setNumIdTipoMateria($_GET['id_tipo_materia']);
        $objTipoMateriaDTO->retTodos();
        $objTipoMateriaRN = new TipoMateriaRN();
        $objTipoMateriaDTO = $objTipoMateriaRN->consultar($objTipoMateriaDTO);
        if ($objTipoMateriaDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objTipoMateriaDTO->setNumIdTipoMateria($_POST['hdnIdTipoMateria']);
        $objTipoMateriaDTO->setStrDescricao($_POST['txtDescricao']);
        $objTipoMateriaDTO->setStrSinAtivo('S');
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objTipoMateriaDTO->getNumIdTipoMateria())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarTipoMateria'])) {
        try{
          $objTipoMateriaRN = new TipoMateriaRN();
          $objTipoMateriaRN->alterar($objTipoMateriaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Tipo de Mat�ria "'.$objTipoMateriaDTO->getStrDescricao().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objTipoMateriaDTO->getNumIdTipoMateria())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'tipo_materia_consultar':
      $strTitulo = 'Consultar Tipo de Mat�ria';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_tipo_materia'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objTipoMateriaDTO->setNumIdTipoMateria($_GET['id_tipo_materia']);
      $objTipoMateriaDTO->setBolExclusaoLogica(false);
      $objTipoMateriaDTO->retTodos();
      $objTipoMateriaRN = new TipoMateriaRN();
      $objTipoMateriaDTO = $objTipoMateriaRN->consultar($objTipoMateriaDTO);
      if ($objTipoMateriaDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
#lblDescricao {position:absolute;left:0%;top:0%;width:95%;}
#txtDescricao {position:absolute;left:0%;top:40%;width:95%;}

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='tipo_materia_cadastrar'){
    document.getElementById('txtDescricao').focus();
  } else if ('<?=$_GET['acao']?>'=='tipo_materia_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
}

function validarCadastro() {
  if (infraTrim(document.getElementById('txtDescricao').value)=='') {
    alert('Informe a Descri��o.');
    document.getElementById('txtDescricao').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmTipoMateriaCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblDescricao" for="txtDescricao" accesskey="d" class="infraLabelObrigatorio"><span class="infraTeclaAtalho">D</span>escri��o:</label>
  <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="<?=PaginaSEI::tratarHTML($objTipoMateriaDTO->getStrDescricao());?>" onkeypress="return infraMascaraTexto(this,event,250);" maxlength="250" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
<?
PaginaSEI::getInstance()->fecharAreaDados();
?>
  <input type="hidden" id="hdnIdTipoMateria" name="hdnIdTipoMateria" value="<?=$objTipoMateriaDTO->getNumIdTipoMateria();?>" />
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>