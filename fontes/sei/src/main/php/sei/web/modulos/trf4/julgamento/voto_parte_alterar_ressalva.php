<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);



  $objVotoParteDTO = new VotoParteDTO();

  $bolRelator=false;
  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '';
  $strParametros.='&id_voto_parte='.$_GET['id_voto_parte'];
  $arrComandos = array();

  switch($_GET['acao']){
    case 'voto_parte_alterar_ressalva':
      $strTitulo = 'Edi��o de ressalva do Voto';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarVotoParte" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objVotoParteRN=new VotoParteRN();
      $bolRelator=false;

      $objVotoParteDTO->setNumIdVotoParte($_GET['id_voto_parte']);
      $objVotoParteDTO->retTodos();
      $objVotoParteDTO->retStrNomeUsuario();
      $objVotoParteDTO->retStrDescricaoJulgamentoParte();
      $objVotoParteDTO->retStrNomeUsuarioAssociado();
      $objVotoParteDTO=$objVotoParteRN->consultar($objVotoParteDTO);


      $objJulgamentoParteDTO=new JulgamentoParteDTO();
      $objJulgamentoParteRN=new JulgamentoParteRN();
      $objJulgamentoParteDTO->setNumIdJulgamentoParte($objVotoParteDTO->getNumIdJulgamentoParte());
      $objJulgamentoParteDTO->retNumIdUsuarioRelatorDistribuicao();
      $objJulgamentoParteDTO=$objJulgamentoParteRN->consultar($objJulgamentoParteDTO);
      if($objJulgamentoParteDTO->getNumIdUsuarioRelatorDistribuicao()==$objVotoParteDTO->getNumIdUsuario()){
        $bolRelator=true;
      }

      if(isset($_POST['txaRessalva'])){
        $objVotoParteDTO->setStrRessalva($_POST['txaRessalva']);
      }

      $numIdVotoParteAssociado =$objVotoParteDTO->getNumIdVotoParteAssociado();
      $strItensSelVotoParteAssociado='';
      if($numIdVotoParteAssociado==null){
        $strItensSelVotoParteAssociado ='<option value="null" selected="selected">Relator</option>'."\n";
      } else{
        $strItensSelVotoParteAssociado ='<option value="'.$numIdVotoParteAssociado.'" selected="selected">'. $objVotoParteDTO->retStrNomeUsuarioAssociado()."</option>\n";
      }

      if (isset($_POST['sbmCadastrarVotoParte'])) {
        try{
          $objVotoParteRN->alterarRessalva($objVotoParteDTO);
          $bolExecucaoOK=true;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensSelStaVotoParte = VotoParteINT::montarSelectStaVotoParte('null','&nbsp;',$objVotoParteDTO->getStrStaVotoParte());
  $strItensProvimento=ProvimentoINT::montarSelectConteudo('null','&nbsp;',$objVotoParteDTO->getNumIdProvimento());


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

#lblUsuario {position:absolute;left:0%;top:0%;width:60%;}
#txtUsuario {position:absolute;left:0%;top:20%;width:60%;}

#lblParte {position:absolute;left:0%;top:50%;width:60%;}
#txtParte {position:absolute;left:0%;top:70%;width:60%;}

#lblStaVotoParte {position:absolute;left:0%;top:0%;width:25%;}
#selStaVotoParte {position:absolute;left:0%;top:40%;width:25%;}

#lblVotoParte {position:absolute;left:30%;top:0%;width:25%;}
#selVotoParte {position:absolute;left:30%;top:40%;width:25%;}

  #lblProvimento {position:absolute;left:0%;top:0%;width:95%;}
  #selProvimento {position:absolute;left:0%;top:15%;width:96%;}
  #lblComplemento {position:absolute;left:0%;top:34%;width:95%;}
  #txaComplemento {position:absolute;left:0%;top:48%;width:95%;}

#lblRessalva {position:absolute;left:0%;top:0%;width:95%;}
#txaRessalva {position:absolute;left:0%;top:17%;width:95%;}


<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

var assoc=<?=$strItensSelVotoParteAssociado!=''?1:0;?>;
function inicializar(){
<?if ($bolExecucaoOK) {?>
  window.parent.document.getElementById('frmItemSessaoJulgamentoJulgar').submit();
<? }
   if ($bolRelator){?>
   document.getElementById('divInfraAreaDados1').style.display="none";
   document.getElementById('selStaVotoParte').value='<?=VotoParteRN::$STA_ACOMPANHA?>';
<? } ?>
  infraDesabilitarCamposAreaDados();
  var txa=document.getElementById('txaRessalva');
  txa.disabled=false;
  txa.readOnly=false;
  txa.focus();
  verificaVoto(document.getElementById('selStaVotoParte'));
  infraEfeitoTabelas();
}
function exibeVotoAssociado(bol) {
  if (bol) {
    document.getElementById('selVotoParte').style.display="";
    document.getElementById('lblVotoParte').style.display="";
  } else {
    document.getElementById('selVotoParte').style.display="none";
    document.getElementById('lblVotoParte').style.display="none";
  }

}
function verificaVoto(sel) {
  if (sel.value=='<?=VotoParteRN::$STA_DIVERGE?>' || <?=$bolRelator?1:0;?>) {
    document.getElementById('divInfraAreaDados2').style.display="";
  } else {
    document.getElementById('divInfraAreaDados2').style.display="none";
  }
  if (assoc){
    if (sel.value=='<?=VotoParteRN::$STA_ACOMPANHA?>') {
      exibeVotoAssociado(true);
    } else {
      exibeVotoAssociado(false);
    }
  }
}

function onSubmitForm() {
  return true;
}
  function finalizar() {
  }

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();" onunload="finalizar();"');
?>
<form id="frmVotoParteCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('10em');
?>


  <label id="lblUsuario" for="txtUsuario" accesskey="" class="infraLabelObrigatorio">Membro do Colegiado:</label>
  <input type="text" id="txtUsuario" name="txtUsuario" class="infraText" readonly="readonly" value="<?=PaginaSEI::tratarHTML($objVotoParteDTO->getStrNomeUsuario());?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblParte" for="txtParte" accesskey="" class="infraLabelObrigatorio">Parte do julgamento:</label>
  <input type="text" id="txtParte" name="txtParte" class="infraText" readonly="readonly" value="<?=PaginaSEI::tratarHTML($objVotoParteDTO->getStrDescricaoJulgamentoParte());?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
<?
PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblStaVotoParte" for="selStaVotoParte" accesskey="" class="infraLabelObrigatorio">Voto:</label>
  <select id="selStaVotoParte" name="selStaVotoParte" onchange="verificaVoto(this);" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelStaVotoParte?>
  </select>
<? if ($strItensSelVotoParteAssociado!='') { ?>
  <label id="lblVotoParte" for="selVotoParte" accesskey="" class="infraLabelObrigatorio">Voto Associado:</label>
  <select id="selVotoParte" name="selVotoParte" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelVotoParteAssociado?>
  </select>
<? } ?>

  <?
  PaginaSEI::getInstance()->fecharAreaDados();

  PaginaSEI::getInstance()->abrirAreaDados('15em');
  ?>

  <label id="lblProvimento" for="selProvimento" accesskey="" class="infraLabelObrigatorio">Provimento:</label>
  <select id="selProvimento" name="selProvimento" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensProvimento ?>
  </select>
  <label id="lblComplemento" for="txaComplemento" accesskey="" class="infraLabelOpcional">Complemento:</label>
  <textarea id="txaComplemento" name="txaComplemento" rows="3" class="infraTextarea"
            tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?= PaginaSEI::tratarHTML($objVotoParteDTO->getStrComplemento()) ?></textarea>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->abrirAreaDados('12em');
  ?>
  <label id="lblRessalva" for="txaRessalva" accesskey="" class="infraLabelOpcional">Ressalva:</label>
  <textarea id="txaRessalva" name="txaRessalva" rows="3"  class="infraTextarea" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>"><?=PaginaSEI::tratarHTML($objVotoParteDTO->getStrRessalva())?></textarea>

  <input type="hidden" id="hdnIdVotoParte" name="hdnIdVotoParte" value="<?=$objVotoParteDTO->getNumIdVotoParte();?>" />

   <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>