<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class MotivoDistribuicaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'motivo_distribuicao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdMotivoDistribuicao','id_motivo_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Descricao','descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinAtivo','sin_ativo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaTipo','sta_tipo');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjRelMotivoDistrColegiadoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdColegiado');

    $this->configurarPK('IdMotivoDistribuicao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarExclusaoLogica('SinAtivo', 'N');

  }
}
?>