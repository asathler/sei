<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class DispositivoDTO extends InfraDTO {

  public function getStrNomeTabela() {
    return null;
  }

  public function montar() {

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Nome');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Ressalvas');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Dispositivo');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Criterio');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Provimento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Vencedor');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdUsuarioVencedor');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Acompanham');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Vencidos');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Abstencoes');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Impedimentos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Aguardam');



  }
}
?>