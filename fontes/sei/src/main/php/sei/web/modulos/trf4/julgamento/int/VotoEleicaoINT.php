<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class VotoEleicaoINT extends InfraINT {

  public static function montarSelectVotantes($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdEleicao){

    $objVotoEleicaoDTO = new VotoEleicaoDTO();
    $objVotoEleicaoDTO->retNumIdVotoEleicao();
    $objVotoEleicaoDTO->retStrNomeUsuario();
    $objVotoEleicaoDTO->setNumIdEleicao($numIdEleicao);
    $objVotoEleicaoDTO->setOrdStrNomeUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objVotoEleicaoRN = new VotoEleicaoRN();
    $arrObjVotoEleicaoDTO = $objVotoEleicaoRN->listar($objVotoEleicaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjVotoEleicaoDTO, 'IdVotoEleicao', 'NomeUsuario');
  }

  public static function montarIconeVotacao(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, $objItemSessaoJulgamentoDTO, $bolIconeGrande = false)
  {
    $arrObjEleicaoDTO = $objItemSessaoJulgamentoDTO->getArrObjEleicaoDTO();

    $strVotacao = '';

    if (SessaoSEI::getInstance()->verificarPermissao('eleicao_votar') && $arrObjEleicaoDTO!=null) {
        $strLink = SessaoJulgamentoINT::montarLink('eleicao_votar', $_GET['acao'], $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento(), '');
        $strVotacao .= '<a onclick="event.stopPropagation();" href="javascript:void(0)" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'">';
        $strVotacao .= '<img src="'.MdJulgarIcone::ELEICAO.'" onclick="marcarTrAcessadaElemento(this);infraAbrirJanelaModal(\''.$strLink.'\',700,400,false);" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'" title="Escrut�nios Eletr�nicos" alt="Escrut�nios Eletr�nicos" style="vertical-align:middle" class="infraImg" />';
        $strVotacao .= '</a>';
    }

    return $strVotacao;
  }
}
