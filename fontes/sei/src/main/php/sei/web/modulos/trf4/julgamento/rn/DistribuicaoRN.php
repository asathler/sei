<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class DistribuicaoRN extends InfraRN {

  public static $STA_DISTRIBUIDO = 'D';
  public static $STA_REDISTRIBUIDO = 'R';
  public static $STA_PAUTADO = 'P';
  public static $STA_JULGADO = 'J';
  public static $STA_PEDIDO_VISTA = 'V';
  public static $STA_ADIADO = 'A';
  public static $STA_EM_MESA = 'M';
  public static $STA_PARA_REFERENDO = 'E';
  public static $STA_CANCELADO='X';
  public static $STA_DILIGENCIA='L';
  public static $STA_RETIRADO_PAUTA='T';

  public static $SD_REDISTRIBUIDO='N'; //redistribu�do
  public static $SD_ULTIMA='S'; //ativo/cancelado
  public static $SD_ULTIMA_JULGADO='I';  //julgados

  public static $FORM_DTH_DISTRIBUICAO = 'DTH_DISTRIBUICAO';
  public static $FORM_DTH_CANCELAMENTO = 'DTH_CANCELAMENTO';
  public static $FORM_NOME_COLEGIADO = 'NOME_COLEGIADO';
  public static $FORM_COMPOSICAO = 'COMPOSICAO';
  public static $FORM_RELATOR = 'RELATOR';
  public static $FORM_NOME = 'NOME';
  public static $FORM_SIGLA_USUARIO = 'SIGLA_USUARIO';
  public static $FORM_UNIDADE = 'UNIDADE';
  public static $FORM_SIGLA_UNIDADE = 'SIGLA_UNIDADE';
  public static $FORM_MOTIVO_IMPEDIMENTO = 'MOTIVO_IMPEDIMENTO';
  public static $FORM_MOTIVO_CANCELAMENTO= 'MOTIVO_CANCELAMENTO';
  public static $FORM_PROTOCOLO = 'PROTOCOLO';
  public static $FORM_ALGORITMO = 'ALGORITMO';



  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  public function listarValoresEstadoDistribuicao(){
    try {

      $objArrEstadoDistribuicaoDTO = array();

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_DISTRIBUIDO);
      $objEstadoDistribuicaoDTO->setStrDescricao('Distribu�do');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_REDISTRIBUIDO);
      $objEstadoDistribuicaoDTO->setStrDescricao('Redistribu�do');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_PAUTADO);
      $objEstadoDistribuicaoDTO->setStrDescricao('Pautado');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_JULGADO);
      $objEstadoDistribuicaoDTO->setStrDescricao('Julgado');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_PEDIDO_VISTA);
      $objEstadoDistribuicaoDTO->setStrDescricao('Com Pedido de Vista');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_ADIADO);
      $objEstadoDistribuicaoDTO->setStrDescricao('Adiado');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_DILIGENCIA);
      $objEstadoDistribuicaoDTO->setStrDescricao('Convertido em Dilig�ncia');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_EM_MESA);
      $objEstadoDistribuicaoDTO->setStrDescricao('Em Mesa');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_PARA_REFERENDO);
      $objEstadoDistribuicaoDTO->setStrDescricao('Para Referendo');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_RETIRADO_PAUTA);
      $objEstadoDistribuicaoDTO->setStrDescricao('Retirado de Pauta');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;

      $objEstadoDistribuicaoDTO = new EstadoDistribuicaoDTO();
      $objEstadoDistribuicaoDTO->setStrStaDistribuicao(self::$STA_CANCELADO);
      $objEstadoDistribuicaoDTO->setStrDescricao('Cancelado');
      $objArrEstadoDistribuicaoDTO[] = $objEstadoDistribuicaoDTO;
      return $objArrEstadoDistribuicaoDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de Estado da Distribuicao.',$e);
    }
  }

  private function validarDblIdProcedimento(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getDblIdProcedimento())){
      $objInfraException->adicionarValidacao('Procedimento n�o informado.');
    }
  }
  private function validarNumIdColegiadoVersao(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getNumIdColegiadoVersao())){
      $objInfraException->adicionarValidacao('Colegiado n�o informado.');
    }
  }
  private function validarNumIdUsuarioRelator(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getNumIdUsuarioRelator())){
      $objInfraException->adicionarValidacao('Relator n�o informado.');
    }
  }
  private function validarNumIdUnidadeRelator(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getNumIdUnidadeRelator())){
      $objInfraException->adicionarValidacao('Unidade do Relator n�o informada.');
    }
  }
  private function validarStrStaDistribuicao(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getStrStaDistribuicao())){
      $objInfraException->adicionarValidacao('Estado da Distribui��o n�o informado.');
    }else{
      if (!in_array($objDistribuicaoDTO->getStrStaDistribuicao(),InfraArray::converterArrInfraDTO($this->listarValoresEstadoDistribuicao(),'StaDistribuicao'))){
        $objInfraException->adicionarValidacao('Estado da Distribui��o inv�lido.');
      }
    }
  }
  private function validarNumIdMotivoPrevencao(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getNumIdMotivoPrevencao())){
      $objInfraException->lancarValidacao('Motivo da Preven��o n�o informado.');
    }
    $objMotivoDistribuicaoDTO=new MotivoDistribuicaoDTO();
    $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($objDistribuicaoDTO->getNumIdMotivoPrevencao());
    $objMotivoDistribuicaoDTO->setStrStaTipo(MotivoDistribuicaoRN::$TIPO_PREVENCAO);
    $objMotivoDistribuicaoRN=new MotivoDistribuicaoRN();
    if ($objMotivoDistribuicaoRN->contar($objMotivoDistribuicaoDTO)==0) {
      $objInfraException->adicionarValidacao('Motivo da Preven��o n�o encontrado.');
      return;
    }

    $objRelMotivoDistrColegiadoDTO=new RelMotivoDistrColegiadoDTO();
    $objRelMotivoDistrColegiadoRN=new RelMotivoDistrColegiadoRN();
    $objRelMotivoDistrColegiadoDTO->setNumIdMotivoDistribuicao($objDistribuicaoDTO->getNumIdMotivoPrevencao());
    $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao());
    $objRelMotivoDistrColegiadoDTO->setStrSinAtivoMotivoDistribuicao('S');
    if ($objRelMotivoDistrColegiadoRN->contar($objRelMotivoDistrColegiadoDTO)==0) {
      $objInfraException->adicionarValidacao('Motivo da Preven��o n�o est� dispon�vel neste colegiado.');
    }
  }
  private function validarNumIdUsuario(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getNumIdUsuario())){
      $objDistribuicaoDTO->setNumIdUsuario(null);
    }
  }
  private function validarNumIdUnidade(DistribuicaoDTO $objDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objDistribuicaoDTO->getNumIdUnidade())){
      $objDistribuicaoDTO->setNumIdUnidade(null);
    }
  }

  public function gerar($arrObjDistribuicaoDTO){
    if (InfraArray::contar($arrObjDistribuicaoDTO)==0) {
      return null;
    }

    FeedSEIProtocolos::getInstance()->setBolAcumularFeeds(true);

    $arrObjDistribuicaoDTO = $this->gerarInterno($arrObjDistribuicaoDTO);

    $objIndexacaoDTO = new IndexacaoDTO();
    $objIndexacaoRN = new IndexacaoRN();

    $objIndexacaoDTO->setArrIdProtocolos(InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO,'IdDocumentoDistribuicao'));
    $objIndexacaoDTO->setStrStaOperacao(IndexacaoRN::$TO_PROTOCOLO_METADADOS_E_CONTEUDO);
    $objIndexacaoRN->indexarProtocolo($objIndexacaoDTO);

    FeedSEIProtocolos::getInstance()->setBolAcumularFeeds(false);
    FeedSEIProtocolos::getInstance()->indexarFeeds();

    return $arrObjDistribuicaoDTO;
  }

  protected function gerarInternoControlado($arrObjDistribuicaoDTO) {
    try{


      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_gerar',__METHOD__,$arrObjDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();
      if (InfraArray::contar($arrObjDistribuicaoDTO)==0) return null;

      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
        $this->validarDblIdProcedimento($objDistribuicaoDTO, $objInfraException);
        $this->validarNumIdColegiadoVersao($objDistribuicaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      if (SessaoSEI::getInstance()->isBolHabilitada()){
        $objInfraSip = new InfraSip(SessaoSEI::getInstance());
        $objInfraSip->autenticar(SessaoSEI::getInstance()->getNumIdOrgaoUsuario(),
            SessaoSEI::getInstance()->getNumIdContextoUsuario(),
            SessaoSEI::getInstance()->getStrSiglaUsuario(),
            $arrObjDistribuicaoDTO[0]->getStrSenha());
      }

      $arrObjDistribuicaoDTO=InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO,'IdProcedimento');

      //para manter a ordem de geracao dos documentos nos testes
      ksort($arrObjDistribuicaoDTO);

      $arrIdProcedimento=array_keys($arrObjDistribuicaoDTO);
      $arrIdColegiado=array_unique(InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO,'IdColegiadoColegiadoVersao'));

      if(InfraArray::contar($arrIdColegiado)>1){
        $objInfraException->lancarValidacao('N�o � poss�vel distribuir processos de m�ltiplos Colegiados simultaneamente.');
      }
      $idColegiado=$arrIdColegiado[0];

      $objProcedimentoDTO=new ProcedimentoDTO();
      $objProcedimentoDTO->setDblIdProcedimento($arrIdProcedimento,InfraDTO::$OPER_IN);
      $objProcedimentoDTO->retDblIdProcedimento();
      $objProcedimentoDTO->retNumIdTipoProcedimento();
      $objProcedimentoRN=new ProcedimentoRN();
      $arrObjProcedimentoDTO=$objProcedimentoRN->listarRN0278($objProcedimentoDTO);
      foreach ($arrObjProcedimentoDTO as $objProcedimentoDTO) {

        //$objProcedimentoRN->bloquear($objProcedimentoDTO);

        $arrObjDistribuicaoDTO[$objProcedimentoDTO->getDblIdProcedimento()]->setNumIdTipoProcedimento($objProcedimentoDTO->getNumIdTipoProcedimento());
      }

      $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($idColegiado);
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
      $objColegiadoComposicaoDTO->retNumIdColegiadoComposicao();
      $objColegiadoComposicaoDTO->retNumIdUsuario();
      $objColegiadoComposicaoDTO->retNumIdUnidade();
      $objColegiadoComposicaoDTO->retStrSiglaUnidade();
      $objColegiadoComposicaoDTO->retStrNomeUsuario();
      $objColegiadoComposicaoDTO->retStrNomeTipoMembroColegiado();
      $objColegiadoComposicaoDTO->retNumIdTipoMembroColegiado();
      $objColegiadoComposicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
      $objColegiadoComposicaoDTO->setNumIdTipoMembroColegiado(TipoMembroColegiadoRN::$TMC_EVENTUAL,InfraDTO::$OPER_DIFERENTE);
      $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);

      $objColegiadoRN=new ColegiadoRN();
      $objColegiadoDTO=new ColegiadoDTO();
      $objColegiadoDTO->setNumIdColegiado($idColegiado);
      $objColegiadoDTO->retTodos();
      $objColegiadoDTO=$objColegiadoRN->consultar($objColegiadoDTO);

      $qtdMembros=InfraArray::contar($arrObjColegiadoComposicaoDTO);
      if ($qtdMembros==0){
        $objInfraException->lancarValidacao('Colegiado n�o possui membros.');
      }

      $qtdMembrosTitulares=0;
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        if ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR){
          $qtdMembrosTitulares++;
        }
      }



      if ($objColegiadoDTO->getNumIdUnidadeResponsavel()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()){
        $objInfraException->lancarValidacao('Unidade Atual n�o pode distribuir processos deste colegiado. ['.$objColegiadoDTO->getStrSigla().']');
      }
      if ($objColegiadoDTO->getNumQuorumMinimo()>$qtdMembros){
        $objInfraException->lancarValidacao('Colegiado n�o possui o qu�rum m�nimo.');
      }

      $objColegiadoVersaoDTO=new ColegiadoVersaoDTO();
      $objColegiadoVersaoRN=new ColegiadoVersaoRN();
      $objColegiadoVersaoDTO->setNumIdColegiado($idColegiado);
      $objColegiadoVersaoDTO->retNumIdColegiadoVersao();
      $objColegiadoVersaoDTO->retNumIdColegiado();
      $objColegiadoVersaoDTO->setStrSinUltima('S');
      $objColegiadoVersaoDTO->retStrSinEditavel();
      $objColegiadoVersaoDTO=$objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);

      //bloqueia a vers�o do colegiado
      if ($objColegiadoVersaoDTO->getStrSinEditavel()=='S'){
        $objColegiadoVersaoDTO->setStrSinEditavel('N');
        $objColegiadoVersaoRN->alterar($objColegiadoVersaoDTO);
      }

      $objRelMotivoDistrColegiadoDTO=new RelMotivoDistrColegiadoDTO();
      $objRelMotivoDistrColegiadoRN=new RelMotivoDistrColegiadoRN();
      $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao());
      $objRelMotivoDistrColegiadoDTO->setStrSinAtivoMotivoDistribuicao('S');
      $objRelMotivoDistrColegiadoDTO->retNumIdMotivoDistribuicao();
      $objRelMotivoDistrColegiadoDTO->retStrDescricaoMotivoDistribuicao();

      $arrOjRelMotivoDistrColegiadoDTO=$objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO);

      $arrMotivoDistribuicao=InfraArray::converterArrInfraDTO($arrOjRelMotivoDistrColegiadoDTO,'DescricaoMotivoDistribuicao','IdMotivoDistribuicao');



      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {

        //verifica se j� houve julgamento por este colegiado
//        $objDistribuicaoDTOBanco = new DistribuicaoDTO();
//        $objDistribuicaoDTOBanco->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
//        $objDistribuicaoDTO->setStrStaUltimo(array(DistribuicaoRN::$SD_ULTIMA,DistribuicaoRN::$SD_ULTIMA_JULGADO),InfraDTO::$OPER_IN);
//        $objDistribuicaoDTOBanco->setNumIdColegiadoColegiadoVersao($idColegiado);
//        $objDistribuicaoDTOBanco->retNumIdDistribuicao();
//        $objDistribuicaoDTOBanco->setStrStaDistribuicao(self::$STA_JULGADO);
//        if ($this->contar($objDistribuicaoDTOBanco) > 0) {
//          $objProtocoloDTO = new ProtocoloDTO();
//          $objProtocoloRN = new ProtocoloRN();
//          $objProtocoloDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
//          $objProtocoloDTO->retStrProtocoloFormatado();
//          $objProtocoloDTO = $objProtocoloRN->consultarRN0186($objProtocoloDTO);
//          $objInfraException->lancarValidacao('Processo j� foi julgado neste colegiado. (' . $objProtocoloDTO->getStrProtocoloFormatado() . ')');
//        }

        $bolPrevencao = $objDistribuicaoDTO->getStrSinPrevencao() == 'S';
        if ($bolPrevencao) {
          $this->validarNumIdMotivoPrevencao($objDistribuicaoDTO, $objInfraException);
          $this->validarNumIdUsuarioRelator($objDistribuicaoDTO, $objInfraException);
          $this->validarNumIdUnidadeRelator($objDistribuicaoDTO, $objInfraException);
        } else {
          $objDistribuicaoDTO->unSetNumIdMotivoPrevencao();
          $arrObjImpedimentoDTO = $objDistribuicaoDTO->getArrObjImpedimentoDTO();
          //verificar somente membros titulares
          if (InfraArray::contar($arrObjImpedimentoDTO) == $qtdMembrosTitulares) {
            $objInfraException->lancarValidacao('Todos os membros titulares deste colegiado possuem impedimentos.');
          }
        }

        $objInfraException->lancarValidacoes();

        $objDistribuicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
        $objDistribuicaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());

      }

      //executa o algoritmo de distribui��o
      $objAlgoritmoRN = new AlgoritmoRN();
      $arrObjDistribuicaoDTO = $objAlgoritmoRN->distribuir($arrObjDistribuicaoDTO);


      /** @var DistribuicaoDTO $objDistribuicaoDTO */
      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {

        $bolPrevencao = $objDistribuicaoDTO->getStrSinPrevencao() == 'S';
        if ($bolPrevencao) {
          $arrObjImpedimentoDTO = null;
        } else {
          $arrObjImpedimentoDTO = $objDistribuicaoDTO->getArrObjImpedimentoDTO();
        }

        $objInfraParametro = new InfraParametro($this->getObjInfraIBanco());
        $objDocumentoDTO = new DocumentoDTO();
        $objDocumentoRN = new DocumentoRN();

        //verifica se existe distribui��o para este colegiado (redistribuir)
        $redistribuicao=false;

        $objDocumentoDTO->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());

        $objDistribuicaoDTOBanco=new DistribuicaoDTO();
        $objDistribuicaoDTOBanco->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
        $objDistribuicaoDTOBanco->setNumIdColegiadoColegiadoVersao($objDistribuicaoDTO->getNumIdColegiadoColegiadoVersao());
        $objDistribuicaoDTOBanco->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
        $objDistribuicaoDTOBanco->setStrStaDistribuicao(self::$STA_CANCELADO,InfraDTO::$OPER_DIFERENTE);
        $objDistribuicaoDTOBanco->retNumIdDistribuicao();
        $objDistribuicaoDTOBanco->retNumIdDistribuicaoAgrupador();
        $objDistribuicaoDTOBanco->retStrStaDistribuicao();
        $objDistribuicaoDTOBanco->retStrProtocoloFormatado();
        $objDistribuicaoDTOBanco=$this->consultar($objDistribuicaoDTOBanco);
        if ($objDistribuicaoDTOBanco!=null) {
          if (!in_array($objDistribuicaoDTOBanco->getStrStaDistribuicao(),array(self::$STA_RETIRADO_PAUTA,self::$STA_DISTRIBUIDO,self::$STA_REDISTRIBUIDO,self::$STA_DILIGENCIA,self::$STA_ADIADO,self::$STA_PEDIDO_VISTA))){
            $arrSituacao=$this->listarValoresEstadoDistribuicao();
            $strSituacao=$arrSituacao[$objDistribuicaoDTOBanco->getStrStaDistribuicao()];
            $objInfraException->lancarValidacao('N�o � poss�vel realizar a distribui��o do processo '.$objDistribuicaoDTOBanco->getStrProtocoloFormatado().'. ('.$strSituacao.')');
          }
          $objDistribuicaoDTOBanco->setStrStaUltimo(DistribuicaoRN::$SD_REDISTRIBUIDO);
          $this->alterar($objDistribuicaoDTOBanco);
          $redistribuicao=true;
        }

        if ($redistribuicao){
          $objDistribuicaoDTO->setNumIdDistribuicaoAgrupador($objDistribuicaoDTOBanco->getNumIdDistribuicaoAgrupador());
          if(in_array($objDistribuicaoDTOBanco->getStrStaDistribuicao(),array(self::$STA_PEDIDO_VISTA,self::$STA_DILIGENCIA))) {
            $objDistribuicaoDTO->setStrStaDistribuicao($objDistribuicaoDTOBanco->getStrStaDistribuicao());
          } else {
            $objDistribuicaoDTO->setStrStaDistribuicao(self::$STA_REDISTRIBUIDO);
          }
        } else {
          $objDistribuicaoDTO->setNumIdDistribuicaoAgrupador(null);
          $objDistribuicaoDTO->setStrStaDistribuicao(self::$STA_DISTRIBUIDO);
        }
        $objDistribuicaoDTO->setStrStaDistribuicaoAnterior($redistribuicao?self::$STA_REDISTRIBUIDO:self::$STA_DISTRIBUIDO);
        $objDistribuicaoDTO->setDthDistribuicao(InfraData::getStrDataHoraAtual());
        $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
        $objDistribuicaoDTO->setDthSituacaoAtual(InfraData::getStrDataHoraAtual());

        $objAtividadeDTO = new AtividadeDTO();
        $objAtividadeRN = new AtividadeRN();
        $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objAtividadeDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
        $objAtividadeDTO->setDthConclusao(null);

        $objSeiRN = new SeiRN();

        if ($objAtividadeRN->contarRN0035($objAtividadeDTO) == 0) {
          //reabertura autom�tica
          $objEntradaReabrirProcessoAPI = new EntradaReabrirProcessoAPI();
          $objEntradaReabrirProcessoAPI->setIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
          $objSeiRN->reabrirProcesso($objEntradaReabrirProcessoAPI);
        }

        //gera certid�o de distribui��o

        $objDocumentoDTO->setDblIdDocumento(null);
        if ($redistribuicao) {
          $objDocumentoDTO->setNumIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_REDISTRIBUICAO));
        } else {
          $objDocumentoDTO->setNumIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_DISTRIBUICAO));
        }

        $objProcedimentoRN = new ProcedimentoRN();
        $objProcedimentoDTO=new ProcedimentoDTO();
        $objProcedimentoDTO->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
        $objProcedimentoDTO->retStrProtocoloProcedimentoFormatado();
        $objProcedimentoDTO->retStrDescricaoProtocolo();
        $objProcedimentoDTO->retStrNomeTipoProcedimento();
        $objProcedimentoDTO->retStrStaNivelAcessoGlobalProtocolo();
        $objProcedimentoDTO=$objProcedimentoRN->consultarRN0201($objProcedimentoDTO);

        $idRelator=$objDistribuicaoDTO->getNumIdUsuarioRelator();
        $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'IdUsuario');

        $objDistribuicaoDTO->setStrProtocoloFormatado($objProcedimentoDTO->getStrProtocoloProcedimentoFormatado());
        $objDistribuicaoDTO->setStrNomeTipoProcedimento($objProcedimentoDTO->getStrNomeTipoProcedimento());
        $objDistribuicaoDTO->setStrStaNivelAcessoGlobal($objProcedimentoDTO->getStrStaNivelAcessoGlobalProtocolo());
        $objDistribuicaoDTO->setStrNomeUsuarioRelator($arrObjColegiadoComposicaoDTO[$idRelator]->getStrNomeUsuario());
        $objDistribuicaoDTO->setStrSiglaUnidadeRelator($arrObjColegiadoComposicaoDTO[$idRelator]->getStrSiglaUnidade());
        $objDistribuicaoDTO->setStrDescricaoProtocolo($objProcedimentoDTO->getStrDescricaoProtocolo());

        //InfraArray::ordenarArrInfraDTO($arrObjColegiadoComposicaoDTO,'NomeUsuario',InfraArray::$TIPO_ORDENACAO_ASC);
        $arrObjImpedimentoDTO=InfraArray::indexarArrInfraDTO($arrObjImpedimentoDTO, 'IdUsuario');

        $strXML = '';
        $strXML .= '<?xml version="1.0" encoding="iso-8859-1"?>'."\n";
        $strXML .= '<documento>'."\n";
        $strXML .= '<atributo nome="'.self::$FORM_PROTOCOLO.'" titulo="Processo">'.$objDistribuicaoDTO->getStrProtocoloFormatado().' - '.$objDistribuicaoDTO->getStrNomeTipoProcedimento().'</atributo>'."\n";
        $strXML .= '<atributo nome="'.self::$FORM_DTH_DISTRIBUICAO.'" titulo="Data da '.($redistribuicao?'Red':'D').'istribui��o">'.$objDistribuicaoDTO->getDthDistribuicao().'</atributo>'."\n";
        $strXML .= '<atributo nome="'.self::$FORM_NOME_COLEGIADO.'" id="'.$idColegiado.'" titulo="Colegiado">'.InfraString::formatarXML($objColegiadoDTO->getStrNome().' ('.$objColegiadoDTO->getStrSigla().')').'</atributo>'."\n";
        $strXML .= '<atributo nome="'.self::$FORM_COMPOSICAO.'" versao="'.$objColegiadoVersaoDTO->retNumIdColegiadoVersao().'" titulo="Composi��o do Colegiado">'."\n";
        $strXML .= '<valores>'."\n";
        foreach($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO){
          $strXML .= '<valor id="'.$objColegiadoComposicaoDTO->getNumIdColegiadoComposicao().'">';
          $strXML .= InfraString::formatarXML($objColegiadoComposicaoDTO->getStrNomeUsuario()).' ('.InfraString::formatarXML($objColegiadoComposicaoDTO->getStrSiglaUnidade().')');
          if ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR){
            $strXML .= ' - '.InfraString::formatarXML($objColegiadoComposicaoDTO->getStrNomeTipoMembroColegiado());
          }
          if (isset($arrObjImpedimentoDTO[$objColegiadoComposicaoDTO->getNumIdUsuario()])) {
            $strXML .= ' - Impedido: '.InfraString::formatarXML($arrMotivoDistribuicao[$arrObjImpedimentoDTO[$objColegiadoComposicaoDTO->getNumIdUsuario()]->getNumIdMotivoImpedimento()]);
          }
          $strXML .='</valor>'."\n";
        }
        $strXML .= '</valores>'."\n";
        $strXML .= '</atributo>'."\n";

        $strXML .= '<atributo nome="'.self::$FORM_RELATOR.'" titulo="Relator" id="'.$idRelator.'">';
        $strXML .= InfraString::formatarXML($objDistribuicaoDTO->getStrNomeUsuarioRelator()).' ('.InfraString::formatarXML($objDistribuicaoDTO->getStrSiglaUnidadeRelator().')');
        if ($bolPrevencao) {
          $strXML .= ' - Preven��o: '.$arrMotivoDistribuicao[$objDistribuicaoDTO->getNumIdMotivoPrevencao()];
        }
        $strXML .= '</atributo>'."\n";
        $objAlgoritmoRN=new AlgoritmoRN();
        $arrObjTipoDTO=InfraArray::indexarArrInfraDTO($objAlgoritmoRN->listarValoresAlgoritmo(),'StaTipo');

        $strXML .= '<atributo nome="'.self::$FORM_ALGORITMO.'" titulo="Algoritmo de Distribui��o" id="'.$objColegiadoDTO->getStrStaAlgoritmoDistribuicao().'" ocultar="S">'.$arrObjTipoDTO[$objColegiadoDTO->getStrStaAlgoritmoDistribuicao()]->getStrDescricao().'</atributo>'."\n";
        $strXML .= '</documento>';

        $objDocumentoDTO->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
        $objDocumentoDTO->setDblIdDocumentoEdoc(null);
        $objDocumentoDTO->setDblIdDocumentoEdocBase(null);
        $objDocumentoDTO->setNumIdUnidadeResponsavel(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objDocumentoDTO->setStrNumero(null);
        $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_FORMULARIO_AUTOMATICO);
        $objDocumentoDTO->setStrConteudo(InfraUtil::filtrarISO88591($strXML));

        $objProtocoloDTO = new ProtocoloDTO();
        $objProtocoloDTO->setDblIdProtocolo(null);
        $objProtocoloDTO->setStrStaNivelAcessoLocal(ProtocoloRN::$NA_PUBLICO);
        $objProtocoloDTO->setStrDescricao(null);
        $objProtocoloDTO->setDtaGeracao(InfraData::getStrDataAtual());
        $objProtocoloDTO->setArrObjRelProtocoloAssuntoDTO(array());

        $objProtocoloDTO->setArrObjParticipanteDTO(array());
        $objProtocoloDTO->setArrObjObservacaoDTO(array());
        $objDocumentoDTO->setObjProtocoloDTO($objProtocoloDTO);

        $objDocumentoDTO = $objDocumentoRN->cadastrarRN0003($objDocumentoDTO);

        //grava a distribui��o
        $objDistribuicaoDTO->setDblIdDocumentoDistribuicao($objDocumentoDTO->getDblIdDocumento());

        $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
        $ret = $objDistribuicaoBD->cadastrar($objDistribuicaoDTO);

        //copiar dados de pedido de vista, se necess�rio
        if ($redistribuicao && $objDistribuicaoDTOBanco->getStrStaDistribuicao()==self::$STA_PEDIDO_VISTA){
          $objPedidoVistaDTO=new PedidoVistaDTO();
          $objPedidoVistaRN=new PedidoVistaRN();
          $objPedidoVistaDTO->setNumIdDistribuicao($objDistribuicaoDTOBanco->getNumIdDistribuicao());
          $objPedidoVistaDTO->setDthDevolucao(null);
          $objPedidoVistaDTO->retTodos();
          $arrObjPedidoVistaDTO=$objPedidoVistaRN->listar($objPedidoVistaDTO);
          foreach ($arrObjPedidoVistaDTO as $objPedidoVistaDTO) {
            $objPedidoVistaDTO->unSetNumIdPedidoVista();
            //verificar se membro continua no colegiado
            $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
            $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
            $objColegiadoComposicaoDTO->setNumIdUnidade($objPedidoVistaDTO->getNumIdUnidade());
            $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($idColegiado);
            $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
            $objColegiadoComposicaoDTO->retNumIdUsuario();
            $objColegiadoComposicaoDTO=$objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);
            if($objColegiadoComposicaoDTO==null || $objPedidoVistaDTO->getNumIdUsuario()!=$objColegiadoComposicaoDTO->getNumIdUsuario()){
              $objInfraException->lancarValidacao('N�o � possivel redistribuir processo com pedido de vista a membro que n�o pertence mais ao colegiado.');
            }

            $objPedidoVistaDTO->setNumIdUsuario($objColegiadoComposicaoDTO->getNumIdUsuario());
            $objPedidoVistaDTO->setNumIdDistribuicao($ret->getNumIdDistribuicao());
            $objPedidoVistaRN->cadastrar($objPedidoVistaDTO);
          }
        }

        //se n�o for definida usa a distribuicao atual como agrupador
        if($objDistribuicaoDTO->getNumIdDistribuicaoAgrupador()==null){
          $objDistribuicaoDTO2=new DistribuicaoDTO();
          $objDistribuicaoDTO2->setNumIdDistribuicao($ret->getNumIdDistribuicao());
          $objDistribuicaoDTO2->setNumIdDistribuicaoAgrupador($ret->getNumIdDistribuicao());
          $objDistribuicaoBD->alterar($objDistribuicaoDTO2);
        }

        //grava os impedimentos
        if (!$bolPrevencao && InfraArray::contar($arrObjImpedimentoDTO)>0){
          $objImpedimentoRN=new ImpedimentoRN();
          foreach ($arrObjImpedimentoDTO as $objImpedimentoDTO) {
            $objImpedimentoDTO->setNumIdDistribuicao($ret->getNumIdDistribuicao());
            $objImpedimentoRN->cadastrar($objImpedimentoDTO);
          }
//          $arrObjImpedimentoDTO=InfraArray::indexarArrInfraDTO($arrObjImpedimentoDTO, 'IdColegiadoComposicao');
        }


        //gera andamento de (re)distribui��o
        $arrObjAtributoAndamentoDTO = array();
        $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
        $objAtributoAndamentoDTO->setStrNome('COLEGIADO');
        $objAtributoAndamentoDTO->setStrValor($objColegiadoDTO->getStrNome());
        $objAtributoAndamentoDTO->setStrIdOrigem($idColegiado);
        $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

        $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
        $objAtributoAndamentoDTO->setStrNome('RELATOR');
        $objAtributoAndamentoDTO->setStrValor($arrObjColegiadoComposicaoDTO[$idRelator]->getStrNomeUsuario());
        $objAtributoAndamentoDTO->setStrIdOrigem($objDistribuicaoDTO->getNumIdUsuarioRelator());
        $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

        $objAtividadeDTO = new AtividadeDTO();
        $objAtividadeDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
        $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objAtividadeDTO->setStrIdTarefaModuloTarefa($redistribuicao?TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_PROCESSO_REDISTRIBUIDO:TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_PROCESSO_DISTRIBUIDO);
        $objAtividadeDTO->setArrObjAtributoAndamentoDTO($arrObjAtributoAndamentoDTO);

        $objAtividadeRN = new AtividadeRN();
        $objAtividadeRN->gerarInternaRN0727($objAtividadeDTO);

        //enviar processo para a unidade do relator

        $objPesquisaPendenciaDTO = new PesquisaPendenciaDTO();
        $objPesquisaPendenciaDTO->setDblIdProtocolo(array($objDistribuicaoDTO->getDblIdProcedimento()));
        $objPesquisaPendenciaDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
        $objPesquisaPendenciaDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $arrObjProcedimentoDTO = $objAtividadeRN->listarPendenciasRN0754($objPesquisaPendenciaDTO);

        $objAtividadeDTO = new AtividadeDTO();
        $objAtividadeDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
        $objAtividadeDTO->setNumIdUsuario(null);
        $objAtividadeDTO->setNumIdUsuarioOrigem(SessaoSEI::getInstance()->getNumIdUsuario());
        $objAtividadeDTO->setNumIdUnidade($arrObjColegiadoComposicaoDTO[$idRelator]->getNumIdUnidade());
        $objAtividadeDTO->setNumIdUnidadeOrigem(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objAtividadeDTO->setDtaPrazo(null);

        if ($objProcedimentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO){
          $objAcessoDTO=new AcessoDTO();
          $objAcessoDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
          $objAcessoDTO->setNumIdUsuario($arrObjColegiadoComposicaoDTO[$idRelator]->getNumIdUsuario());
          $objAcessoDTO->setNumIdUnidade($arrObjColegiadoComposicaoDTO[$idRelator]->getNumIdUnidade());
          $objAcessoDTO->retNumIdAcesso();
          $objAcessoDTO->setStrStaTipo(AcessoRN::$TA_CREDENCIAL_PROCESSO);
          $objAcessoDTO->setNumMaxRegistrosRetorno(1);
          $objAcessoRN=new AcessoRN();
          if($objAcessoRN->consultar($objAcessoDTO)==null){
            $objConcederCredencialDTO = new ConcederCredencialDTO();
            $objConcederCredencialDTO->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
            $objConcederCredencialDTO->setNumIdUsuario($arrObjColegiadoComposicaoDTO[$idRelator]->getNumIdUsuario());
            $objConcederCredencialDTO->setNumIdUnidade($arrObjColegiadoComposicaoDTO[$idRelator]->getNumIdUnidade());
            $objConcederCredencialDTO->setArrAtividadesOrigem($arrObjProcedimentoDTO[0]->getArrObjAtividadeDTO());
            $objAtividadeRN->concederCredencial($objConcederCredencialDTO);
          }
        } else {
          $objEnviarProcessoDTO = new EnviarProcessoDTO();
          $objEnviarProcessoDTO->setArrAtividadesOrigem($arrObjProcedimentoDTO[0]->getArrObjAtividadeDTO());
          $objEnviarProcessoDTO->setArrAtividades(array($objAtividadeDTO));
          $objEnviarProcessoDTO->setStrSinManterAberto('N');
          $objEnviarProcessoDTO->setStrSinEnviarEmailNotificacao('N');
          $objEnviarProcessoDTO->setStrSinRemoverAnotacoes('N');

          $objAtividadeRN->enviarRN0023($objEnviarProcessoDTO);
        }


      }


      return array_values($arrObjDistribuicaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro gerando Distribui��o.',$e);
    }
  }

  protected function alterarControlado(DistribuicaoDTO $objDistribuicaoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_alterar',__METHOD__,$objDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();
      $objDistribuicaoDTOBanco=new DistribuicaoDTO();
      $objDistribuicaoDTOBanco->setNumIdDistribuicao($objDistribuicaoDTO->getNumIdDistribuicao());
      $objDistribuicaoDTOBanco->retStrStaDistribuicao();
      $objDistribuicaoDTOBanco->retNumIdDistribuicaoAgrupador();
      $objDistribuicaoDTOBanco->retDblIdProcedimento();
      $objDistribuicaoDTOBanco->retNumIdColegiadoColegiadoVersao();
      $objDistribuicaoDTOBanco=$this->consultar($objDistribuicaoDTOBanco);
      $staAtual=$objDistribuicaoDTOBanco->getStrStaDistribuicao();
      if ($staAtual==self::$STA_JULGADO) {
        if($_GET['acao']=='item_sessao_julgamento_finalizar'){
          $objDistribuicaoDTO2=new DistribuicaoDTO();
          $objDistribuicaoDTO2->setNumIdUsuarioRelatorAcordao($objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao());
          $objDistribuicaoDTO2->setNumIdUnidadeRelatorAcordao($objDistribuicaoDTO->getNumIdUnidadeRelatorAcordao());
          $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
          $objDistribuicaoBD->alterar($objDistribuicaoDTO);
          return;
        }
        $objInfraException->lancarValidacao('Distribui��o julgada n�o permite altera��o.');
      }
      $objDistribuicaoDTO->unSetDthSituacaoAtual();
      $objDistribuicaoDTO->unSetNumIdColegiadoVersao();

      if ($objDistribuicaoDTO->isSetDblIdProcedimento()){
        $this->validarDblIdProcedimento($objDistribuicaoDTO, $objInfraException);
      }
      
      if ($objDistribuicaoDTO->isSetNumIdUsuarioRelator()){
        $this->validarNumIdUsuarioRelator($objDistribuicaoDTO, $objInfraException);
      }
      if ($objDistribuicaoDTO->isSetStrStaDistribuicao()){
        $this->validarStrStaDistribuicao($objDistribuicaoDTO, $objInfraException);
        //retorno para distribuido: se j� tiver sido redistribuido seta para redistribuido.
        if ($objDistribuicaoDTO->getStrStaDistribuicao()==self::$STA_DISTRIBUIDO){
          $objDistribuicaoDTO2=new DistribuicaoDTO();
          $objDistribuicaoDTO2->setDblIdProcedimento($objDistribuicaoDTOBanco->getDblIdProcedimento());
          $objDistribuicaoDTO2->setNumIdDistribuicaoAgrupador($objDistribuicaoDTOBanco->getNumIdDistribuicaoAgrupador());
          if ($this->contar($objDistribuicaoDTO2)>1) {
            $objDistribuicaoDTO->setStrStaDistribuicao(self::$STA_REDISTRIBUIDO);
          }
        }
        $this->validarProximoEstado($staAtual,$objDistribuicaoDTO->getStrStaDistribuicao(),$objInfraException);
        if ($staAtual!=$objDistribuicaoDTO->getStrStaDistribuicao()) {
          $objDistribuicaoDTO->setDthSituacaoAtual(InfraData::getStrDataHoraAtual());
          $objDistribuicaoDTO->setStrStaDistribuicaoAnterior($objDistribuicaoDTOBanco->getStrStaDistribuicao());
        }
      }
      if ($objDistribuicaoDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objDistribuicaoDTO, $objInfraException);
      }
      if ($objDistribuicaoDTO->isSetNumIdUnidade()){
        $this->validarNumIdUnidade($objDistribuicaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      $objDistribuicaoBD->alterar($objDistribuicaoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Distribui��o.',$e);
    }
  }
  protected function excluirControlado($arrObjDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_excluir',__METHOD__,$arrObjDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
        $objDistribuicaoBD->excluir($objDistribuicaoDTO);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Distribui��o.',$e);
    }
  }
  protected function consultarConectado(DistribuicaoDTO $objDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_consultar',__METHOD__,$objDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objDistribuicaoBD->consultar($objDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Distribui��o.',$e);
    }
  }
  protected function listarConectado($objDistribuicaoDTO) {
    try {

      if(!($objDistribuicaoDTO instanceof DistribuicaoDTO) && !($objDistribuicaoDTO instanceof PainelDistribuicaoDTO)){
        throw new InfraException('Par�metro inv�lido para a fun��o contar.');
      }
      //Valida Permissao
      //SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_listar',__METHOD__,$objDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objDistribuicaoBD->listar($objDistribuicaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Distribui��es.',$e);
    }
  }
  protected function contarConectado($objDistribuicaoDTO){
    try {

      if(!($objDistribuicaoDTO instanceof DistribuicaoDTO) && !($objDistribuicaoDTO instanceof PainelDistribuicaoDTO)){
        throw new InfraException('Par�metro inv�lido para a fun��o contar.');
      }
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_listar',__METHOD__,$objDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objDistribuicaoBD->contar($objDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Distribui��es.',$e);
    }
  }

  /**
   * @param DistribuicaoDTO $objDistribuicaoDTO
   * @throws InfraException
   * @return DocumentoDTO
   */
  protected function cancelarControlado(DistribuicaoDTO $objDistribuicaoDTO){
    try {
      $objDistribuicaoDTOBanco = new DistribuicaoDTO();
      $objDistribuicaoDTOBanco->setNumIdDistribuicao($objDistribuicaoDTO->getNumIdDistribuicao());
      $objDistribuicaoDTOBanco->retNumIdDistribuicao();
      $objDistribuicaoDTOBanco->retStrStaDistribuicao();
      $objDistribuicaoDTOBanco->retDthDistribuicao();
      $objDistribuicaoDTOBanco->retNumIdColegiadoColegiadoVersao();
      $objDistribuicaoDTOBanco->retStrNomeColegiado();
      $objDistribuicaoDTOBanco->retStrSiglaColegiado();
      $objDistribuicaoDTOBanco->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
      $objDistribuicaoDTOBanco->retDblIdProcedimento();
      $objDistribuicaoDTOBanco->retStrProtocoloFormatado();
      $objDistribuicaoDTOBanco->retStrNomeTipoProcedimento();
      $objDistribuicaoDTOBanco = $this->consultar($objDistribuicaoDTOBanco);

      $objInfraException=new InfraException();
      if ($objDistribuicaoDTOBanco==null) {
        $objInfraException->lancarValidacao('Distribui��o n�o localizada.');
      }
      if (in_array($objDistribuicaoDTOBanco->getStrStaDistribuicao(),array(self::$STA_CANCELADO,self::$STA_PAUTADO,self::$STA_EM_MESA,self::$STA_PARA_REFERENDO,self::$STA_PEDIDO_VISTA,self::$STA_JULGADO))){
        $objInfraException->lancarValidacao('Situa��o atual n�o permite cancelamento da distribui��o.');
      }
      
      $objColegiadoDTO=new ColegiadoDTO();
      $objColegiadoDTO->setNumIdColegiado($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
      $objColegiadoDTO->setNumIdUnidadeResponsavel(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objColegiadoRN=new ColegiadoRN();
      if($objColegiadoRN->contar($objColegiadoDTO)!=1){
        $objInfraException->lancarValidacao('Unidade atual n�o � respons�vel por distribui��es do colegiado.');
      }

      $objDistribuicaoDTOBanco->setDthSituacaoAtual(InfraData::getStrDataHoraAtual());
      $objDistribuicaoDTOBanco->setStrStaDistribuicao(self::$STA_CANCELADO);
//      $objDistribuicaoDTOBanco->setStrStaUltimo(DistribuicaoRN::$SD_REDISTRIBUIDO);
      $objDistribuicaoDTOBanco->setNumIdMotivoCancelamento($objDistribuicaoDTO->getNumIdMotivoCancelamento());
      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      $objDistribuicaoBD->alterar($objDistribuicaoDTOBanco);


      $strXML = '';
      $strXML .= '<?xml version="1.0" encoding="iso-8859-1"?>'."\n";
      $strXML .= '<documento>'."\n";
      $strXML .= '<atributo nome="'.self::$FORM_PROTOCOLO.'" titulo="Processo">'.$objDistribuicaoDTOBanco->getStrProtocoloFormatado().' - '.$objDistribuicaoDTOBanco->getStrNomeTipoProcedimento().'</atributo>'."\n";
      $strXML .= '<atributo nome="'.self::$FORM_DTH_DISTRIBUICAO.'" titulo="Data da '.($objDistribuicaoDTOBanco->getStrStaDistribuicao()==self::$STA_REDISTRIBUIDO?'Red':'D').'istribui��o">'.$objDistribuicaoDTOBanco->getDthDistribuicao().'</atributo>'."\n";
      $strXML .= '<atributo nome="'.self::$FORM_NOME_COLEGIADO.'" id="'.$objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao().'" titulo="Colegiado">'.InfraString::formatarXML($objDistribuicaoDTOBanco->getStrNomeColegiado().' ('.$objDistribuicaoDTOBanco->getStrSiglaColegiado().')').'</atributo>'."\n";
      $strXML .= '<atributo nome="'.self::$FORM_DTH_CANCELAMENTO.'" titulo="Data do cancelamento">'.$objDistribuicaoDTOBanco->getDthSituacaoAtual().'</atributo>'."\n";
      $strXML .= '<atributo nome="'.self::$FORM_MOTIVO_CANCELAMENTO.'" id="'.$objDistribuicaoDTO->getNumIdMotivoCancelamento().'" titulo="Motivo do cancelamento">'.$objDistribuicaoDTO->getStrDescricaoMotivoCancelamento().'</atributo>'."\n";
      $strXML .= '</documento>';

      $objDocumentoDTO = new DocumentoDTO();
      $objDocumentoRN = new DocumentoRN();
      $objInfraParametro = new InfraParametro($this->getObjInfraIBanco());

      $objDocumentoDTO->setDblIdDocumento(null);
      $objDocumentoDTO->setDblIdProcedimento($objDistribuicaoDTOBanco->getDblIdProcedimento());
      $objDocumentoDTO->setNumIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_CANCELAMENTO_DISTRIBUICAO));
      $objDocumentoDTO->setStrConteudo(InfraUtil::filtrarISO88591($strXML));
      $objDocumentoDTO->setDblIdDocumentoEdoc(null);
      $objDocumentoDTO->setDblIdDocumentoEdocBase(null);
      $objDocumentoDTO->setNumIdUnidadeResponsavel(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objDocumentoDTO->setStrNumero(null);
      $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_FORMULARIO_AUTOMATICO);

      $objProtocoloDTO = new ProtocoloDTO();
      $objProtocoloDTO->setDblIdProtocolo(null);
      $objProtocoloDTO->setStrStaNivelAcessoLocal(ProtocoloRN::$NA_PUBLICO);
      $objProtocoloDTO->setStrDescricao(null);
      $objProtocoloDTO->setDtaGeracao(InfraData::getStrDataAtual());
      $objProtocoloDTO->setArrObjRelProtocoloAssuntoDTO(array());

      $objProtocoloDTO->setArrObjParticipanteDTO(array());
      $objProtocoloDTO->setArrObjObservacaoDTO(array());
      $objDocumentoDTO->setObjProtocoloDTO($objProtocoloDTO);

      $objDocumentoDTO = $objDocumentoRN->cadastrarRN0003($objDocumentoDTO);

      //

      $arrObjAtributoAndamentoDTO = array();
      $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
      $objAtributoAndamentoDTO->setStrNome('COLEGIADO');
      $objAtributoAndamentoDTO->setStrValor($objDistribuicaoDTOBanco->getStrNomeColegiado());
      $objAtributoAndamentoDTO->setStrIdOrigem($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
      $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

      $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
      $objAtributoAndamentoDTO->setStrNome('MOTIVO');
      $objAtributoAndamentoDTO->setStrValor($objDistribuicaoDTO->getStrDescricaoMotivoCancelamento());
      $objAtributoAndamentoDTO->setStrIdOrigem($objDistribuicaoDTO->getNumIdMotivoCancelamento());
      $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

      $objAtividadeDTO = new AtividadeDTO();
      $objAtividadeDTO->setDblIdProtocolo($objDistribuicaoDTOBanco->getDblIdProcedimento());
      $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objAtividadeDTO->setStrIdTarefaModuloTarefa(TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_CANCELAMENTO_DISTRIBUICAO);
      $objAtividadeDTO->setArrObjAtributoAndamentoDTO($arrObjAtributoAndamentoDTO);

      $objAtividadeRN = new AtividadeRN();
      $objAtividadeRN->gerarInternaRN0727($objAtividadeDTO);


      return $objDocumentoDTO;

    } catch(Exception $e) {
      throw new InfraException('Erro cancelando Distribui��o.', $e);
    }

  }

  private function validarProximoEstado($estadoAtual,$proximoEstado,$objInfraException) {

    switch ($estadoAtual) {
      case self::$STA_JULGADO:
        $objInfraException->lancarValidacao('Distribui��o julgada n�o permite altera��o.');
        break;
      case self::$STA_PEDIDO_VISTA:
      case self::$STA_RETIRADO_PAUTA:
        //if (in_array($proximoEstado,array(self::$STA_EM_MESA,self::$STA_DISTRIBUIDO,self::$STA_REDISTRIBUIDO))) return;
        return;
        break;
      case self::$STA_DISTRIBUIDO:
      case self::$STA_DILIGENCIA:
      case self::$STA_REDISTRIBUIDO:
      case self::$STA_ADIADO:
        if (in_array($proximoEstado,array(self::$STA_EM_MESA,self::$STA_PAUTADO,self::$STA_PARA_REFERENDO))) return;
        break;

      case self::$STA_PAUTADO:
      case self::$STA_EM_MESA:
        if (in_array($proximoEstado,array(self::$STA_PEDIDO_VISTA,self::$STA_DISTRIBUIDO,self::$STA_REDISTRIBUIDO,self::$STA_ADIADO,self::$STA_JULGADO,self::$STA_DILIGENCIA,self::$STA_RETIRADO_PAUTA))) return;
        break;
      case self::$STA_PARA_REFERENDO:
        if (in_array($proximoEstado,array(self::$STA_DISTRIBUIDO,self::$STA_REDISTRIBUIDO,self::$STA_ADIADO,self::$STA_JULGADO))) return;
        break;
      default:
        throw new InfraException('Estado atual inv�lido.');
    }
    if ($estadoAtual==$proximoEstado) return;
    $objInfraException->lancarValidacao('Distribui��o n�o permite a Situa��o informada.');
  }

  protected function alterarEstadoControlado(DistribuicaoDTO $objDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_alterar',__METHOD__,$objDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrStaDistribuicao($objDistribuicaoDTO, $objInfraException);
      ///n�o permitir altera��o em concluidos
      $objDistribuicaoDTOBanco=new DistribuicaoDTO();
      $objDistribuicaoDTOBanco->setNumIdDistribuicao($objDistribuicaoDTO->getNumIdDistribuicao());
      $objDistribuicaoDTOBanco->retNumIdDistribuicao();
      $objDistribuicaoDTOBanco->retStrStaDistribuicao();
      $objDistribuicaoDTOBanco->retDblIdProcedimento();
      $objDistribuicaoDTOBanco->retNumIdColegiadoColegiadoVersao();
      $objDistribuicaoDTOBanco=$this->consultar($objDistribuicaoDTOBanco);

      $staAtual=$objDistribuicaoDTOBanco->getStrStaDistribuicao();
      $staNovo=$objDistribuicaoDTO->getStrStaDistribuicao();

      if ($staAtual==$staNovo) {
        return;
      }
      
      $this->validarProximoEstado($staAtual,$staNovo,$objInfraException);
      $objDistribuicaoDTOBanco->setStrStaDistribuicaoAnterior($staAtual);
      $objDistribuicaoDTOBanco->setStrStaDistribuicao($staNovo);
      $objDistribuicaoDTOBanco->setDthSituacaoAtual(InfraData::getStrDataHoraAtual());

      //retorno para distribuido: se j� tiver sido redistribuido seta para redistribuido.
      if ($staNovo==self::$STA_DISTRIBUIDO){
        $objDistribuicaoDTO2=new DistribuicaoDTO();
        $objDistribuicaoDTO2->setDblIdProcedimento($objDistribuicaoDTOBanco->getDblIdProcedimento());
        $objDistribuicaoDTO2->setNumIdColegiadoColegiadoVersao($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
        if ($this->contar($objDistribuicaoDTO2)>1) {
          $objDistribuicaoDTOBanco->setStrStaDistribuicao(self::$STA_REDISTRIBUIDO);
          $objDistribuicaoDTOBanco->setStrStaDistribuicaoAnterior(self::$STA_REDISTRIBUIDO);
        } else {
          $objDistribuicaoDTOBanco->setStrStaDistribuicaoAnterior(self::$STA_DISTRIBUIDO);  
        }
      } else if($staNovo==DistribuicaoRN::$STA_JULGADO){
        $objDistribuicaoDTOBanco->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA_JULGADO);
      }

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      $objDistribuicaoBD->alterar($objDistribuicaoDTOBanco);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Distribui��o.',$e);
    }
  }

  protected function listarParaJulgamentoConectado(DistribuicaoDTO $objDistribuicaoDTO){
    try {
      $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();

      $objPedidoVistaRN=new PedidoVistaRN();
      $objPedidoVistaDTO=new PedidoVistaDTO();
      $objPedidoVistaDTO->adicionarCriterio(array('IdUnidade','IdUnidadeRelatorDistribuicao'),
                                            array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),
                                            array($numIdUnidadeAtual,$numIdUnidadeAtual),
                                            InfraDTO::$OPER_LOGICO_OR);
      $objPedidoVistaDTO->setDthDevolucao(null);
      $objPedidoVistaDTO->retNumIdDistribuicao();
      $objPedidoVistaDTO->retNumIdUsuario();
      $objPedidoVistaDTO->retNumIdUnidade();
      $objPedidoVistaDTO->retStrSiglaUnidade();
      $objPedidoVistaDTO->retStrDescricaoUnidade();
      $objPedidoVistaDTO->retStrNomeUsuario();
      $objPedidoVistaDTO->retDthPedido();
      $arrObjPedidoVistaDTO=$objPedidoVistaRN->listar($objPedidoVistaDTO);

      if(InfraArray::contar($arrObjPedidoVistaDTO)>0){
        $arrObjPedidoVistaDTO=InfraArray::indexarArrInfraDTO($arrObjPedidoVistaDTO,'IdDistribuicao');
        $objDistribuicaoDTO->adicionarCriterio(array('IdUnidadeRelator','IdDistribuicao','StaUltimo'),
                                               array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IN,InfraDTO::$OPER_IGUAL),
                                               array($numIdUnidadeAtual,array_keys($arrObjPedidoVistaDTO),DistribuicaoRN::$SD_ULTIMA),
                                               array(InfraDTO::$OPER_LOGICO_OR,InfraDTO::$OPER_LOGICO_AND));
      } else {
        $objDistribuicaoDTO->setNumIdUnidadeRelator($numIdUnidadeAtual);
      }

      $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
      $objDistribuicaoDTO->setStrStaDistribuicao(array(DistribuicaoRN::$STA_JULGADO,DistribuicaoRN::$STA_CANCELADO),InfraDTO::$OPER_NOT_IN);
      $objDistribuicaoDTO->retDblIdProcedimento();
      $objDistribuicaoDTO->retNumIdDistribuicao();
      $objDistribuicaoDTO->retStrNomeColegiado();
      $objDistribuicaoDTO->retStrNomeUsuarioRelator();
      $objDistribuicaoDTO->retStrStaDistribuicao();

      $arrObjDistribuicaoDTO=$this->listar($objDistribuicaoDTO);

      $objProtocoloRN=new ProtocoloRN();

      if (InfraArray::contar($arrObjDistribuicaoDTO)>0){

        //complementa informa��es
        $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
        $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_PROCEDIMENTOS);
        $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_AUTORIZADO);
        $objPesquisaProtocoloDTO->setDblIdProtocolo(InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO,'IdProcedimento'));

        $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);

        $arrObjProtocoloDTO = InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO,'IdProtocolo');
        $arrIdDistribuicao=array();
        $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
        foreach ( $arrObjDistribuicaoDTO as $chave=>$objDistribuicaoDTO2) {

          $idProcedimento=$objDistribuicaoDTO2->getDblIdProcedimento();
          if (!isset($arrObjProtocoloDTO[$idProcedimento])){
            unset($arrObjDistribuicaoDTO[$chave]);
            continue;
          }

          if(in_array($objDistribuicaoDTO2->getStrStaDistribuicao(),array(self::$STA_EM_MESA,self::$STA_PARA_REFERENDO,self::$STA_PAUTADO))){
            $arrIdDistribuicao[]=$objDistribuicaoDTO2->getNumIdDistribuicao();
          }
          $objDistribuicaoDTO2->setStrSinProntoPauta('N');
        }

        $arrIdProtocolos = array_keys($arrObjProtocoloDTO);

        if (InfraArray::contar($arrIdProtocolos)) {

          $objProcedimentoDTO = new ProcedimentoDTO();
          $objProcedimentoDTO->setDblIdProcedimento($arrIdProtocolos, InfraDTO::$OPER_IN);
          if($objProcedimentoDTO->isBolExisteAtributo('SinSinalizacoes')){
            $objProcedimentoDTO->setStrSinSinalizacoes('S');
          } else {
            $objProcedimentoDTO->setStrSinAnotacoes('S');
            $objProcedimentoDTO->setStrSinSituacoes('S');
            $objProcedimentoDTO->setStrSinMarcadores('S');
          }

          $objProcedimentoRN = new ProcedimentoRN();
          $arrObjProcedimentoDTO = InfraArray::indexarArrInfraDTO($objProcedimentoRN->listarCompleto($objProcedimentoDTO), 'IdProcedimento');

        }else{
          $arrObjProcedimentoDTO = array();
        }

        foreach($arrObjDistribuicaoDTO as $objDistribuicaoDTO2){
          $dblIdProtocolo = $objDistribuicaoDTO2->getDblIdProcedimento();
          $arrObjProcedimentoDTO[$dblIdProtocolo]->setArrObjRetornoProgramadoDTO(null);
          $objProcedimentoDTO=$arrObjProcedimentoDTO[$dblIdProtocolo];
          $objProcedimentoDTO->setStrSinAberto($arrObjProtocoloDTO[$dblIdProtocolo]->getStrSinAberto());
          $objProcedimentoDTO->setStrDescricaoProtocolo($arrObjProtocoloDTO[$dblIdProtocolo]->getStrDescricao());
          $objDistribuicaoDTO2->setObjProcedimentoDTO($objProcedimentoDTO);
        }

        //busca data das sess�es
        if (InfraArray::contar($arrIdDistribuicao)>0){
          $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
          $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($arrIdDistribuicao,InfraDTO::$OPER_IN);
          $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_PREVISTA,SessaoJulgamentoRN::$ES_FINALIZADA,SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_ENCERRADA),InfraDTO::$OPER_NOT_IN);
          $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
          $objItemSessaoJulgamentoDTO->retDthSessaoSessaoJulgamento();
          $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);
          $arrDthSessao=InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'SessaoSessaoJulgamento','IdDistribuicao');
        }
        foreach ($arrObjDistribuicaoDTO as $chave=>$objDistribuicaoDTO2) {
          $idDistribuicao=$objDistribuicaoDTO2->getNumIdDistribuicao();
          if (isset($arrDthSessao[$idDistribuicao])) {
            $objDistribuicaoDTO2->setDthSessaoJulgamento($arrDthSessao[$idDistribuicao]);
          } else {
            $objDistribuicaoDTO2->setDthSessaoJulgamento(null);
          }
          if(isset($arrObjPedidoVistaDTO[$idDistribuicao])){
            $objDistribuicaoDTO2->setObjPedidoVistaDTO($arrObjPedidoVistaDTO[$idDistribuicao]);
          }
        }
      } else {
        return array();
      }
      return array_values($arrObjDistribuicaoDTO);
    } catch(Exception $e){
      throw new InfraException('Erro listando processos em julgamento.',$e);
    }
  }

  protected function listarPainelConectado(PainelDistribuicaoDTO $objPainelDistribuicaoDTO) {
    try {
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_listar',__METHOD__,$objPainelDistribuicaoDTO);

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());

      $idUnidade=null;
      if($objPainelDistribuicaoDTO->isSetNumIdUnidadeRelator()){
        $idUnidade=$objPainelDistribuicaoDTO->getNumIdUnidadeRelator();
      }
      $bolGlobal=SessaoSEI::getInstance()->verificarPermissao('painel_distribuicao_global');
      $arrIdDistribuicaoPesquisa=array();


      if (!$bolGlobal && (!$objPainelDistribuicaoDTO->isSetStrStaDistribuicao()||in_array(DistribuicaoRN::$STA_PEDIDO_VISTA,$objPainelDistribuicaoDTO->getStrStaDistribuicao()) )) {
        //consultar pedidos de vista
        $objPedidoVistaDTO = new PedidoVistaDTO();
        $objPedidoVistaRN = new PedidoVistaRN();
        if ($idUnidade) {
          $objPedidoVistaDTO->setNumIdUnidade($idUnidade);
        }
        if ($objPainelDistribuicaoDTO->isSetDthSituacaoAtual()){
          $objPedidoVistaDTO->setDthPedido($objPainelDistribuicaoDTO->getDthSituacaoAtual(),InfraDTO::$OPER_MENOR_IGUAL);
        }
        $objPedidoVistaDTO->setDthDevolucao(null);
        $objPedidoVistaDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_PEDIDO_VISTA);
        $objPedidoVistaDTO->setStrStaUltimoDistribuicao(DistribuicaoRN::$SD_ULTIMA);
        $objPedidoVistaDTO->retNumIdDistribuicao();
        if ($objPainelDistribuicaoDTO->isSetNumIdColegiadoColegiadoVersao()) {
          $objPedidoVistaDTO->setNumIdColegiadoColegiadoVersao($objPainelDistribuicaoDTO->getNumIdColegiadoColegiadoVersao());
        }
        $arrObjPedidoVistaDTO = $objPedidoVistaRN->listar($objPedidoVistaDTO);
        if (InfraArray::contar($arrObjPedidoVistaDTO)) {
          $arrIdDistribuicaoPesquisa=InfraArray::converterArrInfraDTO($arrObjPedidoVistaDTO, 'IdDistribuicao');
        }
      }
      if (!$bolGlobal && (!$objPainelDistribuicaoDTO->isSetStrStaDistribuicao()||in_array(DistribuicaoRN::$STA_EM_MESA,$objPainelDistribuicaoDTO->getStrStaDistribuicao()))) {
        //consultar itens de mesa pelo usu�rio, mas de outro relator
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdUnidadeRelatorDistribuicao($objItemSessaoJulgamentoDTO->getObjInfraAtributoDTO('IdUnidadeSessao'),InfraDTO::$OPER_DIFERENTE);
        if ($idUnidade) {
          $objItemSessaoJulgamentoDTO->setNumIdUnidadeSessao($idUnidade);
        }
        $objItemSessaoJulgamentoDTO->setStrStaTipoItemSessaoBloco(TipoSessaoBlocoRN::$STA_MESA);
        $objItemSessaoJulgamentoDTO->setStrStaUltimoDistribuicao(DistribuicaoRN::$SD_ULTIMA);
        $objItemSessaoJulgamentoDTO->setStrStaDistribuicao(self::$STA_EM_MESA);
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(array(ItemSessaoJulgamentoRN::$STA_NORMAL,ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO,ItemSessaoJulgamentoRN::$STA_ADIADO,ItemSessaoJulgamentoRN::$STA_DILIGENCIA,ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA),InfraDTO::$OPER_IN);
        $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
        $objItemSessaoJulgamentoDTO->retNumIdUsuarioSessao();
        $objItemSessaoJulgamentoDTO->retNumIdUnidadeSessao();
        $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
        $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);
        if (InfraArray::contar($arrObjItemSessaoJulgamentoDTO)) {
          $arrIdDistribuicaoPesquisa=array_merge($arrIdDistribuicaoPesquisa,InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdDistribuicao'));
        }
      }
      if (InfraArray::contar($arrIdDistribuicaoPesquisa) && ($objPainelDistribuicaoDTO->isSetNumIdTipoProcedimento() || $objPainelDistribuicaoDTO->isSetNumIdContatoParticipante())){
        $objPainelDistribuicaoDTO2=new PainelDistribuicaoDTO();
        $objPainelDistribuicaoDTO2->setNumIdDistribuicao($arrIdDistribuicaoPesquisa,InfraDTO::$OPER_IN);
        $objPainelDistribuicaoDTO2->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
        $objPainelDistribuicaoDTO2->retNumIdDistribuicao();
        if($objPainelDistribuicaoDTO->isSetNumIdTipoProcedimento()){
          $objPainelDistribuicaoDTO2->setNumIdTipoProcedimento($objPainelDistribuicaoDTO->getNumIdTipoProcedimento());
        }
        if($objPainelDistribuicaoDTO->isSetNumIdContatoParticipante()){
          $objPainelDistribuicaoDTO2->setNumIdContatoParticipante($objPainelDistribuicaoDTO->getNumIdContatoParticipante());
        }
        $arrObjPainelDistribuicaoDTO=$objDistribuicaoBD->listar($objPainelDistribuicaoDTO2);
        if($arrObjPainelDistribuicaoDTO){
          $arrIdDistribuicaoPesquisa=InfraArray::converterArrInfraDTO($arrObjPainelDistribuicaoDTO, 'IdDistribuicao');
        } else {
          $arrIdDistribuicaoPesquisa=null;
        }
      }

      if (InfraArray::contar($arrIdDistribuicaoPesquisa)){
        $arrAtributos=array();
        $arrOperadores=array();
        $arrValores=array();

        if($objPainelDistribuicaoDTO->isSetNumIdUnidadeRelator()){
          $arrAtributos[]='IdUnidadeRelator';
          $arrOperadores[]=InfraDTO::$OPER_IGUAL;
          $arrValores[]=$objPainelDistribuicaoDTO->getNumIdUnidadeRelator();
        }
        if($objPainelDistribuicaoDTO->isSetDthSituacaoAtual()){
          $arrAtributos[]='SituacaoAtual';
          $arrOperadores[]=InfraDTO::$OPER_MAIOR_IGUAL;
          $arrValores[]=$objPainelDistribuicaoDTO->getDthSituacaoAtual();
        }
        if($objPainelDistribuicaoDTO->isSetStrStaUltimo()){
          $arrAtributos[]='StaUltimo';
          $valor=$objPainelDistribuicaoDTO->getStrStaUltimo();
          if (is_array($valor)){
            $arrOperadores[]=InfraDTO::$OPER_IN;
          } else {
            $arrOperadores[]=InfraDTO::$OPER_IGUAL;
          }
          $arrValores[]=$valor;
        }
        if($objPainelDistribuicaoDTO->isSetNumIdColegiadoColegiadoVersao()){
          $arrAtributos[]='IdColegiadoColegiadoVersao';
          $arrOperadores[]=InfraDTO::$OPER_IGUAL;
          $arrValores[]=$objPainelDistribuicaoDTO->getNumIdColegiadoColegiadoVersao();
        }
        if($objPainelDistribuicaoDTO->isSetStrStaDistribuicao()){
          $arrAtributos[]='StaDistribuicao';
          $arrOperadores[]=InfraDTO::$OPER_IN;
          $arrValores[]=$objPainelDistribuicaoDTO->getStrStaDistribuicao();
        }
        if($objPainelDistribuicaoDTO->isSetNumIdTipoProcedimento()){
          $arrAtributos[]='IdTipoProcedimento';
          $arrOperadores[]=InfraDTO::$OPER_IGUAL;
          $arrValores[]=$objPainelDistribuicaoDTO->getNumIdTipoProcedimento();
        }

        $numAtributos=InfraArray::contar($arrAtributos);
        if($numAtributos>0){
          $objPainelDistribuicaoDTO->unSetTodos();
          if($numAtributos==1){
            $arrAtributos[]='IdDistribuicao';
            $arrOperadores[]=InfraDTO::$OPER_IN;
            $arrValores[]=$arrIdDistribuicaoPesquisa;
            $objPainelDistribuicaoDTO->adicionarCriterio($arrAtributos, $arrOperadores, $arrValores,InfraDTO::$OPER_LOGICO_OR);
          } else {
            $arrOperadoresLogicos=array();
            for($i=1;$i<$numAtributos;$i++){
              $arrOperadoresLogicos[]=InfraDTO::$OPER_LOGICO_AND;
            }
            $objPainelDistribuicaoDTO->adicionarCriterio($arrAtributos, $arrOperadores, $arrValores,$arrOperadoresLogicos,'original');
            $objPainelDistribuicaoDTO->adicionarCriterio(array('IdDistribuicao'),array(InfraDTO::$OPER_IN),array($arrIdDistribuicaoPesquisa),null,'adicional');
            $objPainelDistribuicaoDTO->agruparCriterios(array('original', 'adicional'), InfraDTO::$OPER_LOGICO_OR);
          }
        }
      }

      if($objPainelDistribuicaoDTO->isSetStrPalavrasPesquisa()){
        $objPainelDistribuicaoDTO=InfraString::prepararPesquisaDTO($objPainelDistribuicaoDTO, 'PalavrasPesquisa', 'IdxAutuacao');
      }
      $objPainelDistribuicaoDTO->retNumIdDistribuicao();
      $objPainelDistribuicaoDTO->retNumIdUnidadeRelator();
      $objPainelDistribuicaoDTO->retNumIdUsuarioRelator();
      $objPainelDistribuicaoDTO->retStrNomeTipoProcedimento();
      $objPainelDistribuicaoDTO->retDblIdProcedimento();

      $arrObjPainelDistribuicaoDTO = $objDistribuicaoBD->listar($objPainelDistribuicaoDTO);

      if(InfraArray::contar($arrObjPainelDistribuicaoDTO)) {
        $arrObjPainelDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjPainelDistribuicaoDTO, 'IdDistribuicao');

        $objPedidoVistaDTO = new PedidoVistaDTO();
        $objPedidoVistaRN = new PedidoVistaRN();
        $objPedidoVistaDTO->retStrSiglaUnidade();
        $objPedidoVistaDTO->retNumIdUnidade();
        $objPedidoVistaDTO->retNumIdUsuario();
        $objPedidoVistaDTO->retStrDescricaoUnidade();
        $objPedidoVistaDTO->retStrNomeUsuario();
        $objPedidoVistaDTO->setDthDevolucao(null);
        $objPedidoVistaDTO->setNumIdDistribuicao(array_keys($arrObjPainelDistribuicaoDTO), InfraDTO::$OPER_IN);
        $objPedidoVistaDTO->retNumIdDistribuicao();
        $objPedidoVistaDTO->retNumIdPedidoVista();
        $arrObjPedidoVistaDTO = $objPedidoVistaRN->listar($objPedidoVistaDTO);
        foreach ($arrObjPedidoVistaDTO as $objPedidoVistaDTO) {
          $arrObjPainelDistribuicaoDTO[$objPedidoVistaDTO->getNumIdDistribuicao()]->setObjPedidoVistaDTO($objPedidoVistaDTO);
        }
        //buscar partes_procedimento e interessados do procedimento
        $arrIdProcedimento=InfraArray::converterArrInfraDTO($arrObjPainelDistribuicaoDTO,'IdProcedimento');
        $objParticipanteDTO=new ParticipanteDTO();
        $objParticipanteDTO->setDblIdProtocolo($arrIdProcedimento,InfraDTO::$OPER_IN);
        $objParticipanteDTO->retDblIdProtocolo();
        $objParticipanteDTO->retNumIdContato();
        $objParticipanteDTO->retStrNomeContato();
        $objParticipanteDTO->setStrStaParticipacao(ParticipanteRN::$TP_INTERESSADO);
        $objParticipanteRN=new ParticipanteRN();
        $arrObjParticipanteDTO=$objParticipanteRN->listarRN0189($objParticipanteDTO);
        $arrObjParticipanteDTO=InfraArray::indexarArrInfraDTO($arrObjParticipanteDTO,'IdProtocolo',true);

        $objParteProcedimentoDTO=new ParteProcedimentoDTO();
        $objParteProcedimentoRN=new ParteProcedimentoRN();
        $objParteProcedimentoDTO->setDblIdProcedimento($arrIdProcedimento,InfraDTO::$OPER_IN);
        $objParteProcedimentoDTO->retDblIdProcedimento();
        $objParteProcedimentoDTO->retNumIdContato();
        $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
        $objParteProcedimentoDTO->retStrNomeContato();
        $arrObjParteProcedimentoDTO=$objParteProcedimentoRN->listar($objParteProcedimentoDTO);
        $arrObjParteProcedimentoDTO=InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO,'IdProcedimento',true);

        foreach ($arrIdProcedimento as $idProcedimento) {
          if(isset($arrObjParticipanteDTO[$idProcedimento])){
            if(!isset($arrObjParteProcedimentoDTO[$idProcedimento])){
              $arrObjParteProcedimentoDTO[$idProcedimento]=array();
            } else {
              $arrObjParteProcedimentoDTO[$idProcedimento]=InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO[$idProcedimento],'IdContato');
            }
            foreach ($arrObjParticipanteDTO[$idProcedimento] as $objParticipanteDTO) {
              $idContato=$objParticipanteDTO->getNumIdContato();
              if(!isset($arrObjParteProcedimentoDTO[$idProcedimento][$idContato])){
                $objParteProcedimentoDTO=new ParteProcedimentoDTO();
                $objParteProcedimentoDTO->setDblIdProcedimento($idProcedimento);
                $objParteProcedimentoDTO->setNumIdContato($idContato);
                $objParteProcedimentoDTO->setStrNomeContato($objParticipanteDTO->getStrNomeContato());
                $objParteProcedimentoDTO->setStrDescricaoQualificacaoParte('Interessado');
                $arrObjParteProcedimentoDTO[$idProcedimento][$idContato]=$objParteProcedimentoDTO;
              }
            }
          }
        }
        foreach ($arrObjPainelDistribuicaoDTO as $objPainelDistribuicaoDTO2) {
          $idProcedimento=$objPainelDistribuicaoDTO2->getDblIdProcedimento();
          if(isset($arrObjParteProcedimentoDTO[$idProcedimento])){
            $objPainelDistribuicaoDTO2->setArrObjParteProcedimentoDTO(array_values($arrObjParteProcedimentoDTO[$idProcedimento]));
          } else {
            $objPainelDistribuicaoDTO2->setArrObjParteProcedimentoDTO(null);
          }
        }
      }
      //Auditoria

      return array_values($arrObjPainelDistribuicaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro listando Distribui��es para o painel.',$e);
    }
  }
  protected function bloquearControlado(DistribuicaoDTO $objDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('distribuicao_consultar',__METHOD__,$objDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDistribuicaoBD = new DistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objDistribuicaoBD->bloquear($objDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Distribuicao.',$e);
    }
  }
}
?>