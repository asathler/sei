<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 20/12/2019 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class RevisaoItemDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'revisao_item';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdRevisaoItem', 'id_revisao_item');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdItemSessaoJulgamento', 'id_item_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUnidade', 'id_unidade');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUsuario', 'id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'SinRevisado', 'sin_revisado');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'Revisao', 'dth_revisao');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUsuario','sigla','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamentoItem','id_sessao_julgamento','item_sessao_julgamento');


    $this->configurarPK('IdRevisaoItem',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdUnidade','unidade','id_unidade');
    $this->configurarFK('IdUsuario','usuario','id_usuario');
    $this->configurarFK('IdItemSessaoJulgamento','item_sessao_julgamento','id_item_sessao_julgamento');


  }
}
?>