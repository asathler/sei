<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 07/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MotivoMesaRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrDescricao(MotivoMesaDTO $objMotivoMesaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoMesaDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('Descri��o n�o informada.');
    }else{
      $objMotivoMesaDTO->setStrDescricao(trim($objMotivoMesaDTO->getStrDescricao()));

      if (strlen($objMotivoMesaDTO->getStrDescricao())>250){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 250 caracteres.');
      }


      $dto = new MotivoMesaDTO();
      $dto->retStrSinAtivo();
      $dto->setNumIdMotivoMesa($objMotivoMesaDTO->getNumIdMotivoMesa(),InfraDTO::$OPER_DIFERENTE);
      $dto->setStrDescricao($objMotivoMesaDTO->getStrDescricao());
      $dto->setBolExclusaoLogica(false);

      $dto = $this->consultar($dto);
      if ($dto != NULL){
        if ($dto->getStrSinAtivo() == 'S')
          $objInfraException->adicionarValidacao('Existe outra ocorr�ncia cadastrada com a mesma Descri��o.');
        else
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa cadastrada com a mesma Descri��o.');
      }
    }
  }

  private function validarNumOrdem(MotivoMesaDTO $objMotivoMesaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoMesaDTO->getNumOrdem())){
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }

  private function validarStrSinAtivo(MotivoMesaDTO $objMotivoMesaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoMesaDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objMotivoMesaDTO->getStrSinAtivo())){
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }

  protected function cadastrarControlado(MotivoMesaDTO $objMotivoMesaDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_cadastrar',__METHOD__,$objMotivoMesaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objMotivoMesaDTO, $objInfraException);
      $this->validarNumOrdem($objMotivoMesaDTO, $objInfraException);
      $this->validarStrSinAtivo($objMotivoMesaDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      $ret = $objMotivoMesaBD->cadastrar($objMotivoMesaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Motivo de Mesa.',$e);
    }
  }

  protected function alterarControlado(MotivoMesaDTO $objMotivoMesaDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_alterar',__METHOD__,$objMotivoMesaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objMotivoMesaDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objMotivoMesaDTO, $objInfraException);
      }
      if ($objMotivoMesaDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objMotivoMesaDTO, $objInfraException);
      }
      if ($objMotivoMesaDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objMotivoMesaDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      $objMotivoMesaBD->alterar($objMotivoMesaDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Motivo de Mesa.',$e);
    }
  }

  protected function excluirControlado($arrObjMotivoMesaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_excluir',__METHOD__,$arrObjMotivoMesaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();
      $arrId=InfraArray::converterArrInfraDTO($arrObjMotivoMesaDTO,'IdMotivoMesa');
      if(InfraArray::contar($arrId)>0){
        $objMotivoMesaDTO=new MotivoMesaDTO();
        $objMotivoMesaDTO->setNumIdMotivoMesa($arrId,InfraDTO::$OPER_IN);
        $objMotivoMesaDTO->retNumIdMotivoMesa();
        $objMotivoMesaDTO->setBolExclusaoLogica(false);
        $arrObjMotivoMesaDTO=$this->listar($objMotivoMesaDTO);
      }
      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

      for($i=0;$i<InfraArray::contar($arrObjMotivoMesaDTO);$i++){

        $objItemSessaoJulgamentoDTO->setNumIdMotivoMesa($arrObjMotivoMesaDTO[$i]->getNumIdMotivoMesa());
        if ($objItemSessaoJulgamentoRN->contar($objItemSessaoJulgamentoDTO)>0){
          $objInfraException->lancarValidacao('Motivo de mesa j� foi utilizado.');
        }
        $objMotivoMesaBD->excluir($arrObjMotivoMesaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Motivo de Mesa.',$e);
    }
  }

  protected function consultarConectado(MotivoMesaDTO $objMotivoMesaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_consultar',__METHOD__,$objMotivoMesaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      $ret = $objMotivoMesaBD->consultar($objMotivoMesaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Motivo de Mesa.',$e);
    }
  }

  protected function listarConectado(MotivoMesaDTO $objMotivoMesaDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_listar',__METHOD__,$objMotivoMesaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      $ret = $objMotivoMesaBD->listar($objMotivoMesaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Motivos de Mesa.',$e);
    }
  }

  protected function contarConectado(MotivoMesaDTO $objMotivoMesaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_listar',__METHOD__,$objMotivoMesaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      $ret = $objMotivoMesaBD->contar($objMotivoMesaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Motivos de Mesa.',$e);
    }
  }

  protected function desativarControlado($arrObjMotivoMesaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_desativar',__METHOD__,$arrObjMotivoMesaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjMotivoMesaDTO);$i++){
        $objMotivoMesaBD->desativar($arrObjMotivoMesaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Motivo de Mesa.',$e);
    }
  }

  protected function reativarControlado($arrObjMotivoMesaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_reativar',__METHOD__,$arrObjMotivoMesaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjMotivoMesaDTO);$i++){
        $objMotivoMesaBD->reativar($arrObjMotivoMesaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Motivo de Mesa.',$e);
    }
  }

  protected function bloquearControlado(MotivoMesaDTO $objMotivoMesaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_mesa_consultar',__METHOD__,$objMotivoMesaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoMesaBD = new MotivoMesaBD($this->getObjInfraIBanco());
      $ret = $objMotivoMesaBD->bloquear($objMotivoMesaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Motivo de Mesa.',$e);
    }
  }


}
?>