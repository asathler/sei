<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);


  $strTitulo = 'Revis�o de Item de Sess�o de Julgamento';

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_distribuicao','id_sessao_julgamento','id_procedimento_sessao','arvore','id_colegiado'));

  $strParametros='';

  switch($_GET['acao']){
    case 'revisao_item_cadastrar':
      $idItemSessao = $_GET['id_item_sessao_julgamento'];
      $sinRevisado=$_GET['sin_revisado'];
      if ($idItemSessao == '') {
        throw new InfraException('Item da Sess�o de Julgamento n�o informada.');
      }
      if ($sinRevisado == '') {
        throw new InfraException('N�o foi informado se j� foi revisado.');
      }

      $objRevisaoItemDTO = new RevisaoItemDTO();
      $objRevisaoItemDTO->setNumIdItemSessaoJulgamento($idItemSessao);
      $objRevisaoItemDTO->setStrSinRevisado($sinRevisado);



      try {
        $objRevisaoItemRN = new RevisaoItemRN();
        $objRevisaoItemRN->cadastrar($objRevisaoItemDTO);

        PaginaSEI::getInstance()->adicionarMensagem('Sinalizador de revis�o atualizado.');
        //header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem=sessao_julgamento_listar&id_sessao_julgamento='.$_GET['id_sessao_julgamento']));
        //die;
      } catch (Exception $e) {
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      //break;

      header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao_origem'] . '&acao_origem=' . $_GET['acao'] . '&id_sessao_julgamento=' . $_GET['id_sessao_julgamento'].PaginaSEI::getInstance()->montarAncora($idItemSessao)));
      die;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }




}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>


  function inicializar(){

  infraEfeitoTabelas();
}

function onSubmitForm() {


}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmSessaoJulgamentoLista" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
  <?
//  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);


  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>