<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 25/01/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__.'/../../../../SEI.php';

class ItemSessaoDocumentoINT extends InfraINT {

  public static function montarSelectIdDocumento($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdItemSessaoJulgamento='', $numIdUsuario='', $dblIdDocumento=''){
    $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
    $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
    $objItemSessaoDocumentoDTO->retDblIdDocumento();

    if ($numIdItemSessaoJulgamento!==''){
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    }

    if ($numIdUsuario!==''){
      $objItemSessaoDocumentoDTO->setNumIdUsuario($numIdUsuario);
    }

    if ($dblIdDocumento!==''){
      $objItemSessaoDocumentoDTO->setDblIdDocumento($dblIdDocumento);
    }

    $objItemSessaoDocumentoDTO->setOrdDblIdDocumento(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjItemSessaoDocumentoDTO, 'IdItemSessaoDocumento', 'IdDocumento');
  }
}
?>