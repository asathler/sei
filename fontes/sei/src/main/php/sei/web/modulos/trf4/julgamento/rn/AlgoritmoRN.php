<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 10/07/2015 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.35.0
 *
 * Vers�o no CVS: $Id$
 */

require_once __DIR__.'/../../../../SEI.php';

class AlgoritmoRN extends InfraRN {

  public static $ALG_RODADA = 'R';
  public static $ALG_RODADA_TIPO = 'T';
  public static $ALG_RODADA_MATERIA = 'M';
  public static $ALG_PESO = 'P';

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco():BancoSEI {
    return BancoSEI::getInstance();
  }

  public function listarValoresAlgoritmo(): array
  {
    try {

      $objArrTipoDTO = array();

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$ALG_PESO);
      $objTipoDTO->setStrDescricao('Peso atribu�do ao membro');
      $objArrTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$ALG_RODADA);
      $objTipoDTO->setStrDescricao('Distribui��o por rodadas');
      $objArrTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$ALG_RODADA_TIPO);
      $objTipoDTO->setStrDescricao('Distribui��o por rodadas por tipo de processo');
      $objArrTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$ALG_RODADA_MATERIA);
      $objTipoDTO->setStrDescricao('Distribui��o por rodadas por tipo de mat�ria');
      $objArrTipoDTO[] = $objTipoDTO;

      return $objArrTipoDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de Algoritmo de Distribuicao.',$e);
    }
  }

  private function validarNumContador(AlgoritmoDTO $objAlgoritmoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAlgoritmoDTO->getNumContador())){
      $objInfraException->adicionarValidacao('Contador n�o informado.');
    }
  }

  private function validarStrStaAlgoritmo(AlgoritmoDTO $objAlgoritmoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAlgoritmoDTO->getStrStaAlgoritmo())){
      $objInfraException->adicionarValidacao('Algoritmo n�o informado.');
    }else if (!in_array($objAlgoritmoDTO->getStrStaAlgoritmo(),InfraArray::converterArrInfraDTO($this->listarValoresAlgoritmo(),'StaTipo'),false)){
      $objInfraException->adicionarValidacao('Algoritmo inv�lido.');
    }
  }

  private function validarNumIdUsuario(AlgoritmoDTO $objAlgoritmoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAlgoritmoDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
    }
  }

  private function validarNumIdOrigem(AlgoritmoDTO $objAlgoritmoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAlgoritmoDTO->getNumIdOrigem())){
      $objAlgoritmoDTO->setNumIdOrigem(null);
    }
  }

  private function validarNumIdColegiado(AlgoritmoDTO $objAlgoritmoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAlgoritmoDTO->getNumIdColegiado())){
      $objInfraException->adicionarValidacao('Colegiado n�o informado.');
    }
  }

  protected function cadastrarControlado(AlgoritmoDTO $objAlgoritmoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('algoritmo_cadastrar',__METHOD__,$objAlgoritmoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumContador($objAlgoritmoDTO, $objInfraException);
      $this->validarStrStaAlgoritmo($objAlgoritmoDTO, $objInfraException);
      $this->validarNumIdUsuario($objAlgoritmoDTO, $objInfraException);
      $this->validarNumIdOrigem($objAlgoritmoDTO, $objInfraException);
      $this->validarNumIdColegiado($objAlgoritmoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objClone=clone($objAlgoritmoDTO);
      $objClone->unSetNumContador();
      if($this->contar($objClone)!=0){
        throw new InfraException('J� existe registro de algoritmo cadastrado com esses par�metros.');
      }

      $objAlgoritmoBD = new AlgoritmoBD($this->getObjInfraIBanco());
      $ret = $objAlgoritmoBD->cadastrar($objAlgoritmoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Algoritmo.',$e);
    }
  }

  protected function alterarControlado(AlgoritmoDTO $objAlgoritmoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('algoritmo_alterar',__METHOD__,$objAlgoritmoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objAlgoritmoDTO->isSetNumContador()){
        $this->validarNumContador($objAlgoritmoDTO, $objInfraException);
      }
      if ($objAlgoritmoDTO->isSetStrStaAlgoritmo()){
        $this->validarStrStaAlgoritmo($objAlgoritmoDTO, $objInfraException);
      }
      if ($objAlgoritmoDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objAlgoritmoDTO, $objInfraException);
      }
      if ($objAlgoritmoDTO->isSetNumIdOrigem()){
        $this->validarNumIdOrigem($objAlgoritmoDTO, $objInfraException);
      }
      if ($objAlgoritmoDTO->isSetNumIdColegiado()){
        $this->validarNumIdColegiado($objAlgoritmoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objAlgoritmoBD = new AlgoritmoBD($this->getObjInfraIBanco());
      $objAlgoritmoBD->alterar($objAlgoritmoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Algoritmo.',$e);
    }
  }

  protected function excluirControlado($arrObjAlgoritmoDTO){
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('algoritmo_excluir',__METHOD__,$arrObjAlgoritmoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAlgoritmoBD = new AlgoritmoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjAlgoritmoDTO); $i< $iMax; $i++){
        $objAlgoritmoBD->excluir($arrObjAlgoritmoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Algoritmo.',$e);
    }
  }

  protected function consultarConectado(AlgoritmoDTO $objAlgoritmoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('algoritmo_consultar',__METHOD__,$objAlgoritmoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAlgoritmoBD = new AlgoritmoBD($this->getObjInfraIBanco());
      $ret = $objAlgoritmoBD->consultar($objAlgoritmoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Algoritmo.',$e);
    }
  }

  protected function listarConectado(AlgoritmoDTO $objAlgoritmoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('algoritmo_listar',__METHOD__,$objAlgoritmoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAlgoritmoBD = new AlgoritmoBD($this->getObjInfraIBanco());
      $ret = $objAlgoritmoBD->listar($objAlgoritmoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Algoritmos.',$e);
    }
  }

  protected function contarConectado(AlgoritmoDTO $objAlgoritmoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('algoritmo_listar',__METHOD__,$objAlgoritmoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAlgoritmoBD = new AlgoritmoBD($this->getObjInfraIBanco());
      $ret = $objAlgoritmoBD->contar($objAlgoritmoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Algoritmos.',$e);
    }
  }

  /**
   * @param DistribuicaoDTO[] $arrObjDistribuicaoDTO
   * @return DistribuicaoDTO[]
   * @throws InfraException
   */
  protected function distribuirControlado(array $arrObjDistribuicaoDTO):array
  {
    if (InfraArray::contar($arrObjDistribuicaoDTO) == 0) {
      return [];
    }
    return $this->distribuirInterno($arrObjDistribuicaoDTO, false, 1);
  }

  protected function simularDistribuicaoConectado($arrObjDistribuicaoDTO)
  {
    if (InfraArray::contar($arrObjDistribuicaoDTO) === 0) {
      return null;
    }
    $qtd=0;
    if ($arrObjDistribuicaoDTO[0]->isSetNumQuantidade()){
      $qtd=$arrObjDistribuicaoDTO[0]->getNumQuantidade();
    }
    return $this->distribuirInterno($arrObjDistribuicaoDTO,true,$qtd);
  }

  /**
   * @param DistribuicaoDTO[] $arrObjDistribuicaoDTO
   * @param bool $bolSimulacao
   * @param int $numRepeticoes
   * @return array
   * @throws InfraException
   */
  private function distribuirInterno(array $arrObjDistribuicaoDTO, bool $bolSimulacao, int $numRepeticoes):array
  {
    if (InfraArray::contar($arrObjDistribuicaoDTO) === 0) {
      return [];
    }
    $arrIdColegiados = array_keys(InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO, 'IdColegiadoColegiadoVersao', 'IdColegiadoColegiadoVersao'));
    if (InfraArray::contar($arrIdColegiados) > 1) {
      throw new InfraException('N�o � poss�vel simular distribui��o para mais de um Colegiado simultaneamente.');
    }
    $numIdColegiado=$arrIdColegiados[0];
    $objInfraException = new InfraException();



    $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();

    $arrPeso=null;
    $arrProcessamento=null;
    $arrIdOrigem=null;

    if ($bolSimulacao) {
      $arrStaAlgoritmo = array_values(InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO, 'StaAlgoritmoDistribuicao','StaAlgoritmoDistribuicao'));
      if (InfraArray::contar($arrStaAlgoritmo) > 1) {
        throw new InfraException('N�o � poss�vel simular distribui��o para mais de um Algoritmo simultaneamente.');
      }
      $staAlgoritmo = $arrStaAlgoritmo[0];

      if ($staAlgoritmo === self::$ALG_PESO) {
        $objColegiadoComposicaoDTO->setDblPeso(0, InfraDTO::$OPER_MAIOR);
      }
    } else {
      $numRepeticoes = 1;
      $objColegiadoDTO = new ColegiadoDTO();
      $objColegiadoRN = new ColegiadoRN();
      $objColegiadoDTO->setNumIdColegiado($numIdColegiado);
      $objColegiadoDTO->retStrStaAlgoritmoDistribuicao();
      $objColegiadoDTO->retNumIdColegiado();
      $objColegiadoDTO = $objColegiadoRN->consultar($objColegiadoDTO);

      if ($objColegiadoDTO===null) {
        throw new InfraException('Colegiado n�o encontrado.');
      }
      $staAlgoritmo = $objColegiadoDTO->getStrStaAlgoritmoDistribuicao();
    }

    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
    $objColegiadoComposicaoDTO->setNumIdTipoMembroColegiado(TipoMembroColegiadoRN::$TMC_TITULAR);
    $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
    $objColegiadoComposicaoDTO->retNumIdColegiadoComposicao();
    $objColegiadoComposicaoDTO->retNumIdColegiadoVersao();
    $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
    $objColegiadoComposicaoDTO->retNumIdUsuario();
    $objColegiadoComposicaoDTO->retNumIdUnidade();
    $objColegiadoComposicaoDTO->retDblPeso();
    $arrObjColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
    shuffle($arrObjColegiadoComposicaoDTO);
    $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'IdUsuario');

    $arrResultado = array();
    $arrObjAlgoritmoDTO = null;


    //prepara��o do algoritmo
    $objAlgoritmoDTO = new AlgoritmoDTO();
    $objAlgoritmoDTO->setNumIdColegiado($numIdColegiado);
    $objAlgoritmoDTO->setStrStaAlgoritmo($staAlgoritmo);
    $objAlgoritmoDTO->retNumIdAlgoritmo();
    $objAlgoritmoDTO->retNumIdColegiado();
    $objAlgoritmoDTO->retNumIdUsuario();
    $objAlgoritmoDTO->retNumIdOrigem();
    $objAlgoritmoDTO->retNumContador();
    $objAlgoritmoDTO->setOrdNumIdAlgoritmo(InfraDTO::$TIPO_ORDENACAO_DESC);



    switch ($staAlgoritmo) {
      case self::$ALG_PESO:
        $arrPeso = InfraArray::converterArrInfraDTO($arrObjColegiadoComposicaoDTO, 'Peso', 'IdUsuario');
        foreach ($arrPeso as $idUsuario => $peso) {
          $arrPeso[$idUsuario] = (double)InfraUtil::prepararDbl($peso);
          if ($arrPeso[$idUsuario]===0.0) {
            unset($arrPeso[$idUsuario]);
          }
        }
        if ($bolSimulacao) {
          foreach ($arrPeso as $idUsuario => $peso) {
            $arrResultado[$idUsuario] = 0;
          }
        }

        break;
      case self::$ALG_RODADA:
        $arrObjAlgoritmoDTO = $this->listar($objAlgoritmoDTO);
        $ret=array();
        foreach ($arrObjAlgoritmoDTO as $objAlgoritmoDTO) {
          $idUsuario=$objAlgoritmoDTO->getNumIdUsuario();
          if(!isset($ret[$idUsuario])){
            $ret[$idUsuario]=$objAlgoritmoDTO;
          }else {
            $this->excluir(array($objAlgoritmoDTO));
          }
        }
        $arrObjAlgoritmoDTO=$ret;


        $arrProcessamento = array();

        foreach ($arrObjColegiadoComposicaoDTO as $idUsuario => $objColegiadoComposicaoDTO) {
          $dblPeso=(double)InfraUtil::prepararDbl($objColegiadoComposicaoDTO->getDblPeso());
          if ($dblPeso===0.0) {
            continue;
          }
          if($dblPeso!==1.0) {
            $objInfraException->lancarValidacao('Peso inv�lido processando algoritmo de distribui��o. Deve ser 0 ou 1.');
          }
          $arrResultado[$idUsuario] = 0;
          if (isset($arrObjAlgoritmoDTO[$idUsuario])) {
            $objAlgoritmoDTO = $arrObjAlgoritmoDTO[$idUsuario];
            $arrProcessamento[$idUsuario] = $objAlgoritmoDTO->getNumContador();
          } else {
            $arrProcessamento[$idUsuario] = 0;
          }
        }


        break;
      case self::$ALG_RODADA_TIPO:
        $arrProcessamento = array();
        $arrIdOrigem = InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO, 'IdTipoProcedimento', 'IdTipoProcedimento');
        $objAlgoritmoDTO->setNumIdOrigem($arrIdOrigem, InfraDTO::$OPER_IN);
        $arrObjAlgoritmoDTO = $this->listar($objAlgoritmoDTO);

        $arrObjAlgoritmoDTO = InfraArray::indexarArrInfraDTO($arrObjAlgoritmoDTO, 'IdOrigem', true);
        foreach ($arrIdOrigem as $idOrigem) {
          $arrResultado[$idOrigem] = array();
          $arrProcessamento[$idOrigem] = array();
          $arrObjAlgoritmoDTOTipo = array();
          if (isset($arrObjAlgoritmoDTO[$idOrigem])) {
            $arrObjAlgoritmoDTOTipo=array();
            foreach ($arrObjAlgoritmoDTO[$idOrigem] as $objAlgoritmoDTO) {
              $idUsuario=$objAlgoritmoDTO->getNumIdUsuario();
              if(!isset($arrObjAlgoritmoDTOTipo[$idUsuario])){
                $arrObjAlgoritmoDTOTipo[$idUsuario]=$objAlgoritmoDTO;
              }else {
                $this->excluir(array($objAlgoritmoDTO));
              }
            }
            $arrObjAlgoritmoDTO[$idOrigem]=$arrObjAlgoritmoDTOTipo;
          }
          foreach ($arrObjColegiadoComposicaoDTO as $idUsuario => $objColegiadoComposicaoDTO) {
            $dblPeso=(double)InfraUtil::prepararDbl($objColegiadoComposicaoDTO->getDblPeso());
            if ($dblPeso===0.0) {
              continue;
            }
            if($dblPeso!==1.0) {
              $objInfraException->lancarValidacao('Peso inv�lido processando algoritmo de distribui��o. Deve ser 0 ou 1.');
            }
            $arrResultado[$idOrigem][$idUsuario] = 0;
            if (isset($arrObjAlgoritmoDTOTipo[$idUsuario])) {
              $objAlgoritmoDTO = $arrObjAlgoritmoDTOTipo[$idUsuario];
              $arrProcessamento[$idOrigem][$idUsuario] = $objAlgoritmoDTO->getNumContador();
            } else {
              $arrProcessamento[$idOrigem][$idUsuario] = 0;
            }
          }
        }
        break;
      case self::$ALG_RODADA_MATERIA:
        $arrProcessamento = array();

        $arrIdOrigem = array();
        $arrTemp = array();
        foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
          $idOrigem = $objDistribuicaoDTO->getNumIdTipoMateriaAutuacao();
          if ($idOrigem===null) {
            $arrTemp[] = $objDistribuicaoDTO->getDblIdProcedimento();
          }
          $arrIdOrigem[$idOrigem] = $idOrigem;
        }
        if (InfraArray::contar($arrTemp)>0) {
          $objProtocoloDTO = new ProtocoloDTO();
          $objProtocoloRN = new ProtocoloRN();
          $objProtocoloDTO->setDblIdProtocolo($arrTemp, InfraDTO::$OPER_IN);
          $objProtocoloDTO->retStrProtocoloFormatado();
          $arrObjProtocoloDTO = $objProtocoloRN->listarRN0668($objProtocoloDTO);
          foreach ($arrObjProtocoloDTO as $objProtocoloDTO) {
            $objInfraException->adicionarValidacao('Processo ' . $objProtocoloDTO->getStrProtocoloFormatado() . ' n�o possui Tipo de Mat�ria autuada.');
          }
        }
        $objInfraException->lancarValidacoes();


        $objAlgoritmoDTO->setNumIdOrigem($arrIdOrigem, InfraDTO::$OPER_IN);
        $arrObjAlgoritmoDTO = $this->listar($objAlgoritmoDTO);

        $arrObjAlgoritmoDTO = InfraArray::indexarArrInfraDTO($arrObjAlgoritmoDTO, 'IdOrigem', true);
        foreach ($arrIdOrigem as $idOrigem) {
          $arrResultado[$idOrigem] = array();
          $arrProcessamento[$idOrigem] = array();
          $arrObjAlgoritmoDTOTipo = array();
          if (isset($arrObjAlgoritmoDTO[$idOrigem])) {
            $arrObjAlgoritmoDTOTipo=array();
            foreach ($arrObjAlgoritmoDTO[$idOrigem] as $objAlgoritmoDTO) {
              $idUsuario=$objAlgoritmoDTO->getNumIdUsuario();
              if(!isset($arrObjAlgoritmoDTOTipo[$idUsuario])){
                $arrObjAlgoritmoDTOTipo[$idUsuario]=$objAlgoritmoDTO;
              }else {
                $this->excluir(array($objAlgoritmoDTO));
              }
            }
            $arrObjAlgoritmoDTO[$idOrigem]=$arrObjAlgoritmoDTOTipo;
          }
          foreach ($arrObjColegiadoComposicaoDTO as $idUsuario => $objColegiadoComposicaoDTO) {
            $dblPeso=(double)InfraUtil::prepararDbl($objColegiadoComposicaoDTO->getDblPeso());
            if ($dblPeso===0.0) {
              continue;
            }
            if($dblPeso!==1.0) {
              $objInfraException->lancarValidacao('Peso inv�lido processando algoritmo de distribui��o. Deve ser 0 ou 1.');
            }
            $arrResultado[$idOrigem][$idUsuario] = 0;
            if (isset($arrObjAlgoritmoDTOTipo[$idUsuario])) {
              $objAlgoritmoDTO = $arrObjAlgoritmoDTOTipo[$idUsuario];
              $arrProcessamento[$idOrigem][$idUsuario] = $objAlgoritmoDTO->getNumContador();
            } else {
              $arrProcessamento[$idOrigem][$idUsuario] = 0;
            }
          }
        }
        break;
      default:
        throw new InfraException('Algoritmo de distribui��o inv�lido.');
    }
    //execu��o do algoritmo
    $bolHouveExecucao=false;
    while ($numRepeticoes--) { //para simula��es executa a quantidade necess�ria

      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
        if ($objDistribuicaoDTO->getStrSinPrevencao()==='N') {
          $bolHouveExecucao=true;
          $arrObjImpedimentoDTO = $objDistribuicaoDTO->getArrObjImpedimentoDTO();
          $arrImpedidos = InfraArray::converterArrInfraDTO($arrObjImpedimentoDTO, 'IdUsuario', 'IdUsuario');
          switch ($staAlgoritmo) {
            case self::$ALG_PESO:
              $idUsuario = $this->processarPeso($arrPeso, $arrImpedidos);
              if ($bolSimulacao) {
                $arrResultado[$idUsuario] ++;
              }
              break;
            case self::$ALG_RODADA:
              $idUsuario = $this->processarRodada($arrProcessamento, $arrImpedidos);
              if ($bolSimulacao) {
                $arrResultado[$idUsuario] ++;
              }
              break;
            case self::$ALG_RODADA_TIPO:
              $idOrigem = $objDistribuicaoDTO->getNumIdTipoProcedimento();
              $idUsuario = $this->processarRodada($arrProcessamento[$idOrigem], $arrImpedidos);
              if ($bolSimulacao) {
                $arrResultado[$idOrigem][$idUsuario] ++;
              }
              break;
            case self::$ALG_RODADA_MATERIA:
              $idOrigem = $objDistribuicaoDTO->getNumIdTipoMateriaAutuacao();
              $idUsuario = $this->processarRodada($arrProcessamento[$idOrigem], $arrImpedidos);
              if ($bolSimulacao) {
                $arrResultado[$idOrigem][$idUsuario] ++;
              }
              break;

            default:
              throw new InfraException('Algoritmo de distribui��o: Processamento n�o definido.');
          }
          if (!$bolSimulacao) {
            $objDistribuicaoDTO->setNumIdUsuarioRelator($idUsuario);
            $objDistribuicaoDTO->setNumIdUnidadeRelator($arrObjColegiadoComposicaoDTO[$idUsuario]->getNumIdUnidade());
          }
        }
      }
    }

    //processamento de resultados
    if (!$bolSimulacao && $bolHouveExecucao) {
      switch ($staAlgoritmo) {
        case self::$ALG_RODADA:
          foreach ($arrObjColegiadoComposicaoDTO as $idUsuario => $objColegiadoComposicaoDTO) {
            if (isset($arrObjAlgoritmoDTO[$idUsuario],$arrProcessamento[$idUsuario]) ) {
              $contadorBanco = $arrObjAlgoritmoDTO[$idUsuario]->getNumContador();
              if ($contadorBanco!=$arrProcessamento[$idUsuario]) {
                $arrObjAlgoritmoDTO[$idUsuario]->setNumContador($arrProcessamento[$idUsuario]);
                $this->alterar($arrObjAlgoritmoDTO[$idUsuario]);
              }
            } else if ($arrProcessamento[$idUsuario]>0) {
              $objAlgoritmoDTO = new AlgoritmoDTO();
              $objAlgoritmoDTO->setNumIdColegiado($numIdColegiado);
              $objAlgoritmoDTO->setStrStaAlgoritmo($staAlgoritmo);
              $objAlgoritmoDTO->setNumIdUsuario($idUsuario);
              $objAlgoritmoDTO->setNumContador($arrProcessamento[$idUsuario]);
              $objAlgoritmoDTO->setNumIdOrigem(null);
              $this->cadastrar($objAlgoritmoDTO);
            }
          }
          break;
        case self::$ALG_RODADA_TIPO:
        case self::$ALG_RODADA_MATERIA:
          foreach ($arrIdOrigem as $idOrigem) {
            foreach ($arrObjColegiadoComposicaoDTO as $idUsuario => $objColegiadoComposicaoDTO) {
              if (isset($arrObjAlgoritmoDTO[$idOrigem][$idUsuario],$arrProcessamento[$idOrigem][$idUsuario])) {
                $contadorBanco = $arrObjAlgoritmoDTO[$idOrigem][$idUsuario]->getNumContador();
                if ($contadorBanco!=$arrProcessamento[$idOrigem][$idUsuario]) {
                  $arrObjAlgoritmoDTO[$idOrigem][$idUsuario]->setNumContador($arrProcessamento[$idOrigem][$idUsuario]);
                  $this->alterar($arrObjAlgoritmoDTO[$idOrigem][$idUsuario]);
                }
              } else if ($arrProcessamento[$idOrigem][$idUsuario]>0) {
                $objAlgoritmoDTO = new AlgoritmoDTO();
                $objAlgoritmoDTO->setNumIdColegiado($numIdColegiado);
                $objAlgoritmoDTO->setStrStaAlgoritmo($staAlgoritmo);
                $objAlgoritmoDTO->setNumIdUsuario($idUsuario);
                $objAlgoritmoDTO->setNumContador($arrProcessamento[$idOrigem][$idUsuario]);
                $objAlgoritmoDTO->setNumIdOrigem($idOrigem);
                $this->cadastrar($objAlgoritmoDTO);
              }
            }
          }
          break;
      }
    }
    if (!$bolSimulacao) {
      return $arrObjDistribuicaoDTO;
    }

    return $arrResultado;
  }
  private function processarPeso($arrPeso, $arrImpedidos)
  {
    $pesoTotal = 0;
    foreach ($arrPeso as $key => $value) {
      if (!isset($arrImpedidos[$key])) {
        $pesoTotal += 10 * InfraUtil::prepararDbl($value);
      }
    }
    //sorteia
    $sorteio = random_int(0, $pesoTotal - 1);
    $pesoParcial = 0;
    $chaves=array_keys($arrPeso);
    shuffle($chaves);
    foreach ($chaves as $key) {
      if (!isset($arrImpedidos[$key])) {
        $pesoParcial += 10*InfraUtil::prepararDbl($arrPeso[$key]);
      }
      if ($pesoParcial > $sorteio) {
        return $key;
      }
    }
    throw new InfraException('Erro: Ningu�m foi sorteado. (Tot='.$pesoTotal.', Sorteio='.$sorteio.')');
  }
  public function processarRodada(&$arrProcessamento, $arrImpedidos)
  {
    $bolRodadaNova = false;
    $arrTitularesRodada = array();
    foreach ($arrProcessamento as $key => $value) {
      if ($value == 0) {
        $arrTitularesRodada[] = $key;
      }
    }

    //se s� houver um titular dispon�vel na rodada, reinicia rodada
    if (InfraArray::contar($arrTitularesRodada) < 2) {
      $arrTitularesRodada = array();
      $bolRodadaNova = true;
      foreach ($arrProcessamento as $key => $value) {
        $arrProcessamento[$key]=0;
        $arrTitularesRodada[] = $key;
      }
    }
    $arrDisponiveis = array_diff($arrTitularesRodada, $arrImpedidos);
    //se retirar os impedimentos e restar somente 1 ou ningu�m, reinicia rodada
    if (!$bolRodadaNova && InfraArray::contar($arrDisponiveis) <2) {
      $arrTitularesRodada = array();
      foreach ($arrProcessamento as $key => $value) {
        $arrProcessamento[$key]=0;
        $arrTitularesRodada[] = $key;
      }
      $arrDisponiveis = array_diff($arrTitularesRodada, $arrImpedidos);
    }
    //se mesmo reiniciando n�o houver ningu�m, tem erro, pois todos est�o impedidos
    if (InfraArray::contar($arrDisponiveis) === 0){
      throw new InfraException('N�o foi poss�vel distribuir o processo. Todos os membros est�o impedidos.');
    }
    $sorteio = random_int(0, InfraArray::contar($arrDisponiveis) - 1);
    $id = array_values($arrDisponiveis)[$sorteio];
    $arrProcessamento[$id]=1;
    return $id;
  }

}
?>