<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 10/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class EventoSessaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return null;
  }

  public function montar() {

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdItemSessaoJulgamento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'TipoEvento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'SeqEvento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Descricao');

  }
}
?>