<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*
* Vers�o no SVN: $Id$
*/

try {
  require_once __DIR__ .'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('qualificacao_parte_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objQualificacaoParteDTO = new QualificacaoParteDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'qualificacao_parte_cadastrar':
      $strTitulo = 'Nova Qualifica��o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarQualificacaoParte" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objQualificacaoParteDTO->setNumIdQualificacaoParte(null);
      $objQualificacaoParteDTO->setStrDescricao($_POST['txtDescricao']);
      $objQualificacaoParteDTO->setStrSinAtivo('S');

      if (isset($_POST['sbmCadastrarQualificacaoParte'])) {
        try{
          $objQualificacaoParteRN = new QualificacaoParteRN();
          $objQualificacaoParteDTO = $objQualificacaoParteRN->cadastrar($objQualificacaoParteDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Qualifica��o "'.$objQualificacaoParteDTO->getStrDescricao().'" cadastrada com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_qualificacao_parte='.$objQualificacaoParteDTO->getNumIdQualificacaoParte().PaginaSEI::getInstance()->montarAncora($objQualificacaoParteDTO->getNumIdQualificacaoParte())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'qualificacao_parte_alterar':
      $strTitulo = 'Alterar Qualifica��o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarQualificacaoParte" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_qualificacao_parte'])){
        $objQualificacaoParteDTO->setNumIdQualificacaoParte($_GET['id_qualificacao_parte']);
        $objQualificacaoParteDTO->retTodos();
        $objQualificacaoParteRN = new QualificacaoParteRN();
        $objQualificacaoParteDTO = $objQualificacaoParteRN->consultar($objQualificacaoParteDTO);
        if ($objQualificacaoParteDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objQualificacaoParteDTO->setNumIdQualificacaoParte($_POST['hdnIdQualificacaoParte']);
        $objQualificacaoParteDTO->setStrDescricao($_POST['txtDescricao']);
        $objQualificacaoParteDTO->setStrSinAtivo('S');
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objQualificacaoParteDTO->getNumIdQualificacaoParte())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarQualificacaoParte'])) {
        try{
          $objQualificacaoParteRN = new QualificacaoParteRN();
          $objQualificacaoParteRN->alterar($objQualificacaoParteDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Qualifica��o "'.$objQualificacaoParteDTO->getStrDescricao().'" alterada com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objQualificacaoParteDTO->getNumIdQualificacaoParte())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'qualificacao_parte_consultar':
      $strTitulo = 'Consultar Qualifica��o';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_qualificacao_parte'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objQualificacaoParteDTO->setNumIdQualificacaoParte($_GET['id_qualificacao_parte']);
      $objQualificacaoParteDTO->setBolExclusaoLogica(false);
      $objQualificacaoParteDTO->retTodos();
      $objQualificacaoParteRN = new QualificacaoParteRN();
      $objQualificacaoParteDTO = $objQualificacaoParteRN->consultar($objQualificacaoParteDTO);
      if ($objQualificacaoParteDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
#lblDescricao {position:absolute;left:0%;top:0%;width:95%;}
#txtDescricao {position:absolute;left:0%;top:40%;width:95%;}

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='qualificacao_parte_cadastrar'){
    document.getElementById('txtDescricao').focus();
  } else if ('<?=$_GET['acao']?>'=='qualificacao_parte_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
}

function validarCadastro() {
  if (infraTrim(document.getElementById('txtDescricao').value)=='') {
    alert('Informe a Descri��o.');
    document.getElementById('txtDescricao').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmQualificacaoParteCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblDescricao" for="txtDescricao" accesskey="d" class="infraLabelObrigatorio"><span class="infraTeclaAtalho">D</span>escri��o:</label>
  <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="<?=PaginaSEI::tratarHTML($objQualificacaoParteDTO->getStrDescricao());?>" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
<?
PaginaSEI::getInstance()->fecharAreaDados();
?>
  <input type="hidden" id="hdnIdQualificacaoParte" name="hdnIdQualificacaoParte" value="<?=$objQualificacaoParteDTO->getNumIdQualificacaoParte();?>" />
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>