<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4ª REGIÃO
*
* 23/03/2015 - criado por bcu
*
* Versão do Gerador de Código: 1.33.1
*
* Versão no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('motivo_distribuicao_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->salvarCamposPost(array('selStaTipo','selColegiado'));

  switch($_GET['acao']){
    case 'motivo_distribuicao_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoDistribuicaoDTO = array();
        for ($i=0;$i<InfraArray::contar($arrStrIds);$i++){
          $objMotivoDistribuicaoDTO = new MotivoDistribuicaoDTO();
          $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($arrStrIds[$i]);
          $arrObjMotivoDistribuicaoDTO[] = $objMotivoDistribuicaoDTO;
        }
        $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
        $objMotivoDistribuicaoRN->excluir($arrObjMotivoDistribuicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;


    case 'motivo_distribuicao_desativar':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoDistribuicaoDTO = array();
        for ($i=0;$i<InfraArray::contar($arrStrIds);$i++){
          $objMotivoDistribuicaoDTO = new MotivoDistribuicaoDTO();
          $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($arrStrIds[$i]);
          $arrObjMotivoDistribuicaoDTO[] = $objMotivoDistribuicaoDTO;
        }
        $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
        $objMotivoDistribuicaoRN->desativar($arrObjMotivoDistribuicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

    case 'motivo_distribuicao_reativar':
      $strTitulo = 'Reativar Motivos de Distribuição';
      if ($_GET['acao_confirmada']=='sim'){
        try{
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjMotivoDistribuicaoDTO = array();
          for ($i=0;$i<InfraArray::contar($arrStrIds);$i++){
            $objMotivoDistribuicaoDTO = new MotivoDistribuicaoDTO();
            $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($arrStrIds[$i]);
            $arrObjMotivoDistribuicaoDTO[] = $objMotivoDistribuicaoDTO;
          }
          $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
          $objMotivoDistribuicaoRN->reativar($arrObjMotivoDistribuicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        } 
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
        die;
      } 
      break;


    case 'motivo_distribuicao_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Motivo de Distribuição','Selecionar Motivos de Distribuição');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='motivo_distribuicao_cadastrar'){
        if (isset($_GET['id_motivo_distribuicao'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_motivo_distribuicao']);
        }
      }
      break;

    case 'motivo_distribuicao_listar':
      $strTitulo = 'Motivos de Distribuição';
      break;

    default:
      throw new InfraException("Ação '".$_GET['acao']."' não reconhecida.");
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'motivo_distribuicao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  if ($_GET['acao'] == 'motivo_distribuicao_listar' || $_GET['acao'] == 'motivo_distribuicao_selecionar'){
    $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_cadastrar');
    if ($bolAcaoCadastrar){
      $arrComandos[] = '<button type="button" accesskey="N" id="btnNovo" value="Novo" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_distribuicao_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ovo</button>';
    }
  }

  $objMotivoDistribuicaoDTO = new MotivoDistribuicaoDTO();
  $objMotivoDistribuicaoDTO->retNumIdMotivoDistribuicao();
  $objMotivoDistribuicaoDTO->retStrDescricao();
  $objMotivoDistribuicaoDTO->retStrStaTipo();

  $strStaTipo = PaginaSEI::getInstance()->recuperarCampo('selStaTipo');
  if ($strStaTipo!==''){
    $objMotivoDistribuicaoDTO->setStrStaTipo($strStaTipo);
  }

  $objMotivoDistribuicaoDTO->setBolExclusaoLogica(false);
  $objMotivoDistribuicaoDTO->retStrSinAtivo();


  $numIdColegiado = PaginaSEI::getInstance()->recuperarCampo('selColegiado');
  if ($numIdColegiado!=''){
    $objMotivoDistribuicaoDTO->setNumIdColegiado($numIdColegiado);
  }

  PaginaSEI::getInstance()->prepararOrdenacao($objMotivoDistribuicaoDTO, 'Descricao', InfraDTO::$TIPO_ORDENACAO_ASC);
  //PaginaSEI::getInstance()->prepararPaginacao($objMotivoDistribuicaoDTO);

  $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
  $arrObjMotivoDistribuicaoDTO = $objMotivoDistribuicaoRN->listar($objMotivoDistribuicaoDTO);

  //PaginaSEI::getInstance()->processarPaginacao($objMotivoDistribuicaoDTO);
  $numRegistros = InfraArray::contar($arrObjMotivoDistribuicaoDTO);
  $arrObjTipoDTO=$objMotivoDistribuicaoRN->listarValoresTipo();
  $arrObjTipoDTO=InfraArray::indexarArrInfraDTO($arrObjTipoDTO,'StaTipo');

  if ($numRegistros > 0){

    $bolCheck = false;

    if ($_GET['acao']=='motivo_distribuicao_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_alterar');
      $bolAcaoImprimir = false;
      $bolAcaoExcluir = false;
      $bolAcaoDesativar = false;
      $bolCheck = true;

    }else{
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_alterar');
      $bolAcaoImprimir = true;
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_excluir');
      $bolAcaoDesativar = SessaoSEI::getInstance()->verificarPermissao('motivo_distribuicao_desativar');
    }

    
    if ($bolAcaoDesativar){
      $strLinkDesativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_distribuicao_desativar&acao_origem='.$_GET['acao']);
    }

    if ($bolAcaoReativar){
      $strLinkReativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_distribuicao_reativar&acao_origem='.$_GET['acao'].'&acao_confirmada=sim');
    }
    

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_distribuicao_excluir&acao_origem='.$_GET['acao']);
    }

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Motivos de Distribuição.';
    $strCaptionTabela = 'Motivos de Distribuição';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objMotivoDistribuicaoDTO,'Descrição','Descricao',$arrObjMotivoDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="15%">'.PaginaSEI::getInstance()->getThOrdenacao($objMotivoDistribuicaoDTO,'Tipo','StaTipo',$arrObjMotivoDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="15%">Colegiados</th>'."\n";
    $strResultado .= '<th class="infraTh" width="15%">Ações</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';


    $objRelMotivoDistrColegiadoDTO = new RelMotivoDistrColegiadoDTO();
    $objRelMotivoDistrColegiadoDTO->retNumIdMotivoDistribuicao();
    $objRelMotivoDistrColegiadoDTO->setNumIdMotivoDistribuicao(InfraArray::converterArrInfraDTO($arrObjMotivoDistribuicaoDTO,'IdMotivoDistribuicao'),InfraDTO::$OPER_IN);

    $objRelMotivoDistrColegiadoRN = new RelMotivoDistrColegiadoRN();
    $arrObjRelMotivoDistrColegiadoDTO = InfraArray::indexarArrInfraDTO($objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO),'IdMotivoDistribuicao',true);







    for($i = 0;$i < $numRegistros; $i++){

      if ($arrObjMotivoDistribuicaoDTO[$i]->getStrSinAtivo()=='S'){
        $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      }else{
        $strCssTr = '<tr class="trVermelha">';
      }

      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao(),$arrObjMotivoDistribuicaoDTO[$i]->getStrDescricao()).'</td>';
      }
      $strResultado .= '<td>'.$arrObjMotivoDistribuicaoDTO[$i]->getStrDescricao().'</td>';
      $strResultado .= '<td align="center">'.$arrObjTipoDTO[$arrObjMotivoDistribuicaoDTO[$i]->getStrStaTipo()]->getStrDescricao().'</td>';
      $strResultado .= '<td align="center">'.InfraArray::contar($arrObjRelMotivoDistrColegiadoDTO[$arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao()]).'</td>';
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao());

      if ($bolAcaoConsultar){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_distribuicao_consultar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_motivo_distribuicao='.$arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeConsultar().'" title="Consultar Motivo de Distribuição" alt="Consultar Motivo de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoAlterar && $arrObjMotivoDistribuicaoDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_distribuicao_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_motivo_distribuicao='.$arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Motivo de Distribuição" alt="Alterar Motivo de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoDesativar || $bolAcaoReativar || $bolAcaoExcluir){
        $strId = $arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao();
        $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjMotivoDistribuicaoDTO[$i]->getStrDescricao());
      }

      if ($bolAcaoDesativar && $arrObjMotivoDistribuicaoDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoDesativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeDesativar().'" title="Desativar Motivo de Distribuição" alt="Desativar Motivo de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoReativar && $arrObjMotivoDistribuicaoDTO[$i]->getStrSinAtivo()=='N'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoReativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeReativar().'" title="Reativar Motivo de Distribuição" alt="Reativar Motivo de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoExcluir){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoExcluir(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Motivo de Distribuição" alt="Excluir Motivo de Distribuição" class="infraImg" /></a>&nbsp;';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'motivo_distribuicao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

  $strItensSelStaTipo = MotivoDistribuicaoINT::montarSelectStaTipo('','Todos',$strStaTipo);
  $strItensSelColegiado = ColegiadoINT::montarSelectNome('','Todos',$numIdColegiado);

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblStaTipo {position:absolute;left:0%;top:0%;width:30%;}
#selStaTipo {position:absolute;left:0%;top:20%;width:30%;}

#lblColegiado {position:absolute;left:0%;top:50%;width:70%;}
#selColegiado {position:absolute;left:0%;top:70%;width:70%;}
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();

if(0){?><script type="application/javascript"><?}
?>
  var objAutoCompletarColegiado = null;
  
function inicializar(){
  if ('<?=$_GET['acao']?>'=='motivo_distribuicao_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('btnFechar').focus();
  }

  infraEfeitoTabelas();
}

<? if ($bolAcaoDesativar){ ?>
function acaoDesativar(id,desc){
  if (confirm("Confirma desativação do Motivo de Distribuição \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoDistribuicaoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoDistribuicaoLista').submit();
  }
}

function acaoDesativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Distribuição selecionado.');
    return;
  }
  if (confirm("Confirma desativação dos Motivos de Distribuição selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoDistribuicaoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoDistribuicaoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoReativar){ ?>
function acaoReativar(id,desc){
  if (confirm("Confirma reativação do Motivo de Distribuição \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoDistribuicaoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoDistribuicaoLista').submit();
  }
}

function acaoReativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Distribuição selecionado.');
    return;
  }
  if (confirm("Confirma reativação dos Motivos de Distribuição selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoDistribuicaoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoDistribuicaoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclusão do Motivo de Distribuição \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoDistribuicaoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoDistribuicaoLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Distribuição selecionado.');
    return;
  }
  if (confirm("Confirma exclusão dos Motivos de Distribuição selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoDistribuicaoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoDistribuicaoLista').submit();
  }
}
<? } ?>

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmMotivoDistribuicaoLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->abrirAreaDados('10em');
  ?>

  <label id="lblStaTipo" for="selStaTipo" accesskey="" class="infraLabelOpcional">Tipo:</label>
  <select id="selStaTipo" name="selStaTipo" onchange="this.form.submit();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
    <?=$strItensSelStaTipo?>
  </select>

  <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelOpcional">Colegiado:</label>
  <select id="selColegiado" name="selColegiado" onchange="this.form.submit();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
    <?=$strItensSelColegiado?>
  </select>
  
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>