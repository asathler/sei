<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 16/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MotivoCancDistribuicaoINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objMotivoCancDistribuicaoDTO = new MotivoCancDistribuicaoDTO();
    $objMotivoCancDistribuicaoDTO->retNumIdMotivoCancDistribuicao();
    $objMotivoCancDistribuicaoDTO->retStrDescricao();

    if ($strValorItemSelecionado!=null){
      $objMotivoCancDistribuicaoDTO->setBolExclusaoLogica(false);
      $objMotivoCancDistribuicaoDTO->adicionarCriterio(array('SinAtivo','IdMotivoCancDistribuicao'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objMotivoCancDistribuicaoDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
    $arrObjMotivoCancDistribuicaoDTO = $objMotivoCancDistribuicaoRN->listar($objMotivoCancDistribuicaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjMotivoCancDistribuicaoDTO, 'IdMotivoCancDistribuicao', 'Descricao');
  }
}
?>