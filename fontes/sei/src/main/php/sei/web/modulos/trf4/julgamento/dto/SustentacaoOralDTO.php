<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 04/10/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class SustentacaoOralDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'sustentacao_oral';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdSustentacaoOral','id_sustentacao_oral');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdItemSessaoJulgamento','id_item_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdQualificacaoParte','id_qualificacao_parte');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdContato','id_contato');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeContato','nome','contato');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoQualificacaoParte','descricao','qualificacao_parte');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamentoItem','id_sessao_julgamento','item_sessao_julgamento');


    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdParteProcedimento');

    $this->configurarPK('IdSustentacaoOral',InfraDTO::$TIPO_PK_NATIVA);


    $this->configurarFK('IdQualificacaoParte', 'qualificacao_parte', 'id_qualificacao_parte');
    $this->configurarFK('IdContato', 'contato', 'id_contato');
    $this->configurarFK('IdItemSessaoJulgamento', 'item_sessao_julgamento', 'id_item_sessao_julgamento');

  }
}
?>