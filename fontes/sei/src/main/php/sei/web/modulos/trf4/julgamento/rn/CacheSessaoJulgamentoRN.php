<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class CacheSessaoJulgamentoRN extends InfraRN {


  /**
   * array de SessoJulgamentoDTO indexado pelo IdSessao
   * @var SessaoJulgamentoDTO[]
   */
  private static $arrObjSessaoJulgamentoDTO=array();
  /**
   * array de SessaoBlocoDTO indexado pelo IdSessaoBloco
   * @var SessaoBlocoDTO[]
   */
  private static $arrObjSessaoBlocoDTO=[];


  /**
   * array de IdItemSessaoJulgamentoDTO agrupado por IdSessao
   * @var int[][]
   */
  private static $arrIdItemSessaoJulgamentoDTO=array();

  /**
   * array de ItemSessaoJulgamentoDTO indexado pelo IdItemSessao
   * @var ItemSessaoJulgamentoDTO[]
   */
  private static $arrObjItemSessaoJulgamentoDTO=array();
  /**
   * @var PedidoVistaDTO[]
   */
  private static $arrObjPedidoVistaDTO=array();
  /**
   * @var ColegiadoComposicaoDTO[][]
   */
  private static $arrObjColegiadoComposicaoDTO=array();
  /**
   * @var PresencaSessaoDTO[][]
   */
  private static $arrObjPresencaSessaoDTO=array();
  /**
   * @var ProtocoloDTO[][]
   */
  private static $arrObjProtocoloDTO=array();
  /**
   * @var DestaqueDTO[][]
   */
  private static $arrObjDestaqueDTO=array();
  /**
   * array de JulgamentoParteDTO indexado pelo IdJulgamentoParte
   * @var JulgamentoParteDTO[]
   */
  private static $arrObjJulgamentoParteDTO=array();
  /**
   * array de JulgamentoParteDTO indexado pelo IdJulgamentoParte e agrupado por IdItemSessao
   * @var JulgamentoParteDTO[][]
   */
  private static $idxObjJulgamentoParteDTO=array();
  /**
   * array de VotoParteDTO indexado pelo IdUsuario e agrupado por IdJulgamentoParte e IdItem
   * @var VotoParteDTO[][][]
   */
  private static $arrObjVotoParteDTO=array();
  /**
   * array de ItemSessaoDocumentoDTO agrupado por IdItem
   * @var ItemSessaoDocumentoDTO[][]
   */
  private static $arrObjItemSessaoDocumentoDTO=array();
  /**
   * array de BloqueioItemSessUnidadeDTO agrupado por IdItem
   * @var BloqueioItemSessUnidadeDTO[][]
   */
  private static $arrObjBloqueioItemSessUnidadeDTO=array();


  /**
   * array de parametros usados pelo m�dulo julgar
   * @var string[]
   */
  private static $arrParametrosModulo;

  /**
   * @return string[]
   */
  public static function getArrParametrosModulo(): array
  {
    if(self::$arrParametrosModulo==null){
      $objInfraParametroDTO = new InfraParametroDTO();
      $objInfraParametroDTO->retStrValor();
      $objInfraParametroDTO->retStrNome();
      $objInfraParametroDTO->setStrNome('MD_JULGAR%',InfraDTO::$OPER_LIKE);

      $objInfraParametro=new InfraParametro(BancoSEI::getInstance());
      $objInfraParametroRN = new InfraParametroRN();
      $arrObjInfraParametroDTO = $objInfraParametroRN->listar($objInfraParametroDTO);


      self::$arrParametrosModulo=[];
      foreach ($arrObjInfraParametroDTO as $objInfraParametroDTO) {
        self::$arrParametrosModulo[$objInfraParametroDTO->getStrNome()]=$objInfraParametroDTO->getStrValor();
      }
      $objMdJulgarConfiguracaoRN=new MdJulgarConfiguracaoRN();
      $arrParamConf=$objMdJulgarConfiguracaoRN->getArrParametrosConfiguraveis();

      foreach ($arrParamConf as $strNome=>$arr) {
        if(!isset(self::$arrParametrosModulo[$strNome])){
          self::$arrParametrosModulo[$strNome]=null;
        }
      }
    }
    return self::$arrParametrosModulo;
  }



  public function __construct(){
    parent::__construct();
  }

  /**
   * @param $varIdItemSessaoJulgamento
   * @param $arrObj
   * @param $arrIdPesquisa
   * @return array
   */
  private static function prepararPesquisa($varIdItemSessaoJulgamento, $arrObj, &$arrIdPesquisa): array
  {
    if (!is_array($varIdItemSessaoJulgamento)) {
      $varIdItemSessaoJulgamento = array($varIdItemSessaoJulgamento);
    }
    $ret = array();
    $arrIdPesquisa = array();
    foreach ($varIdItemSessaoJulgamento as $numIdItemSessaoJulgamento) {
      if (isset($arrObj[$numIdItemSessaoJulgamento])) {
        $ret[$numIdItemSessaoJulgamento] = $arrObj[$numIdItemSessaoJulgamento];
      } else {
        $arrIdPesquisa[] = $numIdItemSessaoJulgamento;
      }
    }
    return $ret;
  }

  /**
   * @return SessaoBlocoDTO[]
   * @throws InfraException
   */
  public static function getArrObjSessaoBlocoDTO($numIdSessaoJulgamento): array
  {
    $objSessaoBlocoDTO=new SessaoBlocoDTO();
    $objSessaoBlocoDTO->retTodos();
    $objSessaoBlocoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
    $objSessaoBlocoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
    $objSessaoBlocoRN=new SessaoBlocoRN();
    $arrObjSessaoBlocoDTO=$objSessaoBlocoRN->listar($objSessaoBlocoDTO);
    $arrObjSessaoBlocoDTO=InfraArray::indexarArrInfraDTO($arrObjSessaoBlocoDTO,'IdSessaoBloco');
    foreach ($arrObjSessaoBlocoDTO as $numIdSessaoBloco=>$objSessaoBlocoDTO) {
      self::$arrObjSessaoBlocoDTO[$numIdSessaoBloco]=$objSessaoBlocoDTO;
    }

    return $arrObjSessaoBlocoDTO;
  }

  /**
   * @param $numIdSessaoBloco
   * @return SessaoBlocoDTO|null
   */
  public static function getObjSessaoBlocoDTO($numIdSessaoBloco): ?SessaoBlocoDTO
  {
    if(!isset(self::$arrObjSessaoBlocoDTO[$numIdSessaoBloco])){
      $objSessaoBlocoDTO=new SessaoBlocoDTO();
      $objSessaoBlocoDTO->retTodos();
      $objSessaoBlocoDTO->setNumIdSessaoBloco($numIdSessaoBloco);
      $objSessaoBlocoRN=new SessaoBlocoRN();
      self::$arrObjSessaoBlocoDTO[$numIdSessaoBloco]=$objSessaoBlocoRN->consultar($objSessaoBlocoDTO);
    }
    return self::$arrObjSessaoBlocoDTO[$numIdSessaoBloco];
  }

  /**
   * @param $arrObjItemSessaoJulgamentoDTO
   * @return ProtocoloDTO[]
   * @throws InfraException
   */
  public static function pesquisaAcessoDocumentos($arrObjItemSessaoJulgamentoDTO): array
  {
    $arrIdProtocolos = [];
    if($arrObjItemSessaoJulgamentoDTO==null){
      return [];
    }
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
      $arrIdProtocolos[] = $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao();
      if (is_array($arrObjItemSessaoDocumentoDTO)) {
        foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
          $arrIdProtocolos[] = $objItemSessaoDocumentoDTO->getDblIdDocumento();
        }
      }
    }
    $arrObjProtocoloDTO = array();
    if (InfraArray::contar($arrIdProtocolos)) {
      $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
      $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_TODOS);
      $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_AUTORIZADO);
      $objPesquisaProtocoloDTO->setDblIdProtocolo($arrIdProtocolos);

      $objProtocoloRN = new ProtocoloRN();
      $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);
      if ($arrObjProtocoloDTO!=null) {
        $arrObjProtocoloDTO = InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO, 'IdProtocolo');
      }
    }
    return $arrObjProtocoloDTO;
  }


  /**
   * @return BancoSEI
   */
  protected function inicializarObjInfraIBanco() :InfraIBanco {
    return BancoSEI::getInstance();
  }
//----------------------------- Dados da Sess�o --------------------------------
  /**
   * OK
   * @param int $numIdSessaoJulgamento
   * @return SessaoJulgamentoDTO
   */
  public static function getObjSessaoJulgamentoDTO(int $numIdSessaoJulgamento): SessaoJulgamentoDTO
  {
    if(!isset(self::$arrObjSessaoJulgamentoDTO[$numIdSessaoJulgamento])){
      InfraDebug::getInstance()->gravar('Cache Sess�o Pesquisar ID:'.$numIdSessaoJulgamento);
      $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
      $objSessaoJulgamentoDTO->retTodos();
      $objSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objSessaoJulgamentoDTO->retStrNomeColegiado();
      $objSessaoJulgamentoDTO->retStrSiglaColegiado();
      $objSessaoJulgamentoDTO->retNumQuorumMinimoColegiado();
      $objSessaoJulgamentoDTO->retNumIdUsuarioPresidenteColegiado();
      $objSessaoJulgamentoDTO->retNumIdUnidadeResponsavelColegiado();
      $objSessaoJulgamentoDTO->retNumIdTipoProcedimentoColegiado();
      $objSessaoJulgamentoDTO->retNumIdUsuarioPresidente();
      $objSessaoJulgamentoDTO->retNumIdColegiadoVersao();
      $objSessaoJulgamentoDTO->retStrSinVirtualTipoSessao();
      $objSessaoJulgamentoDTO->retStrDescricaoTipoSessao();
      $objSessaoJulgamentoDTO->retStrArtigoColegiado();//acompanhar item

      /** @var SessaoJulgamentoDTO $objSessaoJulgamentoDTO */
      $objSessaoJulgamentoDTO=$objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);

      if($objSessaoJulgamentoDTO!=null){
        $objSessaoJulgamentoDTO->setBolUnidadeSecretaria($objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()==SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $bolHorarioVotacao=false;
        if($objSessaoJulgamentoDTO->getStrStaSituacao()==SessaoJulgamentoRN::$ES_ABERTA){
          $dthAtual=InfraData::getStrDataHoraAtual();
          if($objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()!='S'){
            $bolHorarioVotacao=true;
          }else if(InfraData::compararDataHora($objSessaoJulgamentoDTO->getDthInicio(),$dthAtual)>=0 && InfraData::compararDataHora($objSessaoJulgamentoDTO->getDthFim(),$dthAtual)<0){
            $bolHorarioVotacao=true;
          }
        }
        $objSessaoJulgamentoDTO->setBolHorarioVotacaoAtivo($bolHorarioVotacao);
      }
      self::$arrObjSessaoJulgamentoDTO[$numIdSessaoJulgamento]=$objSessaoJulgamentoDTO;
    }
    return self::$arrObjSessaoJulgamentoDTO[$numIdSessaoJulgamento];
  }

  /**
   * retorna ColegiadoComposicao[] indexado pelo IdUsuario a partir de uma determinada sess�o de julgamento
   * @param int $numIdSessaoJulgamento
   * @return ColegiadoComposicaoDTO[]
   * @throws InfraException
   */
  public static function getArrObjColegiadoComposicaoDTO(int $numIdSessaoJulgamento): array
  {
    $objSessaoJulgamentoDTO=self::getObjSessaoJulgamentoDTO($numIdSessaoJulgamento);
    if(!isset(self::$arrObjColegiadoComposicaoDTO[$numIdSessaoJulgamento])){
      $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
      if ($objSessaoJulgamentoDTO->getNumIdColegiadoVersao() == null) {
        $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objSessaoJulgamentoDTO->getNumIdColegiado());
        $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      } else {
        $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($objSessaoJulgamentoDTO->getNumIdColegiadoVersao());
      }
      $objColegiadoComposicaoDTO->retStrNomeTipoMembroColegiado();
      $objColegiadoComposicaoDTO->retNumIdTipoMembroColegiado();
      $objColegiadoComposicaoDTO->retNumIdUsuario();
      $objColegiadoComposicaoDTO->retNumIdUnidade();
      $objColegiadoComposicaoDTO->retStrSiglaUnidade();
      $objColegiadoComposicaoDTO->retStrSiglaUsuario();
      $objColegiadoComposicaoDTO->retStrNomeUsuario();
      $objColegiadoComposicaoDTO->retStrDescricaoUnidade();
      $objColegiadoComposicaoDTO->retNumIdColegiadoComposicao();
      $objColegiadoComposicaoDTO->retNumIdColegiadoVersao();
      $objColegiadoComposicaoDTO->retNumOrdem();
      $objColegiadoComposicaoDTO->retStrExpressaoCargo();
      $objColegiadoComposicaoDTO->retStrStaGeneroCargo();
      $objColegiadoComposicaoDTO->retStrSinHabilitado();
      $objColegiadoComposicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
      InfraDebug::getInstance()->gravar('Cache Sess�o Pesquisar: Composi��o Colegiado');
      $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
      $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUsuario');
      self::$arrObjColegiadoComposicaoDTO[$numIdSessaoJulgamento]=$arrObjColegiadoComposicaoDTO;
      if(!$objSessaoJulgamentoDTO->isSetArrObjColegiadoComposicaoDTO()){
        $objSessaoJulgamentoDTO->setArrObjColegiadoComposicaoDTO($arrObjColegiadoComposicaoDTO);
      }
    }
    return self::$arrObjColegiadoComposicaoDTO[$numIdSessaoJulgamento];
  }

  /**
   * OK
   * @param int $numIdSessaoJulgamento
   * @return PresencaSessaoDTO[]
   * @throws InfraException
   */
  public static function getArrObjPresencaSessaoDTO(int $numIdSessaoJulgamento): array
  {
    if(!isset(self::$arrObjPresencaSessaoDTO[$numIdSessaoJulgamento])){
      $objPresencaSessaoRN = new PresencaSessaoRN();
      $objPresencaSessaoDTO = new PresencaSessaoDTO();
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
      $objPresencaSessaoDTO->setOrdDthEntrada(InfraDTO::$TIPO_ORDENACAO_ASC);
      $objPresencaSessaoDTO->retDthEntrada();
      $objPresencaSessaoDTO->retDthSaida();
      $objPresencaSessaoDTO->retNumIdUsuario();
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Presen�as');
      $arrObjPresencaSessaoDTO = $objPresencaSessaoRN->listar($objPresencaSessaoDTO);
      $arrObjPresencaSessaoDTO = InfraArray::indexarArrInfraDTO($arrObjPresencaSessaoDTO,'IdUsuario');

      $quorum=count($arrObjPresencaSessaoDTO);
      foreach ($arrObjPresencaSessaoDTO as $objPresencaSessaoDTO) {
        if($objPresencaSessaoDTO->getDthSaida()!=null){
          $quorum--;
        }
      }
      $objSessaoJulgamentoDTO=self::getObjSessaoJulgamentoDTO($numIdSessaoJulgamento);
      $objSessaoJulgamentoDTO->setNumQuorum($quorum);
      self::$arrObjPresencaSessaoDTO[$numIdSessaoJulgamento]=$arrObjPresencaSessaoDTO;
    }
    return self::$arrObjPresencaSessaoDTO[$numIdSessaoJulgamento];
  }

  /**
   * Retorna objProtocoloDTO dos itens da sess�o com seus respectivos objAnotacaoDTO
   * @param int $numIdSessaoJulgamento
   * @return ProtocoloDTO[]
   * @throws InfraException
   */
  public static function getArrObjProtocoloDTO(int $numIdSessaoJulgamento): array
  {
    if(!isset(self::$arrObjProtocoloDTO[$numIdSessaoJulgamento])){
      $arrObjProtocoloDTO = array();
      $baseDTO = new ProtocoloDTO();
      $baseDTO->setDblIdProtocolo(null);
      $baseDTO->setStrStaNivelAcessoGlobal(null);
      $arrObjItemSessaoJulgamentoDTO=self::getArrObjItemSessaoJulgamentoDTO($numIdSessaoJulgamento, true);
      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
        $objProtocoloDTO = clone $baseDTO;
        $objProtocoloDTO->setDblIdProtocolo($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
        $objProtocoloDTO->setStrStaNivelAcessoGlobal($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo());
        $arrObjProtocoloDTO[] = $objProtocoloDTO;
      }
      $objAnotacaoRN = new AnotacaoRN();
      $objAnotacaoRN->complementar($arrObjProtocoloDTO);
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Anota��es Processo');

      self::$arrObjProtocoloDTO[$numIdSessaoJulgamento]=InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO, 'IdProtocolo');
    }
    return self::$arrObjProtocoloDTO[$numIdSessaoJulgamento];
  }

  /**
   * OK
   * retorna id dos itens de determinada sess�o
   * @param $numIdSessaoJulgamento
   * @return int[]
   */
  public static function getArrIdItemSessaoJulgamentoDTO($numIdSessaoJulgamento): array
  {
    if(!isset(self::$arrIdItemSessaoJulgamentoDTO[$numIdSessaoJulgamento])){
      self::getArrObjItemSessaoJulgamentoDTO($numIdSessaoJulgamento, true);
    }
    return self::$arrIdItemSessaoJulgamentoDTO[$numIdSessaoJulgamento];
  }
//----------------------------- Dados dos Itens --------------------------------


  public static function getObjItemSessaoJulgamentoDTO($numIdItemSessaoJulgamento): ItemSessaoJulgamentoDTO
  {
    if(!isset(self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento])){
      self::getArrObjItemSessaoJulgamentoDTO($numIdItemSessaoJulgamento);
    }
    return self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento];
}
  /**
   * OK
   * @param int|int[] $varIdItemSessaoJulgamento
   * @return DestaqueDTO[][]
   * @throws InfraException
   */
  public static function getArrObjDestaqueDTO($varIdItemSessaoJulgamento): array
  {
    $ret=self::prepararPesquisa($varIdItemSessaoJulgamento, self::$arrObjDestaqueDTO, $arrPesquisa);
    $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();

    if(count($arrPesquisa)){
      $objDestaqueDTO = new DestaqueDTO();
      $objDestaqueDTO->retNumIdDestaque();
      $objDestaqueDTO->retStrStaAcesso();
      $objDestaqueDTO->retNumIdUnidade();
      $objDestaqueDTO->retNumIdUsuario();
      $objDestaqueDTO->retStrNomeUsuario();
      $objDestaqueDTO->retStrSiglaUnidade();
      $objDestaqueDTO->retStrDescricao();
      $objDestaqueDTO->retDthDestaque();
      $objDestaqueDTO->retNumIdItemSessaoJulgamento();
      $objDestaqueDTO->retStrStaTipo();
      $objDestaqueDTO->retNumIdUsuarioRelDestaqueUsuario();
      if(count($arrPesquisa)>1){
        $objDestaqueDTO->setNumIdItemSessaoJulgamento($arrPesquisa, InfraDTO::$OPER_IN);
      } else {
        $objDestaqueDTO->setNumIdItemSessaoJulgamento($arrPesquisa[0]);
      }

      $objDestaqueDTO->setNumIdUsuarioRelDestaqueUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objDestaqueDTO->adicionarCriterio(array('StaAcesso', 'IdUnidade'), array(InfraDTO::$OPER_IGUAL, InfraDTO::$OPER_IGUAL), array(DestaqueRN::$TA_PUBLICO, SessaoSEI::getInstance()->getNumIdUnidadeAtual()), InfraDTO::$OPER_LOGICO_OR);

      $objDestaqueRN = new DestaqueRN();
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Destaques');
      $arrObjDestaqueDTO=$objDestaqueRN->listar($objDestaqueDTO);
      $arrObjDestaqueDTO = InfraArray::indexarArrInfraDTO($arrObjDestaqueDTO, 'IdItemSessaoJulgamento', true);

      foreach ($arrPesquisa as $numIdItemSessaoJulgamento) {
        if(isset($arrObjDestaqueDTO[$numIdItemSessaoJulgamento])){
          foreach ($arrObjDestaqueDTO[$numIdItemSessaoJulgamento] as $key=>$objDestaqueDTO) {
            if ($objDestaqueDTO->getStrStaAcesso()===DestaqueRN::$TA_RESTRITO && $objDestaqueDTO->getNumIdUnidade()!=$numIdUnidadeAtual) {
              unset($arrObjDestaqueDTO[$key]);
              continue;
            }
            if ($objDestaqueDTO->getNumIdUsuarioRelDestaqueUsuario() == null) {
              $objDestaqueDTO->setStrSinVisualizado('N');
            } else {
              $objDestaqueDTO->setStrSinVisualizado('S');
            }
          }
          self::$arrObjDestaqueDTO[$numIdItemSessaoJulgamento]=$arrObjDestaqueDTO[$numIdItemSessaoJulgamento];
        } else{
          self::$arrObjDestaqueDTO[$numIdItemSessaoJulgamento]=array();
        }
        $ret[$numIdItemSessaoJulgamento]=self::$arrObjDestaqueDTO[$numIdItemSessaoJulgamento];
        if(isset(self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento])){
          self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento]->setArrObjDestaqueDTO(self::$arrObjDestaqueDTO[$numIdItemSessaoJulgamento]);
        }
      }
    }
    return $ret;
  }

  /**
   * OK - retorna ISJ solicitados no array ou todos os itens da sess�o se passado id_sessao e $bolIdSessao==true;
   * @param int|int[] $varIdItemSessaoJulgamento
   * @param bool $bolIdSessao //buscar por idSessao todos os itens e coloca no obj SessaoJulgamento
   * @return ItemSessaoJulgamentoDTO[]
   */
  public static function getArrObjItemSessaoJulgamentoDTO($varIdItemSessaoJulgamento, bool $bolIdSessao=false): array
  {
    if($bolIdSessao){
      $objSessaoJulgamentoDTO=self::getObjSessaoJulgamentoDTO($varIdItemSessaoJulgamento);
      if($objSessaoJulgamentoDTO->isSetArrObjItemSessaoJulgamentoDTO()){
        return $objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
      }
      $ret=array();
      $arrPesquisa=array($varIdItemSessaoJulgamento);
    } else {
      $ret=self::prepararPesquisa($varIdItemSessaoJulgamento, self::$arrObjItemSessaoJulgamentoDTO, $arrPesquisa);
    }

    if(count($arrPesquisa)){
      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retTodos();
      $objItemSessaoJulgamentoDTO->retNumOrdemMotivoMesa();
      $objItemSessaoJulgamentoDTO->retStrDescricaoMotivoMesa();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->retStrStaNivelAcessoGlobalProtocolo();
      $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTO->retStrStaUltimoDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdUsuarioRelatorDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdUnidadeRelatorDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdUsuarioRelatorAcordaoDistribuicao();
      $objItemSessaoJulgamentoDTO->retStrStaDistribuicao();
      $objItemSessaoJulgamentoDTO->retStrStaDistribuicaoAnterior();
      $objItemSessaoJulgamentoDTO->retStrNomeTipoProcedimento();
      $objItemSessaoJulgamentoDTO->retStrDescricaoProtocolo();
      $objItemSessaoJulgamentoDTO->retStrDescricaoSessaoBloco();
      $objItemSessaoJulgamentoDTO->retStrStaTipoItemSessaoBloco();
      $objItemSessaoJulgamentoDTO->retNumIdDistribuicaoAgrupadorDistribuicao();//acompanhar

      if($bolIdSessao){
        $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($arrPesquisa[0]);
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_IGNORADA, InfraDTO::$OPER_DIFERENTE);
        $objItemSessaoJulgamentoDTO->setOrdNumIdSessaoBloco(InfraDTO::$TIPO_ORDENACAO_ASC);
        $objItemSessaoJulgamentoDTO->setOrdNumIdUsuarioSessao(InfraDTO::$TIPO_ORDENACAO_ASC);
        $objItemSessaoJulgamentoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
        $objItemSessaoJulgamentoDTO->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_ASC);
      } else if(count($arrPesquisa)>1){
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrPesquisa,InfraDTO::$OPER_IN);
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_IGNORADA, InfraDTO::$OPER_DIFERENTE);
      } else  {
          $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrPesquisa[0]);
      }
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Itens');
      $arrObjItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
        $numIdItemSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
        self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento]=$objItemSessaoJulgamentoDTO;
        $ret[$numIdItemSessaoJulgamento]=$objItemSessaoJulgamentoDTO;
      }
    }
    if($bolIdSessao){
      $objSessaoJulgamentoDTO->setArrObjItemSessaoJulgamentoDTO($arrObjItemSessaoJulgamentoDTO);
      self::$arrIdItemSessaoJulgamentoDTO[$varIdItemSessaoJulgamento]=array_keys($ret);
      return $arrObjItemSessaoJulgamentoDTO;
    }
    return $ret;
  }

  /**
   * OK
   * @param int|int[] $varIdItemSessaoJulgamento
   * @return ItemSessaoDocumentoDTO[][]
   * @throws InfraException
   */
  public static function getArrObjItemSessaoDocumentoDTO($varIdItemSessaoJulgamento): array
  {
    $ret=self::prepararPesquisa($varIdItemSessaoJulgamento, self::$arrObjItemSessaoDocumentoDTO, $arrPesquisa);

    if(count($arrPesquisa)){
      $objItemSessaoDocumentoDTO=new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoRN=new ItemSessaoDocumentoRN();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($arrPesquisa,InfraDTO::$OPER_IN);
      $objItemSessaoDocumentoDTO->retTodos();
      $objItemSessaoDocumentoDTO->retStrProtocoloDocumentoFormatado();
      $objItemSessaoDocumentoDTO->retStrNomeSerie();
      $objItemSessaoDocumentoDTO->retStrNomeUsuario();
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Documentos');
      $arrObjItemSessaoDocumentoDTO=$objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
      if(InfraArray::contar($arrObjItemSessaoDocumentoDTO)){
        $arrObjItemSessaoDocumentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoDocumentoDTO,'IdItemSessaoJulgamento',true);
      }

      foreach ($arrPesquisa as $numIdItemSessaoJulgamento) {
        $ret[$numIdItemSessaoJulgamento]=$arrObjItemSessaoDocumentoDTO[$numIdItemSessaoJulgamento];
        self::$arrObjItemSessaoDocumentoDTO[$numIdItemSessaoJulgamento]=$arrObjItemSessaoDocumentoDTO[$numIdItemSessaoJulgamento];
        $objItemSessaoJulgamentoDTO=self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento];
        $objItemSessaoJulgamentoDTO->setArrObjItemSessaoDocumentoDTO($ret[$numIdItemSessaoJulgamento]);
      }
    }

    return self::$arrObjItemSessaoDocumentoDTO;
  }

  /**
   * reotrna arrObjBloqueioItemSessUnidadeDTO indexado por idItemSessaoJulgamento
   * e j� seta o array no $objItemSessaoJulgamentoDTO correspondente da CacheSessaoJulgamentoRN
   * @param int|int[] $varIdItemSessaoJulgamento
   * @return BloqueioItemSessUnidadeDTO[][]
   */
  public static function getArrObjBloqueioItemSessUnidadeDTO($varIdItemSessaoJulgamento): array
  {
    $ret=self::prepararPesquisa($varIdItemSessaoJulgamento, self::$arrObjBloqueioItemSessUnidadeDTO, $arrPesquisa);

    //arr [$idDistribuicao]=$idItemSessaoJulgamento;
    $arrNumIdItemSessaoJulgamento=array();
    foreach ($arrPesquisa as $numIdItemSessaoJulgamento) {
      $objItemSessaoJulgamentoDTO=self::getObjItemSessaoJulgamentoDTO($numIdItemSessaoJulgamento);
      if(!isset($arrNumIdItemSessaoJulgamento[$objItemSessaoJulgamentoDTO->getNumIdDistribuicao()])){
        $arrNumIdItemSessaoJulgamento[$objItemSessaoJulgamentoDTO->getNumIdDistribuicao()]=[$numIdItemSessaoJulgamento];
      } else {
        $arrNumIdItemSessaoJulgamento[$objItemSessaoJulgamentoDTO->getNumIdDistribuicao()][] = $numIdItemSessaoJulgamento;
      }

    }

    if(count($arrNumIdItemSessaoJulgamento)){
      $objBloqueioItemSessUnidadeDTO=new BloqueioItemSessUnidadeDTO();
      $objBloqueioItemSessUnidadeRN=new BloqueioItemSessUnidadeRN();
      $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao(array_keys($arrNumIdItemSessaoJulgamento),InfraDTO::$OPER_IN);
      $objBloqueioItemSessUnidadeDTO->retDblIdProcedimentoDistribuicao();
      $objBloqueioItemSessUnidadeDTO->retTodos();
      $objBloqueioItemSessUnidadeDTO->retStrSiglaUnidade();
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Bloqueios');
      $arrObjBloqueioItemSessUnidadeDTO=$objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);
      foreach ($arrObjBloqueioItemSessUnidadeDTO as $objBloqueioItemSessUnidadeDTO) {
        foreach ($arrNumIdItemSessaoJulgamento[$objBloqueioItemSessUnidadeDTO->getNumIdDistribuicao()] as $numIdItemSessaoJulgamento) {
          self::$arrObjBloqueioItemSessUnidadeDTO[$numIdItemSessaoJulgamento][]=$objBloqueioItemSessUnidadeDTO;
        }
      }
      foreach ($arrNumIdItemSessaoJulgamento as $arr) {
        foreach ($arr as $numIdItemSessaoJulgamento) {
          if(!isset(self::$arrObjBloqueioItemSessUnidadeDTO[$numIdItemSessaoJulgamento])){
            self::$arrObjBloqueioItemSessUnidadeDTO[$numIdItemSessaoJulgamento]=array();
          }
          $ret[$numIdItemSessaoJulgamento]=self::$arrObjBloqueioItemSessUnidadeDTO[$numIdItemSessaoJulgamento];
          self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento]->setArrObjBloqueioItemSessUnidadeDTO($ret[$numIdItemSessaoJulgamento]);
        }
      }
    }
    return $ret;
  }

  /**
   * OK
   * @param int|int[] $varIdItemSessaoJulgamento
   * @return PedidoVistaDTO[]
   * @throws InfraException
   */
  public static function getArrObjPedidoVistaDTO($varIdItemSessaoJulgamento): array
  {
    $ret=self::prepararPesquisa($varIdItemSessaoJulgamento, self::$arrObjPedidoVistaDTO, $arrPesquisa);

    $arrIdDistribuicao=array();
    foreach ($arrPesquisa as $numIdItemSessaoJulgamento) {
      $arrIdDistribuicao[]=self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento]->getNumIdDistribuicao();
    }

    if(count($arrIdDistribuicao)){
      $objPedidoVistaDTO = new PedidoVistaDTO();
      $objPedidoVistaDTO->retNumIdDistribuicao();
      $objPedidoVistaDTO->retNumIdUsuario();
      $objPedidoVistaDTO->setDthDevolucao(null);
      $objPedidoVistaDTO->setNumIdDistribuicao($arrIdDistribuicao,InfraDTO::$OPER_IN);

      $objPedidoVistaRN = new PedidoVistaRN();
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Pedido de vista');
      $arrObjPedidoVistaDTO=$objPedidoVistaRN->listar($objPedidoVistaDTO);
      $arrObjPedidoVistaDTO=InfraArray::indexarArrInfraDTO($arrObjPedidoVistaDTO,'IdDistribuicao');
      foreach ($arrIdDistribuicao as $key=>$numIdDistribuicao) {
        $numIdItemSessaoJulgamento=$arrPesquisa[$key];
        if(isset($arrObjPedidoVistaDTO[$numIdDistribuicao])){
          self::$arrObjPedidoVistaDTO[$numIdItemSessaoJulgamento]=$arrObjPedidoVistaDTO[$numIdDistribuicao];
        } else {
          self::$arrObjPedidoVistaDTO[$numIdItemSessaoJulgamento]=array();
        }
        $ret[$numIdItemSessaoJulgamento]=self::$arrObjPedidoVistaDTO[$numIdItemSessaoJulgamento];
      }
    }
    return $ret;
  }

  /**
   * OK
   * @param int|int[] $varIdItemSessaoJulgamento
   * @throws InfraException
   */
  public static function complementarItensRevisao($varIdItemSessaoJulgamento){
    $arrObjItemSessaoJulgamentoDTO=self::getArrObjItemSessaoJulgamentoDTO($varIdItemSessaoJulgamento);
    $arrIdItemSessaoJulgamento=array();
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      $numIdItemSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
      if(!$objItemSessaoJulgamentoDTO->isSetObjRevisaoItemDTO()){
        $arrIdItemSessaoJulgamento[]=$numIdItemSessaoJulgamento;
      }
    }

    if(count($arrIdItemSessaoJulgamento)){
      $objRevisaoItemDTO=new RevisaoItemDTO();
      $objRevisaoItemDTO->retNumIdItemSessaoJulgamento();
      $objRevisaoItemDTO->retNumIdUnidade();
      $objRevisaoItemDTO->retNumIdUsuario();
      $objRevisaoItemDTO->retStrSinRevisado();
      $objRevisaoItemDTO->retStrSiglaUsuario();
      $objRevisaoItemDTO->retDthRevisao();
      $objRevisaoItemDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objRevisaoItemDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento, InfraDTO::$OPER_IN);

      $objRevisaoItemRN = new RevisaoItemRN();
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Revis�es');
      $arrObjRevisaoItemDTO = InfraArray::indexarArrInfraDTO($objRevisaoItemRN->listar($objRevisaoItemDTO), 'IdItemSessaoJulgamento');
      $arrObjItemSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento');

      foreach ($arrObjRevisaoItemDTO as $numIdItemSessaoJulgamento => $objRevisaoItemDTO) {
        $arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento]->setObjRevisaoItemDTO($objRevisaoItemDTO);
      }
    }
  }

  /**
   * OK
   * @param int|int[] $varIdItemSessaoJulgamento
   * @return JulgamentoParteDTO[][]
   * @throws InfraException
   */
  public static function getArrObjJulgamentoParteDTO($varIdItemSessaoJulgamento): array
  {
    $ret=self::prepararPesquisa($varIdItemSessaoJulgamento, self::$idxObjJulgamentoParteDTO, $arrPesquisa);

    if(count($arrPesquisa)){
      $objJulgamentoParteDTO=new JulgamentoParteDTO();
      $objJulgamentoParteRN=new JulgamentoParteRN();
      $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($arrPesquisa,InfraDTO::$OPER_IN);
      $objJulgamentoParteDTO->retTodos();
      $objJulgamentoParteDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Fra��es');
      $arrObjJulgamentoParteDTO=$objJulgamentoParteRN->listar($objJulgamentoParteDTO);
      $arrObjJulgamentoParteDTO=InfraArray::indexarArrInfraDTO($arrObjJulgamentoParteDTO,'IdJulgamentoParte');
      foreach ($arrObjJulgamentoParteDTO as $idParte=>$objJulgamentoParteDTO) {
        $idItem=$objJulgamentoParteDTO->getNumIdItemSessaoJulgamento();
        self::$idxObjJulgamentoParteDTO[$idItem][$idParte]=$objJulgamentoParteDTO;
        self::$arrObjJulgamentoParteDTO[$idParte]=$objJulgamentoParteDTO;
      }
      foreach ($arrPesquisa as $numIdItemSessaoJulgamento) {
        $ret[$numIdItemSessaoJulgamento]=self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento];
        $objItemSessaoJulgamentoDTO=self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento];
        $objItemSessaoJulgamentoDTO->setArrObjJulgamentoParteDTO($ret[$numIdItemSessaoJulgamento]);
        if (InfraArray::contar($ret[$numIdItemSessaoJulgamento])>1) {
          $objItemSessaoJulgamentoDTO->setStrSinFracionado('S');
        } else {
          $objItemSessaoJulgamentoDTO->setStrSinFracionado('N');
        }
      }
    }
    return $ret;
  }

  public static function getObjJulgamentoParteDTO($numIdJulgamentoParte): JulgamentoParteDTO
  {
    if(!isset(self::$arrObjJulgamentoParteDTO[$numIdJulgamentoParte])) {
      $objJulgamentoParteDTO = new JulgamentoParteDTO();
      $objJulgamentoParteRN = new JulgamentoParteRN();
      $objJulgamentoParteDTO->setNumIdJulgamentoParte($numIdJulgamentoParte);
      $objJulgamentoParteDTO->retTodos();
      $objJulgamentoParteDTO = $objJulgamentoParteRN->consultar($objJulgamentoParteDTO);
      if($objJulgamentoParteDTO){
        self::$idxObjJulgamentoParteDTO[$objJulgamentoParteDTO->getNumIdItemSessaoJulgamento()][$numIdJulgamentoParte]=$objJulgamentoParteDTO;
      }
      self::$arrObjJulgamentoParteDTO[$numIdJulgamentoParte]=$objJulgamentoParteDTO;
    }
    return self::$arrObjJulgamentoParteDTO[$numIdJulgamentoParte];
  }

  //----------------------------- Dados das Fra��es ----------------------------
  /**
   * OK
   * @param int|int[] $varIdItemSessaoJulgamento
   * @return VotoParteDTO[][][]
   * @throws InfraException
   */
  public static function getArrObjVotoParteDTO($varIdItemSessaoJulgamento): array
  {
    $ret=self::prepararPesquisa($varIdItemSessaoJulgamento, self::$arrObjVotoParteDTO, $arrPesquisa);


    if(count($arrPesquisa)){//itens que n�o foram buscados os votos

      $objVotoParteRN=new VotoParteRN();
      $objVotoParteDTO = new VotoParteDTO();
      $objVotoParteDTO->setNumIdItemSessaoJulgamento($arrPesquisa,InfraDTO::$OPER_IN);
      $objVotoParteDTO->retTodos();
      $objVotoParteDTO->retStrConteudoProvimento();
      $objVotoParteDTO->retNumIdItemSessaoJulgamento();
      $objVotoParteDTO->retNumIdUsuarioVotoParte();
      $objVotoParteDTO->retStrNomeUsuario();
      $objVotoParteDTO->retStrNomeUsuarioAssociado();

      InfraDebug::getInstance()->gravar('Sess�o Pesquisar: Votos');
      $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);
//      if(count($arrObjVotoParteDTO)==0){
//        return array();
//      }
      $numIdSessaoJulgamento=self::getObjItemSessaoJulgamentoDTO($arrPesquisa[0])->getNumIdSessaoJulgamento();
      $arrObjColegiadoComposicaoDTO=self::getArrObjColegiadoComposicaoDTO($numIdSessaoJulgamento);
      $arrIdItensComVotoAnterior=[];
      $arrIdItensComVotoComposicaoAnterior=[];
      $arrIdItensComVotoSessaoAtual=[];
      $arrUsuariosVencedores=[];

      /** @var VotoParteDTO[] $arrObjVotoParteDTO */
      foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
        $numIdUsuario=$objVotoParteDTO->getNumIdUsuario();
        $numIdJulgamentoParte=$objVotoParteDTO->getNumIdJulgamentoParte();
        $numIdItemSessaoJulgamento=$objVotoParteDTO->getNumIdItemSessaoJulgamento();

        self::$arrObjVotoParteDTO[$numIdItemSessaoJulgamento][$numIdJulgamentoParte][$numIdUsuario]=$objVotoParteDTO;
        if($objVotoParteDTO->getNumIdSessaoVoto()!=$numIdSessaoJulgamento){
          $arrIdItensComVotoAnterior[$numIdItemSessaoJulgamento]=$numIdItemSessaoJulgamento;
          if(!isset($arrObjColegiadoComposicaoDTO[$numIdUsuario]) || ($arrObjColegiadoComposicaoDTO[$numIdUsuario]->isSetBolComposicaoAnterior() && $arrObjColegiadoComposicaoDTO[$numIdUsuario]->getBolComposicaoAnterior()=='S')){
            $arrIdItensComVotoComposicaoAnterior[$numIdItemSessaoJulgamento][$numIdUsuario]=1;
            CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($objVotoParteDTO->getNumIdSessaoVoto());
          }
        } else if($objVotoParteDTO->getStrStaVotoParte()!=VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO){
            $arrIdItensComVotoSessaoAtual[$numIdItemSessaoJulgamento]=1;
          }
        if($objVotoParteDTO->getStrSinVencedor()=='S'){
          $arrUsuariosVencedores[$numIdJulgamentoParte]=$numIdUsuario;
        }
      }
//-----fim processar---------------------
      foreach ($arrPesquisa as $numIdItemSessaoJulgamento) {
        self::getArrObjJulgamentoParteDTO($numIdItemSessaoJulgamento);
        $objItemSessaoJulgamentoDTO=self::$arrObjItemSessaoJulgamentoDTO[$numIdItemSessaoJulgamento];
        $objItemSessaoJulgamentoDTO->setStrSinPossuiVotos(isset($arrIdItensComVotoSessaoAtual[$numIdItemSessaoJulgamento])?'S':'N');//vota��o iniciada na sess�o atual
        $objItemSessaoJulgamentoDTO->setStrSinPossuiVotosAnteriores(isset($arrIdItensComVotoAnterior[$numIdItemSessaoJulgamento])?'S':'N');//vota��o iniciada na sess�o atual
        $objItemSessaoJulgamentoDTO->setNumVotosComposicaoAnterior(isset($arrIdItensComVotoComposicaoAnterior[$numIdItemSessaoJulgamento])?count($arrIdItensComVotoComposicaoAnterior[$numIdItemSessaoJulgamento]):0);//vota��o iniciada na sess�o atual

        $ret[$numIdItemSessaoJulgamento]=self::$arrObjVotoParteDTO[$numIdItemSessaoJulgamento];
        foreach ( self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento] as $numIdJulgamentoParte =>$objJulgamentoParteDTO) {
          if(!isset(self::$arrObjVotoParteDTO[$numIdItemSessaoJulgamento][$numIdJulgamentoParte])){
            self::$arrObjVotoParteDTO[$numIdItemSessaoJulgamento][$numIdJulgamentoParte]=array();
          }
          $objJulgamentoParteDTO->setArrObjVotoParteDTO(self::$arrObjVotoParteDTO[$numIdItemSessaoJulgamento][$numIdJulgamentoParte]);
          if(isset($arrUsuariosVencedores[$numIdJulgamentoParte])){
            $objJulgamentoParteDTO->setNumUsuarioVencedor($arrUsuariosVencedores[$numIdJulgamentoParte]);
          } else {
            $objJulgamentoParteDTO->setNumUsuarioVencedor(null);
          }
        }
      }
    }
    return $ret;
  }

  public static function  clearArrObjVotoParteDTO($varIdItemSessaoJulgamento){
    if (!is_array($varIdItemSessaoJulgamento)) {
      $varIdItemSessaoJulgamento = array($varIdItemSessaoJulgamento);
    }
    foreach ($varIdItemSessaoJulgamento as $numIdItemSessaoJulgamento) {
      unset(self::$arrObjVotoParteDTO[$numIdItemSessaoJulgamento]);
      if(isset(self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento])){
        foreach (self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento] as $objJulgamentoParteDTO) {
          $objJulgamentoParteDTO->unSetArrObjVotoParteDTO();
          $objJulgamentoParteDTO->unSetArrObjResumoVotacaoDTO();
        }

      }
    }

  }
  public static function  clearArrObjJulgamentoParteDTO($varIdItemSessaoJulgamento){
    if (!is_array($varIdItemSessaoJulgamento)) {
      $varIdItemSessaoJulgamento = array($varIdItemSessaoJulgamento);
    }
    foreach ($varIdItemSessaoJulgamento as $numIdItemSessaoJulgamento) {
      if(isset(self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento])){
        foreach (self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento] as $key=>$objJulgamentoParteDTO) {
          unset(self::$arrObjJulgamentoParteDTO[$key]);
        }
        unset(self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento]);
      }
    }
  }
  public static function  clearArrObjItemSessaoJulgamentoDTO($varIdItemSessaoJulgamento){
    if (!is_array($varIdItemSessaoJulgamento)) {
      $varIdItemSessaoJulgamento = array($varIdItemSessaoJulgamento);
    }
    foreach ($varIdItemSessaoJulgamento as $numIdItemSessaoJulgamento) {
      unset(self::$idxObjJulgamentoParteDTO[$numIdItemSessaoJulgamento]);
    }
  }


}
?>