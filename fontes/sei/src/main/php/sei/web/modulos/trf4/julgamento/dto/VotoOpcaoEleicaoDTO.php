<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 19/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class VotoOpcaoEleicaoDTO extends InfraDTO {

  private $numTipoFkVotoEleicao = null;

  public function __construct(){
    $this->numTipoFkVotoEleicao = InfraDTO::$TIPO_FK_OPCIONAL;
    parent::__construct();
  }

  public function getStrNomeTabela() {
  	 return 'voto_opcao_eleicao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdVotoOpcaoEleicao', 'id_voto_opcao_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdVotoEleicao', 'id_voto_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdOpcaoEleicao', 'id_opcao_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Ordem', 'ordem');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdEleicaoOpcaoEleicao', 'id_eleicao', 'opcao_eleicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'IdentificacaoOpcaoEleicao', 'identificacao', 'opcao_eleicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUsuarioVotoEleicao', 'id_usuario', 'voto_eleicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'OrdemVotoEleicao', 'ordem', 'voto_eleicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SiglaUsuario', 'sigla', 'usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeUsuario', 'nome', 'usuario');

    $this->configurarPK('IdVotoOpcaoEleicao',InfraDTO::$TIPO_PK_NATIVA);
    $this->configurarFK('IdOpcaoEleicao', 'opcao_eleicao', 'id_opcao_eleicao');
    $this->configurarFK('IdVotoEleicao', 'voto_eleicao', 'id_voto_eleicao', $this->getNumTipoFkVotoEleicao());
    $this->configurarFK('IdUsuarioVotoEleicao', 'usuario', 'id_usuario');
  }

  public function getNumTipoFkVotoEleicao(){
    return $this->numTipoFkVotoEleicao;
  }

  public function setNumTipoFkVotoEleicao($numTipoFkVotoEleicao){
    $this->numTipoFkVotoEleicao = $numTipoFkVotoEleicao;
  }

}
