<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class DestaqueDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'destaque';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdDestaque','id_destaque');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdItemSessaoJulgamento','id_item_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidade','id_unidade');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Descricao','descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaAcesso','sta_acesso');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaTipo','sta_tipo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Destaque','dth_destaque');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinBloqueado','sin_bloqueado');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUsuario','sigla','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamentoItem','id_sessao_julgamento','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdDistribuicao','id_distribuicao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdSessaoBlocoItem','id_sessao_bloco','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaTipoItemSessaoBloco','sta_tipo_item','sessao_bloco');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoSessaoJulgamento','id_colegiado','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DTH,'SessaoSessaoJulgamento','dth_sessao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaSituacaoSessaoJulgamento','sta_situacao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdTipoSessaoSessaoJulgamento','id_tipo_sessao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUsuarioRelDestaqueUsuario','id_usuario','rel_destaque_usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DBL,'IdProcedimentoDistribuicao','id_procedimento','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SinVirtualTipoSessao','sin_virtual','tipo_sessao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinVisualizado');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'StaTipoVisualizacao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinForcarExclusao');

    $this->configurarFK('IdUnidade','unidade','id_unidade');
    $this->configurarFK('IdUsuario','usuario','id_usuario');
    $this->configurarFK('IdItemSessaoJulgamento','item_sessao_julgamento','id_item_sessao_julgamento');
    $this->configurarFK('IdDistribuicao','distribuicao','id_distribuicao');
    $this->configurarFK('IdSessaoJulgamentoItem','sessao_julgamento','id_sessao_julgamento');
    $this->configurarFK('IdTipoSessaoSessaoJulgamento','tipo_sessao','id_tipo_sessao');
    $this->configurarFK('IdSessaoBlocoItem','sessao_bloco','id_sessao_bloco');
    $this->configurarFK('IdDestaque','rel_destaque_usuario','id_destaque',InfraDTO::$TIPO_FK_OPCIONAL);

    $this->configurarPK('IdDestaque',InfraDTO::$TIPO_PK_NATIVA);

  }
}
?>