<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 25/01/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__.'/../../../../SEI.php';

class ItemSessaoDocumentoRN extends InfraRN {

  private $numRegistros=0;
  private $arrDTO;

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdItemSessaoJulgamento(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Item de sess�o n�o informado.');
    }
  }
  private function validarNumIdUsuario(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoDocumentoDTO->getNumIdUsuario())){
      $objItemSessaoDocumentoDTO->setNumIdUsuario(null);
    }
  }
  private function validarNumIdUnidade(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoDocumentoDTO->getNumIdUsuario())){
      $objItemSessaoDocumentoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    }
  }
  private function validarDblIdDocumento(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO, InfraException $objInfraException){
    if (!InfraString::isBolVazia($objItemSessaoDocumentoDTO->getDblIdDocumento())) {
      $staItem=$objItemSessaoDocumentoDTO->getStrStaSituacaoItem();
      if ($staItem == TipoSessaoBlocoRN::$STA_PAUTA || $staItem == TipoSessaoBlocoRN::$STA_MESA) {
        $objInfraParametro = new InfraParametro($this->getObjInfraIBanco());
        $objDocumentoDTO = new DocumentoDTO();
        $objDocumentoDTO->setDblIdDocumento($objItemSessaoDocumentoDTO->getDblIdDocumento());
        $objDocumentoDTO->setNumIdSerie(array_filter(explode(',', $objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_DOCUMENTOS_DISPONIBILIZAVEIS).',')), InfraDTO::$OPER_IN);
        $objDocumentoRN = new DocumentoRN();
        if ($objDocumentoRN->contarRN0007($objDocumentoDTO) != 1) {
          $objInfraException->adicionarValidacao('Documento n�o encontrado ou n�o permitido');
        }
      } else {
        $objDocumentoDTO = new DocumentoDTO();
        $objDocumentoDTO->setDblIdDocumento($objItemSessaoDocumentoDTO->getDblIdDocumento());
        $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_EDITOR_INTERNO);
        $objDocumentoDTO->retDblIdDocumento();
//        $objDocumentoDTO->retArrObjAssinaturaDTO();
        $objDocumentoRN = new DocumentoRN();
        $objDocumentoDTO = $objDocumentoRN->consultarRN0005($objDocumentoDTO);
        if ($objDocumentoDTO == null) {
          $objInfraException->adicionarValidacao('Documento n�o encontrado ou n�o � do editor interno.');
        }
      }
    }
  }

  protected function cadastrarControlado(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_cadastrar',__METHOD__,$objItemSessaoDocumentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO, $objInfraException);
      $this->validarNumIdUsuario($objItemSessaoDocumentoDTO, $objInfraException);
      $this->validarNumIdUnidade($objItemSessaoDocumentoDTO, $objInfraException);
      $this->validarDblIdDocumento($objItemSessaoDocumentoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      $ret = $objItemSessaoDocumentoBD->cadastrar($objItemSessaoDocumentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando documento.',$e);
    }
  }
  protected function alterarControlado(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_alterar',__METHOD__,$objItemSessaoDocumentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objItemSessaoDocumentoDTO->isSetNumIdItemSessaoJulgamento()){
        $this->validarNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO, $objInfraException);
      }
      if ($objItemSessaoDocumentoDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objItemSessaoDocumentoDTO, $objInfraException);
      }
      if ($objItemSessaoDocumentoDTO->isSetNumIdUnidade()){
        $this->validarNumIdUnidade($objItemSessaoDocumentoDTO, $objInfraException);
      }
      if ($objItemSessaoDocumentoDTO->isSetDblIdDocumento()){
        $this->validarDblIdDocumento($objItemSessaoDocumentoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      $objItemSessaoDocumentoBD->alterar($objItemSessaoDocumentoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando documento.',$e);
    }
  }
  protected function excluirControlado($arrObjItemSessaoDocumentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_excluir',__METHOD__,$arrObjItemSessaoDocumentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
        $objItemSessaoDocumentoBD->excluir($objItemSessaoDocumentoDTO);
      }

      if(InfraArray::contar($arrObjItemSessaoDocumentoDTO)){
        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_DOCBLOQ');
      }
      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo documento.',$e);
    }
  }
  protected function consultarConectado(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_consultar',__METHOD__,$objItemSessaoDocumentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      $ret = $objItemSessaoDocumentoBD->consultar($objItemSessaoDocumentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando documento.',$e);
    }
  }
  protected function listarConectado(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO) {
    try {

      //Valida Permissao
      //SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_listar',__METHOD__,$objItemSessaoDocumentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      $ret = $objItemSessaoDocumentoBD->listar($objItemSessaoDocumentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando documentos.',$e);
    }
  }
  protected function contarConectado(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_listar',__METHOD__,$objItemSessaoDocumentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      $ret = $objItemSessaoDocumentoBD->contar($objItemSessaoDocumentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando documentos.',$e);
    }
  }

  public function acumular($objItemSessaoDocumentoDTO){
    try{

      if ($objItemSessaoDocumentoDTO!=null){
        $this->arrDTO[]=$objItemSessaoDocumentoDTO;
        $this->numRegistros++;
      }

      if ($this->numRegistros>49 || ($this->numRegistros>0 && $objItemSessaoDocumentoDTO==null)){
        $objItemSessaoDocumentoBD=new ItemSessaoDocumentoBD(self::getObjInfraIBanco());
        $objItemSessaoDocumentoBD->cadastrar($this->arrDTO);
        $this->numRegistros=0;
        $this->arrDTO=array();
      }

    }catch(Exception $e){
      throw new InfraException('Erro acumulando registros de item_sessao_documento para grava��o.',$e);
    }
  }

  protected function verificaDocumentoBloqueadoConectado(DocumentoAPI $objDocumentoAPI){

    try{

      $strCache = 'TRF4_SJ_DOCBLOQ';
      $arrCache = CacheSEI::getInstance()->getAtributo($strCache);

      if ($arrCache == null) {

        $arrCache = array();

        $objItemSessaoDocumentoDTO=new ItemSessaoDocumentoDTO();
        $objItemSessaoDocumentoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_FINALIZADA),InfraDTO::$OPER_NOT_IN);
        $objItemSessaoDocumentoDTO->setStrStaSituacaoItem(array(ItemSessaoJulgamentoRN::$STA_RETIRADO,ItemSessaoJulgamentoRN::$STA_IGNORADA),InfraDTO::$OPER_NOT_IN);
        $objItemSessaoDocumentoDTO->retDblIdDocumento();
        $objItemSessaoDocumentoDTO->setDistinct(true);

        $arrObjItemSessaoDocumentoDTO=$this->listar($objItemSessaoDocumentoDTO);
        foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
          $arrCache[] = $objItemSessaoDocumentoDTO->getDblIdDocumento();
        }


        CacheSEI::getInstance()->setAtributo($strCache, $arrCache, 300);
      }

      return in_array($objDocumentoAPI->getIdDocumento(),$arrCache);

    }catch(Exception $e){
      throw new InfraException('Erro consultando documentos em uso pela sess�o de julgamento. ',$e);
    }

  }

  protected function indisponibilizarControlado(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO) {

    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_excluir',__METHOD__,$objItemSessaoDocumentoDTO);

      $objInfraException = new InfraException();

      $objItemSessaoDocumentoDTOBanco=new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoDTOBanco->setNumIdItemSessaoDocumento($objItemSessaoDocumentoDTO->getNumIdItemSessaoDocumento());
      $objItemSessaoDocumentoDTOBanco->retDblIdDocumento();
      $objItemSessaoDocumentoDTOBanco->retNumIdUsuario();
      $objItemSessaoDocumentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoDocumentoDTOBanco->retStrStaSituacaoItem();
      $objItemSessaoDocumentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoDocumentoDTOBanco->retNumIdSessaoJulgamentoItem();
      $objItemSessaoDocumentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoDocumentoDTOBanco->retDblIdProcedimentoDistribuicao();
      $objItemSessaoDocumentoDTOBanco->retStrProtocoloProcedimentoFormatado();
      $objItemSessaoDocumentoDTOBanco->retStrProtocoloDocumentoFormatado();
      $objItemSessaoDocumentoDTOBanco->retStrSinVirtualTipoSessao();
      $objItemSessaoDocumentoDTOBanco->retNumIdUnidadeSessaoItem();
      $objItemSessaoDocumentoDTOBanco=$this->consultar($objItemSessaoDocumentoDTOBanco);

      if ($objItemSessaoDocumentoDTOBanco == null) {
        $objInfraException->lancarValidacao('Documento n�o encontrado ou n�o disponibilizado.');
      }

      if (!in_array($objItemSessaoDocumentoDTOBanco->getStrStaSituacaoSessaoJulgamento(), array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA))) {
        $objInfraException->lancarValidacao('N�o � poss�vel cancelar a disponibiliza��o de documento para esta sess�o.');
      }

      if ($objItemSessaoDocumentoDTOBanco->getStrStaSituacaoItem() != ItemSessaoJulgamentoRN::$STA_NORMAL) {
        $objVotoParteDTO=new VotoParteDTO();
        $objVotoParteRN=new VotoParteRN();
        $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTOBanco->getNumIdItemSessaoJulgamento());
        $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoDocumentoDTOBanco->getNumIdSessaoJulgamentoItem());
        $bolVotacaoIniciada=$objVotoParteRN->contar($objVotoParteDTO);

        if ($bolVotacaoIniciada>0 || $objItemSessaoDocumentoDTOBanco->getStrStaSituacaoItem() != ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA) {
          $objInfraException->lancarValidacao('N�o � poss�vel cancelar a disponibiliza��o de documento deste processo.');
        }
      }

      $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
      $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objItemSessaoDocumentoDTOBanco->getNumIdColegiadoSessaoJulgamento());
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->retNumIdUsuario();
      $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
      if($arrObjColegiadoComposicaoDTO==null){
        $objInfraException->lancarValidacao('Unidade atual n�o faz parte do Colegiado.');
      }

      //desbloquear documento
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_DOCBLOQ');


      //desmarcar revis�es
      $objRevisaoItemDTO=new RevisaoItemDTO();
      $objRevisaoItemRN=new RevisaoItemRN();
      $objRevisaoItemDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTOBanco->getNumIdItemSessaoJulgamento());
      $objRevisaoItemDTO->retNumIdRevisaoItem();
      $arrObjRevisaoItemDTO=$objRevisaoItemRN->listar($objRevisaoItemDTO);
      if(count($arrObjRevisaoItemDTO)){
        $objRevisaoItemRN->excluir($arrObjRevisaoItemDTO);
      }

//      $objBloqueioItemSessUnidadeDTO=new BloqueioItemSessUnidadeDTO();
//      $objBloqueioItemSessUnidadeDTO->setDblIdDocumento($objItemSessaoDocumentoDTO->getDblIdDocumento());
//      $objBloqueioItemSessUnidadeDTO->retNumIdBloqueioItemSessUnidade();
//      $objBloqueioItemSessUnidadeRN=new BloqueioItemSessUnidadeRN();
//      $arrObjBloqueioItemSessUnidadeDTO=$objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);
//      if(count($arrObjBloqueioItemSessUnidadeDTO)){
//        $objBloqueioItemSessUnidadeRN->excluir($arrObjBloqueioItemSessUnidadeDTO);
//      }

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      $objItemSessaoDocumentoBD->excluir($objItemSessaoDocumentoDTO);


      //retirar votos de 'provimento disponibilizado'
      $objVotoParteDTO=new VotoParteDTO();
      $objVotoParteRN=new VotoParteRN();
      $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTOBanco->getNumIdItemSessaoJulgamento());
      $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO);
      $objVotoParteDTO->setNumIdUsuario($objItemSessaoDocumentoDTOBanco->getNumIdUsuario());
      $objVotoParteDTO->retNumIdVotoParte();
      $arrObjVotoParteDTO=$objVotoParteRN->listar($objVotoParteDTO);
      if (InfraArray::contar($arrObjVotoParteDTO)){
        $objVotoParteRN->excluir($arrObjVotoParteDTO);
      }

      $objAndamentoSessaoDTO=new AndamentoSessaoDTO();
      $objAndamentoSessaoRN=new AndamentoSessaoRN();
      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoDocumentoDTOBanco->getNumIdSessaoJulgamentoItem());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_CANCELADA_DISPONIBILIZACAO_DOCUMENTO);
      $arrObjAtributoAndamentoSessaoDTO=array();
      $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoDocumentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoDocumentoDTOBanco->getStrProtocoloProcedimentoFormatado());
      $arrObjAtributoAndamentoSessaoDTO[]=$objAtributoAndamentoSessaoDTO;

      $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('DOCUMENTO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoDocumentoDTOBanco->getDblIdDocumento());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoDocumentoDTOBanco->getStrProtocoloDocumentoFormatado());
      $arrObjAtributoAndamentoSessaoDTO[]=$objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

    } catch(Exception $e) {
      throw new InfraException('Erro cancelando disponibiliza��o de documento para a Sess�o.', $e);
    }

  }
  protected function disponibilizarControlado(ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO) {

    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_documento_cadastrar', __METHOD__, $objItemSessaoDocumentoDTO);

      $objInfraException = new InfraException();

      $this->validarNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO, $objInfraException);
      $this->validarNumIdUsuario($objItemSessaoDocumentoDTO, $objInfraException);
      $this->validarNumIdUnidade($objItemSessaoDocumentoDTO, $objInfraException);
      $objInfraException->lancarValidacoes();


      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUnidadeRelatorDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioSessao();
      $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrStaDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrSinVirtualTipoSessao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaTipoItemSessaoBloco();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objItemSessaoJulgamentoDTOBanco = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTOBanco);

      if ($objItemSessaoJulgamentoDTOBanco == null) {
        $objInfraException->lancarValidacao('Item da sess�o de julgamento n�o encontrado.');
      }

      $bolSessaoVirtual=$objItemSessaoJulgamentoDTOBanco->getStrSinVirtualTipoSessao()=='S';


      if (!in_array($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento(), array(SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_SUSPENSA))) {
        $objInfraException->lancarValidacao('N�o � poss�vel disponibilizar documento para esta sess�o.');
      }
      if($bolSessaoVirtual && in_array($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento(),[SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA])){
        $objInfraException->lancarValidacao('N�o � mais poss�vel disponibilizar o documento nesta sess�o.');
      }
      if (!in_array($objItemSessaoJulgamentoDTOBanco->getStrStaDistribuicao(), array(DistribuicaoRN::$STA_EM_MESA, DistribuicaoRN::$STA_PARA_REFERENDO, DistribuicaoRN::$STA_PAUTADO))) {
        $objInfraException->lancarValidacao('Situa��o da Distribui��o n�o permite disponibiliza��o de documento.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaDistribuicao() != DistribuicaoRN::$STA_PARA_REFERENDO) {
        $objItemSessaoDocumentoDTOBanco = new ItemSessaoDocumentoDTO();
        $objItemSessaoDocumentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
        $objItemSessaoDocumentoDTOBanco->setNumIdUsuario($objItemSessaoDocumentoDTO->getNumIdUsuario());
        if ($this->contar($objItemSessaoDocumentoDTOBanco) > 0) {
          $objInfraException->lancarValidacao('J� existe documento deste processo disponibilizado para a sess�o para este membro.');
        }

        //validar se documento � dos tipos de documento da sess�o e se � gerado pela unidade atual
        $objInfraParametro=new InfraParametro(BancoSEI::getInstance());
        $arrIdTipoDocumentosPermitidos=explode(',',$objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_DOCUMENTOS_DISPONIBILIZAVEIS));
        $objDocumentoDTO=new DocumentoDTO();
        $objDocumentoDTO->setDblIdDocumento($objItemSessaoDocumentoDTO->getDblIdDocumento());
        $objDocumentoDTO->retNumIdSerie();
        $objDocumentoDTO->retDblIdDocumento();
        $objDocumentoDTO->retNumIdUnidadeGeradoraProtocolo();
        $objDocumentoRN=new DocumentoRN();
        $objDocumentoDTO=$objDocumentoRN->consultarRN0005($objDocumentoDTO);
        if(!$objDocumentoDTO){
          $objInfraException->lancarValidacao('Documento n�o encontrado.');
        }
        if($objDocumentoDTO->getNumIdUnidadeGeradoraProtocolo()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()){
          $objInfraException->lancarValidacao('N�o � poss�vel disponibilizar documento gerado por outra unidade.');
        }
        if(!in_array($objDocumentoDTO->getNumIdSerie(),$arrIdTipoDocumentosPermitidos)){
          $objInfraException->lancarValidacao('Este tipo de documento n�o pode ser disponibilizado para a sess�o.');
        }


      } else {
        if($objItemSessaoDocumentoDTO->getNumIdUsuario()!=$objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioRelatorDistribuicao()){
          $objInfraException->lancarValidacao('Somente relator pode disponibilizar documentos para referendo.');
        }
      }


      $objItemSessaoDocumentoDTOBanco = new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoDocumentoDTOBanco->setDblIdDocumento($objItemSessaoDocumentoDTO->getDblIdDocumento());
      if ($this->contar($objItemSessaoDocumentoDTOBanco) > 0) {
        $objInfraException->lancarValidacao('Documento j� disponibilizado nesta sess�o.');
      }


      $objItemSessaoDocumentoDTO->setStrStaSituacaoItem($objItemSessaoJulgamentoDTOBanco->getStrStaSituacao());
      $this->validarDblIdDocumento($objItemSessaoDocumentoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objItemSessaoDocumentoBD = new ItemSessaoDocumentoBD($this->getObjInfraIBanco());
      $objItemSessaoDocumentoBD->cadastrar($objItemSessaoDocumentoDTO);

      //desmarcar revis�es
      $objRevisaoItemDTO=new RevisaoItemDTO();
      $objRevisaoItemRN=new RevisaoItemRN();
      $objRevisaoItemDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
      $objRevisaoItemDTO->retNumIdRevisaoItem();
      $arrObjRevisaoItemDTO=$objRevisaoItemRN->listar($objRevisaoItemDTO);
      if(count($arrObjRevisaoItemDTO)){
        $objRevisaoItemRN->excluir($arrObjRevisaoItemDTO);
      }

//      $arrObjBloqueioItemSessUnidadeDTO=$objItemSessaoDocumentoDTO->getArrObjBloqueioItemSessUnidadeDTO();

//      $objBloqueioItemSessUnidadeRN=new BloqueioItemSessUnidadeRN();
//      /** @var BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO */
//      foreach ($arrObjBloqueioItemSessUnidadeDTO as $objBloqueioItemSessUnidadeDTO) {
//        $objBloqueioItemSessUnidadeDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
//        $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_DOCUMENTO);
//        $objBloqueioItemSessUnidadeDTO->setNumIdItemSessaoDocumento($objItemSessaoDocumentoDTO->getNumIdItemSessaoDocumento());
//        $objBloqueioItemSessUnidadeRN->cadastrar($objBloqueioItemSessUnidadeDTO);
//      }

      $arrObjVotoParteDTO = $objItemSessaoDocumentoDTO->getArrObjVotoParteDTO();
      if (InfraArray::contar($arrObjVotoParteDTO) > 0) {
        //n�o pode ter voto j� lan�ado na sessao atual
        $objVotoParteDTO = new VotoParteDTO();
        $objVotoParteRN = new VotoParteRN();
        $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
        $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
        $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO, InfraDTO::$OPER_DIFERENTE);
        if ($objVotoParteRN->contar($objVotoParteDTO) != 0) {
          $objInfraException->lancarValidacao('J� existe voto lan�ado, n�o � permitido selecionar o provimento.');
        }

        $objJulgamentoParteDTO = new JulgamentoParteDTO();
        $objJulgamentoParteRN = new JulgamentoParteRN();
        $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
        $objJulgamentoParteDTO->retNumIdJulgamentoParte();
        $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);

        if(InfraArray::contar($arrObjJulgamentoParteDTO)!=InfraArray::contar($arrObjVotoParteDTO)){
          $objInfraException->lancarValidacao('Erro na quantidade de provimentos indicados.');
        }

        if(InfraArray::contar($arrObjVotoParteDTO)==1){
          $arrObjVotoParteDTO[0]->setNumIdJulgamentoParte($arrObjJulgamentoParteDTO[0]->getNumIdJulgamentoParte());
        }
        foreach ($arrObjVotoParteDTO as $objVotoParteDTOPar) {
          //lan�ar voto do relator: indicando provimento
          $objVotoParteDTO = new VotoParteDTO();
          $objVotoParteDTO->setNumIdVotoParteAssociado(null);
          $objVotoParteDTO->setStrRessalva(null);
          $objVotoParteDTO->setNumIdProvimento($objVotoParteDTOPar->getNumIdProvimento());
          $objVotoParteDTO->setStrComplemento($objVotoParteDTOPar->getStrComplemento());
          $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO);
          $objVotoParteDTO->setDthVoto(InfraData::getStrDataHoraAtual());
          $objVotoParteDTO->setNumIdUsuario($objItemSessaoDocumentoDTO->getNumIdUsuario());
          $objVotoParteDTO->setNumIdJulgamentoParte($objVotoParteDTOPar->getNumIdJulgamentoParte());
          $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
          $objVotoParteDTO->setStrSinVencedor('N');
          $objVotoParteRN->registrar($objVotoParteDTO);
        }
      } else {
        $objJulgamentoParteDTO = new JulgamentoParteDTO();
        $objJulgamentoParteRN = new JulgamentoParteRN();
        $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoDocumentoDTO->getNumIdItemSessaoJulgamento());
        $objJulgamentoParteDTO->retNumIdJulgamentoParte();
        $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
        if (InfraArray::contar($arrObjJulgamentoParteDTO) == 1) {
          //obrigat�rio lan�ar provimento
          $objVotoParteDTO = new VotoParteDTO();
          $objVotoParteRN = new VotoParteRN();
          $objVotoParteDTO->setNumIdUsuario($objItemSessaoDocumentoDTO->getNumIdUsuario());
          $objVotoParteDTO->setNumIdJulgamentoParte($arrObjJulgamentoParteDTO[0]->getNumIdJulgamentoParte());
          if ($objVotoParteRN->contar($objVotoParteDTO) == 0) {
            $objInfraException->lancarValidacao('� necess�rio selecionar o provimento.');
          }
        }
      }


      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();
      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_DOCUMENTO_DISPONIBILIZADO);
      $arrObjAtributoAndamentoSessaoDTO = array();
      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objProtocoloDTO = new ProtocoloDTO();
      $objProtocoloDTO->retStrProtocoloFormatado();
      $objProtocoloDTO->setDblIdProtocolo($objItemSessaoDocumentoDTO->getDblIdDocumento());

      $objProtocoloRN = new ProtocoloRN();
      $objProtocoloDTO = $objProtocoloRN->consultarRN0186($objProtocoloDTO);


      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('DOCUMENTO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoDocumentoDTO->getDblIdDocumento());
      $objAtributoAndamentoSessaoDTO->setStrValor($objProtocoloDTO->getStrProtocoloFormatado());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);
      //bloquear documento
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_DOCBLOQ');

    } catch(Exception $e) {
      throw new InfraException('Erro disponibilizando documento para a Sess�o.', $e);
    }

  }

}
?>