<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PresencaSessaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'presenca_sessao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUsuario', 'id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdSessaoJulgamento', 'id_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdPresencaSessao', 'id_presenca_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'Entrada', 'dth_entrada');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'Saida', 'dth_saida');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUsuarioPresidenteSessaoJulgamento', 'id_sessao_julgamento', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoSessaoJulgamento','id_colegiado','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaSituacaoSessaoJulgamento','sta_situacao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DTH,'InicioSessaoJulgamento','dth_inicio','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DTH,'SessaoSessaoJulgamento','dth_sessao','sessao_julgamento');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinPresente');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'NumIdUsuario');

    $this->configurarPK('IdPresencaSessao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdSessaoJulgamento', 'sessao_julgamento', 'id_sessao_julgamento');
    $this->configurarFK('IdUsuario', 'usuario', 'id_usuario');
  }
}
?>