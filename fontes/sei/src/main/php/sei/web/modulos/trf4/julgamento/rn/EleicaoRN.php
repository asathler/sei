<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class EleicaoRN extends InfraRN {

  public static $SE_CRIADA = 'C';
  public static $SE_LIBERADA = 'L';
  public static $SE_CONCLUIDA = 'U';
  public static $SE_FINALIZADA = 'F';
  public static $SE_ANULADA = 'A';

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  public function listarValoresSituacao(){
    try {

      $arrObjSituacaoEleicaoDTO = array();

      $objSituacaoEleicaoDTO = new SituacaoEleicaoDTO();
      $objSituacaoEleicaoDTO->setStrStaSituacao(self::$SE_CRIADA);
      $objSituacaoEleicaoDTO->setStrDescricao('Criado');
      $arrObjSituacaoEleicaoDTO[] = $objSituacaoEleicaoDTO;

      $objSituacaoEleicaoDTO = new SituacaoEleicaoDTO();
      $objSituacaoEleicaoDTO->setStrStaSituacao(self::$SE_LIBERADA);
      $objSituacaoEleicaoDTO->setStrDescricao('Liberado');
      $arrObjSituacaoEleicaoDTO[] = $objSituacaoEleicaoDTO;

      $objSituacaoEleicaoDTO = new SituacaoEleicaoDTO();
      $objSituacaoEleicaoDTO->setStrStaSituacao(self::$SE_CONCLUIDA);
      $objSituacaoEleicaoDTO->setStrDescricao('Conclu�do');
      $arrObjSituacaoEleicaoDTO[] = $objSituacaoEleicaoDTO;

      $objSituacaoEleicaoDTO = new SituacaoEleicaoDTO();
      $objSituacaoEleicaoDTO->setStrStaSituacao(self::$SE_FINALIZADA);
      $objSituacaoEleicaoDTO->setStrDescricao('Finalizado');
      $arrObjSituacaoEleicaoDTO[] = $objSituacaoEleicaoDTO;

      $objSituacaoEleicaoDTO = new SituacaoEleicaoDTO();
      $objSituacaoEleicaoDTO->setStrStaSituacao(self::$SE_ANULADA);
      $objSituacaoEleicaoDTO->setStrDescricao('Anulado');
      $arrObjSituacaoEleicaoDTO[] = $objSituacaoEleicaoDTO;

      return $arrObjSituacaoEleicaoDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de Situa��o.',$e);
    }
  }

  private function validarNumIdItemSessaoJulgamento(EleicaoDTO $objEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objEleicaoDTO->getNumIdItemSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Processo n�o informado.');
    }
  }

  private function validarStrIdentificacao(EleicaoDTO $objEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objEleicaoDTO->getStrIdentificacao())){
      $objInfraException->adicionarValidacao('Identifica��o n�o informada.');
    }else{
      $objEleicaoDTO->setStrIdentificacao(trim($objEleicaoDTO->getStrIdentificacao()));

      if (strlen($objEleicaoDTO->getStrIdentificacao())>250){
        $objInfraException->adicionarValidacao('Identifica��o possui tamanho superior a 250 caracteres.');
      }

      $dto = new EleicaoDTO();
      $dto->retNumIdEleicao();
      $dto->setNumMaxRegistrosRetorno(1);
      $dto->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao(),InfraDTO::$OPER_DIFERENTE);
      $dto->setNumIdItemSessaoJulgamento($objEleicaoDTO->getNumIdItemSessaoJulgamento());
      $dto->setStrIdentificacao($objEleicaoDTO->getStrIdentificacao());
      if ($this->consultar($dto)!=null){
        $objInfraException->lancarValidacao('J� existe um Escrut�nio Eletr�nico no processo com esta identifica��o.');
      }

    }
  }

  private function validarStrDescricao(EleicaoDTO $objEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objEleicaoDTO->getStrDescricao())){
      $objEleicaoDTO->setStrDescricao(null);
    }else{
      $objEleicaoDTO->setStrDescricao(trim($objEleicaoDTO->getStrDescricao()));

      if (strlen($objEleicaoDTO->getStrDescricao())>4000){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 4000 caracteres.');
      }
    }
  }

  private function validarStrStaSituacao(EleicaoDTO $objEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objEleicaoDTO->getStrStaSituacao())){
      $objInfraException->adicionarValidacao('Situa��o n�o informada.');
    }else{
      if (!in_array($objEleicaoDTO->getStrStaSituacao(),InfraArray::converterArrInfraDTO($this->listarValoresSituacao(),'StaSituacao'))){
        $objInfraException->adicionarValidacao('Situa��o inv�lida.');
      }
    }
  }

  private function validarNumQuantidade(EleicaoDTO $objEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objEleicaoDTO->getNumQuantidade())){
      $objInfraException->adicionarValidacao('N� de Escolhas n�o informado.');
    }
    if (!is_numeric($objEleicaoDTO->getNumQuantidade()) || $objEleicaoDTO->getNumQuantidade() <= 0){
      $objInfraException->adicionarValidacao('N� de Escolhas inv�lido.');
    }

    if ($objEleicaoDTO->getNumQuantidade() > InfraArray::contar($objEleicaoDTO->getArrObjOpcaoEleicaoDTO())){
      $objInfraException->adicionarValidacao('N� de Escolhas � maior que a quantidade de op��es.');
    }
  }

  private function validarNumOrdem(EleicaoDTO $objEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objEleicaoDTO->getNumOrdem())){
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }

  private function validarStrSinSecreta(EleicaoDTO $objEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objEleicaoDTO->getStrSinSecreta())){
      $objInfraException->adicionarValidacao('Sinalizador de Sess�o Secreta n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objEleicaoDTO->getStrSinSecreta())){
        $objInfraException->adicionarValidacao('Sinalizador de Sess�o Secreta inv�lido.');
      }
    }
  }

  protected function liberarControlado(EleicaoDTO $parObjEleicaoDTO) {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_liberar', __METHOD__, $parObjEleicaoDTO);

      $objInfraException = new InfraException();

      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoDTO = $this->bloquear($objEleicaoDTO);

      if ($objEleicaoDTO->getStrStaSituacao()!=EleicaoRN::$SE_CRIADA){
        $objInfraException->lancarValidacao('Situa��o do Escrut�nio Eletr�nico n�o consta como Criado.');
      }

      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objEleicaoDTO->getNumIdItemSessaoJulgamento());

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      if ($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){
        $objInfraException->lancarValidacao('Sess�o de julgamento n�o est� aberta.');
      }

      $objEleicaoDTOSituacao = new EleicaoDTO();
      $objEleicaoDTOSituacao->setStrStaSituacao(EleicaoRN::$SE_LIBERADA);
      $objEleicaoDTOSituacao->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $objEleicaoBD->alterar($objEleicaoDTOSituacao);

      $objEleicaoDTO->setStrStaSituacao(EleicaoRN::$SE_LIBERADA);
      $this->lancarAndamento($objEleicaoDTO);

    }catch (Exception $e){
      throw new InfraException('Erro liberando Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function concluirControlado(EleicaoDTO $parObjEleicaoDTO) {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_concluir', __METHOD__, $parObjEleicaoDTO);

      $objInfraException = new InfraException();

      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoDTO = $this->bloquear($objEleicaoDTO);

      if ($objEleicaoDTO->getStrStaSituacao()!=EleicaoRN::$SE_LIBERADA){
        $objInfraException->lancarValidacao('Situa��o do escrut�nio n�o consta como liberado.');
      }

      if ($objEleicaoDTO->getStrSinSecreta()=='S'){
        $objInfraException->lancarValidacao('N�o � poss�vel concluir um escrut�nio secreto.');
      }

      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objEleicaoDTO->getNumIdItemSessaoJulgamento());

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      if ($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){
        $objInfraException->lancarValidacao('Sess�o de julgamento n�o est� aberta.');
      }

      $objVotoEleicaoDTO = new VotoEleicaoDTO();
      $objVotoEleicaoDTO->retNumIdVotoEleicao();
      $objVotoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());

      $objVotoEleicaoRN = new VotoEleicaoRN();
      $arrObjVotoEleicaoDTO = $objVotoEleicaoRN->listar($objVotoEleicaoDTO);

      //gera ordem aleatoria de membros votantes para justificativa de voto na fase de conclusao
      $numVotos = count($arrObjVotoEleicaoDTO);
      $arrVotos = range(1, $numVotos);
      shuffle($arrVotos);

      for($i=0;$i<$numVotos;$i++){
        $arrObjVotoEleicaoDTO[$i]->setNumOrdem($arrVotos[$i]);
        $objVotoEleicaoRN->alterar($arrObjVotoEleicaoDTO[$i]);
      }

      $objEleicaoDTOSituacao = new EleicaoDTO();
      $objEleicaoDTOSituacao->setStrStaSituacao(EleicaoRN::$SE_CONCLUIDA);
      $objEleicaoDTOSituacao->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $objEleicaoBD->alterar($objEleicaoDTOSituacao);

      $objEleicaoDTO->setStrStaSituacao(EleicaoRN::$SE_CONCLUIDA);
      $this->lancarAndamento($objEleicaoDTO);


    }catch (Exception $e){
      throw new InfraException('Erro concluindo Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function finalizarControlado(EleicaoDTO $parObjEleicaoDTO) {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_finalizar', __METHOD__, $parObjEleicaoDTO);

      $objInfraException = new InfraException();

      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoDTO = $this->bloquear($objEleicaoDTO);

      if ($objEleicaoDTO->getStrSinSecreta()=='N') {
        if ($objEleicaoDTO->getStrStaSituacao() != EleicaoRN::$SE_CONCLUIDA) {
          $objInfraException->lancarValidacao('Situa��o do escrut�nio n�o consta como conclu�do.');
        }
      }else{
        if ($objEleicaoDTO->getStrStaSituacao() != EleicaoRN::$SE_LIBERADA) {
          $objInfraException->lancarValidacao('Situa��o do escrut�nio secreto n�o consta como liberado.');
        }
      }

      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objEleicaoDTO->getNumIdItemSessaoJulgamento());

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      if ($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){
        $objInfraException->lancarValidacao('Sess�o de julgamento n�o est� aberta.');
      }

      if ($objEleicaoDTO->getStrSinSecreta()=='S') {
        $objVotoOpcaoEleicaoDTO=new VotoOpcaoEleicaoDTO();
        $objVotoOpcaoEleicaoRN=new VotoOpcaoEleicaoRN();
        $objVotoOpcaoEleicaoDTO->setNumIdEleicaoOpcaoEleicao($parObjEleicaoDTO->getNumIdEleicao());
        $objVotoOpcaoEleicaoDTO->retTodos();
        $arrObjVotoOpcaoEleicaoDTO=$objVotoOpcaoEleicaoRN->listar($objVotoOpcaoEleicaoDTO);
        $objVotoOpcaoEleicaoRN->excluir($arrObjVotoOpcaoEleicaoDTO);
        shuffle($arrObjVotoOpcaoEleicaoDTO);
        foreach ($arrObjVotoOpcaoEleicaoDTO as $objVotoOpcaoEleicaoDTO) {
          $objVotoOpcaoEleicaoDTO->setNumIdVotoOpcaoEleicao(null);
          $objVotoOpcaoEleicaoRN->cadastrar($objVotoOpcaoEleicaoDTO);
        }
      }

      $objEleicaoDTOSituacao = new EleicaoDTO();
      $objEleicaoDTOSituacao->setStrStaSituacao(EleicaoRN::$SE_FINALIZADA);
      $objEleicaoDTOSituacao->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $objEleicaoBD->alterar($objEleicaoDTOSituacao);

      $objEleicaoDTO->setStrStaSituacao(EleicaoRN::$SE_FINALIZADA);
      $this->lancarAndamento($objEleicaoDTO);

    }catch (Exception $e){
      throw new InfraException('Erro finalizando Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function anularControlado(EleicaoDTO $parObjEleicaoDTO) {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_anular', __METHOD__, $parObjEleicaoDTO);

      $objInfraException = new InfraException();

      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoDTO = $this->bloquear($objEleicaoDTO);

      if ($objEleicaoDTO->getStrStaSituacao()==EleicaoRN::$SE_CRIADA){
        $objInfraException->lancarValidacao('Situa��o do escrut�nio consta como criado.');
      }

      $objEleicaoDTOSituacao = new EleicaoDTO();
      $objEleicaoDTOSituacao->setStrStaSituacao(EleicaoRN::$SE_ANULADA);
      $objEleicaoDTOSituacao->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $objEleicaoBD->alterar($objEleicaoDTOSituacao);

      $objEleicaoDTO->setStrStaSituacao(EleicaoRN::$SE_ANULADA);
      $this->lancarAndamento($objEleicaoDTO);


    }catch (Exception $e){
      throw new InfraException('Erro anulando Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function votarControlado(EleicaoDTO $parObjEleicaoDTO) {
    try{

      SessaoSEI::getInstance()->validarPermissao('eleicao_votar');

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objEleicaoDTO = $this->bloquear($objEleicaoDTO);

      if ($objEleicaoDTO == null){
        throw new InfraException('Escrut�nio Eletr�nico n�o encontrado.');
      }

      if ($objEleicaoDTO->getStrStaSituacao()!=self::$SE_LIBERADA){
        $objInfraException->lancarValidacao('Situa��o do escrut�nio n�o permite registro de votos.');
      }

      $arrObjOpcaoEleicaoDTO = $parObjEleicaoDTO->getArrObjOpcaoEleicaoDTO();
      $numOpcoesEscolhidas = InfraArray::contar($arrObjOpcaoEleicaoDTO);

      if ($numOpcoesEscolhidas == 0){
        $objInfraException->lancarValidacao('Nenhum op��o informada para registro do voto no escrut�nio.');
      }else if ($numOpcoesEscolhidas < $objEleicaoDTO->getNumQuantidade()){
        $objInfraException->lancarValidacao('A quantidade de op��es escolhidas ('.$numOpcoesEscolhidas.') � inferior a quantidade necess�ria para registro do voto no escrut�nio ('.$objEleicaoDTO->getNumQuantidade().').');
      }

      $objOpcaoEleicaoRN = new OpcaoEleicaoRN();

      $arrOpcoesEscolhidas = array();
      foreach($arrObjOpcaoEleicaoDTO as $objOpcaoEleicaoDTO){

        if (InfraString::isBolVazia($objOpcaoEleicaoDTO->getNumIdOpcaoEleicao())){
          $objInfraException->lancarValidacao('Op��o n�o informada.');
        }

        $dto = new OpcaoEleicaoDTO();
        $dto->retNumIdEleicao();
        $dto->retStrIdentificacao();
        $dto->setNumIdOpcaoEleicao($objOpcaoEleicaoDTO->getNumIdOpcaoEleicao());
        $dto = $objOpcaoEleicaoRN->consultar($dto);

        if ($dto == null){
          throw new InfraException('Op��o ['.$objOpcaoEleicaoDTO->getNumIdOpcaoEleicao().'] n�o encontrada.');
        }

        if (isset($arrOpcoesEscolhidas[$objOpcaoEleicaoDTO->getNumIdOpcaoEleicao()])){
          $objInfraException->lancarValidacao('Op��o "'.$dto->getStrIdentificacao().'" duplicada.');
        }

        $arrOpcoesEscolhidas[$objOpcaoEleicaoDTO->getNumIdOpcaoEleicao()] = true;

        if ($dto->getNumIdEleicao()!=$objEleicaoDTO->getNumIdEleicao()){
          $objInfraException->lancarValidacao('Op��o "'.$dto->getStrIdentificacao().'" n�o pertence ao escrut�nio "'.$objEleicaoDTO->getStrIdentificacao().'".');
        }
      }

      //recuperar sessao, distribuicao e colegiado
      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objEleicaoDTO->getNumIdItemSessaoJulgamento());

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      if ($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){
        $objInfraException->lancarValidacao('Sess�o de julgamento n�o est� aberta.');
      }


      //recuperar a unidade do membro que esta logado
      $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoDTO->retNumIdUnidade();
      $objColegiadoComposicaoDTO->retStrSiglaUnidade();
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());

      $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
      $objColegiadoComposicaoDTO = $objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);

      if ($objColegiadoComposicaoDTO == null){
        $objInfraException->lancarValidacao('Usu�rio '.SessaoSEI::getInstance()->getStrNomeUsuario().' n�o pertence ao colegiado.');
      }

      //verificar se a unidade do membro esta bloqueada no processo
      $objBloqueioItemSessUnidadeDTO = new BloqueioItemSessUnidadeDTO();
      $objBloqueioItemSessUnidadeDTO->retNumIdBloqueioItemSessUnidade();
      $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      $objBloqueioItemSessUnidadeDTO->setNumIdUnidade($objColegiadoComposicaoDTO->getNumIdUnidade());
      $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);

      $objBloqueioItemSessUnidadeRN = new BloqueioItemSessUnidadeRN();
      if ($objBloqueioItemSessUnidadeRN->consultar($objBloqueioItemSessUnidadeDTO)!=null){
        $objInfraException->lancarValidacao('Unidade '.$objColegiadoComposicaoDTO->getStrSiglaUnidade().' com bloqueio neste processo.');
      }

      //verificar se o membro esta presente na sessao
      $objPresencaSessaoDTO = new PresencaSessaoDTO();
      $objPresencaSessaoDTO->setNumMaxRegistrosRetorno(1);
      $objPresencaSessaoDTO->retNumIdPresencaSessao();
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objPresencaSessaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objPresencaSessaoDTO->setDthSaida(null);

      $objPresencaSessaoRN = new PresencaSessaoRN();
      if ($objPresencaSessaoRN->consultar($objPresencaSessaoDTO)==null){
        $objInfraException->lancarValidacao('Usu�rio n�o est� presente na sess�o.');
      }

      $objVotoEleicaoDTO = new VotoEleicaoDTO();
      $objVotoEleicaoDTO->retNumIdVotoEleicao();
      $objVotoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
      $objVotoEleicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());

      $objVotoEleicaoRN = new VotoEleicaoRN();
      $objVotoEleicaoDTO = $objVotoEleicaoRN->consultar($objVotoEleicaoDTO);

      if ($objVotoEleicaoDTO!=null){
        $objInfraException->lancarValidacao('Usu�rio '.SessaoSEI::getInstance()->getStrSiglaUsuario().' j� votou no escrut�nio "'.$objEleicaoDTO->getStrIdentificacao().'".');
      }

      $objVotoOpcaoEleicaoRN = new VotoOpcaoEleicaoRN();

      $objVotoEleicaoDTO = new VotoEleicaoDTO();
      $objVotoEleicaoDTO->setNumIdVotoEleicao(null);
      $objVotoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
      $objVotoEleicaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objVotoEleicaoDTO->setDthVotoEleicao(InfraData::getStrDataHoraAtual());
      $objVotoEleicaoDTO->setNumOrdem(0);
      $objVotoEleicaoDTO = $objVotoEleicaoRN->cadastrar($objVotoEleicaoDTO);

      $arrObjVotoOpcaoEleicaoDTO = array();
      $n = 1;
      foreach($arrObjOpcaoEleicaoDTO as $objOpcaoEleicaoDTO){
        $objVotoOpcaoEleicaoDTO = new VotoOpcaoEleicaoDTO();
        $objVotoOpcaoEleicaoDTO->setNumIdOpcaoEleicao($objOpcaoEleicaoDTO->getNumIdOpcaoEleicao());
        if ($objEleicaoDTO->getStrSinSecreta()=='S') {
          $objVotoOpcaoEleicaoDTO->setNumIdVotoEleicao(null);
        }else{
          $objVotoOpcaoEleicaoDTO->setNumIdVotoEleicao($objVotoEleicaoDTO->getNumIdVotoEleicao());
        }
        $objVotoOpcaoEleicaoDTO->setNumOrdem($n++);
        $arrObjVotoOpcaoEleicaoDTO[] = $objVotoOpcaoEleicaoDTO;
      }

      if ($objEleicaoDTO->getStrSinSecreta()=='S') {
        $objVotoOpcaoEleicaoDTO = new VotoOpcaoEleicaoDTO();
        $objVotoOpcaoEleicaoDTO->setNumIdEleicaoOpcaoEleicao($parObjEleicaoDTO->getNumIdEleicao());
        $objVotoOpcaoEleicaoDTO->retTodos();
        $arrObjVotoOpcaoEleicaoDTOGravados = $objVotoOpcaoEleicaoRN->listar($objVotoOpcaoEleicaoDTO);
        $objVotoOpcaoEleicaoRN->excluir($arrObjVotoOpcaoEleicaoDTOGravados);
        $arrObjVotoOpcaoEleicaoDTO = array_merge($arrObjVotoOpcaoEleicaoDTO, $arrObjVotoOpcaoEleicaoDTOGravados);
        shuffle($arrObjVotoOpcaoEleicaoDTO);
      }

      foreach ($arrObjVotoOpcaoEleicaoDTO as $objVotoOpcaoEleicaoDTO) {
        $objVotoOpcaoEleicaoDTO->setNumIdVotoOpcaoEleicao(null);
        $objVotoOpcaoEleicaoRN->cadastrar($objVotoOpcaoEleicaoDTO);
      }

      return $objVotoEleicaoDTO;

    }catch(Throwable $e){
      throw new InfraException('Erro processando vota��o no Escrut�nio Eletr�nico.',$e);
    }
  }

  protected function listarSessaoConectado(EleicaoDTO $parObjEleicaoDTO) {
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_listar', __METHOD__, $parObjEleicaoDTO);

      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->retNumIdEleicao();
      $objEleicaoDTO->retStrIdentificacao();
      $objEleicaoDTO->retStrStaSituacao();
      $objEleicaoDTO->retStrSinSecreta();
      $objEleicaoDTO->retNumOrdem();
      $objEleicaoDTO->setNumIdItemSessaoJulgamento($parObjEleicaoDTO->getNumIdItemSessaoJulgamento());
      $objEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
      $objEleicaoDTO->setOrdNumIdEleicao(InfraDTO::$TIPO_ORDENACAO_ASC);

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $ret = $objEleicaoBD->listar($objEleicaoDTO);

      if (count($ret)) {

        $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
        $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
        $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($parObjEleicaoDTO->getNumIdItemSessaoJulgamento());

        $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
        $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);


        $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoDTO->retNumIdUsuario();
        $objColegiadoComposicaoDTO->retStrSiglaUsuario();
        $objColegiadoComposicaoDTO->retStrNomeUsuario();
        $objColegiadoComposicaoDTO->retNumIdUnidade();
        $objColegiadoComposicaoDTO->retStrSiglaUnidade();
        $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
        $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
        $objColegiadoComposicaoDTO->setOrdStrNomeUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
        $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO),'IdUsuario');


        $objBloqueioItemSessUnidadeDTO = new BloqueioItemSessUnidadeDTO();
        $objBloqueioItemSessUnidadeDTO->retNumIdUnidade();
        $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
        $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);

        $objBloqueioItemSessUnidadeRN = new BloqueioItemSessUnidadeRN();
        $arrObjBloqueioItemSessUnidadeDTO = InfraArray::indexarArrInfraDTO($objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO),'IdUnidade');


        $objPresencaSessaoDTO = new PresencaSessaoDTO();
        $objPresencaSessaoDTO->retNumIdUsuario();
        $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objPresencaSessaoDTO->setDthSaida(null);

        $objPresencaSessaoRN = new PresencaSessaoRN();
        $arrObjPresencaSessaoDTO = InfraArray::indexarArrInfraDTO($objPresencaSessaoRN->listar($objPresencaSessaoDTO),'IdUsuario');

        $arrObjSituacaoEleicaoDTO = InfraArray::indexarArrInfraDTO($this->listarValoresSituacao(), 'StaSituacao');

        $objVotoEleicaoRN = new VotoEleicaoRN();

        foreach ($ret as $objEleicaoDTO) {
          $objEleicaoDTO->setStrDescricaoSituacao($arrObjSituacaoEleicaoDTO[$objEleicaoDTO->getStrStaSituacao()]->getStrDescricao());

          $objVotoEleicaoDTO = new VotoEleicaoDTO();
          $objVotoEleicaoDTO->retNumIdUsuario();
          $objVotoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
          $objVotoEleicaoDTO->setOrdStrNomeUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

          $arrObjVotoEleicaoDTO = InfraArray::indexarArrInfraDTO($objVotoEleicaoRN->listar($objVotoEleicaoDTO),'IdUsuario');

          $arrVotos = array();
          foreach($arrObjVotoEleicaoDTO as $numIdUsuario => $objVotoEleicaoDTO){
            $arrVotos[] = $arrObjColegiadoComposicaoDTO[$numIdUsuario];
          }

          $arrAguardando = array();
          foreach($arrObjColegiadoComposicaoDTO as $numIdUsuario => $objColegiadoComposicaoDTO){
            if (!isset($arrObjVotoEleicaoDTO[$numIdUsuario]) && isset($arrObjPresencaSessaoDTO[$numIdUsuario]) && !isset($arrObjBloqueioItemSessUnidadeDTO[$objColegiadoComposicaoDTO->getNumIdUnidade()])){
              $arrAguardando[] = $objColegiadoComposicaoDTO;
            }
          }

          $objEleicaoDTO->setArrObjColegiadoComposicaoDTOVotos($arrVotos);
          $objEleicaoDTO->setArrObjColegiadoComposicaoDTOAguardando($arrAguardando);
        }
      }

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Escrut�nios Eletr�nicos do processo na Sess�o.',$e);
    }
  }

  protected function cadastrarControlado(EleicaoDTO $objEleicaoDTO) {
    try{

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_cadastrar', __METHOD__, $objEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdItemSessaoJulgamento($objEleicaoDTO, $objInfraException);
      $this->validarStrIdentificacao($objEleicaoDTO, $objInfraException);
      $this->validarStrDescricao($objEleicaoDTO, $objInfraException);
      //$this->validarStrStaSituacao($objEleicaoDTO, $objInfraException);
      $this->validarNumQuantidade($objEleicaoDTO, $objInfraException);
      $this->validarNumOrdem($objEleicaoDTO, $objInfraException);
      $this->validarStrSinSecreta($objEleicaoDTO, $objInfraException);


      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objEleicaoDTO->getNumIdItemSessaoJulgamento());

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      if (in_array($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento(), array(SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_FINALIZADA))){
        $objInfraException->lancarValidacao('Situa��o da sess�o n�o permite cria��o de Escrut�nio Eletr�nico.');
      }

      $objInfraException->lancarValidacoes();

      $objEleicaoDTO->setStrStaSituacao(EleicaoRN::$SE_CRIADA);

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $ret = $objEleicaoBD->cadastrar($objEleicaoDTO);

      if ($objEleicaoDTO->isSetArrObjOpcaoEleicaoDTO()){
        $objOpcaoEleicaoRN = new OpcaoEleicaoRN();
        $arrObjOpcaoEleicaoDTO = $objEleicaoDTO->getArrObjOpcaoEleicaoDTO();
        foreach($arrObjOpcaoEleicaoDTO as $objOpcaoEleicaoDTO){
          $objOpcaoEleicaoDTO->setNumIdOpcaoEleicao(null);
          $objOpcaoEleicaoDTO->setNumIdEleicao($ret->getNumIdEleicao());
          $objOpcaoEleicaoRN->cadastrar($objOpcaoEleicaoDTO);
        }
      }


      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Escrut�nio Eletr�nico do Processo.',$e);
    }
  }

  protected function alterarControlado(EleicaoDTO $objEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_alterar', __METHOD__, $objEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objEleicaoDTOBanco = new EleicaoDTO();
      $objEleicaoDTOBanco->retNumIdItemSessaoJulgamento();
      $objEleicaoDTOBanco->retStrIdentificacao();
      $objEleicaoDTOBanco->retStrStaSituacao();
      $objEleicaoDTOBanco->retNumQuantidade();
      $objEleicaoDTOBanco->retStrSinSecreta();
      $objEleicaoDTOBanco->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
      $objEleicaoDTOBanco = $this->consultar($objEleicaoDTOBanco);

      if ($objEleicaoDTOBanco==null){
        throw new InfraException('Escrut�nio Eletr�nico n�o encontrado.');
      }

      $objVotoEleicaoDTO = new VotoEleicaoDTO();
      $objVotoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());

      $objVotoEleicaoRN = new VotoEleicaoRN();
      $numVotos = $objVotoEleicaoRN->contar($objVotoEleicaoDTO);

      if ($objEleicaoDTO->isSetNumIdItemSessaoJulgamento() && $objEleicaoDTO->getNumIdItemSessaoJulgamento()!=$objEleicaoDTOBanco->getNumIdItemSessaoJulgamento()){
        $objInfraException->lancarValidacao('N�o � poss�vel alterar o processo do Escrut�nio Eletr�nico.');
      }else{
        $objEleicaoDTO->setNumIdItemSessaoJulgamento($objEleicaoDTOBanco->getNumIdItemSessaoJulgamento());
      }

      if ($objEleicaoDTO->isSetStrIdentificacao() && $objEleicaoDTO->getStrIdentificacao()!=$objEleicaoDTOBanco->getStrIdentificacao()){
        if ($numVotos){
          $objInfraException->lancarValidacao('N�o � poss�vel alterar a Identifica��o porque o escrut�nio j� possui votos registrados.');
        }
        $this->validarStrIdentificacao($objEleicaoDTO, $objInfraException);
      }

      if ($objEleicaoDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objEleicaoDTO, $objInfraException);
      }

      if ($objEleicaoDTO->isSetStrStaSituacao() && $objEleicaoDTO->getStrStaSituacao()!=$objEleicaoDTOBanco->getStrStaSituacao()){
        $objInfraException->lancarValidacao('N�o � poss�vel alterar a situa��o do escrut�nio.');
      }else{
        $objEleicaoDTO->setStrStaSituacao($objEleicaoDTOBanco->getStrStaSituacao());
      }

      if ($objEleicaoDTO->isSetNumQuantidade()){
        if ($numVotos && $objEleicaoDTO->getNumQuantidade()!=$objEleicaoDTOBanco->getNumQuantidade()){
          $objInfraException->lancarValidacao('N�o � poss�vel alterar o N� de Escolhas porque o escrut�nio j� possui votos registrados.');
        }
        $this->validarNumQuantidade($objEleicaoDTO, $objInfraException);
      }else{
        $objEleicaoDTO->setNumQuantidade($objEleicaoDTOBanco->getNumQuantidade());
      }

      if ($objEleicaoDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objEleicaoDTO, $objInfraException);
      }

      if ($objEleicaoDTO->isSetStrSinSecreta() && $objEleicaoDTO->getStrSinSecreta()!=$objEleicaoDTOBanco->getStrSinSecreta()) {
        if ($numVotos){
          $objInfraException->lancarValidacao('N�o � poss�vel alterar a sinaliza��o Secreto porque o escrut�nio j� possui votos registrados.');
        }
        $this->validarStrSinSecreta($objEleicaoDTO, $objInfraException);
      }else{
        $objEleicaoDTO->setStrSinSecreta($objEleicaoDTOBanco->getStrSinSecreta());
      }

      $objInfraException->lancarValidacoes();

      if ($objEleicaoDTO->isSetArrObjOpcaoEleicaoDTO()){

        $arrObjOpcaoEleicaoDTO = $objEleicaoDTO->getArrObjOpcaoEleicaoDTO();

        $objOpcaoEleicaoRN = new OpcaoEleicaoRN();

        if ($numVotos){
          $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
          $objOpcaoEleicaoDTO->retStrIdentificacao();
          $objOpcaoEleicaoDTO->retNumOrdem();
          $objOpcaoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
          $arrObjOpcaoEleicaoDTOBanco = $objOpcaoEleicaoRN->listar($objOpcaoEleicaoDTO);

          if (count($arrObjOpcaoEleicaoDTO) != count($arrObjOpcaoEleicaoDTOBanco)){
            $objInfraException->lancarValidacao('N�o � poss�vel alterar a quantidade de op��es porque o escrut�nio j� possui votos registrados.');
          }

          $numRegistrosOpcoes = count($arrObjOpcaoEleicaoDTO);
          for($i=0;$i<$numRegistrosOpcoes;$i++){
            if ($arrObjOpcaoEleicaoDTO[$i]->getStrIdentificacao()!=$arrObjOpcaoEleicaoDTOBanco[$i]->getStrIdentificacao()){
              $objInfraException->lancarValidacao('N�o � poss�vel alterar as op��es porque o escrut�nio j� possui votos registrados.');
            }
          }
        }else {
          $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
          $objOpcaoEleicaoDTO->retNumIdOpcaoEleicao();
          $objOpcaoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
          $objOpcaoEleicaoRN->excluir($objOpcaoEleicaoRN->listar($objOpcaoEleicaoDTO));

          foreach($arrObjOpcaoEleicaoDTO as $objOpcaoEleicaoDTO){
            $objOpcaoEleicaoDTO->setNumIdOpcaoEleicao(null);
            $objOpcaoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
            $objOpcaoEleicaoRN->cadastrar($objOpcaoEleicaoDTO);
          }
        }
      }

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $objEleicaoBD->alterar($objEleicaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro alterando Escrut�nio Eletr�nico do Processo.',$e);
    }
  }

  protected function excluirControlado($arrObjEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_excluir', __METHOD__, $arrObjEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objVotoEleicaoRN = new VotoEleicaoRN();

      foreach($arrObjEleicaoDTO as $objEleicaoDTO){

        $objVotoEleicaoDTO = new VotoEleicaoDTO();
        $objVotoEleicaoDTO->setNumMaxRegistrosRetorno(1);
        $objVotoEleicaoDTO->retStrIdentificacaoEleicao();
        $objVotoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
        if (($objVotoEleicaoDTO = $objVotoEleicaoRN->consultar($objVotoEleicaoDTO))!=null) {
          $objInfraException->adicionarValidacao('Escrut�nio "'.$objVotoEleicaoDTO->getStrIdentificacaoEleicao().'" possui votos registrados.');
        }
      }

      $objInfraException->lancarValidacoes();

      $objOpcaoEleicaoRN = new OpcaoEleicaoRN();
      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjEleicaoDTO);$i++){

        $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
        $objOpcaoEleicaoDTO->retNumIdOpcaoEleicao();
        $objOpcaoEleicaoDTO->setNumIdEleicao($arrObjEleicaoDTO[$i]->getNumIdEleicao());
        $objOpcaoEleicaoRN->excluir($objOpcaoEleicaoRN->listar($objOpcaoEleicaoDTO));

        $objEleicaoBD->excluir($arrObjEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Escrut�nio Eletr�nico do Processo.',$e);
    }
  }

  protected function consultarConectado(EleicaoDTO $objEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_consultar', __METHOD__, $objEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $ret = $objEleicaoBD->consultar($objEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Escrut�nio Eletr�nico do Processo.',$e);
    }
  }

  protected function listarConectado(EleicaoDTO $objEleicaoDTO) {
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_listar', __METHOD__, $objEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $ret = $objEleicaoBD->listar($objEleicaoDTO);

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Escrut�nios Eletr�nicos do processo.',$e);
    }
  }

  protected function contarConectado(EleicaoDTO $objEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_listar', __METHOD__, $objEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $ret = $objEleicaoBD->contar($objEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Escrut�nios Eletr�nicos do processo.',$e);
    }
  }

  protected function clonarControlado(EleicaoDTO $parObjEleicaoDTO) {
    try{

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_clonar', __METHOD__, $parObjEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();
      $this->validarStrIdentificacao($parObjEleicaoDTO, $objInfraException);
      $objInfraException->lancarValidacoes();

      $objEleicaoDTO = new EleicaoDTO();
      $objEleicaoDTO->retTodos();
      $objEleicaoDTO->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());
      $objEleicaoDTO = $this->consultar($objEleicaoDTO);

      $objEleicaoDTO->setNumIdEleicao(null);
      $objEleicaoDTO->setStrIdentificacao($parObjEleicaoDTO->getStrIdentificacao());

      $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
      $objOpcaoEleicaoDTO->retStrIdentificacao();
      $objOpcaoEleicaoDTO->retNumOrdem();
      $objOpcaoEleicaoDTO->setNumIdEleicao($parObjEleicaoDTO->getNumIdEleicao());

      $objOpcaoEleicaoRN = new OpcaoEleicaoRN();
      $objEleicaoDTO->setArrObjOpcaoEleicaoDTO($objOpcaoEleicaoRN->listar($objOpcaoEleicaoDTO));

      return $this->cadastrar($objEleicaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro clonando Escrut�nio Eletr�nico do Processo.',$e);
    }
  }

/* 
  protected function desativarControlado($arrObjEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_desativar', __METHOD__, $arrObjEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjEleicaoDTO);$i++){
        $objEleicaoBD->desativar($arrObjEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro desativando Escrut�nio Eletr�nico do Processo.',$e);
    }
  }

  protected function reativarControlado($arrObjEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_reativar', __METHOD__, $arrObjEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjEleicaoDTO);$i++){
        $objEleicaoBD->reativar($arrObjEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro reativando Escrut�nio Eletr�nico do Processo.',$e);
    }
  }
*/

  protected function bloquearControlado(EleicaoDTO $objEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('eleicao_consultar', __METHOD__, $objEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objEleicaoBD = new EleicaoBD($this->getObjInfraIBanco());
      $ret = $objEleicaoBD->bloquear($objEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Escrut�nio Eletr�nico do Processo.',$e);
    }
  }

  private function lancarAndamento(EleicaoDTO $objEleicaoDTO)
  {
    try {
      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objEleicaoDTO->getNumIdItemSessaoJulgamento());

      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_SITUACAO_ELEICAO);

      $arrObjAtributoAndamentoSessaoDTO = array();

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('ELEICAO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objEleicaoDTO->getNumIdEleicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objEleicaoDTO->getStrIdentificacao());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('SITUACAO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objEleicaoDTO->getStrStaSituacao());

      switch ($objEleicaoDTO->getStrStaSituacao()) {
        case self::$SE_LIBERADA:
          $valor = 'liberado';
          break;
        case self::$SE_CONCLUIDA:
          $valor = 'conclu�do';
          break;
        case self::$SE_FINALIZADA:
          $valor = 'finalizado';
          break;
        case self::$SE_ANULADA:
          $valor = 'anulado';
          break;
        default:
          throw new InfraException('Situa��o '.$objEleicaoDTO->getStrStaSituacao().' inv�lida no lan�amento de andamento do Escrut�nio Eletr�nico.');
      }
      $objAtributoAndamentoSessaoDTO->setStrValor($valor);
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);

      $objAndamentoSessaoRN = new AndamentoSessaoRN();
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);
    }catch(Exception $e){
      throw new InfraException('Erro lan�ando andamento do Escrut�nio Eletr�nico.', $e);
    }
  }
}
