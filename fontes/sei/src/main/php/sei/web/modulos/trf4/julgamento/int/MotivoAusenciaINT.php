<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MotivoAusenciaINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objMotivoAusenciaDTO = new MotivoAusenciaDTO();
    $objMotivoAusenciaDTO->retNumIdMotivoAusencia();
    $objMotivoAusenciaDTO->retStrDescricao();

    if ($strValorItemSelecionado!=null){
      $objMotivoAusenciaDTO->setBolExclusaoLogica(false);
      $objMotivoAusenciaDTO->adicionarCriterio(array('SinAtivo','IdMotivoAusencia'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objMotivoAusenciaDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objMotivoAusenciaRN = new MotivoAusenciaRN();
    $arrObjMotivoAusenciaDTO = $objMotivoAusenciaRN->listar($objMotivoAusenciaDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjMotivoAusenciaDTO, 'IdMotivoAusencia', 'Descricao');
  }
}
?>