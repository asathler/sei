<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class BloqueioItemSessUnidadeINT extends InfraINT {

  public static function montarSelectUnidades($strPrimeiroItemValor, $strPrimeiroItemDescricao,$strValorItemSelecionado, $numIdDistribuicao,$dblIdDocumento=null){
    $objBloqueioItemSessUnidadeDTO=new BloqueioItemSessUnidadeDTO();

    $objBloqueioItemSessUnidadeDTO->retNumIdDistribuicao();
    $objBloqueioItemSessUnidadeDTO->retDblIdDocumento();
    $objBloqueioItemSessUnidadeDTO->retStrSiglaUnidade();
    $objBloqueioItemSessUnidadeDTO->retNumIdUnidade();
    $objBloqueioItemSessUnidadeDTO->retStrDescricaoUnidade();
    $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($numIdDistribuicao);

    if($dblIdDocumento!==null){
      $objBloqueioItemSessUnidadeDTO->setDblIdDocumento($dblIdDocumento);
    }

    $objBloqueioItemSessUnidadeDTO->setOrdStrSiglaUnidade(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objBloqueioItemSessUnidadeRN = new BloqueioItemSessUnidadeRN();
    $arrObjBloqueioItemSessUnidadeDTO = $objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);

    foreach ($arrObjBloqueioItemSessUnidadeDTO as $objBloqueioItemSessUnidadeDTO) {
      $objBloqueioItemSessUnidadeDTO->setStrSiglaUnidade(UnidadeINT::formatarSiglaDescricao($objBloqueioItemSessUnidadeDTO->getStrSiglaUnidade(),$objBloqueioItemSessUnidadeDTO->getStrDescricaoUnidade()));
    }

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjBloqueioItemSessUnidadeDTO, 'IdUnidade', 'SiglaUnidade');

  }



}
?>