<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  if ($_GET['pagina_simples']=='1'){
    PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);
  }
  //PaginaSEI::getInstance()->salvarCamposPost(array(''));

  //PaginaSEI::getInstance()->setTipoSelecao(InfraPagina::$TIPO_SELECAO_MULTIPLA);

  $arrComandos = array();


  //Filtrar par�metros
  $strParametros = '';
	$mensagem='';
  if(isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
    $strParametros .= '&arvore='.$_GET['arvore'];
  }

  if (isset($_GET['id_procedimento'])){
    $strParametros .= "&id_procedimento=".$_GET['id_procedimento'];
  }

  if (isset($_GET['pagina_simples'])){
    $strParametros .= "&pagina_simples=".$_GET['pagina_simples'];
  }

  $numIdColegiado = $_POST['selColegiado'];
	if ($numIdColegiado=='null') $numIdColegiado=null;

	if($numIdColegiado==null && isset($_GET['id_colegiado'])){
	  $numIdColegiado=$_GET['id_colegiado'];
  }

	$bolEnvioOK = false;

	$bolMultiplo=($_GET['acao_origem']=='procedimento_controlar' || $_GET['acao_origem']=='painel_distribuicao_detalhar' || isset($_POST['hdnProcedimentos']));
	if ($bolMultiplo) {
    if (isset($_POST['hdnProcedimentos'])) {
//      $arr = explode(',', $_POST['hdnProcedimentos']);
      $arr = PaginaSEI::getInstance()->getArrValuesSelect($_POST['hdnProcedimentos']);
    } else {
      $arr = array_merge(PaginaSEI::getInstance()->getArrStrItensSelecionados('Gerados'), PaginaSEI::getInstance()->getArrStrItensSelecionados('Recebidos'), PaginaSEI::getInstance()->getArrStrItensSelecionados());
//      $arr=PaginaSEI::getInstance()->gerarItensLupa($arr);
    }
    $strItensSelProcedimentos = ProcedimentoINT::conjuntoCompletoFormatadoRI0903($arr);
  }


  switch($_GET['acao']) {

    case 'distribuicao_gerar':

      if (!$bolMultiplo) {
        $strTitulo = 'Distribuir Processo';
        $redistribuicao = false;

        $objDistribuicaoDTO = new DistribuicaoDTO();
        $objDistribuicaoDTO->setDblIdProcedimento($_GET['id_procedimento']);
        $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
        $objDistribuicaoDTO->retNumIdDistribuicao();
        $objDistribuicaoDTO->retStrStaDistribuicao();
        $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
        //buscar somente dos colegiados administrados
        $objDistribuicaoDTO->setNumIdUnidadeResponsavelColegiado(SessaoSEI::getInstance()->getNumIdUnidadeAtual());

        $objDistribuicaoRN = new DistribuicaoRN();
        $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);
        $arrIdDistribuicao=InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO,'IdDistribuicao');
        $hashDistribuicao=hash('sha256',implode(',',$arrIdDistribuicao));

        $arrObjDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO, 'StaDistribuicao', true);

        if (isset($arrObjDistribuicaoDTO[DistribuicaoRN::$STA_DISTRIBUIDO]) || isset($arrObjDistribuicaoDTO[DistribuicaoRN::$STA_REDISTRIBUIDO])) {
          $strTitulo = 'Redistribuir Processo';
          $redistribuicao = true;
        }
        $arrColegiadosJulgados = null;
        if (isset($arrObjDistribuicaoDTO[DistribuicaoRN::$STA_JULGADO])) {
          $arrColegiadosJulgados = InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO[DistribuicaoRN::$STA_JULGADO], 'IdColegiadoColegiadoVersao');
        }
        $strItensSelColegiado = ColegiadoINT::montarSelectNomeAdministrados('null', '&nbsp;', $numIdColegiado, $arrColegiadosJulgados);

      } else {
        $strTitulo = 'Distribuir Processos';
        $redistribuicao = false;
        $objDistribuicaoDTO = new DistribuicaoDTO();
        $objDistribuicaoDTO->setDblIdProcedimento(array_values($arr),InfraDTO::$OPER_IN);
        $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
        $objDistribuicaoDTO->retNumIdDistribuicao();
        $objDistribuicaoDTO->retStrStaDistribuicao();
        $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
        $objDistribuicaoDTO->setNumIdUnidadeResponsavelColegiado(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objDistribuicaoRN = new DistribuicaoRN();
        $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);
        $arrIdDistribuicao=InfraArray::converterArrInfraDTO($arrObjDistribuicaoDTO,'IdDistribuicao');
        $hashDistribuicao=hash('sha256',implode(',',$arrIdDistribuicao));
        $strItensSelColegiado = ColegiadoINT::montarSelectNomeAdministrados('null', '&nbsp;', $numIdColegiado, null);
      }

      $bolExibirBotaoDistribuir = false;


      if ($numIdColegiado != null) {
        $objColegiadoDTO = new ColegiadoDTO();
        $objColegiadoRN = new ColegiadoRN();
        $objColegiadoDTO->setNumIdColegiado($numIdColegiado);
        $objColegiadoDTO = $objColegiadoRN->consultarCompleto($objColegiadoDTO);
        if ($objColegiadoDTO == null) {
          throw new InfraException('Colegiado n�o encontrado.');
        }
        $strItensMotivoImpedimento = RelMotivoDistrColegiadoINT::montarSelectDescricao('null', '&nbsp;', null, MotivoDistribuicaoRN::$TIPO_IMPEDIMENTO, $numIdColegiado);
        $strItensMotivoPrevencao = RelMotivoDistrColegiadoINT::montarSelectDescricao('null', '&nbsp;', null, MotivoDistribuicaoRN::$TIPO_PREVENCAO, $numIdColegiado);
        $arrObjColegiadoComposicaoDTO = $objColegiadoDTO->getArrObjColegiadoComposicaoDTO();
        $arrObjColegiadoComposicaoDTO = InfraArray::retirarElementoArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdTipoMembroColegiado',(string)TipoMembroColegiadoRN::$TMC_EVENTUAL);
      } else {
        $arrObjColegiadoComposicaoDTO = array();
      }


      $numMembros = InfraArray::contar($arrObjColegiadoComposicaoDTO);


      if ($numMembros) {
        if ($numMembros < $objColegiadoDTO->getNumQuorumMinimo()) {
          $mensagem = 'Colegiado n�o possui Qu�rum M�nimo para distribuir.';
        } else {
          $bolExibirBotaoDistribuir = true;
        }

        $strResultado = '';
        $strSumarioTabela = 'Tabela de Membros do Colegiado.';
        $strCaptionTabela = 'Membros do Colegiado';

        $strResultado .= '<tr>';
        $strResultado .= '<th width="15%" class="infraTh impedimento hidden">Impedimento?</th>' . "\n";
        $strResultado .= '<th width="15%" class="infraTh prevencao hidden">Preven��o?</th>' . "\n";
        //$strResultado .= '<th class="infraTh">Sigla</th>'."\n";
        $strResultado .= '<th width="45%" class="infraTh">Nome</th>' . "\n";
        $strResultado .= '<th class="infraTh" width="30%">Motivo</th>' . "\n";
        $strResultado .= '</tr>' . "\n";
        $strCssTr = '';

        $arrItensSelecionados = PaginaSEI::getInstance()->getArrStrItensSelecionados();

        InfraArray::ordenarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'Ordem', InfraArray::$TIPO_ORDENACAO_ASC);
        $idxObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'IdUsuario');

        $numTitulares = 0;
        for ($i = 0; $i < $numMembros; $i++) {
          //if ($arrObjColegiadoComposicaoDTO[$i]->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR) continue;

          $numTitulares++;
          $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
          $strResultado .= $strCssTr;

          $idUsuario = $arrObjColegiadoComposicaoDTO[$i]->getNumIdUsuario();
          $strResultado .= '<td align="center" class="impedimento hidden" valign="top">' . PaginaSEI::getInstance()->getTrCheck($i, $idUsuario, $idUsuario, 'N', 'Infra', 'onclick="ativaSelect(this);"') . '</td>';
          $strResultado .= '<td align="center" class="prevencao hidden"><input type="radio" name="rdoPrevencao" onclick="processarRadio(this)" value="' . $idUsuario . '"></td>';
          //$strResultado .= '<td align="center">'.$arrObjColegiadoComposicaoDTO[$i]->getStrSiglaUsuario().'</td>';
          $strComplemento='';
          $strResultado.='<td>';
          if ($arrObjColegiadoComposicaoDTO[$i]->getNumIdTipoMembroColegiado() != TipoMembroColegiadoRN::$TMC_TITULAR) {
            $strComplemento= ' <b>(Suplente)</b>';
          } else if ($idUsuario == $objColegiadoDTO->getNumIdUsuarioPresidente()) {
            $strResultado.='<b>';
            $strComplemento .= ' (Presidente)</b>';
          }
          $strResultado .= $arrObjColegiadoComposicaoDTO[$i]->getStrNomeUsuario() .$strComplemento;
          $strResultado .= '</td>';
          $strResultado .= '<td align="center">';
          $exibir = in_array($idUsuario, $arrItensSelecionados) ? '' : 'display:none;';
          //$strResultado .= '<select id="selMotivo'.$idUsuario.'" name="selMotivo'.$idUsuario.'" class="infraSelect" style="'.$exibir.'width:99%;font-size:1em;"></select>';
          $strResultado .= '<select id="selMotivoImpedimento' . $idUsuario . '" name="selMotivoImpedimento' . $idUsuario . '" class="infraSelect impedimento" style="' . $exibir . 'width:99%;font-size:1em;">' . $strItensMotivoImpedimento . '</select>';
          $strResultado .= '<select id="selMotivoPrevencao' . $idUsuario . '" name="selMotivoPrevencao' . $idUsuario . '" class="infraSelect prevencao" style="' . $exibir . 'width:99%;font-size:1em;">' . $strItensMotivoPrevencao . '</select>';
          $strResultado .= '</td></tr>' . "\n";
        }
        $strResultado .= '</table>';
      }

      $strResultadoCabecalho = '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
      $strResultadoCabecalho .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numTitulares) . '</caption>';

      $strResultado = $strResultadoCabecalho . $strResultado;

      if ($bolExibirBotaoDistribuir) {
        $arrComandos[] = '<button type="submit" accesskey="D" name="sbmDistribuir" id="sbmDistribuir" value="Distribuir" class="infraButton"><span class="infraTeclaAtalho">D</span>istribuir</button>';
      }

      if (PaginaSEI::getInstance()->getTipoPagina()!=InfraPagina::$TIPO_PAGINA_SIMPLES){
        $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&resultado=0'.$strParametros).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
      }

      if (isset($_POST['sbmDistribuir'])) {
        if($_POST['hdnHashDistribuicao']!=$hashDistribuicao){
          throw new InfraException('J� foi processada a distribui��o deste(s) processo(s).');
        }
        if (!$bolMultiplo) {
          $arr = array($_GET['id_procedimento']);
        }
        try {
          $bolPesquisarMateria=false;
          if($objColegiadoDTO->getStrStaAlgoritmoDistribuicao()==AlgoritmoRN::$ALG_RODADA_MATERIA){
            $bolPesquisarMateria=true;
            $objAutuacaoDTO=new AutuacaoDTO();
            $objAutuacaoRN=new AutuacaoRN();
            $objAutuacaoDTO->setDblIdProcedimento($arr,InfraDTO::$OPER_IN);
            $objAutuacaoDTO->setNumIdTipoMateria(null,InfraDTO::$OPER_DIFERENTE);
            $objAutuacaoDTO->retTodos();
            $arrObjAutuacaoDTO=$objAutuacaoRN->listar($objAutuacaoDTO);
            $arrObjAutuacaoDTO=InfraArray::indexarArrInfraDTO($arrObjAutuacaoDTO,'IdProcedimento');

          }
          $arrObjDistribuicaoDTO = array();

          $objDistribuicaoDTO = new DistribuicaoDTO();
          $objDistribuicaoDTO->setNumIdColegiadoVersao($arrObjColegiadoComposicaoDTO[0]->getNumIdColegiadoVersao());
          $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($objColegiadoDTO->getNumIdColegiado());
          $objDistribuicaoDTO->setStrSenha($_POST['pwdSenha']);
          $bolPrevencao = $_POST['rdoTipoDistribuicao'] == 'P';
          $objDistribuicaoDTO->setStrSinPrevencao($bolPrevencao ? 'S' : 'N');

          if ($bolPrevencao) {
            $id = $_POST['rdoPrevencao'];
            $objDistribuicaoDTO->setNumIdUsuarioRelator($id);
            $objDistribuicaoDTO->setNumIdUnidadeRelator($idxObjColegiadoComposicaoDTO[$id]->getNumIdUnidade());
            $objDistribuicaoDTO->setNumIdMotivoPrevencao($_POST['selMotivoPrevencao' . $id]);
          } else {
            $arrObjImpedimentoDTO = InfraArray::gerarArrInfraDTO('ImpedimentoDTO', 'IdUsuario', PaginaSEI::getInstance()->getArrStrItensSelecionados());
            foreach ($arrObjImpedimentoDTO as $objImpedimentoDTO) {
              $id = $objImpedimentoDTO->getNumIdUsuario();
              $objImpedimentoDTO->setNumIdMotivoImpedimento($_POST['selMotivoImpedimento' . $id]);
            }
            $objDistribuicaoDTO->setArrObjImpedimentoDTO($arrObjImpedimentoDTO);
          }
          $objDistribuicaoDTO->setNumIdTipoMateriaAutuacao(null);

          foreach ($arr as $idProcedimento) {

            $objDistribuicaoDTOClone=clone $objDistribuicaoDTO;
            $objDistribuicaoDTOClone->setDblIdProcedimento($idProcedimento);
            if($bolPesquisarMateria && isset($arrObjAutuacaoDTO[$idProcedimento])){
              $objDistribuicaoDTOClone->setNumIdTipoMateriaAutuacao($arrObjAutuacaoDTO[$idProcedimento]->getNumIdTipoMateria());
            }
            $arrObjDistribuicaoDTO[] = $objDistribuicaoDTOClone;
          }


          $objDistribuicaoRN = new DistribuicaoRN();
          $arrObjDistribuicaoDTO = $objDistribuicaoRN->gerar($arrObjDistribuicaoDTO);

          if ($bolMultiplo) {
            PaginaSEI::getInstance()->adicionarMensagem('Processo distribu�do com sucesso.');
            $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_controlar&acao_origem=' . $_GET['acao'] . $strParametros . PaginaSEI::montarAncora($_POST['hdnProcedimentos']));
//            header('Location: ' . $strLinkRetorno);
//            die;

          } else {
            $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_visualizar&acao_origem=' . $_GET['acao'] . '&id_procedimento=' . $_GET['id_procedimento'] . '&id_documento=' . $arrObjDistribuicaoDTO[0]->getDblIdDocumentoDistribuicao() . '&montar_visualizacao=1');
          }

          $bolEnvioOK = true;

          //PaginaSEI::getInstance()->adicionarMensagem('Processo distribu�do com sucesso.');
          //header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno()));
          //die;

        } catch (Exception $e) {
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }

      break;

    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }

	$strLinkAjaxAutoCompletarMotivo= SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=motivo_distribuicao_auto_completar');

	if ($bolEnvioOK){
    $strTitulo='Resultado da Distribui��o';
	  $arrComandos=array();
	  //gerar tela de resumo da distribuicao
    $numRegistros=InfraArray::contar($arrObjDistribuicaoDTO);
    $strSumarioTabela = 'Tabela de Processos Distribu�dos.';
    $strCaptionTabela = 'Processos Distribu�dos';

    $strResultado = '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
    $strResultado .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistros) . '</caption>';
    $strResultado .= '<tr>';
    $strResultado .= '<th class="infraTh">Processo</th>'."\n";
    $strResultado .= '<th class="infraTh">Tipo</th>' . "\n";
    $strResultado .= '<th class="infraTh">Relator</th>' . "\n";
    $strResultado .= '<th class="infraTh">Unidade</th>' . "\n";
    $strResultado .= '</tr>' . "\n";
    $strCssTr = '';

    foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {

      $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
      $strResultado .= $strCssTr;

      $strClasseProcesso = 'protocoloNormal';
      if ($objDistribuicaoDTO->getStrStaNivelAcessoGlobal() == ProtocoloRN::$NA_SIGILOSO) {
        $strClasseProcesso = 'processoVisualizadoSigiloso';
      }
      $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objDistribuicaoDTO->getDblIdProcedimento());
      //Processo
      $strResultado .= '<td align="center" style="white-space: nowrap">';
      $strResultado .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objDistribuicaoDTO->getStrDescricaoProtocolo(), $objDistribuicaoDTO->getStrNomeTipoProcedimento()) . ' class="' . $strClasseProcesso . '"  style="font-size:1em !important">' . $objDistribuicaoDTO->getStrProtocoloFormatado() . '</a>';
      $strResultado .= '</td>';

      $strResultado .= '<td align="center">'. $objDistribuicaoDTO->getStrNomeTipoProcedimento(). '</td>';
      $strResultado .= '<td>'.$objDistribuicaoDTO->getStrNomeUsuarioRelator().'</td>';
      $strResultado .= '<td align="center">'.$objDistribuicaoDTO->getStrSiglaUnidadeRelator().'</td>';
      $strResultado .= '</tr>' . "\n";
    }
    $strResultado .= '</table>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
#lblColegiado {position:absolute;left:0%;top:0%;width:60%;}
#selColegiado {position:absolute;left:0%;top:38%;width:60%;}

#lblSenha {position:absolute;left:65%;top:0%;width:20%;}
#pwdSenha {position:absolute;left:65%;top:38%;width:20%;}

#divProcedimentos {height:10em;}
#lblProcedimentos {position:absolute;left:0%;top:0%;}
#selProcedimentos {position:absolute;left:0%;top:18%;width:89%;}
#imgExcluirProcedimentos {position:absolute;left:90%;top:18%;}


#fldTipoDistribuicao {height:80%;left: 0;position: absolute;width: 33%;}
#divOptNormal {left:15%;top:<?=(PaginaSEI::getInstance()->isBolAjustarTopFieldset()?'20%':'47%')?>;position: absolute;}
#divOptPrevencao {left:55%;top:<?=(PaginaSEI::getInstance()->isBolAjustarTopFieldset()?'20%':'47%')?>;position: absolute;}

.hidden{ display:none!important;}

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
//PaginaSEI::getInstance()->abrirJavaScript();
?>
<script type="application/javascript">
  var objLupaProcedimentos = null;
  $(document).ready(function(){
    new MaskedPassword(document.getElementById("pwdSenha"), '\u25CF');
  });

function inicializar(){
<? if ($bolEnvioOK) {?>
//  parent.document.getElementById('ifrArvore').src = '<?//=$strLinkRetorno;?>//';
  return;
<? }
  if ($bolMultiplo){?>
  objLupaProcedimentos = new infraLupaSelect('selProcedimentos','hdnProcedimentos',null);
<? }
if ($bolExibirBotaoDistribuir) { ?>
    //noinspection UnreachableCodeJS
	configurarTipo(); <?
     foreach ($_POST as $key=>$value) {
         if ($value!='null' && strpos($key,'selMotivo')===0) echo "$('#".$key."').val(".$value.");\n";
     }


     } else if ($mensagem!='') echo 'alert("'.$mensagem.'");'
?>
  infraEfeitoTabelas();
}

function onSubmitForm(){

  var pwdSenha=$('#pwdSenha');
  if (infraTrim(pwdSenha.val())==''){
    alert('Informe a Senha.');
    pwdSenha.focus();
    return false;
  }

	var val=$('input[name=rdoTipoDistribuicao]:checked').val();
	if (val=='P'){
			if($('input[name^=rdoPrevencao]:checked').length==0) {
				alert('Selecione o Membro para Preven��o.');
				return false;
			}
	}
	var erro=false;
	$('select[name^=selMotivo]').filter(':visible').each(function(){
		if ($(this).val()=='null') {
			this.focus();
			alert('Selecione o Motivo.');
			erro=true;
			return false;
		}
	});
	if(erro){
		return false;
	}
<? if ($redistribuicao) {?>
	if (confirm("Confirma redistribui��o do processo?")!=true) return false;
<? }
  if ($bolMultiplo) {?>
	infraExibirAviso();
  <?}?>
	return true;
}
ativaSelect=function(obj){
	var txt=document.getElementById('selMotivoImpedimento'+obj.value);
	if (obj.checked) txt.style.display="";
	else txt.style.display="none";
};
function configurarTipo(){
	var val=$('input[name=rdoTipoDistribuicao]:checked').val();
	$('select[name^=selMotivo]').hide(); //oculta todos os selects
	$('.infraTrMarcada').removeClass('infraTrMarcada'); //desmarca linhas
	if (val=='N') { //normal
		$('.impedimento').removeClass('hidden');
		$('.prevencao').addClass('hidden');
		$('input[name^=chkInfraItem]:checked').each(function(){
      var line=$(this).parent().parent().addClass('infraTrMarcada');
      line.find('[name^=selMotivoImpedimento]').show();
		});
		window.tipo='I';
	} else { //prevencao
		$('.impedimento').addClass('hidden');
		$('.prevencao').removeClass('hidden');
		$('input[name^=rdoPrevencao]:checked').each(function(){
      var line=$(this).parent().parent().addClass('infraTrMarcada');
      line.find('[name^=selMotivoPrevencao]').show();
		});
		window.tipo='P';
	}
}
	function processarRadio(obj){
		$('select[name^=selMotivoPrevencao]').hide();
		$('.infraTrMarcada').removeClass('infraTrMarcada');
		$(obj).parent().parent().addClass('infraTrMarcada').find('[name^=selMotivoPrevencao]').show();
	}
</script>
<?
//PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmDistribuicao" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>" style="display:inline;">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
if (!$bolEnvioOK) {
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <select id="selColegiado" name="selColegiado" onchange="this.form.submit();" class="infraSelect"
          tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelColegiado; ?>
  </select>
  <? if ($bolExibirBotaoDistribuir) { ?>
    <label id="lblSenha" for="pwdSenha" accesskey="" class="infraLabelObrigatorio">Senha:</label>
    <?=InfraINT::montarInputPassword('pwdSenha','','tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'"')?>
  <?
  }
  PaginaSEI::getInstance()->fecharAreaDados();
  if ($bolMultiplo) {
    PaginaSEI::getInstance()->abrirAreaDados('9em');
    ?>
    <div id="divProcedimentos" class="infraAreaDados">
      <input type="hidden" name="hdnProcedimentos" id="hdnProcedimentos" value="<?= $_POST['hdnProcedimentos']; ?>"/>
      <label id="lblProcedimentos" for="selProcedimentos" class="infraLabelObrigatorio">Processos:</label>
      <select id="selProcedimentos" name="selProcedimentos" size="4" class="infraSelect" multiple="multiple"
              tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
        <?= $strItensSelProcedimentos ?>
      </select>
      <img id="imgExcluirProcedimentos" onclick="objLupaProcedimentos.remover();" src="<?=PaginaSEI::getInstance()->getIconeRemover()?>"
           alt="Remover Processos Selecionados" title="Remover Processos Selecionados" class="infraImg"
           tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>
    </div>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
  }
  if ($bolExibirBotaoDistribuir) {
    PaginaSEI::getInstance()->abrirAreaDados('7em');
    ?>
    <input type="hidden" name="hdnHashDistribuicao" id="hdnHashDistribuicao" value="<?=$hashDistribuicao;?>"/>
    <fieldset id="fldTipoDistribuicao" class="infraFieldset">
      <legend class="infraLegend">&nbsp;Tipo de Distribui��o&nbsp;</legend>

      <div id="divOptNormal" class="infraDivRadio">
        <input type="radio" name="rdoTipoDistribuicao" id="optNormal" value="N"
               onclick="configurarTipo();" <?= ($_POST['rdoTipoDistribuicao'] != 'P' ? 'checked="checked"' : '') ?>
               class="infraRadio"/>
        <span id="spnNormal"><label id="lblNormal" for="optNormal" class="infraLabelRadio"
                                    tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">Normal</label></span>
      </div>

      <div id="divOptPrevencao" class="infraDivRadio">
        <input type="radio" name="rdoTipoDistribuicao" id="optPrevencao" value="P"
               onclick="configurarTipo();" <?= ($_POST['rdoTipoDistribuicao'] == 'P' ? 'checked="checked"' : '') ?>
               class="infraRadio"/>
        <span id="spnPrevencao"><label id="lblPrevencao" for="optPrevencao" class="infraLabelRadio"
                                       tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">Preven��o</label></span>
      </div>

    </fieldset>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
  }
//PaginaSEI::getInstance()->montarAreaValidacao();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado, $numTitulares);
} else {
  PaginaSEI::getInstance()->montarAreaTabela($strResultado, $numRegistros);
}
?>
</form>
<?
PaginaSEI::getInstance()->montarAreaDebug();
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>