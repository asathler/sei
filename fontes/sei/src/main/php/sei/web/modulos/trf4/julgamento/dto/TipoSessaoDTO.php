<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 06/04/2020 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__.'/../../../../SEI.php';

class TipoSessaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'tipo_sessao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdTipoSessao', 'id_tipo_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Descricao', 'descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'SinVirtual', 'sin_virtual');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'SinAtivo', 'sin_ativo');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjTipoSessaoBlocoDTO');

    $this->configurarPK('IdTipoSessao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarExclusaoLogica('SinAtivo', 'N');

  }
}
?>