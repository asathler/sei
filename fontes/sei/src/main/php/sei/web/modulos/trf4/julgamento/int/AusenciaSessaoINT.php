<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class AusenciaSessaoINT extends InfraINT {

  public static function montarSelectIdUsuario($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdUsuario='', $numIdSessaoJulgamento='', $numIdMotivoAusencia=''){
    $objAusenciaSessaoDTO = new AusenciaSessaoDTO();
    $objAusenciaSessaoDTO->retNumIdAusenciaSessao();
    $objAusenciaSessaoDTO->retNumIdUsuario();

    if ($numIdUsuario!==''){
      $objAusenciaSessaoDTO->setNumIdUsuario($numIdUsuario);
    }

    if ($numIdSessaoJulgamento!==''){
      $objAusenciaSessaoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
    }

    if ($numIdMotivoAusencia!==''){
      $objAusenciaSessaoDTO->setNumIdMotivoAusencia($numIdMotivoAusencia);
    }

    $objAusenciaSessaoDTO->setOrdNumIdUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objAusenciaSessaoRN = new AusenciaSessaoRN();
    $arrObjAusenciaSessaoDTO = $objAusenciaSessaoRN->listar($objAusenciaSessaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjAusenciaSessaoDTO, 'IdAusenciaSessao', 'IdUsuario');
  }
}
?>