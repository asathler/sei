<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PresencaSessaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdUsuario(PresencaSessaoDTO $objPresencaSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPresencaSessaoDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Membro n�o informado.');
    }
  }
  private function validarArrNumIdUsuario(PresencaSessaoDTO $objPresencaSessaoDTO, InfraException $objInfraException){
    $arr=$objPresencaSessaoDTO->getArrNumIdUsuario();
    if (!is_array($arr)){
      $objInfraException->adicionarValidacao('Membros n�o informados.');
    }
  }

  private function validarNumIdSessaoJulgamento(PresencaSessaoDTO $objPresencaSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPresencaSessaoDTO->getNumIdSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Sess�o n�o informada.');
    }
  }

  private function validarStrSinPresente(PresencaSessaoDTO $objPresencaSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPresencaSessaoDTO->getStrSinPresente())){
      $objInfraException->adicionarValidacao('Sinalizador de Presen�a n�o informado.');
    }else{
      if (!in_array($objPresencaSessaoDTO->getStrSinPresente(),array('S','N'))){
        $objInfraException->adicionarValidacao('Sinalizador de Presen�a inv�lido.');
      }
    }
  }

  private function validarSituacaoSessao(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException)
  {
    if (!in_array($objSessaoJulgamentoDTO->getStrStaSituacao(),array(SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_SUSPENSA))){
      $objInfraException->lancarValidacao('Sess�o n�o permite cadastro de presen�a na situa��o atual.');
    }
  }

  protected function excluirControlado($arrObjPresencaSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('presenca_sessao_excluir',__METHOD__,$arrObjPresencaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPresencaSessaoBD = new PresencaSessaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjPresencaSessaoDTO);$i++){
        $objPresencaSessaoBD->excluir($arrObjPresencaSessaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Presen�a.',$e);
    }
  }

  protected function consultarConectado(PresencaSessaoDTO $objPresencaSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('presenca_sessao_consultar',__METHOD__,$objPresencaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPresencaSessaoBD = new PresencaSessaoBD($this->getObjInfraIBanco());
      $ret = $objPresencaSessaoBD->consultar($objPresencaSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Presen�a.',$e);
    }
  }

  protected function listarConectado(PresencaSessaoDTO $objPresencaSessaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('presenca_sessao_listar',__METHOD__,$objPresencaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPresencaSessaoBD = new PresencaSessaoBD($this->getObjInfraIBanco());
      $ret = $objPresencaSessaoBD->listar($objPresencaSessaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Presen�as.',$e);
    }
  }

  protected function contarConectado(PresencaSessaoDTO $objPresencaSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('presenca_sessao_listar',__METHOD__,$objPresencaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPresencaSessaoBD = new PresencaSessaoBD($this->getObjInfraIBanco());
      $ret = $objPresencaSessaoBD->contar($objPresencaSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Presen�as.',$e);
    }
  }


  protected function definirPresencaControlado(PresencaSessaoDTO $objPresencaSessaoDTO){
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('presenca_sessao_registrar',__METHOD__,$objPresencaSessaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdSessaoJulgamento($objPresencaSessaoDTO, $objInfraException);
      $this->validarStrSinPresente($objPresencaSessaoDTO, $objInfraException);
      if($objPresencaSessaoDTO->isSetArrNumIdUsuario()){
        $this->validarArrNumIdUsuario($objPresencaSessaoDTO, $objInfraException);
        $idUsuarios=$objPresencaSessaoDTO->getArrNumIdUsuario();
      } else {
        $this->validarNumIdUsuario($objPresencaSessaoDTO,$objInfraException);
        $idUsuarios=[$objPresencaSessaoDTO->getNumIdUsuario()];
      }

      $qtdUsuarios=InfraArray::contar($idUsuarios);
      if($qtdUsuarios==0){
        $objInfraException->lancarValidacao('Nenhum membro selecionado');
      }

      //validar se sess�o est� em pauta_fechada, suspensa ou aberta
      $objSessaoJulgamentoDTO=CacheSessaoJulgamentoRN::getObjSessaoJulgamentoDTO($objPresencaSessaoDTO->getNumIdSessaoJulgamento());
      $this->validarSituacaoSessao($objSessaoJulgamentoDTO,$objInfraException);

      //validar se usu�rios est�o habilitados no colegiado
      $this->validarIdUsuarioHabilitado($objPresencaSessaoDTO, $objInfraException);

      //validar se tem presencas a mais que a quantidade de titulares
      if ($objPresencaSessaoDTO->getStrSinPresente()=='S') {
        $objPresencaSessaoDTO->setNumIdColegiadoSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdColegiado());
        $this->registrarEntradaInterno($objPresencaSessaoDTO, $objInfraException);
      } else {
        $this->registrarSaidaInterno($objPresencaSessaoDTO, $objInfraException);
      }

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Presen�a.',$e);
    }


  }

  /**
   * @param PresencaSessaoDTO $objPresencaSessaoDTO
   * @param InfraException $objInfraException
   * @throws InfraException
   */
  private function registrarSaidaInterno(PresencaSessaoDTO $objPresencaSessaoDTO, InfraException $objInfraException): void
  {
    $objPresencaSessaoDTOBanco=new PresencaSessaoDTO();

    if($objPresencaSessaoDTO->isSetArrNumIdUsuario()){
      $arrNumIdUsuario=$objPresencaSessaoDTO->getArrNumIdUsuario();
      $objPresencaSessaoDTOBanco->setNumIdUsuario($arrNumIdUsuario,InfraDTO::$OPER_IN);
    } else {
      $arrNumIdUsuario=[$objPresencaSessaoDTO->getNumIdUsuario()];
      $objPresencaSessaoDTOBanco->setNumIdUsuario($objPresencaSessaoDTO->getNumIdUsuario());
    }

    if(count($arrNumIdUsuario)==0){
      return;
    }

    $objPresencaSessaoDTOBanco->setNumIdSessaoJulgamento($objPresencaSessaoDTO->getNumIdSessaoJulgamento());
    $objPresencaSessaoDTOBanco->setDthSaida(null);
    $objPresencaSessaoDTOBanco->retNumIdPresencaSessao();
    $objPresencaSessaoDTOBanco->retNumIdUsuario();
    $objPresencaSessaoDTOBanco->retNumIdUsuarioPresidenteSessaoJulgamento();
    $objPresencaSessaoDTOBanco->retStrStaSituacaoSessaoJulgamento();
    $arrObjPresencaSessaoDTOBanco=$this->listar($objPresencaSessaoDTOBanco);

    foreach ($arrObjPresencaSessaoDTOBanco as $objPresencaSessaoDTOBanco) {
      if ($objPresencaSessaoDTOBanco->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_PAUTA_FECHADA) {
        $this->excluir(array($objPresencaSessaoDTOBanco));
      } else {
        $objPresencaSessaoDTOBanco->setDthSaida(InfraData::getStrDataHoraAtual());
        if ($objPresencaSessaoDTOBanco->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_ABERTA &&
            $objPresencaSessaoDTOBanco->getNumIdUsuarioPresidenteSessaoJulgamento()==$objPresencaSessaoDTO->getNumIdUsuario()) {
          $objInfraException->lancarValidacao('� necess�rio alterar o Presidente para que ele possa se ausentar da sess�o em andamento.');
        }
        $objPresencaSessaoBD = new PresencaSessaoBD($this->getObjInfraIBanco());
        $objPresencaSessaoBD->alterar($objPresencaSessaoDTOBanco);
      }
    }


  }

  /**
   * @param PresencaSessaoDTO $objPresencaSessaoDTO
   * @param InfraException $objInfraException
   * @throws InfraException
   */
  private function registrarEntradaInterno(PresencaSessaoDTO $objPresencaSessaoDTO, InfraException $objInfraException): void
  {

    if($objPresencaSessaoDTO->isSetArrNumIdUsuario()){
      $arrNumIdUsuario=$objPresencaSessaoDTO->getArrNumIdUsuario();
    } else {
      $arrNumIdUsuario=[$objPresencaSessaoDTO->getNumIdUsuario()];
    }

    if(count($arrNumIdUsuario)==0){
      return;
    }

    //consulta quantidade de membros titulares do colegiado
    $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
    $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objPresencaSessaoDTO->getNumIdColegiadoSessaoJulgamento());
    $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
    $objColegiadoComposicaoDTO->setNumIdTipoMembroColegiado(TipoMembroColegiadoRN::$TMC_TITULAR);
    $qtdTitulares = $objColegiadoComposicaoRN->contar($objColegiadoComposicaoDTO);

    $arrObjPresencaSessaoDTOBanco=CacheSessaoJulgamentoRN::getArrObjPresencaSessaoDTO($objPresencaSessaoDTO->getNumIdSessaoJulgamento());

    foreach ($arrObjPresencaSessaoDTOBanco as $chave=>$objPresencaSessaoDTOBanco) {
      if($objPresencaSessaoDTOBanco->getDthSaida()!=null){
        unset($arrObjPresencaSessaoDTOBanco[$chave]);
      }
    }
    foreach ($arrNumIdUsuario as $chave=>$numIdUsuario) {
      if(isset($arrObjPresencaSessaoDTOBanco[$numIdUsuario]) && $arrObjPresencaSessaoDTOBanco[$numIdUsuario]->getDthSaida()==null){
        unset($arrNumIdUsuario[$chave]);
      }
    }

    if(count($arrNumIdUsuario)==0){
      return;
    }

    if (count($arrObjPresencaSessaoDTOBanco)+count($arrNumIdUsuario)>$qtdTitulares) {
      $objInfraException->lancarValidacao('N�mero de membros presentes � superior ao n�mero de membros titulares.');
    }


    $objPresencaSessaoBD = new PresencaSessaoBD($this->getObjInfraIBanco());
    $objPresencaSessaoDTOBanco = new PresencaSessaoDTO();
    $objPresencaSessaoDTOBanco->setNumIdSessaoJulgamento($objPresencaSessaoDTO->getNumIdSessaoJulgamento());
    $objPresencaSessaoDTOBanco->setDthSaida(null);
    foreach ($arrNumIdUsuario as $numIdUsuario) {
      $objPresencaSessaoDTOBanco->setDthEntrada(InfraData::getStrDataHoraAtual());
      $objPresencaSessaoDTOBanco->setNumIdUsuario($numIdUsuario);
      $objPresencaSessaoBD->cadastrar($objPresencaSessaoDTOBanco);
    }
  }

  /**
   * @param PresencaSessaoDTO $objPresencaSessaoDTO
   * @param InfraException $objInfraException
   * @throws InfraException
   */
  private function validarIdUsuarioHabilitado(PresencaSessaoDTO $objPresencaSessaoDTO, InfraException $objInfraException):void
  {
    if($objPresencaSessaoDTO->isSetArrNumIdUsuario()){
      $arrNumIdUsuario=$objPresencaSessaoDTO->getArrNumIdUsuario();
    } else {
      $arrNumIdUsuario=[$objPresencaSessaoDTO->getNumIdUsuario()];
    }

    $arrObjColegiadoComposicaoDTO = CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($objPresencaSessaoDTO->getNumIdSessaoJulgamento());
    foreach ($arrNumIdUsuario as $idUsuario) {
      if (!isset($arrObjColegiadoComposicaoDTO[$idUsuario])) {
        $objInfraException->adicionarValidacao('Usu�rio informado [' . $idUsuario . '] n�o pertence ao colegiado.');
        continue;
      }
      if ($arrObjColegiadoComposicaoDTO[$idUsuario]->getStrSinHabilitado()!=='S') {
        $objInfraException->adicionarValidacao('Usu�rio informado [' . $idUsuario . '] n�o est� habilitado no colegiado.');
      }
    }
  }
}
?>