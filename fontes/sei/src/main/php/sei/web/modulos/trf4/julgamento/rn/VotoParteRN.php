<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class VotoParteRN extends InfraRN {

  public static $STA_ACOMPANHA = 'A';
  public static $STA_DIVERGE = 'D';
  public static $STA_ABSTENCAO = 'S';
  public static $STA_PEDIDO_VISTA = 'V';
  public static $STA_AGUARDA_VISTA = 'G';
  public static $STA_AUSENTE = 'X';
  public static $STA_IMPEDIMENTO = 'I';
  public static $STA_PROVIMENTO_DISPONIBILIZADO = 'P';


  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  public function listarValoresVotoParte(){
    try {

      $arrObjEstadoVotoParteDTO = array();

      $objEstadoVotoParteDTO = new EstadoVotoParteDTO();
      $objEstadoVotoParteDTO->setStrStaVotoParte(self::$STA_ACOMPANHA);
      $objEstadoVotoParteDTO->setStrDescricao('Acompanha');
      $arrObjEstadoVotoParteDTO[] = $objEstadoVotoParteDTO;

      $objEstadoVotoParteDTO = new EstadoVotoParteDTO();
      $objEstadoVotoParteDTO->setStrStaVotoParte(self::$STA_DIVERGE);
      $objEstadoVotoParteDTO->setStrDescricao('Diverge');
      $arrObjEstadoVotoParteDTO[] = $objEstadoVotoParteDTO;

      $objEstadoVotoParteDTO = new EstadoVotoParteDTO();
      $objEstadoVotoParteDTO->setStrStaVotoParte(self::$STA_ABSTENCAO);
      $objEstadoVotoParteDTO->setStrDescricao('Absten��o');
      $arrObjEstadoVotoParteDTO[] = $objEstadoVotoParteDTO;

      $objEstadoVotoParteDTO = new EstadoVotoParteDTO();
      $objEstadoVotoParteDTO->setStrStaVotoParte(self::$STA_PEDIDO_VISTA);
      $objEstadoVotoParteDTO->setStrDescricao('Pedido de Vista');
      $arrObjEstadoVotoParteDTO[] = $objEstadoVotoParteDTO;

      $objEstadoVotoParteDTO = new EstadoVotoParteDTO();
      $objEstadoVotoParteDTO->setStrStaVotoParte(self::$STA_AGUARDA_VISTA);
      $objEstadoVotoParteDTO->setStrDescricao('Aguarda Vista');
      $arrObjEstadoVotoParteDTO[] = $objEstadoVotoParteDTO;

      $objEstadoVotoParteDTO = new EstadoVotoParteDTO();
      $objEstadoVotoParteDTO->setStrStaVotoParte(self::$STA_IMPEDIMENTO);
      $objEstadoVotoParteDTO->setStrDescricao('Impedimento/Suspei��o');
      $arrObjEstadoVotoParteDTO[] = $objEstadoVotoParteDTO;

      $objEstadoVotoParteDTO = new EstadoVotoParteDTO();
      $objEstadoVotoParteDTO->setStrStaVotoParte(self::$STA_AUSENTE);
      $objEstadoVotoParteDTO->setStrDescricao('Ausente');
      $arrObjEstadoVotoParteDTO[] = $objEstadoVotoParteDTO;

      return $arrObjEstadoVotoParteDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de VotoParte.',$e);
    }
  }

  private function validarNumIdJulgamentoParte(VotoParteDTO $objVotoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoParteDTO->getNumIdJulgamentoParte())){
      $objInfraException->adicionarValidacao('Item de Julgamento n�o informado.');
    }
  }
  private function validarNumIdVotoParteAssociado(VotoParteDTO $objVotoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoParteDTO->getNumIdVotoParteAssociado())){
      $objVotoParteDTO->setNumIdVotoParteAssociado(null);
    }
  }
  private function validarNumIdProvimento(VotoParteDTO $objVotoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoParteDTO->getNumIdProvimento())){
      $objInfraException->adicionarValidacao('Provimento n�o informado.');
    }
  }
  private function validarStrRessalva(VotoParteDTO $objVotoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoParteDTO->getStrRessalva())){
      $objVotoParteDTO->setStrRessalva(null);
    }else{
      $objVotoParteDTO->setStrRessalva(trim($objVotoParteDTO->getStrRessalva()));

      if (strlen($objVotoParteDTO->getStrRessalva())>1000){
        $objInfraException->adicionarValidacao('Ressalva possui tamanho superior a 1000 caracteres.');
      }
    }
  }
  private function validarStrStaVotoParte(VotoParteDTO $objVotoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoParteDTO->getStrStaVotoParte())){
      $objInfraException->adicionarValidacao('Voto n�o informado.');
    }else{
      $staVoto=$objVotoParteDTO->getStrStaVotoParte();
      if ($staVoto!=self::$STA_PROVIMENTO_DISPONIBILIZADO &&
          !in_array($staVoto,InfraArray::converterArrInfraDTO($this->listarValoresVotoParte(),'StaVotoParte'))){
        $objInfraException->adicionarValidacao('Voto inv�lido.');
      }
    }
  }
  private function validarDthVoto(VotoParteDTO $objVotoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoParteDTO->getDthVoto())){
      $objInfraException->adicionarValidacao('Hora do Voto n�o informada.');
    }else{
      if (!InfraData::validarDataHora($objVotoParteDTO->getDthVoto())){
        $objInfraException->adicionarValidacao('Hora do Voto inv�lida.');
      }
    }
  }
  private function validarNumIdUsuario(VotoParteDTO $objVotoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoParteDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
    }
  }

  private function registrarInterno(VotoParteDTO $objVotoParteDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('voto_parte_cadastrar',__METHOD__,$objVotoParteDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdJulgamentoParte($objVotoParteDTO, $objInfraException);
      $this->validarNumIdVotoParteAssociado($objVotoParteDTO, $objInfraException);
      $this->validarStrRessalva($objVotoParteDTO, $objInfraException);
      $this->validarStrStaVotoParte($objVotoParteDTO, $objInfraException);
      $this->validarDthVoto($objVotoParteDTO, $objInfraException);
      $this->validarNumIdUsuario($objVotoParteDTO, $objInfraException);


      if($objVotoParteDTO->getStrStaVotoParte()!=self::$STA_ACOMPANHA){
        $objVotoParteDTO->setNumIdVotoParteAssociado(null);
      }

      $objInfraException->lancarValidacoes();
      
      $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
      $ret = $objVotoParteBD->cadastrar($objVotoParteDTO);


      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Voto.',$e);
    }
  }
  protected function registrarControlado($varObjVotoParteDTO) {
    if (!is_array($varObjVotoParteDTO)){
      $varObjVotoParteDTO=array($varObjVotoParteDTO);
    }
    if (InfraArray::contar($varObjVotoParteDTO)==0) {
      return;
    }
    $objInfraException = new InfraException();
    $idJulgamentoParte=$varObjVotoParteDTO[0]->getNumIdJulgamentoParte();
    $idxObjVotoParteDTO=InfraArray::indexarArrInfraDTO($varObjVotoParteDTO,'IdUsuario');
    //votos m�ltiplos
    if(InfraArray::contar($varObjVotoParteDTO)>1) {
      foreach ($varObjVotoParteDTO as $objVotoParteDTO) {
        if($objVotoParteDTO->getNumIdJulgamentoParte()!=$idJulgamentoParte){
          $objInfraException->lancarValidacao('Votos m�ltiplos de partes diferentes n�o s�o permitidos.');
        }
        if (in_array($objVotoParteDTO->getStrStaVotoParte(), array(VotoParteRN::$STA_DIVERGE, VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO, VotoParteRN::$STA_PEDIDO_VISTA))) {
          $objInfraException->lancarValidacao('Tipo de voto inv�lido para voto m�ltiplo.');
        }
      }
    }
    $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
    $dthAtutal=InfraData::getStrDataHoraAtual();

    $objJulgamentoParteDTO=new JulgamentoParteDTO();
    $objJulgamentoParteRN=new JulgamentoParteRN();
    $objJulgamentoParteDTO->setNumIdJulgamentoParte($idJulgamentoParte);
    $objJulgamentoParteDTO->retDblIdProcedimentoDistribuicao();
    $objJulgamentoParteDTO->retNumIdDistribuicaoItemSessaoJulgamento();
    $objJulgamentoParteDTO->retStrStaSituacaoItem();
    $objJulgamentoParteDTO->retStrStaSituacaoSessaoJulgamento();
    $objJulgamentoParteDTO->retStrStaTipoItemSessaoBloco();
    $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdUsuarioRelatorDistribuicao();
    $objJulgamentoParteDTO->retNumIdUsuarioSessaoItem();
    $objJulgamentoParteDTO->retNumIdColegiadoSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdUsuarioRelatorAcordaoDistribuicao();
    $objJulgamentoParteDTO->retNumIdUsuarioDesempate();
    $objJulgamentoParteDTO->retStrSinManualItemSessaoJulgamento();
    $objJulgamentoParteDTO->retStrSinVirtualTipoSessao();
    $objJulgamentoParteDTO->retDthSessaoSessaoJulgamento();
    $objJulgamentoParteDTO=$objJulgamentoParteRN->consultar($objJulgamentoParteDTO);

    $numIdUsuarioRelator=$objJulgamentoParteDTO->getNumIdUsuarioRelatorDistribuicao();
    $numIdUsuarioSessao=$objJulgamentoParteDTO->getNumIdUsuarioSessaoItem();
    $bolUsuarioSessaoDiferente=$numIdUsuarioRelator!=$numIdUsuarioSessao;
    $bolReferendo=$objJulgamentoParteDTO->getStrStaTipoItemSessaoBloco()==TipoSessaoBlocoRN::$STA_REFERENDO;

    $objVotoParteDTOBanco=new VotoParteDTO();
    $objVotoParteDTOBanco->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
    $objVotoParteDTOBanco->retNumIdUsuario();
    $objVotoParteDTOBanco->setStrStaVotoParte(VotoParteRN::$STA_PEDIDO_VISTA);
    $objVotoParteDTO=$this->consultar($objVotoParteDTOBanco);
    if($objVotoParteDTO==null){
      $numIdUsuarioVista=null;
    } else {
      $numIdUsuarioVista=$objVotoParteDTO->getNumIdUsuario();
    }


    $objVotoParteDTOBanco=new VotoParteDTO();
    $objVotoParteDTOBanco->setNumIdJulgamentoParte($idJulgamentoParte);
    $objVotoParteDTOBanco->retNumIdVotoParte();
    $objVotoParteDTOBanco->retNumIdUsuario();
    $objVotoParteDTOBanco->retStrStaVotoParte();
    $arrObjVotoParteDTO=$this->listar($objVotoParteDTOBanco);
    if($arrObjVotoParteDTO){
      $arrObjVotoParteDTO=InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO,'IdUsuario');
    }

    if($varObjVotoParteDTO[0]->isSetObjItemSessaoJulgamentoDTO()){
      $objItemSessaoJulgamentoDTO=$varObjVotoParteDTO[0]->getObjItemSessaoJulgamentoDTO();
      $arrObjColegiadoComposicaoDTO=$objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO();
      $arrObjPresencaSessaoDTO=$objItemSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();
    } else {
      $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objJulgamentoParteDTO->getNumIdColegiadoSessaoJulgamento());
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->retNumIdUsuario();
      $objColegiadoComposicaoDTO->retNumIdUnidade();
      $objColegiadoComposicaoDTO->retNumOrdem();
      $objColegiadoComposicaoDTO->retNumIdTipoMembroColegiado();
      $objColegiadoComposicaoDTO->retStrExpressaoCargo();
      $objColegiadoComposicaoDTO->retStrNomeUsuario();
      $objColegiadoComposicaoDTO->retStrStaGeneroCargo();
      $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);

      $objPresencaSessaoDTO=new PresencaSessaoDTO();
      $objPresencaSessaoRN=new PresencaSessaoRN();
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objJulgamentoParteDTO->getNumIdSessaoJulgamento());
      $objPresencaSessaoDTO->setDthSaida(null);
      $objPresencaSessaoDTO->retNumIdUsuario();
      $objPresencaSessaoDTO->retDthEntrada();
      $objPresencaSessaoDTO->retDthSaida();
      $objPresencaSessaoDTO->retNumIdPresencaSessao();
      $arrObjPresencaSessaoDTO=$objPresencaSessaoRN->listar($objPresencaSessaoDTO);
    }

    $idxObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUsuario');
    if($arrObjPresencaSessaoDTO){
      $arrObjPresencaSessaoDTO=InfraArray::indexarArrInfraDTO($arrObjPresencaSessaoDTO,'IdUsuario');
    }

    //Valida��es gerais

    if($varObjVotoParteDTO[0]->getStrStaVotoParte()!=self::$STA_PROVIMENTO_DISPONIBILIZADO){

      //validacao: verificar quorum m�nimo para votar
      $objColegiadoDTO=new ColegiadoDTO();
      $objColegiadoDTO->setNumIdColegiado($objJulgamentoParteDTO->getNumIdColegiadoSessaoJulgamento());
      $objColegiadoDTO->retNumQuorumMinimo();
      $objColegiadoRN=new ColegiadoRN();
      $objColegiadoDTO=$objColegiadoRN->consultar($objColegiadoDTO);
      if(count($arrObjPresencaSessaoDTO)<$objColegiadoDTO->getNumQuorumMinimo()){
        $objInfraException->lancarValidacao('Quorum m�nimo n�o foi atingido para votar.');
      }
      //validacao: 03 n�o pode votar sem a presenca do usuario sessao
      if(!$bolReferendo && (!isset($arrObjPresencaSessaoDTO[$numIdUsuarioSessao]) || $arrObjPresencaSessaoDTO[$numIdUsuarioSessao]->getDthSaida()!=null)){
        $objInfraException->lancarValidacao('N�o � poss�vel lan�ar voto sem a presen�a do usu�rio que pautou o processo.');
      }
      //validacao: 04 tem que ter documento disponibilizado
      $objItemSessaoDocumentoDTO=new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoRN=new ItemSessaoDocumentoRN();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoDocumentoDTO->setNumIdUsuario($numIdUsuarioRelator);
      if ($objItemSessaoDocumentoRN->contar($objItemSessaoDocumentoDTO)==0){
        $objInfraException->lancarValidacao('N�o � poss�vel lan�ar voto em processo sem documento disponibilizado pelo relator.');
      }
      if($bolUsuarioSessaoDiferente){
        $objItemSessaoDocumentoDTO->setNumIdUsuario($numIdUsuarioSessao);
        if ($objItemSessaoDocumentoRN->contar($objItemSessaoDocumentoDTO)==0){
          $objInfraException->lancarValidacao('N�o � poss�vel lan�ar voto sem documento disponibilizado pelo usu�rio que pautou o processo.');
        }
      }
//      if($bolSessaoVirtual && InfraData::compararDataHora($objJulgamentoParteDTO->getDthSessaoSessaoJulgamento(),InfraData::getStrDataHoraAtual())>0){
//        $objInfraException->lancarValidacao('Hor�rio de vota��o de sess�o virtual encerrado.');
//      }

      //validacao: 06 voto somente com sessao aberta, exceto disponibilizacao de provimento (somente relator)
      if ($objJulgamentoParteDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){
        if(InfraArray::contar($varObjVotoParteDTO)>1){
          $objInfraException->lancarValidacao('S� � poss�vel lan�ar votos m�ltiplos em sess�o aberta.');
        }
        if($varObjVotoParteDTO[0]->getNumIdUsuario()!=$numIdUsuarioRelator){
          $objInfraException->lancarValidacao('S� � poss�vel lan�ar voto em Sess�o Aberta.');
        }
      }

    }

    //validacao: 07 n�o pode votar em processo adiado, retirado ou diligencia
    if(in_array($objJulgamentoParteDTO->getStrStaSituacaoItem(),array(ItemSessaoJulgamentoRN::$STA_RETIRADO,ItemSessaoJulgamentoRN::$STA_ADIADO,ItemSessaoJulgamentoRN::$STA_DILIGENCIA))){
      $objInfraException->lancarValidacao('N�o � poss�vel lan�ar voto em processo retirado ou adiado.');
    }

    //validacao: 08 n�o permite voto ap�s j� ter votos igual ao numero de titulares, ignorar em caso de referendo
    if(!$bolReferendo) {
      $numTitulares = 0;
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        if ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR) {
          $numTitulares ++;
        }
      }
      $numVotos = InfraArray::contar($arrObjVotoParteDTO);
      foreach ($varObjVotoParteDTO as $objVotoParteDTO) {
        /** @noinspection NotOptimalIfConditionsInspection */
        if (!isset($arrObjVotoParteDTO[$objVotoParteDTO->getNumIdUsuario()]) && ++ $numVotos>$numTitulares) {
          $objInfraException->lancarValidacao('N�o � poss�vel lan�ar novo voto. Total de votos excede numero de titulares do colegiado.');
        }
      }
    }


    $bolInicioJulgamentoItem=false;
    foreach ($idxObjVotoParteDTO as $numIdUsuarioVoto=>$objVotoParteDTO) {
      $objVotoParteDTO->setDblIdProcedimentoDistribuicao($objJulgamentoParteDTO->getDblIdProcedimentoDistribuicao());
      $strStaVoto=$objVotoParteDTO->getStrStaVotoParte();


      if($strStaVoto!=self::$STA_PROVIMENTO_DISPONIBILIZADO){
        $bolInicioJulgamentoItem=true;
      }

      //validacao: 01 usuario sessao e relator n�o podem pedir vista
      if ($strStaVoto==self::$STA_PEDIDO_VISTA && ($numIdUsuarioVoto==$numIdUsuarioRelator || $numIdUsuarioVoto==$numIdUsuarioSessao)){
        $objInfraException->lancarValidacao('Usu�rio n�o pode pedir vista.');
      }

      //validacao: 02 s� pode votar ap�s relator e usuario sess�o terem disponibilizado ou votado
      if($numIdUsuarioRelator!=$numIdUsuarioVoto && $objJulgamentoParteDTO->getStrSinManualItemSessaoJulgamento()=='N'){
        if (!isset($arrObjVotoParteDTO[$numIdUsuarioRelator])) {
          $objInfraException->lancarValidacao('N�o � poss�vel lan�ar voto antes do relator.');
        }
        if ($bolInicioJulgamentoItem && $arrObjVotoParteDTO[$numIdUsuarioRelator]->getStrStaVotoParte()==self::$STA_PROVIMENTO_DISPONIBILIZADO){
          if($bolUsuarioSessaoDiferente){
            throw new InfraException('Processo pautado por outro usu�rio sem voto do relator.');
          }
          $arrObjVotoParteDTO[$numIdUsuarioRelator]->setStrStaVotoParte(self::$STA_ACOMPANHA);
          if(!$bolReferendo && (!isset($arrObjPresencaSessaoDTO[$numIdUsuarioRelator]) || $arrObjPresencaSessaoDTO[$numIdUsuarioRelator]->getDthSaida()!=null)){
            $objInfraException->lancarValidacao('N�o � poss�vel lan�ar primeiro voto sem a presen�a do relator.');
          }
          $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
          $objVotoParteBD->alterar($arrObjVotoParteDTO[$numIdUsuarioRelator]);
        }
//        if($bolUsuarioSessaoDiferente){
//          if($numIdUsuarioVoto!=$numIdUsuarioSessao && !isset($idxObjVotoParteDTO[$numIdUsuarioSessao])){
//            if (!isset($arrObjVotoParteDTO[$numIdUsuarioSessao])) {
//              $objInfraException->lancarValidacao('N�o � poss�vel lan�ar voto antes do usu�rio que pautou o processo.');
//            } else if ($arrObjVotoParteDTO[$numIdUsuarioSessao]->getStrStaVotoParte()==self::$STA_PROVIMENTO_DISPONIBILIZADO){
//              $objInfraException->lancarValidacao('Falta lan�ar voto-vista do usu�rio que pautou o processo.');
//            }
//          }
//        }

      if($strStaVoto!=self::$STA_PROVIMENTO_DISPONIBILIZADO){
        //validacao: 05 s� membro presente pode votar
        if(!isset($arrObjPresencaSessaoDTO[$numIdUsuarioVoto])|| $arrObjPresencaSessaoDTO[$numIdUsuarioVoto]->getDthSaida()!=null){
          $objInfraException->lancarValidacao('N�o � poss�vel lan�ar voto de membro ausente na sess�o.');
        }
        //validacao: 09 voto relator ou divergente devem ter provimento
        if($numIdUsuarioRelator==$numIdUsuarioVoto|| $numIdUsuarioSessao==$numIdUsuarioVoto||$strStaVoto==self::$STA_DIVERGE ){
          $this->validarNumIdProvimento($objVotoParteDTO, $objInfraException);
        } else {
          $objVotoParteDTO->setNumIdProvimento(null);
          $objVotoParteDTO->setStrComplemento(null);
        }
      }

      //validacao: 10 referendo n�o pode ter pedido de vista
      if($bolReferendo && $strStaVoto==self::$STA_PEDIDO_VISTA){
        $objInfraException->lancarValidacao('N�o � poss�vel pedir vista de referendo.');
      }

      //n�o pode aguardar vista se n�o tiver pedido de vista
      if($strStaVoto==VotoParteRN::$STA_AGUARDA_VISTA && ($numIdUsuarioVista==null || $numIdUsuarioVista==$numIdUsuarioVoto)){
        $objInfraException->lancarValidacao('Processo n�o possui pedido de vista para aguardar.');
      }

      //voto do relator pode ser provimento_disponibilizado ou acompanha sem voto associado
      if($numIdUsuarioRelator==$numIdUsuarioVoto){
        if ($strStaVoto!=self::$STA_PROVIMENTO_DISPONIBILIZADO){
          $objVotoParteDTO->setStrStaVotoParte(self::$STA_ACOMPANHA);
        }
        $objVotoParteDTO->setNumIdVotoParteAssociado(null);
      }

      //n�o permite pedido de vista m�ltiplo - se permitir vista multiplo verificar se ja tem ped vista do mesmo usuario
      if($strStaVoto==self::$STA_PEDIDO_VISTA && $numIdUsuarioVista!=$numIdUsuarioVoto){
        $numIdUsuarioVista=$numIdUsuarioVoto;
        if($objJulgamentoParteDTO->getStrStaSituacaoItem()==ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA){
          $objInfraException->lancarValidacao('Este processo j� est� com pedido de vista.');
        }
      }

      //s� permitir acompanhar relator ou divergencias
      if($strStaVoto==self::$STA_ACOMPANHA && $numIdUsuarioRelator!=$numIdUsuarioVoto){
        $numIdVotoParteAssociado=$objVotoParteDTO->getNumIdVotoParteAssociado();
        if($numIdVotoParteAssociado!=null) {
          $numIdUsuarioAssociado=false;
          foreach ($arrObjVotoParteDTO as $objVotoParteDTO2) {
            if($objVotoParteDTO2->getNumIdVotoParte()==$numIdVotoParteAssociado){
              $numIdUsuarioAssociado=$objVotoParteDTO2->getNumIdUsuario();
            }
          }
          if($numIdUsuarioAssociado===false){
            $objInfraException->lancarValidacao('Voto associado n�o encontrado.');
          }
          if (!isset($arrObjVotoParteDTO[$numIdUsuarioAssociado]) || $arrObjVotoParteDTO[$numIdUsuarioAssociado]->getStrStaVotoParte()!=self::$STA_DIVERGE) {
              $objInfraException->lancarValidacao('S� � poss�vel acompanhar o relator ou diverg�ncias.');
            }
          }
        }
      }

      //voto j� registrado - altera��o
      if (isset($arrObjVotoParteDTO[$numIdUsuarioVoto])) {

        $objVotoParteDTO->setNumIdVotoParte($arrObjVotoParteDTO[$numIdUsuarioVoto]->getNumIdVotoParte());

        //mudou voto
        if ($strStaVoto!=$arrObjVotoParteDTO[$numIdUsuarioVoto]->getStrStaVotoParte()){
          //era divergencia: exclui relacionados
          if ($arrObjVotoParteDTO[$numIdUsuarioVoto]->getStrStaVotoParte()==self::$STA_DIVERGE){
            $this->excluirVotosRelacionados($objVotoParteDTO, $idJulgamentoParte, $arrObjVotoParteDTO);
          }
          //era pedido_vista, excluir pedido de vista e votos 'aguarda vista'
          if($arrObjVotoParteDTO[$numIdUsuarioVoto]->getStrStaVotoParte()==self::$STA_PEDIDO_VISTA){
            $objPedidoVistaDTO=new PedidoVistaDTO();
            $objPedidoVistaDTO->setNumIdUsuario($numIdUsuarioVoto);
            $objPedidoVistaDTO->setStrSinPendente('S');
            $objPedidoVistaDTO->setNumIdDistribuicao($objJulgamentoParteDTO->getNumIdDistribuicaoItemSessaoJulgamento());
            $objPedidoVistaDTO->retNumIdPedidoVista();
            $objPedidoVistaRN=new PedidoVistaRN();
            $objPedidoVistaRN->excluir($objPedidoVistaRN->listar($objPedidoVistaDTO));

            $numIdUsuarioVista=null;
            $objVotoParteDTO2=new VotoParteDTO();
            $objVotoParteDTO2->setNumIdJulgamentoParte($idJulgamentoParte);
            $objVotoParteDTO2->setStrStaVotoParte(self::$STA_AGUARDA_VISTA);
            $objVotoParteDTO2->retNumIdVotoParte();
            $objVotoParteDTO2->retNumIdUsuario();
            $arrObjVotoParteDTO2=$this->listar($objVotoParteDTO2);
            if (InfraArray::contar($arrObjVotoParteDTO)) {
              $this->excluir($arrObjVotoParteDTO2);
              foreach ($arrObjVotoParteDTO2 as $objVotoParteDTO3) {
                unset($arrObjVotoParteDTO[$objVotoParteDTO3->getNumIdUsuario()]);
              }
            }
          }
        }

        $objVotoParteDTO->setDthVoto($dthAtutal);
        if($objVotoParteDTO->getStrStaVotoParte()!=self::$STA_ACOMPANHA){
          $objVotoParteDTO->setNumIdVotoParteAssociado(null);
        } else if($numIdUsuarioRelator!=$numIdUsuarioVoto){
          //n�o � relator e acompanha, desmarca provimento
          $objVotoParteDTO->setNumIdProvimento(null);
        }

        $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
        $objVotoParteBD->alterar($objVotoParteDTO);
      } else {
        $this->registrarInterno($objVotoParteDTO);
        $arrObjVotoParteDTO[$numIdUsuarioVoto]=$objVotoParteDTO;
        $idxObjVotoParteDTO[$numIdUsuarioVoto]=$objVotoParteDTO;
      }
      if($strStaVoto==self::$STA_PEDIDO_VISTA) {
        $objPedidoVistaDTO = new PedidoVistaDTO();
        $objPedidoVistaRN = new PedidoVistaRN();
        $objPedidoVistaDTO->setNumIdDistribuicao($objJulgamentoParteDTO->getNumIdDistribuicaoItemSessaoJulgamento());
        $objPedidoVistaDTO->setStrSinPendente('S');
        $objPedidoVistaDTO->setNumIdUsuario($numIdUsuarioVoto);
        $objPedidoVistaDTO->setNumIdUnidade($idxObjColegiadoComposicaoDTO[$numIdUsuarioVoto]->getNumIdUnidade());
        $objPedidoVistaDTO->setDthPedido($dthAtutal);
        $objPedidoVistaDTO->setDthDevolucao(null);
        $objPedidoVistaRN->cadastrar($objPedidoVistaDTO);
      }
    }

    $objVotoParteDTO=$varObjVotoParteDTO[0];

    $this->desmarcarVotoVencedor($objVotoParteDTO);
    //desmarcar relator de acord�o, houve altera��o de voto
    if($objJulgamentoParteDTO->getNumIdUsuarioRelatorAcordaoDistribuicao()!=null){
      $objDistribuicaoDTO=new DistribuicaoDTO();
      $objDistribuicaoDTO->setNumIdDistribuicao($objJulgamentoParteDTO->getNumIdDistribuicaoItemSessaoJulgamento());
      $objDistribuicaoDTO->setNumIdUnidadeRelatorAcordao(null);
      $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao(null);
      $objDistribuicaoRN=new DistribuicaoRN();
      $objDistribuicaoRN->alterar($objDistribuicaoDTO);
    }
    if ($objJulgamentoParteDTO->getNumIdUsuarioDesempate()!=null){
      $objJulgamentoParteDTO2=new JulgamentoParteDTO();
      $objJulgamentoParteDTO2->setNumIdJulgamentoParte($idJulgamentoParte);
      $objJulgamentoParteDTO2->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
      $objJulgamentoParteDTO2->setNumIdUsuarioDesempate(null);
      $objJulgamentoParteRN=new JulgamentoParteRN();
      $objJulgamentoParteRN->alterar($objJulgamentoParteDTO2);
    }

    if($bolInicioJulgamentoItem){
      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->setStrDispositivo(null); //zerar dispositivo

      if($numIdUsuarioVista!=null){
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA);
      } else {
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO);
      }
      $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO);

      //verifica se deve gerar dispositivo
      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->setArrObjColegiadoComposicaoDTO($arrObjColegiadoComposicaoDTO);
      $objItemSessaoJulgamentoDTO->setArrObjPresencaSessaoDTO($arrObjPresencaSessaoDTO);
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);
      if ($objItemSessaoJulgamentoDTO->getStrSinPermiteFinalizacao()==='S') {
        $strDispositivo=$objItemSessaoJulgamentoRN->gerarTextoDispositivo($objItemSessaoJulgamentoDTO);
        $objItemSessaoJulgamentoDTO2=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
        $objItemSessaoJulgamentoDTO2->setStrDispositivo($strDispositivo);
        $objItemSessaoJulgamentoDTO2->setNumIdUsuarioRelatorAcordaoDistribuicao($objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao());
        $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO2);
      }
    }

    return null;
  }

  protected function cancelarControlado($varObjVotoParteDTO)
  {

    if (!is_array($varObjVotoParteDTO)){
      $varObjVotoParteDTO=array($varObjVotoParteDTO);
    }
    if (InfraArray::contar($varObjVotoParteDTO)==0) {
      return;
    }
    $objInfraException = new InfraException();
    $idJulgamentoParte=$varObjVotoParteDTO[0]->getNumIdJulgamentoParte();
    $idxObjVotoParteDTO=InfraArray::indexarArrInfraDTO($varObjVotoParteDTO,'IdUsuario');
    //votos m�ltiplos
    if(InfraArray::contar($varObjVotoParteDTO)>1) {
      foreach ($varObjVotoParteDTO as $objVotoParteDTO) {
        if($objVotoParteDTO->getNumIdJulgamentoParte()!=$idJulgamentoParte){
          $objInfraException->lancarValidacao('Cancelamento de votos m�ltiplos de partes diferentes n�o s�o permitidos.');
        }
      }
    }
    $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

    $objJulgamentoParteDTO=new JulgamentoParteDTO();
    $objJulgamentoParteRN=new JulgamentoParteRN();
    $objJulgamentoParteDTO->setNumIdJulgamentoParte($idJulgamentoParte);
    $objJulgamentoParteDTO->retDblIdProcedimentoDistribuicao();
    $objJulgamentoParteDTO->retStrStaSituacaoItem();
    $objJulgamentoParteDTO->retStrStaSituacaoSessaoJulgamento();
    $objJulgamentoParteDTO->retStrStaTipoItemSessaoBloco();
    $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdUsuarioRelatorDistribuicao();
    $objJulgamentoParteDTO->retNumIdUsuarioSessaoItem();
    $objJulgamentoParteDTO->retNumIdColegiadoSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdDistribuicaoItemSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdUsuarioDesempate();
    $objJulgamentoParteDTO=$objJulgamentoParteRN->consultar($objJulgamentoParteDTO);

    $numIdUsuarioRelator=$objJulgamentoParteDTO->getNumIdUsuarioRelatorDistribuicao();
    $numIdItemSessaoJulgamento=$objJulgamentoParteDTO->getNumIdItemSessaoJulgamento();

    $objVotoParteDTOBanco=new VotoParteDTO();
    $objVotoParteDTOBanco->setNumIdJulgamentoParte($idJulgamentoParte);
    $objVotoParteDTOBanco->retNumIdVotoParte();
    $objVotoParteDTOBanco->retNumIdUsuario();
    $objVotoParteDTOBanco->retStrStaVotoParte();
    $arrObjVotoParteDTO=$this->listar($objVotoParteDTOBanco);
    if($arrObjVotoParteDTO){
      $arrObjVotoParteDTO=InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO,'IdUsuario');
    }

    $arrObjPresencaSessaoDTO=CacheSessaoJulgamentoRN::getArrObjPresencaSessaoDTO($objJulgamentoParteDTO->getNumIdSessaoJulgamento());

    //Valida��es gerais

    //validacao: excluir voto somente com sessao aberta
    if ($objJulgamentoParteDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){
      $objInfraException->lancarValidacao('S� � poss�vel cancelar voto em Sess�o Aberta.');
    }

    //validacao: n�o pode excluir voto em processo adiado, retirado ou diligencia
    if(in_array($objJulgamentoParteDTO->getStrStaSituacaoItem(),array(ItemSessaoJulgamentoRN::$STA_RETIRADO,ItemSessaoJulgamentoRN::$STA_ADIADO,ItemSessaoJulgamentoRN::$STA_DILIGENCIA))){
      $objInfraException->lancarValidacao('N�o � poss�vel cancelar voto em processo retirado ou adiado.');
    }

    $bolHouveExclusao=false;
    foreach ($idxObjVotoParteDTO as $numIdUsuarioVoto=>$objVotoParteDTO) {
      if (isset($arrObjVotoParteDTO[$numIdUsuarioVoto])) {

        if($numIdUsuarioVoto==$numIdUsuarioRelator){
          $objInfraException->lancarValidacao('N�o � poss�vel cancelar voto do relator.');
        }
      //validacao: s� pemitir cancelar voto de membro presente
        if(!isset($arrObjPresencaSessaoDTO[$numIdUsuarioVoto]) || $arrObjPresencaSessaoDTO[$numIdUsuarioVoto]->getDthSaida()!=null){
          $objInfraException->lancarValidacao('N�o � poss�vel cancelar voto de membro ausente.');
        }

        $objVotoParteDTO->setNumIdVotoParte($arrObjVotoParteDTO[$numIdUsuarioVoto]->getNumIdVotoParte());
        //era divergencia: exclui relacionados
        if ($arrObjVotoParteDTO[$numIdUsuarioVoto]->getStrStaVotoParte()==self::$STA_DIVERGE){
          $this->excluirVotosRelacionados($objVotoParteDTO, $idJulgamentoParte, $arrObjVotoParteDTO);
        }
        //era pedido_vista, excluir votos 'aguarda vista'
        if($arrObjVotoParteDTO[$numIdUsuarioVoto]->getStrStaVotoParte()==self::$STA_PEDIDO_VISTA){
          $objPedidoVistaDTO=new PedidoVistaDTO();
          $objPedidoVistaDTO->setNumIdUsuario($numIdUsuarioVoto);
          $objPedidoVistaDTO->setStrSinPendente('S');
          $objPedidoVistaDTO->setNumIdDistribuicao($objJulgamentoParteDTO->getNumIdDistribuicaoItemSessaoJulgamento());
          $objPedidoVistaDTO->retNumIdPedidoVista();
          $objPedidoVistaRN=new PedidoVistaRN();
          $objPedidoVistaRN->excluir($objPedidoVistaRN->listar($objPedidoVistaDTO));

          $objVotoParteDTO2=new VotoParteDTO();
          $objVotoParteDTO2->setNumIdJulgamentoParte($idJulgamentoParte);
          $objVotoParteDTO2->setStrStaVotoParte(self::$STA_AGUARDA_VISTA);
          $objVotoParteDTO2->retNumIdVotoParte();
          $objVotoParteDTO2->retNumIdUsuario();
          $arrObjVotoParteDTO2=$this->listar($objVotoParteDTO2);
          if (InfraArray::contar($arrObjVotoParteDTO)) {
            $this->excluir($arrObjVotoParteDTO2);
            foreach ($arrObjVotoParteDTO2 as $objVotoParteDTO3) {
              unset($arrObjVotoParteDTO[$objVotoParteDTO3->getNumIdUsuario()]);
            }
          }
        }

        $this->excluir(array($objVotoParteDTO));
        unset($arrObjVotoParteDTO[$numIdUsuarioVoto]);
        $bolHouveExclusao=true;
      }

    }

    if($bolHouveExclusao){
      $objVotoParteDTO=$varObjVotoParteDTO[0];
      $this->desmarcarVotoVencedor($objVotoParteDTO);
      if ($objJulgamentoParteDTO->getNumIdUsuarioDesempate()!=null){
        $objJulgamentoParteDTO2=new JulgamentoParteDTO();
        $objJulgamentoParteDTO2->setNumIdJulgamentoParte($idJulgamentoParte);
        $objJulgamentoParteDTO2->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
        $objJulgamentoParteDTO2->setNumIdUsuarioDesempate(null);
        $objJulgamentoParteRN=new JulgamentoParteRN();
        $objJulgamentoParteRN->alterar($objJulgamentoParteDTO2);
      }

      $staSituacaoItem=$objJulgamentoParteDTO->getStrStaSituacaoItem();
      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
      $objItemSessaoJulgamentoDTO->setStrDispositivo(null); //zerar dispositivo
      if($staSituacaoItem==ItemSessaoJulgamentoRN::$STA_NORMAL){
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO);
      }
      $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO);

    }

    return null;

  }

  protected function alterarRessalvaControlado(VotoParteDTO $objVotoParteDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('voto_parte_alterar_ressalva', __METHOD__, $objVotoParteDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrRessalva($objVotoParteDTO, $objInfraException);

      $objVotoParteDTOBanco = new VotoParteDTO();
      $objVotoParteDTOBanco->retNumIdSessaoJulgamento();
      $objVotoParteDTOBanco->retNumIdVotoParte();
      $objVotoParteDTOBanco->setNumIdVotoParte($objVotoParteDTO->getNumIdVotoParte());
      $objVotoParteDTOBanco = $this->consultar($objVotoParteDTOBanco);

      $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objVotoParteDTOBanco->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTO->retStrStaSituacao();
      $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);

      if ($objSessaoJulgamentoDTO->getStrStaSituacao() != SessaoJulgamentoRN::$ES_ENCERRADA) {
        $objInfraException->lancarValidacao('Edi��o de ressalva permitida somente com Sess�o Encerrada.');
      }

      $objVotoParteDTOBanco->setStrRessalva($objVotoParteDTO->getStrRessalva());

      $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
      $objVotoParteBD->alterar($objVotoParteDTO);
      return $objVotoParteDTO;

    }catch(Exception $e){
      throw new InfraException('Erro alterando ressalva do voto.',$e);
    }

  }
  protected function excluirControlado($arrObjVotoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('voto_parte_excluir',__METHOD__,$arrObjVotoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $arrIdJulgamentoParte=array();
      $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
      $arrNumIdVotoParte=InfraArray::converterArrInfraDTO($arrObjVotoParteDTO,'IdVotoParte');

      if(count($arrNumIdVotoParte)==0){
        return;
      }

      $objVotoParteDTOBanco=new VotoParteDTO();
      $objVotoParteDTOBanco->setNumIdVotoParte($arrNumIdVotoParte,InfraDTO::$OPER_IN);
      $objVotoParteDTOBanco->retNumIdJulgamentoParte();
      $objVotoParteDTOBanco->retNumIdVotoParte();
      $objVotoParteDTOBanco->retNumIdVotoParteAssociado();
      $arrObjVotoParteDTOBanco=$this->listar($objVotoParteDTOBanco);

      //excluir primeiro os votos relacionados
      foreach ($arrObjVotoParteDTOBanco as $chave=>$objVotoParteDTO) {
        $arrIdJulgamentoParte[$objVotoParteDTO->getNumIdJulgamentoParte()]=1;
        if($objVotoParteDTO->getNumIdVotoParteAssociado()!=null){
          $objVotoParteBD->excluir($objVotoParteDTO);
          unset($arrObjVotoParteDTOBanco[$chave]);
        }
      }

      //excluir os outros votos
      foreach ($arrObjVotoParteDTOBanco as $chave=>$objVotoParteDTO) {
        $objVotoParteBD->excluir($objVotoParteDTO);
      }

      foreach ($arrIdJulgamentoParte as $numIdJulgamentoParte=>$valor) {
        $objVotoParteDTO=new VotoParteDTO();
        $objVotoParteDTO->setNumIdJulgamentoParte($numIdJulgamentoParte);
        $this->desmarcarVotoVencedor($objVotoParteDTO);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Voto.',$e);
    }
  }
  protected function consultarConectado(VotoParteDTO $objVotoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('voto_parte_consultar',__METHOD__,$objVotoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
      $ret = $objVotoParteBD->consultar($objVotoParteDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Voto.',$e);
    }
  }
  protected function listarConectado(VotoParteDTO $objVotoParteDTO) {
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('voto_parte_listar',__METHOD__,$objVotoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
      $ret = $objVotoParteBD->listar($objVotoParteDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Votos.',$e);
    }
  }
  protected function contarConectado(VotoParteDTO $objVotoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('voto_parte_listar',__METHOD__,$objVotoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());
      $ret = $objVotoParteBD->contar($objVotoParteDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Votos.',$e);
    }
  }
  protected function copiarVotosControlado(array $arrObjVotoParteDTO)
  {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('voto_parte_cadastrar',__METHOD__,$arrObjVotoParteDTO);
      $objInfraException = new InfraException();
      $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());

      $arrNormal=array();
      $arrAssociado=array();
      foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
        $this->validarNumIdJulgamentoParte($objVotoParteDTO, $objInfraException);
        $this->validarNumIdVotoParteAssociado($objVotoParteDTO, $objInfraException);
        $this->validarStrRessalva($objVotoParteDTO, $objInfraException);
        $this->validarStrStaVotoParte($objVotoParteDTO, $objInfraException);
        $this->validarDthVoto($objVotoParteDTO, $objInfraException);
        $this->validarNumIdUsuario($objVotoParteDTO, $objInfraException);
        $objVotoParteDTO->setStrSinVencedor('N');

        $objInfraException->lancarValidacoes();
        if ($objVotoParteDTO->getStrStaVotoParte() != self::$STA_ACOMPANHA) {
          $objVotoParteDTO->setNumIdVotoParteAssociado(null);
        }

        if($objVotoParteDTO->getNumIdVotoParteAssociado()==null){
          $arrNormal[$objVotoParteDTO->getNumIdVotoParte()]=$objVotoParteDTO;
        } else {
          $arrAssociado[]=$objVotoParteDTO;
        }
      }

      foreach ($arrNormal as $id=>$objVotoParteDTO) {
        if($objVotoParteDTO->getStrStaVotoParte()==self::$STA_AGUARDA_VISTA){
          continue;
        }
        $ret=$objVotoParteBD->cadastrar($objVotoParteDTO);
        $arrNormal[$id]=$ret;
      }

      foreach ($arrAssociado as $objVotoParteDTO) {
        $numIdVotoParteAssociado=$objVotoParteDTO->getNumIdVotoParteAssociado();
        $idNovo=$arrNormal[$numIdVotoParteAssociado]->getNumIdVotoParte();
        $objVotoParteDTO->setNumIdVotoParteAssociado($idNovo);
        $objVotoParteBD->cadastrar($objVotoParteDTO);
      }

      //Auditoria


    }catch(Exception $e){
      throw new InfraException('Erro copiando Votos.',$e);
    }
  }

  protected function registrarVotoVencedorControlado(VotoParteDTO $objVotoParteDTO)
  {
    $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());

    $objVotoParteDTOBanco=new VotoParteDTO();
    $objVotoParteDTOBanco->setNumIdJulgamentoParte($objVotoParteDTO->getNumIdJulgamentoParte());
    $objVotoParteDTOBanco->setStrSinVencedor('S');
    $objVotoParteDTOBanco->retNumIdVotoParte();
    $objVotoParteDTOBanco=$this->consultar($objVotoParteDTOBanco);
    if ($objVotoParteDTOBanco!=null){
      if ($objVotoParteDTOBanco->getNumIdVotoParte()==$objVotoParteDTO->getNumIdVotoParte()){
        return;
      }
      $objVotoParteDTOBanco->setStrSinVencedor('N');
      $objVotoParteBD->alterar($objVotoParteDTOBanco);
    }
    $objVotoParteDTOBanco=new VotoParteDTO();
    $objVotoParteDTOBanco->setNumIdVotoParte($objVotoParteDTO->getNumIdVotoParte());
    $objVotoParteDTOBanco->setNumIdJulgamentoParte($objVotoParteDTO->getNumIdJulgamentoParte());
    $objVotoParteDTOBanco->retNumIdVotoParte();
    $objVotoParteDTOBanco=$this->consultar($objVotoParteDTOBanco);
    if ($objVotoParteDTOBanco==null){
      throw new InfraException('Erro consultando voto.');
    } elseif ($objVotoParteDTOBanco)
    $objVotoParteDTOBanco->setStrSinVencedor('S');
    $objVotoParteBD->alterar($objVotoParteDTOBanco);

  }
  protected function desmarcarVotoVencedorControlado(VotoParteDTO $objVotoParteDTO)
  {
    $objVotoParteBD = new VotoParteBD($this->getObjInfraIBanco());

    $objVotoParteDTOBanco=new VotoParteDTO();
    $objVotoParteDTOBanco->setNumIdJulgamentoParte($objVotoParteDTO->getNumIdJulgamentoParte());
    $objVotoParteDTOBanco->setStrSinVencedor('S');
    $objVotoParteDTOBanco->retNumIdVotoParte();
    $objVotoParteDTOBanco=$this->consultar($objVotoParteDTOBanco);
    if ($objVotoParteDTOBanco!=null){
      $objVotoParteDTOBanco->setStrSinVencedor('N');
      $objVotoParteBD->alterar($objVotoParteDTOBanco);
    }

  }

  /**
   * exclui votos relacionados (acompanham relator/divergencia) do banco e do array interno informado
   * @param $objVotoParteDTO
   * @param $idJulgamentoParte
   * @param array $arrObjVotoParteDTO
   */
  private function excluirVotosRelacionados($objVotoParteDTO, $idJulgamentoParte, array &$arrObjVotoParteDTO): void
  {
    $objVotoParteDTO2 = new VotoParteDTO();
    $objVotoParteDTO2->setNumIdVotoParteAssociado($objVotoParteDTO->getNumIdVotoParte());
    $objVotoParteDTO2->setNumIdJulgamentoParte($idJulgamentoParte);
    $objVotoParteDTO2->retNumIdVotoParte();
    $objVotoParteDTO2->retNumIdUsuario();
    $arrObjVotoParteDTO2 = $this->listar($objVotoParteDTO2);
    if (InfraArray::contar($arrObjVotoParteDTO2)) {
      $this->excluir($arrObjVotoParteDTO2);
      foreach ($arrObjVotoParteDTO2 as $objVotoParteDTO3) {
        unset($arrObjVotoParteDTO[$objVotoParteDTO3->getNumIdUsuario()]);
      }
    }
  }
}
?>