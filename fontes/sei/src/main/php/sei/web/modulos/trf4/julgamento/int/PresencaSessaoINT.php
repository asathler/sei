<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PresencaSessaoINT extends InfraINT {

  public static function montarSelectIdPresencaSessao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdUsuario='', $numIdSessaoJulgamento=''){
    $objPresencaSessaoDTO = new PresencaSessaoDTO();
    $objPresencaSessaoDTO->retNumIdPresencaSessao();

    if ($numIdUsuario!==''){
      $objPresencaSessaoDTO->setNumIdUsuario($numIdUsuario);
    }

    if ($numIdSessaoJulgamento!==''){
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
    }

    $objPresencaSessaoDTO->setOrdNumIdPresencaSessao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objPresencaSessaoRN = new PresencaSessaoRN();
    $arrObjPresencaSessaoDTO = $objPresencaSessaoRN->listar($objPresencaSessaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjPresencaSessaoDTO, 'IdPresencaSessao', 'IdPresencaSessao');
  }

  public static function montarSelectTrocaPresidente($numIdSessaoJulgamento,$idUsuarioPresidenteAtual){
    $objPresencaSessaoDTO=new PresencaSessaoDTO();
    $objPresencaSessaoRN=new PresencaSessaoRN();
    $objPresencaSessaoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
    $objPresencaSessaoDTO->setDthSaida(null);
    $objPresencaSessaoDTO->setNumIdUsuario($idUsuarioPresidenteAtual,InfraDTO::$OPER_DIFERENTE);
    $objPresencaSessaoDTO->retNumIdUsuario();
    $objPresencaSessaoDTO->retStrNomeUsuario();
    $objPresencaSessaoDTO->setOrdStrNomeUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);
    $arrObjPresencaSessaoDTO=$objPresencaSessaoRN->listar($objPresencaSessaoDTO);


    return InfraINT::montarSelectArrInfraDTO('null','&nbsp;',null,$arrObjPresencaSessaoDTO,'IdUsuario','NomeUsuario');
  }
}
?>