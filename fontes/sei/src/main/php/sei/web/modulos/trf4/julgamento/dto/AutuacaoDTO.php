<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class AutuacaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'autuacao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdAutuacao','id_autuacao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL, 'IdProcedimento','id_procedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Descricao','descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdTipoMateria','id_tipo_materia');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'IdxAutuacao','idx_autuacao');


    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoTipoMateria','descricao','tipo_materia');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR, 'ObjParteProcedimentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR, 'ObjRelAutuacaoTipoMateriaDTO');

    $this->configurarPK('IdAutuacao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdProcedimento', 'procedimento', 'id_procedimento');
    $this->configurarFK('IdTipoMateria', 'tipo_materia', 'id_tipo_materia',InfraDTO::$TIPO_FK_OPCIONAL);

  }
}
?>