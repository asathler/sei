<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoVersaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'colegiado_versao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdColegiadoVersao','id_colegiado_versao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdColegiado','id_colegiado');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Versao','dth_versao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinUltima','sin_ultima');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidade','id_unidade');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinEditavel','sin_editavel');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Nome','nome');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Sigla','sigla');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'QuorumMinimo','quorum_minimo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdTipoProcedimento','id_tipo_procedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioSecretario','id_usuario_secretario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioPresidente','id_usuario_presidente');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidadeResponsavel','id_unidade_responsavel');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaAlgoritmoDistribuicao','sta_algoritmo_distribuicao');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeColegiado','nome','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaColegiado','sigla','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaAlgoritmoDistribuicaoColegiado','sta_algoritmo_distribuicao','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'QuorumMinimoColegiado','quorum_minimo','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUsuario','sigla','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeSecretario','u3.nome','usuario u3');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomePresidente','u2.nome','usuario u2');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidadeResponsavel','un2.sigla','unidade un2');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'Distribuicoes');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'Sessoes');

    $this->configurarPK('IdColegiadoVersao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdColegiado', 'colegiado', 'id_colegiado');
    $this->configurarFK('IdUnidade', 'unidade', 'id_unidade');
    $this->configurarFK('IdUsuario', 'usuario', 'id_usuario');
    $this->configurarFK('IdUsuarioSecretario','usuario u3','u3.id_usuario');
    $this->configurarFK('IdUsuarioPresidente','usuario u2','u2.id_usuario');
    $this->configurarFK('IdUnidadeResponsavel','unidade un2','un2.id_unidade');

  }
}
?>