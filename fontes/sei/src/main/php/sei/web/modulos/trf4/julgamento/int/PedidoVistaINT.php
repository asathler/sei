<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 11/11/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PedidoVistaINT extends InfraINT {

  public static function montarSelectPedido($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdDistribuicao='', $numIdUsuario=''){
    $objPedidoVistaDTO = new PedidoVistaDTO();
    $objPedidoVistaDTO->retNumIdPedidoVista();
    $objPedidoVistaDTO->retDthPedido();

    if ($numIdDistribuicao!==''){
      $objPedidoVistaDTO->setNumIdDistribuicao($numIdDistribuicao);
    }

    if ($numIdUsuario!==''){
      $objPedidoVistaDTO->setNumIdUsuario($numIdUsuario);
    }

    $objPedidoVistaDTO->setOrdDthPedido(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objPedidoVistaRN = new PedidoVistaRN();
    $arrObjPedidoVistaDTO = $objPedidoVistaRN->listar($objPedidoVistaDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjPedidoVistaDTO, 'IdPedidoVista', 'Pedido');
  }
}
?>