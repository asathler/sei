<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 22/07/2014 - criado por bcu
*
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
//  InfraDebug::getInstance()->setBolLigado(false);
//  InfraDebug::getInstance()->setBolDebugInfra(false);
//  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $strParametros = '';
  if(isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
    $strParametros .= '&arvore='.$_GET['arvore'];
  }

  if (isset($_GET['id_procedimento'])){
    $strParametros .= '&id_procedimento='.$_GET['id_procedimento'];
  }

  if (isset($_GET['id_documento'])){
    $strParametros .= '&id_documento='.$_GET['id_documento'];
  }

  $objAtribuirDTO = new AtribuirDTO();

  switch($_GET['acao']){

    case 'julgamento_listar':
      $strTitulo = 'Processos em Julgamento';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();

  $numRegistros = 0;


  $objDistribuicaoDTO=new DistribuicaoDTO();
  $objDistribuicaoRN=new DistribuicaoRN();

  PaginaSEI::getInstance()->prepararOrdenacao($objDistribuicaoDTO, 'IdProcedimento', InfraDTO::$TIPO_ORDENACAO_ASC);
  PaginaSEI::getInstance()->prepararPaginacao($objDistribuicaoDTO);

  $arrObjDistribuicaoDTO=$objDistribuicaoRN->listarParaJulgamento($objDistribuicaoDTO);

  PaginaSEI::getInstance()->processarPaginacao($objDistribuicaoDTO);

  $arrObjEstadoDistribuicao=InfraArray::indexarArrInfraDTO($objDistribuicaoRN->listarValoresEstadoDistribuicao(),'StaDistribuicao');

  $numRegistros =  InfraArray::contar($arrObjDistribuicaoDTO);

  $arrRetIconeIntegracao = null;
  $bolAcaoAndamentoSituacaoGerenciar = SessaoSEI::getInstance()->verificarPermissao('andamento_situacao_gerenciar');
  $bolAcaoAndamentoMarcadorGerenciar = SessaoSEI::getInstance()->verificarPermissao('andamento_marcador_gerenciar');

  if ($numRegistros > 0){
    $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();

    //$arrComandos[] = '<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="infraImprimirTabela();" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir</button>';

    $bolAcaoRegistrarAnotacao = SessaoSEI::getInstance()->verificarPermissao('anotacao_registrar');

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Processos.';
    $strCaptionTabela = 'Processos';

    $strResultado .= '<table width="91%" id="tblProcessosDetalhado" class="infraTable tabelaProcessos" summary="'.$strSumarioTabela.'">'."\n"; //81
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    //$strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    $strResultado .= '<th class="infraTh" align="center" width="30%" colspan="2">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Processo','IdProcedimento',$arrObjDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" align="center">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Colegiado','NomeColegiado',$arrObjDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" align="center">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Situa��o','StaDistribuicao',$arrObjDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" align="center" width="17%">'.PaginaSEI::getInstance()->getThOrdenacao($objDistribuicaoDTO,'Data da Sess�o','SessaoJulgamento',$arrObjDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    foreach($arrObjDistribuicaoDTO as $objDistribuicaoDTO2){
      /* @var $objDistribuicaoDTO2 DistribuicaoDTO*/

      $objProcedimentoDTO=$objDistribuicaoDTO2->getObjProcedimentoDTO();
      $numTipoVisualizacao=0;



    	$strResultado .= '<tr class="infraTrClara trProcesso">';
    	$strCorProcesso = ' class="'.($objProcedimentoDTO->getStrSinAberto()=='S'?'protocoloAberto':'protocoloFechado').'"';

      //$strResultado .= '<td valign="center">'.PaginaSEI::getInstance()->getTrCheck($i,$objProtocoloDTO->getDblIdProtocolo(),$objProtocoloDTO->getStrProtocoloFormatado()).'</td>';

      $strResultado .= '<td align="center">';
      $strResultado .= AnotacaoINT::montarIconeAnotacao($objProcedimentoDTO->getObjAnotacaoDTO(),$bolAcaoRegistrarAnotacao, $objDistribuicaoDTO2->getDblIdProcedimento());
      $strResultado .= ProcedimentoINT::montarIconeVisualizacao($numTipoVisualizacao, $objProcedimentoDTO, $arrRetIconeIntegracao,$bolAcaoAndamentoSituacaoGerenciar,$bolAcaoAndamentoMarcadorGerenciar);

      $strResultado .= '</td>';
      $strResultado .= '<td align="center"><a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_procedimento='.$objDistribuicaoDTO2->getDblIdProcedimento()).'" target="_blank" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'" '.PaginaSEI::montarTitleTooltip($objProcedimentoDTO->getStrDescricaoProtocolo(),$objProcedimentoDTO->getStrNomeTipoProcedimento()).' '.$strCorProcesso.'>'.$objProcedimentoDTO->getStrProtocoloProcedimentoFormatado().'</a></td>'."\n";

      $strResultado .= '<td align="left">'.PaginaSEI::tratarHTML($objDistribuicaoDTO2->getStrNomeColegiado()).'</td>'."\n";
      $staDistribuicao=$objDistribuicaoDTO2->getStrStaDistribuicao();
      $strResultado .= '<td align="left">'.PaginaSEI::tratarHTML($arrObjEstadoDistribuicao[$staDistribuicao]->getStrDescricao());
      if ($objDistribuicaoDTO2->isSetObjPedidoVistaDTO()){
        /* @var $objPedidoVistaDTO PedidoVistaDTO*/
        $objPedidoVistaDTO=$objDistribuicaoDTO2->getObjPedidoVistaDTO();
          $strResultado.=' por <b>'.$objPedidoVistaDTO->getStrNomeUsuario().'</b>';
      }
//      if (in_array($arrObjDistribuicaoDTO[$i]->getStrStaDistribuicao(),array(DistribuicaoRN::$STA_DISTRIBUIDO,DistribuicaoRN::$STA_REDISTRIBUIDO)) && $arrObjDistribuicaoDTO[$i]->getStrSinProntoPauta()=='S'){
//        $strResultado .= ' (Pronto para Pautar)';
//      }
      $strResultado .= '</td>'."\n";
      $strResultado .= '<td align="left">'.$objDistribuicaoDTO2->getDthSessaoJulgamento().'</td>'."\n";
      $strResultado .= '</tr>'."\n";
    }
    $strResultado .= '</table>';
  }

  //$arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_procedimento'])).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

table.tabelaProcessos {
background-color:white;
border:0px solid white;
border-spacing:0;
}

table.tabelaProcessos tr{
margin:0;
border:0;
padding:0;
}

th.tituloProcessos{
font-size:1em;
font-weight: bold;
text-align: center;
color: #000;
background-color: #dfdfdf;
border-spacing: 0;
}

tr.trProcesso td{
border-bottom:1px dotted #666;
padding:.3em;
}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>

function inicializar(){
  infraEfeitoTabelas();
}

function abrirProcesso(link){
  document.getElementById('divInfraBarraComandosSuperior').style.visibility = 'hidden';
  document.getElementById('divInfraAreaTabela').style.visibility = 'hidden';
  infraOcultarMenuSistemaEsquema();
  document.getElementById('frmProcedimentoCredencialLista').action = link;
  document.getElementById('frmProcedimentoCredencialLista').submit();
}

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmProcedimentoCredencialLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  //PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>