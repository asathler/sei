<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class AusenciaSessaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdUsuario(AusenciaSessaoDTO $objAusenciaSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAusenciaSessaoDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
    }
  }

  private function validarNumIdSessaoJulgamento(AusenciaSessaoDTO $objAusenciaSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAusenciaSessaoDTO->getNumIdSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Sess�o de Julgamento n�o informada.');
    }
  }

  private function validarNumIdMotivoAusencia(AusenciaSessaoDTO $objAusenciaSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAusenciaSessaoDTO->getNumIdMotivoAusencia())){
      $objInfraException->adicionarValidacao('Motivo de Aus�ncia n�o informado.');
    }
  }

  protected function registrarControlado(AusenciaSessaoDTO $objAusenciaSessaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('ausencia_sessao_registrar',__METHOD__,$objAusenciaSessaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdUsuario($objAusenciaSessaoDTO, $objInfraException);
      $this->validarNumIdSessaoJulgamento($objAusenciaSessaoDTO, $objInfraException);

      $objPresencaSessaoDTO=new PresencaSessaoDTO();
      $objPresencaSessaoRN=new PresencaSessaoRN();
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objAusenciaSessaoDTO->getNumIdSessaoJulgamento());
      $objPresencaSessaoDTO->setDthSaida($objPresencaSessaoDTO->getObjInfraAtributoDTO('InicioSessaoJulgamento'),InfraDTO::$OPER_MAIOR);
      $objPresencaSessaoDTO->setNumIdUsuario($objAusenciaSessaoDTO->getNumIdUsuario());
      $objPresencaSessaoDTO->retNumIdPresencaSessao();
      $bolObrigatorio=($objPresencaSessaoRN->contar($objPresencaSessaoDTO)==0);

      if ($bolObrigatorio) {
        $this->validarNumIdMotivoAusencia($objAusenciaSessaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objAusenciaSessaoDTO->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTO->setStrStaSituacao(SessaoJulgamentoRN::$ES_ENCERRADA);
      if($objSessaoJulgamentoRN->contar($objSessaoJulgamentoDTO)==0){
        $objInfraException->lancarValidacao('Somente pode registrar motivo de aus�ncia com sess�o encerrada.');
      }

      $objAusenciaSessaoBD = new AusenciaSessaoBD($this->getObjInfraIBanco());
      $objAusenciaSessaoDTOBanco=new AusenciaSessaoDTO();
      $objAusenciaSessaoDTOBanco->setNumIdSessaoJulgamento($objAusenciaSessaoDTO->getNumIdSessaoJulgamento());
      $objAusenciaSessaoDTOBanco->setNumIdUsuario($objAusenciaSessaoDTO->getNumIdUsuario());
      $objAusenciaSessaoDTOBanco->retNumIdAusenciaSessao();
      $objAusenciaSessaoDTOBanco=$this->consultar($objAusenciaSessaoDTOBanco);
      if ($objAusenciaSessaoDTOBanco){
        if (!$bolObrigatorio && $objAusenciaSessaoDTO->getNumIdMotivoAusencia()==null){
          $this->excluir(array($objAusenciaSessaoDTOBanco));
          return null;
        } else {
          $objAusenciaSessaoDTO->setNumIdAusenciaSessao($objAusenciaSessaoDTOBanco->getNumIdAusenciaSessao());
          $objAusenciaSessaoBD->alterar($objAusenciaSessaoDTO);
          return $objAusenciaSessaoDTO;
        }
      }
      if ($bolObrigatorio || $objAusenciaSessaoDTO->getNumIdMotivoAusencia()!=null) {
        $ret = $objAusenciaSessaoBD->cadastrar($objAusenciaSessaoDTO);
      } else {
        return null;
      }
      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Aus�ncia.',$e);
    }
  }

  protected function excluirControlado($arrObjAusenciaSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('ausencia_sessao_excluir',__METHOD__,$arrObjAusenciaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAusenciaSessaoBD = new AusenciaSessaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjAusenciaSessaoDTO);$i++){
        $objAusenciaSessaoBD->excluir($arrObjAusenciaSessaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Aus�ncia.',$e);
    }
  }

  protected function consultarConectado(AusenciaSessaoDTO $objAusenciaSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('ausencia_sessao_consultar',__METHOD__,$objAusenciaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAusenciaSessaoBD = new AusenciaSessaoBD($this->getObjInfraIBanco());
      $ret = $objAusenciaSessaoBD->consultar($objAusenciaSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Aus�ncia.',$e);
    }
  }

  protected function listarConectado(AusenciaSessaoDTO $objAusenciaSessaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('ausencia_sessao_listar',__METHOD__,$objAusenciaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAusenciaSessaoBD = new AusenciaSessaoBD($this->getObjInfraIBanco());
      $ret = $objAusenciaSessaoBD->listar($objAusenciaSessaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Aus�ncias.',$e);
    }
  }

  protected function contarConectado(AusenciaSessaoDTO $objAusenciaSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('ausencia_sessao_listar',__METHOD__,$objAusenciaSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAusenciaSessaoBD = new AusenciaSessaoBD($this->getObjInfraIBanco());
      $ret = $objAusenciaSessaoBD->contar($objAusenciaSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Aus�ncias.',$e);
    }
  }
}
?>