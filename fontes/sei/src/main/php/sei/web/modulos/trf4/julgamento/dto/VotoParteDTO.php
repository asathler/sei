<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class VotoParteDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'voto_parte';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdJulgamentoParte','id_julgamento_parte');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdVotoParte','id_voto_parte');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdVotoParteAssociado','id_voto_parte_associado');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Ressalva','ressalva');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaVotoParte','sta_voto_parte');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Voto','dth_voto');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdSessaoVoto','id_sessao_voto');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinVencedor','sin_vencedor');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdProvimento','id_provimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Complemento','complemento');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoJulgamentoParte','descricao','julgamento_parte');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdItemSessaoJulgamento','id_item_sessao_julgamento','julgamento_parte');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamento','id_sessao_julgamento','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaSituacaoItemSessaoJulgamento','sta_situacao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaSituacaoSessaoJulgamento','sta_situacao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoVersaoSessaoJulgamentoVoto','s2.id_colegiado_versao','sessao_julgamento s2');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdDistribuicaoItem','id_distribuicao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DBL,'IdProcedimentoDistribuicao','id_procedimento','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUsuarioRelatorDistribuicao','id_usuario_relator','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUsuarioVotoParte','ar.id_usuario','voto_parte ar');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuarioAssociado','u2.nome','usuario u2');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'ConteudoProvimento','conteudo','provimento');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'DescricaoVoto');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_OBJ,'ItemSessaoJulgamentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'Ordem');

    $this->configurarPK('IdVotoParte',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdJulgamentoParte', 'julgamento_parte', 'id_julgamento_parte',InfraDTO::$TIPO_FK_OBRIGATORIA,InfraDTO::$FILTRO_FK_WHERE);
    $this->configurarFK('IdItemSessaoJulgamento','item_sessao_julgamento','id_item_sessao_julgamento');
    $this->configurarFK('IdSessaoJulgamento','sessao_julgamento','id_sessao_julgamento');
    $this->configurarFK('IdSessaoVoto','sessao_julgamento s2','s2.id_sessao_julgamento');
    $this->configurarFK('IdUsuario','usuario','id_usuario');
    $this->configurarFK('IdProvimento','provimento','id_provimento',InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdDistribuicaoItem','distribuicao','id_distribuicao');
    $this->configurarFK('IdVotoParteAssociado', 'voto_parte ar', 'ar.id_voto_parte',InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdUsuarioVotoParte','usuario u2','u2.id_usuario',InfraDTO::$TIPO_FK_OPCIONAL);
  }
}
?>