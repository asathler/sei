<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 14/10/2014 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.12.0
 */

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->salvarCamposPost(array('hdnIdUnidade',
      'txtUnidade',
      'hdnIdContato',
      'txtContato',
      'txtAutuacao',
      'selColegiado',
      'selTipoMateria',
      'txtDataFinal',
      'txtDataInicial',
      'selTipoProcedimento',
      'hdnTipo'
  ));

  switch($_GET['acao']){

    case 'painel_distribuicao_listar':
    case 'painel_distribuicao_gerar_grafico':
      $strTitulo = 'Painel de Distribui��o';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


  $strParametros='';
  $objDistribuicaoRN=new DistribuicaoRN();

  $arrComandos = array();
  $bolGlobal = SessaoSEI::getInstance()->verificarPermissao('painel_distribuicao_global');
  $bolAcaoRegistrarAnotacao = SessaoSEI::getInstance()->verificarPermissao('anotacao_registrar');



  $strIdUnidade = PaginaSEI::getInstance()->recuperarCampo('hdnIdUnidade');
  $strNomeUnidade = PaginaSEI::getInstance()->recuperarCampo('txtUnidade');
  $numIdColegiado = PaginaSEI::getInstance()->recuperarCampo('selColegiado');
  $strNomeContato = PaginaSEI::getInstance()->recuperarCampo('txtContato');
  $strDescricaoAutuacao = PaginaSEI::getInstance()->recuperarCampo('txtAutuacao');
  $strIdContato = PaginaSEI::getInstance()->recuperarCampo('hdnIdContato');
  $strDataFinal = PaginaSEI::getInstance()->recuperarCampo('txtDataFinal');
  $strDataInicial = PaginaSEI::getInstance()->recuperarCampo('txtDataInicial');
  $numIdTipoProcedimento = PaginaSEI::getInstance()->recuperarCampo('selTipoProcedimento', 'null');
  $numIdTipoMateria=PaginaSEI::getInstance()->recuperarCampo('selTipoMateria', 'null');
  $strHdnTipo = PaginaSEI::getInstance()->recuperarCampo('hdnTipo');

  if ($numIdColegiado=='null' || $numIdColegiado=='') {
    $numIdColegiado=null;
  }

  $arrObjEstadoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
  $arrObjEstadoDistribuicao=InfraArray::indexarArrInfraDTO($arrObjEstadoDistribuicaoDTO,'StaDistribuicao');
  unset($arrObjEstadoDistribuicao[DistribuicaoRN::$STA_CANCELADO]);

  if(isset($_POST['selStaDistribuicao'])){
    $arrStaDistribuicao=$_POST['selStaDistribuicao'];
    if (!is_array($arrStaDistribuicao)){
      $arrStaDistribuicao=array($arrStaDistribuicao);
    }
  } else {
    $arrStaDistribuicao=InfraArray::converterArrInfraDTO($arrObjEstadoDistribuicao,'StaDistribuicao');
  }


  if (SessaoSEI::getInstance()->verificarPermissao('painel_distribuicao_listar')){
    $arrComandos[] = '<button type="button" accesskey="P" id="sbmPesquisar" name="sbmPesquisar" onclick="processar(\'P\',\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=painel_distribuicao_listar&acao_origem='.$_GET['acao']).'\');" value="Pesquisar" class="infraButton"><span class="infraTeclaAtalho">P</span>esquisar Processos</button>';
  }

  if (SessaoSEI::getInstance()->verificarPermissao('painel_distribuicao_gerar_grafico')){
    $arrComandos[] = '<button type="button" accesskey="G" id="sbmGerarGrafico" name="sbmGerarGrafico" onclick="processar(\'G\',\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=painel_distribuicao_gerar_grafico&acao_origem='.$_GET['acao']).'\');" value="Gerar Gr�ficos" class="infraButton"><span class="infraTeclaAtalho">G</span>erar Gr�ficos</button>';
  }

  $arrComandos[] = '<button type="button" accesskey="L" id="btnLimpar" name="btnLimpar" onclick="limpar();" value="Limpar" class="infraButton"><span class="infraTeclaAtalho">L</span>impar Crit�rios</button>';

  $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
  $objPainelDistribuicaoDTO = new PainelDistribuicaoDTO();
  if (!$bolGlobal) {
    $objPainelDistribuicaoDTO->setNumIdUnidadeRelator($numIdUnidadeAtual);
  } else if ($strIdUnidade!='') {
    $objPainelDistribuicaoDTO->setNumIdUnidadeRelator($strIdUnidade);
  }

  if(!InfraString::isBolVazia($strDataInicial)) {
    $objPainelDistribuicaoDTO->adicionarCriterio(array('SituacaoAtual'),array(InfraDTO::$OPER_MAIOR_IGUAL),array($strDataInicial),null);
  }
  if(!InfraString::isBolVazia($strDataFinal)) {
    $objPainelDistribuicaoDTO->adicionarCriterio(array('SituacaoAtual'),array(InfraDTO::$OPER_MENOR_IGUAL),array($strDataFinal.' 23:59:59'),null);
  }
  if($numIdTipoProcedimento!='null'){
    $objPainelDistribuicaoDTO->setNumIdTipoProcedimento($numIdTipoProcedimento);
  }
  if($numIdTipoMateria!='null'){
    $objPainelDistribuicaoDTO->setNumIdTipoMateria($numIdTipoMateria);
  }
  if(!InfraString::isBolVazia($strDescricaoAutuacao)){
    $objPainelDistribuicaoDTO->setStrPalavrasPesquisa($strDescricaoAutuacao);

  }
  if($strIdContato){
    $objPainelDistribuicaoDTO->setStrStaParticipacaoParticipante(ParticipanteRN::$TP_INTERESSADO);
    $objPainelDistribuicaoDTO->setNumIdContatoParticipante($strIdContato);
  }
  $objPainelDistribuicaoDTO->setStrStaUltimo(array(DistribuicaoRN::$SD_ULTIMA,DistribuicaoRN::$SD_ULTIMA_JULGADO),InfraDTO::$OPER_IN);
  if (!$bolGlobal) {
    $objPainelDistribuicaoDTO->retDblIdProcedimento();
    $objPainelDistribuicaoDTO->retStrNomeUsuarioRelator();
    $objPainelDistribuicaoDTO->retDthSituacaoAtual();
    $objPainelDistribuicaoDTO->retStrProtocoloFormatado();
    $objPainelDistribuicaoDTO->retStrStaNivelAcessoGlobal();
  } else {
    $objPainelDistribuicaoDTO->retNumIdUnidadeRelator();
    $objPainelDistribuicaoDTO->setNumIdUnidadeResponsavelColegiado($numIdUnidadeAtual);
  }
  if ($numIdColegiado!=null){
    $objPainelDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
  }
  $objPainelDistribuicaoDTO->retStrSiglaUnidadeRelator();
  $objPainelDistribuicaoDTO->retStrDescricaoUnidadeRelator();
  $objPainelDistribuicaoDTO->retStrStaDistribuicao();
  $objPainelDistribuicaoDTO->retNumIdUnidadeRelator();
  $objPainelDistribuicaoDTO->retStrNomeColegiado();
  $objPainelDistribuicaoDTO->retStrSiglaColegiado();
  $objPainelDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();


  $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
  $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();

  if($numIdColegiado!=null){
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
  }
  $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
  $objColegiadoComposicaoDTO->retNumIdUnidade();
  $objColegiadoComposicaoDTO->retNumIdUsuario();
  $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
  $objColegiadoComposicaoDTO->setDistinct(true);
  $objColegiadoComposicaoDTO->setOrdNumIdColegiadoColegiadoVersao(InfraDTO::$TIPO_ORDENACAO_ASC);

  $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
  $arrColegiados=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdColegiadoColegiadoVersao',true);
  foreach ($arrColegiados as $idColegiado=>$arrComposicao) {
    $arrColegiados[$idColegiado]=InfraArray::converterArrInfraDTO($arrComposicao,'IdUsuario','IdUnidade');
  }

  $strItensSelTipoProcedimento 	= TipoProcedimentoINT::montarSelectNome('null','&nbsp;',$numIdTipoProcedimento);



  if(InfraArray::contar($arrStaDistribuicao)>0){
    $objPainelDistribuicaoDTO->setStrStaDistribuicao($arrStaDistribuicao,InfraDTO::$OPER_IN);
  }


  $numRegistros = 0;


  if (!InfraString::isBolVazia($strHdnTipo)){

    if ($strHdnTipo=='P'){

      $strResultado = '';
      $strSumarioTabela = 'Tabela de Distribui��es.';
      $strCaptionTabela = 'Distribui��es';
      $strResultado .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";



      $objDistribuicaoRN=new DistribuicaoRN();
      if (!$bolGlobal) {
        $arrIdProtocolosPermitidos = array();
        PaginaSEI::getInstance()->prepararOrdenacao($objPainelDistribuicaoDTO, 'Distribuicao', InfraDTO::$TIPO_ORDENACAO_DESC);
        PaginaSEI::getInstance()->prepararPaginacao($objPainelDistribuicaoDTO);
        $arrObjDistribuicaoDTO = $objDistribuicaoRN->listarPainel($objPainelDistribuicaoDTO);
        $arrIdProtocolosSigilosos = array();
        $arrIdProtocolosSigilososPermitidos = array();
        foreach ($arrObjDistribuicaoDTO as $dto) {
          if ($dto->getStrStaNivelAcessoGlobal() === ProtocoloRN::$NA_SIGILOSO) {
            $arrIdProtocolosSigilosos[] = $dto->getDblIdProcedimento();
          }
        }
        if (InfraArray::contar($arrIdProtocolosSigilosos)) {
          $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
          $objPesquisaProtocoloDTO->setDblIdProtocolo($arrIdProtocolosSigilosos);
          $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_PROCEDIMENTOS);
          $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_AUTORIZADO);
          $objProtocoloRN = new ProtocoloRN();
          $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);
          foreach ($arrObjProtocoloDTO as $objProtocoloDTO) {
            if ($objProtocoloDTO->getNumCodigoAcesso() > 0) $arrIdProtocolosPermitidos[$objProtocoloDTO->getDblIdProtocolo()] = 1;
          }
        }

        $arrObjProtocoloDTO = array();
        $baseDTO = new ProtocoloDTO();
        $baseDTO->setDblIdProtocolo(null);
        $baseDTO->setStrStaNivelAcessoGlobal(null);
        foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO) {
          /** @var DistribuicaoDTO $objDistribuicaoDTO  */
          $objProtocoloDTO = clone $baseDTO;
          $objProtocoloDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
          $objProtocoloDTO->setStrStaNivelAcessoGlobal($objDistribuicaoDTO->getStrStaNivelAcessoGlobal());
          $arrObjProtocoloDTO[] = $objProtocoloDTO;
        }
        $objAnotacaoRN = new AnotacaoRN();
        $objAnotacaoRN->complementar($arrObjProtocoloDTO);

        $arrObjProtocoloDTO = InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO, 'IdProtocolo');
        $numRegistros = InfraArray::contar($arrObjDistribuicaoDTO);
        PaginaSEI::getInstance()->processarPaginacao($objPainelDistribuicaoDTO);
        $strResultado .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistros) . '</caption>';
        $strResultado .= '<tr>';

        if ($numRegistros > 0) {

          //$bolAcaoImprimir = true;
          //if ($bolAcaoImprimir) {
          //  $bolCheck = true;
          //  $arrComandos[] = '<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="infraImprimirTabela();" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir</button>';
          //}

//          if ($bolCheck) {
//            $strResultado .= '<th class="infraTh" width="1%">' . PaginaSEI::getInstance()->getThCheck() . '</th>' . "\n";
//          }
          $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO, 'Colegiado', 'SiglaColegiado', $arrObjDistribuicaoDTO) . '</th>' . "\n";
          $strResultado .= '<th class="infraTh">' . PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO, 'Processo', 'IdProcedimento', $arrObjDistribuicaoDTO) . '</th>' . "\n";
          $strResultado .= '<th class="infraTh">Partes</th>'."\n";
          $strResultado .= '<th class="infraTh" width="20%">' . PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO, 'Situa��o', 'StaDistribuicao', $arrObjDistribuicaoDTO) . '</th>' . "\n";
          $strResultado .= '<th class="infraTh" width="20%">' . PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO, 'Data', 'Distribuicao', $arrObjDistribuicaoDTO) . '</th>' . "\n";
          $strResultado .= '</tr>' . "\n";
          $strCssTr = '';
          foreach ($arrObjDistribuicaoDTO as $i => $objPainelDistribuicaoDTOBanco) {
            /* @var $objPainelDistribuicaoDTOBanco DistribuicaoDTO */
            $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
            $idColegiado=$objPainelDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao();
            if ($objPainelDistribuicaoDTOBanco->getStrStaDistribuicao()!=DistribuicaoRN::$STA_JULGADO &&
                (!isset($arrColegiados[$idColegiado][$objPainelDistribuicaoDTOBanco->getNumIdUnidadeRelator()]) ||
                    !in_array($objPainelDistribuicaoDTOBanco->getNumIdUsuarioRelator(),$arrColegiados[$idColegiado])
                    )) {
              $strResultado .= '<tr class="trVermelha">';
            } elseif($objPainelDistribuicaoDTOBanco->getStrStaDistribuicao()==DistribuicaoRN::$STA_PEDIDO_VISTA && (
                !in_array($objPainelDistribuicaoDTOBanco->getObjPedidoVistaDTO()->getNumIdUsuario(),$arrColegiados[$idColegiado]) ||
                !isset($arrColegiados[$idColegiado][$objPainelDistribuicaoDTOBanco->getObjPedidoVistaDTO()->getNumIdUnidade()]))) {
              $strResultado .= '<tr class="trVermelha">';
            } else {
              $strResultado .= $strCssTr;
            }
//            if ($bolCheck) {
//              $strResultado .= '<td valign="top">' . PaginaSEI::getInstance()->getTrCheck($i, $objPainelDistribuicaoDTOBanco->getDblIdProcedimento(), $objPainelDistribuicaoDTOBanco->getStrProtocoloFormatado()) . '</td>';
//            }
            $strResultado .= '<td align="center"><a alt="' . PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrNomeColegiado()) . '" title="' . PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrNomeColegiado()) . '" class="ancoraSigla">' . PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrSiglaColegiado()) . '</a></td>';
            //processo
            $strResultado .= '<td align="center">';
            if ($objPainelDistribuicaoDTOBanco->getStrStaNivelAcessoGlobal() === ProtocoloRN::$NA_SIGILOSO) {
              $class = 'processoVisualizadoSigiloso';
              $bolLink = isset($arrIdProtocolosPermitidos[$objPainelDistribuicaoDTOBanco->getDblIdProcedimento()]);
            } else {
              $class = 'protocoloNormal';
              $bolLink = true;
            }
            $strLink = $bolLink ? 'href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $arrObjDistribuicaoDTO[$i]->getDblIdProcedimento()) . '" target="_blank"' : 'href="javascript:void(0);"';
            $strStaDistribuicao = $arrObjDistribuicaoDTO[$i]->getStrStaDistribuicao();
            $strTipoProcedimento = PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrNomeTipoProcedimento());
            $strResultado .= AnotacaoINT::montarIconeAnotacao($arrObjProtocoloDTO[$objPainelDistribuicaoDTOBanco->getDblIdProcedimento()]->getObjAnotacaoDTO(), $bolAcaoRegistrarAnotacao, $objPainelDistribuicaoDTOBanco->getDblIdProcedimento(), $strParametros);
            $strResultado .= '<a ' . $strLink . ' tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" alt="' . $strTipoProcedimento . '" title="' . $strTipoProcedimento . '" class="' . $class . '">' . PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrProtocoloFormatado()) . '</a>';
            $strResultado .= '</td>';
            //partes
            $strResultado .='<td align="justified">';
            $strResultado .=ParteProcedimentoINT::montarHtmlPartes($objPainelDistribuicaoDTOBanco->getArrObjParteProcedimentoDTO());
            $strResultado .='</td>';
            //situacao
            $strResultado .= '<td align="center">' . PaginaSEI::tratarHTML($arrObjEstadoDistribuicao[$strStaDistribuicao]->getStrDescricao());
            if ($arrObjDistribuicaoDTO[$i]->isSetObjPedidoVistaDTO()) {
              $strResultado .= ' por ';
              $strNomeUsuario = PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getObjPedidoVistaDTO()->getStrNomeUsuario());
              $strResultado .= '<a ' . $strLink . 'tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" alt="' . $strNomeUsuario . '" title="' . $strNomeUsuario . '" class="ancoraSigla" style="font-size:1em">' . $strNomeUsuario . '</a>';
            }
            $strResultado .= '</td>';
            $strResultado .= '<td align="center">' . $objPainelDistribuicaoDTOBanco->getDthSituacaoAtual() . '</td>';
            $strResultado .= '</tr>' . "\n";
          }
          $strResultado .= '</table>';
        }
      } else {

        $arrObjDistribuicaoDTO = $objDistribuicaoRN->listarPainel($objPainelDistribuicaoDTO);
        $arrResultado=array();
        $arrResumo = InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO,'SiglaColegiado',true);
        ksort($arrResumo);
        foreach ($arrResumo as $strSiglaColegiado=>$arrIdxColegiado) {
          $arrResumo[$strSiglaColegiado] = InfraArray::indexarArrInfraDTO($arrIdxColegiado,'SiglaUnidadeRelator',true);
          ksort($arrResumo[$strSiglaColegiado]);
          foreach ($arrResumo[$strSiglaColegiado] as $strSiglaUnidade => $arrIdxStaDistribuicao) {
            $arrResumo[$strSiglaColegiado][$strSiglaUnidade]=InfraArray::indexarArrInfraDTO($arrIdxStaDistribuicao,'StaDistribuicao',true);
            ksort($arrResumo[$strSiglaColegiado][$strSiglaUnidade]);
            foreach ($arrResumo[$strSiglaColegiado][$strSiglaUnidade] as $staDistribuicao=>$arrObjPainelDistribuicaoDTO2) {
              $qtd=InfraArray::contar($arrObjPainelDistribuicaoDTO2);
              //verificar se tem membro que n�o est� no colegiado e colocar em um atributo para pintar linha em vermelho
              //n�o pintar julgados
              $arrObjPainelDistribuicaoDTO2[0]->setStrSinInconsistencia('N');
              foreach ($arrObjPainelDistribuicaoDTO2 as $objPainelDistribuicaoDTO2) {
                if(!isset($arrColegiados[$objPainelDistribuicaoDTO2->getNumIdColegiadoColegiadoVersao()][$objPainelDistribuicaoDTO2->getNumIdUnidadeRelator()]) ||
                    $objPainelDistribuicaoDTO2->getNumIdUsuarioRelator()!=$arrColegiados[$objPainelDistribuicaoDTO2->getNumIdColegiadoColegiadoVersao()][$objPainelDistribuicaoDTO2->getNumIdUnidadeRelator()]){
                  $arrObjPainelDistribuicaoDTO2[0]->setStrSinInconsistencia('S');
                  break;
                }
                if($staDistribuicao==DistribuicaoRN::$STA_PEDIDO_VISTA){
                  $idUsuario=$objPainelDistribuicaoDTO2->getObjPedidoVistaDTO()->getNumIdUsuario();
                  if(!in_array($idUsuario,$arrColegiados[$objPainelDistribuicaoDTO2->getNumIdColegiadoColegiadoVersao()])){
                    $arrObjPainelDistribuicaoDTO2[0]->setStrSinInconsistencia('S');
                    break;
                  }
                }
              }

              $arrObjPainelDistribuicaoDTO2[0]->setNumQuantidade($qtd);
              $arrResultado[]=$arrObjPainelDistribuicaoDTO2[0];
            }
          }
        }


        unset($arrResumo);
        $numRegistros=InfraArray::contar($arrResultado);
        $strResultado .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistros) . '</caption>';
        $strResultado .= '<tr>';
        if ($numRegistros > 0) {
          //$bolAcaoImprimir = true;
          //if ($bolAcaoImprimir) {
          //  $bolCheck = true;
          //  $arrComandos[] = '<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="infraImprimirDiv(\'divInfraAreaTabela\');" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir</button>';
          //}

          $strResultado .= '<th class="infraTh" width="15%">Colegiado</th>' . "\n";
          $strResultado .= '<th class="infraTh" width="30%">Unidade</th>' . "\n";
          $strResultado .= '<th class="infraTh">Situa��o</th>' . "\n";
          $strResultado .= '<th class="infraTh" width="30%">Quantidade</th>' . "\n";
          $strResultado .= '</tr>' . "\n";
          $strCssTr = '';
//          for ($i = 0; $i < $numRegistros; $i++) {
          foreach ($arrResultado as $i => $objPainelDistribuicaoDTOBanco) {
            /* @var $objPainelDistribuicaoDTOBanco PainelDistribuicaoDTO*/
            $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
            if ($objPainelDistribuicaoDTOBanco->getStrStaDistribuicao()!=DistribuicaoRN::$STA_JULGADO &&
                (!isset($arrColegiados[$objPainelDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao()][$objPainelDistribuicaoDTOBanco->getNumIdUnidadeRelator()]) ||
                 $objPainelDistribuicaoDTOBanco->getStrSinInconsistencia()=='S')) {
              $strResultado .= '<tr class="trVermelha">';
            } else {
              $strResultado .= $strCssTr;
            }
            $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=painel_distribuicao_detalhar'.'&sta_distribuicao='.$objPainelDistribuicaoDTOBanco->getStrStaDistribuicao().'&id_unidade='.$objPainelDistribuicaoDTOBanco->getNumIdUnidadeRelator().'&id_colegiado='.$objPainelDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
            $strNomeColegiado=PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrNomeColegiado());
            $strResultado .= '<td align="center"><a alt="'. $strNomeColegiado.'" title="'. $strNomeColegiado.'" class="ancoraSigla">'. PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrSiglaColegiado()) . '</a></td>';
            $strResultado .= '<td align="center"><a alt="'. PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrDescricaoUnidadeRelator()) .'" title="'. PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrDescricaoUnidadeRelator()).'" class="ancoraSigla">'. PaginaSEI::tratarHTML($objPainelDistribuicaoDTOBanco->getStrSiglaUnidadeRelator()) . '</a></td>';
            $strResultado .= '<td align="center">' . PaginaSEI::tratarHTML($arrObjEstadoDistribuicao[$objPainelDistribuicaoDTOBanco->getStrStaDistribuicao()]->getStrDescricao()) . '</td>';
            $strResultado .= '<td align="center"><a href="javascript:void(0);" onclick="abrirDetalhe(\''.$strLink.'\');" class="ancoraPadraoAzul" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" alt="' . $objPainelDistribuicaoDTOBanco->getNumQuantidade(). '" title="'. $objPainelDistribuicaoDTOBanco->getNumQuantidade().'">'. $objPainelDistribuicaoDTOBanco->getNumQuantidade() . '</a></td>';
            $strResultado .= '</tr>' . "\n";
          }
          $strResultado .= '<tr class="totalEstatisticas">';
          $strResultado.='<td colspan="3" align="right">TOTAL</td><td align="center">';
          $strLink='controlador.php?acao=painel_distribuicao_detalhar'.'&sta_distribuicao='.implode(',',$arrStaDistribuicao).'&id_colegiado='.$_POST['selColegiado'];
          if($strIdUnidade){
            $strLink.='&id_unidade='.$strIdUnidade;
          }
          $strResultado.= '<a href="javascript:void(0);" onclick="abrirDetalhe(\''.SessaoSEI::getInstance()->assinarLink($strLink).'\');" class="ancoraPadraoAzul" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" alt="' . InfraArray::contar($arrObjDistribuicaoDTO). '" title="'. InfraArray::contar($arrObjDistribuicaoDTO).'">'. InfraArray::contar($arrObjDistribuicaoDTO) . '</a>';
          $strResultado .= '</td> </tr>'."\n".'</table>';
        }

      }
    }else if ($strHdnTipo=='G'){

      $objEstatisticasRN = new EstatisticasRN();
      $arrCoresGlobal=$objEstatisticasRN->getArrCores();
      $arrCores = array('Distribu�do' => $arrCoresGlobal[0],
          'Redistribu�do' => $arrCoresGlobal[1],
          'Pautado' => $arrCoresGlobal[2],
          'Julgado'=>$arrCoresGlobal[3],
          'Em Mesa'=>$arrCoresGlobal[4],
          'Com Pedido de Vista'=>$arrCoresGlobal[5],
          'Para Referendo'=>$arrCoresGlobal[6],
          'Adiado'=>$arrCoresGlobal[7],
          'Retirado de Pauta'=>$arrCoresGlobal[8],
          'Em Digil�ncia'=>$arrCoresGlobal[9]
          );

      $arrGraficoGeral = array();
      $numTotalGraficoGeral = 0;
      $arrObjDistribuicaoDTO=$objDistribuicaoRN->listarPainel($objPainelDistribuicaoDTO);
      $arrObjDistribuicaoDTO=InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO,'StaDistribuicao',true);
      foreach ($arrStaDistribuicao as $valor) {
        $qtd=InfraArray::contar($arrObjDistribuicaoDTO[$valor]);
        if ($qtd>0) {
          $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=painel_distribuicao_detalhar&sta_distribuicao=' . $valor . '&id_unidade=' . $_POST['hdnIdUnidade'].'&id_colegiado='.$numIdColegiado);
          $arrGraficoGeral[] = array($arrObjEstadoDistribuicao[$valor]->getStrDescricao(), $qtd, $qtd, $strLink);
          $numTotalGraficoGeral += $qtd;
        }
      }

      $numGrafico = 0;
      $strResultadoGraficoGeral = '';
      $strResultadoGraficoGeral .= '<div id="divGrf'.(++$numGrafico).'" class="divAreaGrafico">';

      $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=painel_distribuicao_detalhar'.'&sta_distribuicao='.implode(',',$arrStaDistribuicao).'&id_unidade='.$_POST['hdnIdUnidade'].'&id_colegiado='.$numIdColegiado);

      $strResultadoGraficoGeral .= $objEstatisticasRN->gerarGraficoBarrasSimples($numGrafico,'Total ('.$numTotalGraficoGeral.')',$strLink, $arrGraficoGeral, 150, $arrCores);
      $strResultadoGraficoGeral .= '</div>';

      if ($bolGlobal){
        $objPainelDistribuicaoDTO->setStrStaDistribuicao($arrStaDistribuicao,InfraDTO::$OPER_IN);
        $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objPainelDistribuicaoDTO);
        $arrResultado=array();
        $arrResumo = InfraArray::indexarArrInfraDTO($arrObjDistribuicaoDTO,'SiglaUnidadeRelator',true);
        foreach ($arrResumo as $key=>$value) {
          $arrResumo[$key]=InfraArray::indexarArrInfraDTO($value,'StaDistribuicao',true);
          foreach ($arrResumo[$key] as $sta=>$arr) {
            $qtd=InfraArray::contar($arr);
            $arr[0]->setNumQuantidade($qtd);
            $arrResumo[$key][$sta]=$arr[0];
          }
        }
        $strResultadoGraficoPorTipo = '';
        foreach($arrResumo as $strSiglaUnidade => $arrIdxStaDistribuicao){
          $strResultadoGraficoPorTipo .= '<div id="divGrf'.(++$numGrafico).'" class="divAreaGrafico">';
          $arrGraficoUnidade=array();
          $numTotalUnidade=0;
          foreach ($arrIdxStaDistribuicao as $objPainelDistribuicaoDTO2) {
            $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=painel_distribuicao_detalhar'.'&sta_distribuicao='.$objPainelDistribuicaoDTO2->getStrStaDistribuicao().'&id_unidade='.$objPainelDistribuicaoDTO2->getNumIdUnidadeRelator());
            $qtd=$objPainelDistribuicaoDTO2->getNumQuantidade();
            $arrGraficoUnidade[]=array($arrObjEstadoDistribuicao[$objPainelDistribuicaoDTO2->getStrStaDistribuicao()]->getStrDescricao(),$qtd,$qtd,$strLink);
            $numTotalUnidade+=$qtd;
          }

          $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=painel_distribuicao_detalhar'.'&sta_distribuicao='.implode(',',$arrStaDistribuicao).'&id_unidade='.$objPainelDistribuicaoDTO2->getNumIdUnidadeRelator());
          $strResultadoGraficoPorTipo .= $objEstatisticasRN->gerarGraficoBarrasSimples($numGrafico,$strSiglaUnidade.' ('.$numTotalUnidade.')',$strLink, $arrGraficoUnidade, 150, $arrCores);
          $strResultadoGraficoPorTipo .= '</div>';
        }
      }

    }
  }

  if ($bolGlobal){
    $strItensSelColegiado=ColegiadoINT::montarSelectNomeAdministrados('','Todos',$numIdColegiado);
  } else {
    $strItensSelColegiado=ColegiadoINT::montarSelectNomePermitidos('','Todos',$numIdColegiado);
  }

  $strLinkAjaxUnidade = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=unidade_auto_completar_todas');
  $strLinkAjaxContatos = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=contato_auto_completar_pesquisa');
  $strItensSelTipoProcedimento = TipoProcedimentoINT::montarSelectNome('null','&nbsp;',$numIdTipoProcedimento);
  $strItensSelTipoMateria = TipoMateriaINT::montarSelectDescricao('null','&nbsp;',$numIdTipoMateria);

  $strOptionsStaDistribuicao='';
  $numEstadoDistribuicao = InfraArray::contar($arrObjEstadoDistribuicaoDTO);
  foreach ($arrObjEstadoDistribuicaoDTO as $objEstadoDistribuicaoDTO) {
    $staDistribuicao=$objEstadoDistribuicaoDTO->getStrStaDistribuicao();
    if($staDistribuicao==DistribuicaoRN::$STA_CANCELADO){
      continue;
    }
    $strOptionsStaDistribuicao.='<option value="'.$staDistribuicao.'"';
    if (in_array($staDistribuicao,$arrStaDistribuicao)) {
      $strOptionsStaDistribuicao.=' selected="selected"';
    }
    $strOptionsStaDistribuicao.='>'.$objEstadoDistribuicaoDTO->getStrDescricao().'</option>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
  #lblDataInicial {position:absolute;left:0;top:15%;width:10%;}
  #txtDataInicial {position:absolute;left:14%;top:0;width:10%;}
  #imgCalDataInicial {position:absolute;left:25%;top:10%;}

  #lblDataFinal {position:absolute;left:29%;top:15%;width:10%;}
  #txtDataFinal {position:absolute;left:34%;top:0;width:10%;}
  #imgCalDataFinal {position:absolute;left:45%;top:15%;}

  #lblColegiado {position:absolute;left:0;top:15%;width:9%;}
  #selColegiado {position:absolute;left:14%;top:0;width:40%;}

  #lblTipoProcedimento {position:absolute;left:0;top:15%;width:20%;}
  #selTipoProcedimento {position:absolute;left:14%;top:0;width:40%;}

  #lblContato {position:absolute;left:0;top:15%;width:70%;}
  #txtContato {position:absolute;left:14%;top:0;width:70%;}

  #lblAutuacao {position:absolute;left:0;top:15%;width:70%;}
  #txtAutuacao {position:absolute;left:14%;top:0;width:70%;}

  #lblUnidade {position:absolute;left:0;top:15%;width:70%;}
  #txtUnidade {position:absolute;left:14%;top:0;width:70%;}

  #lblStaDistribuicao {position:absolute;left:0;top:10%;width:9%;}
  #selStaDistribuicao, .multipleSelect {position:absolute;left:14%;top:0;width:40%;}

  #lblTipoMateria {position:absolute;left:0;top:15%;width:20%;}
  #selTipoMateria {position:absolute;left:14%;top:0;width:40%;}

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
?>
  <script type="text/javascript" src="/infra_js/raphaeljs/raphael-min.js"></script>
  <script type="text/javascript" src="/infra_js/raphaeljs/g.raphael-min.js"></script>
  <script type="text/javascript" src="/infra_js/raphaeljs/g.bar-min.js"></script>
<?
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

var objAutoCompletarUnidade = null;
var objAutoCompletarInteressadoRI1225 = null;

$(document).ready(function() {
  $("#selStaDistribuicao").show().multipleSelect({
    filter: false,
    selectAllText: 'Selecionar todas',
    allSelected: 'Todas selecionadas',
    countSelected: '# de % selecionadas',
    minimumCountSelected: 3,
    selectAll: true

  });
});

function inicializar() {
  <? if ($bolGlobal) {?>
  objAutoCompletarUnidade = new infraAjaxAutoCompletar('hdnIdUnidade','txtUnidade','<?=$strLinkAjaxUnidade?>');
  objAutoCompletarUnidade.limparCampo = true;

  objAutoCompletarUnidade.prepararExecucao = function(){
    return 'palavras_pesquisa='+document.getElementById('txtUnidade').value;
  };

  objAutoCompletarUnidade.processarResultado = function(id,descricao,complemento){
    if (id!=''){
      document.getElementById('hdnIdUnidade').value = id;
      document.getElementById('txtUnidade').value = descricao;
     }
  };

  objAutoCompletarUnidade.selecionar('<?=$strIdUnidade?>','<?=PaginaSEI::getInstance()->formatarParametrosJavaScript($strNomeUnidade,false)?>');
  <? } ?>

  objAutoCompletarInteressadoRI1225 = new infraAjaxAutoCompletar('hdnIdContato','txtContato','<?=$strLinkAjaxContatos?>');
  objAutoCompletarInteressadoRI1225.limparCampo = true;
  objAutoCompletarInteressadoRI1225.prepararExecucao = function(){
    return 'palavras_pesquisa='+document.getElementById('txtContato').value;
  };
  objAutoCompletarInteressadoRI1225.selecionar('<?=$strIdContato;?>','<?=PaginaSEI::getInstance()->formatarParametrosJavaScript($strNomeContato,false)?>');





  infraEfeitoTabelas();
}

function limpar() {
  document.getElementById('txtDataInicial').value = '';
  document.getElementById('txtDataFinal').value = '';
  document.getElementById('selColegiado').value = 'null';
  document.getElementById('selTipoProcedimento').value = 'null';

  $("#selStaDistribuicao").multipleSelect("checkAll");

  objAutoCompletarInteressadoRI1225.limpar();
  <? if ($bolGlobal) {?>
  objAutoCompletarUnidade.limpar();
  <? } ?>
}


function onSubmitForm() {

  var a=$("#selStaDistribuicao").multipleSelect("getSelects");
  if (a.length==0) {
    alert('Nenhuma Situa��o selecionada.');
    return false;
  }
  infraExibirAviso(true);
  return true;
}

function processar(tipo, link) {

  document.getElementById('hdnTipo').value = tipo;

  if (onSubmitForm()) {
    document.getElementById('frmPainelDistribuicao').action = link;
    document.getElementById('frmPainelDistribuicao').submit();
  }
}

function abrirDetalhe(link) {
  infraAbrirJanelaModal(link, 850, 550);
}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
$strTamanhoDiv='3em';
?>
  <form id="frmPainelDistribuicao"  method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
    <?
    //PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    PaginaSEI::getInstance()->abrirAreaDados($strTamanhoDiv);
    ?>

    <label id="lblDataInicial" for="txtDataInicial" accesskey="" class="infraLabelOpcional">Per�odo:</label>
    <input type="text" id="txtDataInicial" name="txtDataInicial" class="infraText" value="<?=$strDataInicial?>" onkeypress="return infraMascaraData(this, event)" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <img id="imgCalDataInicial" title="Selecionar Data Inicial" alt="Selecionar Data Inicial" src="<?=PaginaSEI::getInstance()->getIconeCalendario()?>" class="infraImg" onclick="infraCalendario('txtDataInicial',this);" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

    <label id="lblDataFinal" for="txtDataFinal" accesskey="" class="infraLabelOpcional">at�</label>
    <input type="text" id="txtDataFinal" name="txtDataFinal" class="infraText" value="<?=$strDataFinal?>" onkeypress="return infraMascaraData(this, event)" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <img id="imgCalDataFinal" title="Selecionar Data Final" alt="Selecionar Data Final" src="<?=PaginaSEI::getInstance()->getIconeCalendario()?>" class="infraImg" onclick="infraCalendario('txtDataFinal',this);" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados($strTamanhoDiv);
    ?>
    <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelOpcional">Colegiado:</label>
    <select id="selColegiado" name="selColegiado"  class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
      <?=$strItensSelColegiado?>
    </select>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados($strTamanhoDiv);
    ?>
    <label id="lblTipoProcedimento" for="selTipoProcedimento" accesskey="" class="infraLabelOpcional">Tipo de Processo:</label>
    <select id="selTipoProcedimento" name="selTipoProcedimento"  class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
      <?=$strItensSelTipoProcedimento?>
    </select>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados($strTamanhoDiv);
    ?>
    <label id="lblContato" for="txtContato" accesskey=""  class="infraLabelOpcional">Contato:</label>
    <input type="text" id="txtContato" name="txtContato" class="infraText" value="<?=PaginaSEI::tratarHTML($strNomeContato);?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <input type="hidden" id="hdnIdContato" name="hdnIdContato" class="infraText" value="<?=$strIdContato?>" />



    <input type="hidden" id="hdnTipo" name="hdnTipo" value="<?=$_POST['hdnTipo']?>" />


    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    if ($bolGlobal) {
      PaginaSEI::getInstance()->abrirAreaDados($strTamanhoDiv);
      ?>
      <label id="lblUnidade" for="txtUnidade" accesskey="" class="infraLabelOpcional">Unidade:</label>
      <input type="text" id="txtUnidade" name="txtUnidade" class="infraText" value="<?= $strNomeUnidade ?>"
             tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>
      <input type="hidden" id="hdnIdUnidade" name="hdnIdUnidade" value="<?= $strIdUnidade ?>"/>
      <?
      PaginaSEI::getInstance()->fecharAreaDados();
    }
    PaginaSEI::getInstance()->abrirAreaDados($strTamanhoDiv);
    ?>
     <label id="lblAutuacao" for="txtAutuacao" accesskey=""  class="infraLabelOpcional">Descri��o:</label>
    <input type="text" id="txtAutuacao" name="txtAutuacao" class="infraText" value="<?=PaginaSEI::tratarHTML($strDescricaoAutuacao);?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados($strTamanhoDiv);
    ?>
    <label id="lblTipoMateria" for="selTipoMateria" accesskey="" class="infraLabelOpcional">Tipo de Mat�ria:</label>
    <select id="selTipoMateria" name="selTipoMateria"  class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
      <?=$strItensSelTipoMateria?>
    </select>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();



    PaginaSEI::getInstance()->abrirAreaDados('4em','style="overflow:visible;"');
    ?>

    <label id="lblStaDistribuicao" for="selStaDistribuicao" accesskey="" class="infraLabelObrigatorio">Situa��es:</label>
    <select multiple id="selStaDistribuicao" name="selStaDistribuicao[]" class="infraSelect multipleSelect" style="display: none" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
      <?=$strOptionsStaDistribuicao;?>
    </select>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();

    if ($_GET['acao']=='painel_distribuicao_listar') {

      PaginaSEI::getInstance()->montarAreaTabela($strResultado, $numRegistros);

    } else if ($_GET['acao']=='painel_distribuicao_gerar_grafico') {

      EstatisticasINT::montarGrafico('Geral', $strResultadoGraficoGeral, false);
      EstatisticasINT::montarGrafico('Tipos', $strResultadoGraficoPorTipo, false);

    }

    PaginaSEI::getInstance()->montarAreaDebug();
    //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);

    ?>
  </form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>