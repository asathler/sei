<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  //PaginaSEI::getInstance()->salvarCamposPost(array(''));

  //PaginaSEI::getInstance()->setTipoSelecao(InfraPagina::$TIPO_SELECAO_MULTIPLA);

  $arrComandos = array();


  //Filtrar par�metros
  $strParametros = '';
	$mensagem='';
  if(isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
    $strParametros .= '&arvore='.$_GET['arvore'];
  }

  if (isset($_GET['id_procedimento'])){
    $strParametros .= "&id_procedimento=".$_GET['id_procedimento'];
  }


  $numIdColegiado = $_POST['selColegiado'];
	if ($numIdColegiado=='null') $numIdColegiado=null;


	$bolEnvioOK = false;

	$bolMultiplo=($_GET['acao_origem']=='procedimento_controlar' || $_GET['acao_origem']=='painel_distribuicao_detalhar' || isset($_POST['hdnProcedimentos']));
	if ($bolMultiplo) {
    if (isset($_POST['hdnProcedimentos'])) {
      $arr = explode(',', $_POST['hdnProcedimentos']);
    } else {
      $arr = array_merge(PaginaSEI::getInstance()->getArrStrItensSelecionados('Gerados'), PaginaSEI::getInstance()->getArrStrItensSelecionados('Recebidos'), PaginaSEI::getInstance()->getArrStrItensSelecionados('Detalhado'));
    }
  }


  switch($_GET['acao']) {

    case 'distribuicao_cancelar':

      $strTitulo = 'Cancelar Distribui��o de Processo';
      $redistribuicao = false;

      $objDistribuicaoDTO = new DistribuicaoDTO();
      $objDistribuicaoDTO->setDblIdProcedimento($_GET['id_procedimento']);
      $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
      $objDistribuicaoDTO->setStrStaDistribuicao(array(DistribuicaoRN::$STA_CANCELADO,DistribuicaoRN::$STA_PAUTADO,DistribuicaoRN::$STA_EM_MESA,DistribuicaoRN::$STA_PARA_REFERENDO,DistribuicaoRN::$STA_JULGADO,DistribuicaoRN::$STA_PEDIDO_VISTA), InfraDTO::$OPER_NOT_IN);
      $objDistribuicaoDTO->setNumIdUnidadeResponsavelColegiado(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objDistribuicaoDTO->retNumIdDistribuicao();
      $objDistribuicaoDTO->retStrStaDistribuicao();
      $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
      $objDistribuicaoDTO->retStrNomeColegiado();
      $objDistribuicaoDTO->retStrProtocoloFormatado();

      $objDistribuicaoRN = new DistribuicaoRN();
      $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);
      if (InfraArray::contar($arrObjDistribuicaoDTO) == 0) {
        throw new InfraException('N�o foi encontrada distribui��o pass�vel de cancelamento.');
      }

      if (isset($_POST['sbmSalvar'])) {

        try {


          $objDistribuicaoDTO = new DistribuicaoDTO();

          $objDistribuicaoDTO->setNumIdDistribuicao($_POST['selDistribuicao']);

          $objMotivoCancDistribuicaoDTO=new MotivoCancDistribuicaoDTO();
          $objMotivoCancDistribuicaoRN=new MotivoCancDistribuicaoRN();
          $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($_POST['selMotivoCancelamento']);
          $objMotivoCancDistribuicaoDTO->retStrDescricao();
          $objMotivoCancDistribuicaoDTO->retNumIdMotivoCancDistribuicao();
          $objMotivoCancDistribuicaoDTO=$objMotivoCancDistribuicaoRN->consultar($objMotivoCancDistribuicaoDTO);

          if($objMotivoCancDistribuicaoDTO==null){
            throw new InfraException('Motivo de Cancelamento de Distribui��o n�o encontrado.');
          }

          $objDistribuicaoDTO->setNumIdMotivoCancelamento($objMotivoCancDistribuicaoDTO->getNumIdMotivoCancDistribuicao());
          $objDistribuicaoDTO->setStrDescricaoMotivoCancelamento($objMotivoCancDistribuicaoDTO->getStrDescricao());

          $objDocumentoDTO=$objDistribuicaoRN->cancelar($objDistribuicaoDTO);

//          if ($bolMultiplo) {
//            PaginaSEI::getInstance()->adicionarMensagem('Processo distribu�do com sucesso.');
//            $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_controlar&acao_origem=' . $_GET['acao'] . $strParametros . PaginaSEI::montarAncora($_POST['hdnProcedimentos']));
//            header('Location: ' . $strLinkRetorno);
//            die;
//
//          } else {
          $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_visualizar&acao_origem=' . $_GET['acao'] . '&id_procedimento=' . $_GET['id_procedimento'] . '&id_documento=' . $objDocumentoDTO->getDblIdDocumento() . '&montar_visualizacao=1');
//          }

          $bolEnvioOK = true;

          //PaginaSEI::getInstance()->adicionarMensagem('Processo distribu�do com sucesso.');
          //header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno()));
          //die;

        } catch (Exception $e) {
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }

      break;

    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }

  $strItensSelDistribuicao = DistribuicaoINT::montarSelectDistribuicaoCancelamento(null, null, null, $_GET['id_procedimento']);
  $strItensMotivoCancelamento = MotivoCancDistribuicaoINT::montarSelectDescricao('null', '&nbsp;', $_POST['selMotivoCancelamento']);
  $arrComandos = array();
  $arrComandos[] = '<button type="submit" accesskey="S" name="sbmSalvar" id="sbmSalvar" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';



}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblProcesso {position:absolute;left:0%;top:0%;width:40%;}
#txtProcesso {position:absolute;left:15%;top:0%;width:40%;}

  #lblDistribuicao {position:absolute;left:0%;top:30%;width:60%;}
  #selDistribuicao {position:absolute;left:15%;top:30%;width:60%;}

  #lblMotivoCancelamento {position:absolute;left:0%;top:60%;width:60%;}
  #selMotivoCancelamento {position:absolute;left:15%;top:60%;width:60%;}
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
//PaginaSEI::getInstance()->abrirJavaScript();
?>
<script type="application/javascript">

function inicializar(){
<? if ($bolEnvioOK) {?>
  parent.document.getElementById('ifrArvore').src = '<?=$strLinkRetorno;?>';
<? } ?>
}

function onSubmitForm(){

  var pwdSenha=$('#pwdSenha');
  if (infraTrim(pwdSenha.val())==''){
    alert('Informe a Senha.');
    pwdSenha.focus();
    return false;
  }
  if (!infraSelectSelecionado('selMotivoCancelamento')) {
    alert('Selecione um Motivo de Cancelamento.');
    document.getElementById('selMotivoCancelamento').focus();
    return false;
  }

	return true;
}

</script>
<?
//PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmEnviarNotificacao" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>" style="display:inline;">
<?
//PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
PaginaSEI::getInstance()->abrirAreaDados('10em');
?>

	<label id="lblProcesso" for="txtProcesso" accesskey="" class="infraLabelObrigatorio">Processo:</label>
	<input type="text" id="txtProcesso" name="txtProcesso" class="infraText" value="<?=$arrObjDistribuicaoDTO[0]->getStrProtocoloFormatado();?>" readonly maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />


	<label id="lblDistribuicao" for="selDistribuicao" accesskey="" class="infraLabelObrigatorio">Distribui��o:</label>
  <select id="selDistribuicao" name="selDistribuicao" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
  <?=$strItensSelDistribuicao;?>
  </select>

  <label id="lblMotivoCancelamento" for="selMotivoCancelamento" accesskey="" class="infraLabelObrigatorio">Motivo:</label>
  <select id="selMotivoCancelamento" name="selMotivoCancelamento" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
    <?=$strItensMotivoCancelamento;?>
  </select>



<?
  PaginaSEI::getInstance()->fecharAreaDados();

//PaginaSEI::getInstance()->montarAreaValidacao();
//PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numMembros);

?>
</form>
<?
PaginaSEI::getInstance()->montarAreaDebug();
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>