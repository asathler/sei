<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoVersaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdColegiado(ColegiadoVersaoDTO $objColegiadoVersaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoVersaoDTO->getNumIdColegiado())){
      $objInfraException->adicionarValidacao('Id Colegiado n�o informado.');
    }
  }

  private function validarDthVersao(ColegiadoVersaoDTO $objColegiadoVersaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoVersaoDTO->getDthVersao())){
      $objInfraException->adicionarValidacao('Data Hora Vers�o n�o informada.');
    }else{
      if (!InfraData::validarDataHora($objColegiadoVersaoDTO->getDthVersao())){
        $objInfraException->adicionarValidacao('Data Hora Vers�o inv�lida.');
      }
    }
  }

  private function validarStrSinUltima(ColegiadoVersaoDTO $objColegiadoVersaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoVersaoDTO->getStrSinUltima())){
      $objInfraException->adicionarValidacao('Sinalizador de �ltima vers�o n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objColegiadoVersaoDTO->getStrSinUltima())){
        $objInfraException->adicionarValidacao('Sinalizador de �ltima vers�o inv�lido.');
      }
    }
  }
  private function validarStrSinEditavel(ColegiadoVersaoDTO $objColegiadoVersaoDTO, InfraException $objInfraException){
  	if (InfraString::isBolVazia($objColegiadoVersaoDTO->getStrSinEditavel())){
  		$objInfraException->adicionarValidacao('Sinalizador de Edit�vel n�o informado.');
  	}else{
  		if (!InfraUtil::isBolSinalizadorValido($objColegiadoVersaoDTO->getStrSinEditavel())){
  			$objInfraException->adicionarValidacao('Sinalizador de Edit�vel inv�lido.');
  		}
  	}
  }

  private function validarNumIdUnidade(ColegiadoVersaoDTO $objColegiadoVersaoDTO, InfraException $objInfraException){
  	if (InfraString::isBolVazia($objColegiadoVersaoDTO->getNumIdUnidade())){
  		$objInfraException->adicionarValidacao('Unidade da �ltima altera��o n�o informada.');
  	}
  }

  private function validarNumIdUsuario(ColegiadoVersaoDTO $objColegiadoVersaoDTO, InfraException $objInfraException){
  	if (InfraString::isBolVazia($objColegiadoVersaoDTO->getNumIdUsuario())){
  		$objInfraException->adicionarValidacao('Usu�rio da �ltima altera��o n�o informado.');
  	}
  }

  protected function cadastrarControlado(ColegiadoVersaoDTO $objColegiadoVersaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_versao_cadastrar',__METHOD__,$objColegiadoVersaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarDthVersao($objColegiadoVersaoDTO, $objInfraException);
      $this->validarNumIdColegiado($objColegiadoVersaoDTO, $objInfraException);
      $this->validarStrSinUltima($objColegiadoVersaoDTO, $objInfraException);
      $this->validarStrSinEditavel($objColegiadoVersaoDTO, $objInfraException);
      $this->validarNumIdUnidade($objColegiadoVersaoDTO, $objInfraException);
      $this->validarNumIdUsuario($objColegiadoVersaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objColegiadoVersaoBD = new ColegiadoVersaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoVersaoBD->cadastrar($objColegiadoVersaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Colegiado Vers�o.',$e);
    }
  }

  protected function alterarControlado(ColegiadoVersaoDTO $objColegiadoVersaoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_versao_alterar',__METHOD__,$objColegiadoVersaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objColegiadoVersaoDTO->isSetDthVersao()){
        $this->validarDthVersao($objColegiadoVersaoDTO, $objInfraException);
      }
      if ($objColegiadoVersaoDTO->isSetNumIdColegiado()){
        $this->validarNumIdColegiado($objColegiadoVersaoDTO, $objInfraException);
      }
      if ($objColegiadoVersaoDTO->isSetStrSinUltima()){
        $this->validarStrSinUltima($objColegiadoVersaoDTO, $objInfraException);
      }
      if ($objColegiadoVersaoDTO->isSetStrSinEditavel()){
        $this->validarStrSinEditavel($objColegiadoVersaoDTO, $objInfraException);
      }
      if ($objColegiadoVersaoDTO->isSetNumIdUnidade()){
        $this->validarNumIdUnidade($objColegiadoVersaoDTO, $objInfraException);
      }
      if ($objColegiadoVersaoDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objColegiadoVersaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objColegiadoVersaoBD = new ColegiadoVersaoBD($this->getObjInfraIBanco());
      $objColegiadoVersaoBD->alterar($objColegiadoVersaoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Colegiado Vers�o.',$e);
    }
  }

  protected function excluirControlado($arrObjColegiadoVersaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_versao_excluir',__METHOD__,$arrObjColegiadoVersaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();

      $objColegiadoVersaoBD = new ColegiadoVersaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjColegiadoVersaoDTO);$i++){

        $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($arrObjColegiadoVersaoDTO[$i]->getNumIdColegiadoVersao());
        $objColegiadoComposicaoDTO->retNumIdColegiadoComposicao();
        $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
        if (InfraArray::contar($arrObjColegiadoComposicaoDTO)>0) {
          $objColegiadoComposicaoRN->excluir($arrObjColegiadoComposicaoDTO);
        }
        $objColegiadoVersaoBD->excluir($arrObjColegiadoVersaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Colegiado Vers�o.',$e);
    }
  }

  protected function consultarConectado(ColegiadoVersaoDTO $objColegiadoVersaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_versao_consultar',__METHOD__,$objColegiadoVersaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoVersaoBD = new ColegiadoVersaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoVersaoBD->consultar($objColegiadoVersaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Colegiado Vers�o.',$e);
    }
  }

  protected function listarConectado(ColegiadoVersaoDTO $objColegiadoVersaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_versao_listar',__METHOD__,$objColegiadoVersaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoVersaoBD = new ColegiadoVersaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoVersaoBD->listar($objColegiadoVersaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Colegiado Vers�es.',$e);
    }
  }

  protected function contarConectado(ColegiadoVersaoDTO $objColegiadoVersaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_versao_listar',__METHOD__,$objColegiadoVersaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoVersaoBD = new ColegiadoVersaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoVersaoBD->contar($objColegiadoVersaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Colegiado Vers�es.',$e);
    }
  }
  protected function bloquearControlado(ColegiadoVersaoDTO $objColegiadoVersaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_versao_consultar',__METHOD__,$objColegiadoVersaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoVersaoBD = new ColegiadoVersaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoVersaoBD->bloquear($objColegiadoVersaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Colegiado Vers�o.',$e);
    }
  }


}
?>