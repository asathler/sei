<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 11/11/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ItemSessaoJulgamentoDTO extends InfraDTO {

  public function getStrNomeTabela(): string
  {
  	 return 'item_sessao_julgamento';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdItemSessaoJulgamento','id_item_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamento','id_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdDistribuicao','id_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'Ordem','ordem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaSituacao','sta_situacao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Inclusao','dth_inclusao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Dispositivo','dispositivo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioPresidente','id_usuario_presidente');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioSessao','id_usuario_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidadeSessao','id_unidade_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL,'IdDocumentoJulgamento','id_documento_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdMotivoMesa','id_motivo_mesa');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinManual','sin_manual');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdSessaoBloco','id_sessao_bloco');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaTipoItemSessaoBloco', 'sta_tipo_item', 'sessao_bloco');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SinAgruparMembroSessaoBloco', 'sin_agrupar_membro', 'sessao_bloco');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'DescricaoSessaoBloco', 'descricao', 'sessao_bloco');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'OrdemSessaoBloco', 'ordem', 'sessao_bloco');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdColegiadoSessaoJulgamento', 'id_colegiado', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdTipoSessaoJulgamento', 'id_tipo_sessao', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaSituacaoSessaoJulgamento', 'sta_situacao', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DTH, 'SessaoSessaoJulgamento', 'dth_sessao', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUsuarioPresidenteSessaoJulgamento', 'id_usuario_presidente', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DBL, 'IdProcedimentoDistribuicao', 'id_procedimento', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUsuarioRelatorDistribuicao', 'id_usuario_relator', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUnidadeRelatorDistribuicao', 'id_unidade_relator', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUsuarioRelatorAcordaoDistribuicao', 'id_usuario_relator_acordao', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUnidadeRelatorAcordaoDistribuicao', 'id_unidade_relator_acordao', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdColegiadoVersaoDistribuicao', 'id_colegiado_versao', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdDistribuicaoAgrupadorDistribuicao', 'id_distribuicao_agrupador', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaDistribuicao', 'sta_distribuicao', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaDistribuicaoAnterior', 'sta_distribuicao_anterior', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaUltimoDistribuicao', 'sta_ultimo', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeUsuarioRelator', 'nome', 'usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'ProtocoloFormatadoProtocolo', 'protocolo_formatado', 'protocolo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaNivelAcessoGlobalProtocolo', 'sta_nivel_acesso_global', 'protocolo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'DescricaoProtocolo','descricao','protocolo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdTipoProcedimentoProcedimento', 'id_tipo_procedimento', 'procedimento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdProcedimentoProcedimento', 'id_procedimento', 'procedimento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeTipoProcedimento', 'nome', 'tipo_procedimento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeColegiado', 'nome', 'colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'ArtigoColegiado', 'artigo', 'colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUsuarioPresidenteColegiado', 'id_usuario_presidente', 'colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUnidadeResponsavelColegiado', 'id_unidade_responsavel', 'colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'OrdemMotivoMesa','ordem','motivo_mesa');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'DescricaoMotivoMesa','descricao','motivo_mesa');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SinVirtualTipoSessao','sin_virtual','tipo_sessao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_OBJ,'AnotacaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjDestaqueDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdProvimento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Complemento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjBloqueioItemSessUnidadeDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_OBJ,'RevisaoItemDTO');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_OBJ,'SessaoJulgamentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjJulgamentoParteDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjColegiadoComposicaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjPedidoVistaDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjPresencaSessaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjItemSessaoDocumentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjSustentacaoOralDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjParteProcedimentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjEleicaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinPermiteFinalizacao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinPossuiVotos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinPossuiVotosAnteriores');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinFracionado');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'MotivoRetirada');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Provimento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinCancelamentoSessao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'TitularesColegiado');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'DispositivoAnterior');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'FaltaQuorum');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'RelatorComposicaoAnterior');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'VotosComposicaoAnterior');


    $this->configurarPK('IdItemSessaoJulgamento',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdSessaoBloco', 'sessao_bloco', 'id_sessao_bloco');
    $this->configurarFK('IdSessaoJulgamento', 'sessao_julgamento', 'id_sessao_julgamento');
    $this->configurarFK('IdTipoSessaoJulgamento', 'tipo_sessao', 'id_tipo_sessao');
    $this->configurarFK('IdColegiadoSessaoJulgamento', 'colegiado', 'id_colegiado');
    $this->configurarFK('IdDistribuicao', 'distribuicao', 'id_distribuicao');
    $this->configurarFK('IdProcedimentoDistribuicao', 'procedimento', 'id_procedimento');
    $this->configurarFK('IdProcedimentoDistribuicao', 'protocolo', 'id_protocolo');
    $this->configurarFK('IdTipoProcedimentoProcedimento', 'tipo_procedimento', 'id_tipo_procedimento');
    $this->configurarFK('IdMotivoMesa', 'motivo_mesa', 'id_motivo_mesa',InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdUsuarioRelatorDistribuicao', 'usuario', 'id_usuario');
    $this->configurarFK('IdUnidadeSessao', 'unidade', 'id_unidade');

  }
}
?>