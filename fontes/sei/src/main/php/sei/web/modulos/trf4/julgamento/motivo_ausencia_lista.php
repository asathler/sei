<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('motivo_ausencia_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  switch($_GET['acao']){
    case 'motivo_ausencia_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoAusenciaDTO = array();
        foreach ($arrStrIds as $strId) {
          $objMotivoAusenciaDTO = new MotivoAusenciaDTO();
          $objMotivoAusenciaDTO->setNumIdMotivoAusencia($strId);
          $arrObjMotivoAusenciaDTO[] = $objMotivoAusenciaDTO;
        }
        $objMotivoAusenciaRN = new MotivoAusenciaRN();
        $objMotivoAusenciaRN->excluir($arrObjMotivoAusenciaDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;


    case 'motivo_ausencia_desativar':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoAusenciaDTO = array();
        foreach ($arrStrIds as $strId) {
          $objMotivoAusenciaDTO = new MotivoAusenciaDTO();
          $objMotivoAusenciaDTO->setNumIdMotivoAusencia($strId);
          $arrObjMotivoAusenciaDTO[] = $objMotivoAusenciaDTO;
        }
        $objMotivoAusenciaRN = new MotivoAusenciaRN();
        $objMotivoAusenciaRN->desativar($arrObjMotivoAusenciaDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

    case 'motivo_ausencia_reativar':
      $strTitulo = 'Reativar Motivos de Aus�ncia';
      if ($_GET['acao_confirmada']=='sim'){
        try{
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjMotivoAusenciaDTO = array();
          foreach ($arrStrIds as $strId) {
            $objMotivoAusenciaDTO = new MotivoAusenciaDTO();
            $objMotivoAusenciaDTO->setNumIdMotivoAusencia($strId);
            $arrObjMotivoAusenciaDTO[] = $objMotivoAusenciaDTO;
          }
          $objMotivoAusenciaRN = new MotivoAusenciaRN();
          $objMotivoAusenciaRN->reativar($arrObjMotivoAusenciaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        } 
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
        die;
      } 
      break;


    case 'motivo_ausencia_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Motivo de Aus�ncia','Selecionar Motivos de Aus�ncia');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='motivo_ausencia_cadastrar'){
        if (isset($_GET['id_motivo_ausencia'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_motivo_ausencia']);
        }
      }
      break;

    case 'motivo_ausencia_listar':
      $strTitulo = 'Motivos de Aus�ncia';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'motivo_ausencia_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  if ($_GET['acao'] == 'motivo_ausencia_listar' || $_GET['acao'] == 'motivo_ausencia_selecionar'){
    $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_cadastrar');
    if ($bolAcaoCadastrar){
      $arrComandos[] = '<button type="button" accesskey="N" id="btnNovo" value="Novo" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_ausencia_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ovo</button>';
    }
  }

  $objMotivoAusenciaDTO = new MotivoAusenciaDTO();
  $objMotivoAusenciaDTO->retNumIdMotivoAusencia();
  $objMotivoAusenciaDTO->retStrDescricao();

    $objMotivoAusenciaDTO->setBolExclusaoLogica(false);
    $objMotivoAusenciaDTO->retStrSinAtivo();

  PaginaSEI::getInstance()->prepararOrdenacao($objMotivoAusenciaDTO, 'Descricao', InfraDTO::$TIPO_ORDENACAO_ASC);

  $objMotivoAusenciaRN = new MotivoAusenciaRN();
  $arrObjMotivoAusenciaDTO = $objMotivoAusenciaRN->listar($objMotivoAusenciaDTO);

  $numRegistros = InfraArray::contar($arrObjMotivoAusenciaDTO);

  if ($numRegistros > 0){

    $bolCheck = false;

    if ($_GET['acao']=='motivo_ausencia_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_alterar');
      $bolAcaoImprimir = false;
      //$bolAcaoGerarPlanilha = false;
      $bolAcaoExcluir = false;
      $bolAcaoDesativar = false;
      $bolCheck = true;
    }else{
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_alterar');
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_excluir');
      $bolAcaoDesativar = SessaoSEI::getInstance()->verificarPermissao('motivo_ausencia_desativar');
    }

    
    if ($bolAcaoDesativar){
//      $bolCheck = true;
//      $arrComandos[] = '<button type="button" accesskey="t" id="btnDesativar" value="Desativar" onclick="acaoDesativacaoMultipla();" class="infraButton">Desa<span class="infraTeclaAtalho">t</span>ivar</button>';
      $strLinkDesativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_ausencia_desativar&acao_origem='.$_GET['acao']);
    }

    if ($bolAcaoReativar){
//      $bolCheck = true;
//      $arrComandos[] = '<button type="button" accesskey="R" id="btnReativar" value="Reativar" onclick="acaoReativacaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">R</span>eativar</button>';
      $strLinkReativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_ausencia_reativar&acao_origem='.$_GET['acao'].'&acao_confirmada=sim');
    }
    

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_ausencia_excluir&acao_origem='.$_GET['acao']);
    }

    /*
    if ($bolAcaoGerarPlanilha){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="P" id="btnGerarPlanilha" value="Gerar Planilha" onclick="infraGerarPlanilhaTabela(\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=infra_gerar_planilha_tabela').'\');" class="infraButton">Gerar <span class="infraTeclaAtalho">P</span>lanilha</button>';
    }
    */

    $strResultado = '';

      $strSumarioTabela = 'Tabela de Motivos de Aus�ncia.';
      $strCaptionTabela = 'Motivos de Aus�ncia';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objMotivoAusenciaDTO,'Descri��o','Descricao',$arrObjMotivoAusenciaDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="15%">A��es</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      if ($arrObjMotivoAusenciaDTO[$i]->getStrSinAtivo()=='S'){
      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      }else{
        $strCssTr = '<tr class="trVermelha">';
      }

      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjMotivoAusenciaDTO[$i]->getNumIdMotivoAusencia(),$arrObjMotivoAusenciaDTO[$i]->getStrDescricao()).'</td>';
      }
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjMotivoAusenciaDTO[$i]->getStrDescricao()).'</td>';
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjMotivoAusenciaDTO[$i]->getNumIdMotivoAusencia());


      if ($bolAcaoAlterar && $arrObjMotivoAusenciaDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_ausencia_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_motivo_ausencia='.$arrObjMotivoAusenciaDTO[$i]->getNumIdMotivoAusencia()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Motivo de Aus�ncia" alt="Alterar Motivo de Aus�ncia" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoDesativar || $bolAcaoReativar || $bolAcaoExcluir){
        $strId = $arrObjMotivoAusenciaDTO[$i]->getNumIdMotivoAusencia();
        $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjMotivoAusenciaDTO[$i]->getStrDescricao());
      }

      if ($bolAcaoDesativar && $arrObjMotivoAusenciaDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoDesativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeDesativar().'" title="Desativar Motivo de Aus�ncia" alt="Desativar Motivo de Aus�ncia" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoReativar && $arrObjMotivoAusenciaDTO[$i]->getStrSinAtivo()=='N'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoReativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeReativar().'" title="Reativar Motivo de Aus�ncia" alt="Reativar Motivo de Aus�ncia" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoExcluir){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoExcluir(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Motivo de Aus�ncia" alt="Excluir Motivo de Aus�ncia" class="infraImg" /></a>&nbsp;';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'motivo_ausencia_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='motivo_ausencia_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('btnFechar').focus();
  }
  infraEfeitoTabelas();
}

<? if ($bolAcaoDesativar){ ?>
function acaoDesativar(id,desc){
  if (confirm("Confirma desativa��o do Motivo de Aus�ncia \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoAusenciaLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoAusenciaLista').submit();
  }
}

function acaoDesativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Aus�ncia selecionado.');
    return;
  }
  if (confirm("Confirma desativa��o dos Motivos de Aus�ncia selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoAusenciaLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoAusenciaLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoReativar){ ?>
function acaoReativar(id,desc){
  if (confirm("Confirma reativa��o do Motivo de Aus�ncia \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoAusenciaLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoAusenciaLista').submit();
  }
}

function acaoReativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Aus�ncia selecionado.');
    return;
  }
  if (confirm("Confirma reativa��o dos Motivos de Aus�ncia selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoAusenciaLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoAusenciaLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclus�o do Motivo de Aus�ncia \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoAusenciaLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoAusenciaLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Aus�ncia selecionado.');
    return;
  }
  if (confirm("Confirma exclus�o dos Motivos de Aus�ncia selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoAusenciaLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoAusenciaLista').submit();
  }
}
<? } ?>

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmMotivoAusenciaLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  //PaginaSEI::getInstance()->abrirAreaDados('5em');
  //PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>