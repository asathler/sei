<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class AtributoAndamentoSessaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'atributo_andamento_sessao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdAtributoAndamentoSessao','id_atributo_andamento_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Chave','chave');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Valor','valor');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'IdOrigem','id_origem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdAndamentoSessao','id_andamento_sessao');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdTarefaSessaoAndamentoSessao','id_tarefa_sessao','andamento_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DTH,'ExecucaoAndamentoSessao','dth_execucao','andamento_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamentoAndamentoSessao','id_sessao_julgamento','andamento_sessao');

    $this->configurarPK('IdAtributoAndamentoSessao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdAndamentoSessao', 'andamento_sessao', 'id_andamento_sessao');
  }
}
?>