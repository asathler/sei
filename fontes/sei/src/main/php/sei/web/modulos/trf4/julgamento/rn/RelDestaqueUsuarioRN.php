<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class RelDestaqueUsuarioRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdDestaque(RelDestaqueUsuarioDTO $objRelDestaqueUsuarioDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelDestaqueUsuarioDTO->getNumIdDestaque())){
      $objInfraException->adicionarValidacao('Destaque n�o informado.');
    }
  }

  private function validarNumIdUsuario(RelDestaqueUsuarioDTO $objRelDestaqueUsuarioDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelDestaqueUsuarioDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
    }
  }

  protected function cadastrarControlado(RelDestaqueUsuarioDTO $objRelDestaqueUsuarioDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_destaque_usuario_cadastrar',__METHOD__,$objRelDestaqueUsuarioDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdDestaque($objRelDestaqueUsuarioDTO, $objInfraException);
      $this->validarNumIdUsuario($objRelDestaqueUsuarioDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objRelDestaqueUsuarioBD = new RelDestaqueUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelDestaqueUsuarioBD->cadastrar($objRelDestaqueUsuarioDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Visualizacao do Destaque.',$e);
    }
  }

  protected function alterarControlado(RelDestaqueUsuarioDTO $objRelDestaqueUsuarioDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('rel_destaque_usuario_alterar',__METHOD__,$objRelDestaqueUsuarioDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objRelDestaqueUsuarioDTO->isSetNumIdDestaque()){
        $this->validarNumIdDestaque($objRelDestaqueUsuarioDTO, $objInfraException);
      }
      if ($objRelDestaqueUsuarioDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objRelDestaqueUsuarioDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objRelDestaqueUsuarioBD = new RelDestaqueUsuarioBD($this->getObjInfraIBanco());
      $objRelDestaqueUsuarioBD->alterar($objRelDestaqueUsuarioDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Visualizacao do Destaque.',$e);
    }
  }

  protected function excluirControlado($arrObjRelDestaqueUsuarioDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_destaque_usuario_excluir',__METHOD__,$arrObjRelDestaqueUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelDestaqueUsuarioBD = new RelDestaqueUsuarioBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjRelDestaqueUsuarioDTO);$i++){
        $objRelDestaqueUsuarioBD->excluir($arrObjRelDestaqueUsuarioDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Visualizacao do Destaque.',$e);
    }
  }

  protected function consultarConectado(RelDestaqueUsuarioDTO $objRelDestaqueUsuarioDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_destaque_usuario_consultar',__METHOD__,$objRelDestaqueUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelDestaqueUsuarioBD = new RelDestaqueUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelDestaqueUsuarioBD->consultar($objRelDestaqueUsuarioDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Visualizacao do Destaque.',$e);
    }
  }

  protected function listarConectado(RelDestaqueUsuarioDTO $objRelDestaqueUsuarioDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_destaque_usuario_listar',__METHOD__,$objRelDestaqueUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelDestaqueUsuarioBD = new RelDestaqueUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelDestaqueUsuarioBD->listar($objRelDestaqueUsuarioDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Visualiza��es do Destaque.',$e);
    }
  }

  protected function contarConectado(RelDestaqueUsuarioDTO $objRelDestaqueUsuarioDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_destaque_usuario_listar',__METHOD__,$objRelDestaqueUsuarioDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelDestaqueUsuarioBD = new RelDestaqueUsuarioBD($this->getObjInfraIBanco());
      $ret = $objRelDestaqueUsuarioBD->contar($objRelDestaqueUsuarioDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Visualiza��es do Destaque.',$e);
    }
  }
}
?>