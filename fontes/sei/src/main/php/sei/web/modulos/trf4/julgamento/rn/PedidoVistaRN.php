<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 11/11/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PedidoVistaRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarDthPedido(PedidoVistaDTO $objPedidoVistaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPedidoVistaDTO->getDthPedido())){
      $objPedidoVistaDTO->setDthPedido(null);
    }else{
      if (!InfraData::validarDataHora($objPedidoVistaDTO->getDthPedido())){
        $objInfraException->adicionarValidacao('Data do Pedido inv�lida.');
      }
    }
  }
  private function validarDthDevolucao(PedidoVistaDTO $objPedidoVistaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPedidoVistaDTO->getDthDevolucao())){
      $objPedidoVistaDTO->setDthDevolucao(null);
    }else{
      if (!InfraData::validarDataHora($objPedidoVistaDTO->getDthDevolucao())){
        $objInfraException->adicionarValidacao('Data da Devolu��o inv�lida.');
      }
    }
  }

  private function validarNumIdDistribuicao(PedidoVistaDTO $objPedidoVistaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPedidoVistaDTO->getNumIdDistribuicao())){
      $objInfraException->adicionarValidacao('Distribui��o n�o informada.');
    }
  }
  private function validarNumIdUsuario(PedidoVistaDTO $objPedidoVistaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPedidoVistaDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Membro do Colegiado n�o informado.');
    }
  }
  private function validarNumIdUnidade(PedidoVistaDTO $objPedidoVistaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPedidoVistaDTO->getNumIdUnidade())){
      $objInfraException->adicionarValidacao('Unidade do Membro do Colegiado n�o informada.');
    }
  }
  private function validarStrMotivo(PedidoVistaDTO $objPedidoVistaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objPedidoVistaDTO->getStrMotivo())){
      $objInfraException->adicionarValidacao('Motivo de devolu��o do processo n�o informado.');
    }
  }

  protected function confirmarControlado(PedidoVistaDTO $objPedidoVistaDTO){
    try{
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_cadastrar',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //verifica se j� possui pedido de vista pendente
      $objPedidoVistaDTOBanco=new PedidoVistaDTO();
      $objPedidoVistaDTOBanco->setNumIdPedidoVista($objPedidoVistaDTO->getNumIdPedidoVista());
      $objPedidoVistaDTOBanco->setDthDevolucao(null);
      $objPedidoVistaDTOBanco->setStrSinPendente('S');
      $objPedidoVistaDTOBanco->retNumIdPedidoVista();
      $objPedidoVistaDTOBanco->retNumIdDistribuicao();
      $objPedidoVistaDTOBanco->retNumIdUsuario();
      $objPedidoVistaDTOBanco->retStrNomeUsuario();
      $objPedidoVistaDTOBanco=$this->consultar($objPedidoVistaDTOBanco);

      $idUsuarioVista=$objPedidoVistaDTOBanco->getNumIdUsuario();

      if ($objPedidoVistaDTOBanco==null){
        $objInfraException->lancarValidacao('Pedido de vista pendente n�o localizado.');
      }

      $objPedidoVistaDTOBanco->setStrSinPendente('N');
      $objPedidoVistaBD = new PedidoVistaBD($this->getObjInfraIBanco());
      $objPedidoVistaBD->alterar($objPedidoVistaDTOBanco);

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($objPedidoVistaDTOBanco->getNumIdDistribuicao());
      $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento(SessaoJulgamentoRN::$ES_ABERTA);
      $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrNomeColegiado();
      $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrStaDistribuicao();
      $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      //lan�ar andamento no processo de pedido de vista
      if($objItemSessaoJulgamentoDTO) { //se vier de redistribui��o n�o possui item
        $arrObjAtributoAndamentoDTO = array();
        $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
        $objAtributoAndamentoDTO->setStrNome('COLEGIADO');
        $objAtributoAndamentoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrNomeColegiado());
        $objAtributoAndamentoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
        $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;


        $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
        $objAtributoAndamentoDTO->setStrNome('USUARIO');
        $objAtributoAndamentoDTO->setStrValor($objPedidoVistaDTOBanco->getStrNomeUsuario());
        $objAtributoAndamentoDTO->setStrIdOrigem($idUsuarioVista);
        $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

        $objAtividadeDTO = new AtividadeDTO();
        $objAtividadeDTO->setDblIdProtocolo($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
        $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objAtividadeDTO->setStrIdTarefaModuloTarefa(TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_PEDIDO_VISTA);
        $objAtividadeDTO->setArrObjAtributoAndamentoDTO($arrObjAtributoAndamentoDTO);

        $objAtividadeRN = new AtividadeRN();
        $objAtividadeRN->gerarInternaRN0727($objAtividadeDTO);
        //lan�ar andamento na sess�o de pedido de vista
        $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
        $objAndamentoSessaoRN = new AndamentoSessaoRN();

        $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_PEDIDO_VISTA);

        $arrObjAtributoAndamentoSessaoDTO = array();

        $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
        $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
        $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
        $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo());
        $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

        $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
        $objAtributoAndamentoSessaoDTO->setStrChave('USUARIO');
        $objAtributoAndamentoSessaoDTO->setStrIdOrigem($idUsuarioVista);
        $objAtributoAndamentoSessaoDTO->setStrValor($objPedidoVistaDTOBanco->getStrNomeUsuario());
        $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

        $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
        $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);
      }


    }catch(Exception $e){
      throw new InfraException('Erro confirmando Pedido de Vista.',$e);
    }

  }
  protected function cadastrarControlado(PedidoVistaDTO $objPedidoVistaDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_cadastrar',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdDistribuicao($objPedidoVistaDTO, $objInfraException);
      $this->validarDthPedido($objPedidoVistaDTO, $objInfraException);
      $this->validarDthDevolucao($objPedidoVistaDTO, $objInfraException);
      $this->validarNumIdUsuario($objPedidoVistaDTO, $objInfraException);
      $this->validarNumIdUnidade($objPedidoVistaDTO, $objInfraException);

      $objInfraException->lancarValidacoes();
      //verifica se j� possui pedido de vista para este item
      $objPedidoVistaDTOBanco=new PedidoVistaDTO();
      $objPedidoVistaDTOBanco->setNumIdDistribuicao($objPedidoVistaDTO->getNumIdDistribuicao());
      $objPedidoVistaDTOBanco->setDthDevolucao(null);
      $objPedidoVistaDTOBanco->setStrSinPendente('S');
      $objPedidoVistaDTOBanco->retNumIdPedidoVista();
      if ($this->contar($objPedidoVistaDTOBanco)>0){
//        return $objPedidoVistaDTOBanco;
        $objInfraException->lancarValidacao('Este processo j� est� com pedido de vista.');
      }

      $objPedidoVistaDTO->setStrSinPendente('S');
      $objPedidoVistaBD = new PedidoVistaBD($this->getObjInfraIBanco());
      $ret = $objPedidoVistaBD->cadastrar($objPedidoVistaDTO);


      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Pedido de Vista.',$e);
    }
  }

  protected function devolverControlado(PedidoVistaDTO $objPedidoVistaDTO){
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_cadastrar',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdDistribuicao($objPedidoVistaDTO,$objInfraException);
      $this->validarNumIdUsuario($objPedidoVistaDTO,$objInfraException);
      $this->validarDthDevolucao($objPedidoVistaDTO,$objInfraException);

      $objInfraException->lancarValidacoes();

      //consulta os pedido de vista do usu�rio
      $objPedidoVistaDTOBanco=new PedidoVistaDTO();
      $objPedidoVistaDTOBanco->setNumIdDistribuicao($objPedidoVistaDTO->getNumIdDistribuicao());
      $objPedidoVistaDTOBanco->setNumIdUsuario($objPedidoVistaDTO->getNumIdUsuario());
      $objPedidoVistaDTOBanco->retNumIdPedidoVista();
      $objPedidoVistaDTOBanco->retNumIdDistribuicao();
      $objPedidoVistaDTOBanco->retDthDevolucao();
      $arrObjPedidoVistaDTO=$this->listar($objPedidoVistaDTOBanco);
      if (InfraArray::contar($arrObjPedidoVistaDTO)==0) {
        $objInfraException->lancarValidacao('N�o foi encontrado o Pedido de Vista para ser devolvido.');
      }

      $bolPedidoVistaAtivo=false;
      foreach ($arrObjPedidoVistaDTO as $objPedidoVistaDTOBanco) {
        if($objPedidoVistaDTOBanco->getDthDevolucao()==null){
          $objPedidoVistaDTOBanco->setDthDevolucao($objPedidoVistaDTO->getDthDevolucao());
          $this->alterar($objPedidoVistaDTOBanco);
          $bolPedidoVistaAtivo=true;
        }
      }

      if (!$bolPedidoVistaAtivo){
        $objInfraException->lancarValidacao('Pedido de Vista j� foi devolvido.');
      }

    }catch(Exception $e){
      throw new InfraException('Erro cancelando Pedido de Vista.',$e);
    }
  }

  protected function devolverSecretariaControlado(PedidoVistaDTO $objPedidoVistaDTO){
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_devolver_secretaria',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrMotivo($objPedidoVistaDTO,$objInfraException);
      $objInfraException->lancarValidacoes();

      //verifica se n�o est� em sess�o n�o finalizada
      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($objPedidoVistaDTO->getNumIdDistribuicao());
      $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_FINALIZADA),InfraDTO::$OPER_NOT_IN);
      $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      if($objItemSessaoJulgamentoRN->contar($objItemSessaoJulgamentoDTO)>0){
        $objInfraException->lancarValidacao('Processo inclu�do em sess�o de julgamento n�o finalizada.');
      }


      $this->devolver($objPedidoVistaDTO);

      $objDistribuicaoDTO=new DistribuicaoDTO();
      $objDistribuicaoRN=new DistribuicaoRN();
      $objDistribuicaoDTO->setNumIdDistribuicao($objPedidoVistaDTO->getNumIdDistribuicao());
      $objDistribuicaoDTO->retStrStaDistribuicao();
      $objDistribuicaoDTO->retNumIdDistribuicao();
      $objDistribuicaoDTO->retDblIdProcedimento();
      $objDistribuicaoDTO->retNumIdUnidade();
      $objDistribuicaoDTO->retNumIdUsuario();
      $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);

      //atualizar estado da distribui��o para estado anterior
      $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_DISTRIBUIDO);
      $objDistribuicaoRN->alterarEstado($objDistribuicaoDTO);

      // lancar andamento no processo: devolucao de pedido de vista para a secretaria
      $objSeiRN=new SeiRN();
      $objAtributoAndamentoAPI=new AtributoAndamentoAPI();
      $objAtributoAndamentoAPI->setIdOrigem(null);
      $objAtributoAndamentoAPI->setNome('MOTIVO');
      $objAtributoAndamentoAPI->setValor($objPedidoVistaDTO->getStrMotivo());

      $objEntradaLancarAndamentoAPI=new EntradaLancarAndamentoAPI();
      $objEntradaLancarAndamentoAPI->setIdTarefaModulo(TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_DEVOLUCAO_SECRETARIA);
      $objEntradaLancarAndamentoAPI->setIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
      $objEntradaLancarAndamentoAPI->setAtributos(array($objAtributoAndamentoAPI));
      $objSeiRN->lancarAndamento($objEntradaLancarAndamentoAPI);

      //devolver para a unidade que distribuiu o processo.
      $objProcedimentoRN = new ProcedimentoRN();
      $objProcedimentoDTO=new ProcedimentoDTO();
      $objProcedimentoDTO->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
      $objProcedimentoDTO->retStrProtocoloProcedimentoFormatado();
      $objProcedimentoDTO->retStrNomeTipoProcedimento();
      $objProcedimentoDTO->retStrStaNivelAcessoGlobalProtocolo();
      $objProcedimentoDTO=$objProcedimentoRN->consultarRN0201($objProcedimentoDTO);

      $objAtividadeRN=new AtividadeRN();

      //se � a secretaria que est� puxando de volta, s� reabre o processo
      if(SessaoSEI::getInstance()->getNumIdUnidadeAtual()==$objDistribuicaoDTO->getNumIdUnidade()){
        $objAtividadeDTO = new AtividadeDTO();
        $objAtividadeDTO->retNumIdAtividade();
        $objAtividadeDTO->setNumMaxRegistrosRetorno(1);
        $objAtividadeDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
        $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objAtividadeDTO->setDthConclusao(null);

        if ($objAtividadeRN->consultarRN0033($objAtividadeDTO)==null){
          $objReabrirProcessoDTO = new ReabrirProcessoDTO();
          $objReabrirProcessoDTO->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
          $objReabrirProcessoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
          $objReabrirProcessoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
          $objProcedimentoRN->reabrirRN0966($objReabrirProcessoDTO);
        }
      } else {
        $objPesquisaPendenciaDTO = new PesquisaPendenciaDTO();
        $objPesquisaPendenciaDTO->setDblIdProtocolo(array($objDistribuicaoDTO->getDblIdProcedimento()));
        $objPesquisaPendenciaDTO->setNumIdUsuario($objPedidoVistaDTO->getNumIdUsuario());
        $objPesquisaPendenciaDTO->setNumIdUnidade($objPedidoVistaDTO->getNumIdUnidade());
        $arrObjProcedimentoDTO = $objAtividadeRN->listarPendenciasRN0754($objPesquisaPendenciaDTO);

        $objAtividadeDTO = new AtividadeDTO();
        $objAtividadeDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
        $objAtividadeDTO->setNumIdUsuario(null);
        $objAtividadeDTO->setNumIdUsuarioOrigem(SessaoSEI::getInstance()->getNumIdUsuario());
        $objAtividadeDTO->setNumIdUnidade($objDistribuicaoDTO->getNumIdUnidade());
        $objAtividadeDTO->setNumIdUnidadeOrigem(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
        $objAtividadeDTO->setDtaPrazo(null);

        if ($objProcedimentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO){
          $objAcessoDTO=new AcessoDTO();
          $objAcessoDTO->setDblIdProtocolo($objDistribuicaoDTO->getDblIdProcedimento());
          $objAcessoDTO->setNumIdUsuario($objDistribuicaoDTO->getNumIdUsuario());
          $objAcessoDTO->setNumIdUnidade($objDistribuicaoDTO->getNumIdUnidade());
          $objAcessoDTO->retNumIdAcesso();
          $objAcessoDTO->setStrStaTipo(AcessoRN::$TA_CREDENCIAL_PROCESSO);
          $objAcessoDTO->setNumMaxRegistrosRetorno(1);
          $objAcessoRN=new AcessoRN();
          if($objAcessoRN->consultar($objAcessoDTO)==null){
            $objConcederCredencialDTO = new ConcederCredencialDTO();
            $objConcederCredencialDTO->setDblIdProcedimento($objDistribuicaoDTO->getDblIdProcedimento());
            $objConcederCredencialDTO->setNumIdUsuario($objDistribuicaoDTO->getNumIdUsuario());
            $objConcederCredencialDTO->setNumIdUnidade($objDistribuicaoDTO->getNumIdUnidade());
            $objConcederCredencialDTO->setArrAtividadesOrigem($arrObjProcedimentoDTO[0]->getArrObjAtividadeDTO());
            $objAtividadeRN->concederCredencial($objConcederCredencialDTO);
          }
        } else {
          $objEnviarProcessoDTO = new EnviarProcessoDTO();
          $objEnviarProcessoDTO->setArrAtividadesOrigem($arrObjProcedimentoDTO[0]->getArrObjAtividadeDTO());
          $objEnviarProcessoDTO->setArrAtividades(array($objAtividadeDTO));
          $objEnviarProcessoDTO->setStrSinManterAberto('N');
          $objEnviarProcessoDTO->setStrSinEnviarEmailNotificacao('N');
          $objEnviarProcessoDTO->setStrSinRemoverAnotacoes('N');

          $objAtividadeRN->enviarRN0023($objEnviarProcessoDTO);
        }
      }

      //Auditoria


    }catch(Exception $e){
      throw new InfraException('Erro devolvendo o processo para a Secretaria.',$e);
    }
  }
  protected function alterarControlado(PedidoVistaDTO $objPedidoVistaDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_alterar',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objPedidoVistaDTO->isSetDthPedido()){
        $this->validarDthPedido($objPedidoVistaDTO, $objInfraException);
      }
      if ($objPedidoVistaDTO->isSetDthDevolucao()){
        $this->validarDthDevolucao($objPedidoVistaDTO, $objInfraException);
      }
      $objPedidoVistaDTO->unSetNumIdDistribuicao();

      if ($objPedidoVistaDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objPedidoVistaDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objPedidoVistaBD = new PedidoVistaBD($this->getObjInfraIBanco());
      $objPedidoVistaBD->alterar($objPedidoVistaDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Pedido de Vista.',$e);
    }
  }

  protected function excluirControlado($arrObjPedidoVistaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_excluir',__METHOD__,$arrObjPedidoVistaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPedidoVistaBD = new PedidoVistaBD($this->getObjInfraIBanco());
      foreach ($arrObjPedidoVistaDTO as $objPedidoVistaDTO) {
        $objPedidoVistaBD->excluir($objPedidoVistaDTO);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Pedido de Vista.',$e);
    }
  }

  protected function consultarConectado(PedidoVistaDTO $objPedidoVistaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_consultar',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPedidoVistaBD = new PedidoVistaBD($this->getObjInfraIBanco());
      $ret = $objPedidoVistaBD->consultar($objPedidoVistaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Pedido de Vista.',$e);
    }
  }

  protected function listarConectado(PedidoVistaDTO $objPedidoVistaDTO) {
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_listar',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPedidoVistaBD = new PedidoVistaBD($this->getObjInfraIBanco());
      $ret = $objPedidoVistaBD->listar($objPedidoVistaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Pedidos de Vista.',$e);
    }
  }

  protected function contarConectado(PedidoVistaDTO $objPedidoVistaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('pedido_vista_listar',__METHOD__,$objPedidoVistaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objPedidoVistaBD = new PedidoVistaBD($this->getObjInfraIBanco());
      $ret = $objPedidoVistaBD->contar($objPedidoVistaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Pedidos de Vista.',$e);
    }
  }

}
?>