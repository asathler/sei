<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 19/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class VotoOpcaoEleicaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdVotoEleicao(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoOpcaoEleicaoDTO->getNumIdVotoEleicao())){
      $objVotoOpcaoEleicaoDTO->setNumIdVotoEleicao(null);
    }
  }

  private function validarNumIdOpcaoEleicao(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoOpcaoEleicaoDTO->getNumIdOpcaoEleicao())){
      $objInfraException->adicionarValidacao('Op��o n�o informada.');
    }
  }

  private function validarNumOrdem(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objVotoOpcaoEleicaoDTO->getNumOrdem())){
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }

  protected function cadastrarControlado(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO) {
    try{

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_cadastrar', __METHOD__, $objVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdVotoEleicao($objVotoOpcaoEleicaoDTO, $objInfraException);
      $this->validarNumIdOpcaoEleicao($objVotoOpcaoEleicaoDTO, $objInfraException);
      $this->validarNumOrdem($objVotoOpcaoEleicaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoOpcaoEleicaoBD->cadastrar($objVotoOpcaoEleicaoDTO);

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Op��o do Voto.',$e);
    }
  }

  protected function alterarControlado(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_alterar', __METHOD__, $objVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objVotoOpcaoEleicaoDTO->isSetNumIdVotoEleicao()){
        $this->validarNumIdVotoEleicao($objVotoOpcaoEleicaoDTO, $objInfraException);
      }
      if ($objVotoOpcaoEleicaoDTO->isSetNumIdOpcaoEleicao()){
        $this->validarNumIdOpcaoEleicao($objVotoOpcaoEleicaoDTO, $objInfraException);
      }
      if ($objVotoOpcaoEleicaoDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objVotoOpcaoEleicaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      $objVotoOpcaoEleicaoBD->alterar($objVotoOpcaoEleicaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro alterando Op��o do Voto.',$e);
    }
  }

  protected function excluirControlado($arrObjVotoOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_excluir', __METHOD__, $arrObjVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjVotoOpcaoEleicaoDTO);$i++){
        $objVotoOpcaoEleicaoBD->excluir($arrObjVotoOpcaoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Op��o do Voto.',$e);
    }
  }

  protected function consultarConectado(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_consultar', __METHOD__, $objVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoOpcaoEleicaoBD->consultar($objVotoOpcaoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Op��o do Voto.',$e);
    }
  }

  protected function listarConectado(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO) {
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_listar', __METHOD__, $objVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoOpcaoEleicaoBD->listar($objVotoOpcaoEleicaoDTO);

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Op��es do Voto.',$e);
    }
  }

  protected function contarConectado(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_listar', __METHOD__, $objVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoOpcaoEleicaoBD->contar($objVotoOpcaoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Op��es do Voto.',$e);
    }
  }
/* 
  protected function desativarControlado($arrObjVotoOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_desativar', __METHOD__, $arrObjVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjVotoOpcaoEleicaoDTO);$i++){
        $objVotoOpcaoEleicaoBD->desativar($arrObjVotoOpcaoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro desativando Op��o do Voto.',$e);
    }
  }

  protected function reativarControlado($arrObjVotoOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_reativar', __METHOD__, $arrObjVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjVotoOpcaoEleicaoDTO);$i++){
        $objVotoOpcaoEleicaoBD->reativar($arrObjVotoOpcaoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro reativando Op��o do Voto.',$e);
    }
  }

  protected function bloquearControlado(VotoOpcaoEleicaoDTO $objVotoOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('voto_opcao_eleicao_consultar', __METHOD__, $objVotoOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoOpcaoEleicaoBD = new VotoOpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objVotoOpcaoEleicaoBD->bloquear($objVotoOpcaoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Op��o do Voto.',$e);
    }
  }

 */
}
