<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class AtributoAndamentoSessaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrChave(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAtributoAndamentoSessaoDTO->getStrChave())){
      $objInfraException->adicionarValidacao('Chave n�o informada.');
    }else{
      $objAtributoAndamentoSessaoDTO->setStrChave(trim($objAtributoAndamentoSessaoDTO->getStrChave()));

      if (strlen($objAtributoAndamentoSessaoDTO->getStrChave())>50){
        $objInfraException->adicionarValidacao('Chave possui tamanho superior a 50 caracteres.');
      }
    }
  }

  private function validarStrValor(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAtributoAndamentoSessaoDTO->getStrValor())){
      $objAtributoAndamentoSessaoDTO->setStrValor(null);
    }else{
      $objAtributoAndamentoSessaoDTO->setStrValor(trim($objAtributoAndamentoSessaoDTO->getStrValor()));

      if (strlen($objAtributoAndamentoSessaoDTO->getStrValor())>4000){
        $objInfraException->adicionarValidacao('Valor possui tamanho superior a 4000 caracteres.');
      }
    }
  }

  private function validarStrIdOrigem(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAtributoAndamentoSessaoDTO->getStrIdOrigem())){
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem(null);
    }else{
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem(trim($objAtributoAndamentoSessaoDTO->getStrIdOrigem()));

      if (strlen($objAtributoAndamentoSessaoDTO->getStrIdOrigem())>50){
        $objInfraException->adicionarValidacao('ID Origem possui tamanho superior a 50 caracteres.');
      }
    }
  }

  private function validarNumIdAndamentoSessao(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAtributoAndamentoSessaoDTO->getNumIdAndamentoSessao())){
      $objInfraException->adicionarValidacao('Andamento da Sess�o n�o informado.');
    }
  }

  protected function cadastrarControlado(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('atributo_andamento_sessao_cadastrar',__METHOD__,$objAtributoAndamentoSessaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrChave($objAtributoAndamentoSessaoDTO, $objInfraException);
      $this->validarStrValor($objAtributoAndamentoSessaoDTO, $objInfraException);
      $this->validarStrIdOrigem($objAtributoAndamentoSessaoDTO, $objInfraException);
      $this->validarNumIdAndamentoSessao($objAtributoAndamentoSessaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objAtributoAndamentoSessaoBD = new AtributoAndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAtributoAndamentoSessaoBD->cadastrar($objAtributoAndamentoSessaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Atributo do Andamento.',$e);
    }
  }

  protected function alterarControlado(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('atributo_andamento_sessao_alterar',__METHOD__,$objAtributoAndamentoSessaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objAtributoAndamentoSessaoDTO->isSetStrChave()){
        $this->validarStrChave($objAtributoAndamentoSessaoDTO, $objInfraException);
      }
      if ($objAtributoAndamentoSessaoDTO->isSetStrValor()){
        $this->validarStrValor($objAtributoAndamentoSessaoDTO, $objInfraException);
      }
      if ($objAtributoAndamentoSessaoDTO->isSetStrIdOrigem()){
        $this->validarStrIdOrigem($objAtributoAndamentoSessaoDTO, $objInfraException);
      }
      if ($objAtributoAndamentoSessaoDTO->isSetNumIdAndamentoSessao()){
        $this->validarNumIdAndamentoSessao($objAtributoAndamentoSessaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objAtributoAndamentoSessaoBD = new AtributoAndamentoSessaoBD($this->getObjInfraIBanco());
      $objAtributoAndamentoSessaoBD->alterar($objAtributoAndamentoSessaoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Atributo do Andamento.',$e);
    }
  }

  protected function excluirControlado($arrObjAtributoAndamentoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('atributo_andamento_sessao_excluir',__METHOD__,$arrObjAtributoAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAtributoAndamentoSessaoBD = new AtributoAndamentoSessaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjAtributoAndamentoSessaoDTO);$i++){
        $objAtributoAndamentoSessaoBD->excluir($arrObjAtributoAndamentoSessaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Atributo do Andamento.',$e);
    }
  }

  protected function consultarConectado(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('atributo_andamento_sessao_consultar',__METHOD__,$objAtributoAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAtributoAndamentoSessaoBD = new AtributoAndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAtributoAndamentoSessaoBD->consultar($objAtributoAndamentoSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Atributo do Andamento.',$e);
    }
  }

  protected function listarConectado(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('atributo_andamento_sessao_listar',__METHOD__,$objAtributoAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAtributoAndamentoSessaoBD = new AtributoAndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAtributoAndamentoSessaoBD->listar($objAtributoAndamentoSessaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Atributos do Andamento.',$e);
    }
  }

  protected function contarConectado(AtributoAndamentoSessaoDTO $objAtributoAndamentoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('atributo_andamento_sessao_listar',__METHOD__,$objAtributoAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAtributoAndamentoSessaoBD = new AtributoAndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAtributoAndamentoSessaoBD->contar($objAtributoAndamentoSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Atributos do Andamento.',$e);
    }
  }
}
?>