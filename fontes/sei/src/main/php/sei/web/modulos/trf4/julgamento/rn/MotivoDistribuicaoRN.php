<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class MotivoDistribuicaoRN extends InfraRN {

  public static $TIPO_PREVENCAO = 'P';
  public static $TIPO_IMPEDIMENTO = 'I';

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  public function listarValoresTipo(){
    try {

      $objArrTipoDTO = array();

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$TIPO_IMPEDIMENTO);
      $objTipoDTO->setStrDescricao('Impedimento');
      $objArrTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$TIPO_PREVENCAO);
      $objTipoDTO->setStrDescricao('Preven��o');
      $objArrTipoDTO[] = $objTipoDTO;

      return $objArrTipoDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de Tipo.',$e);
    }
  }

  private function validarStrDescricao(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoDistribuicaoDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('Motivo n�o informado.');
    }else{
      $objMotivoDistribuicaoDTO->setStrDescricao(trim($objMotivoDistribuicaoDTO->getStrDescricao()));

      if (strlen($objMotivoDistribuicaoDTO->getStrDescricao())>250){
        $objInfraException->adicionarValidacao('Motivo possui tamanho superior a 250 caracteres.');
      }

      $dto = new MotivoDistribuicaoDTO();
      $dto->setBolExclusaoLogica(false);
      $dto->retStrSinAtivo();
      $dto->setNumIdMotivoDistribuicao($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao(),InfraDTO::$OPER_DIFERENTE);
      $dto->setStrStaTipo($objMotivoDistribuicaoDTO->getStrStaTipo());
      $dto->setStrDescricao($objMotivoDistribuicaoDTO->getStrDescricao());
      $dto = $this->consultar($dto);

      if ($dto!=null) {
        if ($dto->getStrSinAtivo()=='S') {
          $objInfraException->adicionarValidacao('Existe outra ocorr�ncia com esta Descri��o para o mesmo Tipo.');
        } else {
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa com esta Descri��o para o mesmo Tipo.');
        }
      }
    }
  }

  private function validarStrSinAtivo(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoDistribuicaoDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objMotivoDistribuicaoDTO->getStrSinAtivo())){
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }

  private function validarStrStaTipo(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoDistribuicaoDTO->getStrStaTipo())){
      $objInfraException->adicionarValidacao('Tipo n�o informado.');
    }else{
      if (!in_array($objMotivoDistribuicaoDTO->getStrStaTipo(),InfraArray::converterArrInfraDTO($this->listarValoresTipo(),'StaTipo'))){
        $objInfraException->adicionarValidacao('Tipo inv�lido.');
      }
    }
  }

  protected function cadastrarControlado(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_cadastrar',__METHOD__,$objMotivoDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objMotivoDistribuicaoDTO, $objInfraException);
      $this->validarStrSinAtivo($objMotivoDistribuicaoDTO, $objInfraException);
      $this->validarStrStaTipo($objMotivoDistribuicaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoDistribuicaoBD->cadastrar($objMotivoDistribuicaoDTO);

      $arrObjRelMotivoDistrColegiadoDTO = $objMotivoDistribuicaoDTO->getArrObjRelMotivoDistrColegiadoDTO();
      $objRelMotivoDistrColegiadoRN = new RelMotivoDistrColegiadoRN();
      /** @var RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO */
      foreach ($arrObjRelMotivoDistrColegiadoDTO as $objRelMotivoDistrColegiadoDTO) {
        $objRelMotivoDistrColegiadoDTO->setNumIdMotivoDistribuicao($ret->getNumIdMotivoDistribuicao());
        $objRelMotivoDistrColegiadoRN->cadastrar($objRelMotivoDistrColegiadoDTO);
      }
      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Motivo de Distribui��o.',$e);
    }
  }

  protected function alterarControlado(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_alterar',__METHOD__,$objMotivoDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objMotivoDistribuicaoDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objMotivoDistribuicaoDTO, $objInfraException);
      }
      if ($objMotivoDistribuicaoDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objMotivoDistribuicaoDTO, $objInfraException);
      }
      if ($objMotivoDistribuicaoDTO->isSetStrStaTipo()){
        $objMotivoDistribuicaoDTOBanco=new MotivoDistribuicaoDTO();
        $objMotivoDistribuicaoDTOBanco->setNumIdMotivoDistribuicao($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao());
        $objMotivoDistribuicaoDTOBanco->retStrStaTipo();
        $objMotivoDistribuicaoDTOBanco=$this->consultar($objMotivoDistribuicaoDTOBanco);
        if($objMotivoDistribuicaoDTOBanco->getStrStaTipo()!=$objMotivoDistribuicaoDTO->getStrStaTipo()){
          $objInfraException->adicionarValidacao('Tipo do motivo de distribui��o n�o pode ser alterado.');
        }
      }

      $objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      $objMotivoDistribuicaoBD->alterar($objMotivoDistribuicaoDTO);

      if($objMotivoDistribuicaoDTO->isSetArrObjRelMotivoDistrColegiadoDTO()){
        $objRelMotivoDistrColegiadoRN = new RelMotivoDistrColegiadoRN();
        $objRelMotivoDistrColegiadoDTO=new RelMotivoDistrColegiadoDTO();
        $objRelMotivoDistrColegiadoDTO->setNumIdMotivoDistribuicao($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao());
        $objRelMotivoDistrColegiadoDTO->retTodos();
        $objRelMotivoDistrColegiadoRN->excluir($objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO));

        $arrObjRelMotivoDistrColegiadoDTO = $objMotivoDistribuicaoDTO->getArrObjRelMotivoDistrColegiadoDTO();
        /** @var RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO */
        foreach ($arrObjRelMotivoDistrColegiadoDTO as $objRelMotivoDistrColegiadoDTO) {
          $objRelMotivoDistrColegiadoDTO->setNumIdMotivoDistribuicao($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao());
          $objRelMotivoDistrColegiadoRN->cadastrar($objRelMotivoDistrColegiadoDTO);
        }
      }




      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Motivo de Distribui��o.',$e);
    }
  }

  protected function excluirControlado($arrObjMotivoDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_excluir',__METHOD__,$arrObjMotivoDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();
      $arrId=InfraArray::converterArrInfraDTO($arrObjMotivoDistribuicaoDTO,'IdMotivoDistribuicao');
      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());

      if(count($arrId)>0){
        $objMotivoDistribuicaoDTO=new MotivoDistribuicaoDTO();
        $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($arrId,InfraDTO::$OPER_IN);
        $objMotivoDistribuicaoDTO->retStrStaTipo();
        $objMotivoDistribuicaoDTO->retNumIdMotivoDistribuicao();
        $objMotivoDistribuicaoDTO->setBolExclusaoLogica(false);
        $arrObjMotivoDistribuicaoDTO=$this->listar($objMotivoDistribuicaoDTO);

      }
      for($i=0, $iMax = count($arrObjMotivoDistribuicaoDTO); $i<$iMax; $i++){
        if ($arrObjMotivoDistribuicaoDTO[$i]->getStrStaTipo()==self::$TIPO_IMPEDIMENTO){
          $objImpedimentoDTO=new ImpedimentoDTO();
          $objImpedimentoRN=new ImpedimentoRN();
          $objImpedimentoDTO->setNumIdMotivoImpedimento($arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao());
          if ($objImpedimentoRN->contar($objImpedimentoDTO)){
            $objInfraException->lancarValidacao('Motivo j� foi utilizado em impedimentos.');
          }
        } elseif ($arrObjMotivoDistribuicaoDTO[$i]->getStrStaTipo()==self::$TIPO_PREVENCAO){
          $objDistribuicaoDTO=new DistribuicaoDTO();
          $objDistribuicaoRN=new DistribuicaoRN();
          $objDistribuicaoDTO->setNumIdMotivoPrevencao($arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao());
          if ($objDistribuicaoRN->contar($objDistribuicaoDTO)>0){
            $objInfraException->lancarValidacao('Motivo j� foi utilizado em preven��es.');
          }
        } 

        $objRelMotivoDistrColegiadoDTO=new RelMotivoDistrColegiadoDTO();
        $objRelMotivoDistrColegiadoRN=new RelMotivoDistrColegiadoRN();
        $objRelMotivoDistrColegiadoDTO->setNumIdMotivoDistribuicao($arrObjMotivoDistribuicaoDTO[$i]->getNumIdMotivoDistribuicao());
        $objRelMotivoDistrColegiadoDTO->retTodos();
        $arrObjRelMotivoDistrColegiadoDTO=$objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO);
        $objRelMotivoDistrColegiadoRN->excluir($arrObjRelMotivoDistrColegiadoDTO);

        $objMotivoDistribuicaoBD->excluir($arrObjMotivoDistribuicaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Motivo de Distribui��o.',$e);
    }
  }

  protected function consultarConectado(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_consultar',__METHOD__,$objMotivoDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoDistribuicaoBD->consultar($objMotivoDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Motivo de Distribui��o.',$e);
    }
  }

  protected function listarConectado(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_listar',__METHOD__,$objMotivoDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();
      if($objMotivoDistribuicaoDTO->isSetNumIdColegiado()){
        $objRelMotivoDistrColegiadoDTO = new RelMotivoDistrColegiadoDTO();
        $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($objMotivoDistribuicaoDTO->getNumIdColegiado());
        $objRelMotivoDistrColegiadoDTO->retNumIdMotivoDistribuicao();
        $objRelMotivoDistrColegiadoRN = new RelMotivoDistrColegiadoRN();
        $arrObjRelMotivoDistrColegiadoDTO = $objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO);
        if(count($arrObjRelMotivoDistrColegiadoDTO)==0){
          return array();
        }
        $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao(InfraArray::converterArrInfraDTO($arrObjRelMotivoDistrColegiadoDTO,'IdMotivoDistribuicao'),InfraDTO::$OPER_IN);
      }

      //$objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoDistribuicaoBD->listar($objMotivoDistribuicaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Motivos de Distribui��o.',$e);
    }
  }

  protected function contarConectado(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_listar',__METHOD__,$objMotivoDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoDistribuicaoBD->contar($objMotivoDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Motivos de Distribui��o.',$e);
    }
  }

  protected function desativarControlado($arrObjMotivoDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_desativar',__METHOD__,$arrObjMotivoDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = count($arrObjMotivoDistribuicaoDTO); $i<$iMax; $i++){

        $objMotivoDistribuicaoBD->desativar($arrObjMotivoDistribuicaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Motivo de Distribui��o.',$e);
    }
  }

  protected function reativarControlado($arrObjMotivoDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_reativar',__METHOD__,$arrObjMotivoDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = count($arrObjMotivoDistribuicaoDTO); $i<$iMax; $i++){
        $objMotivoDistribuicaoBD->reativar($arrObjMotivoDistribuicaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Motivo de Distribui��o.',$e);
    }
  }

  protected function bloquearControlado(MotivoDistribuicaoDTO $objMotivoDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_distribuicao_consultar',__METHOD__,$objMotivoDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoDistribuicaoBD = new MotivoDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoDistribuicaoBD->bloquear($objMotivoDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Motivo de Distribui��o.',$e);
    }
  }


}
?>