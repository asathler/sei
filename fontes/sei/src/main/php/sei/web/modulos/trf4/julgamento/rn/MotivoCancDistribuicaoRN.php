<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 16/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MotivoCancDistribuicaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrDescricao(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoCancDistribuicaoDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('Descri��o n�o informada.');
    }else{
      $objMotivoCancDistribuicaoDTO->setStrDescricao(trim($objMotivoCancDistribuicaoDTO->getStrDescricao()));

      if (strlen($objMotivoCancDistribuicaoDTO->getStrDescricao())>250){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 250 caracteres.');
      }

      $dto = new MotivoCancDistribuicaoDTO();
      $dto->setBolExclusaoLogica(false);
      $dto->retStrSinAtivo();
      $dto->setNumIdMotivoCancDistribuicao($objMotivoCancDistribuicaoDTO->getNumIdMotivoCancDistribuicao(),InfraDTO::$OPER_DIFERENTE);
      $dto->setStrDescricao($objMotivoCancDistribuicaoDTO->getStrDescricao());
      $dto = $this->consultar($dto);

      if ($dto!=null) {
        if ($dto->getStrSinAtivo()=='S') {
          $objInfraException->adicionarValidacao('Existe outra ocorr�ncia com esta Descri��o.');
        } else {
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa com esta Descri��o.');
        }
      }

    }
  }

  private function validarStrSinAtivo(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objMotivoCancDistribuicaoDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objMotivoCancDistribuicaoDTO->getStrSinAtivo())){
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }

  protected function cadastrarControlado(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_cadastrar',__METHOD__,$objMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objMotivoCancDistribuicaoDTO, $objInfraException);
      $this->validarStrSinAtivo($objMotivoCancDistribuicaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoCancDistribuicaoBD->cadastrar($objMotivoCancDistribuicaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Motivo de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function alterarControlado(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_alterar',__METHOD__,$objMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objMotivoCancDistribuicaoDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objMotivoCancDistribuicaoDTO, $objInfraException);
      }
      if ($objMotivoCancDistribuicaoDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objMotivoCancDistribuicaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      $objMotivoCancDistribuicaoBD->alterar($objMotivoCancDistribuicaoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Motivo de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function excluirControlado($arrObjMotivoCancDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_excluir',__METHOD__,$arrObjMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      $arrId=InfraArray::converterArrInfraDTO($arrObjMotivoCancDistribuicaoDTO,'IdMotivoCancDistribuicao');

      if(InfraArray::contar($arrId)>0){
        $objMotivoCancDistribuicaoDTO=new MotivoCancDistribuicaoDTO();
        $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($arrId,InfraDTO::$OPER_IN);
        $objMotivoCancDistribuicaoDTO->retNumIdMotivoCancDistribuicao();
        $objMotivoCancDistribuicaoDTO->setBolExclusaoLogica(false);
        $arrObjMotivoCancDistribuicaoDTO=$this->listar($objMotivoCancDistribuicaoDTO);

      }
      $objDistribuicaoDTO=new DistribuicaoDTO();
      $objDistribuicaoRN=new DistribuicaoRN();

      for($i=0;$i<InfraArray::contar($arrObjMotivoCancDistribuicaoDTO);$i++){
        $objDistribuicaoDTO->setNumIdMotivoCancelamento($arrObjMotivoCancDistribuicaoDTO[$i]->getNumIdMotivoCancDistribuicao());
        if ($objDistribuicaoRN->contar($objDistribuicaoDTO)>0){
          $objInfraException->lancarValidacao('Motivo de cancelamento j� foi utilizado em distribui��es.');
        }
        
        $objMotivoCancDistribuicaoBD->excluir($arrObjMotivoCancDistribuicaoDTO[$i]);
      }

      
      
      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Motivo de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function consultarConectado(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_consultar',__METHOD__,$objMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoCancDistribuicaoBD->consultar($objMotivoCancDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Motivo de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function listarConectado(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_listar',__METHOD__,$objMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoCancDistribuicaoBD->listar($objMotivoCancDistribuicaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Motivos de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function contarConectado(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_listar',__METHOD__,$objMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoCancDistribuicaoBD->contar($objMotivoCancDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Motivos de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function desativarControlado($arrObjMotivoCancDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_desativar',__METHOD__,$arrObjMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjMotivoCancDistribuicaoDTO);$i++){
        $objMotivoCancDistribuicaoBD->desativar($arrObjMotivoCancDistribuicaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Motivo de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function reativarControlado($arrObjMotivoCancDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_reativar',__METHOD__,$arrObjMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjMotivoCancDistribuicaoDTO);$i++){
        $objMotivoCancDistribuicaoBD->reativar($arrObjMotivoCancDistribuicaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Motivo de Cancelamento de Distribui��o.',$e);
    }
  }

  protected function bloquearControlado(MotivoCancDistribuicaoDTO $objMotivoCancDistribuicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('motivo_canc_distribuicao_consultar',__METHOD__,$objMotivoCancDistribuicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objMotivoCancDistribuicaoBD = new MotivoCancDistribuicaoBD($this->getObjInfraIBanco());
      $ret = $objMotivoCancDistribuicaoBD->bloquear($objMotivoCancDistribuicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Motivo de Cancelamento de Distribui��o.',$e);
    }
  }


}
?>