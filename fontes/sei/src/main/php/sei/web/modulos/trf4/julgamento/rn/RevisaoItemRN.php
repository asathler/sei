<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 20/12/2019 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class RevisaoItemRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdItemSessaoJulgamento(RevisaoItemDTO $objRevisaoItemDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRevisaoItemDTO->getNumIdItemSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Item da Sess�o n�o informado.');
    }
  }

  private function validarNumIdUnidade(RevisaoItemDTO $objRevisaoItemDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRevisaoItemDTO->getNumIdUnidade())){
      $objInfraException->adicionarValidacao('Unidade n�o informada.');
    }
  }

  private function validarNumIdUsuario(RevisaoItemDTO $objRevisaoItemDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRevisaoItemDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
    }
  }

  private function validarStrSinRevisado(RevisaoItemDTO $objRevisaoItemDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRevisaoItemDTO->getStrSinRevisado())){
      $objInfraException->adicionarValidacao('Sinalizador de Revisado n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objRevisaoItemDTO->getStrSinRevisado())){
        $objInfraException->adicionarValidacao('Sinalizador de Revisado inv�lid.');
      }
    }
  }

  protected function cadastrarControlado(RevisaoItemDTO $objRevisaoItemDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('revisao_item_cadastrar',__METHOD__,$objRevisaoItemDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdItemSessaoJulgamento($objRevisaoItemDTO, $objInfraException);
      $this->validarStrSinRevisado($objRevisaoItemDTO, $objInfraException);

      $objRevisaoItemDTO2=new RevisaoItemDTO();
      $objRevisaoItemDTO2->setNumIdItemSessaoJulgamento($objRevisaoItemDTO->getNumIdItemSessaoJulgamento());
      $objRevisaoItemDTO2->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objRevisaoItemDTO2->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());

      $objRevisaoItemDTO2->retTodos();
      $objRevisaoItemDTOBanco=$this->consultar($objRevisaoItemDTO2);
      if($objRevisaoItemDTOBanco){
        if($objRevisaoItemDTO->getStrSinRevisado()==$objRevisaoItemDTOBanco->getStrSinRevisado()){
          return $objRevisaoItemDTOBanco;
        }
        $objRevisaoItemDTOBanco->setDthRevisao(InfraData::getStrDataHoraAtual());
        $objRevisaoItemDTOBanco->setStrSinRevisado($objRevisaoItemDTO->getStrSinRevisado());
        $this->alterar($objRevisaoItemDTOBanco);
        return $objRevisaoItemDTOBanco;
      }

      $objRevisaoItemDTO2->setDthRevisao(InfraData::getStrDataHoraAtual());
      $objRevisaoItemDTO2->setStrSinRevisado($objRevisaoItemDTO->getStrSinRevisado());


      $objInfraException->lancarValidacoes();
      $objRevisaoItemBD = new RevisaoItemBD($this->getObjInfraIBanco());
      $ret = $objRevisaoItemBD->cadastrar($objRevisaoItemDTO2);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Revis�o.',$e);
    }
  }

  protected function alterarControlado(RevisaoItemDTO $objRevisaoItemDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('revisao_item_alterar',__METHOD__,$objRevisaoItemDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objRevisaoItemDTO->isSetNumIdItemSessaoJulgamento()){
        $this->validarNumIdItemSessaoJulgamento($objRevisaoItemDTO, $objInfraException);
      }
      if ($objRevisaoItemDTO->isSetNumIdUnidade()){
        $this->validarNumIdUnidade($objRevisaoItemDTO, $objInfraException);
      }
      if ($objRevisaoItemDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objRevisaoItemDTO, $objInfraException);
      }
      if ($objRevisaoItemDTO->isSetStrSinRevisado()){
        $this->validarStrSinRevisado($objRevisaoItemDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objRevisaoItemBD = new RevisaoItemBD($this->getObjInfraIBanco());
      $objRevisaoItemBD->alterar($objRevisaoItemDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Revis�o.',$e);
    }
  }

  protected function excluirControlado($arrObjRevisaoItemDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('revisao_item_excluir',__METHOD__,$arrObjRevisaoItemDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRevisaoItemBD = new RevisaoItemBD($this->getObjInfraIBanco());
      for($i=0, $iMax = count($arrObjRevisaoItemDTO); $i<$iMax; $i++){
        $objRevisaoItemBD->excluir($arrObjRevisaoItemDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Revis�o.',$e);
    }
  }

  protected function consultarConectado(RevisaoItemDTO $objRevisaoItemDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('revisao_item_consultar',__METHOD__,$objRevisaoItemDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRevisaoItemBD = new RevisaoItemBD($this->getObjInfraIBanco());
      $ret = $objRevisaoItemBD->consultar($objRevisaoItemDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Revis�o.',$e);
    }
  }

  protected function listarConectado(RevisaoItemDTO $objRevisaoItemDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('revisao_item_listar',__METHOD__,$objRevisaoItemDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRevisaoItemBD = new RevisaoItemBD($this->getObjInfraIBanco());
      $ret = $objRevisaoItemBD->listar($objRevisaoItemDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Revis�es.',$e);
    }
  }

  protected function contarConectado(RevisaoItemDTO $objRevisaoItemDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('revisao_item_listar',__METHOD__,$objRevisaoItemDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRevisaoItemBD = new RevisaoItemBD($this->getObjInfraIBanco());
      $ret = $objRevisaoItemBD->contar($objRevisaoItemDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Revis�es.',$e);
    }
  }

}
?>