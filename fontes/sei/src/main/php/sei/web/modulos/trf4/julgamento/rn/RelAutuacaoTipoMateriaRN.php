<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 13/03/2020 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class RelAutuacaoTipoMateriaRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdAutuacao(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelAutuacaoTipoMateriaDTO->getNumIdAutuacao())){
      $objInfraException->adicionarValidacao('Autua��o n�o informada.');
    }
  }

  private function validarNumIdTipoMateria(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelAutuacaoTipoMateriaDTO->getNumIdTipoMateria())){
      $objInfraException->adicionarValidacao('Tipo de Mat�ria n�o informado.');
    }
  }

  private function validarNumOrdem(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelAutuacaoTipoMateriaDTO->getNumOrdem())){
      $objRelAutuacaoTipoMateriaDTO->setNumOrdem(null);
    }
  }

  protected function cadastrarControlado(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_autuacao_tipo_materia_cadastrar',__METHOD__,$objRelAutuacaoTipoMateriaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdAutuacao($objRelAutuacaoTipoMateriaDTO, $objInfraException);
      $this->validarNumIdTipoMateria($objRelAutuacaoTipoMateriaDTO, $objInfraException);
      $this->validarNumOrdem($objRelAutuacaoTipoMateriaDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objRelAutuacaoTipoMateriaBD->cadastrar($objRelAutuacaoTipoMateriaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Tipo de Mat�ria.',$e);
    }
  }

  protected function alterarControlado(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('rel_autuacao_tipo_materia_alterar',__METHOD__,$objRelAutuacaoTipoMateriaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objRelAutuacaoTipoMateriaDTO->isSetNumIdAutuacao()){
        $this->validarNumIdAutuacao($objRelAutuacaoTipoMateriaDTO, $objInfraException);
      }
      if ($objRelAutuacaoTipoMateriaDTO->isSetNumIdTipoMateria()){
        $this->validarNumIdTipoMateria($objRelAutuacaoTipoMateriaDTO, $objInfraException);
      }
      if ($objRelAutuacaoTipoMateriaDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objRelAutuacaoTipoMateriaDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      $objRelAutuacaoTipoMateriaBD->alterar($objRelAutuacaoTipoMateriaDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Tipo de Mat�ria.',$e);
    }
  }

  protected function excluirControlado($arrObjRelAutuacaoTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_autuacao_tipo_materia_excluir',__METHOD__,$arrObjRelAutuacaoTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      for($i=0, $iMax = count($arrObjRelAutuacaoTipoMateriaDTO); $i<$iMax; $i++){
        $objRelAutuacaoTipoMateriaBD->excluir($arrObjRelAutuacaoTipoMateriaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Tipo de Mat�ria.',$e);
    }
  }

  protected function consultarConectado(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_autuacao_tipo_materia_consultar',__METHOD__,$objRelAutuacaoTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objRelAutuacaoTipoMateriaBD->consultar($objRelAutuacaoTipoMateriaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Tipo de Mat�ria.',$e);
    }
  }

  protected function listarConectado(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_autuacao_tipo_materia_listar',__METHOD__,$objRelAutuacaoTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objRelAutuacaoTipoMateriaBD->listar($objRelAutuacaoTipoMateriaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Tipos de Mat�ria.',$e);
    }
  }

  protected function contarConectado(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_autuacao_tipo_materia_listar',__METHOD__,$objRelAutuacaoTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objRelAutuacaoTipoMateriaBD->contar($objRelAutuacaoTipoMateriaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Tipos de Mat�ria.',$e);
    }
  }
/* 
  protected function desativarControlado($arrObjRelAutuacaoTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('rel_autuacao_tipo_materia_desativar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjRelAutuacaoTipoMateriaDTO);$i++){
        $objRelAutuacaoTipoMateriaBD->desativar($arrObjRelAutuacaoTipoMateriaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Tipo de Mat�ria.',$e);
    }
  }

  protected function reativarControlado($arrObjRelAutuacaoTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('rel_autuacao_tipo_materia_reativar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjRelAutuacaoTipoMateriaDTO);$i++){
        $objRelAutuacaoTipoMateriaBD->reativar($arrObjRelAutuacaoTipoMateriaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Tipo de Mat�ria.',$e);
    }
  }

  protected function bloquearControlado(RelAutuacaoTipoMateriaDTO $objRelAutuacaoTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('rel_autuacao_tipo_materia_consultar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelAutuacaoTipoMateriaBD = new RelAutuacaoTipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objRelAutuacaoTipoMateriaBD->bloquear($objRelAutuacaoTipoMateriaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Tipo de Mat�ria.',$e);
    }
  }

 */
}
?>