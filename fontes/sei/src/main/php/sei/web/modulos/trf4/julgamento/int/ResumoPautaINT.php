<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ResumoPautaINT extends InfraINT {


  /**
   * @param AutuacaoDTO $objAutuacaoDTO
   * @return string
   */
  public static function montarDivAutuacao(AutuacaoDTO $objAutuacaoDTO): string
  {
    $strResultado='';
    if ($objAutuacaoDTO->getStrDescricaoTipoMateria()!=null) {
      $strResultado .= '<div class="row align-items-center"><div class="col-1 divRotulo">Tipo da Mat�ria:</div>';
      $strResultado .= '<div class="col">';
      $strResultado .= PaginaSEI::tratarHTML($objAutuacaoDTO->getStrDescricaoTipoMateria());
      $strResultado .= "</div></div>\n";
    }
    if ($objAutuacaoDTO->getStrDescricao()!=null) {
      $strResultado .= '<div class="row align-items-center"><div class="col-1 divRotulo">Descri��o:</div>';
      $strResultado .= '<div class="col">';
      $strResultado .= PaginaSEI::tratarHTML($objAutuacaoDTO->getStrDescricao());
      $strResultado .= "</div></div>\n";
    }
    return $strResultado;
  }

  /**
   * @param ParticipanteDTO[] $arrObjParticipanteDTO
   * @return string
   */
  public static function montarDivInteressados(array $arrObjParticipanteDTO): string
  {
    $strResultado = '<div class="row align-items-center"><div class="col-1 divRotulo">Interessados:</div>';
    $strResultado .= '<div class="col">';
    $strSeparador = '';
    foreach ($arrObjParticipanteDTO as $objParticipanteDTO) {
      $strResultado .= $strSeparador . PaginaSEI::tratarHTML($objParticipanteDTO->getStrNomeContato());
      $strSeparador = ', ';
    }
    $strResultado .= "</div></div>\n";
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @return string
   */
  public static function montarDivProcesso(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {

    $strClasseProcesso = 'ancoraPadraoAzul';
    $style = 'padding:0;font-size:1.3em !important;';
    if ($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO) {
      $strClasseProcesso = 'processoVisualizadoSigiloso';
      $style = 'padding:0 10px;font-size:1.3em !important;';
    }
    $strResultado = '<div class="row align-items-center" style="font-size: 1.1em;"><div class="col-2 col-lg-1 divRotulo">Processo:</div>';
    $strResultado .= '<div class="col">';
    $strLink = '<a onclick="event.stopPropagation();" href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()) . '" target="_blank" ' . PaginaSEI::montarTitleTooltip($objItemSessaoJulgamentoDTO->getStrDescricaoProtocolo()) . ' class="' . $strClasseProcesso . '"  style="' . $style . '">' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a>';
    $strResultado .= $strLink . ' - ' . PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . "</div></div>\n";
    return $strResultado;
  }

  /**
   * @param ParteProcedimentoDTO[] $arrObjParteProcedimentoDTO
   * @return string
   */
  public static function montarDivPartes(array $arrObjParteProcedimentoDTO): string
  {
    $j = InfraArray::contar($arrObjParteProcedimentoDTO);

    if ($j==0) {
      return '';
    }

    $strResultado = '<div class="row"><div class="col-1 divRotulo">Partes:</div>';
    $strResultado .= '<div class="col">';

    foreach ($arrObjParteProcedimentoDTO as $objParteProcedimentoDTO) {
      $strResultado .= PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrNomeContato()) . ' (' . PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrDescricaoQualificacaoParte()) . ')';
      if (-- $j>0) {
        $strResultado .= $j>1 ? ', ' : ' e ';
      }
    }

    $strResultado .= "</div></div>\n";

    return $strResultado;
  }

  /**
   * @param $staItem
   * @param ColegiadoComposicaoDTO $objColegiadoComposicaoDTO
   * @return string
   */
  public static function montarDivUsuarioSessao($staItem, ColegiadoComposicaoDTO $objColegiadoComposicaoDTO): string
  {
    $strResultado = '';
    if ($staItem!=TipoSessaoBlocoRN::$STA_REFERENDO) {
      $strResultado .= '<div class="row align-items-center"><div class="col-1 divRotulo">';
      if ($staItem==TipoSessaoBlocoRN::$STA_PAUTA) {
        $strResultado .= 'Relator: ';
      } else {
        $strResultado .= 'Trazido por: ';
      }
      $strResultado .= '</div><div class="col">';
      $strResultado .= PaginaSEI::tratarHTML($objColegiadoComposicaoDTO->getStrNomeUsuario()) . "</div></div>\n";
    }
    return $strResultado;
  }

  /**
   * @param array $arrObjProtocoloDTO
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @return string
   */
  public static function montarDivDocumentos(array $arrObjProtocoloDTO, ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {
    $staItem=$objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
    $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
    $strResultado = '<div class="row"><div class="col-1 divRotulo">Documentos:</div>';
    $bolExibiuBalao = false;
    $strResultado .= '<div class="col">';
    foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {

      $strResultado .= '<div class="row m-0 align-items-center"><div>' . PaginaSEI::tratarHTML($objItemSessaoDocumentoDTO->getStrNomeUsuario()) . '</div>';

      //link documento
      $dblIdDocumento = $objItemSessaoDocumentoDTO->getDblIdDocumento();
      if (isset($arrObjProtocoloDTO[$dblIdDocumento])) {
        $acaoJs = "exibirDoc({$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()},$dblIdDocumento,this);";
      } else {
        $acaoJs = "alert('Sem acesso ao documento.');event.stopPropagation();";
      }
      $strDocumento = '<a href="javascript:void(0);" onclick="' . $acaoJs . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"  class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
      $strResultado .= '<div class="px-2">(' . PaginaSEI::tratarHTML($objItemSessaoDocumentoDTO->getStrNomeSerie()) . ' ' . $strDocumento . ')</div>';

      if (!$bolExibiuBalao && $objItemSessaoJulgamentoDTO->getStrSinManual()=='N') {
        if ($staItem==TipoSessaoBlocoRN::$STA_REFERENDO) {
          $bolExibiuBalao = true;
        }
        /** @var JulgamentoParteDTO $objJulgamentoParteDTO */
        foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
          $arrObjVotoParteDTO = $objJulgamentoParteDTO->getArrObjVotoParteDTO();
          if (isset($arrObjVotoParteDTO[$objItemSessaoDocumentoDTO->getNumIdUsuario()])) {
            /** @var VotoParteDTO $objVotoParteDTO */
            $objVotoParteDTO = $arrObjVotoParteDTO[$objItemSessaoDocumentoDTO->getNumIdUsuario()];
            $balao = InfraString::transformarCaixaAlta($objVotoParteDTO->getStrConteudoProvimento()) . ' ' . $objVotoParteDTO->getStrComplemento();
            if ($objVotoParteDTO->getStrStaVotoParte()==VotoParteRN::$STA_ACOMPANHA && $objVotoParteDTO->getNumIdProvimento()==null) {
              $numIdUsuarioAcompanhado = $objVotoParteDTO->getNumIdUsuarioVotoParte();
              if ($numIdUsuarioAcompanhado==null) {
                $numIdUsuarioAcompanhado = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
              }
              if (isset($arrObjVotoParteDTO[$numIdUsuarioAcompanhado])) {
                $balao = $arrObjVotoParteDTO[$numIdUsuarioAcompanhado]->getStrConteudoProvimento() . ' ' . $arrObjVotoParteDTO[$numIdUsuarioAcompanhado]->getStrComplemento();
              }
            }
            $icone = MdJulgarIcone::BALAO1;
            $strResultado .= '<div>';
            $strResultado .= ' <a href="javascript:void(0);" style="vertical-align:middle" ' . PaginaSEI::montarTitleTooltip($balao, $objJulgamentoParteDTO->getStrDescricao()) . '><img src="' . $icone . '" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
            $strResultado .= "</div>";
          }
        }
      }


      $strResultado .= '</div>';
    }
    $strResultado .= "</div></div>\n";
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @return string
   */
  public static function montarDivDispositivo(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {
    $idItem = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
    $strResultado = '<div class="row"><div class="col-1 divRotulo">Dispositivo:</div>';
    $strResultado .= '<div class="col">';
    $strDispositivo = PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrDispositivo());

    if ($strDispositivo!=null) {
      $pos = strrpos($strDispositivo, "\nPresentes:");
      if ($pos!==false) {
        $strDispositivo = substr($strDispositivo, 0, $pos);
      }

      $strResultado .= '<p class="pDispositivo" id="pDispositivo' . $idItem . '" >' . nl2br(trim($strDispositivo)) . '</p>' . "\n";
    } else {
      $strResultado .= '<p class="pDispositivo" id="pDispositivo' . $idItem . '" style="display:none"></p>' . "\n";
    }

    $strResultado .= "</div></div>";
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param bool $bolAdmin
   * @param array $arrSituacaoItem
   * @return string
   */
  public static function montarColunaDireita(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, bool $bolAdmin, array $arrSituacaoItem): string
  {
    $staSessao=$objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
    $bolBloqueado = in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(), array(ItemSessaoJulgamentoRN::$STA_JULGADO, ItemSessaoJulgamentoRN::$STA_DILIGENCIA, ItemSessaoJulgamentoRN::$STA_ADIADO, ItemSessaoJulgamentoRN::$STA_RETIRADO));
    $bolDisponibilizado = InfraArray::contar($arrObjItemSessaoDocumentoDTO)>0;
    $bolPossuiVotos = $objItemSessaoJulgamentoDTO->getStrSinPossuiVotos()==='S';
    $bolAcaoJulgar = ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && $bolAdmin && !$bolBloqueado && $bolDisponibilizado);
    $bolAcaoConsultarJulgamento = ($bolDisponibilizado && $bolPossuiVotos &&
        !in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(), array(ItemSessaoJulgamentoRN::$STA_RETIRADO, ItemSessaoJulgamentoRN::$STA_ADIADO, ItemSessaoJulgamentoRN::$STA_DILIGENCIA)));

    $strResultado = '<div class="col-1" style="text-align: center;">';
    //julgar
    $strResultado .= SessaoJulgamentoINT::montarIconeJulgar($bolAcaoJulgar, $objItemSessaoJulgamentoDTO, $bolAcaoConsultarJulgamento, true);

    //revis�o de processos
    $strResultado .= SessaoJulgamentoINT::montarIconeRevisao($objItemSessaoJulgamentoDTO);

    $staSituacao = $objItemSessaoJulgamentoDTO->getStrStaSituacao();
    if ($staSituacao!==ItemSessaoJulgamentoRN::$STA_NORMAL) {
      $strResultado .= '<br><span class="tdStatus">' . $arrSituacaoItem[$staSituacao] . '</span>';
    }
    $strResultado .= '</div>' . "\n";
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param array $arrObjProtocoloDTO
   * @return string
   * @throws InfraException
   */
  public static function montarDivConteudoDocumetos(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, array $arrObjProtocoloDTO): string
  {
    $idItem = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();

    $strResultado = "<div id='doc$idItem' onclick=\"if(event.target.nodeName!='A'){exibirDoc($idItem);}\" style='display:none'>\n";

    $objDocumentoRN = new DocumentoRN();
    $objDocumentoDTO = new DocumentoDTO();
    $objDocumentoDTO->retDblIdDocumento();
    $objDocumentoDTO->retStrNomeSerie();
    $objDocumentoDTO->retStrNumero();
    $objDocumentoDTO->retStrSiglaUnidadeGeradoraProtocolo();
    $objDocumentoDTO->retStrProtocoloDocumentoFormatado();
    $objDocumentoDTO->retStrStaProtocoloProtocolo();
    $objDocumentoDTO->retStrStaDocumento();
    $objDocumentoDTO->retDblIdDocumentoEdoc();

    $objEditorRN = new EditorRN();
    foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
      $idDocumento = $objItemSessaoDocumentoDTO->getDblIdDocumento();
      $strResultado .= "<div id='dd$idDocumento' class='documentoResumo'>\n";
      $strConteudoDoc = '';
      if (!isset($arrObjProtocoloDTO[$idDocumento])) {
        $strConteudoDoc = '<p>Sem acesso ao documento.</p>';
      } else {
        $objDocumentoDTO2 = clone $objDocumentoDTO;
        $objDocumentoDTO2->setDblIdDocumento($idDocumento);
        $objDocumentoDTO2 = $objDocumentoRN->consultarRN0005($objDocumentoDTO2);
        if ($objDocumentoDTO2==null) {
          $strConteudoDoc = '<p>Documento n�o encontrado.</p>';
        } else if ($objDocumentoDTO2->getStrStaDocumento()===DocumentoRN::$TD_EDITOR_INTERNO) {

          $objEditorDTO = new EditorDTO();
          $objEditorDTO->setDblIdDocumento($idDocumento);
          $objEditorDTO->setNumIdBaseConhecimento(null);
          $objEditorDTO->setStrSinCabecalho('N');
          $objEditorDTO->setStrSinRodape('N');
          $objEditorDTO->setStrSinCarimboPublicacao('S');
          $objEditorDTO->setStrSinIdentificacaoVersao('N');
          $objEditorDTO->setStrSinAssinaturas('N');
          $objEditorDTO->setStrSinProcessarLinks('S');

          $strConteudoDoc = $objEditorRN->consultarHtmlVersao($objEditorDTO);
          $a = strpos($strConteudoDoc, '<body');
          $a = strpos($strConteudoDoc, '>', $a);
          $strConteudoDoc = substr($strConteudoDoc, $a + 1);
          $a = strpos($strConteudoDoc, '</body>');
          $strConteudoDoc = substr($strConteudoDoc, 0, $a);
          $strConteudoDoc = preg_replace('@(<span[^<>]*)(data-scayt-word="[^<>]*")([^<>]*>)([^<]*)</span>@i', '$4', $strConteudoDoc);
          $strConteudoDoc = str_replace('<div style="font-weight: 500; text-align: left; font-size: 9pt; border: 2px solid #777; position: absolute; left: 67%; padding: 4px;">',
              '<div class="divPublicacao">', $strConteudoDoc);

          $objAuditoriaProtocoloDTO = new AuditoriaProtocoloDTO();
          $objAuditoriaProtocoloDTO->setStrRecurso($_GET['acao']);
          $objAuditoriaProtocoloDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
          $objAuditoriaProtocoloDTO->setDblIdProtocolo($_GET['id_documento']);
          $objAuditoriaProtocoloDTO->setNumIdAnexo(null);
          $objAuditoriaProtocoloDTO->setDtaAuditoria(InfraData::getStrDataAtual());
          $objAuditoriaProtocoloDTO->setNumVersao($objEditorDTO->getNumVersao());

          $objAuditoriaProtocoloRN = new AuditoriaProtocoloRN();
          $objAuditoriaProtocoloRN->auditarVisualizacao($objAuditoriaProtocoloDTO);

        }

      }
      $strResultado .= $strConteudoDoc;
      $strResultado .= "</div>\n";
    }


    $strResultado .= "</div>\n";
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return string
   * @throws InfraException
   */
  public static function montarColunaEsquerda(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, SessaoJulgamentoDTO $objSessaoJulgamentoDTO): string
  {
    $qtd = 0;
    $strDestaque = '';
    $strSinRevisado = 'N';
    $staSessao = $objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento();
    $arrObjDestaqueDTO = $objItemSessaoJulgamentoDTO->getArrObjDestaqueDTO();
    $bolAcaoDestaques = ($staSessao!==SessaoJulgamentoRN::$ES_CANCELADA && $staSessao!==SessaoJulgamentoRN::$ES_PAUTA_ABERTA &&
        (InfraArray::contar($arrObjDestaqueDTO)>0 || !in_array($staSessao, array(SessaoJulgamentoRN::$ES_ENCERRADA, SessaoJulgamentoRN::$ES_FINALIZADA))));
    if ($bolAcaoDestaques) {
      $strDestaque = '<div class="divDestaque">';
      $strDestaque .= DestaqueINT::montarIconeDestaques($objItemSessaoJulgamentoDTO->getArrObjDestaqueDTO(),
          $objSessaoJulgamentoDTO->getNumIdColegiado(), $objSessaoJulgamentoDTO, $objItemSessaoJulgamentoDTO->getNumOrdem(), $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento(), $qtd);
      $strDestaque .= '</div>';
    }
    if ($objItemSessaoJulgamentoDTO->isSetObjRevisaoItemDTO() && $objItemSessaoJulgamentoDTO->getObjRevisaoItemDTO()->getStrSinRevisado()=='S') {
      $strSinRevisado = 'S';
    }

    $strResultado = "<div class='tdSeq col-2 col-lg-1 align-self-stretch' data-destaques='$qtd' data-revisado='$strSinRevisado'>";
    $strResultado .= '<div style="font-size: 40px;height: 55px;">' . $objItemSessaoJulgamentoDTO->getNumOrdem() . '</div>';
    $strResultado .= $strDestaque;

    $strResultado .= '<div class="divEleicao">';
    $strResultado .= VotoEleicaoINT::montarIconeVotacao($objSessaoJulgamentoDTO, $objItemSessaoJulgamentoDTO, true);
    $strResultado .= '</div>';

    $strResultado .= '</div>' . "\n";
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param $objColegiadoComposicaoDTO
   * @param array $arrObjAutuacaoDTO
   * @param array $arrObjParteProcedimentoDTO
   * @param array $arrObjParticipanteDTO
   * @param array $arrObjProtocoloDTO
   * @return string
   */
  public static function montarColunaCentral(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, $objColegiadoComposicaoDTO, array $arrObjAutuacaoDTO, array $arrObjParteProcedimentoDTO, array $arrObjParticipanteDTO, array $arrObjProtocoloDTO): string
  {
    $dblIdProcedimento = $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
    $staItem = $objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco();
    $strResultado = '<div class="col-8 col-lg-10" >';
    $strResultado .= ResumoPautaINT::montarDivProcesso($objItemSessaoJulgamentoDTO); //processo
    $strResultado .= ResumoPautaINT::montarDivUsuarioSessao($staItem, $objColegiadoComposicaoDTO);//trazido por

    //descricao e tipo de mat�ria
    if (isset($arrObjAutuacaoDTO[$dblIdProcedimento])) {
      $strResultado .= ResumoPautaINT::montarDivAutuacao($arrObjAutuacaoDTO[$dblIdProcedimento]);
    }
    //partes ou interessados
    if (isset($arrObjParteProcedimentoDTO[$dblIdProcedimento])) {
      $strResultado .= ResumoPautaINT::montarDivPartes($arrObjParteProcedimentoDTO[$dblIdProcedimento]);
    } else if ($staItem!=TipoSessaoBlocoRN::$STA_REFERENDO && isset($arrObjParticipanteDTO[$dblIdProcedimento])) {
      $strResultado .= ResumoPautaINT::montarDivInteressados($arrObjParticipanteDTO[$dblIdProcedimento]);
    }
    //bloco documentos
    if (InfraArray::contar($arrObjItemSessaoDocumentoDTO)) {
      $strResultado .= ResumoPautaINT::montarDivDocumentos($arrObjProtocoloDTO, $objItemSessaoJulgamentoDTO);
    }
    $strResultado .= ResumoPautaINT::montarDivDispositivo($objItemSessaoJulgamentoDTO);
    $strResultado .= '</div>' . "\n";
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param $objColegiadoComposicaoDTO
   * @param array $arrObjAutuacaoDTO
   * @param array $arrObjParteProcedimentoDTO
   * @param array $arrObjParticipanteDTO
   * @param array $arrObjProtocoloDTO
   * @param bool $bolAdmin
   * @param array $arrSituacaoItem
   * @return string
   * @throws InfraException
   */
  public static function montarExibicaoItem(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, SessaoJulgamentoDTO $objSessaoJulgamentoDTO, $objColegiadoComposicaoDTO, array $arrObjAutuacaoDTO, array $arrObjParteProcedimentoDTO, array $arrObjParticipanteDTO, array $arrObjProtocoloDTO, bool $bolAdmin, array $arrSituacaoItem): string
  {
    $idItem = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
    //bloco de dados do item
    $strResultado = "<div id='item$idItem' class='trItem row mx-0 align-items-center' onclick='exibirDoc($idItem)'>\n";

    $strResultado .= ResumoPautaINT::montarColunaEsquerda($objItemSessaoJulgamentoDTO, $objSessaoJulgamentoDTO);
    $strResultado .= ResumoPautaINT::montarColunaCentral($objItemSessaoJulgamentoDTO, $objColegiadoComposicaoDTO, $arrObjAutuacaoDTO, $arrObjParteProcedimentoDTO, $arrObjParticipanteDTO, $arrObjProtocoloDTO);
    $strResultado .= ResumoPautaINT::montarColunaDireita($objItemSessaoJulgamentoDTO, $bolAdmin, $arrSituacaoItem);

    $strResultado .= '</div>';

    //---------------- area dos documentos
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
    if (InfraArray::contar($arrObjItemSessaoDocumentoDTO)==0) {
      $strResultado .= '<p id="doc' . $idItem . '" style="margin-left:4em;font-size:12px;display:none;">Documento n�o disponibilizado.</p>';
    } else {
      $strResultado .= ResumoPautaINT::montarDivConteudoDocumetos($objItemSessaoJulgamentoDTO, $arrObjProtocoloDTO);
    }
    $strResultado .= '<br />';
    return $strResultado;
  }
}
?>