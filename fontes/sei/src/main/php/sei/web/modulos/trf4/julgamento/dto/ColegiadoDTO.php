<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'colegiado';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdColegiado','id_colegiado');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Nome','nome');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Sigla','sigla');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Artigo','artigo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'QuorumMinimo','quorum_minimo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinAtivo','sin_ativo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdTipoProcedimento','id_tipo_procedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioSecretario','id_usuario_secretario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioPresidente','id_usuario_presidente');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidadeResponsavel','id_unidade_responsavel');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaAlgoritmoDistribuicao','sta_algoritmo_distribuicao');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeSecretario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaSecretario','sigla','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdContatoSecretario','id_contato','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomePresidente','u2.nome','usuario u2');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjColegiadoComposicaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjRelColegiadoUsuarioDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdColegiadoVersao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_DTH,'CriacaoVersao');

    $this->configurarPK('IdColegiado',InfraDTO::$TIPO_PK_NATIVA);
    $this->configurarFK('IdUsuarioSecretario','usuario','id_usuario');
    $this->configurarFK('IdUsuarioPresidente','usuario u2','u2.id_usuario');
    $this->configurarFK('IdUnidadeResponsavel','unidade','id_unidade');



    $this->configurarExclusaoLogica('SinAtivo', 'N');

  }
}
?>