<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PesquisaSessaoJulgamentoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return null;
  }

  public function montar() {

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM, 'IdSessaoJulgamento');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinComposicao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinPresenca');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinItemSessao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinAnotacao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinPedidoVista');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinDestaque');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinRevisao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinEleicoes');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinVotos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinDocumentos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'SinOrdenarItens');

  }
}
?>