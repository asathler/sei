<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);
  PaginaSEI::getInstance()->salvarCamposPost(array('rdoTipoDestaque'));

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_distribuicao','id_sessao_julgamento','id_procedimento_sessao','id_item_sessao_julgamento','id_colegiado','arvore','num_seq'));

  $objDestaqueDTO = new DestaqueDTO();

  $strDesabilitar = '';

  $arrComandos = array();
  $staTipoBloco=null;

  switch($_GET['acao']){
    case 'destaque_cadastrar':
      $strTitulo = 'Novo Destaque';

      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarDestaque" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $numIdItemSessaoJulgamento = $_GET['id_item_sessao_julgamento'];
      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrStaTipoItemSessaoBloco();

      /** @var ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO */
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);


      $staTipoBloco=$objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco();
      $objDestaqueDTO->setNumIdDestaque(null);

      $objDestaqueDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
      $objDestaqueDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objDestaqueDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objDestaqueDTO->setStrDescricao($_POST['txaDescricao']);
      $objDestaqueDTO->setStrStaTipo($_POST['hdnStaTipoDestaque']);

      if($_POST['hdnStaTipoDestaque']===DestaqueRN::$TD_COMENTARIO_RESTRITO){
        $objDestaqueDTO->setStrStaAcesso(DestaqueRN::$TA_RESTRITO);
      } else {
        $objDestaqueDTO->setStrStaAcesso(DestaqueRN::$TA_PUBLICO);
      }
      $objDestaqueDTO->setStrSinBloqueado('N');

      if (isset($_POST['sbmCadastrarDestaque'])) {
        try{
          $objDestaqueRN = new DestaqueRN();
          $objDestaqueDTO = $objDestaqueRN->cadastrar($objDestaqueDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Destaque "'.$objDestaqueDTO->getNumIdUnidade().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_destaque='.$objDestaqueDTO->getNumIdDestaque().PaginaSEI::getInstance()->montarAncora($objDestaqueDTO->getNumIdDestaque())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'destaque_alterar':
      $strTitulo = 'Alterar Destaque';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarDestaque" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $bolAlterar = true;

      if (isset($_GET['id_destaque'])){
        $objDestaqueDTO->setNumIdDestaque($_GET['id_destaque']);
        $objDestaqueDTO->retTodos();
        $objDestaqueDTO->retStrStaTipoItemSessaoBloco();
        $objDestaqueRN = new DestaqueRN();
        $objDestaqueDTO = $objDestaqueRN->consultar($objDestaqueDTO);
        if ($objDestaqueDTO===null){
          throw new InfraException('Registro n�o encontrado.');
        }
        $staTipoBloco=$objDestaqueDTO->getStrStaTipoItemSessaoBloco();
      } else {
        $objDestaqueDTO->setNumIdDestaque($_POST['hdnIdDestaque']);
        $objDestaqueDTO->setStrStaTipo($_POST['hdnStaTipoDestaque']);

        if($_POST['hdnStaTipoDestaque']===DestaqueRN::$TD_COMENTARIO_RESTRITO){
          $objDestaqueDTO->setStrStaAcesso(DestaqueRN::$TA_RESTRITO);
        } else {
          $objDestaqueDTO->setStrStaAcesso(DestaqueRN::$TA_PUBLICO);
        }
        $numIdItemSessaoJulgamento = $_GET['id_item_sessao_julgamento'];
        $objDestaqueDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
        $objDestaqueDTO->setStrDescricao($_POST['txaDescricao']);
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objDestaqueDTO->getNumIdDestaque())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
      if (isset($_POST['sbmAlterarDestaque'])) {
        try{
          $objDestaqueRN = new DestaqueRN();
          $objDestaqueRN->alterar($objDestaqueDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Destaque alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objDestaqueDTO->getNumIdDestaque())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'destaque_consultar':
      $strTitulo = 'Consultar Destaque';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_destaque'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objDestaqueDTO->setNumIdDestaque($_GET['id_destaque']);
      $objDestaqueDTO->setBolExclusaoLogica(false);
      $objDestaqueDTO->retTodos();
      $objDestaqueDTO->retStrStaTipoItemSessaoBloco();
      $objDestaqueRN = new DestaqueRN();
      $objDestaqueDTO = $objDestaqueRN->consultar($objDestaqueDTO);
      if ($objDestaqueDTO===null){
        throw new InfraException('Registro n�o encontrado.');
      }
      $staTipoBloco=$objDestaqueDTO->getStrStaTipoItemSessaoBloco();
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensSelTipoDestaque=DestaqueINT::montarSelectStaTipo('null','&nbsp;',$objDestaqueDTO->getStrStaTipo(),$staTipoBloco,$bolAlterar);

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
  #lblTipoDestaque {position:absolute;left:0%;top:0%;}
  #selTipoDestaque {position:absolute;left:0%;top:40%;}
#lblDescricao {position:absolute;left:0%;top:0;width:99%;}
#txaDescricao {position:absolute;left:0%;top:6%;width:99%;font-size:1.4em;}



<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
function inicializar(){
  $('#selTipoDestaque').ddslick({width: 300,
    onSelected: function(data){
      if(data.selectedIndex > 0) {
        document.getElementById('hdnStaTipoDestaque').value = data.selectedData.value;

        var scrollingElement = document.scrollingElement || document.documentElement;

        if (scrollingElement!=null) {
          scrollingElement.scrollTop = 0;
        }
        document.getElementById('txaDescricao').focus();
      }else{
        document.getElementById('hdnStaTipoDestaque').value = '';
      }
    }
  });
  document.getElementById('selTipoDestaque').focus();
}

function validarCadastro() {
  if (document.getElementById('hdnStaTipoDestaque').value == '') {
    alert('Selecione um Tipo de Destaque.');
    document.getElementById('selTipoDestaque').focus();
    return false;
  }
  return true;
}

function onSubmitForm() {
  return validarCadastro();
}

<?
  if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmDestaqueCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblTipoDestaque" for="selTipoDestaque" accesskey="" class="infraLabelObrigatorio">Tipo de Destaque:</label>
  <select id="selTipoDestaque" name="selTipoDestaque" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelTipoDestaque?>
  </select>
  <input type="hidden" id="hdnStaTipoDestaque" name="hdnStaTipoDestaque" value="<?=$objDestaqueDTO->getStrStaTipo()?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('40em');
?>

  <label id="lblDescricao" for="txaDescricao" accesskey="" class="infraLabelOpcional">Descri��o:</label>
  <textarea id="txaDescricao" name="txaDescricao" class="infraTextarea" rows="15" onkeypress="return infraMascaraTexto(this,event);" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" ><?=PaginaSEI::tratarHTML($objDestaqueDTO->getStrDescricao());?></textarea>


  <input type="hidden" id="hdnIdDestaque" name="hdnIdDestaque" value="<?=$objDestaqueDTO->getNumIdDestaque();?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>