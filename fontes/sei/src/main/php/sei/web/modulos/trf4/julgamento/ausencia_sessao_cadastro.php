<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no SVN: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);


  $objAusenciaSessaoDTO = new AusenciaSessaoDTO();
  $objAusenciaSessaoRN = new AusenciaSessaoRN();

  $strDesabilitar = '';

  $numIdUsuario = $_GET['id_usuario'];
  $numIdSessaoJulgamento = $_GET['id_sessao_julgamento'];
  $strParametros='&id_usuario='.$numIdUsuario.'&id_sessao_julgamento='.$numIdSessaoJulgamento;
  $arrComandos = array();

  switch($_GET['acao']){
    case 'ausencia_sessao_registrar':
      $strTitulo = 'Registrar Motivo de Aus�ncia';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarAusenciaSessao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';

      $objAusenciaSessaoDTO->setNumIdUsuario($numIdUsuario);
      $objAusenciaSessaoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);

      if (isset($_POST['hdnIdAusenciaSessao'])){
        $objAusenciaSessaoDTO->setNumIdAusenciaSessao($_POST['hdnIdAusenciaSessao']);
        $objAusenciaSessaoDTO->setNumIdMotivoAusencia($_POST['selMotivoAusencia']);
      } else {
        $objAusenciaSessaoDTO->retTodos();
        $objAusenciaSessaoDTOBanco=$objAusenciaSessaoRN->consultar($objAusenciaSessaoDTO);
        if ($objAusenciaSessaoDTOBanco){
          $objAusenciaSessaoDTO->setNumIdMotivoAusencia($objAusenciaSessaoDTOBanco->getNumIdMotivoAusencia());
          $objAusenciaSessaoDTO->setNumIdAusenciaSessao($objAusenciaSessaoDTOBanco->getNumIdAusenciaSessao());
        } else {
          $objAusenciaSessaoDTO->setNumIdMotivoAusencia(null);
          $objAusenciaSessaoDTO->setNumIdAusenciaSessao(null);
        }
      }

      if (isset($_POST['sbmCadastrarAusenciaSessao'])) {
        try{
          $ret = $objAusenciaSessaoRN->registrar($objAusenciaSessaoDTO);
          if($ret){
            PaginaSEI::getInstance()->adicionarMensagem('Motivo de Aus�ncia registrado com sucesso.');
            $objAusenciaSessaoDTO=$ret;
          } else {
            PaginaSEI::getInstance()->adicionarMensagem('Motivo de Aus�ncia exclu�do com sucesso.');
          }

//          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_ausencia_sessao='.$objAusenciaSessaoDTO->getNumIdAusenciaSessao().PaginaSEI::getInstance()->montarAncora($objAusenciaSessaoDTO->getNumIdAusenciaSessao())));
//          die;
          $bolExecutouOK=true;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'ausencia_sessao_consultar':
      $strTitulo = 'Consultar Aus�ncia';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_ausencia_sessao'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objAusenciaSessaoDTO->setNumIdAusenciaSessao($_GET['id_ausencia_sessao']);
      $objAusenciaSessaoDTO->setBolExclusaoLogica(false);
      $objAusenciaSessaoDTO->retTodos();
      $objAusenciaSessaoRN = new AusenciaSessaoRN();
      $objAusenciaSessaoDTO = $objAusenciaSessaoRN->consultar($objAusenciaSessaoDTO);
      if ($objAusenciaSessaoDTO===null){
        throw new InfraException('Registro n�o encontrado.');
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


  $objPresencaSessaoDTO=new PresencaSessaoDTO();
  $objPresencaSessaoRN=new PresencaSessaoRN();
  $objPresencaSessaoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
  $objPresencaSessaoDTO->setDthSaida($objPresencaSessaoDTO->getObjInfraAtributoDTO('InicioSessaoJulgamento'),InfraDTO::$OPER_MAIOR);
  $objPresencaSessaoDTO->setNumIdUsuario($numIdUsuario);
  $objPresencaSessaoDTO->retNumIdPresencaSessao();
  $bolObrigatorio=($objPresencaSessaoRN->contar($objPresencaSessaoDTO)==0);
  
  $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $objSessaoJulgamentoDTO->retNumIdColegiadoVersao();
  $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
  $objSessaoJulgamentoDTO=$objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);

  $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
  $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
  $objColegiadoComposicaoDTO->retStrNomeUsuario();
  $objColegiadoComposicaoDTO->retStrExpressaoCargo();
  $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($objSessaoJulgamentoDTO->getNumIdColegiadoVersao());
  $objColegiadoComposicaoDTO->setNumIdUsuario($numIdUsuario);
  $objColegiadoComposicaoDTO=$objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);

  if($bolObrigatorio){
    $strDefault='&nbsp;';
  } else {
    $strDefault='* N�o registrar motivo *';
  }
  $strItensSelMotivoAusencia = MotivoAusenciaINT::montarSelectDescricao('null',$strDefault,$objAusenciaSessaoDTO->getNumIdMotivoAusencia());

$strUsuario=$objColegiadoComposicaoDTO->getStrExpressaoCargo().' '.$objColegiadoComposicaoDTO->getStrNomeUsuario();

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
#lblUsuario {position:absolute;left:0;top:0;width:25%;}
#txtUsuario {position:absolute;left:0;top:40%;width:80%;}

#lblMotivoAusencia {position:absolute;left:0;top:0;width:70%;}
#selMotivoAusencia {position:absolute;left:0;top:40%;width:80%;}

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  <? if($bolExecutouOK){ ?>
  fechar_pagina('','about:_blank');
  <?}?>
  infraEfeitoTabelas();
}

function validarCadastro() {
  <?if($bolObrigatorio){?>
  if (!infraSelectSelecionado('selMotivoAusencia')) {
    alert('Selecione um Motivo de Aus�ncia.');
    document.getElementById('selMotivoAusencia').focus();
    return false;
  }
  <?}?>
  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmAusenciaSessaoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblUsuario" for="txtUsuario" accesskey="" class="infraLabelObrigatorio">Usu�rio:</label>
  <input type="text" id="txtUsuario" name="txtUsuario" class="infraText" value="<?=PaginaSEI::tratarHTML($strUsuario);?>" disabled="disabled" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
<?
PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblMotivoAusencia" for="selMotivoAusencia" accesskey="" class="<?=$bolObrigatorio?'infraLabelObrigatorio':'infraLabelOpcional';?>">Motivo de Aus�ncia:</label>
  <select id="selMotivoAusencia" name="selMotivoAusencia" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelMotivoAusencia?>
  </select>
<?
PaginaSEI::getInstance()->fecharAreaDados();
?>
  <input type="hidden" id="hdnIdAusenciaSessao" name="hdnIdAusenciaSessao" value="<?=$objAusenciaSessaoDTO->getNumIdAusenciaSessao();?>" />
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>