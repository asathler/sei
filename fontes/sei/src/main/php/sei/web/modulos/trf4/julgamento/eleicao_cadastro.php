<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento'));

  PaginaSEI::getInstance()->verificarSelecao('eleicao_selecionar');

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objEleicaoDTO = new EleicaoDTO();

  $arrComandos = array();

  $strStaSituacao = '';

  $bolAcaoLiberar = false;
  $strLinkLiberar = '';

  $bolAcaoConcluir = false;
  $strLinkConcluir = '';

  $bolAcaoFinalizar = false;
  $strLinkFinalizar = '';

  $bolAcaoAnular = false;
  $strLinkAnular = '';

  switch($_GET['acao']){

    case 'eleicao_liberar':
      try{
        $objEleicaoDTO = new EleicaoDTO();
        $objEleicaoDTO->setNumIdEleicao($_GET['id_eleicao']);

        $objEleicaoRN = new EleicaoRN();
        $objEleicaoRN->liberar($objEleicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_listar&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())));
        die;
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_alterar&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']));
      die;

    case 'eleicao_concluir':
      try{
        $objEleicaoDTO = new EleicaoDTO();
        $objEleicaoDTO->setNumIdEleicao($_GET['id_eleicao']);

        $objEleicaoRN = new EleicaoRN();
        $objEleicaoRN->concluir($objEleicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_listar&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())));
        die;
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_alterar&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']));
      die;

    case 'eleicao_finalizar':
      try{
        $objEleicaoDTO = new EleicaoDTO();
        $objEleicaoDTO->setNumIdEleicao($_GET['id_eleicao']);

        $objEleicaoRN = new EleicaoRN();
        $objEleicaoRN->finalizar($objEleicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_listar&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())));
        die;
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_alterar&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']));
      die;

    case 'eleicao_anular':
      try{
        $objEleicaoDTO = new EleicaoDTO();
        $objEleicaoDTO->setNumIdEleicao($_GET['id_eleicao']);

        $objEleicaoRN = new EleicaoRN();
        $objEleicaoRN->anular($objEleicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_listar&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())));
        die;
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_alterar&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']));
      die;

    case 'eleicao_cadastrar':
      $strTitulo = 'Novo Escrut�nio Eletr�nico';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarEleicao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objEleicaoDTO->setNumIdEleicao(null);
      $objEleicaoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      $objEleicaoDTO->setStrIdentificacao($_POST['txtIdentificacao']);
      $objEleicaoDTO->setStrDescricao($_POST['txaDescricao']);
      $objEleicaoDTO->setNumQuantidade($_POST['txtQuantidade']);
      $objEleicaoDTO->setNumOrdem($_POST['txtOrdem']);
      $objEleicaoDTO->setStrSinSecreta($_POST['selSinSecreta']);

      $arr = array_reverse(PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnOpcoes']));
      $numOrdem = 0;
      $arrObjOpcaoEleicaoDTO = array();
      foreach ($arr as $linha) {
        $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
        $objOpcaoEleicaoDTO->setNumIdOpcaoEleicao($linha[0]);
        $objOpcaoEleicaoDTO->setStrIdentificacao($linha[1]);
        $objOpcaoEleicaoDTO->setNumOrdem($numOrdem++);
        $arrObjOpcaoEleicaoDTO[]=$objOpcaoEleicaoDTO;
      }
      $strOpcoes = $_POST['hdnOpcoes'];
      $objEleicaoDTO->setArrObjOpcaoEleicaoDTO($arrObjOpcaoEleicaoDTO);

      if (isset($_POST['sbmCadastrarEleicao'])) {
        try{
          $objEleicaoRN = new EleicaoRN();
          $objEleicaoDTO = $objEleicaoRN->cadastrar($objEleicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Escrut�nio Eletr�nico "'.$objEleicaoDTO->getStrIdentificacao().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_eleicao='.$objEleicaoDTO->getNumIdEleicao().PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'eleicao_alterar':
      $strTitulo = 'Alterar Escrut�nio Eletr�nico';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarEleicao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';

      if (isset($_GET['id_eleicao'])){
        $objEleicaoDTO->setNumIdEleicao($_GET['id_eleicao']);
        $objEleicaoDTO->retTodos();
        $objEleicaoRN = new EleicaoRN();
        $objEleicaoDTO = $objEleicaoRN->consultar($objEleicaoDTO);
        if ($objEleicaoDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }

        $arrObjSituacaoEleicaoDTO = InfraArray::indexarArrInfraDTO($objEleicaoRN->listarValoresSituacao(),'StaSituacao');
        $strStaSituacao = $arrObjSituacaoEleicaoDTO[$objEleicaoDTO->getStrStaSituacao()]->getStrDescricao();

        $bolAcaoLiberar = SessaoSEI::getInstance()->verificarPermissao('eleicao_liberar') && $objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_CRIADA;
        if ($bolAcaoLiberar){
          $arrComandos[] = '<button type="button" id="btnLiberar" value="Liberar" onclick="acaoLiberar();" class="infraButton">Liberar</button>';
          $strLinkLiberar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_liberar&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']);
        }

        $bolAcaoConcluir = SessaoSEI::getInstance()->verificarPermissao('eleicao_concluir') && $objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_LIBERADA && $objEleicaoDTO->getStrSinSecreta()=='N';
        if ($bolAcaoConcluir){
          $arrComandos[] = '<button type="button" id="btnConcluir" value="Concluir" onclick="acaoConcluir();" class="infraButton">Concluir</button>';
          $strLinkConcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_concluir&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']);
        }

        $bolAcaoFinalizar = SessaoSEI::getInstance()->verificarPermissao('eleicao_finalizar') && ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_CONCLUIDA || ($objEleicaoDTO->getStrStaSituacao() == EleicaoRN::$SE_LIBERADA && $objEleicaoDTO->getStrSinSecreta()=='S'));
        if ($bolAcaoFinalizar){
          $arrComandos[] = '<button type="button" id="btnFinalizar" value="Finalizar" onclick="acaoFinalizar();" class="infraButton">Finalizar</button>';
          $strLinkFinalizar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_finalizar&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']);
        }

        $bolAcaoAnular = SessaoSEI::getInstance()->verificarPermissao('eleicao_anular') && $objEleicaoDTO->getStrStaSituacao() != EleicaoRN::$SE_CRIADA;
        if ($bolAcaoAnular){
          $arrComandos[] = '<button type="button" id="btnAnular" value="Anular" onclick="acaoAnular();" class="infraButton">Anular</button>';
          $strLinkAnular = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_anular&acao_origem='.$_GET['acao'].'&id_eleicao='.$_GET['id_eleicao']);
        }

        $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
        $objOpcaoEleicaoDTO->retNumIdOpcaoEleicao();
        $objOpcaoEleicaoDTO->retStrIdentificacao();
        $objOpcaoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
        $objOpcaoEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_DESC);

        $objOpcaoEleicaoRN = new OpcaoEleicaoRN();
        $arrObjOpcaoEleicaoDTO = $objOpcaoEleicaoRN->listar($objOpcaoEleicaoDTO);

        $arrOpcoes = array();
        foreach($arrObjOpcaoEleicaoDTO as $objOpcaoEleicaoDTO){
          $arrOpcoes[] = array($objOpcaoEleicaoDTO->getNumIdOpcaoEleicao(),$objOpcaoEleicaoDTO->getStrIdentificacao());
        }

        $strOpcoes = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrOpcoes);

      } else {
        $objEleicaoDTO->setNumIdEleicao($_POST['hdnIdEleicao']);
        $objEleicaoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
        $objEleicaoDTO->setStrIdentificacao($_POST['txtIdentificacao']);
        $objEleicaoDTO->setStrDescricao($_POST['txaDescricao']);
        $objEleicaoDTO->setNumQuantidade($_POST['txtQuantidade']);
        $objEleicaoDTO->setNumOrdem($_POST['txtOrdem']);
        $objEleicaoDTO->setStrSinSecreta($_POST['selSinSecreta']);

        $arr = array_reverse(PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnOpcoes']));
        $numOrdem = 0;
        $arrObjOpcaoEleicaoDTO = array();
        foreach ($arr as $linha) {
          $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
          $objOpcaoEleicaoDTO->setNumIdOpcaoEleicao($linha[0]);
          $objOpcaoEleicaoDTO->setStrIdentificacao($linha[1]);
          $objOpcaoEleicaoDTO->setNumOrdem($numOrdem++);
          $arrObjOpcaoEleicaoDTO[]=$objOpcaoEleicaoDTO;
        }

        $strOpcoes = $_POST['hdnOpcoes'];
      }

      $objEleicaoDTO->setArrObjOpcaoEleicaoDTO($arrObjOpcaoEleicaoDTO);

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarEleicao'])) {
        try{
          $objEleicaoRN = new EleicaoRN();
          $objEleicaoRN->alterar($objEleicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Escrut�nio Eletr�nico "'.$objEleicaoDTO->getStrIdentificacao().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objEleicaoDTO->getNumIdEleicao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'eleicao_consultar':
      $strTitulo = 'Consultar Escrut�nio Eletr�nico';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_eleicao'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objEleicaoDTO->setNumIdEleicao($_GET['id_eleicao']);
      $objEleicaoDTO->setBolExclusaoLogica(false);
      $objEleicaoDTO->retTodos();
      $objEleicaoRN = new EleicaoRN();
      $objEleicaoDTO = $objEleicaoRN->consultar($objEleicaoDTO);
      if ($objEleicaoDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }

      $arrObjSituacaoEleicaoDTO = InfraArray::indexarArrInfraDTO($objEleicaoRN->listarValoresSituacao(),'StaSituacao');
      $strStaSituacao = $arrObjSituacaoEleicaoDTO[$objEleicaoDTO->getStrStaSituacao()]->getStrDescricao();

      $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
      $objOpcaoEleicaoDTO->retNumIdOpcaoEleicao();
      $objOpcaoEleicaoDTO->retStrIdentificacao();
      $objOpcaoEleicaoDTO->setNumIdEleicao($objEleicaoDTO->getNumIdEleicao());
      $objOpcaoEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_DESC);

      $objOpcaoEleicaoRN = new OpcaoEleicaoRN();
      $arrObjOpcaoEleicaoDTO = $objOpcaoEleicaoRN->listar($objOpcaoEleicaoDTO);

      $arrOpcoes = array();
      foreach($arrObjOpcaoEleicaoDTO as $objOpcaoEleicaoDTO){
        $arrOpcoes[] = array($objOpcaoEleicaoDTO->getNumIdOpcaoEleicao(),$objOpcaoEleicaoDTO->getStrIdentificacao());
      }

      $strOpcoes = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrOpcoes);

      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensSelSinSecreta = InfraINT::montarSelectArray('null','&nbsp;', $objEleicaoDTO->getStrSinSecreta(), array('S' => 'Sim', 'N' => 'N�o'));


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>

#lblIdentificacao {position:absolute;left:0%;top:0%;width:75%;}
#txtIdentificacao {position:absolute;left:0%;top:40%;width:75%;}

#lblSinSecreta {position:absolute;left:77%;top:0%;width:13%;}
#selSinSecreta {position:absolute;left:77%;top:40%;width:13%;}
#ancAjudaSecreta {position:absolute;left:91%;top:45%;}

#lblStaSituacao {position:absolute;left:0%;top:0%;}
#txtStaSituacao {position:absolute;left:0%;top:40%;width:25%;}

#lblDescricao {position:absolute;left:0%;top:0%;width:95%;}
#txaDescricao {position:absolute;left:0%;top:18%;width:95%;}

#lblQuantidade {position:absolute;left:0%;top:0%;}
#txtQuantidade {position:absolute;left:0%;top:40%;width:15%;}
#ancAjudaQuantidade {position:absolute;left:16%;top:45%;}

#lblOrdem {position:absolute;left:25%;top:0%;}
#txtOrdem {position:absolute;left:25%;top:40%;width:15%;}
#ancAjudaOrdem {position:absolute;left:41%;top:45%;}

#lblIdentificacaoOpcao {position:absolute;left:0%;top:0%;}
#txtIdentificacaoOpcao {position:absolute;left:0%;top:40%;width:80%;}
#btnAdicionarOpcao {position:absolute;left:82%;top:40%;}

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='eleicao_cadastrar'){
    document.getElementById('txtIdentificacao').focus();
  } else if ('<?=$_GET['acao']?>'=='eleicao_consultar'){
    infraDesabilitarCamposAreaDados();
    document.getElementById('imgAjudaSecreta').style.visibility='visible';
    document.getElementById('imgAjudaQuantidade').style.visibility='visible';
    document.getElementById('imgAjudaOrdem').style.visibility='visible';
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas(true);

  objTabelaOpcoes = new infraTabelaDinamica('tblOpcoes','hdnOpcoes', <?=$_GET['acao']=='eleicao_consultar'?'false,false,false':'true,true,true';?>);
  objTabelaOpcoes.inserirNoInicio = false;
  objTabelaOpcoes.gerarEfeitoTabela=true;
  objTabelaOpcoes.alterar= function (arr) {
    document.getElementById('hdnIdOpcao').value = arr[0];
    document.getElementById('txtIdentificacaoOpcao').value = arr[1].infraReplaceAll("<br />","\n");
  };
}

function validarCadastro() {

  if (infraTrim(document.getElementById('txtIdentificacao').value)=='') {
    alert('Informe a Identifica��o.');
    document.getElementById('txtIdentificacao').focus();
    return false;
  }

  if (!infraSelectSelecionado('selSinSecreta')) {
    alert('Selecione uma op��o para Secreto.');
    document.getElementById('selSinSecreta').focus();
    return false;
  }

  if (infraTrim(document.getElementById('txtQuantidade').value)=='') {
    alert('Informe o N� de Escolhas.');
    document.getElementById('txtQuantidade').focus();
    return false;
  }

  if (infraTrim(document.getElementById('txtOrdem').value)=='') {
    alert('Informe a Ordem.');
    document.getElementById('txtOrdem').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

  function adicionarOpcao(){

    var txa=document.getElementById('txtIdentificacaoOpcao');
    var hdn=document.getElementById('hdnIdOpcao');

    var id = ((hdn.value!=='') ? hdn.value : 'N' + (new Date()).getTime());
    var descricao = txa.value;
    if (infraTrim(descricao)=='') {
      alert('Informe a descri��o da op��o.');
      txa.focus();
      return false;
    }
    descricao = descricao.infraReplaceAll("\n","<br />")
    objTabelaOpcoes.adicionar([id, descricao]);
    //depois de incluir limpa os input
    txa.value = '';
    hdn.value = '';
    txa.focus();
  }

<? if ($bolAcaoLiberar){ ?>
  function acaoLiberar(){
    var msg = '';

    <?if ($objEleicaoDTO->getStrSinSecreta()=='N'){?>
    msg += 'ATEN��O: Escrut�nio sinalizado como N�O secreto ent�o o conte�do dos votos ser� identificado na finaliza��o.\n\n';
    <?}?>

    msg += 'Confirma libera��o do Escrut�nio Eletr�nico para lan�amento de votos?';


    if (confirm(msg)){
      document.getElementById('frmEleicaoCadastro').action='<?=$strLinkLiberar?>';
      document.getElementById('frmEleicaoCadastro').submit();
    }
  }
<? } ?>

<? if ($bolAcaoConcluir){ ?>
function acaoConcluir(){
  if (confirm("Confirma conclus�o do Escrut�nio Eletr�nico?")){
    document.getElementById('frmEleicaoCadastro').action='<?=$strLinkConcluir?>';
    document.getElementById('frmEleicaoCadastro').submit();
  }
}
<? } ?>

<? if ($bolAcaoFinalizar){ ?>
function acaoFinalizar(){
  if (confirm("Confirma finaliza��o do Escrut�nio Eletr�nico?")){
    document.getElementById('frmEleicaoCadastro').action='<?=$strLinkFinalizar?>';
    document.getElementById('frmEleicaoCadastro').submit();
  }
}
<? } ?>

<? if ($bolAcaoAnular){ ?>
function acaoAnular(){
  if (confirm("Confirma anula��o do Escrut�nio Eletr�nico?")){
    document.getElementById('frmEleicaoCadastro').action='<?=$strLinkAnular?>';
    document.getElementById('frmEleicaoCadastro').submit();
  }
}
<? } ?>


<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmEleicaoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('4.5em');
?>
  <label id="lblIdentificacao" for="txtIdentificacao" accesskey="" class="infraLabelObrigatorio">Identifica��o:</label>
  <input type="text" id="txtIdentificacao" name="txtIdentificacao" class="infraText" value="<?=PaginaSEI::tratarHTML($objEleicaoDTO->getStrIdentificacao());?>" onkeypress="return infraMascaraTexto(this,event,250);" maxlength="250" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblSinSecreta" for="selSinSecreta" accesskey="" class="infraLabelObrigatorio">Secreto:</label>
  <select id="selSinSecreta" name="selSinSecreta" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelSinSecreta?>
  </select>
  <a href="javascript:void(0);" id="ancAjudaSecreta" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" onmouseover="return infraTooltipMostrar('<?=PaginaSEI::tratarHTML(PaginaSEI::formatarParametrosJavaScript('Indica se o conte�do dos votos ser� identificado ao finalizar a vota��o.'))?>','',200);" onmouseout="return infraTooltipOcultar();"><img id="imgAjudaSecreta" src="<?=PaginaSEI::getInstance()->getIconeAjuda()?>" class="infraImg"/></a>
<?
PaginaSEI::getInstance()->fecharAreaDados();
if ($strStaSituacao!='') {
  PaginaSEI::getInstance()->abrirAreaDados('4.5em');
  ?>
  <label id="lblStaSituacao" for="txtStaSituacao" accesskey="" class="infraLabelObrigatorio">Situa��o:</label>
  <input type="text" id="txtStaSituacao" name="txtStaSituacao" readonly="readonly" class="infraText infraReadOnly"
         value="<?= $strStaSituacao ?>"/>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
}
PaginaSEI::getInstance()->abrirAreaDados('11em');
?>
  <label id="lblDescricao" for="txaDescricao" accesskey="" class="infraLabelOpcional">Descri��o:</label>
  <textarea id="txaDescricao" name="txaDescricao" rows="4" class="infraTextarea" maxlength="4000" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?= PaginaSEI::tratarHTML($objEleicaoDTO->getStrDescricao()) ?></textarea>
<?
PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('4.5em');
?>
  <label id="lblQuantidade" for="txtQuantidade" accesskey="" class="infraLabelObrigatorio">N� de Escolhas:</label>
  <input type="text" id="txtQuantidade" name="txtQuantidade" onkeypress="return infraMascaraNumero(this, event)" class="infraText" value="<?=PaginaSEI::tratarHTML($objEleicaoDTO->getNumQuantidade());?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  <a href="javascript:void(0);" id="ancAjudaQuantidade" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" onmouseover="return infraTooltipMostrar('<?=PaginaSEI::tratarHTML(PaginaSEI::formatarParametrosJavaScript('Quantidade m�nima de votos que cada membro dever� escolher para concluir a vota��o.'))?>','',200);" onmouseout="return infraTooltipOcultar();"><img id="imgAjudaQuantidade" src="<?=PaginaSEI::getInstance()->getIconeAjuda()?>" class="infraImg"/></a>

  <label id="lblOrdem" for="txtOrdem" accesskey="" class="infraLabelObrigatorio">Ordem:</label>
  <input type="text" id="txtOrdem" name="txtOrdem" onkeypress="return infraMascaraNumero(this, event)" class="infraText" value="<?=PaginaSEI::tratarHTML($objEleicaoDTO->getNumOrdem());?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  <a href="javascript:void(0);" id="ancAjudaOrdem" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" onmouseover="return infraTooltipMostrar('<?=PaginaSEI::tratarHTML(PaginaSEI::formatarParametrosJavaScript('Permite alterar a posi��o do Escrut�nio Eletr�nico na lista da tela de vota��o.'))?>','',200);" onmouseout="return infraTooltipOcultar();"><img id="imgAjudaOrdem" src="<?=PaginaSEI::getInstance()->getIconeAjuda()?>" class="infraImg"/></a>
<?
PaginaSEI::getInstance()->fecharAreaDados();
if ($_GET['acao']!='eleicao_consultar') {
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblIdentificacaoOpcao" for="txtIdentificacaoOpcao" accesskey="" class="infraLabelOpcional">Op��o:</label>
  <input type="text" id="txtIdentificacaoOpcao" name="txtIdentificacaoOpcao" class="infraText" maxlength="4000" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>" />

  <input type="button" id="btnAdicionarOpcao" onclick="adicionarOpcao();" name="btnAdicionarOpcao" value="Adicionar"
         class="infraButton" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
}
?>
  <div id="divTabelaOpcoes" class="infraAreaTabela">
    <table id="tblOpcoes" class="infraTable" style="width:99%">
      <caption class="infraCaption"><?=PaginaSEI::getInstance()->gerarCaptionTabela('Op��es',0)?></caption>
      <tr>
        <th style="display:none;">ID</th>
        <th class="infraTh" style="width:82%;">Identifica��o</th>
        <? if($_GET['acao']!='eleicao_consultar') {?>
          <th class="infraTh">A��es</th>
        <? }?>
      </tr>
    </table>
    <input type="hidden" id="hdnIdOpcao" name="hdnIdOpcao" value=""/>
    <input type="hidden" id="hdnOpcoes" name="hdnOpcoes" value="<?=$strOpcoes?>" />
  </div>
  <input type="hidden" id="hdnIdEleicao" name="hdnIdEleicao" value="<?=$objEleicaoDTO->getNumIdEleicao();?>" />
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
