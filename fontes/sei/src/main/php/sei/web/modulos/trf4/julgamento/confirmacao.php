<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  ////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);

  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_distribuicao','id_sessao_julgamento','id_procedimento_sessao','id_item_sessao_julgamento','id_distribuicao_sessao','id_colegiado','arvore'));

  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '';

  $mensagem='';
  $arrComandos = array();


  switch($_GET['acao']){
    case 'item_sessao_julgamento_diligenciar':
      $arrComandos[] = '<button type="submit" name="sbmDiligenciarItemSessaoJulgamento" value="Salvar" class="infraButton">Confirmar</button>';
      $strTitulo = 'Converter Julgamento em Dilig�ncia';

      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
      $strTitulo.= ' '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo();

      if (isset($_POST['sbmDiligenciarItemSessaoJulgamento'])){
        try{
          $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
          $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
          $objItemSessaoJulgamentoRN->converterDiligencia($objItemSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Processo convertido em dilig�ncia com sucesso.');
          $bolExecucaoOK=true;
          $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros);
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }

      break;
    case 'item_sessao_julgamento_diligencia_cancelar':
      $arrComandos[] = '<button type="submit" name="sbmDiligenciaCancelar" value="Salvar" class="infraButton">Confirmar</button>';
      $strTitulo = 'Cancelar convers�o do Julgamento em Dilig�ncia';

      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
      $strTitulo.= ' - '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo();

      if (isset($_POST['sbmDiligenciaCancelar'])){
        try{
          $objItemSessaoJulgamentoRN->cancelarDiligencia($objItemSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Dilig�ncia cancelada com sucesso.');
          $bolExecucaoOK=true;
          $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros);
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }

      break;
    case 'item_sessao_julgamento_adiamento_cancelar':
      $arrComandos[] = '<button type="submit" name="sbmAdiamentoCancelar" value="Salvar" class="infraButton">Confirmar</button>';
      $strTitulo = 'Cancelar adiamento do processo';

      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
      $strTitulo.= ' - '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo();

      if (isset($_POST['sbmAdiamentoCancelar'])) {
        try {
          $objItemSessaoJulgamentoRN->cancelarAdiamento($objItemSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Adiamento cancelado com sucesso.');
          $bolExecucaoOK = true;
          $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . PaginaSEI::getInstance()->getAcaoRetorno() . '&acao_origem=' . $_GET['acao'] . $strParametros);
        } catch (Exception $e) {
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  //
  if (isset($_GET['id_distribuicao'])){
    $strLinkRetorno=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_pautar&acao_origem='.$_GET['acao'].$strParametros);
    $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.$strLinkRetorno.'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
  } else {
    $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="fechar();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
  #divInfraAreaTela {display:none;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
?>
//<script>
  var objLupaProcedimentos = null;
  function fechar(){
    fechar_pagina('','about:blank');
  }
function inicializar(){
<?if($bolExecucaoOK){ ?>
  fechar_pagina('<?=PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento'])?>','<?=$strLinkRetorno?>');
  return;
<?}?>

  document.getElementById('divInfraAreaTela').style.display = 'block';

}

function onSubmitForm() {
  return true;
}

function finalizar() {

}
//</script>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();" onunload="finalizar();"');
?>
<form id="frmItemSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('10em');

PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>