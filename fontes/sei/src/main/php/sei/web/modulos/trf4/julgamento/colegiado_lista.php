<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('colegiado_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  switch($_GET['acao']){
    case 'colegiado_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjColegiadoDTO = array();
        for ($i=0;$i<InfraArray::contar($arrStrIds);$i++){
          $objColegiadoDTO = new ColegiadoDTO();
          $objColegiadoDTO->setNumIdColegiado($arrStrIds[$i]);
          $arrObjColegiadoDTO[] = $objColegiadoDTO;
        }
        $objColegiadoRN = new ColegiadoRN();
        $objColegiadoRN->excluir($arrObjColegiadoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;


    case 'colegiado_desativar':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjColegiadoDTO = array();
        foreach ($arrStrIds as $strId) {
          $objColegiadoDTO = new ColegiadoDTO();
          $objColegiadoDTO->setNumIdColegiado($strId);
          $arrObjColegiadoDTO[] = $objColegiadoDTO;
        }
        $objColegiadoRN = new ColegiadoRN();
        $objColegiadoRN->desativar($arrObjColegiadoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

    case 'colegiado_reativar':
      $strTitulo = 'Reativar Colegiados';
      if ($_GET['acao_confirmada']=='sim'){
        try{
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjColegiadoDTO = array();
          foreach ($arrStrIds as $strId) {
            $objColegiadoDTO = new ColegiadoDTO();
            $objColegiadoDTO->setNumIdColegiado($strId);
            $arrObjColegiadoDTO[] = $objColegiadoDTO;
          }
          $objColegiadoRN = new ColegiadoRN();
          $objColegiadoRN->reativar($arrObjColegiadoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
        die;
      }
      break;


    case 'colegiado_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Colegiado','Selecionar Colegiados');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='colegiado_cadastrar'){
        if (isset($_GET['id_colegiado'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_colegiado']);
        }
      }
      break;

    case 'colegiado_listar':
      $strTitulo = 'Colegiados';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'colegiado_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  if ($_GET['acao'] == 'colegiado_listar' || $_GET['acao'] == 'colegiado_selecionar'){
    $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('colegiado_cadastrar');
    if ($bolAcaoCadastrar){
      $arrComandos[] = '<button type="button" accesskey="N" id="btnNovo" value="Novo" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ovo</button>';
    }
  }

  $objColegiadoDTO = new ColegiadoDTO();
  $objColegiadoDTO->retNumIdColegiado();
  $objColegiadoDTO->retStrNome();
  $objColegiadoDTO->retStrArtigo();
  $objColegiadoDTO->retStrSigla();
  $objColegiadoDTO->retStrSiglaUnidade();


  //if ($_GET['acao'] == 'colegiado_reativar'){
    //Lista somente inativos
  //  $objColegiadoDTO->setBolExclusaoLogica(false);
  //  $objColegiadoDTO->setStrSinAtivo('N');
  //}

  $objColegiadoDTO->setBolExclusaoLogica(false);
  $objColegiadoDTO->retStrSinAtivo();

  PaginaSEI::getInstance()->prepararOrdenacao($objColegiadoDTO, 'Sigla', InfraDTO::$TIPO_ORDENACAO_ASC);
  //PaginaSEI::getInstance()->prepararPaginacao($objColegiadoDTO);

  $objColegiadoRN = new ColegiadoRN();
  $arrObjColegiadoDTO = $objColegiadoRN->listar($objColegiadoDTO);

  //PaginaSEI::getInstance()->processarPaginacao($objColegiadoDTO);
  $numRegistros = InfraArray::contar($arrObjColegiadoDTO);

  if ($numRegistros > 0){

    $bolCheck = false;

    if ($_GET['acao']=='colegiado_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = false;
      $bolAcaoAlterar = false;
      $bolAcaoImprimir = false;
      //$bolAcaoGerarPlanilha = false;
      $bolAcaoExcluir = false;
      $bolAcaoDesativar = false;
      $bolCheck = true;
    /*
    }else if ($_GET['acao']=='colegiado_reativar'){
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('colegiado_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('colegiado_consultar');
      $bolAcaoAlterar = false;
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('colegiado_excluir');
      $bolAcaoDesativar = false;
    */
    }else{
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('colegiado_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('colegiado_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('colegiado_alterar');
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('colegiado_excluir');
      $bolAcaoDesativar = SessaoSEI::getInstance()->verificarPermissao('colegiado_desativar');
    }


    if ($bolAcaoDesativar){
      //$bolCheck = true;
      //$arrComandos[] = '<button type="button" accesskey="t" id="btnDesativar" value="Desativar" onclick="acaoDesativacaoMultipla();" class="infraButton">Desa<span class="infraTeclaAtalho">t</span>ivar</button>';
      $strLinkDesativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_desativar&acao_origem='.$_GET['acao']);
    }

    if ($bolAcaoReativar){
      //$bolCheck = true;
      //$arrComandos[] = '<button type="button" accesskey="R" id="btnReativar" value="Reativar" onclick="acaoReativacaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">R</span>eativar</button>';
      $strLinkReativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_reativar&acao_origem='.$_GET['acao'].'&acao_confirmada=sim');
    }

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_excluir&acao_origem='.$_GET['acao']);
    }

    $strResultado = '';

    //if ($_GET['acao']!='colegiado_reativar'){
      $strSumarioTabela = 'Tabela de Colegiados.';
      $strCaptionTabela = 'Colegiados';
    //}else{
    //  $strSumarioTabela = 'Tabela de Colegiados Inativos.';
    //  $strCaptionTabela = 'Colegiados Inativos';
    //}

    $strResultado .= '<table width="90%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoDTO,'Sigla','Sigla',$arrObjColegiadoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoDTO,'Artigo','Artigo',$arrObjColegiadoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoDTO,'Nome','Nome',$arrObjColegiadoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoDTO,'Unidade Respons�vel','UnidadeResponsavel',$arrObjColegiadoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="20%">A��es</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      if ($arrObjColegiadoDTO[$i]->getStrSinAtivo()=='S'){
        $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      }else{
        $strCssTr = '<tr class="trVermelha">';
      }

      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjColegiadoDTO[$i]->getNumIdColegiado(),$arrObjColegiadoDTO[$i]->getStrNome()).'</td>';
      }
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjColegiadoDTO[$i]->getStrSigla()).'</td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML($arrObjColegiadoDTO[$i]->getStrArtigo()).'</td>';
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjColegiadoDTO[$i]->getStrNome()).'</td>';
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjColegiadoDTO[$i]->getStrSiglaUnidade()).'</td>';
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjColegiadoDTO[$i]->getNumIdColegiado());

      if ($bolAcaoAlterar && $arrObjColegiadoDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_colegiado='.$arrObjColegiadoDTO[$i]->getNumIdColegiado()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Colegiado" alt="Alterar Colegiado" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoConsultar){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_consultar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_colegiado='.$arrObjColegiadoDTO[$i]->getNumIdColegiado()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeConsultar().'" title="Consultar Colegiado" alt="Consultar Colegiado" class="infraImg" /></a>&nbsp;';
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_versao_listar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_colegiado='.$arrObjColegiadoDTO[$i]->getNumIdColegiado()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.MdJulgarIcone::COLEGIADO_VERSOES.'" title="Consultar Composi��es do Colegiado" alt="Consultar Composi��es do Colegiado" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoDesativar || $bolAcaoReativar || $bolAcaoExcluir){
        $strId = $arrObjColegiadoDTO[$i]->getNumIdColegiado();
        $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjColegiadoDTO[$i]->getStrNome());
      }

      if ($bolAcaoDesativar && $arrObjColegiadoDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoDesativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeDesativar().'" title="Desativar Colegiado" alt="Desativar Colegiado" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoReativar && $arrObjColegiadoDTO[$i]->getStrSinAtivo()=='N'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoReativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeReativar().'" title="Reativar Colegiado" alt="Reativar Colegiado" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoExcluir){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoExcluir(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Colegiado" alt="Excluir Colegiado" class="infraImg" /></a>&nbsp;';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'colegiado_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='colegiado_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('btnFechar').focus();
  }
  infraEfeitoTabelas();
}

<? if ($bolAcaoDesativar){ ?>
function acaoDesativar(id,desc){
  if (confirm("Confirma desativa��o do Colegiado \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmColegiadoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmColegiadoLista').submit();
  }
}

function acaoDesativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Colegiado selecionado.');
    return;
  }
  if (confirm("Confirma desativa��o dos Colegiados selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmColegiadoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmColegiadoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoReativar){ ?>
function acaoReativar(id,desc){
  if (confirm("Confirma reativa��o do Colegiado \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmColegiadoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmColegiadoLista').submit();
  }
}

function acaoReativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Colegiado selecionado.');
    return;
  }
  if (confirm("Confirma reativa��o dos Colegiados selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmColegiadoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmColegiadoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclus�o do Colegiado \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmColegiadoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmColegiadoLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Colegiado selecionado.');
    return;
  }
  if (confirm("Confirma exclus�o dos Colegiados selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmColegiadoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmColegiadoLista').submit();
  }
}
<? } ?>

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmColegiadoLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  //PaginaSEI::getInstance()->abrirAreaDados('5em');
  //PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>