<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class TipoMateriaRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrDescricao(TipoMateriaDTO $objTipoMateriaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoMateriaDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('Descri��o n�o informada.');
    }else{
      $objTipoMateriaDTO->setStrDescricao(trim($objTipoMateriaDTO->getStrDescricao()));

      if (strlen($objTipoMateriaDTO->getStrDescricao())>250){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 250 caracteres.');
      }

      $dto = new TipoMateriaDTO();
      $dto->setBolExclusaoLogica(false);
      $dto->retStrSinAtivo();
      $dto->setNumIdTipoMateria($objTipoMateriaDTO->getNumIdTipoMateria(),InfraDTO::$OPER_DIFERENTE);
      $dto->setStrDescricao($objTipoMateriaDTO->getStrDescricao());
      $dto = $this->consultar($dto);

      if ($dto!=null) {
        if ($dto->getStrSinAtivo()=='S') {
          $objInfraException->adicionarValidacao('Existe outra ocorr�ncia com esta Descri��o.');
        } else {
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa com esta Descri��o.');
        }
      }

    }
  }

  private function validarStrSinAtivo(TipoMateriaDTO $objTipoMateriaDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoMateriaDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objTipoMateriaDTO->getStrSinAtivo())){
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }

  protected function cadastrarControlado(TipoMateriaDTO $objTipoMateriaDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_cadastrar',__METHOD__,$objTipoMateriaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objTipoMateriaDTO, $objInfraException);
      $this->validarStrSinAtivo($objTipoMateriaDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objTipoMateriaBD->cadastrar($objTipoMateriaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Tipo de Mat�ria.',$e);
    }
  }

  protected function alterarControlado(TipoMateriaDTO $objTipoMateriaDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_alterar',__METHOD__,$objTipoMateriaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objTipoMateriaDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objTipoMateriaDTO, $objInfraException);
      }
      if ($objTipoMateriaDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objTipoMateriaDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      $objTipoMateriaBD->alterar($objTipoMateriaDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Tipo de Mat�ria.',$e);
    }
  }

  protected function excluirControlado($arrObjTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_excluir',__METHOD__,$arrObjTipoMateriaDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();
      $arrId=InfraArray::converterArrInfraDTO($arrObjTipoMateriaDTO,'IdTipoMateria');
      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());

      if(count($arrId)>0){
        $objTipoMateriaDTO=new TipoMateriaDTO();
        $objTipoMateriaDTO->setNumIdTipoMateria($arrId,InfraDTO::$OPER_IN);
        $objTipoMateriaDTO->retNumIdTipoMateria();
        $objTipoMateriaDTO->setBolExclusaoLogica(false);
        $arrObjTipoMateriaDTO=$this->listar($objTipoMateriaDTO);
      }

      $arrId=InfraArray::converterArrInfraDTO($arrObjTipoMateriaDTO,'IdTipoMateria');
      if(count($arrId)>0){
        $objAutuacaoRN=new AutuacaoRN();
        $objAutuacaoDTO=new AutuacaoDTO();
        $objAutuacaoDTO->setNumIdTipoMateria($arrId,InfraDTO::$OPER_IN);
        if($objAutuacaoRN->contar($objAutuacaoDTO)>0){
          $objInfraException->lancarValidacao('Tipo de Mat�ria j� foi utilizado em autua��es.');
        }
      }

      for($i=0, $iMax = InfraArray::contar($arrObjTipoMateriaDTO); $i< $iMax; $i++){
        $objTipoMateriaBD->excluir($arrObjTipoMateriaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Tipo de Mat�ria.',$e);
    }
  }

  protected function consultarConectado(TipoMateriaDTO $objTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_consultar',__METHOD__,$objTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objTipoMateriaBD->consultar($objTipoMateriaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Tipo de Mat�ria.',$e);
    }
  }

  protected function listarConectado(TipoMateriaDTO $objTipoMateriaDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_listar',__METHOD__,$objTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objTipoMateriaBD->listar($objTipoMateriaDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Tipos de Mat�ria.',$e);
    }
  }

  protected function contarConectado(TipoMateriaDTO $objTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_listar',__METHOD__,$objTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objTipoMateriaBD->contar($objTipoMateriaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Tipos de Mat�ria.',$e);
    }
  }

  protected function desativarControlado($arrObjTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_desativar',__METHOD__,$arrObjTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjTipoMateriaDTO); $i< $iMax; $i++){
        $objTipoMateriaBD->desativar($arrObjTipoMateriaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Tipo de Mat�ria.',$e);
    }
  }

  protected function reativarControlado($arrObjTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_reativar',__METHOD__,$arrObjTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjTipoMateriaDTO); $i< $iMax; $i++){
        $objTipoMateriaBD->reativar($arrObjTipoMateriaDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Tipo de Mat�ria.',$e);
    }
  }

  protected function bloquearControlado(TipoMateriaDTO $objTipoMateriaDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_materia_consultar',__METHOD__,$objTipoMateriaDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoMateriaBD = new TipoMateriaBD($this->getObjInfraIBanco());
      $ret = $objTipoMateriaBD->bloquear($objTipoMateriaDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Tipo de Mat�ria.',$e);
    }
  }


}
?>