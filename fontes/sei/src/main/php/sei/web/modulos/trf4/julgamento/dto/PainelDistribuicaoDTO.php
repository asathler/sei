<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PainelDistribuicaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'distribuicao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdDistribuicao','id_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL,'IdProcedimento','id_procedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL,'IdDocumentoDistribuicao','id_documento_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdColegiadoVersao','id_colegiado_versao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioRelator','id_usuario_relator');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidadeRelator','id_unidade_relator');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioRelatorAcordao','id_usuario_relator_acordao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidadeRelatorAcordao','id_unidade_relator_acordao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaDistribuicao','sta_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaDistribuicaoAnterior','sta_distribuicao_anterior');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidade','id_unidade');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinPrevencao','sin_prevencao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaUltimo','sta_ultimo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdDistribuicaoAgrupador','id_distribuicao_agrupador');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdMotivoPrevencao','id_motivo_prevencao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdMotivoCancelamento','id_motivo_canc_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Distribuicao','dth_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'SituacaoAtual','dth_situacao_atual');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuarioRelator','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidadeRelator','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidadeRelator','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoColegiadoVersao','id_colegiado','colegiado_versao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeColegiado','nome','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaColegiado','sigla','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUnidadeResponsavelColegiado','id_unidade_responsavel','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'MotivoPrevencao','descricao','motivo_distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaAlgoritmoDistribuicao','sta_algoritmo_distribuicao','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'ProtocoloFormatado','protocolo_formatado','protocolo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaNivelAcessoGlobal','sta_nivel_acesso_global','protocolo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdTipoProcedimento','id_tipo_procedimento','procedimento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeTipoProcedimento','nome','tipo_procedimento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoMotivoCancelamento','descricao','motivo_canc_distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdContatoParticipante','id_contato','participante');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaParticipacaoParticipante','sta_participacao','participante');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdTipoMateria','id_tipo_materia','autuacao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoAutuacao','descricao','autuacao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'IdxAutuacao','idx_autuacao','autuacao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjImpedimentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Senha');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'Quantidade');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_OBJ,'ProcedimentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_DTH,'SessaoJulgamento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinProntoPauta');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinInconsistencia');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinSimularDistribuicao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdColegiadoComposicaoDistribuido');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ResultadoSimulacao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_OBJ,'PedidoVistaDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjParteProcedimentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'PalavrasPesquisa');

    $this->configurarPK('IdDistribuicao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdUsuarioRelator', 'usuario', 'id_usuario');
    $this->configurarFK('IdUnidadeRelator', 'unidade', 'id_unidade');
    $this->configurarFK('IdProcedimento', 'protocolo', 'id_protocolo');
    $this->configurarFK('IdProcedimento', 'autuacao', 'id_procedimento', InfraDTO::$TIPO_FK_OPCIONAL,InfraDTO::$FILTRO_FK_WHERE);
    $this->configurarFK('IdProcedimento', 'procedimento', 'id_procedimento');
    $this->configurarFK('IdColegiadoVersao','colegiado_versao','id_colegiado_versao');
    $this->configurarFK('IdColegiadoColegiadoVersao','colegiado','id_colegiado');
    $this->configurarFK('IdMotivoPrevencao','motivo_distribuicao','id_motivo_distribuicao', InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdTipoProcedimento', 'tipo_procedimento', 'id_tipo_procedimento');
    $this->configurarFK('IdMotivoCancelamento', 'motivo_canc_distribuicao', 'id_motivo_canc_distribuicao', InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdProcedimento', 'participante', 'id_protocolo');
  }
}
?>