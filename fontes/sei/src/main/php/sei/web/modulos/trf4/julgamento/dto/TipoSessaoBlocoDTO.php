<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 03/07/2021 - criado por alv77
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class TipoSessaoBlocoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'tipo_sessao_bloco';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdTipoSessaoBloco', 'id_tipo_sessao_bloco');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Descricao', 'descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Ordem', 'ordem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'SinAgruparMembro', 'sin_agrupar_membro');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'StaTipoItem', 'sta_tipo_item');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdTipoSessao', 'id_tipo_sessao');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'DescricaoTipoSessao', 'descricao', 'tipo_sessao');

    $this->configurarPK('IdTipoSessaoBloco',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdTipoSessao', 'tipo_sessao', 'id_tipo_sessao');
  }
}
?>