<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 03/07/2021 - criado por alv77
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class SessaoBlocoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrDescricao(SessaoBlocoDTO $objSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoBlocoDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('Descri��o n�o informada.');
    }else{
      $objSessaoBlocoDTO->setStrDescricao(trim($objSessaoBlocoDTO->getStrDescricao()));

      if (strlen($objSessaoBlocoDTO->getStrDescricao())>50){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 50 caracteres.');
      }
    }
  }

  private function validarNumOrdem(SessaoBlocoDTO $objSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoBlocoDTO->getNumOrdem())){
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }

  private function validarStrSinAgruparMembro(SessaoBlocoDTO $objSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoBlocoDTO->getStrSinAgruparMembro())){
      $objInfraException->adicionarValidacao('Sinalizador de Agrupar por Membro n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objSessaoBlocoDTO->getStrSinAgruparMembro())){
        $objInfraException->adicionarValidacao('Sinalizador de Agrupar por Membro inv�lido.');
      }
    }
  }

  private function validarStrStaTipoItem(SessaoBlocoDTO $objSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoBlocoDTO->getStrStaTipoItem())){
      $objInfraException->adicionarValidacao('Tipo dos Itens n�o informado.');
    }else if (!in_array($objSessaoBlocoDTO->getStrStaTipoItem(),InfraArray::converterArrInfraDTO(TipoSessaoBlocoRN::listarValoresTipoItem(),'StaTipo'))){
      $objInfraException->adicionarValidacao('Tipo dos Itens inv�lido.');
    }
  }

  private function validarNumIdSessaoJulgamento(SessaoBlocoDTO $objSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoBlocoDTO->getNumIdSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Sess�o de Julgamento n�o informada.');
    }
  }

  protected function cadastrarControlado(SessaoBlocoDTO $objSessaoBlocoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_cadastrar');

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objSessaoBlocoDTO, $objInfraException);
      $this->validarNumOrdem($objSessaoBlocoDTO, $objInfraException);
      $this->validarStrSinAgruparMembro($objSessaoBlocoDTO, $objInfraException);
      $this->validarStrStaTipoItem($objSessaoBlocoDTO, $objInfraException);
      $this->validarNumIdSessaoJulgamento($objSessaoBlocoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objSessaoBlocoBD->cadastrar($objSessaoBlocoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Bloco de Julgamento.',$e);
    }
  }

  protected function alterarControlado(SessaoBlocoDTO $objSessaoBlocoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarPermissao('sessao_bloco_alterar');

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objSessaoBlocoDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objSessaoBlocoDTO, $objInfraException);
      }
      if ($objSessaoBlocoDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objSessaoBlocoDTO, $objInfraException);
      }
      if ($objSessaoBlocoDTO->isSetStrSinAgruparMembro()){
        $this->validarStrSinAgruparMembro($objSessaoBlocoDTO, $objInfraException);
      }
      if ($objSessaoBlocoDTO->isSetStrStaTipoItem()){
        $this->validarStrStaTipoItem($objSessaoBlocoDTO, $objInfraException);
      }
      if ($objSessaoBlocoDTO->isSetNumIdSessaoJulgamento()){
        $this->validarNumIdSessaoJulgamento($objSessaoBlocoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      $objSessaoBlocoBD->alterar($objSessaoBlocoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Bloco de Julgamento.',$e);
    }
  }

  protected function excluirControlado($arrObjSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_excluir');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = count($arrObjSessaoBlocoDTO); $i<$iMax; $i++){
        $objSessaoBlocoBD->excluir($arrObjSessaoBlocoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Bloco de Julgamento.',$e);
    }
  }

  protected function consultarConectado(SessaoBlocoDTO $objSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_consultar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objSessaoBlocoBD->consultar($objSessaoBlocoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Bloco de Julgamento.',$e);
    }
  }

  protected function listarConectado(SessaoBlocoDTO $objSessaoBlocoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_listar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objSessaoBlocoBD->listar($objSessaoBlocoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Blocos de Julgamento.',$e);
    }
  }

  protected function contarConectado(SessaoBlocoDTO $objSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_listar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objSessaoBlocoBD->contar($objSessaoBlocoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Blocos de Julgamento.',$e);
    }
  }
/* 
  protected function desativarControlado($arrObjSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_desativar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjSessaoBlocoDTO);$i++){
        $objSessaoBlocoBD->desativar($arrObjSessaoBlocoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Bloco de Julgamento.',$e);
    }
  }

  protected function reativarControlado($arrObjSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_reativar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjSessaoBlocoDTO);$i++){
        $objSessaoBlocoBD->reativar($arrObjSessaoBlocoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Bloco de Julgamento.',$e);
    }
  }

  protected function bloquearControlado(SessaoBlocoDTO $objSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('sessao_bloco_consultar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoBlocoBD = new SessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objSessaoBlocoBD->bloquear($objSessaoBlocoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Bloco de Julgamento.',$e);
    }
  }

 */
}
?>