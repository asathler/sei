<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 03/07/2021 - criado por alv77
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class TipoSessaoBlocoRN extends InfraRN {

  public static $STA_PAUTA = '1';
  public static $STA_MESA = '2';
  public static $STA_REFERENDO = '3';

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  /**
   * @return TipoDTO[]
   * @throws InfraException
   */
  public static function listarValoresTipoItem(): array
  {
    try {

      $arrObjTipoDTO = array();

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_PAUTA);
      $objTipoDTO->setStrDescricao('Pauta');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_MESA);
      $objTipoDTO->setStrDescricao('Mesa');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_REFERENDO);
      $objTipoDTO->setStrDescricao('Referendo');
      $arrObjTipoDTO[] = $objTipoDTO;

      return $arrObjTipoDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de Tipos de Item de Sess�o.',$e);
    }
  }

  private function validarStrDescricao(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoBlocoDTO->getStrDescricao())) {
      $objInfraException->adicionarValidacao('Descri��o n�o informada.');
    } else {
      $objTipoSessaoBlocoDTO->setStrDescricao(trim($objTipoSessaoBlocoDTO->getStrDescricao()));

      if (strlen($objTipoSessaoBlocoDTO->getStrDescricao())>50) {
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 50 caracteres.');
      }

      $dto = new TipoSessaoBlocoDTO();
      $dto->setNumIdTipoSessao($objTipoSessaoBlocoDTO->getNumIdTipoSessao());
      $dto->setNumIdTipoSessaoBloco($objTipoSessaoBlocoDTO->getNumIdTipoSessaoBloco(), InfraDTO::$OPER_DIFERENTE);
      $dto->setStrDescricao($objTipoSessaoBlocoDTO->getStrDescricao());

      if ($this->contar($dto)>0) {
        $objInfraException->adicionarValidacao('Existe outra ocorr�ncia com esta Descri��o para o mesmo tipo de Sess�o.');
      }

    }
  }

  private function validarNumOrdem(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoBlocoDTO->getNumOrdem())){
      $objTipoSessaoBlocoDTO->setNumOrdem(null);
    }
  }

  private function validarStrSinAgruparMembro(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoBlocoDTO->getStrSinAgruparMembro())){
      $objInfraException->adicionarValidacao('Sinalizador Agrupador n�o informado.');
    }else if (!InfraUtil::isBolSinalizadorValido($objTipoSessaoBlocoDTO->getStrSinAgruparMembro())){
      $objInfraException->adicionarValidacao('Sinalizador Agrupador inv�lido.');
    }
  }

  private function validarStrStaTipoItem(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoBlocoDTO->getStrStaTipoItem())){
      $objTipoSessaoBlocoDTO->setStrStaTipoItem(null);
    }else{
      if (!in_array($objTipoSessaoBlocoDTO->getStrStaTipoItem(),InfraArray::converterArrInfraDTO(self::listarValoresTipoItem(),'StaTipo'))){
        $objInfraException->adicionarValidacao('Tipo de Itens inv�lido.');
      }
    }
  }

  private function validarNumIdTipoSessao(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoBlocoDTO->getNumIdTipoSessao())){
      $objInfraException->adicionarValidacao('Tipo de Sess�o n�o informado.');
    }
  }

  protected function cadastrarControlado(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_cadastrar');

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objTipoSessaoBlocoDTO, $objInfraException);
      $this->validarNumOrdem($objTipoSessaoBlocoDTO, $objInfraException);
      $this->validarStrSinAgruparMembro($objTipoSessaoBlocoDTO, $objInfraException);
      $this->validarStrStaTipoItem($objTipoSessaoBlocoDTO, $objInfraException);
      $this->validarNumIdTipoSessao($objTipoSessaoBlocoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBlocoBD->cadastrar($objTipoSessaoBlocoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Bloco de Julgamento.',$e);
    }
  }

  protected function alterarControlado(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_alterar');

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objTipoSessaoBlocoDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objTipoSessaoBlocoDTO, $objInfraException);
      }
      if ($objTipoSessaoBlocoDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objTipoSessaoBlocoDTO, $objInfraException);
      }
      if ($objTipoSessaoBlocoDTO->isSetStrSinAgruparMembro()){
        $this->validarStrSinAgruparMembro($objTipoSessaoBlocoDTO, $objInfraException);
      }
      if ($objTipoSessaoBlocoDTO->isSetStrStaTipoItem()){
        $this->validarStrStaTipoItem($objTipoSessaoBlocoDTO, $objInfraException);
      }
      if ($objTipoSessaoBlocoDTO->isSetNumIdTipoSessao()){
        $this->validarNumIdTipoSessao($objTipoSessaoBlocoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      $objTipoSessaoBlocoBD->alterar($objTipoSessaoBlocoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Bloco de Julgamento.',$e);
    }
  }

  protected function excluirControlado($arrObjTipoSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_excluir');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = count($arrObjTipoSessaoBlocoDTO); $i<$iMax; $i++){
        $objTipoSessaoBlocoBD->excluir($arrObjTipoSessaoBlocoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Bloco de Julgamento.',$e);
    }
  }

  protected function consultarConectado(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_consultar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBlocoBD->consultar($objTipoSessaoBlocoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Bloco de Julgamento.',$e);
    }
  }

  protected function listarConectado(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_listar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBlocoBD->listar($objTipoSessaoBlocoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Bloco de Julgamento.',$e);
    }
  }

  protected function contarConectado(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_listar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBlocoBD->contar($objTipoSessaoBlocoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Bloco de Julgamento.',$e);
    }
  }
/* 
  protected function desativarControlado($arrObjTipoSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_desativar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjTipoSessaoBlocoDTO);$i++){
        $objTipoSessaoBlocoBD->desativar($arrObjTipoSessaoBlocoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Bloco de Julgamento.',$e);
    }
  }

  protected function reativarControlado($arrObjTipoSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_reativar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjTipoSessaoBlocoDTO);$i++){
        $objTipoSessaoBlocoBD->reativar($arrObjTipoSessaoBlocoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Bloco de Julgamento.',$e);
    }
  }

  protected function bloquearControlado(TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarPermissao('tipo_sessao_bloco_consultar');

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBlocoBD = new TipoSessaoBlocoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBlocoBD->bloquear($objTipoSessaoBlocoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Bloco de Julgamento.',$e);
    }
  }

 */
}
?>