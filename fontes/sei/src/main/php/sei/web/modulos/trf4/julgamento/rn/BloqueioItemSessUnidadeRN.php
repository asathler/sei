<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 06/06/2019 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class BloqueioItemSessUnidadeRN extends InfraRN {

  public static $TB_DOCUMENTO = 'D';
  public static $TB_PROCEDIMENTO = 'P';


  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  public function listarValoresTipo(){
    try {

      $objArrTipoDTO = array();

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$TB_DOCUMENTO);
      $objTipoDTO->setStrDescricao('Bloqueio de Documento');
      $objArrTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$TB_PROCEDIMENTO);
      $objTipoDTO->setStrDescricao('Bloqueio de Processo');
      $objArrTipoDTO[] = $objTipoDTO;


      return $objArrTipoDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de Tipo.',$e);
    }
  }

  private function validarNumIdDistribuicao(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objBloqueioItemSessUnidadeDTO->getNumIdDistribuicao())){
      $objInfraException->adicionarValidacao('Distribui��o n�o encontrada.');
    }
  }

  private function validarDblIdDocumento(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objBloqueioItemSessUnidadeDTO->getDblIdDocumento())){
      $objBloqueioItemSessUnidadeDTO->setDblIdDocumento(null);
    }
  }

  private function validarStrStaTipo(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objBloqueioItemSessUnidadeDTO->getStrStaTipo())){
      $objInfraException->adicionarValidacao('Tipo de Bloqueio n�o informado.');
    }else if (!in_array($objBloqueioItemSessUnidadeDTO->getStrStaTipo(),InfraArray::converterArrInfraDTO($this->listarValoresTipo(),'StaTipo'))){
      $objInfraException->adicionarValidacao('Tipo de Bloqueio inv�lido.');
    }
  }

  private function validarNumIdUnidade(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objBloqueioItemSessUnidadeDTO->getNumIdUnidade())){
      $objBloqueioItemSessUnidadeDTO->setNumIdUnidade(null);
    }
  }

  protected function registrarControlado(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('bloqueio_item_sess_unidade_cadastrar',__METHOD__,$objBloqueioItemSessUnidadeDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdDistribuicao($objBloqueioItemSessUnidadeDTO, $objInfraException);
      $this->validarDblIdDocumento($objBloqueioItemSessUnidadeDTO, $objInfraException);
      $this->validarStrStaTipo($objBloqueioItemSessUnidadeDTO, $objInfraException);

      $arrIdUnidade=$objBloqueioItemSessUnidadeDTO->getArrNumIdUnidade();
      if($arrIdUnidade==null){
        $arrIdUnidade=array();
      }

      $idUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
      if(in_array($idUnidadeAtual,$arrIdUnidade)){
        $objInfraException->lancarValidacao('Unidade atual n�o pode ser bloqueada.');
      }

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($objBloqueioItemSessUnidadeDTO->getNumIdDistribuicao());
      $objItemSessaoJulgamentoDTO->setStrStaUltimoDistribuicao(DistribuicaoRN::$SD_ULTIMA);//n�o pega julgados
      $objItemSessaoJulgamentoDTO->retStrStaSituacao();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdUnidadeSessao();
      $objItemSessaoJulgamentoDTO->retNumIdUnidadeRelatorDistribuicao();
      $objItemSessaoJulgamentoDTO->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_DESC);
      $objItemSessaoJulgamentoDTO->setNumMaxRegistrosRetorno(1);
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
      if($objItemSessaoJulgamentoDTO==null){
        throw new InfraException('Item de sess�o de julgamento n�o encontrado.');
      }

      $objColegiadoDTO=new ColegiadoDTO();
      $objColegiadoRN=new ColegiadoRN();
      $objColegiadoDTO->setNumIdColegiado($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
      $objColegiadoDTO->retNumIdUnidadeResponsavel();
      $objColegiadoDTO=$objColegiadoRN->consultar($objColegiadoDTO);

      if($objBloqueioItemSessUnidadeDTO->getStrStaTipo()==BloqueioItemSessUnidadeRN::$TB_DOCUMENTO){
        $objItemSessaoDocumentoDTO=new ItemSessaoDocumentoDTO();
        $objItemSessaoDocumentoDTO->setDblIdDocumento($objBloqueioItemSessUnidadeDTO->getDblIdDocumento());
        $objItemSessaoDocumentoDTO->setNumIdDistribuicaoItem($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
        $objItemSessaoDocumentoDTO->retNumIdUnidade();
        $objItemSessaoDocumentoDTO->setNumMaxRegistrosRetorno(1);
        $objItemSessaoDocumentoDTO->setOrdDthSessaoSessaoJulgamento(InfraDTO::$TIPO_ORDENACAO_DESC);
        $objItemSessaoDocumentoRN=new ItemSessaoDocumentoRN();
        $objItemSessaoDocumentoDTO=$objItemSessaoDocumentoRN->consultar($objItemSessaoDocumentoDTO);
        if($idUnidadeAtual!=$objColegiadoDTO->getNumIdUnidadeResponsavel() && $idUnidadeAtual!=$objItemSessaoDocumentoDTO->getNumIdUnidade()){
          throw new InfraException('Unidade atual n�o pode alterar bloqueios de acesso ao documento.');
        }
        if(in_array($objItemSessaoDocumentoDTO->getNumIdUnidade(),$arrIdUnidade)){
          $objInfraException->lancarValidacao('Unidade que disponibilizou documento n�o pode ser bloqueada.');
        }

      }
      if($objBloqueioItemSessUnidadeDTO->getStrStaTipo()==BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO){
        if($idUnidadeAtual!=$objColegiadoDTO->getNumIdUnidadeResponsavel() && $idUnidadeAtual!=$objItemSessaoJulgamentoDTO->getNumIdUnidadeRelatorDistribuicao()){
          throw new InfraException('Unidade atual n�o pode alterar bloqueios de acesso ao documento.');
        }
        if(in_array($objItemSessaoJulgamentoDTO->getNumIdUnidadeRelatorDistribuicao(),$arrIdUnidade)){
          $objInfraException->lancarValidacao('Unidade do relator do processo n�o pode ser bloqueada.');
        }
        if(in_array($objItemSessaoJulgamentoDTO->getNumIdUnidadeSessao(),$arrIdUnidade)){
          $objInfraException->lancarValidacao('Unidade que colocou o processo em mesa n�o pode ser bloqueada.');
        }
      }
      $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
//      $objColegiadoComposicaoDTO->setStrSinHabilitado('S');
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->retNumIdUnidade();
      $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
      $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
      $arrIdUnidadesColegiado=InfraArray::converterArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUnidade','IdUnidade');

      foreach ($arrIdUnidade as $idUnidade) {
        if(!isset($arrIdUnidadesColegiado[$idUnidade])){
          $objInfraException->lancarValidacao('Unidade inclu�da n�o pertence ao colegiado.');
        }
      }


      $objInfraException->lancarValidacoes();

      $objBloqueioItemSessUnidadeDTOBanco=new BloqueioItemSessUnidadeDTO();
      $objBloqueioItemSessUnidadeDTOBanco->setNumIdDistribuicao($objBloqueioItemSessUnidadeDTO->getNumIdDistribuicao());
      $objBloqueioItemSessUnidadeDTOBanco->setDblIdDocumento($objBloqueioItemSessUnidadeDTO->getDblIdDocumento());
      $objBloqueioItemSessUnidadeDTOBanco->setStrStaTipo($objBloqueioItemSessUnidadeDTO->getStrStaTipo());
      $objBloqueioItemSessUnidadeDTOBanco->retNumIdBloqueioItemSessUnidade();
      $this->excluir($this->listar($objBloqueioItemSessUnidadeDTOBanco));


      if(count($arrIdUnidade)){
        $objBloqueioItemSessUnidadeBD = new BloqueioItemSessUnidadeBD($this->getObjInfraIBanco());

        $objBloqueioItemSessUnidadeDTOBase=new BloqueioItemSessUnidadeDTO();
        $objBloqueioItemSessUnidadeDTOBase->setNumIdDistribuicao($objBloqueioItemSessUnidadeDTO->getNumIdDistribuicao());
        $objBloqueioItemSessUnidadeDTOBase->setDblIdDocumento($objBloqueioItemSessUnidadeDTO->getDblIdDocumento());
        $objBloqueioItemSessUnidadeDTOBase->setStrStaTipo($objBloqueioItemSessUnidadeDTO->getStrStaTipo());

        foreach ($arrIdUnidade as $idUnidade) {
          $objBloqueioItemSessUnidadeDTO2= clone $objBloqueioItemSessUnidadeDTOBase;
          $objBloqueioItemSessUnidadeDTO2->setNumIdUnidade($idUnidade);
          $objBloqueioItemSessUnidadeBD->cadastrar($objBloqueioItemSessUnidadeDTO2);
        }

      }


      //Auditoria
      return count($arrIdUnidade);

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Bloqueio de Item.',$e);
    }
  }
  protected function cadastrarControlado(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('bloqueio_item_sess_unidade_cadastrar',__METHOD__,$objBloqueioItemSessUnidadeDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdDistribuicao($objBloqueioItemSessUnidadeDTO, $objInfraException);
      $this->validarDblIdDocumento($objBloqueioItemSessUnidadeDTO, $objInfraException);
      $this->validarStrStaTipo($objBloqueioItemSessUnidadeDTO, $objInfraException);
      $this->validarNumIdUnidade($objBloqueioItemSessUnidadeDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objBloqueioItemSessUnidadeBD = new BloqueioItemSessUnidadeBD($this->getObjInfraIBanco());
      $ret = $objBloqueioItemSessUnidadeBD->cadastrar($objBloqueioItemSessUnidadeDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Bloqueio de Item.',$e);
    }
  }


  protected function excluirControlado($arrObjBloqueioItemSessUnidadeDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('bloqueio_item_sess_unidade_excluir',__METHOD__,$arrObjBloqueioItemSessUnidadeDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objBloqueioItemSessUnidadeBD = new BloqueioItemSessUnidadeBD($this->getObjInfraIBanco());
      for($i=0, $iMax = count($arrObjBloqueioItemSessUnidadeDTO); $i<$iMax; $i++){
        $objBloqueioItemSessUnidadeBD->excluir($arrObjBloqueioItemSessUnidadeDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Bloqueio de Item.',$e);
    }
  }

  protected function consultarConectado(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('bloqueio_item_sess_unidade_consultar',__METHOD__,$objBloqueioItemSessUnidadeDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objBloqueioItemSessUnidadeBD = new BloqueioItemSessUnidadeBD($this->getObjInfraIBanco());
      $ret = $objBloqueioItemSessUnidadeBD->consultar($objBloqueioItemSessUnidadeDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Bloqueio de Item.',$e);
    }
  }

  protected function listarConectado(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('bloqueio_item_sess_unidade_listar',__METHOD__,$objBloqueioItemSessUnidadeDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objBloqueioItemSessUnidadeBD = new BloqueioItemSessUnidadeBD($this->getObjInfraIBanco());
      $ret = $objBloqueioItemSessUnidadeBD->listar($objBloqueioItemSessUnidadeDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Bloqueio de Itens.',$e);
    }
  }

  protected function contarConectado(BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('bloqueio_item_sess_unidade_listar',__METHOD__,$objBloqueioItemSessUnidadeDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objBloqueioItemSessUnidadeBD = new BloqueioItemSessUnidadeBD($this->getObjInfraIBanco());
      $ret = $objBloqueioItemSessUnidadeBD->contar($objBloqueioItemSessUnidadeDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Bloqueio de Itens.',$e);
    }
  }
}
?>