<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class SessaoJulgamentoDTO extends InfraDTO {

  public function getStrNomeTabela(): string
  {
  	 return 'sessao_julgamento';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdSessaoJulgamento', 'id_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL, 'IdProcedimento', 'id_procedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'Sessao', 'dth_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'StaSituacao', 'sta_situacao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdTipoSessao', 'id_tipo_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdColegiado', 'id_colegiado');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdColegiadoVersao', 'id_colegiado_versao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'Inicio', 'dth_inicio');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'Fim', 'dth_fim');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUsuarioPresidente','id_usuario_presidente');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL, 'IdDocumentoSessao','id_documento_sessao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL, 'IdDocumentoPauta','id_documento_pauta');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaColegiado','sigla','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeColegiado','nome','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'ArtigoColegiado','artigo','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'QuorumMinimoColegiado','quorum_minimo','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdTipoProcedimentoColegiado','id_tipo_procedimento','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUsuarioPresidenteColegiado','id_usuario_presidente','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUnidadeResponsavelColegiado','id_unidade_responsavel','colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomePresidenteSessao','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'ProtocoloFormatadoProtocolo','protocolo_formatado','protocolo');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'ProtocoloFormatadoDocumentoPauta','p2.protocolo_formatado','protocolo p2');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoTipoSessao','descricao','tipo_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SinVirtualTipoSessao','sin_virtual','tipo_sessao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjColegiadoComposicaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjPresencaSessaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjItemSessaoJulgamentoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjPedidoVistaDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjBloqueioItemSessUnidadeDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'Quorum');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'HashPauta');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'HorarioVotacaoAtivo');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'UnidadeSecretaria');


    $this->configurarFK('IdColegiado','colegiado','id_colegiado');
    $this->configurarFK('IdTipoSessao','tipo_sessao','id_tipo_sessao');
    $this->configurarFK('IdUsuarioPresidente','usuario','id_usuario',InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdProcedimento','protocolo','id_protocolo',InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdDocumentoPauta','protocolo p2','p2.id_protocolo',InfraDTO::$TIPO_FK_OPCIONAL);

    $this->configurarPK('IdSessaoJulgamento',InfraDTO::$TIPO_PK_NATIVA);

  }
}
?>