<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ResumoVotacaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return null;
  }

  public function montar() {

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'Votos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Membros');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Tipo');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Provimento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Complemento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Acompanham');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'Ressalvas');

  }
}
?>