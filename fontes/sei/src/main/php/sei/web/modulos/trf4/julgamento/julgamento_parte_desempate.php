<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  $numIdItemSessaoJulgamento=$_GET['id_item_sessao_julgamento'];
  $numIdJulgamentoParte=$_GET['id_julgamento_parte'];

  $strParametros= '&id_item_sessao_julgamento='.$numIdItemSessaoJulgamento;
  $strParametros.='&id_julgamento_parte='.$numIdJulgamentoParte;


  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'julgamento_parte_desempatar':
      $strTitulo = 'Desempatar Julgamento';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmGravarDesempate" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="parent.infraFecharJanelaModal();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);
      $arrObjJulgamentoParteDTO=InfraArray::indexarArrInfraDTO($objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO(),'IdJulgamentoParte');
      if (!isset($arrObjJulgamentoParteDTO[$numIdJulgamentoParte])){
        $objInfraException=new InfraException('Item do Julgamento n�o localizada');
      }
      $objJulgamentoParteDTO=$arrObjJulgamentoParteDTO[$numIdJulgamentoParte];

      if ($objJulgamentoParteDTO->getBolPossuiEmpate()){
        $arrUsuariosEmpate=$objJulgamentoParteDTO->getArrUsuariosEmpate();
        if (InfraArray::contar($arrUsuariosEmpate)<=1){
          $objInfraException=new InfraException('Erro obtendo votos empatados');
        }
      } else {
        $objInfraException=new InfraException('Item do Julgamento n�o possui empate');
      }

      if (isset($_POST['sbmGravarDesempate'])) {
        try{
          $objJulgamentoParteRN = new JulgamentoParteRN();
          $objJulgamentoParteDTO2=new JulgamentoParteDTO();
          $objJulgamentoParteDTO2->setNumIdJulgamentoParte($numIdJulgamentoParte);
          $objJulgamentoParteDTO2->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
          $objJulgamentoParteDTO2->setNumIdUsuarioDesempate($_POST['rdoUsuarioDesempate']);

          $objJulgamentoParteRN->alterar($objJulgamentoParteDTO2);

          $bolExecucaoOK=true;
          //PaginaSEI::getInstance()->adicionarMensagem('Destaque "'.$objDestaqueDTO->getNumIdUnidade().'" cadastrado com sucesso.');
          //header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_destaque='.$objDestaqueDTO->getNumIdDestaque().$strParametros.PaginaSEI::getInstance()->montarAncora($objDestaqueDTO->getNumIdDestaque())));
          //die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;
    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }
  $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(),'IdUsuario');
  $arrObjVotoParteDTO=InfraArray::indexarArrInfraDTO($objJulgamentoParteDTO->getArrObjVotoParteDTO(),'IdUsuario');
  $numRegistros=InfraArray::contar($arrUsuariosEmpate);
  $strResultado = '<br/><br/>';
  $strSumarioTabela = 'Tabela de Votos Empatados.';
  $strCaptionTabela = 'Votos Empatados';
  $strResultado .= '<table width="90%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
  $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
  $strResultado .= '<tr>';
  $strResultado .= '<th class="infraTh" width="1%">Voto de Desempate</th>'."\n"; //radio
  $strResultado .= '<th class="infraTh">Membro</th>'."\n";
  $strResultado .= '<th class="infraTh">Provimento</th>'."\n";
  $strResultado .= '</tr>'."\n";
  $strCssTr='';

  for($i = 0;$i < $numRegistros; $i++){
    $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
    $strResultado .= $strCssTr;

    $strResultado .= '<td align="center" valign="center"><input type="radio" name="rdoUsuarioDesempate"';
    if($objJulgamentoParteDTO->getNumIdUsuarioDesempate()==$arrUsuariosEmpate[$i]){
      $strResultado.=' checked';
    }
    $strResultado .= ' value="'.$arrUsuariosEmpate[$i].'"></td>';
    $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjColegiadoComposicaoDTO[$arrUsuariosEmpate[$i]]->getStrNomeUsuario()).'</td>';
    $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjVotoParteDTO[$arrUsuariosEmpate[$i]]->getStrConteudoProvimento().' '.$arrObjVotoParteDTO[$arrUsuariosEmpate[$i]]->getStrComplemento()).'</td>';
    $strResultado .= '</tr>'."\n";
  }
  $strResultado .= '</table>';

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblDescricao {position:absolute;left:0%;top:0%;width:95%;}
#txaDescricao {position:absolute;left:0%;top:10%;width:95%;}


<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
function inicializar(){
<?if($bolExecucaoOK){ ?>
  window.parent.infraFecharJanelaModal();
  window.parent.document.getElementById('frmItemSessaoJulgamentoJulgar').submit();
  window.close();
<?}?>
  infraEfeitoTabelas();
}

function validarCadastro() {


  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmDestaqueCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->abrirAreaDados('5em');
//PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
//PaginaSEI::getInstance()->montarAreaDebug();
//PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>