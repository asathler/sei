<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  ////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);
  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento','id_item_sessao_documento','num_seq'));


  $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
  $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();

  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '';
  $mensagem='';


  $arrComandos = array();
  $strTitulo = 'Restri��o de Acesso';
  $bolExecucaoOK=false;
  $bolBloqueioDocumento=false;
  if(isset($_POST['id_item_sessao_documento'])){
    $bolBloqueioDocumento=true;
//    $strTitulo = 'Restri��o de acesso a documento disponibilizado';
    $staTipo=BloqueioItemSessUnidadeRN::$TB_DOCUMENTO;
  } else {
    $bolBloqueioDocumento=false;
//    $strTitulo = 'Restri��o de acesso a processo disponibilizado';
    $staTipo=BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO;
    $dblIdDocumento=null;
  }





  switch($_GET['acao']) {
    case 'bloqueio_item_sess_unidade_alterar':

      $arrComandos[] = '<button type="submit" accesskey="" name="sbmSalvar" value="Salvar" class="infraButton">Salvar</button>';

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->retNumIdUnidadeRelatorDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTO->retStrDescricaoSessaoBloco();
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);


      if($bolBloqueioDocumento){
        $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
        $objItemSessaoDocumentoDTO->setNumIdItemSessaoDocumento($_GET['id_item_sessao_documento']);
        $objItemSessaoDocumentoDTO->retStrProtocoloDocumentoFormatado();
        $objItemSessaoDocumentoDTO->retTodos();
        $objItemSessaoDocumentoRN=new ItemSessaoDocumentoRN();
        $objItemSessaoDocumentoDTO=$objItemSessaoDocumentoRN->consultar($objItemSessaoDocumentoDTO);
        if($objItemSessaoDocumentoDTO==null){
          throw new InfraException('Documento n�o localizado na sess�o de julgamento.');
        }
        $dblIdDocumento=$objItemSessaoDocumentoDTO->getDblIdDocumento();
      }



      $objBloqueioItemSessUnidadeDTO=new BloqueioItemSessUnidadeDTO();
      $objBloqueioItemSessUnidadeRN=new BloqueioItemSessUnidadeRN();
      $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      $objBloqueioItemSessUnidadeDTO->retTodos();
      $arrObjBloqueioItemSessUnidadeDTO=$objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);


      $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
      $numIdUsuario=ColegiadoINT::getUsuarioUnidadeAtualColegiado($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());


      if (isset($_POST['sbmSalvar'])) {
        try {

          $arrUnidadesRestritas=PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $objBloqueioItemSessUnidadeDTO = new BloqueioItemSessUnidadeDTO();
          $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
          if ($bolBloqueioDocumento) {
            $objBloqueioItemSessUnidadeDTO->setDblIdDocumento($objItemSessaoDocumentoDTO->getDblIdDocumento());
            $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_DOCUMENTO);
          } else {
            $objBloqueioItemSessUnidadeDTO->setDblIdDocumento(null);
            $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);
          }
          $objBloqueioItemSessUnidadeDTO->setArrNumIdUnidade($arrUnidadesRestritas);

          $qtdUnidades=$objBloqueioItemSessUnidadeRN->registrar($objBloqueioItemSessUnidadeDTO);

          if($qtdUnidades>0){
            $objBloqueioItemSessUnidadeDTO->retStrSiglaUnidade();
            $objBloqueioItemSessUnidadeDTO->setOrdStrSiglaUnidade(InfraDTO::$TIPO_ORDENACAO_ASC);
            $arrObjBloqueioItemSessUnidadeDTO=$objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);
            $arrStrSiglaUnidadesRestritas=InfraArray::converterArrInfraDTO($arrObjBloqueioItemSessUnidadeDTO,'SiglaUnidade');
            $strUnidades=PaginaSEI::formatarParametrosJavaScript(implode('\n',$arrStrSiglaUnidadesRestritas));
            $tooltip=PaginaSEI::montarTitleTooltip($strUnidades, 'Restri��o de acesso');
          } else {
            $tooltip=PaginaSEI::montarTitleTooltip('Sem restri��o');
          }

          if(strpos($tooltip,'title')===0){
            $pos1=strpos($tooltip,'" onmouseover=');
            $pos2=strpos($tooltip,'" onmouseout=');
            $strTituloIcone=substr($tooltip,7,$pos1-7);
            $strMouseOverIcone=substr($tooltip,$pos1+15,$pos2-$pos1-15);
            $strMouseOutIcone=substr($tooltip,$pos2+14,strlen($tooltip)-$pos2-15);
          } else {
            $strTituloIcone='';
            $pos2=strpos($tooltip,'" onmouseout=');
            $strMouseOverIcone=substr($tooltip,13,$pos2-13);
            $strMouseOutIcone=substr($tooltip,$pos2+14,strlen($tooltip)-$pos2-15);
          }

          PaginaSEI::getInstance()->adicionarMensagem('Restri��o registrada com sucesso.');
          $bolExecucaoOK = true;
        } catch (Exception $e) {
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      } else {
        $strItensSelUnidadesMultipla = BloqueioItemSessUnidadeINT::montarSelectUnidades(null,null,'',$objItemSessaoJulgamentoDTO->getNumIdDistribuicao(),$dblIdDocumento);
      }



      break;

    default:
      throw new InfraException("A��o '" . $_GET['acao'] . '\' n�o reconhecida.');
  }

  $strProcesso=' '.$objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco().' ';
  $strProcesso .= $_GET['num_seq']. ' ('.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo().')';

  if($bolBloqueioDocumento) {
    $strDocumento = $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado();
  }




  $objBloqueioItemSessUnidadeDTO=new BloqueioItemSessUnidadeDTO();
  $objBloqueioItemSessUnidadeRN=new BloqueioItemSessUnidadeRN();
  $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao( $objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
  $objBloqueioItemSessUnidadeDTO->retTodos();
  $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);
  $arrObjBloqueioItemSessUnidadeDTO=$objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);
  $arrIdUnidadesSelecionadas=InfraArray::converterArrInfraDTO($arrObjBloqueioItemSessUnidadeDTO,'IdUnidade');
  $bolExibeTabelaBloqueio=true;
  $arrIdUnidadesBloqueio=array();
  $arrIdUnidadesBloqueio[]=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
  $arrIdUnidadesBloqueio[]=$objItemSessaoJulgamentoDTO->getNumIdUnidadeRelatorDistribuicao();
  $arrIdUnidadesBloqueio=array_unique($arrIdUnidadesBloqueio);
  $strTabelaUnidadesBloqueio=ColegiadoComposicaoINT::montarTabelaUnidades($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento(),$arrIdUnidadesBloqueio,$arrIdUnidadesSelecionadas);






}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}
//$acaoRetorno=PaginaSEI::getInstance()->getAcaoRetorno();
//$strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&montar_visualizacao=1'.$strParametros);
//$strLinkArvore=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_visualizar&acao_origem='.$_GET['acao'].$strParametros.'&montar_visualizacao=1');
//$arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" value="Cancelar" onclick="location.href=\''.$strLinkRetorno.'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';



PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>

  #lblProcesso {position:absolute;left:0%;top:0%;width:20%;font-size: 1.4em}
  #lblProcesso2 {position:absolute;left:12%;top:0%;width:40%;font-size: 1.4em}
  #lblDocumento {position:absolute;left:0%;top:15%;width:20%;font-size: 1.4em}
  #lblDocumento2 {position:absolute;left:12%;top:15%;width:40%;font-size: 1.4em}


<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
var objLupaUnidadesRestritas;
function inicializar(){
<? if ($bolExecucaoOK) { ?>
  if (typeof window.parent.atualizarIconeBloqueio === 'function'){
    window.parent.atualizarIconeBloqueio(<?=$_GET['id_item_sessao_julgamento']?>,'<?=base64_encode($strTituloIcone)?>','<?=base64_encode($strMouseOverIcone)?>','<?=base64_encode($strMouseOutIcone)?>',<?=$qtdUnidades>0?1:0?>);
  }
  window.parent.infraFecharJanelaModal();
  <?
  }?>
  infraEfeitoTabelas();
}
  function validarCadastro() {
    return true;
  }
function onSubmitForm(){
  return validarCadastro();
}
function atualizarTelaSessao(){

}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmItemSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
//PaginaSEI::getInstance()->abrirAreaDados('10em');

  //PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados('5em');
    ?>

    <label id="lblProcesso" accesskey="" class="infraLabelOpcional">Processo:</label>
    <label id="lblProcesso2" accesskey="" class="infraLabelObrigatorio"><?=$strProcesso?></label>
  <?
  if($bolBloqueioDocumento) {
    ?>
    <label id="lblDocumento" accesskey="" class="infraLabelOpcional">Documento:</label>
    <label id="lblDocumento2" accesskey="" class="infraLabelObrigatorio"><?= $strDocumento ?></label>
    <?
  }

    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->AbrirAreaDados();
    ?>
  <label id="lblTabelaBloqueio" for="" accesskey="" class="infraLabelOpcional">Bloquear acesso �s seguintes unidades:</label>
  <?
  PaginaSEI::getInstance()->montarAreaTabela($strTabelaUnidadesBloqueio, 1);
  PaginaSEI::getInstance()->fecharAreaDados();

  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>