<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class AusenciaSessaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'ausencia_sessao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,
                                   'IdAusenciaSessao',
                                   'id_ausencia_sessao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,
                                   'IdUsuario',
                                   'id_usuario');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,
                                   'IdSessaoJulgamento',
                                   'id_sessao_julgamento');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,
                                   'IdMotivoAusencia',
                                   'id_motivo_ausencia');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,
        'DescricaoMotivoAusencia',
        'descricao',
        'motivo_ausencia');
    
    $this->configurarPK('IdAusenciaSessao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdMotivoAusencia', 'motivo_ausencia', 'id_motivo_ausencia');
  }
}
?>