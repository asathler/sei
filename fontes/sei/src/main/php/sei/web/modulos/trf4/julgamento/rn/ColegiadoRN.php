<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 16/07/2014 - criado por bcu@trf4.jus.br
 *
 * Vers�o do Gerador de C�digo: 1.33.1
 *
 * Vers�o no CVS: $Id$
 */

require_once __DIR__ . '/../../../../SEI.php';

class ColegiadoRN extends InfraRN
{

  public function __construct()
  {
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco()
  {
    return BancoSEI::getInstance();
  }

  private function validarStrSigla(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getStrSigla())) {
      $objInfraException->adicionarValidacao('Sigla n�o informada.');
    } else {
      $objColegiadoDTO->setStrSigla(trim($objColegiadoDTO->getStrSigla()));

      if (strlen($objColegiadoDTO->getStrSigla())>30) {
        $objInfraException->adicionarValidacao('Sigla possui tamanho superior a 30 caracteres.');
      }
      $dto = new ColegiadoDTO();
      $dto->retStrSinAtivo();
      $dto->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado(), InfraDTO::$OPER_DIFERENTE);
      $dto->setStrSigla($objColegiadoDTO->getStrSigla());
      $dto->setBolExclusaoLogica(false);

      $dto = $this->consultar($dto);
      if ($dto!=NULL) {
        if ($dto->getStrSinAtivo()=='S') {
          $objInfraException->adicionarValidacao('Existe outra ocorr�ncia de Colegiado que utiliza a mesma Sigla.');
        } else {
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa de Colegiado que utiliza a mesma Sigla.');
        }
      }
    }
  }

  private function validarStrNome(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getStrNome())) {
      $objInfraException->adicionarValidacao('Nome n�o informado.');
    } else {
      $objColegiadoDTO->setStrNome(trim($objColegiadoDTO->getStrNome()));

      if (strlen($objColegiadoDTO->getStrNome())>100) {
        $objInfraException->adicionarValidacao('Nome possui tamanho superior a 100 caracteres.');
      }
      $dto = new ColegiadoDTO();
      $dto->retStrSinAtivo();
      $dto->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado(), InfraDTO::$OPER_DIFERENTE);
      $dto->setStrNome($objColegiadoDTO->getStrNome());
      $dto->setBolExclusaoLogica(false);

      $dto = $this->consultar($dto);
      if ($dto!=NULL) {
        if ($dto->getStrSinAtivo()=='S')
          $objInfraException->adicionarValidacao('Existe outra ocorr�ncia de Colegiado que utiliza o mesmo Nome.');
        else
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa de Colegiado que utiliza o mesmo Nome.');
      }
    }
  }

  private function validarNumIdUsuarioSecretario(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getNumIdUsuarioSecretario())) {
      $objInfraException->adicionarValidacao('Secret�rio n�o informado.');
    }
  }

  private function validarNumIdUsuarioPresidente(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getNumIdUsuarioPresidente())) {
      $objInfraException->adicionarValidacao('Presidente n�o informado.');
    }
  }

  private function validarNumIdUnidadeResponsavel(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getNumIdUnidadeResponsavel())) {
      $objInfraException->adicionarValidacao('Unidade Respons�vel n�o informada.');
    }
  }

  private function validarNumIdTipoProcedimento(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getNumIdTipoProcedimento())) {
      $objInfraException->adicionarValidacao('Tipo de Procedimento n�o informado.');
    }
  }

  private function validarStrSinAtivo(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getStrSinAtivo())) {
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    } else {
      if (!InfraUtil::isBolSinalizadorValido($objColegiadoDTO->getStrSinAtivo())) {
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }

  private function validarNumQuorumMinimo(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getNumQuorumMinimo())) {
      $objInfraException->adicionarValidacao('Qu�rum M�nimo n�o informado.');
    } else {
      $numQuorumMinimo = $objColegiadoDTO->getNumQuorumMinimo();
      if ($numQuorumMinimo<1 || $numQuorumMinimo>99) {
        $objInfraException->adicionarValidacao('Qu�rum M�nimo deve ser entre 1 e 99.');
      }

      if ($numQuorumMinimo>InfraArray::contar($objColegiadoDTO->getArrObjColegiadoComposicaoDTO())) {
        $objInfraException->adicionarValidacao('Qu�rum M�nimo maior que o n�mero de membros.');
      }
    }
  }

  private function validarStrStaAlgoritmoDistribuicao(ColegiadoDTO $objColegiadoDTO, InfraException $objInfraException)
  {
    if (InfraString::isBolVazia($objColegiadoDTO->getStrStaAlgoritmoDistribuicao())) {
      $objInfraException->adicionarValidacao('Algoritmo de Distribui��o n�o informado.');
    } else {
      $objAlgoritmoRN = new AlgoritmoRN();
      if (!in_array($objColegiadoDTO->getStrStaAlgoritmoDistribuicao(), InfraArray::converterArrInfraDTO($objAlgoritmoRN->listarValoresAlgoritmo(), 'StaTipo'))) {
        $objInfraException->adicionarValidacao('Algoritmo de Distribui��o inv�lido.');
      }
    }
  }

  /**
   * @param ColegiadoDTO $objColegiadoDTO
   * @return array|ColegiadoDTO|mixed|null|string
   * @throws InfraException
   */
  protected function cadastrarControlado(ColegiadoDTO $objColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_cadastrar', __METHOD__, $objColegiadoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrNome($objColegiadoDTO, $objInfraException);
      $this->validarNumIdUsuarioSecretario($objColegiadoDTO, $objInfraException);
      $this->validarNumIdUsuarioPresidente($objColegiadoDTO, $objInfraException);
      $this->validarNumIdUnidadeResponsavel($objColegiadoDTO, $objInfraException);
      $this->validarNumIdTipoProcedimento($objColegiadoDTO, $objInfraException);
      $this->validarStrSigla($objColegiadoDTO, $objInfraException);
      $this->validarNumQuorumMinimo($objColegiadoDTO, $objInfraException);
      $this->validarStrStaAlgoritmoDistribuicao($objColegiadoDTO, $objInfraException);
      $this->validarStrSinAtivo($objColegiadoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoBD->cadastrar($objColegiadoDTO);

      //cadastrar vers�o do colegiado
      $objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
      $objColegiadoVersaoDTO->setNumIdColegiado($ret->getNumIdColegiado());
      $objColegiadoVersaoDTO->setDthVersao(InfraData::getStrDataHoraAtual());
      $objColegiadoVersaoDTO->setStrSinUltima('S');
      $objColegiadoVersaoDTO->setStrSinEditavel('S');
      $objColegiadoVersaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objColegiadoVersaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objColegiadoVersaoDTO->setNumIdTipoProcedimento($objColegiadoDTO->getNumIdTipoProcedimento());
      $objColegiadoVersaoDTO->setNumIdUsuarioSecretario($objColegiadoDTO->getNumIdUsuarioSecretario());
      $objColegiadoVersaoDTO->setNumIdUsuarioPresidente($objColegiadoDTO->getNumIdUsuarioPresidente());
      $objColegiadoVersaoDTO->setNumIdUnidadeResponsavel($objColegiadoDTO->getNumIdUnidadeResponsavel());
      $objColegiadoVersaoDTO->setStrStaAlgoritmoDistribuicao($objColegiadoDTO->getStrStaAlgoritmoDistribuicao());
      $objColegiadoVersaoDTO->setNumQuorumMinimo($objColegiadoDTO->getNumQuorumMinimo());
      $objColegiadoVersaoDTO->setStrNome($objColegiadoDTO->getStrNome());
      $objColegiadoVersaoDTO->setStrSigla($objColegiadoDTO->getStrSigla());
      $objColegiadoVersaoRN = new ColegiadoVersaoRN();
      $objColegiadoVersaoDTO = $objColegiadoVersaoRN->cadastrar($objColegiadoVersaoDTO);
      $idVersao = $objColegiadoVersaoDTO->getNumIdColegiadoVersao();
      //cadastrar composi��o do colegiado
      $arrObjColegiadoComposicaoDTO = $objColegiadoDTO->getArrObjColegiadoComposicaoDTO();
      //validar se existe unidade duplicada no colegiado
      $this->validarComposicaoColegiado($objColegiadoDTO, $objInfraException);
      if (InfraArray::contar($arrObjColegiadoComposicaoDTO)>0) {
        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
        foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
          $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($idVersao);
          $objColegiadoComposicaoDTO->setStrSinRodada('N');
          $objColegiadoComposicaoRN->cadastrar($objColegiadoComposicaoDTO);
        }
      }
      //cadastrar observadores do colegiado
      $arrObjRelColegiadoUsuarioDTO = $objColegiadoDTO->getArrObjRelColegiadoUsuarioDTO();
      if (InfraArray::contar($arrObjRelColegiadoUsuarioDTO)>0) {
        $arrIdUsuarios = InfraArray::converterArrInfraDTO($arrObjRelColegiadoUsuarioDTO, 'IdUsuario');
        $objUsuarioDTO = new UsuarioDTO();
        $objUsuarioRN = new UsuarioRN();
        $objUsuarioDTO->setStrStaTipo(UsuarioRN::$TU_EXTERNO);
        $objUsuarioDTO->setNumIdUsuario($arrIdUsuarios, InfraDTO::$OPER_IN);
        $objUsuarioDTO->retNumIdUsuario();
        $arrObjUsuarioDTO = $objUsuarioRN->listarRN0490($objUsuarioDTO);
        if (InfraArray::contar($arrObjUsuarioDTO)!==InfraArray::contar($arrIdUsuarios)) {
          $objInfraException->lancarValidacao('Observador externo inv�lido.');
        }
        $objRelColegiadoUsuarioRN = new RelColegiadoUsuarioRN();
        foreach ($arrObjRelColegiadoUsuarioDTO as $objRelColegiadoUsuarioDTO) {
          $objRelColegiadoUsuarioDTO->setNumIdColegiado($ret->getNumIdColegiado());
          $objRelColegiadoUsuarioRN->cadastrar($objRelColegiadoUsuarioDTO);
        }
      }
      //Auditoria
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_UNIDADES');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_SECRETARIAS');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC_' . $objColegiadoDTO->getNumIdColegiado());

      return $ret;

    } catch (Exception $e) {
      throw new InfraException('Erro cadastrando Colegiado.', $e);
    }
  }

  protected function alterarControlado(ColegiadoDTO $objColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_alterar', __METHOD__, $objColegiadoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objColegiadoDTOBanco = new ColegiadoDTO();
      $objColegiadoDTOBanco->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
      $objColegiadoDTOBanco->retTodos();
      $objColegiadoDTOBanco = $this->consultar($objColegiadoDTOBanco);
      $bolAlterarVersao = false;
      $bolGerarNovaVersao = false;

      //busca registros de composicao do colegiado
      $objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
      $objColegiadoVersaoRN = new ColegiadoVersaoRN();
      $objColegiadoVersaoDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
      $objColegiadoVersaoDTO->setStrSinUltima('S');
      $objColegiadoVersaoDTO->retTodos();
      $objColegiadoVersaoDTO = $objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);

      if ($objColegiadoDTO->isSetStrNome()) {
        $this->validarStrNome($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getStrNome()!==$objColegiadoDTOBanco->getStrNome()) {
          $objColegiadoVersaoDTO->setStrNome($objColegiadoDTO->getStrNome());
          $bolAlterarVersao = true;
          $bolGerarNovaVersao=true;
        }
      }

      if ($objColegiadoDTO->isSetNumIdUsuarioSecretario()) {
        $this->validarNumIdUsuarioSecretario($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getNumIdUsuarioSecretario()!=$objColegiadoDTOBanco->getNumIdUsuarioSecretario()) {
          $objColegiadoVersaoDTO->setNumIdUsuarioSecretario($objColegiadoDTO->getNumIdUsuarioSecretario());
          $bolAlterarVersao = true;
        }
      }

      if ($objColegiadoDTO->isSetNumIdUsuarioPresidente()) {
        $this->validarNumIdUsuarioPresidente($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getNumIdUsuarioPresidente()!=$objColegiadoDTOBanco->getNumIdUsuarioPresidente()) {
          $objColegiadoVersaoDTO->setNumIdUsuarioPresidente($objColegiadoDTO->getNumIdUsuarioPresidente());
          $bolAlterarVersao = true;
          $bolGerarNovaVersao=true;
        }
      }

      if ($objColegiadoDTO->isSetNumIdUnidadeResponsavel()) {
        $this->validarNumIdUnidadeResponsavel($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getNumIdUnidadeResponsavel()!=$objColegiadoDTOBanco->getNumIdUnidadeResponsavel()) {
          $objColegiadoVersaoDTO->setNumIdUnidadeResponsavel($objColegiadoDTO->getNumIdUnidadeResponsavel());
          $bolAlterarVersao = true;
          $bolGerarNovaVersao=true;
        }
      }

      if ($objColegiadoDTO->isSetNumIdTipoProcedimento()) {
        $this->validarNumIdTipoProcedimento($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getNumIdTipoProcedimento()!=$objColegiadoDTOBanco->getNumIdTipoProcedimento()) {
          $objColegiadoVersaoDTO->setNumIdTipoProcedimento($objColegiadoDTO->getNumIdTipoProcedimento());
          $bolAlterarVersao = true;
        }
      }
      if ($objColegiadoDTO->isSetStrSigla()) {
        $this->validarStrSigla($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getStrSigla()!=$objColegiadoDTOBanco->getStrSigla()) {
          $objColegiadoVersaoDTO->setStrSigla($objColegiadoDTO->getStrSigla());
          $bolAlterarVersao = true;
          $bolGerarNovaVersao=true;
        }
      }

      if ($objColegiadoDTO->isSetStrSinAtivo()) {
        $this->validarStrSinAtivo($objColegiadoDTO, $objInfraException);
      }

      if ($objColegiadoDTO->isSetNumQuorumMinimo()) {
        $this->validarNumQuorumMinimo($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getNumQuorumMinimo()!=$objColegiadoDTOBanco->getNumQuorumMinimo()) {
          $objColegiadoVersaoDTO->setNumQuorumMinimo($objColegiadoDTO->getNumQuorumMinimo());
          $bolAlterarVersao = true;
          $bolGerarNovaVersao=true;
        }
      }

      if ($objColegiadoDTO->isSetStrStaAlgoritmoDistribuicao()) {
        $this->validarStrStaAlgoritmoDistribuicao($objColegiadoDTO, $objInfraException);
        if ($objColegiadoDTO->getStrStaAlgoritmoDistribuicao()!=$objColegiadoDTOBanco->getStrStaAlgoritmoDistribuicao()) {
          $objColegiadoVersaoDTO->setStrStaAlgoritmoDistribuicao($objColegiadoDTO->getStrStaAlgoritmoDistribuicao());
          $bolAlterarVersao = true;
          $bolGerarNovaVersao=true;
        }
      }

      $objInfraException->lancarValidacoes();

      if ($bolAlterarVersao) {
        if (!$bolGerarNovaVersao || $objColegiadoVersaoDTO->getStrSinEditavel()==='S'){
          $objColegiadoVersaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
          $objColegiadoVersaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
          $objColegiadoVersaoRN->alterar($objColegiadoVersaoDTO);
        }
        else {
          $objColegiadoVersaoDTO2 = new ColegiadoVersaoDTO();
          $objColegiadoVersaoDTO2->setNumIdColegiadoVersao($objColegiadoVersaoDTO->getNumIdColegiadoVersao());
          $objColegiadoVersaoDTO2->setStrSinUltima('N');
          $objColegiadoVersaoDTO2->setStrSinEditavel('N');
          $objColegiadoVersaoRN->alterar($objColegiadoVersaoDTO2);

          $objColegiadoVersaoDTO->unSetNumIdColegiadoVersao();
          $objColegiadoVersaoDTO->setDthVersao(InfraData::getStrDataHoraAtual());
          $objColegiadoVersaoDTO->setStrSinUltima('S');
          $objColegiadoVersaoDTO->setStrSinEditavel('S');
          $objColegiadoVersaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
          $objColegiadoVersaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
          $objColegiadoVersaoDTO->setNumIdTipoProcedimento($objColegiadoDTO->getNumIdTipoProcedimento());
          $objColegiadoVersaoDTO->setNumIdUsuarioSecretario($objColegiadoDTO->getNumIdUsuarioSecretario());
          $objColegiadoVersaoDTO->setNumIdUsuarioPresidente($objColegiadoDTO->getNumIdUsuarioPresidente());
          $objColegiadoVersaoDTO->setNumIdUnidadeResponsavel($objColegiadoDTO->getNumIdUnidadeResponsavel());
          $objColegiadoVersaoDTO->setStrStaAlgoritmoDistribuicao($objColegiadoDTO->getStrStaAlgoritmoDistribuicao());
          $objColegiadoVersaoDTO->setNumQuorumMinimo($objColegiadoDTO->getNumQuorumMinimo());
          $objColegiadoVersaoDTO->setStrNome($objColegiadoDTO->getStrNome());
          $objColegiadoVersaoDTO->setStrSigla($objColegiadoDTO->getStrSigla());
          $objColegiadoVersaoDTO = $objColegiadoVersaoRN->cadastrar($objColegiadoVersaoDTO);

        }
      }


      if ($objColegiadoDTO->isSetArrObjColegiadoComposicaoDTO()) {

        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
        $arrObjColegiadoComposicaoDTOBanco = array();
        if ($objColegiadoVersaoDTO!==null) {
          $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();

          $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($objColegiadoVersaoDTO->getNumIdColegiadoVersao());
          $objColegiadoComposicaoDTO->retNumIdColegiadoComposicao();
          $objColegiadoComposicaoDTO->retNumIdColegiadoVersao();
          $objColegiadoComposicaoDTO->retNumIdTipoMembroColegiado();
          $objColegiadoComposicaoDTO->retNumOrdem();
          $objColegiadoComposicaoDTO->retDblPeso();
          $objColegiadoComposicaoDTO->retNumIdUnidade();
          $objColegiadoComposicaoDTO->retNumIdUsuario();
          $objColegiadoComposicaoDTO->retStrDescricaoUnidade();
          $objColegiadoComposicaoDTO->retStrSiglaUnidade();
          $objColegiadoComposicaoDTO->retStrNomeUsuario();
          $objColegiadoComposicaoDTO->retStrSiglaUsuario();
          $objColegiadoComposicaoDTO->retStrSinRodada();
          $objColegiadoComposicaoDTO->retNumIdCargo();
          $objColegiadoComposicaoDTO->retStrSinHabilitado();
          $arrObjColegiadoComposicaoDTOBanco = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
          $arrObjColegiadoComposicaoDTOBanco = InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTOBanco, 'IdUsuario');

        }
        //verifica se houve altera��o

        $arrObjColegiadoComposicaoDTO = $objColegiadoDTO->getArrObjColegiadoComposicaoDTO();

        $this->validarComposicaoColegiado($objColegiadoDTO, $objInfraException);

        $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'IdUsuario');
        $bolAlterarComposicao = false;

        $arrRemocao = array_diff_key($arrObjColegiadoComposicaoDTOBanco, $arrObjColegiadoComposicaoDTO);
        $arrInclusao = array();
        $arrAlteracao = array();
        $arrDesabilitados = array();


        foreach ($arrObjColegiadoComposicaoDTO as $idUsuario => $objColegiadoComposicaoDTOInformado) {
          //verifica se � membro novo
          if (!isset($arrObjColegiadoComposicaoDTOBanco[$idUsuario])) {
            $arrInclusao[$idUsuario] = $objColegiadoComposicaoDTOInformado;
            continue;
          }
          $objColegiadoComposicaoDTOInformado->setNumIdColegiadoComposicao($arrObjColegiadoComposicaoDTOBanco[$idUsuario]->getNumIdColegiadoComposicao());
          //se alterou unidade remove e inclui novamente.
          if ($objColegiadoComposicaoDTOInformado->getNumIdUnidade()!=$arrObjColegiadoComposicaoDTOBanco[$idUsuario]->getNumIdUnidade()) {
            $arrInclusao[$idUsuario] = $objColegiadoComposicaoDTOInformado;
            $arrRemocao[$idUsuario] = $objColegiadoComposicaoDTOInformado;
            continue;
          }
          //se alterou tipo de membro considera nova composi��o
          if ($objColegiadoComposicaoDTOInformado->getNumIdTipoMembroColegiado()!=$arrObjColegiadoComposicaoDTOBanco[$idUsuario]->getNumIdTipoMembroColegiado()) {
            $arrAlteracao[$idUsuario] = $objColegiadoComposicaoDTOInformado;
            $bolAlterarComposicao = true;
            continue;
          }
          //se n�o for titular verifica se foi desabilitado
          if ($objColegiadoComposicaoDTOInformado->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR &&
              $objColegiadoComposicaoDTOInformado->getStrSinHabilitado()!=$arrObjColegiadoComposicaoDTOBanco[$idUsuario]->getStrSinHabilitado()) {
            if ($objColegiadoComposicaoDTOInformado->getStrSinHabilitado()=='N') {
              $arrDesabilitados[] = $idUsuario;
            }
            $arrAlteracao[$idUsuario] = $objColegiadoComposicaoDTOInformado;
            continue;
          }
          //se possuir alguma outra altera��o, permanece no array
          if ($objColegiadoComposicaoDTOInformado->getNumOrdem()!=$arrObjColegiadoComposicaoDTOBanco[$idUsuario]->getNumOrdem() ||
              $objColegiadoComposicaoDTOInformado->getNumIdCargo()!=$arrObjColegiadoComposicaoDTOBanco[$idUsuario]->getNumIdCargo() ||
              InfraUtil::formatarDbl($objColegiadoComposicaoDTOInformado->getDblPeso())!=InfraUtil::formatarDbl($arrObjColegiadoComposicaoDTOBanco[$idUsuario]->getDblPeso())
          ) {
            $arrAlteracao[$idUsuario] = $objColegiadoComposicaoDTOInformado;
          }
        }

        if ($arrInclusao!=null || $arrRemocao!=null) {
          $bolAlterarComposicao = true;
        }


        if ($bolAlterarComposicao) {
          //validar se existe sess�o aberta deste colegiado
          $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
          $objSessaoJulgamentoDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
          $objSessaoJulgamentoDTO->setStrStaSituacao(array(SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_SUSPENSA), InfraDTO::$OPER_IN);
          $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
          $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
          if ($objSessaoJulgamentoRN->contar($objSessaoJulgamentoDTO)>0) {
            $objInfraException->lancarValidacao('Este colegiado possui sess�o em andamento, n�o � poss�vel alterar a composi��o.');
          }
        }

        if ($arrDesabilitados!=null) {
          $objPresencaSessaoRN = new PresencaSessaoRN();
          $objPresencaSessaoDTO = new PresencaSessaoDTO();
          $objPresencaSessaoDTO->setNumIdUsuario($arrDesabilitados, InfraDTO::$OPER_IN);
          $objPresencaSessaoDTO->setDthSaida(null);
          $objPresencaSessaoDTO->setNumIdColegiadoSessaoJulgamento($objColegiadoDTO->getNumIdColegiado());
          $objPresencaSessaoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_SUSPENSA), InfraDTO::$OPER_IN);
          if ($objPresencaSessaoRN->contar($objPresencaSessaoDTO)>0) {
            $objInfraException->lancarValidacao('N�o � poss�vel desabilitar membro presente em sess�o de julgamento n�o encerrada.');
          }
        }


        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC');
        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_UNIDADES');
        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_SECRETARIAS');
        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC_' . $objColegiadoDTO->getNumIdColegiado());


        //verifica necessiidadede gerar nova versao
        if ($bolAlterarComposicao) {
          if ($objColegiadoVersaoDTO->getStrSinEditavel()==='N') {
            //validar se membros a excluir n�o tem restri��es
            $objPresencaSessaoRN = new PresencaSessaoRN();
            $objPresencaSessaoDTO = new PresencaSessaoDTO();
            $objPresencaSessaoDTO->setNumIdColegiadoSessaoJulgamento($objColegiadoDTO->getNumIdColegiado());
            $objPresencaSessaoDTO->setDthSaida(null);
            $objPresencaSessaoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_CANCELADA, SessaoJulgamentoRN::$ES_ENCERRADA, SessaoJulgamentoRN::$ES_FINALIZADA), InfraDTO::$OPER_NOT_IN);
            $objPresencaSessaoDTO->retNumIdPresencaSessao();

            foreach ($arrRemocao as $objColegiadoComposicaoDTO) {
              //validar se membro est� presente em alguma sess�o n�o finalizada
              $objPresencaSessaoDTO->setNumIdUsuario($objColegiadoComposicaoDTO->getNumIdUsuario());
              if ($objPresencaSessaoRN->contar($objPresencaSessaoDTO)>0) {
                $objInfraException->adicionarValidacao('O usu�rio "' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . '" est� presente em uma sess�o n�o encerrada.');
                continue;
              }
              //validar se possui item em pauta /mesa ou referendo antes de excluir
              $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
              $objItemSessaoJulgamentoDTO->setNumIdUsuarioSessao($objColegiadoComposicaoDTO->getNumIdUsuario());
              $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_PAUTA_ABERTA), InfraDTO::$OPER_IN);
              $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
              $qtd = $objItemSessaoJulgamentoRN->contar($objItemSessaoJulgamentoDTO);
              if ($qtd>0) {
                $objInfraException->adicionarValidacao('O usu�rio "' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . '" possui ' . $qtd . ' itens patuados, em mesa ou para referendo.');
              }
            }
            $objInfraException->lancarValidacoes();

            //altera para n�o ser mais a �ltima
            $objColegiadoVersaoDTO2 = new ColegiadoVersaoDTO();
            $objColegiadoVersaoDTO2->setNumIdColegiadoVersao($objColegiadoVersaoDTO->getNumIdColegiadoVersao());
            $objColegiadoVersaoDTO2->setStrSinUltima('N');
            $objColegiadoVersaoDTO2->setStrSinEditavel('N');
            $objColegiadoVersaoRN->alterar($objColegiadoVersaoDTO2);
            //cria nova vers�o e seta como �ltima e editavel
            $objColegiadoVersaoDTO->unSetNumIdColegiadoVersao();
            $objColegiadoVersaoDTO->setDthVersao(InfraData::getStrDataHoraAtual());
            $objColegiadoVersaoDTO->setStrSinUltima('S');
            $objColegiadoVersaoDTO->setStrSinEditavel('S');
            $objColegiadoVersaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
            $objColegiadoVersaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
            $objColegiadoVersaoDTO = $objColegiadoVersaoRN->cadastrar($objColegiadoVersaoDTO);
            //cadastrar nova composi��o do colegiado - todos os registros
            foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
              $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($objColegiadoVersaoDTO->getNumIdColegiadoVersao());
              $objColegiadoComposicaoDTO->setStrSinRodada('N');
              $objColegiadoComposicaoRN->cadastrar($objColegiadoComposicaoDTO);
            }
            //zerar os contadores de distribui��es por usuario - algoritmo por peso
            $objAlgoritmoDTO = new AlgoritmoDTO();
            $objAlgoritmoRN = new AlgoritmoRN();
            $objAlgoritmoDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
            $objAlgoritmoDTO->setStrStaAlgoritmo(AlgoritmoRN::$ALG_PESO);
            $objAlgoritmoDTO->retNumIdAlgoritmo();
            $arrObjAlgoritmoDTO = $objAlgoritmoRN->listar($objAlgoritmoDTO);
            $objAlgoritmoRN->excluir($arrObjAlgoritmoDTO);
          } else {
            //exclui membros removidos
            $objColegiadoComposicaoRN->excluir(array_values($arrRemocao));

            //cadastrar nova composi��o do colegiado - novos membros
            foreach ($arrInclusao as $objColegiadoComposicaoDTO) {
              $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($objColegiadoVersaoDTO->getNumIdColegiadoVersao());
              $objColegiadoComposicaoDTO->setStrSinRodada('N');
              $objColegiadoComposicaoRN->cadastrar($objColegiadoComposicaoDTO);
            }
            //grava outras altera��es
            foreach ($arrAlteracao as $objColegiadoComposicaoDTO) {
              $objColegiadoComposicaoRN->alterar($objColegiadoComposicaoDTO);
            }
          }
        } else if ($arrAlteracao!=null) {
          foreach ($arrAlteracao as $objColegiadoComposicaoDTO) {
            $objColegiadoComposicaoRN->alterar($objColegiadoComposicaoDTO);
          }
        }
      }

      if ($objColegiadoDTO->isSetArrObjRelColegiadoUsuarioDTO()) {

        $arrObjRelColegiadoUsuarioDTO = $objColegiadoDTO->getArrObjRelColegiadoUsuarioDTO();
        $arrIdUsuarios = InfraArray::converterArrInfraDTO($arrObjRelColegiadoUsuarioDTO, 'IdUsuario');
        //busca registros de composicao do colegiado
        $objRelColegiadoUsuarioDTO = new RelColegiadoUsuarioDTO();
        $objRelColegiadoUsuarioDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
        $objRelColegiadoUsuarioDTO->retNumIdUsuario();
        $objRelColegiadoUsuarioRN = new RelColegiadoUsuarioRN();
        $arrObjRelColegiadoUsuarioDTOBanco = $objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO);
        $arrIdUsuariosBanco = InfraArray::converterArrInfraDTO($arrObjRelColegiadoUsuarioDTOBanco, 'IdUsuario');

        //verifica se houve altera��o
        $arrInclusao = array_diff($arrIdUsuarios, $arrIdUsuariosBanco);
        $arrRemocao = array_diff($arrIdUsuariosBanco, $arrIdUsuarios);

        if ($arrRemocao!=null) {
          $objRelColegiadoUsuarioDTO = new RelColegiadoUsuarioDTO();
          $objRelColegiadoUsuarioDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
          $objRelColegiadoUsuarioDTO->setNumIdUsuario($arrRemocao, InfraDTO::$OPER_IN);
          $objRelColegiadoUsuarioDTO->retTodos();
          $objRelColegiadoUsuarioRN->excluir($objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO));
        }

        //cadastrar observadores do colegiado
        if ($arrInclusao!=null) {
          $objUsuarioDTO = new UsuarioDTO();
          $objUsuarioRN = new UsuarioRN();
          $objUsuarioDTO->setStrStaTipo(UsuarioRN::$TU_EXTERNO);
          $objUsuarioDTO->setNumIdUsuario($arrInclusao, InfraDTO::$OPER_IN);
          $objUsuarioDTO->retTodos();
          $arrObjUsuarioDTO = $objUsuarioRN->listarRN0490($objUsuarioDTO);
          if (InfraArray::contar($arrObjUsuarioDTO)!==InfraArray::contar($arrInclusao)) {
            $objInfraException->lancarValidacao('Observador externo inv�lido.');
          }
          $objRelColegiadoUsuarioRN = new RelColegiadoUsuarioRN();
          foreach ($arrInclusao as $idUsuario) {
            $objRelColegiadoUsuarioDTO = new RelColegiadoUsuarioDTO();
            $objRelColegiadoUsuarioDTO->setNumIdUsuario($idUsuario);
            $objRelColegiadoUsuarioDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
            $objRelColegiadoUsuarioRN->cadastrar($objRelColegiadoUsuarioDTO);
          }
        }
      }

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      $objColegiadoBD->alterar($objColegiadoDTO);
      //Auditoria

    } catch (Exception $e) {
      throw new InfraException('Erro alterando Colegiado.', $e);
    }
  }

  protected function excluirControlado($arrObjColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_excluir', __METHOD__, $arrObjColegiadoDTO);


      ////

      $objInfraException = new InfraException();

      $objDistribuicaoRN = new DistribuicaoRN();

      foreach ($arrObjColegiadoDTO as $objColegiadoDTO) {

        $objDistribuicaoDTO = new DistribuicaoDTO();
        $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($objColegiadoDTO->getNumIdColegiado());

        if ($objDistribuicaoRN->contar($objDistribuicaoDTO)>0) {

          $objColegiadoDTOBanco = new ColegiadoDTO();
          $objColegiadoDTOBanco->retStrNome();
          $objColegiadoDTOBanco->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
          $objColegiadoDTOBanco->setBolExclusaoLogica(false);
          $objColegiadoDTOBanco = $this->consultar($objColegiadoDTOBanco);

          $objInfraException->adicionarValidacao('Colegiado "' . $objColegiadoDTOBanco->getStrNome() . '" possui distribui��o.');
        }
      }

      $objInfraException->lancarValidacoes();

      $objColegiadoVersaoRN = new ColegiadoVersaoRN();
      $objRelMotivoDistrColegiadoRN = new RelMotivoDistrColegiadoRN();
      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      $objRelColegiadoUsuarioRN = new RelColegiadoUsuarioRN();

      $objRelColegiadoUsuarioDTO = new RelColegiadoUsuarioDTO();
      $objRelColegiadoUsuarioDTO->retTodos();

      $objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
      $objColegiadoVersaoDTO->retNumIdColegiadoVersao();

      $objMotivoDistribuicaoDTO = new MotivoDistribuicaoDTO();
      $objMotivoDistribuicaoDTO->retNumIdMotivoDistribuicao();

      foreach ($arrObjColegiadoDTO as $objColegiadoDTO) {

        $objRelColegiadoUsuarioDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
        $objRelColegiadoUsuarioRN->excluir($objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO));

        $objColegiadoVersaoDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
        $objColegiadoVersaoRN->excluir($objColegiadoVersaoRN->listar($objColegiadoVersaoDTO));

        $objRelMotivoDistrColegiadoDTO=new RelMotivoDistrColegiadoDTO();
        $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
        $objRelMotivoDistrColegiadoDTO->retNumIdColegiado();
        $objRelMotivoDistrColegiadoDTO->retNumIdMotivoDistribuicao();
        $arrObjRelMotivoDistrColegiadoDTO = $objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO);
        if ($arrObjRelMotivoDistrColegiadoDTO!==null) {
          $objRelMotivoDistrColegiadoRN->excluir($arrObjRelMotivoDistrColegiadoDTO);
        }
        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC_' . $objColegiadoDTO->getNumIdColegiado());
        $objColegiadoBD->excluir($objColegiadoDTO);
      }
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_UNIDADES');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_SECRETARIAS');
      //Auditoria

    } catch (Exception $e) {
      throw new InfraException('Erro excluindo Colegiado.', $e);
    }
  }

  protected function consultarConectado(ColegiadoDTO $objColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_consultar', __METHOD__, $objColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoBD->consultar($objColegiadoDTO);

      //Auditoria

      return $ret;
    } catch (Exception $e) {
      throw new InfraException('Erro consultando Colegiado.', $e);
    }
  }

  protected function consultarCompletoConectado(ColegiadoDTO $objColegiadoDTO)
  {
    try {

      $objColegiadoDTO->retTodos(true);
      $ret = $this->consultar($objColegiadoDTO);

      if ($ret!=null) {
        $objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
        $objColegiadoVersaoRN = new ColegiadoVersaoRN();
        $objColegiadoVersaoDTO->setNumIdColegiado($ret->getNumIdColegiado());

        if ($objColegiadoDTO->isSetNumIdColegiadoVersao()) {
          $objColegiadoVersaoDTO->setNumIdColegiadoVersao($objColegiadoDTO->getNumIdColegiadoVersao());
        } else {
          $objColegiadoVersaoDTO->setStrSinUltima('S');
        }
        $objColegiadoVersaoDTO->retTodos(true);
        $objColegiadoVersaoDTO = $objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);
        if ($objColegiadoVersaoDTO===null) {
          return null;
        }
        $versao = $objColegiadoVersaoDTO->getNumIdColegiadoVersao();
        $ret->setDthCriacaoVersao($objColegiadoVersaoDTO->getDthVersao());
        $ret->setStrNome($objColegiadoVersaoDTO->getStrNome());
        $ret->setStrSigla($objColegiadoVersaoDTO->getStrSigla());
        $ret->setNumIdTipoProcedimento($objColegiadoVersaoDTO->getNumIdTipoProcedimento());
        $ret->setNumIdUnidadeResponsavel($objColegiadoVersaoDTO->getNumIdUnidadeResponsavel());
        $ret->setNumIdUsuarioPresidente($objColegiadoVersaoDTO->getNumIdUsuarioPresidente());
        $ret->setNumIdUsuarioSecretario($objColegiadoVersaoDTO->getNumIdUsuarioSecretario());
        $ret->setStrNomeSecretario($objColegiadoVersaoDTO->getStrNomeSecretario());
        $ret->setStrStaAlgoritmoDistribuicao($objColegiadoVersaoDTO->getStrStaAlgoritmoDistribuicao());

        $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
        $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($versao);
        $objColegiadoComposicaoDTO->retNumIdColegiadoComposicao();
        $objColegiadoComposicaoDTO->retNumIdColegiadoVersao();
        $objColegiadoComposicaoDTO->retNumIdUnidade();
        $objColegiadoComposicaoDTO->retNumIdUsuario();
        $objColegiadoComposicaoDTO->retStrDescricaoUnidade();
        $objColegiadoComposicaoDTO->retStrSiglaUnidade();
        $objColegiadoComposicaoDTO->retNumOrdem();
        $objColegiadoComposicaoDTO->retDblPeso();
        $objColegiadoComposicaoDTO->retStrNomeUsuario();
        $objColegiadoComposicaoDTO->retStrSiglaUsuario();
        $objColegiadoComposicaoDTO->retNumIdTipoMembroColegiado();
        $objColegiadoComposicaoDTO->retStrNomeTipoMembroColegiado();
        $objColegiadoComposicaoDTO->retStrExpressaoCargo();
        $objColegiadoComposicaoDTO->retNumIdCargo();
        $objColegiadoComposicaoDTO->retStrSinHabilitado();

        $objColegiadoComposicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_DESC);

        $arrColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
        $ret->setArrObjColegiadoComposicaoDTO($arrColegiadoComposicaoDTO);

        $objRelColegiadoUsuarioDTO = new RelColegiadoUsuarioDTO();
        $objRelColegiadoUsuarioRN = new RelColegiadoUsuarioRN();
        $objRelColegiadoUsuarioDTO->setNumIdColegiado($ret->getNumIdColegiado());
        $objRelColegiadoUsuarioDTO->retTodos(true);
        $arrObjRelColegiadoUsuarioDTO = $objRelColegiadoUsuarioRN->listar($objRelColegiadoUsuarioDTO);
        $ret->setArrObjRelColegiadoUsuarioDTO($arrObjRelColegiadoUsuarioDTO);
      }

      //Auditoria

      return $ret;
    } catch (Exception $e) {
      throw new InfraException('Erro consultando Colegiado.', $e);
    }
  }

  protected function listarConectado(ColegiadoDTO $objColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_listar', __METHOD__, $objColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoBD->listar($objColegiadoDTO);

      //Auditoria

      return $ret;

    } catch (Exception $e) {
      throw new InfraException('Erro listando Colegiados.', $e);
    }
  }

  protected function contarConectado(ColegiadoDTO $objColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_listar', __METHOD__, $objColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoBD->contar($objColegiadoDTO);

      //Auditoria

      return $ret;
    } catch (Exception $e) {
      throw new InfraException('Erro contando Colegiados.', $e);
    }
  }

  protected function desativarControlado($arrObjColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_desativar', __METHOD__, $arrObjColegiadoDTO);

      $objInfraException = new InfraException();

      $objDistribuicaoRN = new DistribuicaoRN();

      foreach ($arrObjColegiadoDTO as $objColegiadoDTO) {

        $objDistribuicaoDTO = new DistribuicaoDTO();
        $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($objColegiadoDTO->getNumIdColegiado());
        $objDistribuicaoDTO->setStrStaDistribuicao(array(DistribuicaoRN::$STA_JULGADO, DistribuicaoRN::$STA_CANCELADO), InfraDTO::$OPER_NOT_IN);
        $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);

        if ($objDistribuicaoRN->contar($objDistribuicaoDTO)>0) {

          $objColegiadoDTOBanco = new ColegiadoDTO();
          $objColegiadoDTOBanco->retStrNome();
          $objColegiadoDTOBanco->setNumIdColegiado($objColegiadoDTO->getNumIdColegiado());
          $objColegiadoDTOBanco = $this->consultar($objColegiadoDTOBanco);

          $objInfraException->adicionarValidacao('Colegiado "' . $objColegiadoDTOBanco->getStrNome() . '" possui distribui��o n�o julgada.');
        }
      }

      $objInfraException->lancarValidacoes();

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      foreach ($arrObjColegiadoDTO as $objColegiadoDTO) {
        $objColegiadoBD->desativar($objColegiadoDTO);
        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC_' . $objColegiadoDTO->getNumIdColegiado());
      }
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_UNIDADES');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_SECRETARIAS');


      //Auditoria

    } catch (Exception $e) {
      throw new InfraException('Erro desativando Colegiado.', $e);
    }
  }

  protected function reativarControlado($arrObjColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_reativar', __METHOD__, $arrObjColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      foreach ($arrObjColegiadoDTO as $objColegiadoDTO) {
        $objColegiadoBD->reativar($objColegiadoDTO);
        CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC_' . $objColegiadoDTO->getNumIdColegiado());
      }
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_UC');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_UNIDADES');
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_COLEGIADOS_SECRETARIAS');

      //Auditoria

    } catch (Exception $e) {
      throw new InfraException('Erro reativando Colegiado.', $e);
    }
  }

  protected function bloquearControlado(ColegiadoDTO $objColegiadoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_consultar', __METHOD__, $objColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoBD = new ColegiadoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoBD->bloquear($objColegiadoDTO);

      //Auditoria

      return $ret;
    } catch (Exception $e) {
      throw new InfraException('Erro bloqueando Colegiado.', $e);
    }
  }

  /**
   * @param ColegiadoDTO|null $objColegiadoDTO
   * @return bool
   * @throws InfraException
   */
  protected function verificaUnidadeAtualConectado($objColegiadoDTO): bool
  {

    try {

      $strCache = 'TRF4_SJ_UC';
      if ($objColegiadoDTO!==null && $objColegiadoDTO->isSetNumIdColegiado()) {
        $strCache .= '_' . $objColegiadoDTO->getNumIdColegiado();
      }
      $arrCache = CacheSEI::getInstance()->getAtributo($strCache);

      if ($arrCache==null) {
        $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
        if ($objColegiadoDTO!==null && $objColegiadoDTO->isSetNumIdColegiado()) {
          $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objColegiadoDTO->getNumIdColegiado());
        }
        $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
        $objColegiadoComposicaoDTO->retNumIdUnidade();
        $objColegiadoComposicaoDTO->setDistinct(true);
        $objColegiadoComposicaoDTO->adicionarCriterio(array('IdTipoMembroColegiado','SinHabilitado'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array(TipoMembroColegiadoRN::$TMC_TITULAR,'S'),InfraDTO::$OPER_LOGICO_OR);
        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();

        $arrObjColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);

        $arrCache = array();

        foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
          $arrCache[] = $objColegiadoComposicaoDTO->getNumIdUnidade();
        }

        CacheSEI::getInstance()->setAtributo($strCache, $arrCache, CacheSEI::getInstance()->getNumTempo());
      }

      return in_array(SessaoSEI::getInstance()->getNumIdUnidadeAtual(), $arrCache);

    } catch (Exception $e) {
      throw new InfraException('Erro consultando unidades dos colegiados.', $e);
    }

  }

  public function listarColegiadosUnidadeAtual()
  {

    try {

      $idUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();

      $strCache = 'TRF4_SJ_COLEGIADOS_UNIDADES';
      $arrCache = CacheSEI::getInstance()->getAtributo($strCache);

      if ($arrCache==null) {
        $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
        $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
        $objColegiadoComposicaoDTO->retNumIdUnidade();
        $objColegiadoComposicaoDTO->setDistinct(true);
        $objColegiadoComposicaoDTO->adicionarCriterio(array('IdTipoMembroColegiado','SinHabilitado'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array(TipoMembroColegiadoRN::$TMC_TITULAR,'S'),InfraDTO::$OPER_LOGICO_OR);
        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();

        $arrObjColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
        $arrCache = array();

        if (InfraArray::contar($arrObjColegiadoComposicaoDTO)) {
          $arrCache = InfraArray::converterArrInfraDTO($arrObjColegiadoComposicaoDTO, 'IdColegiadoColegiadoVersao', 'IdUnidade', true);
        }
        CacheSEI::getInstance()->setAtributo($strCache, $arrCache, CacheSEI::getInstance()->getNumTempo());
      }

      return ($arrCache[$idUnidadeAtual] ?? array());

    } catch (Exception $e) {
      throw new InfraException('Erro consultando unidades dos colegiados.', $e);
    }

  }

  /**
   * Lista os colegiados que a unidade atual � respons�vel
   * @return array
   * @throws InfraException
   */
  public function listarColegiadosAdministrados(): array
  {
    try {

      $idUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();

      $strCache = 'TRF4_SJ_COLEGIADOS_SECRETARIAS';
      $arrCache = CacheSEI::getInstance()->getAtributo($strCache);

      if ($arrCache==null) {
        $objColegiadoDTO=new ColegiadoDTO();
        $objColegiadoDTO->retNumIdColegiado();
        $objColegiadoDTO->retNumIdUnidadeResponsavel();
        $arrObjColegiadoDTO=$this->listar($objColegiadoDTO);

        $arrCache = array();

        if (InfraArray::contar($arrObjColegiadoDTO)) {
          $arrCache = InfraArray::converterArrInfraDTO($arrObjColegiadoDTO,  'IdColegiado','IdUnidadeResponsavel',true);
        }
        CacheSEI::getInstance()->setAtributo($strCache, $arrCache, CacheSEI::getInstance()->getNumTempo());
      }

      return ($arrCache[$idUnidadeAtual] ?? array());

    } catch (Exception $e) {
      throw new InfraException('Erro consultando unidades respons�veis pelos colegiados.', $e);
    }

  }

  /**
   * @param ColegiadoDTO $objColegiadoDTO
   * @param $objInfraException
   */
  private function validarComposicaoColegiado(ColegiadoDTO $objColegiadoDTO, $objInfraException)
  {
    $arrObjColegiadoComposicaoDTO=$objColegiadoDTO->getArrObjColegiadoComposicaoDTO();
    $staAlgoritmo=$objColegiadoDTO->getStrStaAlgoritmoDistribuicao();
    $dblPesoTotal=0;
    $arrIdUnidades=array();

    /** @var ColegiadoComposicaoDTO $objColegiadoComposicaoDTO */
    foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
      $dblPeso=(double)InfraUtil::prepararDbl($objColegiadoComposicaoDTO->getDblPeso());
      $dblPesoTotal+=$dblPeso;
      if($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR){
        if($dblPeso!=0.0 && $dblPeso!=1.0 && $staAlgoritmo!==AlgoritmoRN::$ALG_PESO){
          $objInfraException->lancarValidacao('Peso s� pode ser 0 ou 1 para este algoritmo.');
        }
      } else if($dblPeso!=0.0){
        $objInfraException->lancarValidacao('Suplentes e Eventuais devem ter peso 0.');
      }
      $numIdUnidade=$objColegiadoComposicaoDTO->getNumIdUnidade();
      if(isset($arrIdUnidades[$numIdUnidade])){
        $objInfraException->lancarValidacao('N�o s�o permitidos v�rios Membros na mesma Unidade.');
      }
      $arrIdUnidades[$numIdUnidade]=1;
    }
    if($dblPesoTotal==0.0){
      $objInfraException->lancarValidacao('Nenhum membro possui peso para receber distribui��o.');
    }


  }

}

?>