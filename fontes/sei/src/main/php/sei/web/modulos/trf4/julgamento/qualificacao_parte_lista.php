<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4ª REGIÃO
*
* 09/08/2017 - criado por bcu
*
* Versão do Gerador de Código: 1.39.0
*/

try {
  require_once __DIR__ .'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('qualificacao_parte_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  switch($_GET['acao']){
    case 'qualificacao_parte_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjQualificacaoParteDTO = array();
        for ($i=0, $iMax = InfraArray::contar($arrStrIds); $i< $iMax; $i++){
          $objQualificacaoParteDTO = new QualificacaoParteDTO();
          $objQualificacaoParteDTO->setNumIdQualificacaoParte($arrStrIds[$i]);
          $arrObjQualificacaoParteDTO[] = $objQualificacaoParteDTO;
        }
        $objQualificacaoParteRN = new QualificacaoParteRN();
        $objQualificacaoParteRN->excluir($arrObjQualificacaoParteDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;


    case 'qualificacao_parte_desativar':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjQualificacaoParteDTO = array();
        for ($i=0, $iMax = InfraArray::contar($arrStrIds); $i< $iMax; $i++){
          $objQualificacaoParteDTO = new QualificacaoParteDTO();
          $objQualificacaoParteDTO->setNumIdQualificacaoParte($arrStrIds[$i]);
          $arrObjQualificacaoParteDTO[] = $objQualificacaoParteDTO;
        }
        $objQualificacaoParteRN = new QualificacaoParteRN();
        $objQualificacaoParteRN->desativar($arrObjQualificacaoParteDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

    case 'qualificacao_parte_reativar':
      $strTitulo = 'Reativar Qualificações';
      if ($_GET['acao_confirmada']=='sim'){
        try{
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjQualificacaoParteDTO = array();
          for ($i=0, $iMax = InfraArray::contar($arrStrIds); $i< $iMax; $i++){
            $objQualificacaoParteDTO = new QualificacaoParteDTO();
            $objQualificacaoParteDTO->setNumIdQualificacaoParte($arrStrIds[$i]);
            $arrObjQualificacaoParteDTO[] = $objQualificacaoParteDTO;
          }
          $objQualificacaoParteRN = new QualificacaoParteRN();
          $objQualificacaoParteRN->reativar($arrObjQualificacaoParteDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        } 
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
        die;
      } 
      break;


    case 'qualificacao_parte_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Qualificação','Selecionar Qualificações');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='qualificacao_parte_cadastrar'){
        if (isset($_GET['id_qualificacao_parte'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_qualificacao_parte']);
        }
      }
      break;

    case 'qualificacao_parte_listar':
      $strTitulo = 'Qualificações';
      break;

    default:
      throw new InfraException("Ação '".$_GET['acao']."' não reconhecida.");
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'qualificacao_parte_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  if ($_GET['acao'] == 'qualificacao_parte_listar' || $_GET['acao'] == 'qualificacao_parte_selecionar'){
    $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_cadastrar');
    if ($bolAcaoCadastrar){
      $arrComandos[] = '<button type="button" accesskey="N" id="btnNova" value="Nova" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=qualificacao_parte_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ova</button>';
    }
  }

  $objQualificacaoParteDTO = new QualificacaoParteDTO();
  $objQualificacaoParteDTO->retNumIdQualificacaoParte();
  $objQualificacaoParteDTO->retStrDescricao();

    $objQualificacaoParteDTO->setBolExclusaoLogica(false);
    $objQualificacaoParteDTO->retStrSinAtivo();

  PaginaSEI::getInstance()->prepararOrdenacao($objQualificacaoParteDTO, 'Descricao', InfraDTO::$TIPO_ORDENACAO_ASC);

  $objQualificacaoParteRN = new QualificacaoParteRN();
  $arrObjQualificacaoParteDTO = $objQualificacaoParteRN->listar($objQualificacaoParteDTO);

  $numRegistros = InfraArray::contar($arrObjQualificacaoParteDTO);

  if ($numRegistros > 0){

    $bolCheck = false;

    if ($_GET['acao']=='qualificacao_parte_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_alterar');
      $bolAcaoImprimir = false;
      //$bolAcaoGerarPlanilha = false;
      $bolAcaoExcluir = false;
      $bolAcaoDesativar = false;
      $bolCheck = true;
    }else{
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_alterar');
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_excluir');
      $bolAcaoDesativar = SessaoSEI::getInstance()->verificarPermissao('qualificacao_parte_desativar');
    }

    
    if ($bolAcaoDesativar){
//      $bolCheck = true;
//      $arrComandos[] = '<button type="button" accesskey="t" id="btnDesativar" value="Desativar" onclick="acaoDesativacaoMultipla();" class="infraButton">Desa<span class="infraTeclaAtalho">t</span>ivar</button>';
      $strLinkDesativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=qualificacao_parte_desativar&acao_origem='.$_GET['acao']);
    }

    if ($bolAcaoReativar){
//      $bolCheck = true;
//      $arrComandos[] = '<button type="button" accesskey="R" id="btnReativar" value="Reativar" onclick="acaoReativacaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">R</span>eativar</button>';
      $strLinkReativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=qualificacao_parte_reativar&acao_origem='.$_GET['acao'].'&acao_confirmada=sim');
    }
    

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=qualificacao_parte_excluir&acao_origem='.$_GET['acao']);
    }

    /*
    if ($bolAcaoGerarPlanilha){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="P" id="btnGerarPlanilha" value="Gerar Planilha" onclick="infraGerarPlanilhaTabela(\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=infra_gerar_planilha_tabela').'\');" class="infraButton">Gerar <span class="infraTeclaAtalho">P</span>lanilha</button>';
    }
    */

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Qualificações.';
    $strCaptionTabela = 'Qualificações';

    $strResultado .= '<table width="70%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objQualificacaoParteDTO,'Descrição','Descricao',$arrObjQualificacaoParteDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">Ações</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      if ($arrObjQualificacaoParteDTO[$i]->getStrSinAtivo()=='S'){
      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      }else{
        $strCssTr = '<tr class="trVermelha">';
      }

      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjQualificacaoParteDTO[$i]->getNumIdQualificacaoParte(),$arrObjQualificacaoParteDTO[$i]->getStrDescricao()).'</td>';
      }
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjQualificacaoParteDTO[$i]->getStrDescricao()).'</td>';
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjQualificacaoParteDTO[$i]->getNumIdQualificacaoParte());


      if ($bolAcaoAlterar && $arrObjQualificacaoParteDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=qualificacao_parte_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_qualificacao_parte='.$arrObjQualificacaoParteDTO[$i]->getNumIdQualificacaoParte()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Qualificação" alt="Alterar Qualificação" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoDesativar || $bolAcaoReativar || $bolAcaoExcluir){
        $strId = $arrObjQualificacaoParteDTO[$i]->getNumIdQualificacaoParte();
        $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjQualificacaoParteDTO[$i]->getStrDescricao());
      }

      if ($bolAcaoDesativar && $arrObjQualificacaoParteDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoDesativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeDesativar().'" title="Desativar Qualificação" alt="Desativar Qualificação" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoReativar && $arrObjQualificacaoParteDTO[$i]->getStrSinAtivo()=='N'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoReativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeReativar().'" title="Reativar Qualificação" alt="Reativar Qualificação" class="infraImg" /></a>&nbsp;';
      }


      if ($bolAcaoExcluir){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoExcluir(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Qualificação" alt="Excluir Qualificação" class="infraImg" /></a>&nbsp;';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'qualificacao_parte_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='qualificacao_parte_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('btnFechar').focus();
  }
  infraEfeitoTabelas();
}

<? if ($bolAcaoDesativar){ ?>
function acaoDesativar(id,desc){
  if (confirm("Confirma desativação da Qualificação \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmQualificacaoParteLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmQualificacaoParteLista').submit();
  }
}

function acaoDesativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhuma Qualificação selecionada.');
    return;
  }
  if (confirm("Confirma desativação das Qualificações selecionadas?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmQualificacaoParteLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmQualificacaoParteLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoReativar){ ?>
function acaoReativar(id,desc){
  if (confirm("Confirma reativação da Qualificação \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmQualificacaoParteLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmQualificacaoParteLista').submit();
  }
}

function acaoReativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhuma Qualificação selecionada.');
    return;
  }
  if (confirm("Confirma reativação das Qualificações selecionadas?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmQualificacaoParteLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmQualificacaoParteLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclusão da Qualificação \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmQualificacaoParteLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmQualificacaoParteLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhuma Qualificação selecionada.');
    return;
  }
  if (confirm("Confirma exclusão das Qualificações selecionadas?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmQualificacaoParteLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmQualificacaoParteLista').submit();
  }
}
<? } ?>

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmQualificacaoParteLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  //PaginaSEI::getInstance()->abrirAreaDados('5em');
  //PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>