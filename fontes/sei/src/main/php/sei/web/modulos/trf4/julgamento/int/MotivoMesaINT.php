<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 07/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MotivoMesaINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objMotivoMesaDTO = new MotivoMesaDTO();
    $objMotivoMesaDTO->retNumIdMotivoMesa();
    $objMotivoMesaDTO->retStrDescricao();

    if ($strValorItemSelecionado!=null){
      $objMotivoMesaDTO->setBolExclusaoLogica(false);
      $objMotivoMesaDTO->adicionarCriterio(array('SinAtivo','IdMotivoMesa'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objMotivoMesaDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objMotivoMesaRN = new MotivoMesaRN();
    $arrObjMotivoMesaDTO = $objMotivoMesaRN->listar($objMotivoMesaDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjMotivoMesaDTO, 'IdMotivoMesa', 'Descricao');
  }
}
?>