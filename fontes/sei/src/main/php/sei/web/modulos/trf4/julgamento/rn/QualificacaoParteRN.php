<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class QualificacaoParteRN extends InfraRN {

  public static $TP_INTERESSADO=1;
  public static $TP_ADVOGADO=4;

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrDescricao(QualificacaoParteDTO $objQualificacaoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objQualificacaoParteDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('Descri��o n�o informada.');
    }else{
      $objQualificacaoParteDTO->setStrDescricao(trim($objQualificacaoParteDTO->getStrDescricao()));

      if (strlen($objQualificacaoParteDTO->getStrDescricao())>100){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 100 caracteres.');
      }
      $dto = new QualificacaoParteDTO();
      $dto->setBolExclusaoLogica(false);
      $dto->retStrSinAtivo();
      $dto->setNumIdQualificacaoParte($objQualificacaoParteDTO->getNumIdQualificacaoParte(),InfraDTO::$OPER_DIFERENTE);
      $dto->setStrDescricao($objQualificacaoParteDTO->getStrDescricao());
      $dto = $this->consultar($dto);

      if ($dto!=null) {
        if ($dto->getStrSinAtivo()=='S') {
          $objInfraException->adicionarValidacao('Existe outra ocorr�ncia com esta Descri��o.');
        } else {
          $objInfraException->adicionarValidacao('Existe ocorr�ncia inativa com esta Descri��o.');
        }
      }
    }
   
  }

  private function validarStrSinAtivo(QualificacaoParteDTO $objQualificacaoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objQualificacaoParteDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objQualificacaoParteDTO->getStrSinAtivo())){
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }

  protected function cadastrarControlado(QualificacaoParteDTO $objQualificacaoParteDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_cadastrar',__METHOD__,$objQualificacaoParteDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objQualificacaoParteDTO, $objInfraException);
      $this->validarStrSinAtivo($objQualificacaoParteDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      $ret = $objQualificacaoParteBD->cadastrar($objQualificacaoParteDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Qualifica��o.',$e);
    }
  }

  protected function alterarControlado(QualificacaoParteDTO $objQualificacaoParteDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_alterar',__METHOD__,$objQualificacaoParteDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objQualificacaoParteDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objQualificacaoParteDTO, $objInfraException);
      }
      if ($objQualificacaoParteDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objQualificacaoParteDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      $objQualificacaoParteBD->alterar($objQualificacaoParteDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Qualifica��o.',$e);
    }
  }

  protected function excluirControlado($arrObjQualificacaoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_excluir',__METHOD__,$arrObjQualificacaoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjQualificacaoParteDTO); $i< $iMax; $i++){
        $objQualificacaoParteBD->excluir($arrObjQualificacaoParteDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Qualifica��o.',$e);
    }
  }

  protected function consultarConectado(QualificacaoParteDTO $objQualificacaoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_consultar',__METHOD__,$objQualificacaoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      $ret = $objQualificacaoParteBD->consultar($objQualificacaoParteDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Qualifica��o.',$e);
    }
  }

  protected function listarConectado(QualificacaoParteDTO $objQualificacaoParteDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_listar',__METHOD__,$objQualificacaoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      $ret = $objQualificacaoParteBD->listar($objQualificacaoParteDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Qualifica��es.',$e);
    }
  }

  protected function contarConectado(QualificacaoParteDTO $objQualificacaoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_listar',__METHOD__,$objQualificacaoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      $ret = $objQualificacaoParteBD->contar($objQualificacaoParteDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Qualifica��es.',$e);
    }
  }

  protected function desativarControlado($arrObjQualificacaoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_desativar',__METHOD__,$arrObjQualificacaoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjQualificacaoParteDTO); $i< $iMax; $i++){
        $objQualificacaoParteBD->desativar($arrObjQualificacaoParteDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Qualifica��o.',$e);
    }
  }

  protected function reativarControlado($arrObjQualificacaoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_reativar',__METHOD__,$arrObjQualificacaoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjQualificacaoParteDTO); $i< $iMax; $i++){
        $objQualificacaoParteBD->reativar($arrObjQualificacaoParteDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Qualifica��o.',$e);
    }
  }

  protected function bloquearControlado(QualificacaoParteDTO $objQualificacaoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_consultar',__METHOD__,$objQualificacaoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objQualificacaoParteBD = new QualificacaoParteBD($this->getObjInfraIBanco());
      $ret = $objQualificacaoParteBD->bloquear($objQualificacaoParteDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Qualifica��o.',$e);
    }
  }


}
?>