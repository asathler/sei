<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 23/03/2015 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.33.1
 *
 * Vers�o no CVS: $Id$
 */

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('motivo_distribuicao_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->salvarCamposPost(array('selStaTipo'));

  $objMotivoDistribuicaoDTO = new MotivoDistribuicaoDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'motivo_distribuicao_cadastrar':
      $strTitulo = 'Novo Motivo de Distribui��o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarMotivoDistribuicao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao(null);
      $objMotivoDistribuicaoDTO->setStrDescricao($_POST['txtDescricao']);
      $strStaTipo = PaginaSEI::getInstance()->recuperarCampo('selStaTipo');
      if ($strStaTipo!==''){
        $objMotivoDistribuicaoDTO->setStrStaTipo($strStaTipo);
      }else{
        $objMotivoDistribuicaoDTO->setStrStaTipo(null);
      }

      $arrColegiados = PaginaSEI::getInstance()->getArrValuesSelect($_POST['hdnColegiados']);
      $arrObjRelMotivoDistrColegiadoDTO = array();
      foreach($arrColegiados as $Colegiado){
        $objRelMotivoDistrColegiadoDTO = new RelMotivoDistrColegiadoDTO();
        $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($Colegiado);
        $arrObjRelMotivoDistrColegiadoDTO[] = $objRelMotivoDistrColegiadoDTO;
      }
      $objMotivoDistribuicaoDTO->setArrObjRelMotivoDistrColegiadoDTO($arrObjRelMotivoDistrColegiadoDTO);

      $objMotivoDistribuicaoDTO->setStrSinAtivo('S');

      if (isset($_POST['sbmCadastrarMotivoDistribuicao'])) {
        try{
          $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
          $objMotivoDistribuicaoDTO = $objMotivoDistribuicaoRN->cadastrar($objMotivoDistribuicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo de Distribui��o "'.$objMotivoDistribuicaoDTO->getStrDescricao().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_motivo_distribuicao='.$objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao().PaginaSEI::getInstance()->montarAncora($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_distribuicao_alterar':
      $strTitulo = 'Alterar Motivo de Distribui��o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarMotivoDistribuicao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_motivo_distribuicao'])){
        $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($_GET['id_motivo_distribuicao']);
        $objMotivoDistribuicaoDTO->retTodos();
        $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
        $objMotivoDistribuicaoDTO = $objMotivoDistribuicaoRN->consultar($objMotivoDistribuicaoDTO);
        if ($objMotivoDistribuicaoDTO==null){
          throw new InfraException('Registro n�o encontrado.');
        }
      } else {
        $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($_POST['hdnIdMotivoDistribuicao']);
        $objMotivoDistribuicaoDTO->setStrDescricao($_POST['txtDescricao']);
        $objMotivoDistribuicaoDTO->setStrSinAtivo('S');
        $objMotivoDistribuicaoDTO->setStrStaTipo($_POST['selStaTipo']);
        
        $arrColegiados = PaginaSEI::getInstance()->getArrValuesSelect($_POST['hdnColegiados']);
        $arrObjRelMotivoDistrColegiadoDTO = array();
        foreach($arrColegiados as $Colegiado){
          $objRelMotivoDistrColegiadoDTO = new RelMotivoDistrColegiadoDTO();
          $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($Colegiado);
          $arrObjRelMotivoDistrColegiadoDTO[] = $objRelMotivoDistrColegiadoDTO;
        }
        $objMotivoDistribuicaoDTO->setArrObjRelMotivoDistrColegiadoDTO($arrObjRelMotivoDistrColegiadoDTO);
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarMotivoDistribuicao'])) {
        try{
          $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
          $objMotivoDistribuicaoRN->alterar($objMotivoDistribuicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo de Distribui��o "'.$objMotivoDistribuicaoDTO->getStrDescricao().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_distribuicao_consultar':
      $strTitulo = 'Consultar Motivo de Distribui��o';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_motivo_distribuicao'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objMotivoDistribuicaoDTO->setNumIdMotivoDistribuicao($_GET['id_motivo_distribuicao']);
      $objMotivoDistribuicaoDTO->setBolExclusaoLogica(false);
      $objMotivoDistribuicaoDTO->retTodos();
      $objMotivoDistribuicaoRN = new MotivoDistribuicaoRN();
      $objMotivoDistribuicaoDTO = $objMotivoDistribuicaoRN->consultar($objMotivoDistribuicaoDTO);
      if ($objMotivoDistribuicaoDTO===null){
        throw new InfraException('Registro n�o encontrado.');
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensSelStaTipo = MotivoDistribuicaoINT::montarSelectStaTipo('null','&nbsp;',$objMotivoDistribuicaoDTO->getStrStaTipo());
  $strLinkAjaxColegiado = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=colegiado_auto_completar_todas');
  $strLinkColegiadoSelecao = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_selecionar&tipo_selecao=2&id_object=objLupaColegiados');
  $strItensSelColegiado = RelMotivoDistrColegiadoINT::montarSelectIdColegiado(null,null,null,'',$objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao());

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

  #lblStaTipo {position:absolute;left:0%;top:0%;width:25%;}
  #selStaTipo {position:absolute;left:0%;top:6%;width:25%;}

  #lblDescricao {position:absolute;left:0%;top:16%;width:70%;}
  #txtDescricao {position:absolute;left:0%;top:22%;width:70%;}

  #lblColegiados {position:absolute;left:0%;top:32%;width:70%;}
  #selColegiados {position:absolute;left:0%;top:39%;width:70%;}
  #imgLupaColegiados {position:absolute;left:71%;top:39%;}
  #imgExcluirColegiados {position:absolute;left:71%;top:45%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script type="text/javascript"><?}
?>

var objLupaColegiados= null;

  function inicializar() {
    if ('<?=$_GET['acao']?>'=='motivo_distribuicao_cadastrar') {
      document.getElementById('selStaTipo').focus();
    } else if ('<?=$_GET['acao']?>'=='motivo_distribuicao_consultar') {
        infraDesabilitarCamposAreaDados();
    } else {
        document.getElementById('btnCancelar').focus();
    }

    objLupaColegiados = new infraLupaSelect('selColegiados','hdnColegiados','<?=$strLinkColegiadoSelecao?>');

    infraEfeitoTabelas();
  }

  function validarCadastro() {

  if (!infraSelectSelecionado('selStaTipo')) {
    alert('Selecione um Tipo.');
    document.getElementById('selStaTipo').focus();
    return false;
  }

  if (infraTrim(document.getElementById('txtDescricao').value)=='') {
    alert('Informe a Descri��o.');
    document.getElementById('txtDescricao').focus();
    return false;
  }


  return true;
  }

  function OnSubmitForm() {
  return validarCadastro();
  }

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
  <form id="frmMotivoDistribuicaoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
    <?
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    //PaginaSEI::getInstance()->montarAreaValidacao();
    PaginaSEI::getInstance()->abrirAreaDados('30em');
    ?>

    <label id="lblStaTipo" for="selStaTipo" accesskey="" class="infraLabelObrigatorio">Tipo:</label>
    <select id="selStaTipo" name="selStaTipo" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
      <?=$strItensSelStaTipo?>
    </select>

    <label id="lblDescricao" for="txtDescricao" accesskey="" class="infraLabelObrigatorio">Descri��o:</label>
    <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="<?=PaginaSEI::tratarHTML($objMotivoDistribuicaoDTO->getStrDescricao());?>" onkeypress="return infraMascaraTexto(this,event,250);" maxlength="250" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />


    <label id="lblColegiados" for="selColegiados" class="infraLabelObrigatorio">Colegiados:</label>
    <input type="hidden" id="hdnIdColegiado" name="hdnIdColegiado" class="infraText" value="" />
    <select id="selColegiados" name="selColegiados" size="10" multiple="multiple" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
      <?=$strItensSelColegiado?>
    </select>
    <img id="imgLupaColegiados" onclick="objLupaColegiados.selecionar(700,500);" src="<?=PaginaSEI::getInstance()->getIconePesquisar()?>" alt="Selecionar Colegiados" title="Selecionar Colegiados" class="infraImg" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <img id="imgExcluirColegiados" onclick="objLupaColegiados.remover();" src="<?=PaginaSEI::getInstance()->getIconeRemover()?>" alt="Remover Colegiados Selecionadas" title="Remover Colegiados Selecionadas" class="infraImg" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <input type="hidden" id="hdnColegiados" name="hdnColegiados" value="<?=$_POST['hdnColegiados']?>" />
    <input type="hidden" id="hdnIdMotivoDistribuicao" name="hdnIdMotivoDistribuicao" value="<?=PaginaSEI::tratarHTML($objMotivoDistribuicaoDTO->getNumIdMotivoDistribuicao());?>" />
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    //PaginaSEI::getInstance()->montarAreaDebug();
    //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
    ?>
  </form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>