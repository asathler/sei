<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class TipoMateriaINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objTipoMateriaDTO = new TipoMateriaDTO();
    $objTipoMateriaDTO->retNumIdTipoMateria();
    $objTipoMateriaDTO->retStrDescricao();

    if ($strValorItemSelecionado!=null){
      $objTipoMateriaDTO->setBolExclusaoLogica(false);
      $objTipoMateriaDTO->adicionarCriterio(array('SinAtivo','IdTipoMateria'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objTipoMateriaDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objTipoMateriaRN = new TipoMateriaRN();
    $arrObjTipoMateriaDTO = $objTipoMateriaRN->listar($objTipoMateriaDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjTipoMateriaDTO, 'IdTipoMateria', 'Descricao');
  }
}
?>