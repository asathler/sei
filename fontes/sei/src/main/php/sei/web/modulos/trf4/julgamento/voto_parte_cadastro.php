<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);


  if ($_GET['acao_origem']!='voto_parte_cadastrar'){
    $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
  } else {
    $arrStrIds = explode(',',$_POST['hdnAncora']);
  }
  $strAncora=implode(',',$arrStrIds);




  $objVotoParteDTO = new VotoParteDTO();

  $bolRelator=false;
  $bolMultiplo=false;
  $bolPedidoVista=false;
  $bolReferendo=false;
  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '';
  $strParametros.='&id_julgamento_parte='.$_GET['id_julgamento_parte'];
  //$strParametros.='&id_usuario='.$_GET['id_usuario'];
  $arrComandos = array();

  switch($_GET['acao']){
    case 'voto_parte_cadastrar':
      $strTitulo = 'Proclama��o do Voto';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarVotoParte" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
//      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objUsuarioDTO=new UsuarioDTO();
      $objVotoParteRN=new VotoParteRN();
      $objJulgamentoParteRN=new JulgamentoParteRN();
      $bolRelator=false;

      $objJulgamentoParteDTO=new JulgamentoParteDTO();
      $objJulgamentoParteDTO->setNumIdJulgamentoParte($_GET['id_julgamento_parte']);
      $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
      $objJulgamentoParteDTO->retNumIdUsuarioRelatorDistribuicao();
      $objJulgamentoParteDTO->retStrStaTipoItemSessaoBloco();
      $objJulgamentoParteDTO->retNumIdDistribuicaoItemSessaoJulgamento();
      $objJulgamentoParteDTO=$objJulgamentoParteRN->consultar($objJulgamentoParteDTO);

      if (InfraArray::contar($arrStrIds)==1){
        $objUsuarioRN=new UsuarioRN();
        $objUsuarioDTO->setNumIdUsuario($arrStrIds[0]);
        $objUsuarioDTO->retStrNome();
        $objUsuarioDTO=$objUsuarioRN->consultarRN0489($objUsuarioDTO);


        if($objJulgamentoParteDTO->getNumIdUsuarioRelatorDistribuicao()==$arrStrIds[0]){
          $bolRelator=true;
        }
        if($objJulgamentoParteDTO->getStrStaTipoItemSessaoBloco()==TipoSessaoBlocoRN::$STA_REFERENDO){
          $bolReferendo=true;
        }
        $objVotoParteDTOBanco = new VotoParteDTO();
        $objVotoParteDTOBanco->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
        $objVotoParteDTOBanco->setStrStaVotoParte(VotoParteRN::$STA_PEDIDO_VISTA);
        $objVotoParteDTOBanco->setNumIdUsuario($arrStrIds[0],InfraDTO::$OPER_DIFERENTE);
        if($objVotoParteRN->contar($objVotoParteDTOBanco)>0) {
          $bolPedidoVista=true;
        }

        $objVotoParteDTOBanco = new VotoParteDTO();
        $objVotoParteDTOBanco->setNumIdJulgamentoParte($_GET['id_julgamento_parte']);
        $objVotoParteDTOBanco->setNumIdUsuario($arrStrIds[0]);
        $objVotoParteDTOBanco->retTodos();
        $objVotoParteDTOBanco=$objVotoParteRN->consultar($objVotoParteDTOBanco);

      } else {
        $bolMultiplo=true;
        $objVotoParteDTOBanco=null;
        $objUsuarioDTO->setStrNome(InfraArray::contar($arrStrIds).' Membros Selecionados.');

        $objVotoParteDTO2 = new VotoParteDTO();
        $objVotoParteDTO2->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
        $objVotoParteDTO2->setStrStaVotoParte(VotoParteRN::$STA_PEDIDO_VISTA);
        $objVotoParteDTO2->setNumIdUsuario($arrStrIds[0],InfraDTO::$OPER_DIFERENTE);
        if($objVotoParteRN->contar($objVotoParteDTO2)>0) {
          $bolPedidoVista=true;
        }

      }
      if ($objVotoParteDTOBanco!=null && !isset($_POST['txaRessalva'])){
        $_POST['txaRessalva']=$objVotoParteDTOBanco->getStrRessalva();
        $_POST['selStaVotoParte']=$objVotoParteDTOBanco->getStrStaVotoParte();
        $_POST['selVotoParte']=$objVotoParteDTOBanco->getNumIdVotoParteAssociado();
        $_POST['selProvimento']=$objVotoParteDTOBanco->getNumIdProvimento();
        $_POST['txaComplemento']=$objVotoParteDTOBanco->getStrComplemento();
      }
      $objVotoParteDTO->setNumIdVotoParte(null);
      $objVotoParteDTO->setStrRessalva($_POST['txaRessalva']);
      $objVotoParteDTO->setStrComplemento($_POST['txaComplemento']);
      $objVotoParteDTO->setNumIdProvimento($_POST['selProvimento']);
      $numIdJulgamentoParte = $_GET['id_julgamento_parte'];
      $objVotoParteDTO->setNumIdJulgamentoParte($numIdJulgamentoParte);

      $objJulgamentoParteDTO=new JulgamentoParteDTO();

      $objJulgamentoParteDTO->setNumIdJulgamentoParte($numIdJulgamentoParte);
      $objJulgamentoParteDTO->retStrDescricao();
      $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
      $objJulgamentoParteDTO->retNumIdDistribuicaoItemSessaoJulgamento();
      $objJulgamentoParteDTO->retNumIdSessaoJulgamento();
      $objJulgamentoParteDTO=$objJulgamentoParteRN->consultar($objJulgamentoParteDTO);

      $numIdVotoParteAssociado =$_POST['selVotoParte'];
      if ($numIdVotoParteAssociado!==''){
        $objVotoParteDTO->setNumIdVotoParteAssociado($numIdVotoParteAssociado);
      }else{
        $objVotoParteDTO->setNumIdVotoParteAssociado(null);
      }

      $objVotoParteDTOBanco=new VotoParteDTO();
      $objVotoParteDTOBanco->setNumIdJulgamentoParte($_GET['id_julgamento_parte']);
      $objVotoParteDTOBanco->setNumIdUsuario($arrStrIds,InfraDTO::$OPER_NOT_IN);
      $objVotoParteDTOBanco->setStrStaVotoParte(VotoParteRN::$STA_DIVERGE);
      $objVotoParteDTOBanco->retNumIdVotoParte();
      $objVotoParteDTOBanco->retStrNomeUsuario();
      $arrObjVotoParteDTOBanco=$objVotoParteRN->listar($objVotoParteDTOBanco);

      $strItensSelVotoParteAssociado='';
      if(InfraArray::contar($arrObjVotoParteDTOBanco)>0){
        $strItensSelVotoParteAssociado.='<option value="null"';
        if ($numIdVotoParteAssociado==''){
          $strItensSelVotoParteAssociado.=' selected="selected"';
        }
        $strItensSelVotoParteAssociado.=">Relator</option>\n";
      }
      foreach ($arrObjVotoParteDTOBanco as $dto) {
        $strItensSelVotoParteAssociado.='<option value="'.$dto->getNumIdVotoParte().'"';
        if ($numIdVotoParteAssociado==$dto->getNumIdVotoParte()){
          $strItensSelVotoParteAssociado.=' selected="selected"';
        }
        $strItensSelVotoParteAssociado.='>'.$dto->getStrNomeUsuario()."</option>\n";
      }

      if ($bolRelator){
        $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_ACOMPANHA);
      } else {
        $objVotoParteDTO->setStrStaVotoParte($_POST['selStaVotoParte']);
      }

      $objVotoParteDTO->setDthVoto(InfraData::getStrDataHoraAtual());
      $objVotoParteDTO->setNumIdSessaoVoto($objJulgamentoParteDTO->getNumIdSessaoJulgamento());

      $numIdUsuario = PaginaSEI::getInstance()->recuperarCampo('selUsuario');
      if ($numIdUsuario!==''){
        $objVotoParteDTO->setNumIdUsuario($numIdUsuario);
      }else{
        $objVotoParteDTO->setNumIdUsuario(null);
      }
      $objVotoParteDTO->setStrSinVencedor('N');

      if (isset($_POST['sbmCadastrarVotoParte'])) {
        try{
          $objVotoParteRN = new VotoParteRN();
          $arrObjVotoParteDTO=array();
          foreach ($arrStrIds as $idUsuario) {
            $dto=clone $objVotoParteDTO;
            $dto->setNumIdUsuario($idUsuario);
            $arrObjVotoParteDTO[]=$dto;
          }
          //voto em branco - excluir voto
          if($objVotoParteDTO->getStrStaVotoParte()==null){
            $objVotoParteRN->cancelar($arrObjVotoParteDTO);
          } else {
            $objVotoParteRN->registrar($arrObjVotoParteDTO);
          }
          $bolExecucaoOK=true;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  //$strItensSelJulgamentoParte = JulgamentoParteINT::montarSelectDescricao('null','&nbsp;',$objVotoParteDTO->getNumIdJulgamentoParte());
  if($bolReferendo){
    $strItensSelStaVotoParte = VotoParteINT::montarSelectStaVotoParteReferendo('null','&nbsp;',$_POST['selStaVotoParte']);
  } else {
    $strItensSelStaVotoParte = VotoParteINT::montarSelectStaVotoParte('null','&nbsp;',$_POST['selStaVotoParte'],$bolMultiplo,$bolPedidoVista);
  }

  $strItensProvimento=ProvimentoINT::montarSelectConteudo('null','&nbsp;',$_POST['selProvimento']);

  $classVoto=$bolRelator?'infraLabelOpcional':'infraLabelObrigatorio';

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

#lblUsuario {position:absolute;left:0%;top:0%;width:50%;}
#txtUsuario {position:absolute;left:0%;top:20%;width:50%;}

#lblParte {position:absolute;left:0%;top:50%;width:50%;}
#txtParte {position:absolute;left:0%;top:70%;width:50%;}

#lblStaVotoParte {position:absolute;left:0%;top:0%;width:25%;}
#selStaVotoParte {position:absolute;left:0%;top:40%;width:25%;}

#lblVotoParte {position:absolute;left:30%;top:0%;width:50%;}
#selVotoParte {position:absolute;left:30%;top:40%;width:50%;}

  #lblProvimento {position:absolute;left:0%;top:0%;width:95%;}
  #selProvimento {position:absolute;left:0%;top:15%;width:96%;}
  #lblComplemento {position:absolute;left:0%;top:34%;width:95%;}
  #txaComplemento {position:absolute;left:0%;top:48%;width:95%;}


#lblRessalva {position:absolute;left:0%;top:0%;width:95%;}
#txaRessalva {position:absolute;left:0%;top:17%;width:95%;}


<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>


var assoc=<?=$strItensSelVotoParteAssociado!=''?1:0;?>  
function inicializar(){
<?if ($bolExecucaoOK) {?>
  window.parent.infraFecharJanelaModal();
  window.parent.$('input[type=checkbox]:checked').each(function(){this.click()});
<? }
   if ($bolRelator){?>
   document.getElementById('divInfraAreaDados1').style.display="none";
   document.getElementById('selStaVotoParte').value='<?=VotoParteRN::$STA_ACOMPANHA?>';
<? } ?>
  document.getElementById('selStaVotoParte').focus();
  verificaVoto(document.getElementById('selStaVotoParte'));
  infraEfeitoTabelas();
};
function exibeVotoAssociado(bol) {
  if (bol) {
    document.getElementById('selVotoParte').style.display="";
    document.getElementById('lblVotoParte').style.display="";
  } else {
    document.getElementById('selVotoParte').style.display="none";
    document.getElementById('lblVotoParte').style.display="none";
  }

}
function verificaVoto(sel) {
  if (sel.value=='<?=VotoParteRN::$STA_DIVERGE?>' || <?=$bolRelator?1:0;?>) {
    document.getElementById('divInfraAreaDados2').style.display="";
  } else {
    document.getElementById('divInfraAreaDados2').style.display="none";
  }
  if (assoc){
    if (sel.value=='<?=VotoParteRN::$STA_ACOMPANHA?>') {
      exibeVotoAssociado(true);
    } else {
      exibeVotoAssociado(false);
    }
  }
}
function validarCadastro() {

  <?if(!$bolPedidoVista){?>
  if (document.getElementById('divInfraAreaDados1').style.display==""){
//    if(!infraSelectSelecionado('selStaVotoParte')) {
//      alert('Selecione um Voto.');
//      document.getElementById('selStaVotoParte').focus();
//      return false;
//    }
  }
  <?}?>
  if (document.getElementById('divInfraAreaDados2').style.display==""){
    if(!infraSelectSelecionado('selProvimento')) {
      alert('Selecione um Provimento.');
      document.getElementById('selProvimento').focus();
      return false;
    }
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}
  function finalizar() {
<?if ($bolExecucaoOK) {?>
  window.parent.infraFecharJanelaModal();
  window.parent.document.getElementById('frmItemSessaoJulgamentoJulgar').submit();
<? } ?>
  }

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();" onunload="finalizar();"');
?>
<form id="frmVotoParteCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('10em');
?>


  <label id="lblUsuario" for="txtUsuario" accesskey="" class="infraLabelObrigatorio">Membro do Colegiado:</label>
  <input type="text" id="txtUsuario" name="txtUsuario" class="infraText" readonly="readonly" value="<?=PaginaSEI::tratarHTML($objUsuarioDTO->getStrNome());?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblParte" for="txtParte" accesskey="" class="infraLabelObrigatorio">Item do julgamento:</label>
  <input type="text" id="txtParte" name="txtParte" class="infraText" readonly="readonly" value="<?=PaginaSEI::tratarHTML($objJulgamentoParteDTO->getStrDescricao());?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
<?
PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblStaVotoParte" for="selStaVotoParte" accesskey="" class="<?=$classVoto;?>">Voto:</label>
  <select id="selStaVotoParte" name="selStaVotoParte" onchange="verificaVoto(this);" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelStaVotoParte?>
  </select>
<? if ($strItensSelVotoParteAssociado!='') { ?>
  <label id="lblVotoParte" for="selVotoParte" accesskey="" class="infraLabelObrigatorio">Voto Associado:</label>
  <select id="selVotoParte" name="selVotoParte" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelVotoParteAssociado?>
  </select>
<? } ?>

  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->abrirAreaDados('15em');
  ?>

  <label id="lblProvimento" for="selProvimento" accesskey="" class="infraLabelObrigatorio">Provimento:</label>
  <select id="selProvimento" name="selProvimento" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensProvimento ?>
  </select>
  <label id="lblComplemento" for="txaComplemento" accesskey="" class="infraLabelOpcional">Complemento:</label>
  <textarea id="txaComplemento" name="txaComplemento" rows="3" class="infraTextarea"
            tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?= PaginaSEI::tratarHTML($_POST['txaComplemento']) ?></textarea>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->abrirAreaDados('12em');
  ?>
  <label id="lblRessalva" for="txaRessalva" accesskey="" class="infraLabelOpcional">Ressalva:</label>
  <textarea id="txaRessalva" name="txaRessalva" rows="3"  class="infraTextarea" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>"><?=PaginaSEI::tratarHTML($_POST['txaRessalva'])?></textarea>

  <input type="hidden" id="hdnAncora" name="hdnAncora" value="<?=PaginaSEI::tratarHTML($strAncora)?>" />
  <input type="hidden" id="hdnIdVotoParte" name="hdnIdVotoParte" value="<?=$objVotoParteDTO->getNumIdVotoParte();?>" />

   <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>