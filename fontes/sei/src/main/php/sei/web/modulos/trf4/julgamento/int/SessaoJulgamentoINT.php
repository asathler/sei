<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 27/10/2014 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.33.1
 *
 * Vers�o no CVS: $Id$
 */

require_once __DIR__ . '/../../../../SEI.php';

class SessaoJulgamentoINT extends InfraINT
{

  /**
   * @param string $strPrimeiroItemValor
   * @param string $strPrimeiroItemDescricao
   * @param string $strValorItemSelecionado
   * @param string|array $varIdColegiado
   * @param string|array $varStaSituacao
   * @return string
   */
  public static function montarSelectDescricao(string $strPrimeiroItemValor, string $strPrimeiroItemDescricao, string $strValorItemSelecionado, $varIdColegiado = '', $varStaSituacao = ''): string
  {
    $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
    $objSessaoJulgamentoDTO->retDthSessao();
    $objSessaoJulgamentoDTO->retStrNomeColegiado();


    if ($varStaSituacao!=='') {
      if (is_array($varStaSituacao)) {
        $objSessaoJulgamentoDTO->setStrStaSituacao($varStaSituacao, InfraDTO::$OPER_IN);
      } else {
        $objSessaoJulgamentoDTO->setStrStaSituacao($varStaSituacao);
      }
    } else {
      $objSessaoJulgamentoDTO->setStrStaSituacao(SessaoJulgamentoRN::$ES_PAUTA_ABERTA);
    }

    if ($varIdColegiado!=='') {
      if (is_array($varIdColegiado)) {
        $objSessaoJulgamentoDTO->setNumIdColegiado($varIdColegiado, InfraDTO::$OPER_IN);
      } else {
        $objSessaoJulgamentoDTO->setNumIdColegiado($varIdColegiado);
      }

    }

    $objSessaoJulgamentoDTO->setOrdDthSessao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
    $arrObjSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);
    foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {
      $objSessaoJulgamentoDTO->setStrNomeColegiado($objSessaoJulgamentoDTO->getStrNomeColegiado() . ' - ' . $objSessaoJulgamentoDTO->getDthSessao());
    }


    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjSessaoJulgamentoDTO, 'IdSessaoJulgamento', 'NomeColegiado');
  }

  public static function montarSelectDthSessao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdColegiado = '', $strStaSituacao = ''): string
  {
    $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
    $objSessaoJulgamentoDTO->retDthSessao();
    $objSessaoJulgamentoDTO->retStrSinVirtualTipoSessao();
    $objSessaoJulgamentoDTO->retStrNomeColegiado();
    $objSessaoJulgamentoDTO->retStrStaSituacao();

    if ($numIdColegiado!=='') {
      $objSessaoJulgamentoDTO->setNumIdColegiado($numIdColegiado);
    }

    if ($strStaSituacao!=='') {
      if (strpos($strStaSituacao, ',')>0) {
        $objSessaoJulgamentoDTO->adicionarCriterio(array('StaSituacao'), array(InfraDTO::$OPER_IN), array(explode(',', $strStaSituacao)), null, 'pesquisa');
      } else {
        $objSessaoJulgamentoDTO->adicionarCriterio(array('StaSituacao'), array(InfraDTO::$OPER_IGUAL), array($strStaSituacao), null, 'pesquisa');
      }
    }

    if ($strValorItemSelecionado!=null) {
      if ($strStaSituacao!=='') {
        $objSessaoJulgamentoDTO->adicionarCriterio(array('IdSessaoJulgamento'), array(InfraDTO::$OPER_IGUAL), array($strValorItemSelecionado), null, 'selecao');
        $objSessaoJulgamentoDTO->agruparCriterios(array('pesquisa', 'selecao'), InfraDTO::$OPER_LOGICO_OR);
      } else {
        $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($strValorItemSelecionado);
      }
    }

    $objSessaoJulgamentoDTO->setOrdDthSessao(InfraDTO::$TIPO_ORDENACAO_DESC);
    $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
    $arrObjSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);
    $objArrSituacaoSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoRN->listarValoresSituacaoSessao(), 'StaSituacao');

    foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {
      $strSessao=substr($objSessaoJulgamentoDTO->getDthSessao(), 0, - 3);
      if ($strValorItemSelecionado!=$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento()) {
        $strDescricao = $objArrSituacaoSessaoJulgamentoDTO[$objSessaoJulgamentoDTO->getStrStaSituacao()]->getStrDescricao();
        $strSessao .= ' - ' . $strDescricao;
      }
      if($objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()==='S'){
        $strSessao .= ' - VIRTUAL';
      }
      $objSessaoJulgamentoDTO->setDthSessao($strSessao);
    }

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjSessaoJulgamentoDTO, 'IdSessaoJulgamento', 'Sessao');
  }

  public static function montarSelectDthSessaoInclusao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdColegiado = ''): string
  {
    $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
    $objSessaoJulgamentoDTO->retDthSessao();
    $objSessaoJulgamentoDTO->retStrNomeColegiado();
    $objSessaoJulgamentoDTO->retStrSinVirtualTipoSessao();
    $objSessaoJulgamentoDTO->retStrStaSituacao();

    if ($numIdColegiado!=='') {
      $objSessaoJulgamentoDTO->setNumIdColegiado($numIdColegiado);
    }

    $arrStaSituacao=array(SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_SUSPENSA,SessaoJulgamentoRN::$ES_ABERTA);

    $objSessaoJulgamentoDTO->adicionarCriterio(array('StaSituacao'), array(InfraDTO::$OPER_IN), array(array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA)), null, 'pesquisa1');
    $objSessaoJulgamentoDTO->adicionarCriterio(array('StaSituacao','SinVirtualTipoSessao'),
        array(InfraDTO::$OPER_IN,InfraDTO::$OPER_DIFERENTE),
        array($arrStaSituacao,'S'),
        array(InfraDTO::$OPER_LOGICO_AND), 'pesquisa2');

    if ($strValorItemSelecionado!=null) {
      $objSessaoJulgamentoDTO->adicionarCriterio(array('IdSessaoJulgamento'), array(InfraDTO::$OPER_IGUAL), array($strValorItemSelecionado), null, 'selecao');
      $objSessaoJulgamentoDTO->agruparCriterios(array('pesquisa1', 'pesquisa2', 'selecao'), array(InfraDTO::$OPER_LOGICO_OR,InfraDTO::$OPER_LOGICO_OR));
    } else {
      $objSessaoJulgamentoDTO->agruparCriterios(array('pesquisa1', 'pesquisa2'), array(InfraDTO::$OPER_LOGICO_OR));
    }

    $objSessaoJulgamentoDTO->setOrdDthSessao(InfraDTO::$TIPO_ORDENACAO_DESC);
    $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
    $arrObjSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);
    $objArrSituacaoSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoRN->listarValoresSituacaoSessao(), 'StaSituacao');

    foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {
      $strSessao=substr($objSessaoJulgamentoDTO->getDthSessao(), 0, - 3);
      if ($strValorItemSelecionado!=$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento()) {
        $strDescricao = $objArrSituacaoSessaoJulgamentoDTO[$objSessaoJulgamentoDTO->getStrStaSituacao()]->getStrDescricao();
        $strSessao .= ' - ' . $strDescricao;
      }
      if($objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()==='S'){
        $strSessao .= ' - VIRTUAL';
      }
      $objSessaoJulgamentoDTO->setDthSessao($strSessao);
    }

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjSessaoJulgamentoDTO, 'IdSessaoJulgamento', 'Sessao');
  }
  public static function montarSelectStaSituacao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado): string
  {
    $objSessaoJulgamentoRN = new SessaoJulgamentoRN();

    $arrObjSituacaoSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listarValoresSituacaoSessao();

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjSituacaoSessaoJulgamentoDTO, 'StaSituacao', 'Descricao');

  }

  public static function montarSelectStaSituacaoFiltrado($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado): string
  {
    $objSessaoJulgamentoRN = new SessaoJulgamentoRN();

    $arrObjSituacaoSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listarValoresSituacaoSessaoFiltrado($strValorItemSelecionado);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjSituacaoSessaoJulgamentoDTO, 'StaSituacao', 'Descricao');

  }

   public static function montarAcoesTabelaJulgamento(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, SessaoJulgamentoDTO $objSessaoJulgamentoDTO, $strParametros): string
  {
    $idProcedimento = $_GET['id_procedimento_sessao']??null;
    $arrObjDestaqueDTO = $objItemSessaoJulgamentoDTO->getArrObjDestaqueDTO();
    if ($arrObjDestaqueDTO!==null) {
      $arrObjDestaqueDTO = InfraArray::filtrarArrInfraDTO($objItemSessaoJulgamentoDTO->getArrObjDestaqueDTO(), 'StaAcesso', DestaqueRN::$TA_RESTRITO);
    }
    $idUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();
    $idColegiado = $objSessaoJulgamentoDTO->getNumIdColegiado();
    $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();
    $idUsuarioRelator = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
    $idUsuarioSessao = $objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao();
    $idItemSessaoJulgamento = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
     $numIdSessaoBloco=$objItemSessaoJulgamentoDTO->getNumIdSessaoBloco();
     $staItem=CacheSessaoJulgamentoRN::getObjSessaoBlocoDTO($numIdSessaoBloco)->getStrStaTipoItem();
    $bolQuorumMinimoAtingido = $objSessaoJulgamentoDTO->getNumQuorum()>=$objSessaoJulgamentoDTO->getNumQuorumMinimoColegiado();
    $bolAdmin = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') && $idUnidadeAtual==$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado();
    $bolManual = $objItemSessaoJulgamentoDTO->getStrSinManual()==='S';
    $bolDiaSessao=(InfraData::compararDatasSimples(InfraData::getStrDataAtual(),$objSessaoJulgamentoDTO->getDthSessao())<=0);

    $arrObjPresencaSessaoDTO = $objSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();
    $arrObjPresencaSessaoDTO = InfraArray::indexarArrInfraDTO($arrObjPresencaSessaoDTO, 'IdUsuario');
    $strResultado = '';

    $bolUnidadeSessao = $objItemSessaoJulgamentoDTO->getNumIdUnidadeSessao()==$idUnidadeAtual;
    $bolBloqueado = in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(), array(ItemSessaoJulgamentoRN::$STA_JULGADO, ItemSessaoJulgamentoRN::$STA_DILIGENCIA, ItemSessaoJulgamentoRN::$STA_ADIADO, ItemSessaoJulgamentoRN::$STA_RETIRADO));
    $bolPossuiVotos = $objItemSessaoJulgamentoDTO->getStrSinPossuiVotos()==='S';
    $bolPossuiVotosAnteriores = $objItemSessaoJulgamentoDTO->getStrSinPossuiVotosAnteriores()==='S';
    //validar se tem doc disponibilizado do relator
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();

    if ($arrObjItemSessaoDocumentoDTO==null) {
      $arrObjItemSessaoDocumentoDTO = array();
    }
    $arrObjItemSessaoDocumentoDTO = InfraArray::converterArrInfraDTO($arrObjItemSessaoDocumentoDTO, 'IdUsuario');
    $bolDisponibilizadoRelator = in_array($idUsuarioRelator, $arrObjItemSessaoDocumentoDTO);
    $bolDisponibilizadoUsuarioSessao = in_array($idUsuarioSessao, $arrObjItemSessaoDocumentoDTO);

    $bolReferendo = $staItem==TipoSessaoBlocoRN::$STA_REFERENDO;

    //a�oes secretaria:
    $bolAcaoPartes = ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && $bolAdmin && !$bolBloqueado && $bolQuorumMinimoAtingido && $bolDisponibilizadoRelator && !$bolReferendo);
    $bolAcaoJulgar = ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && $bolAdmin && !$bolBloqueado && $bolQuorumMinimoAtingido && $bolDisponibilizadoRelator && $bolDisponibilizadoUsuarioSessao && (isset($arrObjPresencaSessaoDTO[$idUsuarioSessao]) || $bolReferendo));
    if($bolManual){
      $bolAcaoJulgar=($staSessao==SessaoJulgamentoRN::$ES_ABERTA && $bolAdmin && !$bolBloqueado && $bolQuorumMinimoAtingido);
    }
//    $bolAcaoJulgarUnanime=$bolAcaoJulgar && $objItemSessaoJulgamentoDTO->getStrSinFracionado()=='N' && $objItemSessaoJulgamentoDTO->getStrStaSituacao()!=ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA;
    $bolAcaoAdiar = ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && !$bolReferendo && $bolAdmin && !$bolBloqueado);
    $bolAcaoCancelarAdiamento = ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && $bolAdmin && $objItemSessaoJulgamentoDTO->getStrStaSituacao()==ItemSessaoJulgamentoRN::$STA_ADIADO);
    $bolAcaoDiligencia = ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && !$bolReferendo && $bolAdmin && !$bolBloqueado);
    $bolAcaoCancelarDiligencia = ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && $bolAdmin && $objItemSessaoJulgamentoDTO->getStrStaSituacao()==ItemSessaoJulgamentoRN::$STA_DILIGENCIA);

    $bolAcaoBloqueioProcesso=$objItemSessaoJulgamentoDTO->getStrStaUltimoDistribuicao()==DistribuicaoRN::$SD_ULTIMA &&
        ($bolAdmin || $objItemSessaoJulgamentoDTO->getNumIdUnidadeRelatorDistribuicao()==$idUnidadeAtual);

    $bolAcaoRetirar = !$bolBloqueado &&
        ($staSessao==SessaoJulgamentoRN::$ES_PAUTA_ABERTA || $staSessao==SessaoJulgamentoRN::$ES_ABERTA || ($staSessao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA && $staItem!=TipoSessaoBlocoRN::$STA_PAUTA)) &&
        ($bolAdmin || ($bolUnidadeSessao && !$bolPossuiVotos && $staSessao!=SessaoJulgamentoRN::$ES_ABERTA));

//    if($staItem==TipoSessaoBlocoRN::$STA_PAUTA && $idUsuarioRelator!=$idUsuarioSessao){
//      $bolAcaoRetirar=false;
//    }

    $bolAcaoConsultarJulgamento = $bolManual || $bolPossuiVotos || $bolPossuiVotosAnteriores || in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(), array(ItemSessaoJulgamentoRN::$STA_RETIRADO, ItemSessaoJulgamentoRN::$STA_ADIADO, ItemSessaoJulgamentoRN::$STA_DILIGENCIA));
    $bolAcaoConsultarJulgamento |= $bolDiaSessao;
    if(!isset($arrObjPresencaSessaoDTO[$idUsuarioSessao]) && $objItemSessaoJulgamentoDTO->getStrStaSituacao()==ItemSessaoJulgamentoRN::$STA_NORMAL){
      $bolAcaoConsultarJulgamento=false;
    }

    $bolAcaoComentariosRestritos = ($staSessao!=SessaoJulgamentoRN::$ES_CANCELADA &&
        (!in_array($staSessao, array(SessaoJulgamentoRN::$ES_ENCERRADA, SessaoJulgamentoRN::$ES_FINALIZADA)) || InfraArray::contar($arrObjDestaqueDTO)>0));

    if ($idProcedimento==$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()) {
      $strAcaoRetorno = 'procedimento_trabalhar';
    } else {
      $strAcaoRetorno = PaginaSEI::getInstance()->getAcaoRetorno();
    }
    //liberado para todos (inclusive secretaria)
    $bolAcaoRevisao=true;

    //exibir provimento - caso j� possua vencedor
    if ($objItemSessaoJulgamentoDTO->getStrDispositivo()!=null) {
      $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
      /** @var JulgamentoParteDTO $objJulgamentoParteDTO */
      foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
        $arrObjVotoParteDTO = $objJulgamentoParteDTO->getArrObjVotoParteDTO();
        /** @var VotoParteDTO $objVotoParteDTO */
        if ($objJulgamentoParteDTO->getNumUsuarioVencedor()==null) {
          continue;
        }
        $objVotoParteDTO = $arrObjVotoParteDTO[$objJulgamentoParteDTO->getNumUsuarioVencedor()];
        $balao = InfraString::transformarCaixaAlta($objVotoParteDTO->getStrConteudoProvimento()) . ' ' . $objVotoParteDTO->getStrComplemento();
        if (!in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(), array(ItemSessaoJulgamentoRN::$STA_RETIRADO, ItemSessaoJulgamentoRN::$STA_ADIADO, ItemSessaoJulgamentoRN::$STA_DILIGENCIA, ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA))) {
          $strResultado .= '<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($balao, $objJulgamentoParteDTO->getStrDescricao()) . '><img src="'.MdJulgarIcone::BALAO2.'" alt="bal�o de provimento" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
        }
      }

    }

    //julgar
     $strResultado .= self::montarIconeJulgar($bolAcaoJulgar, $objItemSessaoJulgamentoDTO, $bolAcaoConsultarJulgamento);

    //listar partes
    if ($bolAcaoPartes && !$bolManual) {
      $strLink = self::montarLink('julgamento_parte_cadastrar', $strAcaoRetorno, $idItemSessaoJulgamento, $strParametros);
      $strResultado .= self::montarAncoraDocumento(MdJulgarIcone::FRACIONAR_JULGAMENTO, 'Fracionar Julgamento', $strLink);
    }

    if ($bolAdmin && SessaoSEI::getInstance()->verificarPermissao('eleicao_listar')){
      $strLink = self::montarLink('eleicao_listar', $strAcaoRetorno, $idItemSessaoJulgamento, $strParametros);
      $strResultado .= self::montarAncoraDocumento(MdJulgarIcone::ELEICAO, 'Escrut�nios Eletr�nicos do Processo', $strLink);
    }

    //bloqueio de processo
    if ($bolAcaoBloqueioProcesso) {
      //exibir icone de bloqueio do processo

      $arrObjBloqueioItemSessUnidadeDTO=$objItemSessaoJulgamentoDTO->getArrObjBloqueioItemSessUnidadeDTO();
      $numIdDistribuicao=$objItemSessaoJulgamentoDTO->getNumIdDistribuicao();
      $arrUnidadesBloqueadas=array();
      /** @var BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO */
      foreach ($arrObjBloqueioItemSessUnidadeDTO as $objBloqueioItemSessUnidadeDTO) {
        if($objBloqueioItemSessUnidadeDTO->getStrStaTipo()==BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO && $numIdDistribuicao==$objBloqueioItemSessUnidadeDTO->getNumIdDistribuicao()){
          $arrUnidadesBloqueadas[]=$objBloqueioItemSessUnidadeDTO->getStrSiglaUnidade();
        }
      }

      $strLink=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=bloqueio_item_sess_unidade_alterar&id_item_sessao_julgamento='.$idItemSessaoJulgamento.$strParametros);
      if(count($arrUnidadesBloqueadas)>0){
        $strUnidades=PaginaSEI::formatarParametrosJavaScript(implode('\n',$arrUnidadesBloqueadas));
        $strResultado .= '<a href="javascript:void(0);" data-icon="bloqueio" onclick="infraAbrirJanelaModal(\''.$strLink.'\',800,600,false)" ' . PaginaSEI::montarTitleTooltip($strUnidades, 'Restri��o de acesso') . '><img src="'.MdJulgarIcone::ACESSO_RESTRITO1.'" alt="acesso restrito" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
      } else {
        $strResultado .= '<a href="javascript:void(0);" data-icon="bloqueio" onclick="infraAbrirJanelaModal(\''.$strLink.'\',800,600,false)" ' . PaginaSEI::montarTitleTooltip('Sem restri��o') . '><img src="'.MdJulgarIcone::ACESSO_RESTRITO2.'" alt="sem restri��o de acesso" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
      }
    }

    //retirar de pauta
    if ($bolAcaoRetirar) {
      $strLink = self::montarLink('item_sessao_julgamento_retirar', $strAcaoRetorno, null, '&id_distribuicao_sessao=' . $objItemSessaoJulgamentoDTO->getNumIdDistribuicao() . $strParametros);
      $strResultado .= self::montarAncoraDocumento(PaginaSEI::getInstance()->getIconeRemover(), 'Retirar processo da Pauta', $strLink);
    }
    //adiar
    if ($bolAcaoAdiar) {
      $strLink = self::montarLink('item_sessao_julgamento_adiar', $_GET['acao'], $idItemSessaoJulgamento, $strParametros);
      $strResultado .= self::montarAncoraDocumento(MdJulgarIcone::ADIAR_JULGAMENTO, 'Adiar', $strLink);

    } else if ($bolAcaoCancelarAdiamento) {
      $strLink = self::montarLink('item_sessao_julgamento_adiamento_cancelar', $_GET['acao'], $idItemSessaoJulgamento, $strParametros);
      $strResultado .= self::montarAncoraDocumento(MdJulgarIcone::ADIAR_JULGAMENTO_CANCELAR, 'Cancelar Adiamento', $strLink);

    }
    //converter em diligencia
    if ($bolAcaoDiligencia) {
      $strLink = self::montarLink('item_sessao_julgamento_diligenciar', $strAcaoRetorno, $idItemSessaoJulgamento, $strParametros);
      $strResultado .= self::montarAncoraDocumento(MdJulgarIcone::CONVERTER_DILIGENCIA, 'Converter em Dilig�ncia', $strLink);
    } else if ($bolAcaoCancelarDiligencia) {
      $strLink = self::montarLink('item_sessao_julgamento_diligencia_cancelar', $_GET['acao'], $idItemSessaoJulgamento, $strParametros);
      $strResultado .= self::montarAncoraDocumento(MdJulgarIcone::CONVERTER_DILIGENCIA_CANCELAR, 'Cancelar Dilig�ncia', $strLink);
    }
    //coment�rios restritos
    if ($bolAcaoComentariosRestritos) {
      $strResultado .= DestaqueINT::montarIconeComentariosRestritos($strParametros, $arrObjDestaqueDTO, $idItemSessaoJulgamento, $idColegiado);
    }
    //revis�o de processos
    if ($bolAcaoRevisao){
      $strResultado .= self::montarIconeRevisao($objItemSessaoJulgamentoDTO);
    }


    return $strResultado;
  }

  public static function montarLink($acaoDestino, $acaoRetorno, $idItemSessao, $strParametros)
  {
    $strAcaoAtual=$_GET['acao']??$_GET['acao_ajax'];
    if($strAcaoAtual=='dados_sessao'){
      $strAcaoAtual=$_GET['acao_origem'];
    }
    $strLink = 'controlador.php?acao=' . $acaoDestino;
    $strLink .= '&acao_origem=' . $strAcaoAtual;
    if($acaoRetorno!='') {
    $strLink .= '&acao_retorno=' . $acaoRetorno;
    }
    if ($idItemSessao) {
      $strLink .= '&id_item_sessao_julgamento=' . $idItemSessao;
    }
    $strLink .= $strParametros;
    return SessaoSEI::getInstance()->assinarLink($strLink);
  }

  public static function montarAncoraDocumento($strImagem, $strTitulo, $strLink): string
  {
    $tab = PaginaSEI::getInstance()->getProxTabTabela();
    $str = "<a href=\"javascript:void(0)\" tabindex=\"$tab\">";
    $str .= "<img src=\"$strImagem\" onclick=\"abrirDocumento('$strLink',this);\" title=\"$strTitulo\" alt=\"$strTitulo\" class=\"infraImg\" />";
    $str .= '</a>';
    return $str;
  }

  public static function montarAncoraHref($strImagem, $strTitulo, $strLink): string
  {
    $str = '<a href="' . $strLink . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '">';
    $str.='<img src="' . $strImagem . '" title="' . $strTitulo . '" alt="' . $strTitulo . '" class="infraImg" /></a>';
    return $str;
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param $strResumo
   * @return string $strResultadoMembros
   * @throws InfraException
   */
  public static function montarTabelaComposicao(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, &$strResumo): string
  {
    PaginaSEI::getInstance()->getThCheck();
    $bolAdmin = SessaoJulgamentoRN::isAdministradorSessao($objSessaoJulgamentoDTO);
    $bolExibirCheckbox=SessaoJulgamentoRN::permiteDefinirPresencas($objSessaoJulgamentoDTO);
    $strResultadoMembros = '';
    $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();
    $strStyleCheckComposicao = '';
    if (!$bolExibirCheckbox) {
      $strStyleCheckComposicao = 'style="display:none"';
    }

    $bolMostraPresenca=!in_array($staSessao,[SessaoJulgamentoRN::$ES_PAUTA_ABERTA,SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_PREVISTA]);
    $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(), 'IdUsuario');

    $arrTitularesAusentes = array();
    if ($bolMostraPresenca) {
      $arrObjPresencaSessaoDTO = $objSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();

      $qtdPresentes=0;
      foreach ($arrObjPresencaSessaoDTO as $objPresencaSessaoDTO) {
        if ($objPresencaSessaoDTO->getDthSaida()==null){
          ++$qtdPresentes;
        }
      }
      if($qtdPresentes>0) {
        $strResumo = ' (' . $qtdPresentes . ($qtdPresentes===1 ? ' presente' : ' presentes') . ')';
      }

      $objVotoParteDTO=new VotoParteDTO();
      $objVotoParteDTO->setNumIdSessaoVoto($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objVotoParteDTO->retNumIdUsuario();
      $objVotoParteDTO->setDistinct(true);
      $objVotoParteRN=new VotoParteRN();
      $arrObjVotoParteDTO=$objVotoParteRN->listar($objVotoParteDTO);
      $arrIdUsuariosVotantes=InfraArray::converterArrInfraDTO($arrObjVotoParteDTO,'IdUsuario','IdUsuario');

      $arrPresencaSessao=array();
      foreach ($arrObjPresencaSessaoDTO as $objPresencaSessaoDTO) {
        if($objPresencaSessaoDTO->getDthSaida()==null) {
          $arrPresencaSessao[$objPresencaSessaoDTO->getNumIdUsuario()] = $objPresencaSessaoDTO->getDthEntrada();
        }
      }
      if ($staSessao==SessaoJulgamentoRN::$ES_ENCERRADA || $staSessao==SessaoJulgamentoRN::$ES_FINALIZADA) {
        foreach ($arrObjColegiadoComposicaoDTO as $id => $objColegiadoComposicaoDTO) {
          if ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR && !isset($arrPresencaSessao[$objColegiadoComposicaoDTO->getNumIdUsuario()])) {
            $arrTitularesAusentes[$id] = array('parcial' => false, 'motivo' => null);
          }
        }
        if (InfraArray::contar($arrTitularesAusentes)>0) {
          $objPresencaSessaoDTO = new PresencaSessaoDTO();
          $objPresencaSessaoRN = new PresencaSessaoRN();
          $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
          $objPresencaSessaoDTO->setDthSaida($objSessaoJulgamentoDTO->getDthInicio(), InfraDTO::$OPER_MAIOR);
          $objPresencaSessaoDTO->setNumIdUsuario(array_keys($arrTitularesAusentes), InfraDTO::$OPER_IN);
          $objPresencaSessaoDTO->retNumIdUsuario();
          $objPresencaSessaoDTO->setDistinct(true);
          $arrObjPresencaSessaoDTO2 = $objPresencaSessaoRN->listar($objPresencaSessaoDTO);
          if (InfraArray::contar($arrObjPresencaSessaoDTO2)>0) {
            foreach ($arrObjPresencaSessaoDTO2 as $objPresencaSessaoDTO) {
              $arrTitularesAusentes[$objPresencaSessaoDTO->getNumIdUsuario()]['parcial'] = true;
            }
          }
          $objAusenciaSessaoDTO = new AusenciaSessaoDTO();
          $objAusenciaSessaoRN = new AusenciaSessaoRN();
          $objAusenciaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
          $objAusenciaSessaoDTO->setNumIdUsuario(array_keys($arrTitularesAusentes), InfraDTO::$OPER_IN);
          $objAusenciaSessaoDTO->retTodos();
          $objAusenciaSessaoDTO->retStrDescricaoMotivoAusencia();
          $arrObjAusenciaSessaoDTO = $objAusenciaSessaoRN->listar($objAusenciaSessaoDTO);
          foreach ($arrObjAusenciaSessaoDTO as $objAusenciaSessaoDTO) {
            $arrTitularesAusentes[$objAusenciaSessaoDTO->getNumIdUsuario()]['motivo'] = $objAusenciaSessaoDTO->getStrDescricaoMotivoAusencia();
          }

        }
      }
    }

    $bolExibirAcoes=$bolAdmin && (($staSessao==SessaoJulgamentoRN::$ES_ABERTA || $staSessao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA || $staSessao==SessaoJulgamentoRN::$ES_SUSPENSA) ||
            ($staSessao==SessaoJulgamentoRN::$ES_ENCERRADA && InfraArray::contar($arrTitularesAusentes)>0));

    $strSumarioTabela = 'Tabela de Membros do Colegiado.';

    $strResultadoMembros .= '<table width="99%" class="infraTable tblMembros" summary="' . $strSumarioTabela . '">' . "\n";
    $strResultadoMembros .= '<tr style="">';
    $strResultadoMembros .= '<th class="infraTh" width="1%" ' . $strStyleCheckComposicao . '>' . PaginaSEI::getInstance()->getThCheck('', 'membros','style="display:none"') . "</th>\n";
    $strResultadoMembros .= '<th class="infraTh">Nome</th>' . "\n";
    if ($bolMostraPresenca) {
      $strResultadoMembros .= '<th class="infraTh">Presente</th>' . "\n";
    }
    if ($bolExibirAcoes) {
      $strResultadoMembros .= '<th class="infraTh">A��es</th>' . "\n";
    }
    $strResultadoMembros .= '</tr>' . "\n";
    $strCssTr = '';
    $i = 0;
    $arrObjColegiadoComposicaoDTO2=array_values($arrObjColegiadoComposicaoDTO);
    InfraArray::ordenarArrInfraDTO($arrObjColegiadoComposicaoDTO2,'NomeUsuario',InfraArray::$TIPO_ORDENACAO_ASC);

    foreach ($arrObjColegiadoComposicaoDTO2 as $objColegiadoComposicaoDTO) {
      $id=$objColegiadoComposicaoDTO->getNumIdUsuario();
      if($staSessao==SessaoJulgamentoRN::$ES_FINALIZADA){
        if(!isset($arrIdUsuariosVotantes[$id]) && $objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR){
          continue;
        }
      } else if(!isset($arrObjPresencaSessaoDTO[$id]) && $objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR &&
      $objColegiadoComposicaoDTO->getStrSinHabilitado()=='N'){
        continue;
      }
      $presente = isset($arrPresencaSessao[$objColegiadoComposicaoDTO->getNumIdUsuario()]);
      $strCssTr = ($strCssTr==='infraTrClara') ? 'infraTrEscura' : 'infraTrClara';
      $strResultadoMembros .= '<tr class="';
      if ($bolMostraPresenca && !$presente) {
        $strResultadoMembros .= 'ausente';
      } else {
        $strResultadoMembros .= $strCssTr;
      }
      $strResultadoMembros .= '">';
      $strResultadoMembros .= '<td valign="center" ' . $strStyleCheckComposicao . '>' . PaginaSEI::getInstance()->getTrCheck($i ++, $objColegiadoComposicaoDTO->getNumIdUsuario(), $objColegiadoComposicaoDTO->getStrNomeUsuario(), 'N', 'membros') . '</td>';
      $strResultadoMembros .= '<td>' . $objColegiadoComposicaoDTO->getStrNomeUsuario();
      switch($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()){
        case TipoMembroColegiadoRN::$TMC_SUPLENTE:
          $strResultadoMembros .= ' (Suplente)';
          break;
        case TipoMembroColegiadoRN::$TMC_EVENTUAL:
          $strResultadoMembros .= ' (Eventual)';
          break;
      }
      //colocar img de presidente se for o caso:
      $bolPresidente = ($objColegiadoComposicaoDTO->getNumIdUsuario()==$objSessaoJulgamentoDTO->getNumIdUsuarioPresidente());
      if ($bolPresidente) {
        $strResultadoMembros .= ' <b>(Presidente)</b>';
      }
      $strResultadoMembros .= '</td>';

      if ($bolMostraPresenca) {
        $strResultadoMembros .= '<td style="text-align: center">' . ($presente ? 'Sim' : 'N�o');
        if (!$presente) {
          $balao = $arrTitularesAusentes[$objColegiadoComposicaoDTO->getNumIdUsuario()]['motivo'];
          $strResultadoMembros .= '<a id="ancAusencia' . $objColegiadoComposicaoDTO->getNumIdUsuario() . '"';
          if ($balao==null) {
            $strResultadoMembros .= ' style="display:none;"';
          }
          $strResultadoMembros .= ' href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($balao) . '><img src="'.MdJulgarIcone::BALAO1.'" alt="bal�o de provimento" class="infraImg" style="vertical-align:top" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
        }

        $strResultadoMembros .= '&nbsp;</td>';
      }
      if ($bolExibirAcoes) {
        $strResultadoMembros .= '<td style="text-align: center">';
        if ($staSessao==SessaoJulgamentoRN::$ES_ABERTA || $staSessao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA || $staSessao==SessaoJulgamentoRN::$ES_SUSPENSA) {
          if ($bolPresidente) {
            $strLink = self::montarLink('sessao_julgamento_alterar_presidente', $_GET['acao'], null, '&id_usuario=' . $objColegiadoComposicaoDTO->getNumIdUsuario());
            $strResultadoMembros .= self::montarAncoraHref(MdJulgarIcone::MEDALHA, 'Alterar Presidente', $strLink) . '&nbsp;';
          }
          if ($presente) {
            if ($staSessao===SessaoJulgamentoRN::$ES_PAUTA_FECHADA || !$bolPresidente) {
              $strLink = self::montarLink('presenca_sessao_registrar', $_GET['acao'], null, '&id_usuario=' . $objColegiadoComposicaoDTO->getNumIdUsuario() . '&presente=N');
              $strResultadoMembros .= self::montarAncoraHref(MdJulgarIcone::AUSENTE, 'Registrar Aus�ncia', $strLink) . '&nbsp;';
            }
          } else {
            $strLink = self::montarLink('presenca_sessao_registrar', $_GET['acao'], null, '&id_usuario=' . $objColegiadoComposicaoDTO->getNumIdUsuario() . '&presente=S');
            $strResultadoMembros .= self::montarAncoraHref(MdJulgarIcone::PRESENTE, 'Registrar Presen�a', $strLink) . '&nbsp;';
          }

        } else {
          if (isset($arrTitularesAusentes[$objColegiadoComposicaoDTO->getNumIdUsuario()])) {
            $icone =  ($arrTitularesAusentes[$objColegiadoComposicaoDTO->getNumIdUsuario()]['parcial']==false ? MdJulgarIcone::USUARIO2 : MdJulgarIcone::USUARIO1);
            $strLink = self::montarLink('ausencia_sessao_registrar', $_GET['acao'], null, '&id_usuario=' . $objColegiadoComposicaoDTO->getNumIdUsuario());
            $strResultadoMembros .= self::montarAncoraDocumento($icone, 'Registrar Motivo de Aus�ncia', $strLink) . '&nbsp;';
          }
        }
          $strResultadoMembros .= '</td>';
        }
      $strResultadoMembros .= '</tr>' . "\n";
    }
    $strResultadoMembros .= '</table>';
    return $strResultadoMembros;
  }

  /**
   * retorna as tabelas html para exibi��o
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param $bolIfrDocumento
   * @param $strCssAcoes
   * @return TabelaHtmlDTO[]
   * @throws InfraException
   */
  public static function montarTabelasItens(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, $bolIfrDocumento, &$strCssAcoes): array
  {


    $strStyleCheck = '';
    $strCssAcoes = '';
    $idUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();
    $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();

    $bolAdmin = SessaoJulgamentoRN::isAdministradorSessao($objSessaoJulgamentoDTO);
    if (!SessaoJulgamentoRN::permiteSelecaoItens($objSessaoJulgamentoDTO)) {
      $strStyleCheck = 'style="display:none"';
    }

    $arrObjItemSessaoJulgamentoDTO = $objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
    $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(), 'IdUsuario');

    $arrObjProtocoloDTO = CacheSessaoJulgamentoRN::pesquisaAcessoDocumentos($arrObjItemSessaoJulgamentoDTO);
    $arrObjItemSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdSessaoBloco', true);

    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoDTO->setNumIdUsuario(-1);
    $objColegiadoComposicaoDTO->setStrNomeUsuario('Processos colocados em mesa ap�s a abertura da Sess�o');
    $objColegiadoComposicaoDTO->setNumIdUnidade(-1);
    $arrObjColegiadoComposicaoDTO[-1]=$objColegiadoComposicaoDTO;

    $arrObjTabelaHtmlDTO=[];
    foreach ($arrObjItemSessaoJulgamentoDTO as $numIdSessaoBloco=>$arrObjItemSessaoJulgamentoDTO2) {
      $objSessaoBlocoDTO = CacheSessaoJulgamentoRN::getObjSessaoBlocoDTO($numIdSessaoBloco);
      if ($objSessaoBlocoDTO->getStrSinAgruparMembro()=='S') {
        //s� separa os itens inclu�dos ap�s a abertura da sess�o se for mesa
        $dthInicioSessao = $objSessaoJulgamentoDTO->getDthInicio();
        $arrItensSessaoAberta = array();
        if ($dthInicioSessao!=null && $objSessaoBlocoDTO->getStrStaTipoItem()==TipoSessaoBlocoRN::$STA_MESA) {
          /**
           * @var ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
           */
          foreach ($arrObjItemSessaoJulgamentoDTO2 as $chave => $objItemSessaoJulgamentoDTO) {
            if (InfraData::compararDataHorasSimples($dthInicioSessao, $objItemSessaoJulgamentoDTO->getDthInclusao())>=1) {
              $objItemSessaoJulgamentoDTO->setStrNomeUsuarioRelator($arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()]->getStrNomeUsuario());
              $arrItensSessaoAberta[] = $objItemSessaoJulgamentoDTO;
              unset($arrObjItemSessaoJulgamentoDTO2[$chave]);
            }
          }
        }
        $arrObjItemSessaoJulgamentoDTO2 = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO2, 'IdUsuarioSessao', true);
        if (count($arrItensSessaoAberta)>0) {
          $arrObjItemSessaoJulgamentoDTO2[-1] = $arrItensSessaoAberta;
        }


        $numRegistrosBloco = 0;
        //pauta
        foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
          $numIdUsuario = $objColegiadoComposicaoDTO->getNumIdUsuario();
          if (isset($arrObjItemSessaoJulgamentoDTO2[$numIdUsuario])) {
            $bolUnidadeSessao = $objColegiadoComposicaoDTO->getNumIdUnidade()==$idUnidadeAtual;
            $arrObjItemSessaoJulgamentoDTO = $arrObjItemSessaoJulgamentoDTO2[$objColegiadoComposicaoDTO->getNumIdUsuario()];
            $idTabela = $numIdSessaoBloco . 'T' . $numIdUsuario;
            $numOrdemSequencial = $numRegistrosBloco;
            $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
            $bolOrdenarProcessos = false;
            if ($numRegistros>1 &&
                (
                    ($bolUnidadeSessao && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA))) ||
                    ($bolAdmin && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_ABERTA)))
                )
            ) {
              $bolOrdenarProcessos = true;
            }
            $arrObjItemSessaoJulgamentoDTO[0]->setArrObjColegiadoComposicaoDTO(array($objColegiadoComposicaoDTO));
            $strResultado = self::montarTabelaItem($objSessaoJulgamentoDTO, $bolIfrDocumento, $arrObjItemSessaoJulgamentoDTO, $bolOrdenarProcessos, $bolAdmin, $strStyleCheck, $numOrdemSequencial, $arrObjProtocoloDTO, $strCssAcoes);
            $numRegistrosBloco += $numRegistros;
            $objTabelaHtmlDTO = new TabelaHtmlDTO();
            $objTabelaHtmlDTO->setNumIdSessaoBloco($numIdSessaoBloco);
            $objTabelaHtmlDTO->setNumRegistros($numRegistros);
            $objTabelaHtmlDTO->setStrHtml($strResultado);
            $objTabelaHtmlDTO->setStrIdTabela($idTabela);
            $arrObjTabelaHtmlDTO[] = $objTabelaHtmlDTO;
          }
        }

      } else {

        $numOrdemSequencial = 0;
        $numRegistrosBloco = InfraArray::contar($arrObjItemSessaoJulgamentoDTO2);
        $bolOrdenarProcessos = false;
        if ($numRegistrosBloco>1 && $bolAdmin && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_ABERTA))
        ) {
          $bolOrdenarProcessos = true;
        }
        $strResultado = self::montarTabelaItem($objSessaoJulgamentoDTO, $bolIfrDocumento, $arrObjItemSessaoJulgamentoDTO2, $bolOrdenarProcessos, $bolAdmin, $strStyleCheck, $numOrdemSequencial, $arrObjProtocoloDTO, $strCssAcoes);
        $objTabelaHtmlDTO = new TabelaHtmlDTO();
        $objTabelaHtmlDTO->setNumIdSessaoBloco($numIdSessaoBloco);
        $objTabelaHtmlDTO->setNumRegistros($numRegistrosBloco);
        $objTabelaHtmlDTO->setStrHtml($strResultado);
        $objTabelaHtmlDTO->setStrIdTabela($numIdSessaoBloco . 'T');
        $arrObjTabelaHtmlDTO[] = $objTabelaHtmlDTO;
      }

    }

    return $arrObjTabelaHtmlDTO;

  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param bool $bolIfrDocumento
   * @param ItemSessaoJulgamentoDTO[] $arrObjItemSessaoJulgamentoDTO
   * @param bool $bolOrdenarProcessos
   * @param bool $bolAdmin
   * @param $strStyleCheck
   * @param $numOrdemSequencial
   * @param $arrObjProtocoloDTO
   * @param $strCssAcoes
   * @return string $strResultado
   * @throws InfraException
   */
  private static function montarTabelaItem(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, bool $bolIfrDocumento, array $arrObjItemSessaoJulgamentoDTO, bool $bolOrdenarProcessos, bool $bolAdmin, $strStyleCheck, $numOrdemSequencial, $arrObjProtocoloDTO, &$strCssAcoes): string
  {
//montartabela
    $exibeAcoes = false;
    $idProcedimento = $_GET['id_procedimento_sessao']??null;
    $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
    if ($numRegistros==0) {
      return '';
    }
    $strResultado = '';
    $objItemSessaoJulgamentoDTO = $arrObjItemSessaoJulgamentoDTO[0];

    $numIdUsuario = $objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao();
    $numIdSessaoJulgamento = $objSessaoJulgamentoDTO->getNumIdSessaoJulgamento();


    $numIdSessaoBloco=$objItemSessaoJulgamentoDTO->getNumIdSessaoBloco();
    $objSessaoBlocoDTO=CacheSessaoJulgamentoRN::getObjSessaoBlocoDTO($numIdSessaoBloco);
    $strCaption = 'Tabela de '.$objSessaoBlocoDTO->getStrDescricao();
    $idTabela = $numIdSessaoBloco.'T';
    $bolAgrupar=$objSessaoBlocoDTO->getStrSinAgruparMembro()=='S';

    $bolExibirNomeUsuarioMesa=false;

    if ($bolAgrupar) {
      $idTabela.=$numIdUsuario;
      $objColegiadoComposicaoDTO = $objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO()[0];
      if($objColegiadoComposicaoDTO->getNumIdUsuario()==-1){
        $bolExibirNomeUsuarioMesa=true;
        $idTabela = $numIdSessaoBloco.'T-1';
      }
      $strResultado = '<label class="infraLabelObrigatorio" style="font-size: 1rem">' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . '&nbsp;</label>';
    }
    if ($bolOrdenarProcessos) {
      $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=item_sessao_julgamento_ordenar&acao_origem=' . $_GET['acao'] . '&id_sessao_bloco=' . $numIdSessaoBloco . '&id_usuario=' . $numIdUsuario . '&id_sessao_julgamento=' . $numIdSessaoJulgamento);
      $strResultado .= '<a href="javascript:void(0);" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="'.MdJulgarIcone::ORDENAR.'" onclick="infraAbrirJanelaModal(\'' . $strLink . '\',600,610);" title="Ordenar Processos" alt="Ordenar Processos" class="infraImg" style="vertical-align: middle" /></a>&nbsp;';
    }

    $strResultado .= '<table width="99%" id="' . $idTabela . '" class="infraTable tblItens" summary="' . $strCaption . '">' . "\n";
    $strResultado .= '<thead><tr style="">';
    $strResultado .= '<th class="infraTh" width="1%" ' . $strStyleCheck . '>' . PaginaSEI::getInstance()->getThCheck('', $idTabela, 'style="display:none"') . "</th>\n";
    $strResultado .= '<th class="infraTh">Ordem</th>' . "\n";
    $strResultado .= '<th class="infraTh">Destaques</th>' . "\n";
    $strResultado .= '<th class="infraTh">Processo</th>' . "\n";

    $strResultado .= '<th class="infraTh">Documento</th>' . "\n";
    $strResultado .= '<th class="infraTh">Situa��o</th>' . "\n";
    $strResultado .= '<th class="infraTh tdAcoes">A��es</th>' . "\n";
    $strResultado .= '</tr></thead>' . "\n";
    $strResultado .= '<tbody>' . "\n";
    $strCssTr = '';
    for ($i = 0, $check = 0; $i<$numRegistros; $i ++) {
      $objItemSessaoJulgamentoDTO = $arrObjItemSessaoJulgamentoDTO[$i];
      $numIdItemSessaoJulgamento = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
      ++ $numOrdemSequencial;
      $strDestaque = DestaqueINT::montarIconeDestaques($objItemSessaoJulgamentoDTO->getArrObjDestaqueDTO(),
          $objSessaoJulgamentoDTO->getNumIdColegiado(),$objSessaoJulgamentoDTO, $numOrdemSequencial, $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento(), $qtd);

      //TR
      $strCssTr = ($strCssTr==='<tr class="infraTrClara"') ? '<tr class="infraTrEscura"' : '<tr class="infraTrClara"';
      if ($idProcedimento && $idProcedimento==$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()) {
        $strResultado .= '<tr class="infraTrMarcada"';
      } else {
        $strResultado .= $strCssTr;
      }
      $strResultado .= ' id="trItem' . $numIdItemSessaoJulgamento . '" data-sta-item="' . $objItemSessaoJulgamentoDTO->getStrStaSituacao() . '"';
      if($objItemSessaoJulgamentoDTO->isSetObjRevisaoItemDTO() && $objItemSessaoJulgamentoDTO->getObjRevisaoItemDTO()->getStrSinRevisado()=='S'){
        $strResultado .= ' data-revisado="S"';
      } else {
        $strResultado .= ' data-revisado="N"';
      }
      $strResultado .= ' data-destaques="' . $qtd . '">';

      //Checkbox
      if ($objItemSessaoJulgamentoDTO->getStrStaSituacao()==ItemSessaoJulgamentoRN::$STA_NORMAL) {
        $strResultado .= '<td class="tdCheck" valign="center" ' . $strStyleCheck . '>' . PaginaSEI::getInstance()->getTrCheck($check ++, $numIdItemSessaoJulgamento, $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo(), 'N', $idTabela) . '</td>';
      } else {
        $strResultado .= '<td class="tdCheck" ' . $strStyleCheck . '>';
        $strResultado .= PaginaSEI::getInstance()->getTrCheck($check ++, $numIdItemSessaoJulgamento, $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo(), 'N', $idTabela, 'style="display:none" disabled');
        $strResultado .= '</td>';
      }

      //Ordem
      $strResultado .= '<td class="tdOrdem" style="text-align: center">' . $numOrdemSequencial . '</td>';

      //Destaque/Elei��es
      $strResultado .= '<td class="tdDestaque" style="text-align: center">';
      $strResultado .= '<div class="divEleicao">';
      $strResultado .= VotoEleicaoINT::montarIconeVotacao($objSessaoJulgamentoDTO, $objItemSessaoJulgamentoDTO);
      $strResultado .= '</div>';
      $strResultado .= '<div class="divDestaque">';
      $strResultado.= $strDestaque;
      $strResultado .= '</div></td>';

      //Processo / Motivo


      $strResultado .= '<td class="tdProcesso" style="text-align: center">';
      $strResultado .= self::montarTdProcesso($objItemSessaoJulgamentoDTO, $arrObjProtocoloDTO, $bolExibirNomeUsuarioMesa);
      $strResultado .= '</td>';

      //Documentos
      $strResultado .= '<td class="tdDocumento" style="text-align: center">';
      $strResultado .= self::montarTdDocumento($bolIfrDocumento, $arrObjProtocoloDTO, $objItemSessaoJulgamentoDTO, $objSessaoJulgamentoDTO);
      $strResultado .= '</td>';

      //Situa��o
      $strResultado .= '<td class="tdSituacao" style="text-align: center">';
      $strResultado .= self::montarTdSituacao($objItemSessaoJulgamentoDTO);
      $strResultado .= '</td>';

      //A��es
      $strResultado .= '<td style="text-align: center" class="tdAcoes">';
      $strAcoes = self::montarAcoesTabelaJulgamento($objItemSessaoJulgamentoDTO, $objSessaoJulgamentoDTO, '&num_seq=' . $numOrdemSequencial);
      if ($strAcoes!='') {
        $exibeAcoes = true;
      }
      $strResultado .= $strAcoes;


      $strResultado .= '</td>';
      $strResultado .= '</tr>' . "\n";
    }
    $strResultado .= '</tbody></table><br />';
    if (!$exibeAcoes) {
      $strCssAcoes .= '#' . $idTabela . ' .tdAcoes {display:none}' . "\n";
    }

    return $strResultado;
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param ItemSessaoJulgamentoDTO[] $arrObjItemSessaoJulgamentoDTO
   * @param $numOrdemSequencial
   * @return string
   * @throws InfraException
   */
  public static function montarTabelaItemObservador(SessaoJulgamentoDTO $objSessaoJulgamentoDTO,  array $arrObjItemSessaoJulgamentoDTO, $numOrdemSequencial): string
  {
//montartabela
    $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
    if ($numRegistros==0) {
      return '';
    }
    $strResultado = '';
    $objItemSessaoJulgamentoDTO = $arrObjItemSessaoJulgamentoDTO[0];

    $numIdUsuario = $objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao();
    $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();

    $objDistribuicaoRN = new DistribuicaoRN();
    $arrObjEstadoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
    $arrObjEstadoDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjEstadoDistribuicaoDTO, 'StaDistribuicao');

    $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
    $arrSituacaoItem = InfraArray::converterArrInfraDTO($objItemSessaoJulgamentoRN->listarValoresStaSituacao(), 'Descricao', 'StaTipo');

    $numIdSessaoBloco=$objItemSessaoJulgamentoDTO->getNumIdSessaoBloco();
    $objSessaoBlocoDTO=CacheSessaoJulgamentoRN::getObjSessaoBlocoDTO($numIdSessaoBloco);
    $staItem = $objSessaoBlocoDTO->getStrStaTipoItem();
    $strCaption = 'Tabela de '.$objSessaoBlocoDTO->getStrDescricao();
    $idTabela = $numIdSessaoBloco.'T';
    $bolAgrupar=$objSessaoBlocoDTO->getStrSinAgruparMembro()=='S';

    $bolExibirNomeUsuarioMesa=false;

    if ($bolAgrupar) {
      $idTabela.=$numIdUsuario;
      $objColegiadoComposicaoDTO = $objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO()[0];
      if($objColegiadoComposicaoDTO->getNumIdUsuario()==-1){
        $bolExibirNomeUsuarioMesa=true;
        $idTabela = $numIdSessaoBloco.'T-1';
      }
      $strResultado = '<label class="infraLabelObrigatorio" style="font-size: 1rem">' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . '&nbsp;</label>';
    }
    $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
    $bolPermiteVisualizacaoPautaFechada=($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_ACESSO_OBSERVADOR_EXTERNO,false)==1);


    $strResultado .= '<table width="99%" id="' . $idTabela . '" class="infraTable tblItens" summary="' . $strCaption . '">' . "\n";
    $strResultado .= '<thead><tr style="">';
    $strResultado .= '<th class="infraTh" style="width: 5%">Ordem</th>' . "\n";
    $strResultado .= '<th class="infraTh" style="width: 38%">Processo</th>' . "\n";

    $strResultado .= '<th class="infraTh" style="width: 16%">Documento</th>' . "\n";
    $strResultado .= '<th class="infraTh" style="width: 17%">Situa��o</th>' . "\n";
    $strResultado .= '</tr></thead>' . "\n";
    $strResultado .= '<tbody>' . "\n";
    $strCssTr = '';
    for ($i = 0; $i<$numRegistros; $i ++) {
      $objItemSessaoJulgamentoDTO = $arrObjItemSessaoJulgamentoDTO[$i];
      ++ $numOrdemSequencial;
      //TR
      $strCssTr = ($strCssTr==='<tr class="infraTrClara>"') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
      $strResultado .= $strCssTr;

      //Ordem
      $strResultado .= '<td class="tdOrdem" style="text-align: center">' . $numOrdemSequencial . '</td>';

      //Processo / Motivo
      $strResultado .= '<td class="tdProcesso" style="text-align: center">';




      $strResultado .= '<a ';

      if ($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO) {
        $strResultado .= 'href="javascript:void(0);" onclick="alert(\'Sem acesso ao processo.\')" class="ancoraPadraoPreta"';
      } else {
        $strResultado .= 'href="' . SessaoSEIExterna::getInstance()->assinarLink('processo_acesso_externo_consulta.php?id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()) . '" target="_blank" class="ancoraPadraoAzul"';
      }

      $strResultado .= ' title="' . PaginaSEIExterna::tratarHTML($objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . '" >' . PaginaSEIExterna::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo()) . '</a>';

      $strResultado .= self::complementarTdProcesso($objItemSessaoJulgamentoDTO, $bolExibirNomeUsuarioMesa);
      $strResultado .= '</td>';


      $arrSituacaoPermitidaDocumento=array(SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA);
      if($bolPermiteVisualizacaoPautaFechada){
        $arrSituacaoPermitidaDocumento[]=SessaoJulgamentoRN::$ES_PAUTA_FECHADA;
      }
      //Documentos
      $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
      $strDocumento = '';
      if ($arrObjItemSessaoDocumentoDTO==null) {
        $arrObjItemSessaoDocumentoDTO = array();
      }
      foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
        /* @var $objItemSessaoDocumentoDTO ItemSessaoDocumentoDTO */
        if ($strDocumento!='') {
          $strDocumento .= '<br />';
        }
        $strTooltip = PaginaSEI::montarTitleTooltip($objItemSessaoDocumentoDTO->getStrNomeSerie(), $objItemSessaoDocumentoDTO->getStrNomeUsuario());
        if ($staItem==TipoSessaoBlocoRN::$STA_REFERENDO || in_array($staSessao, $arrSituacaoPermitidaDocumento)) {
          $strLinkDocumento = SessaoSEIExterna::getInstance()->assinarLink(ConfiguracaoSEI::getInstance()->getValor('SEI', 'URL') . '/documento_consulta_externa.php?id_procedimento=' . $arrObjItemSessaoJulgamentoDTO[$i]->getDblIdProcedimentoDistribuicao() . '&id_documento=' . $objItemSessaoDocumentoDTO->getDblIdDocumento());
          $strDocumento .= '<a onclick="infraLimparFormatarTrAcessada(this.parentNode.parentNode);infraAbrirJanelaModal(\'' . $strLinkDocumento . '\',900,650,false);" href="#" ' . $strTooltip . ' tabindex="' . PaginaSEIExterna::getInstance()->getProxTabTabela() . '" class="ancoraPadraoAzul"  title="' . PaginaSEIExterna::tratarHTML($objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado()) . '">' . PaginaSEIExterna::tratarHTML($objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado()) . '</a>';
        } else {
          $strDocumento .= '<a href="javascript:void(0);" onclick="alert(\'Sem acesso ao documento.\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
        }
      }
      $strResultado .= '<td style="text-align: center">' . $strDocumento . '</td>';

      //Situa��o
      $strResultado .= '<td class="tdSituacao" style="text-align: center">';
      if ($objItemSessaoJulgamentoDTO->getStrStaSituacao()!==ItemSessaoJulgamentoRN::$STA_NORMAL) {
        $strResultado .= PaginaSEIExterna::tratarHTML($arrSituacaoItem[$objItemSessaoJulgamentoDTO->getStrStaSituacao()]);
      } else {
        $strResultado .= PaginaSEIExterna::tratarHTML($arrObjEstadoDistribuicaoDTO[$objItemSessaoJulgamentoDTO->getStrStaDistribuicao()]->getStrDescricao());
      }
      $strResultado .= '</td>';
      $strResultado .= '</tr>' . "\n";
    }
    $strResultado .= '</tbody></table><br />';

    return $strResultado;
  }
  /**
   * @param bool $bolIfrDocumento
   * @param ProtocoloDTO[] $arrObjProtocoloDTO
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return string
   * @throws InfraException
   */
  public static function montarTdDocumento(bool $bolIfrDocumento, array $arrObjProtocoloDTO, ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO,  SessaoJulgamentoDTO $objSessaoJulgamentoDTO): string
  {
    $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
    $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();
    $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
    $numIdItemSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
    $numIdSessaoBloco=$objItemSessaoJulgamentoDTO->getNumIdSessaoBloco();
    $staItem=CacheSessaoJulgamentoRN::getObjSessaoBlocoDTO($numIdSessaoBloco)->getStrStaTipoItem();
    $bolManual=$objItemSessaoJulgamentoDTO->getStrSinManual()==='S';
    $strDocumento = '';
    $bolProvimentoExibido=false;
    $bolAgruparTooltipsReferendo = $staItem==TipoSessaoBlocoRN::$STA_REFERENDO && InfraArray::contar($arrObjItemSessaoDocumentoDTO)>1;
    $idProcedimento = $_GET['id_procedimento_sessao']??null;
    if ($idProcedimento==$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()) {
      $strAcaoRetorno = 'procedimento_trabalhar';
    } else {
      $strAcaoRetorno = PaginaSEI::getInstance()->getAcaoRetorno();
    }
    if ($arrObjItemSessaoDocumentoDTO==null) {
      $arrObjItemSessaoDocumentoDTO = array();
    }
    $bolAdmin=($numIdUnidadeAtual==$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado() && SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar'));

    $strBalaoTooltip='';
    if ($bolAgruparTooltipsReferendo) { //uma div fora do foreach
      $strDocumento .= '<div style="display: table-row">';
      //manter ordem para scripts de teste
      InfraArray::ordenarArrInfraDTO($arrObjItemSessaoDocumentoDTO, 'ProtocoloDocumentoFormatado', InfraArray::$TIPO_ORDENACAO_ASC);
    }
    foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
      /* @var $objItemSessaoDocumentoDTO ItemSessaoDocumentoDTO */
      if ($strDocumento!='' && $staItem!=TipoSessaoBlocoRN::$STA_REFERENDO) {
        $strDocumento .= '<hr style="margin: 2px 0;border-bottom: 0;">';
      }
      $strDocumento .= '<div style="display: table-row;vertical-align: middle">';
      if (!$bolAgruparTooltipsReferendo) {
        $strDocumento .= '<div style="display: table-cell;vertical-align: middle">';
      }

      $strTooltip = PaginaSEI::montarTitleTooltip($objItemSessaoDocumentoDTO->getStrNomeSerie(), $objItemSessaoDocumentoDTO->getStrNomeUsuario());
      $dblIdDocumento=$objItemSessaoDocumentoDTO->getDblIdDocumento();
      if (isset($arrObjProtocoloDTO[$dblIdDocumento]) && !isset($arrDocumentosBloqueados[$dblIdDocumento][$numIdUnidadeAtual])) {
        $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=documento_visualizar&id_documento=' . $dblIdDocumento);

        if (!$bolIfrDocumento) {
          $strDocumento .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal"  style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
        } else {
          $strDocumento .= '<a href="javascript:void(0);" onclick="abrirDocumento(\'' . $strLink . '\',this);" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
        }
      } else {
        $strDocumento .= '<a href="javascript:void(0);" onclick="alert(\'Sem acesso ao documento.\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
      }
      $strDocumento .= '</div>';
      if (isset($arrDocumentosBloqueados[$dblIdDocumento]) && ($bolAdmin || $objItemSessaoDocumentoDTO->getNumIdUnidade()==$numIdUnidadeAtual)) {
        //exibir icone de bloqueio do documento
        $strLink=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=bloqueio_item_sess_unidade_alterar&id_item_sessao_documento=' . $objItemSessaoDocumentoDTO->getNumIdItemSessaoDocumento().'&id_item_sessao_julgamento='.$numIdItemSessaoJulgamento);
        $strDocumento .= '<a href="javascript:void(0);" onclick="infraAbrirJanelaModal(\''.$strLink.'\',800,600)" ' . PaginaSEI::montarTitleTooltip('Com restri��o', 'Restri��o de acesso') . '><img src="'.MdJulgarIcone::ACESSO_RESTRITO1.'" alt="Restri��o de acesso" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
      }
      if (!$bolManual && !$bolProvimentoExibido) {
        if ($bolAgruparTooltipsReferendo) {
          $bolProvimentoExibido=true;
          $strBalaoTooltip .= '<div style="display: table-cell;vertical-align: middle">';
        }else{
          $strBalaoTooltip .= '<div style="display: table-cell;">';
        }

        $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
        /** @var JulgamentoParteDTO $objJulgamentoParteDTO */
        foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
          $arrObjVotoParteDTO = $objJulgamentoParteDTO->getArrObjVotoParteDTO();
          if (isset($arrObjVotoParteDTO[$objItemSessaoDocumentoDTO->getNumIdUsuario()])) {
            /** @var VotoParteDTO $objVotoParteDTO */
            $objVotoParteDTO = $arrObjVotoParteDTO[$objItemSessaoDocumentoDTO->getNumIdUsuario()];
            $balao = InfraString::transformarCaixaAlta($objVotoParteDTO->getStrConteudoProvimento()) . ' ' . $objVotoParteDTO->getStrComplemento();
            if ($objVotoParteDTO->getStrStaVotoParte()==VotoParteRN::$STA_ACOMPANHA && $objVotoParteDTO->getNumIdProvimento()==null) {
              $numIdUsuarioAcompanhado = $objVotoParteDTO->getNumIdUsuarioVotoParte();
              if ($numIdUsuarioAcompanhado==null) {
                $numIdUsuarioAcompanhado = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
              }
              if (isset($arrObjVotoParteDTO[$numIdUsuarioAcompanhado])) {
                $balao = $arrObjVotoParteDTO[$numIdUsuarioAcompanhado]->getStrConteudoProvimento() . ' ' . $arrObjVotoParteDTO[$numIdUsuarioAcompanhado]->getStrComplemento();
              }

            }
            $icone = MdJulgarIcone::BALAO1;
            if ($objItemSessaoJulgamentoDTO->getStrDispositivo()!=null && $objItemSessaoJulgamentoDTO->getStrStaSituacao()!=ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA && $objVotoParteDTO->getNumIdUsuario()==$objJulgamentoParteDTO->getNumUsuarioVencedor()) {
              $icone = MdJulgarIcone::BALAO2;
            }
            //#148103 - permitir alterar provimento de referendo sem necessidade de retirar documentos e disponibilizar novamente (pauta aberta ou fechada)
            $acaoAlterarProvimento = '';

            if (($staSessao===SessaoJulgamentoRN::$ES_PAUTA_ABERTA || $staSessao===SessaoJulgamentoRN::$ES_PAUTA_FECHADA) && $objItemSessaoDocumentoDTO->getNumIdUnidade()==$numIdUnidadeAtual && $objItemSessaoJulgamentoDTO->getNumIdUnidadeSessao()==$numIdUnidadeAtual) {
              $strLink = self::montarLink('provimento_disponibilizado_alterar', $strAcaoRetorno, $numIdItemSessaoJulgamento, '&id_procedimento='.$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
              $acaoAlterarProvimento = 'onclick="abrirDocumento(\'' . $strLink . '\',this);" ';
            }
            $strBalaoTooltip .= '<a href="javascript:void(0);" ' .$acaoAlterarProvimento . PaginaSEI::montarTitleTooltip($balao, $objJulgamentoParteDTO->getStrDescricao()) . '><img src="' . $icone . '" alt="bal�o de provimento" class="infraImg" style="vertical-align:top" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
          }
        }
        $strBalaoTooltip .= '</div>';
        if (!$bolAgruparTooltipsReferendo){
          $strDocumento.=$strBalaoTooltip;
          $strBalaoTooltip='';
        }
      }
      if (!$bolAgruparTooltipsReferendo) {
        $strDocumento .= '</div>';
      }
    }
    if ($bolAgruparTooltipsReferendo) {
      $strDocumento.=$strBalaoTooltip;
      $strDocumento .= '</div>';
    }
    return $strDocumento;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @return string
   */
  public static function montarIconeRevisao(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {
    $icone = MdJulgarIcone::VERIFICACAO1;
    if (!$objItemSessaoJulgamentoDTO->isSetObjRevisaoItemDTO()) {
      $strRevisao = 'N�o revisado';
      $sinRevisado = 'S';
    } else {
      /** @var RevisaoItemDTO $objResumoItemDTO */
      $objResumoItemDTO = $objItemSessaoJulgamentoDTO->getObjRevisaoItemDTO();
      if ($objResumoItemDTO->getStrSinRevisado()=='N') {
        $sinRevisado = 'S';
        $strRevisao = 'N�o revisado por ' . $objResumoItemDTO->getStrSiglaUsuario() . ' em ' . $objResumoItemDTO->getDthRevisao();
      } else {
        $sinRevisado = 'N';
        $strRevisao = 'Revisado por ' . $objResumoItemDTO->getStrSiglaUsuario() . ' em ' . $objResumoItemDTO->getDthRevisao();
        $icone = MdJulgarIcone::VERIFICACAO2;
      }
    }

    $strLink = SessaoJulgamentoINT::montarLink('revisao_item_cadastrar', '',$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento(),  '&sin_revisado=' . $sinRevisado);
    $strResultado = SessaoJulgamentoINT::montarAncoraHref($icone, $strRevisao, $strLink);

    return '&nbsp;'.$strResultado;
  }

  /**
   * ObjItemSessaoJulgamento necessita ter processado os votos composi��o anterior
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @return string
   * @throws InfraException
   */
  public static function montarTdSituacao(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {
    $staSituacao = $objItemSessaoJulgamentoDTO->getStrStaSituacao();
    if ($staSituacao!==ItemSessaoJulgamentoRN::$STA_NORMAL) {
      $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
      $arrSituacaoItem = InfraArray::converterArrInfraDTO($objItemSessaoJulgamentoRN->listarValoresStaSituacao(), 'Descricao', 'StaTipo');
      $strResultado = $arrSituacaoItem[$staSituacao];
      $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
    } else {
      $objDistribuicaoRN = new DistribuicaoRN();
      $arrObjEstadoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
      $arrObjEstadoDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjEstadoDistribuicaoDTO, 'StaDistribuicao');
      $strResultado = $arrObjEstadoDistribuicaoDTO[$objItemSessaoJulgamentoDTO->getStrStaDistribuicao()]->getStrDescricao();
    }
    $numVotosComposicaoAnterior = $objItemSessaoJulgamentoDTO->getNumVotosComposicaoAnterior();
    if ($numVotosComposicaoAnterior>0) {
      $objSessaoJulgamentoDTO = CacheSessaoJulgamentoRN::getObjSessaoJulgamentoDTO($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();
      if (!in_array($staSessao, array(SessaoJulgamentoRN::$ES_CANCELADA, SessaoJulgamentoRN::$ES_ENCERRADA, SessaoJulgamentoRN::$ES_FINALIZADA))) {
        $str = 'Aten��o: ' . $numVotosComposicaoAnterior . ' membro(s) do colegiado atual n�o poder�(�o) votar neste processo!';
        $strResultado .= '<a href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($str) . '><img src="' . MdJulgarIcone::VOTACAO_EXCESSO . '" class="infraImg" alt="Excesso de votos" style="vertical-align:middle" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
      }
    }
    return $strResultado;
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param $arrObjProtocoloDTO
   * @param bool $bolExibirNomeUsuarioMesa
   * @return string
   * @throws InfraException
   */
  private static function montarTdProcesso(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, $arrObjProtocoloDTO, bool $bolExibirNomeUsuarioMesa): string
  {
    $strResultado='';

//$strResultado .= AnotacaoINT::montarIconeAnotacao($objItemSessaoJulgamentoDTO->getObjAnotacaoDTO(), $bolAcaoRegistrarAnotacao, $objItemSessaoJulginamentoDTO->getDblIdProcedimentoDistribuicao(),  '&id_item_sessao_julgamento=' . $numIdItemSessaoJulgamento);
    $strClasseProcesso = 'protocoloNormal';
    if ($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO) {
      $strClasseProcesso = 'processoVisualizadoSigiloso';
    }

    $strProtocoloProcesso = $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo();
    $posPrimeiroPonto = strpos($strProtocoloProcesso, '.');
    if ($posPrimeiroPonto!==false) {
      $strProtocoloProcesso = substr($strProtocoloProcesso, 0, $posPrimeiroPonto) . '<wbr>' . substr($strProtocoloProcesso, $posPrimeiroPonto);
    }

    if (isset($arrObjProtocoloDTO[$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()])) {
      $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
      $strResultado .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objItemSessaoJulgamentoDTO->getStrDescricaoProtocolo(), $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . ' class="' . $strClasseProcesso . '"  style="font-size:1em !important">' . $strProtocoloProcesso . '</a>';
    } else {
      $strResultado .= '<a onclick="alert(\'Sem acesso ao processo.\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objItemSessaoJulgamentoDTO->getStrDescricaoProtocolo(), $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . ' class="' . $strClasseProcesso . '"  style="font-size:1em !important">' . $strProtocoloProcesso . '</a>';
    }
    $strResultado .= self::complementarTdProcesso($objItemSessaoJulgamentoDTO, $bolExibirNomeUsuarioMesa);
    return $strResultado;
  }

  /**
   * @param bool $bolAcaoJulgar
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param bool $bolAcaoConsultarJulgamento
   * @param bool $bolResumo
   * @return string
   */
  public static function montarIconeJulgar(bool $bolAcaoJulgar, ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, bool $bolAcaoConsultarJulgamento, bool $bolResumo=false): string
  {
    $bolManual = $objItemSessaoJulgamentoDTO->getStrSinManual()==='S';
    $idItemSessaoJulgamento= $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
    $strResultado='';
    $icone = MdJulgarIcone::JULGAMENTO;
    if ($bolManual) {
      $icone = MdJulgarIcone::MARTELO_M_AM;
    }
    $descricao = '';
    if ($bolAcaoJulgar) {
      $descricao = $bolManual ? 'Julgamento Manual' : 'Julgar';
      if ($objItemSessaoJulgamentoDTO->getStrDispositivo()!=null) {
        $icone = $bolManual ? MdJulgarIcone::MARTELO_M_VD : MdJulgarIcone::MARTELO_VD;
      } else if ($objItemSessaoJulgamentoDTO->getStrSinPossuiVotos()==='S') {
        $icone = $bolManual ? MdJulgarIcone::MARTELO_M_AM : MdJulgarIcone::MARTELO_AM;
      }
    } else if ($bolAcaoConsultarJulgamento) {
      $descricao = $bolManual ? 'Resultado do Julgamento' : 'Consultar Vota��o';
      if ($objItemSessaoJulgamentoDTO->getStrStaSituacao()===ItemSessaoJulgamentoRN::$STA_JULGADO || $objItemSessaoJulgamentoDTO->getStrDispositivo()!=null) {
        $icone = $bolManual ? MdJulgarIcone::MARTELO_M_VD : MdJulgarIcone::MARTELO_VD;
      } else if ($objItemSessaoJulgamentoDTO->getStrSinPossuiVotos()==='S') {
        $icone = $bolManual ? MdJulgarIcone::MARTELO_M_AM : MdJulgarIcone::MARTELO_AM;
      }
    }
    if ($bolAcaoJulgar || $bolAcaoConsultarJulgamento) {
      $strLink = self::montarLink($bolManual ? 'item_sessao_julgamento_manual' : 'item_sessao_julgamento_julgar', $_GET['acao'], $idItemSessaoJulgamento, '');
      $strResultado = self::montarAncoraHref($icone, $descricao, $strLink);
      if($bolResumo){
        $strResultado = '<a href="javascript:void(0);" onclick="abrirJanela(\'' .$strLink . '\',this);" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="' . $icone . '" title="'.$descricao.'" alt="'.$descricao.'" class="infraImg" /></a>';
      }

    }
    return $strResultado;
  }

  public static function montarDivFiltros($strClasses)
  {
    echo "<div class='$strClasses'>\n";
    echo "  <fieldset id='fldFiltros' class='mr-2 flex-grow-1 mr-md-2 flex-md-grow-0 infraFieldset'>\n";
    echo "    <legend class='infraLegend' style='padding-top: 1px;'>&nbsp;Filtros&nbsp;</legend>";
    echo "    <div id='divChkDestaque' class='infraDivCheckbox ml-3 mr-2 my-1'>\n";
    echo "      <input id='chkDestaque' name='chkDestaque' type='checkbox' class='infraCheckbox' onchange='processarFiltros();'>";
    echo "      <label id='lblDestaque' for='chkDestaque' class='ml-1'>Diverg�ncias</label>";
    echo "    </div>";
    echo "    <div id='divChkRevisao' class='infraDivCheckbox ml-3 mr-2 my-1'>";
    echo "      <input id='chkRevisao' name='chkRevisao' type='checkbox' class='infraCheckbox' onchange='processarFiltros();'>";
    echo "      <label id='lblRevisao' for='chkRevisao' class='ml-1'>N�o revisados</label>";
    echo "    </div>";
    echo "  </fieldset>";
    echo "</div>";
  }

  /**
   * monta �rea de dados com a linha de label de cada bloco
   * @param $idSessaoBloco
   * @param bool $bolPermiteSelecaoItens
   * @param bool $bolCheckItensNaoVotados
   * @param $strDescricao
   * @param int $numRegistrosBloco
   * @return void
   */
  public static function montarBarraBloco($idSessaoBloco, bool $bolPermiteSelecaoItens, bool $bolCheckItensNaoVotados, $strDescricao, int $numRegistrosBloco): void
  {
    PaginaSEI::getInstance()->abrirAreaDados('3.4em');

    echo "<label id='lblTabela$idSessaoBloco' style='display:block;position:absolute' class='infraLabelTitulo infraCorBarraSuperior'>\n";
    echo "<img onclick='exibe(\"#divBloco$idSessaoBloco\",this);' alt='Exibir/Ocultar $strDescricao' title='Exibir/Ocultar $strDescricao' src='" . PaginaSEI::getInstance()->getIconeOcultar() . "' />";
    if ($bolPermiteSelecaoItens) {
      echo "<img src='/infra_css/svg/check.svg' id='imgSelPautaCheck' title='Selecionar Tudo' alt='Selecionar Tudo' class='infraImg' onclick='selecionarTodos(\"#divBloco$idSessaoBloco\",this);'>";
    }
    if ($bolCheckItensNaoVotados) {
      echo "<img onclick='selecionaNaoVotados(\"#divBloco$idSessaoBloco\");' id='imgBloco${idSessaoBloco}Check' alt='Selecionar itens n�o votados' src='" . MdJulgarIcone::SELECIONAR_RESTANTES . " title='Selecionar itens n�o votados'>";
    }
    echo ' ' . $strDescricao . '<div class="resumo">';
    if ($numRegistrosBloco) {
      if ($numRegistrosBloco==1) {
        echo '1 Item';
      } else {
        echo $numRegistrosBloco . ' Itens';
      }
    }
    echo "</div></label>";
    PaginaSEI::getInstance()->fecharAreaDados();
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @param bool $bolExibirNomeUsuarioMesa
   * @return string
   * @throws InfraException
   */
  private static function complementarTdProcesso(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO,  bool $bolExibirNomeUsuarioMesa): string
  {
    $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
    $numIdSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento();
    $staItem=$objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco();
    $strResultado='';
    $bolExibirNomeRelator = false;
    if ($staItem==TipoSessaoBlocoRN::$STA_PAUTA && $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao()!=$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()) {
      $bolExibirNomeRelator = true;
      $strResultado = '<p style="margin:0;line-height: 18px">(Pedido de Vista)</p>' . "\n";
    }

    if ($staItem==TipoSessaoBlocoRN::$STA_MESA) {
      $strResultado .= '<p style="margin:0;line-height: 18px">(' . $objItemSessaoJulgamentoDTO->getStrDescricaoMotivoMesa() . ")</p>\n";
      if ($bolExibirNomeUsuarioMesa) {
        $strResultado .= '<p style="margin:0;line-height: 18px"><b>' . $objItemSessaoJulgamentoDTO->getStrNomeUsuarioRelator() . "</b></p>\n";
      }
      if ($objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao()!=$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()) {
        $bolExibirNomeRelator = true;

      }
    }
    if ($bolExibirNomeRelator) {
      $numIdUsuarioRelator = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
      $arrObjColegiadoComposicaoDTO = CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($numIdSessaoJulgamento);
      if (!isset($arrObjColegiadoComposicaoDTO[$numIdUsuarioRelator])) {
        $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO2->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
        $objItemSessaoJulgamentoDTO2->retNumIdSessaoJulgamento();
        $objItemSessaoJulgamentoDTO2->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_ASC);
        $objItemSessaoJulgamentoDTO2->setNumMaxRegistrosRetorno(1);
        $objItemSessaoJulgamentoDTO2 = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO2);
        $arrObjColegiadoComposicaoDTO = CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($objItemSessaoJulgamentoDTO2->getNumIdSessaoJulgamento());

      }
      if (isset($arrObjColegiadoComposicaoDTO[$numIdUsuarioRelator])) {
        $strResultado .= '<p style="margin:0;line-height: 18px">Relator: <b>' . $arrObjColegiadoComposicaoDTO[$numIdUsuarioRelator]->getStrNomeUsuario() . "</b></p>\n";
      }
    }
    return $strResultado;
  }

}

?>