<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);
  PaginaSEI::getInstance()->salvarCamposPost(array('hdnTipoHistorico'));

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento','id_sessao_julgamento','id_procedimento_sessao','arvore','id_colegiado','id_distribuicao'));

  switch($_GET['acao']){
    case 'sessao_julgamento_historico':
      $strTitulo = 'Hist�rico da Sess�o';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();

  $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
  $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($_GET['id_sessao_julgamento']);
  $objAndamentoSessaoDTO->retStrNomeUsuario();
  $objAndamentoSessaoDTO->retStrSiglaUsuario();
  $objAndamentoSessaoDTO->retStrSiglaUnidade();
  $objAndamentoSessaoDTO->retStrDescricaoUnidade();
  $objAndamentoSessaoDTO->retNumIdAndamentoSessao();
  $objAndamentoSessaoDTO->retDthExecucao();
  $objAndamentoSessaoDTO->retStrNomeTarefaSessao();
  $objAndamentoSessaoDTO->setOrdNumIdAndamentoSessao(InfraDTO::$TIPO_ORDENACAO_DESC);

  $strStaTipoHistorico=PaginaSEI::getInstance()->recuperarCampo('hdnTipoHistorico',ProcedimentoRN::$TH_RESUMIDO);

  if ($strStaTipoHistorico==ProcedimentoRN::$TH_RESUMIDO){
    $objAndamentoSessaoDTO->setStrSinHistoricoResumido('S');
  }

  //PaginaSEI::getInstance()->prepararOrdenacao($objAndamentoSessaoDTO, 'Execucao', InfraDTO::$TIPO_ORDENACAO_ASC);
  //PaginaSEI::getInstance()->prepararPaginacao($objAndamentoSessaoDTO);

  $objAndamentoSessaoRN = new AndamentoSessaoRN();
  $arrObjAndamentoSessaoDTO = $objAndamentoSessaoRN->consultarHistorico($objAndamentoSessaoDTO);

  //PaginaSEI::getInstance()->processarPaginacao($objAndamentoSessaoDTO);
  $numRegistros = InfraArray::contar($arrObjAndamentoSessaoDTO);

  if ($numRegistros > 0){

    $bolCheck = false;

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Andamentos da Sess�o.';
    $strCaptionTabela = 'Andamentos da Sess�o';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    $strResultado .= '<th class="infraTh" width="20%" align="center">Data/Hora</th>'."\n";
    $strResultado .= '<th class="infraTh" width="10%" align="center">Unidade</th>'."\n";
    $strResultado .= '<th class="infraTh" width="10%" align="center">Usu�rio</th>'."\n";
    $strResultado .= '<th class="infraTh">Descri��o</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      $strResultado .= '<tr class="infraTrClara">';
      $tooltipUnidade = '<a class="ancoraSigla" alt="' . $arrObjAndamentoSessaoDTO[$i]->getStrDescricaoUnidade() . '" title="' . $arrObjAndamentoSessaoDTO[$i]->getStrDescricaoUnidade() . '">' . $arrObjAndamentoSessaoDTO[$i]->getStrSiglaUnidade() . '</a>';
      $tooltipUsuario = '<a class="ancoraSigla" alt="' . $arrObjAndamentoSessaoDTO[$i]->getStrNomeUsuario().'" title="'.$arrObjAndamentoSessaoDTO[$i]->getStrNomeUsuario().'">'.$arrObjAndamentoSessaoDTO[$i]->getStrSiglaUsuario(). '</a>';

      $strResultado .= '<td align="center">'.$arrObjAndamentoSessaoDTO[$i]->getDthExecucao().'</td>';
      $strResultado .= '<td align="center">'.$tooltipUnidade.'</td>';
      $strResultado .= '<td align="center">'.$tooltipUsuario.'</td>';
      $strResultado .= '<td>'.nl2br($arrObjAndamentoSessaoDTO[$i]->getStrNomeTarefaSessao()).'</td>';
      $strResultado .= '</tr>'."\n";
    }
    $strResultado .= '</table>';
  }



  if ($_GET['acao_origem']=='sessao_julgamento_pautar'){
    $strLinkRetorno=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_pautar&acao_origem='.$_GET['acao']);
    $arrComandos[] = '<button type="button" accesskey="V" name="btnVoltar" id="btnVoltar" value="Voltlar" onclick="location.href=\''.$strLinkRetorno.'\';" class="infraButton"><span class="infraTeclaAtalho">V</span>oltar</button>';
  } else {
    //$arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" id="btnFechar" value="Fechar" onclick="parent.document.getElementById(\'frmSessaoJulgamentoCadastro\').submit();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

  if ($strStaTipoHistorico==ProcedimentoRN::$TH_PARCIAL){
    $strLinkTipoHistorico = '<a id="ancTipoHistorico" onclick="verHistorico(\''.ProcedimentoRN::$TH_RESUMIDO.'\');" class="ancoraPadraoPreta">Ver hist�rico resumido</a>';
  }else {
    $strLinkTipoHistorico = '<a id="ancTipoHistorico" onclick="verHistorico(\''.ProcedimentoRN::$TH_PARCIAL.'\');" class="ancoraPadraoPreta">Ver hist�rico completo</a>';
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>

function inicializar(){
  //document.getElementById('btnFechar').focus();
  infraEfeitoTabelas();
}
  function verHistorico(valor){
  document.getElementById('hdnTipoHistorico').value = valor;
  document.getElementById('frmAndamentoSessaoLista').submit();
  }
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmAndamentoSessaoLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->abrirAreaDados('2.5em');
  echo $strLinkTipoHistorico.'&nbsp;&nbsp;&nbsp;&nbsp;'.$strLinkTipoHistoricoTotal.'<br />';
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  ?>
  <input type="hidden" id="hdnTipoHistorico" name="hdnTipoHistorico" value="<?=$strStaTipoHistorico;?>" />
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>