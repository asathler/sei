<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 11/11/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class PedidoVistaDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'pedido_vista';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdPedidoVista','id_pedido_vista');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Pedido','dth_pedido');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH,'Devolucao','dth_devolucao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdDistribuicao','id_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUnidade','id_unidade');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinPendente','sin_pendente');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DBL,'IdProcedimentoDistribuicao','id_procedimento','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUnidadeRelatorDistribuicao','id_unidade_relator','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaDistribuicao','sta_distribuicao','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaUltimoDistribuicao','sta_ultimo','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoVersaoDistribuicao','id_colegiado_versao','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoColegiadoVersao','id_colegiado','colegiado_versao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeColegiadoVersao','nome','colegiado_versao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Motivo');

    $this->configurarPK('IdPedidoVista',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdDistribuicao', 'distribuicao', 'id_distribuicao');
    $this->configurarFK('IdUsuario', 'usuario', 'id_usuario');
    $this->configurarFK('IdUnidade', 'unidade', 'id_unidade');
    $this->configurarFK('IdColegiadoVersaoDistribuicao','colegiado_versao','id_colegiado_versao');
  }
}
?>