<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 03/07/2021 - criado por alv77
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__.'/../../../../SEI.php';

class TipoSessaoBlocoINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdTipoSessao=''): string
  {
    $objTipoSessaoBlocoDTO = new TipoSessaoBlocoDTO();
    $objTipoSessaoBlocoDTO->retNumIdTipoSessaoBloco();
    $objTipoSessaoBlocoDTO->retNumDescricao();

    if ($numIdTipoSessao!==''){
      $objTipoSessaoBlocoDTO->setNumIdTipoSessao($numIdTipoSessao);
    }

    $objTipoSessaoBlocoDTO->setOrdNumDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objTipoSessaoBlocoRN = new TipoSessaoBlocoRN();
    $arrObjTipoSessaoBlocoDTO = $objTipoSessaoBlocoRN->listar($objTipoSessaoBlocoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjTipoSessaoBlocoDTO, 'IdTipoSessaoBloco', 'Descricao');
  }

  public static function montarSelectStaTipoItem($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado): string
  {

    $arrObjTipoItemTipoSessaoBlocoDTO = TipoSessaoBlocoRN::listarValoresTipoItem();

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjTipoItemTipoSessaoBlocoDTO, 'StaTipo', 'Descricao');

  }
}
?>