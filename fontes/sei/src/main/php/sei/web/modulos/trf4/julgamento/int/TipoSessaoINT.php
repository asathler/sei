<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 03/07/2021 - criado por alv77
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__.'/../../../../SEI.php';

class TipoSessaoINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objTipoSessaoDTO = new TipoSessaoDTO();
    $objTipoSessaoDTO->retNumIdTipoSessao();
    $objTipoSessaoDTO->retStrDescricao();

    if ($strValorItemSelecionado!=null){
      $objTipoSessaoDTO->setBolExclusaoLogica(false);
      $objTipoSessaoDTO->adicionarCriterio(array('SinAtivo','IdTipoSessao'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objTipoSessaoDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objTipoSessaoRN = new TipoSessaoRN();
    $arrObjTipoSessaoDTO = $objTipoSessaoRN->listar($objTipoSessaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjTipoSessaoDTO, 'IdTipoSessao', 'Descricao');
  }
}
?>