<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('colegiado_versao_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);



  $strParametros = '';

  if (isset($_GET['id_colegiado'])){
  	$strParametros .= "&id_colegiado=".$_GET['id_colegiado'];
  }


  switch($_GET['acao']){

    case 'colegiado_versao_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Composi��o do Colegiado','Selecionar Composi��es do Colegiado');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='colegiado_versao_cadastrar'){
        if (isset($_GET['id_colegiado_versao'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_colegiado_versao']);
        }
      }
      break;

    case 'colegiado_versao_listar':

      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'colegiado_versao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  $objColegiadoVersaoDTO = new ColegiadoVersaoDTO();
  $objColegiadoVersaoDTO->retNumIdColegiadoVersao();
  $objColegiadoVersaoDTO->retDthVersao();
  $objColegiadoVersaoDTO->retStrSiglaUnidade();
  $objColegiadoVersaoDTO->retStrDescricaoUnidade();
  $objColegiadoVersaoDTO->retStrSiglaUsuario();
  $objColegiadoVersaoDTO->retStrNomeUsuario();
  $objColegiadoVersaoDTO->retStrNomeColegiado();
  //$objColegiadoVersaoDTO->retStrSinEditavel();
  $objColegiadoVersaoDTO->setNumIdColegiado($_GET['id_colegiado']);


  PaginaSEI::getInstance()->prepararOrdenacao($objColegiadoVersaoDTO, 'Versao', InfraDTO::$TIPO_ORDENACAO_DESC);
  //PaginaSEI::getInstance()->prepararPaginacao($objColegiadoVersaoDTO);

  $objColegiadoVersaoRN = new ColegiadoVersaoRN();
  $arrObjColegiadoVersaoDTO = $objColegiadoVersaoRN->listar($objColegiadoVersaoDTO);

  //PaginaSEI::getInstance()->processarPaginacao($objColegiadoVersaoDTO);
  $numRegistros = InfraArray::contar($arrObjColegiadoVersaoDTO);

  if ($numRegistros==0){
  	throw new InfraException('Colegiado n�o possui composi��es.');
  }

  if ($numRegistros > 0){
    $objDistribuicaoDTO=new DistribuicaoDTO();
    $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
    $objDistribuicaoRN=new DistribuicaoRN();
    $objSessaoJulgamentoRN=new SessaoJulgamentoRN();

    foreach ($arrObjColegiadoVersaoDTO as $objColegiadoVersaoDTOBanco) {
      $objDistribuicaoDTO->setNumIdColegiadoVersao($objColegiadoVersaoDTOBanco->getNumIdColegiadoVersao());
      $objColegiadoVersaoDTOBanco->setNumDistribuicoes($objDistribuicaoRN->contar($objDistribuicaoDTO));
      $objSessaoJulgamentoDTO->setNumIdColegiadoVersao($objColegiadoVersaoDTOBanco->getNumIdColegiadoVersao());
      $objColegiadoVersaoDTOBanco->setNumSessoes($objSessaoJulgamentoRN->contar($objSessaoJulgamentoDTO));
    }

    $strTitulo = 'Composi��es do Colegiado '.$arrObjColegiadoVersaoDTO[0]->getStrNomeColegiado();

    $bolCheck = false;

    if ($_GET['acao']=='colegiado_versao_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('colegiado_versao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('colegiado_versao_alterar');
      $bolAcaoImprimir = false;
      $bolCheck = true;
    }else{
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('colegiado_versao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('colegiado_versao_alterar');
      $bolAcaoImprimir = true;
    }


    $strResultado = '';

    $strSumarioTabela = 'Tabela de Composi��es do Colegiado.';
    $strCaptionTabela = 'Composi��es do Colegiado';

    $strResultado .= '<table width="80%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    $strResultado .= '<th class="infraTh" width="1%" style="display: none">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoVersaoDTO,'Data e Hora','Versao',$arrObjColegiadoVersaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoVersaoDTO,'Usu�rio','SiglaUsuario',$arrObjColegiadoVersaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoVersaoDTO,'Unidade','SiglaUnidade',$arrObjColegiadoVersaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh">Distribui��es</th>'."\n";
    $strResultado .= '<th class="infraTh">Sess�es</th>'."\n";
    $strResultado .= '<th class="infraTh">A��es</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      $strResultado .= $strCssTr;

      $strResultado .= '<td valign="top" style="display: none">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjColegiadoVersaoDTO[$i]->getNumIdColegiadoVersao(),$arrObjColegiadoVersaoDTO[$i]->getDthVersao()).'</td>';
      $strResultado .= '<td align="center">'.$arrObjColegiadoVersaoDTO[$i]->getDthVersao().'</td>';
      $strResultado .= '<td align="center"><a alt="'.PaginaSEI::tratarHTML($arrObjColegiadoVersaoDTO[$i]->getStrNomeUsuario()).'" title="'.PaginaSEI::tratarHTML($arrObjColegiadoVersaoDTO[$i]->getStrNomeUsuario()).'" class="ancoraSigla">'.PaginaSEI::tratarHTML($arrObjColegiadoVersaoDTO[$i]->getStrSiglaUsuario()).'</a></td>';
      $strResultado .= '<td align="center"><a alt="'.PaginaSEI::tratarHTML($arrObjColegiadoVersaoDTO[$i]->getStrDescricaoUnidade()).'" title="'.PaginaSEI::tratarHTML($arrObjColegiadoVersaoDTO[$i]->getStrDescricaoUnidade()).'" class="ancoraSigla">'.PaginaSEI::tratarHTML($arrObjColegiadoVersaoDTO[$i]->getStrSiglaUnidade()).'</a></td>';

      $quantidade=$arrObjColegiadoVersaoDTO[$i]->getNumDistribuicoes();
      if ($quantidade>0){
        $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=distribuicao_listar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_colegiado_versao='.$arrObjColegiadoVersaoDTO[$i]->getNumIdColegiadoVersao());
        $strResultado .= '<td align="center"><a href="javascript:void(0);" onclick="abrirDetalhe(\''.$strLink.'\');" class="ancoraPadraoAzul" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" alt="' . $quantidade. '" title="'. $quantidade.'">'. $quantidade . '</a></td>';

      } else {
        $strResultado .= '<td align="center"></td>';
      }


      $quantidade=$arrObjColegiadoVersaoDTO[$i]->getNumSessoes();
      if ($quantidade>0){
        $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_listar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_colegiado_versao='.$arrObjColegiadoVersaoDTO[$i]->getNumIdColegiadoVersao());
        $strResultado .= '<td align="center"><a href="javascript:void(0);" onclick="abrirDetalhe(\''.$strLink.'\');" class="ancoraPadraoAzul" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" alt="' . $quantidade. '" title="'. $quantidade.'">'. $quantidade . '</a></td>';

      } else {
        $strResultado .= '<td align="center"></td>';
      }
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjColegiadoVersaoDTO[$i]->getNumIdColegiadoVersao());

      if ($bolAcaoConsultar){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=colegiado_consultar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_colegiado='.$_GET['id_colegiado'].'&id_colegiado_versao='.$arrObjColegiadoVersaoDTO[$i]->getNumIdColegiadoVersao()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeConsultar().'" title="Consultar composi��o do Colegiado" alt="Consultar composi��o do Colegiado" class="infraImg" /></a> ';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'colegiado_versao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_colegiado'])).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
function inicializar(){
  <? if ($_GET['acao']=='colegiado_versao_selecionar'){ ?>
  infraReceberSelecao();
  document.getElementById('btnFecharSelecao').focus();
   <? }else{ ?>
  document.getElementById('btnFechar').focus();
  <? }?>
  infraEfeitoTabelas();
}
function abrirDetalhe(link) {
  infraAbrirJanelaModal(link, 850, 550, false);
}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmColegiadoVersaoLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>