<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 25/01/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__.'/../../../../SEI.php';

class ItemSessaoDocumentoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'item_sessao_documento';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdItemSessaoDocumento', 'id_item_sessao_documento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdItemSessaoJulgamento', 'id_item_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUsuario', 'id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUnidade', 'id_unidade');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL, 'IdDocumento', 'id_documento');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdSerie', 'id_serie', 'documento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdSessaoJulgamentoItem','id_sessao_julgamento','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdDistribuicaoItem','id_distribuicao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdUnidadeSessaoItem','id_unidade_sessao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaSituacaoItem','sta_situacao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DBL, 'IdProcedimentoDistribuicao','id_procedimento','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaSituacaoSessaoJulgamento','sta_situacao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdTipoSessaoJulgamento', 'id_tipo_sessao', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SinVirtualTipoSessao','sin_virtual','tipo_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdColegiadoSessaoJulgamento','id_colegiado','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdTipoSessaoJulgamento', 'id_tipo_sessao', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DTH, 'SessaoSessaoJulgamento','dth_sessao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeSerie', 'nome', 'serie');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'ProtocoloDocumentoFormatado', 'p.protocolo_formatado', 'protocolo p');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'ProtocoloProcedimentoFormatado', 'p2.protocolo_formatado', 'protocolo p2');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeColegiado', 'nome', 'colegiado');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeUsuario', 'nome', 'usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SinVirtualTipoSessao','sin_virtual','tipo_sessao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdProvimento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Complemento');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjVotoParteDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjBloqueioItemSessUnidadeDTO');

    $this->configurarPK('IdItemSessaoDocumento',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdItemSessaoJulgamento','item_sessao_julgamento','id_item_sessao_julgamento');
    $this->configurarFK('IdTipoSessaoJulgamento', 'tipo_sessao', 'id_tipo_sessao');
    $this->configurarFK('IdSessaoJulgamentoItem','sessao_julgamento','id_sessao_julgamento');
    $this->configurarFK('IdColegiadoSessaoJulgamento','colegiado','id_colegiado');
    $this->configurarFK('IdDistribuicaoItem','distribuicao','id_distribuicao');
    $this->configurarFK('IdDocumento','protocolo p','p.id_protocolo');
    $this->configurarFK('IdProcedimentoDistribuicao','protocolo p2','p2.id_protocolo');
    $this->configurarFK('IdDocumento','documento','id_documento');
    $this->configurarFK('IdSerie', 'serie', 'id_serie');
    $this->configurarFK('IdUsuario', 'usuario', 'id_usuario');


  }
}
?>