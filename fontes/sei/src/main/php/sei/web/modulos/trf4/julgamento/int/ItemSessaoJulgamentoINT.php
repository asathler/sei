<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 11/11/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ItemSessaoJulgamentoINT extends InfraINT {

  public static function montarSelectItensOrdenacao($numIdSessaoJulgamento, $numIdSessaoBloco, $numIdUsuario): string
  {

    $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
    $objItemSessaoJulgamentoDTO->setNumIdSessaoBloco($numIdSessaoBloco);
    $objItemSessaoJulgamentoDTO->setNumIdUsuarioSessao($numIdUsuario);
    $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
    $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
    $objItemSessaoJulgamentoDTO->retNumOrdem();
    $objItemSessaoJulgamentoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

    $arr=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

    return parent::montarSelectArrInfraDTO(null, null, null, $arr, 'IdItemSessaoJulgamento', 'ProtocoloFormatadoProtocolo');
  }
}
?>