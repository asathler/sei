<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/06/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.34.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ProvimentoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'provimento';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdProvimento','id_provimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Conteudo','conteudo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinAtivo','sin_ativo');

    $this->configurarPK('IdProvimento',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarExclusaoLogica('SinAtivo', 'N');
  }
}
?>