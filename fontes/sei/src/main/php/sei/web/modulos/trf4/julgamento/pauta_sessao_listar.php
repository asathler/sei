<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 23/04/2012 - criado por bcu
 */

try {
  require_once __DIR__ . '/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////
  ///


  SessaoSEIExterna::getInstance()->validarLink();

  PaginaSEIExterna::getInstance()->setTipoPagina(PaginaSEIExterna::$TIPO_PAGINA_SEM_MENU);

  PaginaSEIExterna::getInstance()->salvarCamposPost(array('selColegiado'));

  $objUsuarioDTO = new UsuarioDTO();
  $arrComandos = array();

  switch ($_GET['acao']) {

    case 'pauta_sessao_listar':
      $strTitulo = 'Sess�es de Julgamento';
      $arrComandos[] = '<button type="submit" accesskey="P" id="sbmPesquisar" name="sbmPesquisar" value="Pesquisar" class="infraButton"><span class="infraTeclaAtalho">P</span>esquisar</button>';
      break;


    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }

  $numIdColegiado = PaginaSEIExterna::getInstance()->recuperarCampo('selColegiado');

  if(isset($_GET['sigla_colegiado'])){
    $objColegiadoDTO=new ColegiadoDTO();
    $objColegiadoDTO->setStrSigla($_GET['sigla_colegiado']);
    $objColegiadoDTO->retNumIdColegiado();
    $objColegiadoRN=new ColegiadoRN();
    $objColegiadoDTO=$objColegiadoRN->consultar($objColegiadoDTO);
    if($objColegiadoDTO){
      $numIdColegiado=$objColegiadoDTO->getNumIdColegiado();
    }
  }

  $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
  if(is_numeric($numIdColegiado)){
    $objSessaoJulgamentoDTO->setNumIdColegiado($numIdColegiado);
    $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
    $objSessaoJulgamentoDTO->retDthSessao();
    $objSessaoJulgamentoDTO->retStrStaSituacao();
    $objSessaoJulgamentoDTO->retStrDescricaoTipoSessao();
    $objSessaoJulgamentoDTO->retDthInicio();
    $objSessaoJulgamentoDTO->retDthFim();
    $objSessaoJulgamentoDTO->retStrSiglaColegiado();
    $objSessaoJulgamentoDTO->retNumIdUsuarioPresidenteColegiado();
    $objSessaoJulgamentoDTO->retNumIdUsuarioPresidente();
    $objSessaoJulgamentoDTO->retStrNomeColegiado();
    $objSessaoJulgamentoDTO->retStrProtocoloFormatadoDocumentoPauta();
    $objSessaoJulgamentoDTO->retDblIdDocumentoPauta();
    $objSessaoJulgamentoDTO->setOrdDthSessao(InfraDTO::$TIPO_ORDENACAO_DESC);
    $arrStaSessao = array(
        SessaoJulgamentoRN::$ES_SUSPENSA,
        SessaoJulgamentoRN::$ES_CANCELADA,
        SessaoJulgamentoRN::$ES_ABERTA,
        SessaoJulgamentoRN::$ES_FINALIZADA,
        SessaoJulgamentoRN::$ES_PAUTA_FECHADA,
        SessaoJulgamentoRN::$ES_ENCERRADA);
    $objSessaoJulgamentoDTO->setStrStaSituacao($arrStaSessao, InfraDTO::$OPER_IN);
    /** @var SessaoJulgamentoDTO[] $arrObjSessaoJulgamentoDTO */
    $arrObjSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);

  } else {
    $arrObjSessaoJulgamentoDTO=[];
  }

  $arrObjSituacaoSessaoDTO = $objSessaoJulgamentoRN->listarValoresSituacaoSessao();
  $arrObjSituacaoSessao = InfraArray::indexarArrInfraDTO($arrObjSituacaoSessaoDTO, 'StaSituacao');


  $strResultado = '';

  $arrIdDocumentoPautaConsulta=[];
  $arrIdDocumentoPautaLiberados=[];
  foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {
    $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
    $numIdDocumentoPauta=$objSessaoJulgamentoDTO->getDblIdDocumentoPauta();
    if($numIdDocumentoPauta!=null && ($staSessao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA || $staSessao==SessaoJulgamentoRN::$ES_CANCELADA)){
      $arrIdDocumentoPautaConsulta[]=$numIdDocumentoPauta;
    }
  }

  if(count($arrIdDocumentoPautaConsulta)){
    $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
    $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_DOCUMENTOS);
    $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_TODOS);
    $objPesquisaProtocoloDTO->setDblIdProtocolo($arrIdDocumentoPautaConsulta,InfraDTO::$OPER_IN);

    $objProtocoloRN = new ProtocoloRN();
    /** @var ProtocoloDTO[] $arrObjProtocoloDTO */
    $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);

    $objInfraParametro=new InfraParametro(BancoSEI::getInstance());
    $criterio=$objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_EVENTO_PAUTA_FECHADA,0);
    foreach ($arrObjProtocoloDTO as $objProtocoloDTO) {
      if(($criterio==0 && $objProtocoloDTO->getStrSinAssinado()=='S' )||($criterio==1 && $objProtocoloDTO->getStrSinPublicado()=='S')){
        $arrIdDocumentoPautaLiberados[$objProtocoloDTO->getDblIdProtocolo()]=1;
      }
    }

  }

  //remove cancelados, pauta_fechada e pauta_aberta que n�o tenham pauta assinada ou publicada
  foreach ($arrObjSessaoJulgamentoDTO as $key=>$objSessaoJulgamentoDTO) {
    if(in_array($objSessaoJulgamentoDTO->getStrStaSituacao(), [SessaoJulgamentoRN::$ES_CANCELADA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_PAUTA_ABERTA]) &&
        ($objSessaoJulgamentoDTO->getDblIdDocumentoPauta()==null || !isset($arrIdDocumentoPautaLiberados[$objSessaoJulgamentoDTO->getDblIdDocumentoPauta()]))) {
      unset($arrObjSessaoJulgamentoDTO[$key]);
    }
  }


  $numRegistros = InfraArray::contar($arrObjSessaoJulgamentoDTO);

  if ($numRegistros>0) {
    $strSumarioTabela = 'Tabela de Sess�es de Julgamento.';
    $strCaptionTabela = 'Sess�es de Julgamento';


    $strResultado .= '<tr>';
    $strResultado .= '<th class="infraTh" style="width: 15%">Data da Sess�o</th>' . "\n";
    $strResultado .= '<th class="infraTh" >Colegiado</th>' . "\n";
    $strResultado .= '<th class="infraTh" >Tipo de Sess�o</th>' . "\n";
    $strResultado .= '<th class="infraTh" >Composi��o</th>' . "\n";
    $strResultado .= '<th class="infraTh" style="width: 15%">Pauta</th>' . "\n";
    $strResultado .= '</tr>' . "\n";
    $strCssTr = '';
    $numIdUsuarioPresidenteColegiado = $objSessaoJulgamentoDTO->getNumIdUsuarioPresidenteColegiado();
    foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {

      $numIdUsuarioPresidente=$objSessaoJulgamentoDTO->getNumIdUsuarioPresidente();
      if($numIdUsuarioPresidente==null){
        $numIdUsuarioPresidente=$numIdUsuarioPresidenteColegiado;
      }


      $arrObjColegiadoComposicaoDTO = CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $arrObjPresencaSessaoDTO = CacheSessaoJulgamentoRN::getArrObjPresencaSessaoDTO($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $strNomePresidente = '';
      $arrMembros = [];
      if(in_array($objSessaoJulgamentoDTO->getStrStaSituacao(),[SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_CANCELADA]) ){
        foreach ($arrObjColegiadoComposicaoDTO as $numIdUsuario => $objColegiadoComposicaoDTO) {
          if ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR) {
            continue;
          }
          $strNomeMembro = '';
//        $strNomeMembro=$objColegiadoComposicaoDTO->getStrAbreviaturaTitulo();
//          if ($strNomeMembro=='') {
//            $strNomeMembro = $objColegiadoComposicaoDTO->getStrExpressaoCargo();
//          }
//          if ($strNomeMembro!='') {
//            $strNomeMembro .= ' ';
//          }
          $strNomeMembro .= $objColegiadoComposicaoDTO->getStrNomeUsuario();
          if ($numIdUsuario==$numIdUsuarioPresidente) {
            $strNomePresidente = $strNomeMembro;
          } else {
            $arrMembros[] = $strNomeMembro;
          }
        }
      } else{
        foreach ($arrObjPresencaSessaoDTO as $numIdUsuario=>$objPresencaSessaoDTO) {
          $strNomeMembro = '';
//        $strNomeMembro=$objColegiadoComposicaoDTO->getStrAbreviaturaTitulo();
//          if ($strNomeMembro=='') {
//            $strNomeMembro = $arrObjColegiadoComposicaoDTO[$numIdUsuario]->getStrExpressaoCargo();
//          }
//          if ($strNomeMembro!='') {
//            $strNomeMembro .= ' ';
//          }
          $strNomeMembro .= $arrObjColegiadoComposicaoDTO[$numIdUsuario]->getStrNomeUsuario();
          if ($numIdUsuario==$numIdUsuarioPresidente) {
            $strNomePresidente = $strNomeMembro;
          } else {
            $arrMembros[] = $strNomeMembro;
          }
        }

      }

      sort($arrMembros);

      $strCssTr = ($strCssTr=='<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
      $strResultado .= $strCssTr;

      $strResultado .= '<td style="text-align: center">' . substr($objSessaoJulgamentoDTO->getDthSessao(), 0, - 3);
      if($objSessaoJulgamentoDTO->getStrStaSituacao()==SessaoJulgamentoRN::$ES_CANCELADA){
        $strResultado.='<br>(Cancelada)';
      }
      $strResultado .= '</td>';
      $strResultado .= '<td style="text-align: center">' . PaginaSEIExterna::tratarHTML($objSessaoJulgamentoDTO->getStrNomeColegiado()) . '</td>';
      $strResultado .= '<td style="text-align: center">' . PaginaSEIExterna::tratarHTML($objSessaoJulgamentoDTO->getStrDescricaoTipoSessao()) . '</td>';

      $strResultado .= '<td style="text-align: center">';
      $strResultado .= '<b>' . $strNomePresidente . '</b>';
      foreach ($arrMembros as $strNomeMembro) {
        $strResultado .= '<br>' . $strNomeMembro;
      }
      $strResultado .= '</td>';

      //coluna da pauta
      $strResultado .= '<td style="text-align: center">';
//      $strResultado .= PaginaSEIExterna::tratarHTML($arrObjSituacaoSessao[$objSessaoJulgamentoDTO->getStrStaSituacao()]->getStrDescricao());
//      $strResultado .= '<br>';

      $tab = PaginaSEI::getInstance()->getProxTabTabela();
      $strLink = SessaoSEIExterna::getInstance()->assinarLink('controlador_externo.php?acao=pauta_sessao_visualizar&id_sessao_julgamento=' . $objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $strResultado .='<a onclick="infraLimparFormatarTrAcessada(this.parentNode.parentNode);" target="_blank" class="protocoloNormal"  tabindex='.$tab.' href="'.$strLink.'" title="Pauta">Pauta</a>';
//      $strResultado .= "<a href='$strLink' target='_blank' tabindex='$tab'>Pauta</a>";
//      $strResultado .= '['.$objSessaoJulgamentoDTO->getStrProtocoloFormatadoDocumentoPauta().']';
      $strResultado .= '</td>';
      $strResultado .= '</tr>' . "\n";
    }
    $strResultado .= '</table>';
    $strTopo = '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
    $strTopo .= '<caption class="infraCaption">' . PaginaSEIExterna::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistros) . '</caption>';

    $strResultado = $strTopo . $strResultado;
  }

  $strItensSelColegiado = ColegiadoINT::montarSelectNome('null', '&nbsp;', $numIdColegiado);

}catch(Exception $e){
  PaginaSEIExterna::getInstance()->processarExcecao($e);
}

PaginaSEIExterna::getInstance()->montarDocType();
PaginaSEIExterna::getInstance()->abrirHtml();
PaginaSEIExterna::getInstance()->abrirHead();
PaginaSEIExterna::getInstance()->montarMeta();
PaginaSEIExterna::getInstance()->montarTitle(PaginaSEIExterna::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEIExterna::getInstance()->montarStyle();
PaginaSEIExterna::getInstance()->abrirStyle();
if(0){?><style><?}
?>
  #lblColegiado {position:absolute;left:0%;top:10%;width:25%;}
  #selColegiado {position:absolute;left:10%;top:0%;width:60%;}

<?
if(0){?></style><?}
PaginaSEIExterna::getInstance()->fecharStyle();
PaginaSEIExterna::getInstance()->montarJavaScript();
PaginaSEIExterna::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

  function inicializar(){
    infraEfeitoTabelas();
  }

  function onSubmitForm() {
    return validarForm();
  }

  function validarForm() {
    return true;
  }

<?
if(0){?></script><?}
PaginaSEIExterna::getInstance()->fecharJavaScript();
PaginaSEIExterna::getInstance()->fecharHead();
PaginaSEIExterna::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
  <form id="frmSessaoJulgamentoLista" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEIExterna::getInstance()->assinarLink('controlador_externo.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
    <?
    PaginaSEIExterna::getInstance()->montarBarraComandosSuperior($arrComandos);
    PaginaSEI::getInstance()->abrirAreaDados('4em');
    ?>
    <label id="lblColegiado" for="selColegiado" accesskey="o" class="infraLabelOpcional">C<span class="infraTeclaAtalho">o</span>legiado:</label>
    <select id="selColegiado" name="selColegiado" onchange="this.form.submit();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
      <?=$strItensSelColegiado?>
    </select>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    if(is_numeric($numIdColegiado)){
      PaginaSEIExterna::getInstance()->montarAreaTabela($strResultado,$numRegistros);
    }
    //PaginaSEIExterna::getInstance()->montarAreaDebug();
//    PaginaSEIExterna::getInstance()->montarBarraComandosInferior($arrComandos);
    ?>
  </form>
<?
PaginaSEIExterna::getInstance()->fecharBody();
PaginaSEIExterna::getInstance()->fecharHtml();
?>