<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class OpcaoEleicaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'opcao_eleicao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdOpcaoEleicao', 'id_opcao_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdEleicao', 'id_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Identificacao', 'identificacao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Ordem', 'ordem');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'IdentificacaoEleicao', 'identificacao', 'eleicao');

    $this->configurarPK('IdOpcaoEleicao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdEleicao', 'eleicao', 'id_eleicao');
  }
}
