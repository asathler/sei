<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/06/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.34.0
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'julgamento_configurar':
      $strTitulo = 'Configura��o do M�dulo SEI Julgar';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmSalvar" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objJulgamentoConfiguracaoRN=new JulgamentoConfiguracaoRN();

      $arrParametrosConfiguracao=$objJulgamentoConfiguracaoRN->getArrParametrosConfiguraveis();
      $arrObjInfraParametroDTO=array();
      $objInfraParametro = new InfraParametro(BancoSEI::getInstance());
      if (isset($_POST['sbmSalvar'])) {
        foreach ($arrParametrosConfiguracao as $strNome=>$arrConfig) {
          $prefixo=$arrConfig[ConfiguracaoRN::$POS_PREFIXO];
          $objInfraParametroDTO=new InfraParametroDTO();
          $objInfraParametroDTO->setStrNome($strNome);
          $valor=$_POST[$prefixo.$strNome];
          if($arrConfig[ConfiguracaoRN::$POS_MULTIPLO]){
            $valor=PaginaSEI::getInstance()->getArrValuesSelect($_POST['hdn'.$strNome]);
            $valor=implode(',',$valor);
          }
          $objInfraParametroDTO->setStrValor($valor);
          $arrObjInfraParametroDTO[$strNome]=$objInfraParametroDTO;
        }
      } else {
        $arrParametrosBanco=$objInfraParametro->listarValores(array_keys($arrParametrosConfiguracao),false);
        foreach ($arrParametrosBanco as $strNome=>$valor) {
          $objInfraParametroDTO=new InfraParametroDTO();
          $objInfraParametroDTO->setStrNome($strNome);
          $objInfraParametroDTO->setStrValor($valor);
          $arrObjInfraParametroDTO[$strNome]=$objInfraParametroDTO;
        }
      }


      //validar dados


      if (isset($_POST['sbmSalvar'])) {
        try{

          $objJulgamentoConfiguracaoRN=new JulgamentoConfiguracaoRN();

          $objJulgamentoConfiguracaoRN->gravar($arrObjInfraParametroDTO);

          PaginaSEI::getInstance()->adicionarMensagem('Parametros gravados com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $objSerieDTO=new SerieDTO();
  $objSerieRN=new SerieRN();
  $objSerieDTO->setStrSinAtivo('S');
  $objSerieDTO->retStrStaAplicabilidade();
  $objSerieDTO->retStrNome();
  $objSerieDTO->retNumIdSerie();
  $arrObjSerieDTO=$objSerieRN->listarRN0646($objSerieDTO);
  $arrObjSerieDTO=InfraArray::indexarArrInfraDTO($arrObjSerieDTO,'IdSerie');


  $strInicializar='';
  $id=0;
  $arrAreaDados=array();
  foreach ($arrParametrosConfiguracao as $strNome=>$arrConfig) {
    $staTipo=$arrConfig[ConfiguracaoRN::$POS_TIPO];
    $prefixo=$arrConfig[ConfiguracaoRN::$POS_PREFIXO];
    $strRotulo=$strNome;
    if (isset($arrConfig[ConfiguracaoRN::$POS_ROTULO])){
      $strRotulo=$arrConfig[ConfiguracaoRN::$POS_ROTULO];
    }
    $valor=null;
    if (isset($arrObjInfraParametroDTO[$strNome])){
      $valor=$arrObjInfraParametroDTO[$strNome]->getStrValor();
    }
    $tamAreaDados='5em';
    $id++;
    $strHtml='';
    $strHtml.='<label id="lblParam'.$id.'" for="'.$prefixo.$strNome.'" class="infraLabelObrigatorio">'.$strRotulo.':</label>'."\n";
    switch($staTipo){
      case ConfiguracaoRN::$TP_NUMERICO:
        $strHtml.='<input type="text" id="txtParam'.$id.'" name="txt'.$strNome.'" class="infraText" value="'.$valor.'" onkeypress="return infraMascaraNumero(this,event,9);" maxlength="9" tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'" />';
        break;
      case ConfiguracaoRN::$TP_TEXTO:
      case ConfiguracaoRN::$TP_EMAIL:
        $strHtml.='<input type="text" id="txtParam'.$id.'" name="txt'.$strNome.'" class="infraText" value="'.$valor.'" onkeypress="return infraMascaraTexto(this,event,9);" maxlength="9" tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'" />';
        break;
      case ConfiguracaoRN::$TP_COMBO:
        $strHtml.='<select id="selParam'.$id.'" name="sel'.$strNome.'" class="infraSelect" tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'">';
        $regra=$arrConfig[ConfiguracaoRN::$POS_REGRA];
        $arrValores=call_user_func('JulgamentoConfiguracaoRN::montarArray'.$regra);
        $strHtml.=InfraINT::montarSelectArray('null', '&nbsp;', $valor, $arrValores);
        $strHtml.='</select>'."\n";
        break;
      case ConfiguracaoRN::$TP_ID:
        $strHtml.='<select id="selParam'.$id.'" name="sel'.$strNome.'" class="infraSelect" ';

        if(!$arrConfig[ConfiguracaoRN::$POS_MULTIPLO]) {
          $strHtml.='tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'">';
          $strHtml .= SerieINT::montarSelectNomeRI0802('null', '&nbsp;', $valor);
          $strHtml.='</select>'."\n";
        } else {
          $strHtml.='size="4" multiple="multiple"';
          $strHtml.='tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'">';
          $arrItens = array();
          if (!InfraString::isBolVazia($valor)){
            $tmp = explode(',', $valor);
            foreach ($tmp as $idSerie) {
              if ($arrObjSerieDTO[$idSerie]) {
                $arrItens[] = $arrObjSerieDTO[$idSerie];
              }
            }
          }

          $strHtml .= InfraINT::montarSelectArrInfraDTO(null, null, null, $arrItens, 'IdSerie', 'Nome');
          $strHtml.='</select>'."\n";
          $tamAreaDados = '11em';
          $strHtml .= '<img id="imgLupa'.$id.'" onclick="objLupa'.$id.'.selecionar(700,500);" src="'.PaginaSEI::getInstance()->getIconePesquisar().'" style="top:35%;" alt="Selecionar Tipo de Documento" title="Selecionar Tipo de Documento" class="infraImg" tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'" />';
          $strHtml .= '<img id="imgExcluir'.$id.'" onclick="objLupa'.$id.'.remover();" src="'.PaginaSEI::getInstance()->getIconeRemover().'" style="top:55%;" alt="Remover Tipos de Documento Selecionados" title="Remover Tipos de Documento Selecionados" class="infraImg" tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'" />';
          $strHtml .= '<input type="text" id="txtParam' . $id . '" name="txt' . $strNome . '" class="infraText" style="top:15%" tabindex="' . PaginaSEI::getInstance()->getProxTabDados() . '" />';
          $strHtml .= '<input type="hidden" id="hdnParam' . $id . '" name="hdn' . $strNome . '" class="infraText" value="' . $_POST['hdn' . $strNome] . '" />';
          $strHtml .= '<input type="hidden" id="hdnAjax' . $id . '" name="hdnAjax' . $strNome . '" class="infraText" value="' . $_POST['hdnAjax' . $strNome] . '" />';
          $strInicializar.='objAutoCompletarSerie'.$id.'=new infraAjaxAutoCompletar(\'hdnAjax'.$id.'\',\'txtParam'.$id.'\',\''.SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=serie_auto_completar').'\');'."\n";
          $strInicializar.='objAutoCompletarSerie'.$id.'.limparCampo = true;'."\n";
          $strInicializar.='objAutoCompletarSerie'.$id.'.prepararExecucao = prep;'."\n";
          $strInicializar.='objAutoCompletarSerie'.$id.'.processarResultado = proc;'."\n";
          $strInicializar.='objAutoCompletarSerie'.$id.'.objSel = $("#selParam'.$id.'");'."\n";

          $strInicializar.='objLupa'.$id.' = new infraLupaSelect(\'selParam'.$id.'\',\'hdnParam'.$id.'\',\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=serie_selecionar&tipo_selecao=2&id_object=objLupa'.$id).'\');'."\n";
          $strInicializar.='objAutoCompletarSerie'.$id.'.objLupa = objLupa'.$id.''."\n";
        }


        break;
      case ConfiguracaoRN::$TP_HTML:
        $tamAreaDados='18em';
        $strHtml.='<textarea id="txaParam'.$id.'" name="txa'.$strNome.'" rows="10" class="infraTextarea" tabindex="'.PaginaSEI::getInstance()->getProxTabDados().'">'.$valor.'</textarea>';
        break;

    }
      $arrAreaDados[] = array($strHtml, $tamAreaDados);
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
  #frmParametrosJulgamento label {position:absolute;left:0;top:0;}
  #frmParametrosJulgamento input {position:absolute;left:0;top:40%;width:50%}
  #frmParametrosJulgamento select {position:absolute;left:0;top:40%;width:50%}
  #frmParametrosJulgamento textarea {position:absolute;left:0;top:2em;width:70%}
  #frmParametrosJulgamento img {position:absolute;left:50.5%;}

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
  var arrObjAutoCompletarSerie=[];
  var objLupaSerieRelatorio,objLupaSerieVoto;
  var prep=function(){
    return 'palavras_pesquisa='+this.elem.value;
  };
  var proc = function(id,descricao){
    if (id!=''){
      var opt=this.objSel.find('option[value="'+id+'"]');
      if (opt.length>0){
        self.setTimeout('alert(\'Tipo de Documento j� consta na lista.\')',100);
      } else {
        this.objSel.find('option').prop('selected',false);
        this.objSel.append($('<option>', {
          value: id,
          text: descricao,
          selected: 'selected'
        }));
        this.objLupa.atualizar();
      }
      this.elem.value='';
      this.elem.focus();
    }
  };

  function inicializar(){

<?=$strInicializar;?>

}

function validarCadastro() {
  
  return true;
}

function onSubmitForm() {
  return validarCadastro();
}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmParametrosJulgamento" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();

foreach ($arrAreaDados as $areaDados) {
  PaginaSEI::getInstance()->abrirAreaDados($areaDados[1]);
  echo $areaDados[0];
  PaginaSEI::getInstance()->fecharAreaDados();
}

  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>