<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 19/05/2023 - criado por neijobson
 *
 * Vers�o do Gerador de C�digo: 1.43.2
 */

require_once dirname(__FILE__) . '/../../../SEI.php';

class MdIaAvaliacaoRN extends InfraRN
{

    public function __construct()
    {
        parent::__construct();
    }

    protected function inicializarObjInfraIBanco()
    {
        return BancoSEI::getInstance();
    }

    private function validarDblIdProcedimento(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO, InfraException $objInfraException)
    {
        if (InfraString::isBolVazia($objMdIaAvaliacaoDTO->getDblIdProcedimento())) {
            $objInfraException->adicionarValidacao('Procedimento n�o informado.');
        }
    }

    private function validarNumIdUnidade(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO, InfraException $objInfraException)
    {
        if (InfraString::isBolVazia($objMdIaAvaliacaoDTO->getNumIdUnidade())) {
            $objInfraException->adicionarValidacao('Unidade n�o informada.');
        }
    }

    private function validarNumIdUsuario(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO, InfraException $objInfraException)
    {
        if (InfraString::isBolVazia($objMdIaAvaliacaoDTO->getNumIdUsuario())) {
            $objInfraException->adicionarValidacao('Usu�rio n�o informado.');
        }
    }

    private function validarDthAtual(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO, InfraException $objInfraException)
    {
        if (InfraString::isBolVazia($objMdIaAvaliacaoDTO->getDthAtual())) {
            $objMdIaAvaliacaoDTO->setDthAtual(null);
        } else {
            if (!InfraData::validarDataHora($objMdIaAvaliacaoDTO->getDthAtual())) {
                $objInfraException->adicionarValidacao('Data Atual inv�lida.');
            }
        }
    }

    private function validarStrSugestao(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO, InfraException $objInfraException)
    {
        if (InfraString::isBolVazia($objMdIaAvaliacaoDTO->getStrSugestao())) {
            $objInfraException->adicionarValidacao('Sugest�o n�o informada.');
        } else {
            $objMdIaAvaliacaoDTO->setStrSugestao(trim($objMdIaAvaliacaoDTO->getStrSugestao()));

            if (strlen($objMdIaAvaliacaoDTO->getStrSugestao()) > 500) {
                $objInfraException->adicionarValidacao('Sugest�o possui tamanho superior a 500 caracteres.');
            }
        }
    }

    protected function cadastrarControlado(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO)
    {
        try {

            SessaoSEI::getInstance()->validarAuditarPermissao('md_ia_avaliacao_cadastrar', __METHOD__, $objMdIaAvaliacaoDTO);

            //Regras de Negocio
            $objInfraException = new InfraException();

            $this->validarDblIdProcedimento($objMdIaAvaliacaoDTO, $objInfraException);
            $this->validarNumIdUnidade($objMdIaAvaliacaoDTO, $objInfraException);
            $this->validarNumIdUsuario($objMdIaAvaliacaoDTO, $objInfraException);
            $this->validarDthAtual($objMdIaAvaliacaoDTO, $objInfraException);
            $this->validarStrSugestao($objMdIaAvaliacaoDTO, $objInfraException);

            $objInfraException->lancarValidacoes();

            $objMdIaAvaliacaoBD = new MdIaAvaliacaoBD($this->getObjInfraIBanco());
            $ret = $objMdIaAvaliacaoBD->cadastrar($objMdIaAvaliacaoDTO);

            return $ret;

        } catch (Exception $e) {
            throw new InfraException('Erro cadastrando Avalia��o.', $e);
        }
    }

    protected function alterarControlado(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO)
    {
        try {

            SessaoSEI::getInstance()->validarAuditarPermissao('md_ia_avaliacao_alterar', __METHOD__, $objMdIaAvaliacaoDTO);

            //Regras de Negocio
            $objInfraException = new InfraException();

            if ($objMdIaAvaliacaoDTO->isSetDblIdProcedimento()) {
                $this->validarDblIdProcedimento($objMdIaAvaliacaoDTO, $objInfraException);
            }
            if ($objMdIaAvaliacaoDTO->isSetNumIdUnidade()) {
                $this->validarNumIdUnidade($objMdIaAvaliacaoDTO, $objInfraException);
            }
            if ($objMdIaAvaliacaoDTO->isSetNumIdUsuario()) {
                $this->validarNumIdUsuario($objMdIaAvaliacaoDTO, $objInfraException);
            }
            if ($objMdIaAvaliacaoDTO->isSetDthAtual()) {
                $this->validarDthAtual($objMdIaAvaliacaoDTO, $objInfraException);
            }
            if ($objMdIaAvaliacaoDTO->isSetStrSugestao()) {
                $this->validarStrSugestao($objMdIaAvaliacaoDTO, $objInfraException);
            }

            $objInfraException->lancarValidacoes();

            $objMdIaAvaliacaoBD = new MdIaAvaliacaoBD($this->getObjInfraIBanco());
            $objMdIaAvaliacaoBD->alterar($objMdIaAvaliacaoDTO);

        } catch (Exception $e) {
            throw new InfraException('Erro alterando Avalia��o.', $e);
        }
    }

    protected function excluirControlado($arrObjMdIaAvaliacaoDTO)
    {
        try {

            SessaoSEI::getInstance()->validarAuditarPermissao('md_ia_avaliacao_excluir', __METHOD__, $arrObjMdIaAvaliacaoDTO);

            //Regras de Negocio
            //$objInfraException = new InfraException();

            //$objInfraException->lancarValidacoes();

            $objMdIaAvaliacaoBD = new MdIaAvaliacaoBD($this->getObjInfraIBanco());
            for ($i = 0; $i < count($arrObjMdIaAvaliacaoDTO); $i++) {
                $objMdIaAvaliacaoBD->excluir($arrObjMdIaAvaliacaoDTO[$i]);
            }

        } catch (Exception $e) {
            throw new InfraException('Erro excluindo Avalia��o.', $e);
        }
    }

    protected function consultarConectado(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO)
    {
        try {

            SessaoSEI::getInstance()->validarAuditarPermissao('md_ia_avaliacao_consultar', __METHOD__, $objMdIaAvaliacaoDTO);

            //Regras de Negocio
            //$objInfraException = new InfraException();

            //$objInfraException->lancarValidacoes();

            $objMdIaAvaliacaoBD = new MdIaAvaliacaoBD($this->getObjInfraIBanco());

            /** @var MdIaAvaliacaoDTO $ret */
            $ret = $objMdIaAvaliacaoBD->consultar($objMdIaAvaliacaoDTO);

            return $ret;
        } catch (Exception $e) {
            throw new InfraException('Erro consultando Avalia��o.', $e);
        }
    }

    protected function listarConectado(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO)
    {
        try {

            SessaoSEI::getInstance()->validarAuditarPermissao('md_ia_avaliacao_listar', __METHOD__, $objMdIaAvaliacaoDTO);

            //Regras de Negocio
            //$objInfraException = new InfraException();

            //$objInfraException->lancarValidacoes();

            $objMdIaAvaliacaoBD = new MdIaAvaliacaoBD($this->getObjInfraIBanco());

            /** @var MdIaAvaliacaoDTO[] $ret */
            $ret = $objMdIaAvaliacaoBD->listar($objMdIaAvaliacaoDTO);

            return $ret;

        } catch (Exception $e) {
            throw new InfraException('Erro listando Avalia��es.', $e);
        }
    }

    protected function contarConectado(MdIaAvaliacaoDTO $objMdIaAvaliacaoDTO)
    {
        try {

            SessaoSEI::getInstance()->validarAuditarPermissao('md_ia_avaliacao_listar', __METHOD__, $objMdIaAvaliacaoDTO);

            //Regras de Negocio
            //$objInfraException = new InfraException();

            //$objInfraException->lancarValidacoes();

            $objMdIaAvaliacaoBD = new MdIaAvaliacaoBD($this->getObjInfraIBanco());
            $ret = $objMdIaAvaliacaoBD->contar($objMdIaAvaliacaoDTO);

            return $ret;
        } catch (Exception $e) {
            throw new InfraException('Erro contando Avalia��es.', $e);
        }
    }

    protected function conexaoApiRecomendacaoSimilaridadeConectado($numeroProcessoProcedimentoFormatado)
    {
        try {
            $urlApi = $this->retornarUrlApi();

            $curl = curl_init();

            // Configura
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://'.$urlApi.':8082/process-recommenders/weighted-mlt-recommender/recommendations/'.$numeroProcessoProcedimentoFormatado[0].'?rows=5',
                CURLOPT_TIMEOUT_MS => '2000'
            ]);

            // Envio e armazenamento da resposta
            $response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            // Fecha e limpa recursos
            curl_close($curl);
            if($httpcode == "200") {
                return json_decode($response);
            } elseif($httpcode == "404") {
                return $httpcode;
            } else {
                $log = "00001 - INDISPONIBILIDADE DE RECURSO NO SEI IA \n";
                $log .= "00002 - Processo: " . $numeroProcessoProcedimentoFormatado[1] . " \n";
                $log .= "00003 - Usuario: " . SessaoSEI::getInstance()->getStrNomeUsuario() . " - Unidade: " . SessaoSEI::getInstance()->getStrSiglaUnidadeAtual() . " \n";
                $log .= "00004 - Hostname do Amdiente do SEI IA: ".$urlApi." \n";
                $log .= "00005 - Endpoint do Recurso: /process-recommenders/weighted-mlt-recommender/recommendations/{id_protocolo} \n";
                $log .= "00006 - Tipo de Indisponibilidade: ".$httpcode." \n";
                $log .= "00007 - FIM \n";
                LogSEI::getInstance()->gravar($log, InfraLog::$INFORMACAO);
                return null;
            }

            return json_decode($response);
        } catch (Exception $e) {
            throw new InfraException('Erro contando Avalia��es.', $e);
        }
    }

    protected function submeteAvaliacaoConectado($avaliacao)
    {
        try {
            $urlApi = $this->retornarUrlApi();
            
            // Inicia
            $curl = curl_init();

            // Configura
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => 'http://'.$urlApi.':8086/insert_feedback/',
                CURLOPT_POSTFIELDS => $avaliacao,
                CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
                CURLOPT_TIMEOUT_MS => '2000'
            ]);
            // Envio e armazenamento da resposta
            $response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            // Fecha e limpa recursos
            curl_close($curl);

            if($httpcode == "200") {
                return json_decode($response);
            } else {
                throw new InfraException('Erro ao cadastrar avalia��o');
            }

        } catch (Exception $e) {
            throw new InfraException('Erro contando Avalia��es.', $e);
        }
    }
    protected function retornarUrlApi() {
        $objConfiguracaoSEI = ConfiguracaoSEI::getInstance();
        $arrConfiguracoes = $objConfiguracaoSEI->getArrConfiguracoes();

        if ($arrConfiguracoes['SEI']['Producao']) {
            return 'rhgiadpdin01';
        } else {
            return 'rhgiadhmin01';
        }
    }

    protected function consultarIndexacaoProcessoConectado($numeroProcessoProcedimentoFormatado)
    {
        try {
            $urlApi = $this->retornarUrlApi();

            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://'.$urlApi.':8082/process-recommenders/weighted-mlt-recommender/indexed-ids/'.$numeroProcessoProcedimentoFormatado,
                CURLOPT_TIMEOUT_MS => '2000'
            ]);

            $response = curl_exec($curl);
            curl_close($curl);
            return $response;

        } catch (Exception $e) {
            throw new InfraException('Erro contando Avalia��es.', $e);
        }
    }
}
