<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 07/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('motivo_mesa_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  switch($_GET['acao']){
    case 'motivo_mesa_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoMesaDTO = array();
        for ($i=0;$i<InfraArray::contar($arrStrIds);$i++){
          $objMotivoMesaDTO = new MotivoMesaDTO();
          $objMotivoMesaDTO->setNumIdMotivoMesa($arrStrIds[$i]);
          $arrObjMotivoMesaDTO[] = $objMotivoMesaDTO;
        }
        $objMotivoMesaRN = new MotivoMesaRN();
        $objMotivoMesaRN->excluir($arrObjMotivoMesaDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;


    case 'motivo_mesa_desativar':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjMotivoMesaDTO = array();
        for ($i=0;$i<InfraArray::contar($arrStrIds);$i++){
          $objMotivoMesaDTO = new MotivoMesaDTO();
          $objMotivoMesaDTO->setNumIdMotivoMesa($arrStrIds[$i]);
          $arrObjMotivoMesaDTO[] = $objMotivoMesaDTO;
        }
        $objMotivoMesaRN = new MotivoMesaRN();
        $objMotivoMesaRN->desativar($arrObjMotivoMesaDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

    case 'motivo_mesa_reativar':
      $strTitulo = 'Reativar Motivos';
      if ($_GET['acao_confirmada']=='sim'){
        try{
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjMotivoMesaDTO = array();
          for ($i=0;$i<InfraArray::contar($arrStrIds);$i++){
            $objMotivoMesaDTO = new MotivoMesaDTO();
            $objMotivoMesaDTO->setNumIdMotivoMesa($arrStrIds[$i]);
            $arrObjMotivoMesaDTO[] = $objMotivoMesaDTO;
          }
          $objMotivoMesaRN = new MotivoMesaRN();
          $objMotivoMesaRN->reativar($arrObjMotivoMesaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        } 
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
        die;
      } 
      break;


    case 'motivo_mesa_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Motivo de Mesa','Selecionar Motivos de Mesa');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='motivo_mesa_cadastrar'){
        if (isset($_GET['id_motivo_mesa'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_motivo_mesa']);
        }
      }
      break;

    case 'motivo_mesa_listar':
      $strTitulo = 'Motivos de Mesa';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'motivo_mesa_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  if ($_GET['acao'] == 'motivo_mesa_listar' || $_GET['acao'] == 'motivo_mesa_selecionar'){
    $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_cadastrar');
    if ($bolAcaoCadastrar){
      $arrComandos[] = '<button type="button" accesskey="N" id="btnNovo" value="Novo" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_mesa_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ovo</button>';
    }
  }

  $objMotivoMesaDTO = new MotivoMesaDTO();
  $objMotivoMesaDTO->setBolExclusaoLogica(false);
  $objMotivoMesaDTO->retNumIdMotivoMesa();
  $objMotivoMesaDTO->retStrDescricao();
  $objMotivoMesaDTO->retNumOrdem();
  $objMotivoMesaDTO->retStrSinAtivo();

  /*
  if ($_GET['acao'] == 'motivo_mesa_reativar'){
    //Lista somente inativos
    $objMotivoMesaDTO->setBolExclusaoLogica(false);
    $objMotivoMesaDTO->setStrSinAtivo('N');
  }
  */

  PaginaSEI::getInstance()->prepararOrdenacao($objMotivoMesaDTO, 'Ordem', InfraDTO::$TIPO_ORDENACAO_ASC);
  //PaginaSEI::getInstance()->prepararPaginacao($objMotivoMesaDTO);

  $objMotivoMesaRN = new MotivoMesaRN();
  $arrObjMotivoMesaDTO = $objMotivoMesaRN->listar($objMotivoMesaDTO);

  //PaginaSEI::getInstance()->processarPaginacao($objMotivoMesaDTO);
  $numRegistros = InfraArray::contar($arrObjMotivoMesaDTO);

  if ($numRegistros > 0){

    $bolCheck = false;

    if ($_GET['acao']=='motivo_mesa_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_alterar');
      $bolAcaoImprimir = false;
      //$bolAcaoGerarPlanilha = false;
      $bolAcaoExcluir = false;
      $bolAcaoDesativar = false;
      $bolCheck = true;
    /*
    }else if ($_GET['acao']=='motivo_mesa_reativar'){
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_consultar');
      $bolAcaoAlterar = false;
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_excluir');
      $bolAcaoDesativar = false;
    */
    }else{
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_alterar');
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_excluir');
      $bolAcaoDesativar = SessaoSEI::getInstance()->verificarPermissao('motivo_mesa_desativar');
    }

    
    if ($bolAcaoDesativar){
      //$bolCheck = true;
      //$arrComandos[] = '<button type="button" accesskey="t" id="btnDesativar" value="Desativar" onclick="acaoDesativacaoMultipla();" class="infraButton">Desa<span class="infraTeclaAtalho">t</span>ivar</button>';
      $strLinkDesativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_mesa_desativar&acao_origem='.$_GET['acao']);
    }

    if ($bolAcaoReativar){
      //$bolCheck = true;
      //$arrComandos[] = '<button type="button" accesskey="R" id="btnReativar" value="Reativar" onclick="acaoReativacaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">R</span>eativar</button>';
      $strLinkReativar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_mesa_reativar&acao_origem='.$_GET['acao'].'&acao_confirmada=sim');
    }
    

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_mesa_excluir&acao_origem='.$_GET['acao']);
    }

    /*
    if ($bolAcaoGerarPlanilha){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="P" id="btnGerarPlanilha" value="Gerar Planilha" onclick="infraGerarPlanilhaTabela(\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=infra_gerar_planilha_tabela').'\');" class="infraButton">Gerar <span class="infraTeclaAtalho">P</span>lanilha</button>';
    }
    */

    $strResultado = '';

    //if ($_GET['acao']!='motivo_mesa_reativar'){
      $strSumarioTabela = 'Tabela de Motivos de Mesa.';
      $strCaptionTabela = 'Motivos de Mesa';
    //}else{
    //  $strSumarioTabela = 'Tabela de Motivos de Mesa Inativos.';
    //  $strCaptionTabela = 'Motivos de Mesa Inativos';
    //}

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objMotivoMesaDTO,'Descri��o','Descricao',$arrObjMotivoMesaDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="10%">'.PaginaSEI::getInstance()->getThOrdenacao($objMotivoMesaDTO,'Ordem','Ordem',$arrObjMotivoMesaDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="15%">A��es</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      if ($arrObjMotivoMesaDTO[$i]->getStrSinAtivo()=='S'){
        $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      }else{
        $strCssTr = '<tr class="trVermelha">';
      }

      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjMotivoMesaDTO[$i]->getNumIdMotivoMesa(),$arrObjMotivoMesaDTO[$i]->getStrDescricao()).'</td>';
      }
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjMotivoMesaDTO[$i]->getStrDescricao()).'</td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML($arrObjMotivoMesaDTO[$i]->getNumOrdem()).'</td>';
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjMotivoMesaDTO[$i]->getNumIdMotivoMesa());

      //if ($bolAcaoConsultar){
      //  $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_mesa_consultar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_motivo_mesa='.$arrObjMotivoMesaDTO[$i]->getNumIdMotivoMesa()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeConsultar().'" title="Consultar Motivo de Mesa" alt="Consultar Motivo de Mesa" class="infraImg" /></a>&nbsp;';
      //}

      if ($bolAcaoAlterar && $arrObjMotivoMesaDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=motivo_mesa_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_motivo_mesa='.$arrObjMotivoMesaDTO[$i]->getNumIdMotivoMesa()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Motivo de Mesa" alt="Alterar Motivo de Mesa" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoDesativar || $bolAcaoReativar || $bolAcaoExcluir){
        $strId = $arrObjMotivoMesaDTO[$i]->getNumIdMotivoMesa();
        $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjMotivoMesaDTO[$i]->getStrDescricao());
      }

      if ($bolAcaoDesativar && $arrObjMotivoMesaDTO[$i]->getStrSinAtivo()=='S'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoDesativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeDesativar().'" title="Desativar Motivo de Mesa" alt="Desativar Motivo de Mesa" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoReativar && $arrObjMotivoMesaDTO[$i]->getStrSinAtivo()=='N'){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoReativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeReativar().'" title="Reativar Motivo de Mesa" alt="Reativar Motivo de Mesa" class="infraImg" /></a>&nbsp;';
      }


      if ($bolAcaoExcluir){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoExcluir(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Motivo de Mesa" alt="Excluir Motivo de Mesa" class="infraImg" /></a>&nbsp;';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'motivo_mesa_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='motivo_mesa_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('btnFechar').focus();
  }
  infraEfeitoTabelas();
}

<? if ($bolAcaoDesativar){ ?>
function acaoDesativar(id,desc){
  if (confirm("Confirma desativa��o do Motivo de Mesa \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoMesaLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoMesaLista').submit();
  }
}

function acaoDesativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Mesa selecionado.');
    return;
  }
  if (confirm("Confirma desativa��o dos Motivos de Mesa selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoMesaLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmMotivoMesaLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoReativar){ ?>
function acaoReativar(id,desc){
  if (confirm("Confirma reativa��o do Motivo de Mesa \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoMesaLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoMesaLista').submit();
  }
}

function acaoReativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Mesa selecionado.');
    return;
  }
  if (confirm("Confirma reativa��o dos Motivos de Mesa selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoMesaLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmMotivoMesaLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclus�o do Motivo de Mesa \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmMotivoMesaLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoMesaLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Motivo de Mesa selecionado.');
    return;
  }
  if (confirm("Confirma exclus�o dos Motivos de Mesa selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmMotivoMesaLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmMotivoMesaLista').submit();
  }
}
<? } ?>

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmMotivoMesaLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  //PaginaSEI::getInstance()->abrirAreaDados('5em');
  //PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>