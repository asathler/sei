<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 07/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class MotivoMesaDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'motivo_mesa';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdMotivoMesa','id_motivo_mesa');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Descricao','descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'Ordem','ordem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'SinAtivo','sin_ativo');

    $this->configurarPK('IdMotivoMesa',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarExclusaoLogica('SinAtivo', 'N');

  }
}
?>