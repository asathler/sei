<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 04/10/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*
* Vers�o no SVN: $Id$
*/

try {
  require_once __DIR__ .'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);


  $numIdItemSessaoJulgamento=$_GET['id_item_sessao_julgamento'];

  $objSustentacaoOralDTO = new SustentacaoOralDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'sustentacao_oral_cadastrar':
      $strTitulo = 'Sustenta��es Orais';
      try {
        $objSustentacaoOralRN = new SustentacaoOralRN();
        $objSustentacaoOralDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
        $objSustentacaoOralDTO->setNumIdParteProcedimento($_GET['id_parte_procedimento']);
        $objSustentacaoOralDTO = $objSustentacaoOralRN->cadastrar($objSustentacaoOralDTO);
        header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sustentacao_oral_consultar&acao_origem=' . $_GET['acao'] . '&id_item_sessao_julgamento=' . $numIdItemSessaoJulgamento));
        die;
      } catch (Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      break;
    case 'sustentacao_oral_excluir':
      $strTitulo = 'Sustenta��es Orais';
      try {
        $objSustentacaoOralRN = new SustentacaoOralRN();
        $objSustentacaoOralDTO->setNumIdSustentacaoOral($_GET['id_sustentacao_oral']);
        $objSustentacaoOralDTO = $objSustentacaoOralRN->excluir(array($objSustentacaoOralDTO));
        header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sustentacao_oral_consultar&acao_origem=' . $_GET['acao'] . '&id_item_sessao_julgamento=' . $numIdItemSessaoJulgamento));
        die;
      } catch (Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      break;


    case 'sustentacao_oral_consultar':
      $strTitulo = 'Sustenta��es Orais';
//      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $bolCadastrar=SessaoSEI::getInstance()->verificarPermissao('sustentacao_oral_cadastrar');

  $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
  $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
  $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
  $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
  $objItemSessaoJulgamentoDTO->retStrNomeColegiado();
  $objItemSessaoJulgamentoDTO->retDthSessaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);


  $objParteProcedimentoDTO=new ParteProcedimentoDTO();
  $objParteProcedimentoRN=new ParteProcedimentoRN();
  $objParteProcedimentoDTO->setDblIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
  $objParteProcedimentoDTO->retNumIdParteProcedimento();
  $objParteProcedimentoDTO->retNumIdContato();
  $objParteProcedimentoDTO->retStrNomeContato();
  $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
  $objParteProcedimentoDTO->retNumIdQualificacaoParte();
  $arrObjParteProcedimentoDTO=$objParteProcedimentoRN->listar($objParteProcedimentoDTO);

  $objSustentacaoOralDTO=new SustentacaoOralDTO();
  $objSustentacaoOralRN = new SustentacaoOralRN();
  $objSustentacaoOralDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
  $objSustentacaoOralDTO->retNumIdSustentacaoOral();
  $objSustentacaoOralDTO->retNumIdContato();
  $objSustentacaoOralDTO->retNumIdQualificacaoParte();
  $objSustentacaoOralDTO->retStrNomeContato();
  $objSustentacaoOralDTO->retStrDescricaoQualificacaoParte();
  $arrObjSustentacaoOralDTO = $objSustentacaoOralRN->listar($objSustentacaoOralDTO);
  $arrIdContatoSustentacao=InfraArray::converterArrInfraDTO($arrObjSustentacaoOralDTO,'IdContato','IdContato');
  
  $numRegistrosParteProcedimento=InfraArray::contar($arrObjParteProcedimentoDTO);
  $strResultadoParteProcedimento = '';
  if($numRegistrosParteProcedimento>0) {
    $strSumarioTabela = 'Tabela de Partes Qualificadas no Processo.';
    $strCaptionTabela = 'Partes';


    $strResultadoParteProcedimento .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
    $strResultadoParteProcedimento .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistrosParteProcedimento) . '</caption>';
    $strResultadoParteProcedimento .= '<tr>';
    $strResultadoParteProcedimento .= '<th class="infraTh">Nome</th>' . "\n";
    $strResultadoParteProcedimento .= '<th class="infraTh">Qualifica��o</th>' . "\n";
    if($bolCadastrar){
      $strResultadoParteProcedimento .= '<th class="infraTh">A��es</th>' . "\n";
    }
    $strResultadoParteProcedimento .= '</tr>' . "\n";
    $strCssTr = '';
    for ($i = 0; $i < $numRegistrosParteProcedimento; $i++) {

      $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
      $strResultadoParteProcedimento .= $strCssTr;
      $strResultadoParteProcedimento .= '<td>' . $arrObjParteProcedimentoDTO[$i]->getStrNomeContato() .'</td>'."\n";
      $strResultadoParteProcedimento .= '<td>' . $arrObjParteProcedimentoDTO[$i]->getStrDescricaoQualificacaoParte() .'</td>'."\n";
      if($bolCadastrar) {
        $strResultadoParteProcedimento .= '<td align="center">';
        if (!isset($arrIdContatoSustentacao[$arrObjParteProcedimentoDTO[$i]->getNumIdContato()])) {
          $strResultadoParteProcedimento .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sustentacao_oral_cadastrar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_parte_procedimento=' . $arrObjParteProcedimentoDTO[$i]->getNumIdParteProcedimento() . '&id_item_sessao_julgamento=' . $numIdItemSessaoJulgamento) . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="' . PaginaSEI::getInstance()->getIconeMais() . '" title="Incluir para Sustenta��o Oral" alt="Incluir para Sustenta��o Oral" class="infraImg" /></a>&nbsp;';
        }
        $strResultadoParteProcedimento .= '</td>';
      }
      $strResultadoParteProcedimento .= '</tr>' . "\n";
    }
    $strResultadoParteProcedimento .= '</table>';
  }



  $numRegistrosSustentacaoOral=InfraArray::contar($arrObjSustentacaoOralDTO);
  $strResultadoSustentacaoOral = '';
  if($numRegistrosSustentacaoOral>0) {
    $strSumarioTabela = 'Tabela de Sustenta��es Orais.';
    $strCaptionTabela = 'Sustenta��es Orais';


    $strResultadoSustentacaoOral .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
    $strResultadoSustentacaoOral .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistrosSustentacaoOral) . '</caption>';
    $strResultadoSustentacaoOral .= '<tr>';
    $strResultadoSustentacaoOral .= '<th class="infraTh">Nome</th>' . "\n";
    $strResultadoSustentacaoOral .= '<th class="infraTh">Qualifica��o</th>' . "\n";
    if($bolCadastrar) {
      $strResultadoSustentacaoOral .= '<th class="infraTh">A��es</th>' . "\n";
    }
    $strResultadoSustentacaoOral .= '</tr>' . "\n";
    $strCssTr = '';
    for ($i = 0; $i < $numRegistrosSustentacaoOral; $i++) {

      $strCssTr = ($strCssTr == '<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
      $strResultadoSustentacaoOral .= $strCssTr;
      $strResultadoSustentacaoOral .= '<td>' . $arrObjSustentacaoOralDTO[$i]->getStrNomeContato() .'</td>'."\n";
      $strResultadoSustentacaoOral .= '<td>' . $arrObjSustentacaoOralDTO[$i]->getStrDescricaoQualificacaoParte() .'</td>'."\n";
      if($bolCadastrar) {
        $strResultadoSustentacaoOral .= '<td align="center">';
        $strResultadoSustentacaoOral .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sustentacao_oral_excluir&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_sustentacao_oral=' . $arrObjSustentacaoOralDTO[$i]->getNumIdSustentacaoOral() . '&id_item_sessao_julgamento=' . $numIdItemSessaoJulgamento) . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="' . PaginaSEI::getInstance()->getIconeMenos() . '" title="Excluir de Sustenta��o Oral" alt="Excluir de Sustenta��o Oral" class="infraImg" /></a>&nbsp;';
        $strResultadoParteProcedimento .= '</td>';
      }
      $strResultadoSustentacaoOral .= '</tr>' . "\n";
    }
    $strResultadoSustentacaoOral .= '</table>';
  }

  $strLinkProcesso = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>

  #lblColegiado {position:absolute;left:0;top:0;width:40%;}
  #txtColegiado {position:absolute;left:0;top:38%;width:40%;}

  #lblProcesso {position:absolute;left:66%;top:0;width:30%;}
  #ancProcesso {position:absolute;left:66%;top:38%;}

  #lblSessao {position:absolute;left:43%;top:0;width:20%;}
  #txtSessao {position:absolute;left:43%;top:38%;width: 20%;}

  #lblTabelaPartes {position:absolute;left:0;top:0;width:99%;}
  #lblTabelaSustentacoes {position:absolute;left:0;top:0;width:99%;}

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='sustentacao_oral_cadastrar'){
    document.getElementById('selItemSessaoJulgamento').focus();
  } else if ('<?=$_GET['acao']?>'=='sustentacao_oral_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
}

function validarCadastro() {
  if (!infraSelectSelecionado('selItemSessaoJulgamento')) {
    alert('Selecione um item.');
    document.getElementById('selItemSessaoJulgamento').focus();
    return false;
  }

  if (!infraSelectSelecionado('selQualificacaoParte')) {
    alert('Selecione uma qualifica��o.');
    document.getElementById('selQualificacaoParte').focus();
    return false;
  }

  if (!infraSelectSelecionado('selContato')) {
    alert('Selecione uma parte.');
    document.getElementById('selContato').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmSustentacaoOralCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>

  <label id="lblColegiado" for="txtColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <input type="text" id="txtColegiado" name="txtColegiado" class="infraText" value="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrNomeColegiado()); ?>" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>

  <label id="lblProcesso" for="ancProcesso" accesskey="" class="infraLabelObrigatorio">Processo:</label>
  <a id="ancProcesso" href="<?=$strLinkProcesso?>" target="_blank" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" alt="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>" title="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>"><?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?></a>

  <label id="lblSessao" for="txtSessao" accesskey="" class="infraLabelObrigatorio">Data da Sess�o:</label>
  <input type="text" id="txtSessao" name="txtSessao" class="infraText" value="<?=PaginaSEI::tratarHTML(substr($objItemSessaoJulgamentoDTO->getDthSessaoSessaoJulgamento(),0,16)); ?>" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>

  <?
PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->abrirAreaDados('3em');
  ?>
  <label id="lblTabelaPartes" class="infraLabelTitulo">Partes</label>
    <?

PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->montarAreaTabela($strResultadoParteProcedimento,$numRegistrosParteProcedimento,true);
?><br><br>
  <?
PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?>
  <label id="lblTabelaSustentacoes" class="infraLabelTitulo">Sustenta��es</label>
  <?

PaginaSEI::getInstance()->fecharAreaDados();
PaginaSEI::getInstance()->montarAreaTabela($strResultadoSustentacaoOral,$numRegistrosSustentacaoOral,true);?>

  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>