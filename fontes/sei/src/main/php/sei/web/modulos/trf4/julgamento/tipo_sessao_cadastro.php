<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 06/04/2020 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*
* Vers�o no SVN: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('tipo_sessao_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objTipoSessaoDTO = new TipoSessaoDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  $arrObjTipoItemDTO=TipoSessaoBlocoRN::listarValoresTipoItem();
  $arrTipoItem=InfraArray::converterArrInfraDTO($arrObjTipoItemDTO,'Descricao','StaTipo');

  switch($_GET['acao']){
    case 'tipo_sessao_cadastrar':
      $strTitulo = 'Novo Tipo de Sess�o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarTipoSessao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objTipoSessaoDTO->setNumIdTipoSessao(null);
      $objTipoSessaoDTO->setStrDescricao($_POST['txtDescricao']);
      $objTipoSessaoDTO->setStrSinVirtual(PaginaSEI::getInstance()->getCheckbox($_POST['chkSinVirtual']));
      $objTipoSessaoDTO->setStrSinAtivo('S');

      $arr = PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnTipoSessaoBloco']);

      $ordem=InfraArray::contar($arr);
      $arrObjTipoSessaoBlocoDTO=[];

      foreach($arr as $linha){
        $objTipoSessaoBlocoDTO=new TipoSessaoBlocoDTO();
        $objTipoSessaoBlocoDTO->setNumOrdem($ordem--);
        $objTipoSessaoBlocoDTO->setStrDescricao($linha[0]);
        $objTipoSessaoBlocoDTO->setStrSinAgruparMembro($linha[3]);
        $objTipoSessaoBlocoDTO->setStrStaTipoItem($linha[4]);
        $arrObjTipoSessaoBlocoDTO[] = $objTipoSessaoBlocoDTO;
      }

      $objTipoSessaoDTO->setArrObjTipoSessaoBlocoDTO($arrObjTipoSessaoBlocoDTO);
      $strTipoSessaoBloco = $_POST['hdnTipoSessaoBloco'];

      if (isset($_POST['sbmCadastrarTipoSessao'])) {
        try{
          $objTipoSessaoRN = new TipoSessaoRN();
          $objTipoSessaoDTO = $objTipoSessaoRN->cadastrar($objTipoSessaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Tipo de Sess�o "'.$objTipoSessaoDTO->getStrDescricao().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_tipo_sessao='.$objTipoSessaoDTO->getNumIdTipoSessao().PaginaSEI::getInstance()->montarAncora($objTipoSessaoDTO->getNumIdTipoSessao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'tipo_sessao_alterar':
      $strTitulo = 'Alterar Tipo de Sess�o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarTipoSessao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_tipo_sessao'])){
        $objTipoSessaoDTO->setNumIdTipoSessao($_GET['id_tipo_sessao']);
        $objTipoSessaoDTO->retArrObjTipoSessaoBlocoDTO();
        $objTipoSessaoDTO->retTodos();
        $objTipoSessaoRN = new TipoSessaoRN();
        $objTipoSessaoDTO = $objTipoSessaoRN->consultar($objTipoSessaoDTO);
        if ($objTipoSessaoDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objTipoSessaoDTO->setNumIdTipoSessao($_POST['hdnIdTipoSessao']);
        $objTipoSessaoDTO->setStrDescricao($_POST['txtDescricao']);
        $objTipoSessaoDTO->setStrSinVirtual(PaginaSEI::getInstance()->getCheckbox($_POST['chkSinVirtual']));
        $objTipoSessaoDTO->setStrSinAtivo('S');
        $arr = PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnTipoSessaoBloco']);

        $ordem=InfraArray::contar($arr);
        $arrObjTipoSessaoBlocoDTO=[];

        foreach($arr as $linha){
          $objTipoSessaoBlocoDTO=new TipoSessaoBlocoDTO();
          $objTipoSessaoBlocoDTO->setNumOrdem($ordem--);
          $objTipoSessaoBlocoDTO->setStrDescricao($linha[0]);
          $objTipoSessaoBlocoDTO->setStrSinAgruparMembro($linha[3]);
          $objTipoSessaoBlocoDTO->setStrStaTipoItem($linha[4]);
          $arrObjTipoSessaoBlocoDTO[] = $objTipoSessaoBlocoDTO;
        }

        $objTipoSessaoDTO->setArrObjTipoSessaoBlocoDTO($arrObjTipoSessaoBlocoDTO);
        $strTipoSessaoBloco = $_POST['hdnTipoSessaoBloco'];
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objTipoSessaoDTO->getNumIdTipoSessao())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarTipoSessao'])) {
        try{
          $objTipoSessaoRN = new TipoSessaoRN();
          $objTipoSessaoRN->alterar($objTipoSessaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Tipo de Sess�o "'.$objTipoSessaoDTO->getStrDescricao().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objTipoSessaoDTO->getNumIdTipoSessao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'tipo_sessao_consultar':
      $strTitulo = 'Consultar Tipo de Sess�o';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_tipo_sessao'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objTipoSessaoDTO->setNumIdTipoSessao($_GET['id_tipo_sessao']);
      $objTipoSessaoDTO->setBolExclusaoLogica(false);
      $objTipoSessaoDTO->retTodos();
      $objTipoSessaoRN = new TipoSessaoRN();
      $objTipoSessaoDTO = $objTipoSessaoRN->consultar($objTipoSessaoDTO);
      if ($objTipoSessaoDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  if (!isset($_POST['hdnTipoSessaoBloco']) && $objTipoSessaoDTO->getNumIdTipoSessao()!=null) {
    $arrObjTipoSessaoBlocoDTO = $objTipoSessaoDTO->getArrObjTipoSessaoBlocoDTO();
    $arrTipoSessaoBloco = [];

    if (InfraArray::contar($arrObjTipoSessaoBlocoDTO)>0) {

      /** @var TipoSessaoBlocoDTO $objTipoSessaoBlocoDTO */
      foreach ($arrObjTipoSessaoBlocoDTO as $objTipoSessaoBlocoDTO) {
        $arrTipoSessaoBloco[] = [
            $objTipoSessaoBlocoDTO->getStrDescricao(),
            $objTipoSessaoBlocoDTO->getStrSinAgruparMembro()=='S'?'Sim':'N�o',
            $arrTipoItem[$objTipoSessaoBlocoDTO->getStrStaTipoItem()],
            $objTipoSessaoBlocoDTO->getStrSinAgruparMembro(),
            $objTipoSessaoBlocoDTO->getStrStaTipoItem(),
        ];
      }
    }
    $strTipoSessaoBloco = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrTipoSessaoBloco);
  }
  $strItensSelTipoItem=TipoSessaoBlocoINT::montarSelectStaTipoItem('null','&nbsp;',null);

  }catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
#lblDescricao {position:absolute;left:0;top:0;width:50%;}
#txtDescricao {position:absolute;left:0;top:32%;width:50%;}
#divSinVirtual {position:absolute;left:52%;top:38%;}

    #divSinAgrupar {position: absolute;left:59%;top:63%}
    #divTabelaTipoSessaoBloco {margin-top:1em;}
    #tblTipoSessaoBloco {width:95%;}
    #lblComposicao {position:absolute;top:0%;width:95%;margin-top:.3em;}

    #lblDescricaoBloco {position:absolute;left:0%;top:40%;width:40%;}
    #txtDescricaoBloco {position:absolute;left:0%;top:60%;width:40%;}

    #lblTipoItem {position:absolute;left:42%;top:40%;width:15%;}
    #selTipoItem {position:absolute;left:42%;top:60%;width:15%;}

    #btnAdicionar {position:absolute;left:80%;top:60%;}


<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>
  var objTabelaTipoSessaoBloco;
function inicializar(){
  if ('<?=$_GET['acao']?>'=='tipo_sessao_cadastrar'){
    document.getElementById('txtDescricao').focus();
  } else if ('<?=$_GET['acao']?>'=='tipo_sessao_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }

  var bolAcoes=<?=$_GET['acao']=='tipo_sessao_consultar'?'false':'true'?>;
  objTabelaTipoSessaoBloco = new infraTabelaDinamica('tblTipoSessaoBloco','hdnTipoSessaoBloco',false,bolAcoes,bolAcoes);
  objTabelaTipoSessaoBloco.inserirNoInicio=false;


  infraEfeitoTabelas();
}
  function adicionar(){
    if (infraTrim(document.getElementById('txtDescricaoBloco').value)=='') {
      alert('Informe a descri��o do bloco.');
      document.getElementById('txtDescricaoBloco').focus();
      return false;
    }

    if (!infraSelectSelecionado('selTipoItem')) {
      alert('Selecione o Tipo de Bloco.');
      document.getElementById('selTipoItem').focus();
      return false;
    }


    var sinAgrupado,strAgrupado;
    sinAgrupado=document.getElementById('chkSinAgrupar').checked?'S':'N';
    strAgrupado=sinAgrupado==='S'?'Sim':'N�o';
    var txtDescricao=document.getElementById('txtDescricaoBloco').value;

    var optionSelecionado=document.getElementById('selTipoItem').options[document.getElementById('selTipoItem').selectedIndex];
    var txtTipoItem=optionSelecionado.text;

    objTabelaTipoSessaoBloco.adicionar([txtDescricao,strAgrupado,txtTipoItem,sinAgrupado,optionSelecionado.value]);


    document.getElementById('txtDescricaoBloco').value='';
    document.getElementById('selTipoItem').value=null;
    document.getElementById('chkSinAgrupar').checked=false;
  }
function validarCadastro() {
  if (infraTrim(document.getElementById('txtDescricao').value)=='') {
    alert('Informe  .');
    document.getElementById('txtDescricao').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmTipoSessaoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('6em');
?>
  <label id="lblDescricao" for="txtDescricao" accesskey="" class="infraLabelObrigatorio">Descricao:</label>
  <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="<?=PaginaSEI::tratarHTML($objTipoSessaoDTO->getStrDescricao());?>" onkeypress="return infraMascaraTexto(this,event,50);" maxlength="50" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  <div id="divSinVirtual" class="infraDivCheckbox">
    <input type="checkbox" id="chkSinVirtual" name="chkSinVirtual" class="infraCheckbox" <?=PaginaSEI::getInstance()->setCheckbox($objTipoSessaoDTO->getStrSinVirtual())?>  tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <label id="lblSinVirtual" for="chkSinVirtual" accesskey="" class="infraLabelCheckbox">Sess�o Virtual</label>
  </div>
<?
PaginaSEI::getInstance()->fecharAreaDados();
if ($_GET['acao']!='tipo_sessao_consultar'){
  PaginaSEI::getInstance()->abrirAreaDados('9em');?>
  <label id="lblComposicao" accesskey="" class="infraLabelTitulo">&nbsp;&nbsp;Blocos</label>

  <label id="lblDescricaoBloco" for="txtDescricaoBloco" accesskey="" class="infraLabelOpcional">Descricao:</label>
  <input type="text" id="txtDescricaoBloco" name="txtDescricaoBloco" class="infraText" value="" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblTipoItem" for="selTipoItem" accesskey="" class="infraLabelOpcional">Tipo de Bloco:</label>
  <select id="selTipoItem" name="selTipoItem" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelTipoItem?>
  </select>


  <div id="divSinAgrupar" class="infraDivCheckbox">
    <input type="checkbox" id="chkSinAgrupar" name="chkSinAgrupar" class="infraCheckbox"  tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>"/>
    <label id="lblSinAgrupar" for="chkSinAgrupar" accesskey="" class="infraLabelCheckbox">Agrupar processos por membro</label>
  </div>

  <input type="button" id="btnAdicionar" onclick="adicionar();" name="btnAdicionar" value="Adicionar" class="infraButton" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
}
?>
  <input type="hidden" id="hdnIdTipoSessao" name="hdnIdTipoSessao" value="<?=$objTipoSessaoDTO->getNumIdTipoSessao();?>" />

  <div id="divTabelaTipoSessaoBloco" class="infraAreaTabela">
    <table id="tblTipoSessaoBloco" class="infraTable">
      <caption class="infraCaption"><?=PaginaSEI::getInstance()->gerarCaptionTabela("Blocos",0)?></caption>
      <tr>
        <th class="infraTh" style="width:35%;">Descricao</th>
        <th class="infraTh" align="center" style="text-align:center;width:10%;">Agrupar por Membro</th>
        <th class="infraTh" align="center">Tipo de Bloco</th>
        <th style="display:none;">SinAgrupar</th>
        <th style="display:none;">StaTipoItem</th>
        <? if($_GET['acao']!='tipo_sessao_consultar') {?>
          <th class="infraTh" style="width:10%;">A��es</th>
        <? }?>
      </tr>
    </table>
    <input type="hidden" id="hdnTipoSessaoBloco" name="hdnTipoSessaoBloco" value="<?=$strTipoSessaoBloco?>" />
  </div>
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>