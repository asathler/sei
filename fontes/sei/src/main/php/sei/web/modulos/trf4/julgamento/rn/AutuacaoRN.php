<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class AutuacaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarDblIdProcedimento(AutuacaoDTO $objAutuacaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAutuacaoDTO->getDblIdProcedimento())){
      $objInfraException->adicionarValidacao('Processo n�o informado.');
    }
  }

  private function validarStrDescricao(AutuacaoDTO $objAutuacaoDTO, InfraException $objInfraException){
    if (!InfraString::isBolVazia($objAutuacaoDTO->getStrDescricao())){
      $objAutuacaoDTO->setStrDescricao(trim($objAutuacaoDTO->getStrDescricao()));
      if (strlen($objAutuacaoDTO->getStrDescricao())>2000){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 2000 caracteres.');
      }
    }
  }

  protected function registrarControlado(AutuacaoDTO $objAutuacaoDTO) {
    try{

      $objAutuacaoBD = new ParteProcedimentoBD($this->getObjInfraIBanco());
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('procedimento_autuacao_gerenciar',__METHOD__,$objAutuacaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();
      $this->validarDblIdProcedimento($objAutuacaoDTO,$objInfraException);
      $this->validarStrDescricao($objAutuacaoDTO,$objInfraException);
      $objInfraException->lancarValidacoes();

      $bolPossuiTipoMateria=false;
      $arrObjRelAutuacaoTipoMateriaDTO=$objAutuacaoDTO->getArrObjRelAutuacaoTipoMateriaDTO();

      if(count($arrObjRelAutuacaoTipoMateriaDTO)>0){
        $arrObjRelAutuacaoTipoMateriaDTO=InfraArray::indexarArrInfraDTO($arrObjRelAutuacaoTipoMateriaDTO,'IdTipoMateria');
        $bolPossuiTipoMateria=true;
        $numIdTipoMateriaPrincipal=$objAutuacaoDTO->getNumIdTipoMateria();
        if(!isset($arrObjRelAutuacaoTipoMateriaDTO[$numIdTipoMateriaPrincipal])){
          $objInfraException->lancarValidacao('Tipo de mat�ria principal n�o informado.');
        }
      }

      $objRelAutuacaoTipoMateriaRN=new RelAutuacaoTipoMateriaRN();
      $objAutuacaoDTOBanco=new AutuacaoDTO();
      $objAutuacaoDTOBanco->setDblIdProcedimento($objAutuacaoDTO->getDblIdProcedimento());
      $objAutuacaoDTOBanco->retTodos();
      $objAutuacaoDTOBanco->retArrObjRelAutuacaoTipoMateriaDTO();
      $objAutuacaoDTOBanco=$this->consultar($objAutuacaoDTOBanco);

      if ($objAutuacaoDTOBanco!=null){
        $objRelAutuacaoTipoMateriaDTO=new RelAutuacaoTipoMateriaDTO();
        $objRelAutuacaoTipoMateriaDTO->setNumIdAutuacao($objAutuacaoDTOBanco->getNumIdAutuacao());
        $objRelAutuacaoTipoMateriaDTO->retNumIdAutuacao();
        $objRelAutuacaoTipoMateriaDTO->retNumIdTipoMateria();
        $objRelAutuacaoTipoMateriaRN->excluir($objRelAutuacaoTipoMateriaRN->listar($objRelAutuacaoTipoMateriaDTO));

        if(!$bolPossuiTipoMateria && InfraString::isBolVazia($objAutuacaoDTO->getStrDescricao())) {
          $objAutuacaoBD->excluir($objAutuacaoDTOBanco);
        } else {
          $objAutuacaoBD->alterar($objAutuacaoDTO);
          $this->montarIndexacao($objAutuacaoDTO);
          $objAutuacaoDTOBanco=$objAutuacaoDTO;
        }
      } else {

        if($bolPossuiTipoMateria  || !InfraString::isBolVazia($objAutuacaoDTO->getStrDescricao())) {
          $objAutuacaoDTOBanco = $objAutuacaoBD->cadastrar($objAutuacaoDTO);
          $this->montarIndexacao($objAutuacaoDTOBanco);
        }
      }
      foreach ($arrObjRelAutuacaoTipoMateriaDTO as $objRelAutuacaoTipoMateriaDTO) {
        $objRelAutuacaoTipoMateriaDTO->setNumIdAutuacao($objAutuacaoDTOBanco->getNumIdAutuacao());
        $objRelAutuacaoTipoMateriaRN->cadastrar($objRelAutuacaoTipoMateriaDTO);
      }


      $objParteProcedimentoDTO=new ParteProcedimentoDTO();
      $objParteProcedimentoRN=new ParteProcedimentoRN();
      $objParteProcedimentoDTO->setDblIdProcedimento($objAutuacaoDTO->getDblIdProcedimento());
      $objParteProcedimentoDTO->retTodos();
      $arrObjParteProcedimentoDTOBanco=$objParteProcedimentoRN->listar($objParteProcedimentoDTO);
      if($arrObjParteProcedimentoDTOBanco==null){
        $arrObjParteProcedimentoDTOBanco=array();
      }

      $arrObjParteProcedimentoDTO=$objAutuacaoDTO->getArrObjParteProcedimentoDTO();

      if($arrObjParteProcedimentoDTO==null){
        $objParteProcedimentoRN->excluir($arrObjParteProcedimentoDTOBanco);
      } else {
        //verificar alteracoes
        $arrObjParteProcedimentoDTOBanco=InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTOBanco,'IdContato');
        $arrObjParteProcedimentoDTO=InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO,'IdContato');

        $arrExclusao=array();
        foreach ($arrObjParteProcedimentoDTOBanco as $numIdContato=>$objParteProcedimentoDTO) {
          if (!isset($arrObjParteProcedimentoDTO[$numIdContato])) {
            $arrExclusao[] = $objParteProcedimentoDTO;
            continue;
          }
          $objParteProcedimentoDTOpar=$arrObjParteProcedimentoDTO[$numIdContato];
          if($objParteProcedimentoDTO->getNumIdQualificacaoParte()!=$objParteProcedimentoDTOpar->getNumIdQualificacaoParte()) {
            $objParteProcedimentoDTO->setNumIdQualificacaoParte($objParteProcedimentoDTOpar->getNumIdQualificacaoParte());
            $objParteProcedimentoDTO->setNumIdUnidade($objParteProcedimentoDTOpar->getNumIdUnidade());
            $objParteProcedimentoRN->alterar($objParteProcedimentoDTO);
          }
        }
        $objParteProcedimentoRN->excluir($arrExclusao);
        foreach ($arrObjParteProcedimentoDTO as $numIdContato=>$objParteProcedimentoDTO) {
          if (!isset($arrObjParteProcedimentoDTOBanco[$numIdContato])) {
            $objParteProcedimentoRN->cadastrar($objParteProcedimentoDTO);
          }
        }
      }

      //Auditoria

      return $objAutuacaoDTOBanco;

    }catch(Exception $e){
      throw new InfraException('Erro registrando Autua��o.',$e);
    }
  }

  protected function consultarConectado(AutuacaoDTO $objAutuacaoDTO){
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('procedimento_autuacao_consultar',__METHOD__,$objAutuacaoDTO);

      //Regras de Negocio
//      $objInfraException = new InfraException();

//      $objInfraException->lancarValidacoes();

      $objAutuacaoBD = new AutuacaoBD($this->getObjInfraIBanco());
      $ret = $objAutuacaoBD->consultar($objAutuacaoDTO);

      if($ret && $ret->isSetDblIdProcedimento() &&  $objAutuacaoDTO->isRetArrObjParteProcedimentoDTO()) {

        $objParteProcedimentoDTO = new ParteProcedimentoDTO();
        $objParteProcedimentoRN = new ParteProcedimentoRN();
        $objParteProcedimentoDTO->setDblIdProcedimento($objAutuacaoDTO->getDblIdProcedimento());
        $objParteProcedimentoDTO->retStrNomeContato();
        $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
        $objParteProcedimentoDTO->retTodos();
        $arrObjParteProcedimentoDTO = $objParteProcedimentoRN->listar($objParteProcedimentoDTO);

        $ret->setArrObjParteProcedimentoDTO($arrObjParteProcedimentoDTO);
      }
      if($ret && $ret->isSetNumIdAutuacao() &&  $objAutuacaoDTO->isRetArrObjRelAutuacaoTipoMateriaDTO()) {

        $objRelAutuacaoTipoMateriaDTO=new RelAutuacaoTipoMateriaDTO();
        $objRelAutuacaoTipoMateriaRN=new RelAutuacaoTipoMateriaRN();
        $objRelAutuacaoTipoMateriaDTO->setNumIdAutuacao($ret->getNumIdAutuacao());
        $objRelAutuacaoTipoMateriaDTO->retTodos(true);
        $objRelAutuacaoTipoMateriaDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
        $arrObjRelAutuacaoTipoMateria=$objRelAutuacaoTipoMateriaRN->listar($objRelAutuacaoTipoMateriaDTO);
        $ret->setArrObjRelAutuacaoTipoMateriaDTO($arrObjRelAutuacaoTipoMateria);
      }
      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Autua��o.',$e);
    }
  }
  protected function listarConectado(AutuacaoDTO $objAutuacaoDTO) {
    try {

      //Valida Permissao
      //SessaoSEI::getInstance()->validarAuditarPermissao('procedimento_autuacao_consultar',__METHOD__,$objAutuacaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAutuacaoBD = new AutuacaoBD($this->getObjInfraIBanco());
      $ret = $objAutuacaoBD->listar($objAutuacaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Autua��o.',$e);
    }
  }
  protected function excluirConectado(array $arrObjAutuacaoDTO) {
    try {

      //Valida Permissao
      if(SessaoSEI::getInstance()->verificarPermissao('procedimento_autuacao_gerenciar')) {
        SessaoSEI::getInstance()->validarAuditarPermissao('procedimento_autuacao_gerenciar');
      }

      //Regras de Negocio
//      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();
      $objAutuacaoBD = new AutuacaoBD($this->getObjInfraIBanco());
      $objRelAutuacaoTipoMateriaRN=new RelAutuacaoTipoMateriaRN();

      for($i=0, $iMax = InfraArray::contar($arrObjAutuacaoDTO); $i<$iMax; $i++) {
        $objRelAutuacaoTipoMateriaDTO=new RelAutuacaoTipoMateriaDTO();
        $objRelAutuacaoTipoMateriaDTO->setNumIdAutuacao($arrObjAutuacaoDTO[$i]->getNumIdAutuacao());
        $objRelAutuacaoTipoMateriaDTO->retNumIdAutuacao();
        $objRelAutuacaoTipoMateriaDTO->retNumIdTipoMateria();
        $objRelAutuacaoTipoMateriaRN->excluir($objRelAutuacaoTipoMateriaRN->listar($objRelAutuacaoTipoMateriaDTO));
        $objAutuacaoBD->excluir($arrObjAutuacaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Autua��o.',$e);
    }
  }
  protected function contarConectado(AutuacaoDTO $objAutuacaoDTO)
  {
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('procedimento_autuacao_listar', __METHOD__, $objAutuacaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAutuacaoBD = new AutuacaoBD($this->getObjInfraIBanco());
      $ret = $objAutuacaoBD->contar($objAutuacaoDTO);

      //Auditoria

      return $ret;
    } catch (Exception $e) {
      throw new InfraException('Erro contando Autuac�es.', $e);
    }
  }

  protected function montarIndexacaoControlado(AutuacaoDTO $parObjAutuacaoDTO){
    try{

      $objInfraException = new InfraException();

      $objAutuacaoDTO = new AutuacaoDTO();
      $objAutuacaoDTO->retNumIdAutuacao();
      $objAutuacaoDTO->retStrDescricao();

      if (is_array($parObjAutuacaoDTO->getNumIdAutuacao())){
        $objAutuacaoDTO->setNumIdAutuacao($parObjAutuacaoDTO->getNumIdAutuacao(),InfraDTO::$OPER_IN);
      }else{
        $objAutuacaoDTO->setNumIdAutuacao($parObjAutuacaoDTO->getNumIdAutuacao());
      }

      $objAutuacaoDTOIdx = new AutuacaoDTO();
      
      $objAutuacaoBD = new AutuacaoBD($this->getObjInfraIBanco());

      $arrObjAutuacaoDTO = $this->listar($objAutuacaoDTO);

      foreach($arrObjAutuacaoDTO as $objAutuacaoDTO) {

        $objAutuacaoDTOIdx->setStrIdxAutuacao(InfraString::prepararIndexacao($objAutuacaoDTO->getStrDescricao()));
        $objAutuacaoDTOIdx->setNumIdAutuacao($objAutuacaoDTO->getNumIdAutuacao());

        $this->validarStrIdxAutuacao($objAutuacaoDTOIdx, $objInfraException);
        $objInfraException->lancarValidacoes();

        $objAutuacaoBD->alterar($objAutuacaoDTOIdx);
      }

    }catch(Exception $e){
      throw new InfraException('Erro montando indexa��o de Observa��o.',$e);
    }
  }
  private function validarStrIdxAutuacao(AutuacaoDTO $objAutuacaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAutuacaoDTO->getStrIdxAutuacao())){
      $objAutuacaoDTO->setStrIdxAutuacao(null);
    }
  }
}
?>