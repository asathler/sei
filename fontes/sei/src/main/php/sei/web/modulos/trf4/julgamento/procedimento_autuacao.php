<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*
* Vers�o no SVN: $Id$
*/

try {
  require_once __DIR__ .'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $strDesabilitar = '';

  PaginaSEI::getInstance()->setBolArvore(true);

  $arrComandos = array();
  $dblIdProcedimento=$_GET['id_procedimento'];


  $objAutuacaoRN=new AutuacaoRN();
  $objAutuacaoDTO=new AutuacaoDTO();

  $objAutuacaoDTO->setDblIdProcedimento($dblIdProcedimento);
  $objAutuacaoDTO->retTodos();
  $objAutuacaoDTO->retArrObjParteProcedimentoDTO();
  $objAutuacaoDTO->retArrObjRelAutuacaoTipoMateriaDTO();
  $objAutuacaoDTO=$objAutuacaoRN->consultar($objAutuacaoDTO);

  if($objAutuacaoDTO===null){
    $objAutuacaoDTO=new AutuacaoDTO();
    $objAutuacaoDTO->setDblIdProcedimento($dblIdProcedimento);
    $objAutuacaoDTO->setNumIdTipoMateria(null);

    $objParteProcedimentoDTO=new ParteProcedimentoDTO();
    $objParteProcedimentoRN=new ParteProcedimentoRN();
    $objParteProcedimentoDTO->setDblIdProcedimento($objAutuacaoDTO->getDblIdProcedimento());
    $objParteProcedimentoDTO->retTodos();
    $objParteProcedimentoDTO->retStrNomeContato();
    $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();

    $arrObjParteProcedimentoDTOBanco=$objParteProcedimentoRN->listar($objParteProcedimentoDTO);


    $objAutuacaoDTO->setArrObjRelAutuacaoTipoMateriaDTO(array());
    $objAutuacaoDTO->setArrObjParteProcedimentoDTO($arrObjParteProcedimentoDTOBanco);
    $objAutuacaoDTO->setStrDescricao($_POST['txaDescricao']);
  }

  $arrObjParteProcedimentoDTO=array();
  $arrObjRelAutuacaoTipoMateriaDTO=array();
  $bolGerenciar=0;


  switch($_GET['acao']){
    case 'procedimento_autuacao_gerenciar':
      $strTitulo = 'Autua��o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarAutuacao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
      $bolGerenciar=1;




      if (isset($_POST['sbmCadastrarAutuacao'])) {
        try{
          $arrPartes = PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnPartes']);
          $arrMaterias=PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnMaterias']);

          $objBaseDTO=new ParteProcedimentoDTO();
          $objBaseDTO->setDblIdProcedimento($dblIdProcedimento);
          $objBaseDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());

          foreach($arrPartes as $linha){
            $objParteProcedimentoDTO=clone $objBaseDTO;
            $objParteProcedimentoDTO->setNumIdContato($linha[0]);
            $objParteProcedimentoDTO->setStrNomeContato($linha[1]);
            $objParteProcedimentoDTO->setNumIdQualificacaoParte($linha[2]);
            $objParteProcedimentoDTO->setStrDescricaoQualificacaoParte($linha[3]);
            $arrObjParteProcedimentoDTO[] = $objParteProcedimentoDTO;
          }

          $objAutuacaoDTO->setArrObjParteProcedimentoDTO($arrObjParteProcedimentoDTO);

          $i=count($arrMaterias);
          foreach ($arrMaterias as $linha) {
            $objRelAutuacaoTipoMateriaDTO=new RelAutuacaoTipoMateriaDTO();
            $objRelAutuacaoTipoMateriaDTO->setNumIdTipoMateria($linha[0]);
            $objRelAutuacaoTipoMateriaDTO->setStrDescricaoTipoMateria($linha[1]);
            $objRelAutuacaoTipoMateriaDTO->setNumOrdem($i--);
            $arrObjRelAutuacaoTipoMateriaDTO[]=$objRelAutuacaoTipoMateriaDTO;
          }
          $objAutuacaoDTO->setArrObjRelAutuacaoTipoMateriaDTO($arrObjRelAutuacaoTipoMateriaDTO);
          $objAutuacaoDTO->setNumIdTipoMateria($_POST['infraRadioPrincipal']);

          $objAutuacaoDTO->setStrDescricao($_POST['txaDescricao']);

          $objAutuacaoDTO = $objAutuacaoRN->registrar($objAutuacaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Autua��o cadastrada com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_procedimento='.$dblIdProcedimento));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;


    case 'procedimento_autuacao_consultar':
      $strTitulo = 'Consultar Autua��o';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_rel_proced_tipo_materia'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';


      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrObjParteProcedimentoDTO=$objAutuacaoDTO->getArrObjParteProcedimentoDTO();
  $arrPartes=array();
  
  foreach ($arrObjParteProcedimentoDTO as $objParteProcedimentoDTO) {
    /** @var ParteProcedimentoDTO $objParteProcedimentoDTO */
    $arrPartes[]=array(
        $objParteProcedimentoDTO->getNumIdContato(),
        $objParteProcedimentoDTO->getStrNomeContato(),
        $objParteProcedimentoDTO->getNumIdQualificacaoParte(),
        $objParteProcedimentoDTO->getStrDescricaoQualificacaoParte()
    );
  }

  $arrObjRelAutuacaoTipoMateriaDTO=$objAutuacaoDTO->getArrObjRelAutuacaoTipoMateriaDTO();
  InfraArray::ordenarArrInfraDTO($arrObjRelAutuacaoTipoMateriaDTO,'Ordem',InfraDTO::$TIPO_ORDENACAO_DESC);
  $arrMaterias=array();

  $idTipoMateriaPrincipal=$objAutuacaoDTO->getNumIdTipoMateria();
  foreach ($arrObjRelAutuacaoTipoMateriaDTO as $objRelAutuacaoTipoMateriaDTO) {
    $arrMaterias[]=array(
      $objRelAutuacaoTipoMateriaDTO->getNumIdTipoMateria(),
      $objRelAutuacaoTipoMateriaDTO->getStrDescricaoTipoMateria()
    );

  }

  $strMaterias = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrMaterias);
  $strPartes = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrPartes);

  $strItensSelTipoMateria=TipoMateriaINT::montarSelectDescricao('null','&nbsp;',null);
  $strItensSelQualificacaoParte=QualificacaoParteINT::montarSelectDescricao('null','&nbsp;',null);

  $strLinkAjaxContatos = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=contato_auto_completar_contexto_RI1225');
  $strLinkAjaxCadastroAutomatico = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=contato_cadastro_contexto_temporario');


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
#lblTipoMateria {position:absolute;left:0;top:0;width:75%;}
#selTipoMateria {position:absolute;left:0;top:40%;width:75%;}

#lblDescricao {position:absolute;left:0;top:0;width:75%;}
#txaDescricao {position:absolute;left:0;top:12%;width:75%;}

#lblParte {position:absolute;left:0;top:0%;width:25%;}
#txtParte {position:absolute;left:0;top:40%;width:50%;}

#lblQualificacaoParte {position:absolute;left:51%;top:0%;width:25%;}
#selQualificacaoParte {position:absolute;left:51%;top:40%;width:25%;}


  #btnAdicionarParte {position:absolute;left:77%;top:40%;}
  #btnAdicionarMateria {position:absolute;left:77%;top:40%;}
<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

  var objTabelaPartesInteressadas,objAutoCompletarParteInteressada;
  var objTabelaMaterias;

function inicializar(){

  objTabelaPartesInteressadas = new infraTabelaDinamica('tblPartes','hdnPartes', <?=$bolGerenciar?>, <?=$bolGerenciar?>);
  objTabelaPartesInteressadas.inserirNoInicio = false;
  infraEfeitoTabelas();
  var config={
    gerarEfeitoTabela:true,
    flagRemover:<?=$bolGerenciar?>,
    flagOrdenar:<?=$bolGerenciar?>,
    nomeRadio:'infraRadioPrincipal',
    idRadioSelecionado:'<?=$idTipoMateriaPrincipal?>'
  };
  objTabelaMaterias = new infraTabelaDinamica2('tblMaterias','hdnMaterias', config);
  objTabelaMaterias.inserirNoInicio = false;


<?if($bolGerenciar){?>
    document.getElementById('btnCancelar').focus();

  objTabelaPartesInteressadas.alterar = function(arr){
    objAutoCompletarParteInteressada.selecionar(arr[0],arr[1]);
    infraSelectSelecionarItem('selQualificacaoParte',arr[2]);
  };
  objTabelaPartesInteressadas.remover = function(arr){
    return true;
  };
  objTabelaPartesInteressadas.gerarEfeitoTabela=true;

  //tipo materia

  objTabelaMaterias.remover = function(arr){
    return true;
  };
  objTabelaMaterias.gerarEfeitoTabela=true;

  //Parte Interessada
  objAutoCompletarParteInteressada = new infraAjaxAutoCompletar('hdnIdParte','txtParte','<?=$strLinkAjaxContatos?>');
  objAutoCompletarParteInteressada.limparCampo = true;
  objAutoCompletarParteInteressada.prepararExecucao = function(){
    return 'palavras_pesquisa='+encodeURIComponent(document.getElementById('txtParte').value);
  };
  objAutoCompletarParteInteressada.processarResultado = function(id,descricao,complemento){
    if (id!=''){
      document.getElementById('hdnIdParte').value = id;
      document.getElementById('txtParte').value = descricao;
    }
  };
//  infraAdicionarEvento(document.getElementById('txtParte'),'keyup',tratarEnterParteInteressada);
  <?} else {?>
  infraDesabilitarCamposAreaDados();
  <?}?>




}

function validarCadastro() {




  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}
  function adicionarParte(){

    if (infraTrim(document.getElementById('txtParte').value)=='') {
      alert('Nome da Parte n�o informado.');
      document.getElementById('txtParte').focus();
      return false;
    }


    if (!infraSelectSelecionado('selQualificacaoParte')) {
      alert('Selecione a Qualificacao da Parte');
      document.getElementById('selQualificacaoParte').focus();
      return false;
    }

    var idContato = document.getElementById('hdnIdParte').value;
    var nomeContato = document.getElementById('txtParte').value;
    var idQualificacaoParte = null;
    var strQualificacaoParte = null;

    for(var i=0;i < document.getElementById('selQualificacaoParte').options.length;i++){
      if (document.getElementById('selQualificacaoParte').options[i].selected){
        idQualificacaoParte = document.getElementById('selQualificacaoParte').options[i].value;
        strQualificacaoParte = document.getElementById('selQualificacaoParte').options[i].text;
      }
    }

    objTabelaPartesInteressadas.adicionar([idContato, nomeContato, idQualificacaoParte, strQualificacaoParte]);

    //depois de incluir limpa os input
    document.getElementById('txtParte').value = '';
    document.getElementById('hdnIdParte').value = '';
    document.getElementById('selQualificacaoParte').options[0].selected = true;
    document.getElementById('txtParte').focus();
  }

  function adicionarMateria(){
    if (!infraSelectSelecionado('selTipoMateria')) {
      alert('Selecione o tipo de mat�ria');
      document.getElementById('selTipoMateria').focus();
      return false;
    }

    var idTipoMateria = null;
    var strTipoMateria = null;
    var objSelect=document.getElementById('selTipoMateria');

    for(var i=0;i < objSelect.options.length;i++){
      if (objSelect.options[i].selected){
        idTipoMateria = objSelect.options[i].value;
        strTipoMateria = objSelect.options[i].text;
      }
    }

    objTabelaMaterias.adicionar([idTipoMateria, strTipoMateria]);
    var objRadio=$('infraRadioPrincipal');

    if(objRadio.length===1){
      objRadio.click();
    }

    //depois de incluir limpa os input
    document.getElementById('selTipoMateria').options[0].selected = true;
  }
<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmRelProcedAutuacao" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].'&id_procedimento='.$dblIdProcedimento)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();

PaginaSEI::getInstance()->abrirAreaDados('15em');
?>
  <label id="lblDescricao" for="txaDescricao" class="infraLabelOpcional">Descri��o:</label>
  <textarea id="txaDescricao" name="txaDescricao" class="infraTextarea" rows="7" onkeypress="return infraMascaraTexto(this,event,2000);" maxlength="2000" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?= $objAutuacaoDTO->getStrDescricao(); ?></textarea>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  if($bolGerenciar){
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblTipoMateria" for="selTipoMateria" accesskey="t" class="infraLabelOpcional"><span class="infraTeclaAtalho">T</span>ipo de Mat�ria:</label>
  <select id="selTipoMateria" name="selTipoMateria" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelTipoMateria ?>
  </select>
  <input type="button" id="btnAdicionarMateria" name="btnAdicionarMateria" class="infraButton" value="Adicionar" onclick="adicionarMateria();" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  }
  ?>
  <div id="divTabelaTipoMateria" class="infraAreaTabela">

    <table id="tblMaterias" width="99%" class="infraTable">
      <caption class="infraCaption"><?= PaginaSEI::getInstance()->gerarCaptionTabela('Tipos de Mat�ria', 0) ?></caption>
      <tr>
        <th class="infraTh" style="width:5%;">Principal</th>
        <th style="display:none;">ID Tipo Materia</th>
        <th class="infraTh" width="80%" align="left">Tipo de Mat�ria</th>
        <? if ($bolGerenciar) { ?>
          <th class="infraTh">A��es</th>
        <? } ?>
      </tr>
    </table>

    <input type="hidden" id="hdnMaterias" name="hdnMaterias" value="<?= $strMaterias; ?>"/>
  </div>
  <br/> <br/>
  <?
  if ($bolGerenciar){
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>
  <label id="lblParte" for="txtParte" accesskey="" class="infraLabelOpcional">Parte:</label>
  <input type="text" id="txtParte" name="txtParte" class="infraText" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>
  <input type="hidden" id="hdnIdParte" name="hdnIdParte" value=""/>

  <label id="lblQualificacaoParte" for="selQualificacaoParte" accesskey="" class="infraLabelOpcional">Qualifica��o:</label>
  <select id="selQualificacaoParte" name="selQualificacaoParte" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelQualificacaoParte ?>
  </select>

  <input type="button" id="btnAdicionarParte" name="btnAdicionarParte" class="infraButton" value="Adicionar" onclick="adicionarParte();" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  }
  ?>
  <div id="divTabelaPartes" class="infraAreaTabela" >

    <table  id="tblPartes" width="99%" class="infraTable">
      <caption class="infraCaption"><?=PaginaSEI::getInstance()->gerarCaptionTabela('Partes',0)?></caption>
      <tr>
        <th style="display:none;">ID Contato</th>
        <th class="infraTh" width="55%" align="left">Parte</th>
        <th style="display:none;">ID Qualificacao Parte</th>
        <th class="infraTh" width="35%" align="center">Qualifica��o</th>
        <? if($bolGerenciar) { ?>
        <th class="infraTh">A��es</th>
        <? } ?>
      </tr>
    </table>

    <input type="hidden" id="hdnPartes" name="hdnPartes" value="<?=$strPartes;?>"/>
  </div>
  <?
?>
<?
PaginaSEI::getInstance()->fecharAreaDados();
?>
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>