<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
  $objDistribuicaoRN = new DistribuicaoRN();

  $strTitulo = 'Sess�o de Julgamento';

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_distribuicao','id_sessao_julgamento','id_procedimento_sessao','arvore','id_colegiado'));

  $strParametros='';

  if (isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }
  if (isset($_POST['selSessao'])){
    $idSessaoJulgamento=$_POST['selSessao'];
    $_GET['id_sessao_julgamento']=$idSessaoJulgamento;
  } else {
    $idSessaoJulgamento=$_GET['id_sessao_julgamento'];
  }

  $idColegiado = $_GET['id_colegiado'] ?? null;

  $arrComandos = array();
  $arrComandosSessao=array();

  $idUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
  $bolAdmin = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar');

  $chkSinExibirIframe = 'S';
  if (isset($_GET['id_procedimento_sessao']) || PaginaSEI::getInstance()->isBolIpad() || PaginaSEI::getInstance()->isBolIphone() || PaginaSEI::getInstance()->isBolAndroid()) {
    $chkSinExibirIframe = 'N';
  }

  switch($_GET['acao']) {

    case 'presenca_sessao_registrar':
      $idSessao = $_GET['id_sessao_julgamento'];
      $idMembro = $_GET['id_usuario'];
      $sinPresente = $_GET['presente'];
      $bolMultiplo = $_GET['multiplo'] == 1;
      if ($idSessao == '') {
        throw new InfraException('Sess�o de Julgamento n�o informada.');
      }
      if ($sinPresente == '') {
        throw new InfraException('N�o foi informado se membro est� presente.');
      }
      if ($idMembro == '' && !$bolMultiplo) {
        throw new InfraException('Membro do Colegiado n�o informado.');
      }

      $objPresencaSessaoDTO = new PresencaSessaoDTO();
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($idSessao);
      $objPresencaSessaoDTO->setStrSinPresente($sinPresente);


      try {
        $objPresencaSessaoRN = new PresencaSessaoRN();
        if ($bolMultiplo) {
          $idMembro = PaginaSEI::getInstance()->getArrStrItensSelecionados('membros');
          $objPresencaSessaoDTO->setArrNumIdUsuario($idMembro);
        } else {
          $objPresencaSessaoDTO->setNumIdUsuario($idMembro);
        }
        $objPresencaSessaoRN->definirPresenca($objPresencaSessaoDTO);

        PaginaSEI::getInstance()->adicionarMensagem('Presen�a alterada com sucesso.');
        //header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem=sessao_julgamento_listar&id_sessao_julgamento='.$_GET['id_sessao_julgamento']));
        //die;
      } catch (Exception $e) {
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      //break;

      header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao_origem'] . '&acao_origem=' . $_GET['acao'] .PaginaSEI::getInstance()->montarAncora($idMembro)));
      die;

    case 'sessao_julgamento_cancelar':
      $idSessao = $_GET['id_sessao_julgamento'];
      if ($idSessao == '') {
        throw new InfraException('Sess�o de Julgamento n�o informada.');
      }
      try {

        $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
        $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
        $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessao);
        $objSessaoJulgamentoRN->cancelar($objSessaoJulgamentoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Sess�o cancelada com sucesso.');

        header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_listar' . PaginaSEI::getInstance()->montarAncora($idSessao)));
        die;

      } catch (Exception $e) {
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao_origem'] . '&acao_origem=' . $_GET['acao']));
      die;

    case 'sessao_julgamento_encerrar':
    case 'sessao_julgamento_finalizar':
      $idSessao = $_GET['id_sessao_julgamento'];
      $strTitulo = 'Encerramento de Sess�o de Julgamento';
      if ($_GET['acao'] === 'sessao_julgamento_finalizar') {
        $strTitulo = 'Finaliza��o de Sess�o de Julgamento';
      }
      if ($idSessao == '') {
        throw new InfraException('Sess�o de Julgamento n�o informada.');
      }
      if ($_GET['progresso'] === 'S') {
        PaginaSEI::getInstance()->prepararBarraProgresso2($strTitulo);
      }
      try {

        $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
        $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
        $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessao);
        if ($_GET['acao'] === 'sessao_julgamento_finalizar') {
          $objSessaoJulgamentoRN->finalizar($objSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Sess�o finalizada com sucesso.');
        } else {
          $objSessaoJulgamentoRN->encerrar($objSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Sess�o encerrada com sucesso.');
        }

        if ($_GET['progresso'] === 'S') {
          PaginaSEI::getInstance()->finalizarBarraProgresso2(SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao_origem'] . '&acao_origem=' . $_GET['acao'] . $strParametros));

        } else {
          header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao_origem'] . '&acao_origem=' . $_GET['acao'] . $strParametros));
        }
        die;
      } catch (Exception $e) {
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      if ($_GET['progresso'] === 'S') {
        PaginaSEI::getInstance()->finalizarBarraProgresso2(SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao_origem'] . '&acao_origem=' . $_GET['acao'] ));

      } else {
        header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao_origem'] . '&acao_origem=' . $_GET['acao'] ));
      }
      die;

    case 'sessao_julgamento_acompanhar': //vem do controle de processos, bloqueia a sele��o de sess�o e colegiado
      $chkSinExibirIframe='N';
    break;

    case 'sessao_julgamento_consultar': //vem da lista de sess�es da COJAD
      break;

    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }


  $strComandoFechar='';
  if (PaginaSEI::getInstance()->getAcaoRetorno()==='sessao_julgamento_listar'){
    $strComandoFechar = '<button type="button" accesskey="" id="btnVoltar" name="btnVoltar" value="Voltar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&montar_visualizacao=1'.$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_sessao_julgamento'])).'\';" class="infraButton">Voltar</button>';
  }


  if ($idSessaoJulgamento==='null') {
    $idSessaoJulgamento = null;
  }

  if ($idSessaoJulgamento!=null) {

    $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
    $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
    $objPesquisaSessaoJulgamentoDTO->setStrSinAnotacao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinDestaque('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinVotos('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinEleicoes('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinPedidoVista('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinPresenca('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinDocumentos('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinRevisao('S');

    /** @var SessaoJulgamentoDTO $objSessaoJulgamentoDTO */
    $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);


    if ($objSessaoJulgamentoDTO == null) {
      throw new InfraException('Sess�o de Julgamento n�o encontrada.');
    }
    $bolSessaoVirtual=($objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S');

    if ($bolAdmin && $idUnidadeAtual!=$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()) {
      $bolAdmin=false;
    }
    $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(),'IdUsuario');
    $arrObjItemSessaoJulgamentoDTO = $objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
    $numTotalItens=InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
    $arrObjSituacaoSessaoDTO = $objSessaoJulgamentoRN->listarValoresSituacaoSessao();

    $arrObjSituacaoSessaoDTO = InfraArray::indexarArrInfraDTO($arrObjSituacaoSessaoDTO, 'StaSituacao');

    $idColegiado=$objSessaoJulgamentoDTO->getNumIdColegiado();
    $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
    $strSituacaoSessao = $arrObjSituacaoSessaoDTO[$staSessao]->getStrDescricao();
    $bolQuorumMinimoAtingido=$objSessaoJulgamentoDTO->getNumQuorum()>=$objSessaoJulgamentoDTO->getNumQuorumMinimoColegiado();

    //se n�o � adm do colegiado verifica se faz parte do colegiado
    if($idUnidadeAtual!=$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()){
      $objColegiadoRN=new ColegiadoRN();
      $arrColegiadoUnidadeAtual=$objColegiadoRN->listarColegiadosUnidadeAtual();
      if(!in_array($idColegiado,$arrColegiadoUnidadeAtual)){
        $siglaUnidade=SessaoSEI::getInstance()->getStrSiglaUnidadeAtual();
        $siglaColegiado=$objSessaoJulgamentoDTO->getStrSiglaColegiado();
        PaginaSEI::getInstance()->adicionarMensagem("Unidade atual $siglaUnidade n�o pertence ao colegiado $siglaColegiado.",PaginaSEI::$TIPO_MSG_AVISO);
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=principal&acao_origem='.$_GET['acao']));
        die;
      }
    }

    $btnEncerrar = '<button type="button" accesskey="" id="btnEncerrar" name="btnEncerrar" value="Encerrar" onclick="encerrarSessao();" class="infraButton">Encerrar Sess�o</button>';
    $btnFinalizar = '<button type="button" accesskey="" id="btnFinalizar" name="btnFinalizar" value="Finalizar" onclick="finalizarSessao();" class="infraButton">Finalizar Sess�o</button>';
    $btnCancelarSessao='<button type="button" accesskey="" id="btnCancelar" name="btnCancelar" value="Cancelar" onclick="cancelarSessao();" class="infraButton">Cancelar Sess�o</button>';

    $btnSuspender   = '<button type="button" accesskey="" id="btnSuspender" name="btnSuspender" value="Suspender" onclick="location.href=\''.SessaoJulgamentoINT::montarLink('sessao_julgamento_alterar',$_GET['acao'],null,'&sta_situacao='.SessaoJulgamentoRN::$ES_SUSPENSA).'\';" class="infraButton">Suspender Sess�o</button>';
    $btnFecharPauta = '<button type="button" accesskey="" id="btnFecharPauta" name="btnFecharPauta" value="Fechar Pauta" onclick="location.href=\''.SessaoJulgamentoINT::montarLink('sessao_julgamento_alterar',$_GET['acao'],null,'&sta_situacao='.SessaoJulgamentoRN::$ES_PAUTA_FECHADA).'\';" class="infraButton">Fechar Pauta</button>';
    $btnAbrirSessao = '<button type="button" accesskey="" id="btnAbrir" name="btnAbrir" value="Abrir Sess�o" onclick="location.href=\''.SessaoJulgamentoINT::montarLink('sessao_julgamento_alterar',$_GET['acao'],null,'&sta_situacao='.SessaoJulgamentoRN::$ES_ABERTA).'\';" class="infraButton">Abrir Sess�o</button>';
    $btnAbrirPauta  = '<button type="button" accesskey="" id="btnAbrirPauta" name="btnAbrirPauta" value="Abrir Pauta" onclick="location.href=\''.SessaoJulgamentoINT::montarLink('sessao_julgamento_alterar',$_GET['acao'],null,'&sta_situacao='.SessaoJulgamentoRN::$ES_PAUTA_ABERTA).'\';" class="infraButton">Abrir Pauta</button>';
    $btnReabrirPauta  = '<button type="button" accesskey="" id="btnAbrirPauta" name="btnAbrirPauta" value="Abrir Pauta" onclick="location.href=\''.SessaoJulgamentoINT::montarLink('sessao_julgamento_alterar',$_GET['acao'],null,'&sta_situacao='.SessaoJulgamentoRN::$ES_PAUTA_ABERTA).'\';" class="infraButton">Reabrir Pauta</button>';
    $btnReabrirSessao='<button type="button" accesskey="" id="btnAbrir" name="btnAbrir" value="Reabrir Sess�o" onclick="location.href=\''.SessaoJulgamentoINT::montarLink('sessao_julgamento_alterar',$_GET['acao'],null,'&sta_situacao='.SessaoJulgamentoRN::$ES_ABERTA).'\';" class="infraButton">Reabrir Sess�o</button>';

    $btnRegistrarPresenca='<button type="button" accesskey="" id="btnRegistrarPresenca" name="btnRegistrarPresenca" value="Registrar Presenca" onclick="acaoMultiplaMembros(\'' . SessaoJulgamentoINT::montarLink('presenca_sessao_registrar' , PaginaSEI::getInstance()->getAcaoRetorno(),null, '&presente=S&multiplo=1') . '\');" class="infraButton">Registrar Presen�a</button>';
    $btnRetirarProcessos = '<button type="button" accesskey="" id="btnRetirarProcessos" name="btnRetirarProcessos" value="Retirar Processos" onclick="acaoMultipla(\'' . SessaoJulgamentoINT::montarLink('item_sessao_julgamento_retirar',PaginaSEI::getInstance()->getAcaoRetorno(),null, '&multiplo=1') . '\');" class="infraButton">Retirar Processos</button>';


    $bolMostraPresenca = false;
    $bolExibeProcesso=($objSessaoJulgamentoDTO->getDblIdProcedimento()!=null);
    if ($bolExibeProcesso) {
      $strLinkProcesso = '<a id="ancProcesso" class="my-xl-2 pt-1 pt-xl-0 pb-xl-1" href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objSessaoJulgamentoDTO->getDblIdProcedimento()) . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" title="' . $objSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '">' . $objSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a>';
    }
    switch ($staSessao) {
      case SessaoJulgamentoRN::$ES_PREVISTA:
        $arrComandosSessao[] = $btnAbrirPauta;
        break;

      case SessaoJulgamentoRN::$ES_ABERTA:
        $arrComandosSessao[] = $btnEncerrar;
        $arrComandosSessao[] = $btnSuspender;
        $strStyle = 'color:#000;background:#c6efce;';
        $bolMostraPresenca = true;
        if($bolAdmin){
          $arrComandos[] = $btnRetirarProcessos;
          $arrComandos[] = $btnRegistrarPresenca;
        }

        break;
      case SessaoJulgamentoRN::$ES_ENCERRADA:
        $arrComandosSessao[] = $btnFinalizar;
        $strStyle = 'color:#000;background:#9fd8f5;';
        $bolMostraPresenca = true;
        break;
      case SessaoJulgamentoRN::$ES_FINALIZADA:
        $strStyle = 'color:#000;background:#c6efce;';
        $bolMostraPresenca = true;

        break;
      case SessaoJulgamentoRN::$ES_PAUTA_ABERTA:
        $arrComandosSessao[] = $btnFecharPauta;
        $arrComandosSessao[] = $btnCancelarSessao;
        if ($bolAdmin) {
          $arrComandos[] = $btnRetirarProcessos;
        }
        $strStyle = 'color:#000;background:#ffec9c;';

        break;
      case SessaoJulgamentoRN::$ES_PAUTA_FECHADA:
        if ($bolQuorumMinimoAtingido){
          $strStyle = 'color:#000;background:#c6efce;';
          $arrComandosSessao[] = $btnAbrirSessao;
        } else {
          $strStyle = 'color:#000;background:#ffc7ce;';
        }
        $arrComandosSessao[] = $btnReabrirPauta;
        $arrComandosSessao[] = $btnCancelarSessao;

        if($bolAdmin){
          $arrComandos[] = $btnRegistrarPresenca;
        }

        $bolMostraPresenca = true;


        break;
      case SessaoJulgamentoRN::$ES_SUSPENSA:
        $strStyle = 'color:#000;background:#ffc7ce;';
        $bolMostraPresenca = true;
        if($bolAdmin){
          $arrComandos[] = $btnRegistrarPresenca;
        }
        if ($bolQuorumMinimoAtingido){
          $arrComandosSessao[] = $btnReabrirSessao;
        }
        break;

      default:
        $strStyle = '';
    }

    //----------------------------- montar tabelas de membros

    $strPresentes='';

    $numRegistrosMembros=0;
    foreach ($arrObjColegiadoComposicaoDTO as $id =>$objColegiadoComposicaoDTO) {
      if (!isset($arrObjPresencaSessaoDTO[$id]) && $objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR &&
          $objColegiadoComposicaoDTO->getStrSinHabilitado()=='N') {
        continue;
      }
      ++$numRegistrosMembros;
    }
    $bolExibirCheckboxMembros=SessaoJulgamentoRN::permiteDefinirPresencas($objSessaoJulgamentoDTO);
    if ($numRegistrosMembros > 0) {
      $strResultadoMembros = SessaoJulgamentoINT::montarTabelaComposicao($objSessaoJulgamentoDTO, $strPresentes);
    }

    $bolPermiteSelecaoItens=SessaoJulgamentoRN::permiteSelecaoItens($objSessaoJulgamentoDTO);
    //----------------------------- montar tabelas de itens
    $strCssAcoes='';

    $arrObjTabelaHtmlDTO=SessaoJulgamentoINT::montarTabelasItens($objSessaoJulgamentoDTO, $chkSinExibirIframe=='S', $strCssAcoes);
    $arrObjTabelaHtmlDTO=InfraArray::indexarArrInfraDTO($arrObjTabelaHtmlDTO,'IdSessaoBloco',true);
    $arrObjSessaoBlocoDTO=CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($idSessaoJulgamento);

    //----------------------------- fim das tabelas




    if($numTotalItens>3){
      $strLinkEncerrarSessao=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_encerrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&progresso=S');
      $strLinkFinalizarSessao=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_finalizar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&progresso=S');
    } else {
      $strLinkEncerrarSessao = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_encerrar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao']);
      $strLinkFinalizarSessao = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_finalizar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao']);
    }
    $strLinkVotar=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=item_sessao_julgamento_julgar_multiplo&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].$strParametros);

    $bolCheckItensNaoVotados=false;
    if ($bolAdmin) {

      if ($staSessao===SessaoJulgamentoRN::$ES_ABERTA){
        array_unshift($arrComandos, '<button type="button" accesskey="" id="btnVotarSel" name="btnVotarSel" value="Votar" onclick="votarMultiplo(false);" class="infraButton">Votar Selecionados</button>');
        array_unshift($arrComandos, '<button type="button" accesskey="" id="btnVotarRem" name="btnVotarRem" value="Votar" onclick="votarMultiplo(true);" class="infraButton">Votar Remanescentes</button>');
        $bolCheckItensNaoVotados=true;
      }

      $arrComandos=array_merge($arrComandos, $arrComandosSessao);
    }
    $arrComandos[] = '<button type="button" accesskey="" id="btnHistorico" name="btnHistorico" value="Historico" onclick="abrirDocumento(\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_historico&acao_origem=' . $_GET['acao'] . $strParametros) . '\');" class="infraButton">Hist�rico</button>';
    if ($staSessao !== SessaoJulgamentoRN::$ES_FINALIZADA && $staSessao !== SessaoJulgamentoRN::$ES_PREVISTA && $staSessao !== SessaoJulgamentoRN::$ES_CANCELADA) {
      $arrComandos[]= '<button type="submit" accesskey="" id="btnAtualizar" name="btnAtualizar" value="Atualizar" class="infraButton">Atualizar</button>';
    }
    if($staSessao !== SessaoJulgamentoRN::$ES_PREVISTA && SessaoSEI::getInstance()->verificarPermissao('resumo_pauta')){
      $arrComandos[]= '<button type="button" accesskey="" id="btnResumo" name="btnResumo" value="Resumo" onclick="verResumo(\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=resumo_pauta').'\');" class="infraButton">Resumo</button>';
    }
  }

  $arrComandos[]=$strComandoFechar;
  $arrParametros=CacheSessaoJulgamentoRN::getArrParametrosModulo();

  $strItensSelColegiado=ColegiadoINT::montarSelectNomePermitidos(null,'',$idColegiado);
  $strLinkAjaxSessao=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=sessao_julgamento');
  $strLinkCancelarSessao=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_cancelar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']);
  $strLinkAction=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros);

  $strLinkAjaxDispositivo=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=dispositivo_item');
  $strLinkAjaxEventos=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=eventos_sessao');//id_sessao vai pelo setArrParametrosRepasseLink
  $strLinkAjaxDadosSessao=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=dados_sessao&sin_exibir_iframe='.$chkSinExibirIframe.'&acao_origem='.$_GET['acao']);



}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
echo $strCssAcoes;
?>
    .sombreado {
        background-color:white;
        height: 100%;
        padding:10px;
        border-radius: 5px;
        box-shadow: 0 0.125rem 0.5rem rgba(0, 0, 0, .3), 0 0.0625rem 0.125rem rgba(0, 0, 0, .2);
    <? if (PaginaSEI::getInstance()->isBolNavegadorSafariIpad()){?>
        overflow: scroll !important;
        -webkit-overflow-scrolling:touch;
    <? } else {?>
        overflow:hidden;
        <?}?>
    }
    .cinza {background-color: #e0e0e0;padding: 1rem;}
    #ifrDocumento{height: 100%;width: 100%}


    #divHtml {height: 100%}

    .divDestaque, .divEleicao {display: inline}
  #divChkDestaque {padding-top:3px}
  #lblDestaque {margin-top: 3px;}
  #fldFiltros {position: relative; height:6rem;width: 100%;border:1px solid #aaa}

  .protocoloNormal {margin-left: .2em;padding: .2em 10px;vertical-align: middle}
  .processoVisualizadoSigiloso {margin-left: .2em;vertical-align: middle}

#divSinExibirIframe {position: absolute;left:38%;top:20%;width:30%;}

#lblColegiado {}
#selColegiado {width:100%;}

#lblSessao {}
#selSessao {width:100%;margin-bottom: 0.2rem}

#lblQuorum {}
#txtQuorum {width:100%;}

#lblProcesso  {}
/*noinspection CssUnusedSymbol*/
#ancProcesso {display:block;padding-top: 0.4rem;padding-bottom: 0.1rem}


#lblStatus {left:71%;top:0;}
#lblStatus2 {display: block;text-align:center;font-size:20px;line-height:5rem;border: 1px solid #AAA;}

#lblTabelaColegiado {position:absolute;left:0;top:0;}


.tblMembros,
.tblItens {
    border: 2px solid #666!important;
    border-radius: .25rem!important;
    border-collapse: unset!important;
    border-spacing: 0!important;
}

th.infraTh{
  padding:0;
  line-height: unset;
  border:0;
  font-size:.75rem;
  background-color: #ccc !important;
  color: black  !important;
}

/*largura das colunas das tabelas*/
    .tdCheck {width: 2%}
    .tdProcesso {}
    .tdOrdem {font-size:1rem!important; font-weight:bold}
    .tdDocumento {width: 16%}
    .tdAcoes {width: 15%}
    .tdDestaque {width: 15%;}
    .tdSituacao {width: 17%;}

.infraNotificacao {
  border-radius: 1px;
}

    a.processoVisualizadoSigiloso:hover{
        color:white;
    }
/*noinspection CssUnusedSymbol*/
.presente{ background-color: #c6efce;}
/*noinspection CssUnusedSymbol*/
.ausente { background-color: #ffc7ce;}
.oculta {display:none!important;}

.resumo {float:right;color:white;font-size:0.9em;padding-right: 3px;position: relative;top:2px;}
#divConteudo {display:none;margin-top:.3rem;height:22em;width:100%;border-top:1px solid #ccc;padding-top:3px;}
#divAreaPauta {float:left;width:40%;height:99%;overflow:auto;}
#divAreaDocumento {float:left;width:60%;height:99%;}

/*noinspection CssUnknownTarget*/
#divRedimensionar {float:left;display:inline;top:0;height:99%;overflow:auto;cursor:w-resize;width:5px;
  background: white url(imagens/barra_redimensionamento.gif) repeat-y center;
}
#divArvore{height:100%;width:100%;position:absolute;margin-left:0;left:0;top:0;display:none;background-color:yellow;opacity:0.0;filter:alpha(opacity=0);}

  /*noinspection CssUnusedSymbol*/
a.espacamento{padding: 0 0.1em;}
  .flashit{
    -webkit-animation: flash linear 2s infinite;
    animation: flash linear 2s infinite;
  }
  @-webkit-keyframes flash {
    0% { opacity: 1; }
    50% { opacity: .1; }
    100% { opacity: 1; }
  }
  @keyframes flash {
    0% { opacity: 1; }
    50% { opacity: .1; }
    100% { opacity: 1; }
  }
<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
  var objAjaxSessao;
  var cookie='<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>';
  var isdrag=false;
  var x;
  var dobj;
  var divLeftTamanhoInicial = null;
  var divRightTamanhoInicial = null;
  //-------------- processamento div - redimensionamento
  function redimensionarMenu() {
    var wGlobal = document.getElementById('divInfraAreaGlobal').offsetWidth;
    var wMenu = document.getElementById('divInfraAreaTelaE').offsetWidth;
    var wLivre = wGlobal - wMenu;
    if (wLivre > 0) {
      var tamIfrDocumento=58;
      var tamAreaPauta=infraLerCookie(cookie +'_tamAreaPauta');
      if(tamAreaPauta===null){
        tamAreaPauta=40;
      } else {
        tamIfrDocumento=98-tamAreaPauta;
      }
      document.getElementById("divAreaPauta").style.width = Math.floor(tamAreaPauta/100 * wLivre) + 'px';
      document.getElementById("ifrDocumento").style.width = Math.floor(tamIfrDocumento/100 * wLivre) + 'px';
    }
    redimensionar();
  }
  function redimensionar(){
    var tamIfrDocumento=58;
    var tamAreaPauta=infraLerCookie(cookie +'_tamAreaPauta');
    if(tamAreaPauta===null){
      tamAreaPauta=40;
    } else {
      tamIfrDocumento=99-tamAreaPauta;
    }

    if (INFRA_IOS || INFRA_SAFARI){
      tamAreaPauta = 49;
      tamIfrDocumento = 50;

      if (document.getElementById("divAreaPauta").offsetHeight < 100){
        document.getElementById("divAreaPauta").style.height = '100px';
      }

      if (document.getElementById("divRedimensionar").offsetHeight < 100){
        document.getElementById("divRedimensionar").style.height ='100px';
      }
    }

    var hTela = infraClientHeight();

    if (hTela > 0){

      var hDivGlobal = document.getElementById('divInfraAreaGlobal').scrollHeight;
      var PosYConteudo = infraOffsetTopTotal(document.getElementById('divConteudo'));

      var hRedimensionamento = 0;

      if (hTela > hDivGlobal){
        hRedimensionamento = hTela - PosYConteudo;
      }else{
        hRedimensionamento = hDivGlobal - PosYConteudo;
      }

      if (hRedimensionamento > 0 && hRedimensionamento < 1920){ //FullHD

        hRedimensionamento = hRedimensionamento - 10;



        if (!INFRA_IOS){
          document.getElementById("divAreaPauta").style.width = tamAreaPauta+'%';
          document.getElementById("divAreaDocumento").style.width = tamIfrDocumento+'%';
        }else{
          var wConteudo = document.getElementById('divConteudo').offsetWidth;
          document.getElementById("divAreaPauta").style.width = Math.floor(tamAreaPauta/100*wConteudo) + 'px';
          document.getElementById("divAreaDocumento").style.width = Math.floor(tamIfrDocumento/100*wConteudo) + 'px';
        }

        document.getElementById('divConteudo').style.height = hRedimensionamento + 'px';
        document.getElementById("divAreaPauta").style.height = hRedimensionamento + 'px';
        document.getElementById('divRedimensionar').style.height = hRedimensionamento + 'px';
        document.getElementById("divAreaDocumento").style.height = hRedimensionamento + 'px';


      }
    }
  }
  function movemouse(e) {

    if (e.button <= 1 && isdrag){

      var tamanhoRedimensionamento = tx + e.clientX - x;

      var tamanhoLeft = 0;
      var tamanhoRight = 0;

      if (tamanhoRedimensionamento > 0){
        tamanhoLeft = (divLeftTamanhoInicial + tamanhoRedimensionamento);
        tamanhoRight = (divRightTamanhoInicial - tamanhoRedimensionamento);
      }else{
        tamanhoLeft = (divLeftTamanhoInicial - Math.abs(tamanhoRedimensionamento));
        tamanhoRight = (divRightTamanhoInicial + Math.abs(tamanhoRedimensionamento));
      }

      if (tamanhoLeft < 0 || tamanhoRight < 0){
        if (tamanhoRedimensionamento > 0){
          tamanhoLeft = 0;
          tamanhoRight = (divLeftTamanhoInicial - divRightTamanhoInicial) ;
        }else{
          tamanhoLeft = (divLeftTamanhoInicial - divRightTamanhoInicial);
          tamanhoRight = 0;
        }
      }

      if(tamanhoLeft > 50 && tamanhoRight > 100){
        var wConteudo=$("#divConteudo").width();
        var tamAreaPauta=Math.floor(tamanhoLeft/wConteudo*100);
        infraCriarCookie(cookie+'_tamAreaPauta',tamAreaPauta,356);
        document.getElementById("divAreaPauta").style.width = tamanhoLeft + 'px';
        document.getElementById("divAreaDocumento").style.width = wConteudo*0.99-tamanhoLeft + 'px';
      }
    }
    return false;
  }
  function selectmouse(e){

    document.getElementById("divArvore").style.display = 'block';

    var fobj       =  e.target;
    var topelement =  "HTML";
    while (fobj.tagName != topelement && fobj.className != "dragme") {
      fobj = fobj.parentNode;
    }

    if (fobj.className=="dragme") {
      isdrag = true;
      dobj = fobj;
      tx = parseInt(dobj.style.left+0);
      x = e.clientX;
      divLeftTamanhoInicial = document.getElementById("divAreaPauta").offsetWidth;
      divRightTamanhoInicial = document.getElementById("divAreaDocumento").offsetWidth;

      if (!INFRA_IOS){
        document.onmousemove=movemouse;
      } else {
        document.ontouchmove=movemouse;
      }
      return false;
    }

  }
  function dropmouse(){
    isdrag=false;
    var da=document.getElementById("divArvore");
    if(da) {
      da.style.display = 'none';
    }
  }
//-------------- fim processamento div - redimensionamento

  function abrirDocumento(link,el) {
    var bolLinkDocumento=(link.indexOf('acao=documento_visualizar')!== -1);
    var ifr = document.getElementById('ifrDocumento');
    if (ifr!=null){ //tela de sess�o completa
      if (bolLinkDocumento) {
        $('#divAreaDocumento').removeClass('cinza');
        $('#divDocumentoConteudo').removeClass('sombreado');
      } else {
        $('#divAreaDocumento').addClass('cinza');
        $('#divDocumentoConteudo').addClass('sombreado');
      }
    }

    if (ifr==null && !bolLinkDocumento){
      ifr = parent.document.getElementById('ifrVisualizacao');
    }
    if (ifr==null) { //tela sess�o de celular ou �rvore do processo e link de documento
      window.open(link, '_blank');
    } else { //arvore do processo
      ifr.src = link;
    }

    marcarTrAcessadaElemento(el);
  }
  function marcarTrAcessadaElemento(el){
    $('.infraTrAcessada').removeClass('infraTrAcessada');
    $(el).closest('tr').addClass('infraTrAcessada');
  }
  function cancelarSessao(){
  if (confirm('Confirma o cancelamento da sess�o?')) {
    location.href='<?=$strLinkCancelarSessao;?>';
  }
}
  function encerrarSessao(){
    if (confirm('Confirma o encerramento da sess�o?')) {
      <?
      if($numTotalItens>3){
      ?>
      infraAbrirBarraProgresso(document.getElementById('frmSessaoJulgamentoCadastro'), '<?=$strLinkEncerrarSessao;?>', 600, 200);
      <?
      } else {
      ?>
      infraExibirAviso();
      location.href='<?=$strLinkEncerrarSessao;?>';
      <?
      }
      ?>
    }
  }
  function finalizarSessao(){
    if (confirm('Confirma a finaliza��o da sess�o?')) {
      <?
      if($numTotalItens>3){
      ?>
      infraAbrirBarraProgresso(document.getElementById('frmSessaoJulgamentoCadastro'), '<?=$strLinkFinalizarSessao;?>', 600, 200);
      <?
      } else {
      ?>
      infraExibirAviso();
      location.href='<?=$strLinkFinalizarSessao;?>';
      <?
      }
      ?>
    }
  }
  function acaoMultipla(acao){
    var selecao=$('input[type=checkbox]:checked').not('[name^=chkmembros]');
    if (selecao.length>0){
      var form=document.getElementById('frmSessaoJulgamentoCadastro');
      if (document.getElementById('ifrDocumento')==null){
        form.target='ifrVisualizacao';
      } else {
        form.target='ifrDocumento';
      }
      form.action=acao;
      form.submit();
      form.action='<?=$strLinkAction?>';
      form.target='_self';
      $('.infraTrAcessada').removeClass('infraTrAcessada');
      selecao.closest('tr').addClass('infraTrAcessada');
    } else {
      $('.infraTrAcessada').removeClass('infraTrAcessada');
      alert('Nenhum Processo selecionado.');
    }
  }
  function acaoMultiplaMembros(acao){
    var selecao=$('input[type=checkbox][name^=chkmembros]:checked');
    if (selecao.length>0){
      var form=document.getElementById('frmSessaoJulgamentoCadastro');
      form.action=acao;
      form.submit();
    } else {
      alert('Nenhum Membro selecionado.');
    }
  }
  function votarMultiplo(remanescentes){
    if (remanescentes===true){
      $('tr[data-sta-item!=N] input:checked').each(function(){this.click()});
      $('tr[data-sta-item=N] input:not(:checked)').each(function(){this.click()});
    }
    if ($('input[type=checkbox]:checked').not('[name^=chkmembros]').length===0){
      if(remanescentes){
        alert('Nenhum Processo remanescente encontrado (sem vota��o iniciada).')
      } else {
        alert('Nenhum Processo selecionado.');
      }
      return;
    }
    infraAbrirJanelaModal('about:blank',800,500);
    var form=document.getElementById('frmSessaoJulgamentoCadastro');
    form.target='modal-frame';
    form.action='<?=$strLinkVotar;?>';
    form.submit();
    form.action='<?=$strLinkAction?>';
    form.target='_self';
  }
  function ocultarTabelaVazia(){
    if($(this).find('tbody tr:not(.oculta)').length===0){
      $(this).addClass('oculta');
    }
  }
  function processarFiltros(){
    var bolDestaques=$('#chkDestaque').prop('checked');
    var bolRevisado=$('#chkRevisao').prop('checked');

    var divs=$('div[id^="divBloco"]');

    //remove filtros
    $('.oculta').removeClass('oculta');

    if(bolDestaques || bolRevisado){
      var filtro='';
      if(bolDestaques){
        filtro+='D';
      }
      if(bolRevisado){
        filtro+='R';
      }
      infraCriarCookie(cookie +'_filtros',filtro,356);

      if(bolDestaques){
        $('tr[data-destaques="0"]').addClass('oculta');
      }
      if(bolRevisado){
        $('tr[data-revisado="S"]').addClass('oculta');
      }
      divs.each(function(){
        $(this).find('.infraAreaTabela').each(ocultarTabelaVazia);
      });

    } else {
      infraRemoverCookie(cookie +'_filtros');
    }

    divs.each(function(){
      var a=$(this).find('tbody tr').not('.oculta').length;
      var resumo=$(this).prev().find('.resumo');
      if(a===0){
        resumo.text('');
      } else if(a===1){
        resumo.text('1 Item');
      }else {
        resumo.text(a+ ' Itens');
      }
      $(this).find('.infraTable tbody').each(function(){
        $(this).find('tr').removeClass('infaTrClara');
        $(this).find('tr').removeClass('infaTrEscura');
        $(this).find('tr:odd').addClass('infraTrEscura');
        $(this).find('tr:even').addClass('infraTrClara');
      })
    });

  }
  function inicializar(){

    if (document.getElementById('divConteudo')!=null) {
      document.getElementById('divConteudo').style.display = 'block';
    }

    // processarFiltros();
    var divs=['divMembros','divPauta','divMesa','divReferendo'];
    for(var i=0;i<4;i++) {
      if (infraLerCookie(cookie +'_'+ divs[i]) === 'Fechar') {
        exibe('#' + divs[i]);
      }
    }
    var filtros=infraLerCookie(cookie +'_filtros');
    if(filtros && document.getElementById('chkDestaque')!=null){
      if (filtros.indexOf('D') !== -1) {
        document.getElementById('chkDestaque').click();
      }
      if (filtros.indexOf('R') !== -1) {
        document.getElementById('chkRevisao').click();
      }
    }

    var ifr=document.getElementById('ifrDocumento');
    if (ifr) ifr.src="about:blank";
    <? if ($idSessaoJulgamento!=null && $chkSinExibirIframe==='S') { ?>
    redimensionar();
    var lnkMenu=document.getElementById("lnkInfraMenuSistema");
    if (lnkMenu){
      infraAdicionarEvento(lnkMenu,'click',redimensionarMenu);
    }

    infraAdicionarEvento(window,'resize',redimensionar);
    if (!INFRA_IOS){
      document.getElementById("divRedimensionar").onmousedown = selectmouse;
      document.onmouseup = dropmouse;
      document.body.onmouseleave = dropmouse;
    }
    infraFlagResize=true;
    <? } ?>

    infraOcultarMenuSistemaEsquema();
    objAjaxSessao=new infraAjaxMontarSelectDependente('selColegiado','selSessao','<?=$strLinkAjaxSessao;?>');
    objAjaxSessao.mostrarAviso = false;
    objAjaxSessao.tempoAviso = 1000;
    objAjaxSessao.prepararExecucao = function(){
      var ret='IdColegiado='+document.getElementById('selColegiado').value;
      ret=ret+'&IdSessaoJulgamento=<?=$idSessaoJulgamento?>';
      ret=ret+'&StaSituacao=<?=SessaoJulgamentoRN::$ES_CANCELADA?>,<?=SessaoJulgamentoRN::$ES_PREVISTA?>,<?=SessaoJulgamentoRN::$ES_SUSPENSA?>,<?=SessaoJulgamentoRN::$ES_PAUTA_ABERTA?>,<?=SessaoJulgamentoRN::$ES_PAUTA_FECHADA?>,<?=SessaoJulgamentoRN::$ES_ABERTA?>,<?=SessaoJulgamentoRN::$ES_ENCERRADA?>,<?=SessaoJulgamentoRN::$ES_FINALIZADA?>';
      return ret;
    };

    objAjaxSessao.executar();
    infraDesabilitarCamposAreaDados();
    $('#chkDestaque').prop('disabled',false);
    $('#chkRevisao').prop('disabled',false);
      $('label.infraLabelTitulo img').css('visibility','');
    //document.getElementById('chkSinExibirIframe').disabled=false;
    if (document.getElementById('btnVoltar')!==null) {
      document.getElementById('btnVoltar').focus();
    }

    infraEfeitoTabelas();
    var ancora=infraGetAnchor();
    if (ancora!=null) {
      ancora = ancora.replace('ID-', '');
      var tr=document.getElementById('trItem'+ancora);
      if(tr) {
        infraFormatarTrAcessada(tr);
        $(tr).find('a:visible').eq(0).focus();
      }
    }
    <?if ($_GET['acao']!='sessao_julgamento_acompanhar') {  ?>
    document.getElementById('selColegiado').disabled = false;
    document.getElementById('selSessao').disabled = false;
    <? }  ?>


  }
  function selecionarTodos(sel,img){
    var div=$(sel);
    if(img.title=='Selecionar Tudo'){
      img.title = 'Remover Sele��o';
      div.find('input:not(:checked):visible').each(function(){this.click()});
    } else {
      img.title = 'Selecionar Tudo';
      div.find('input:checked:visible').each(function(){this.click()});
    }


  }
  function selecionaNaoVotados(sel){
    var div=$(sel);
    div.find('input:checked').each(function(){this.click()});
    div.find('tr[data-sta-item=N] input:not(:checked):visible').each(function(){this.click()});
  }
  function limparTela(){
    var a=document.getElementById('divConteudo');
    if (a) {
      $(a).empty();
      $('#lblStatus').remove();
      $('#lblStatus2').remove();
      $('#lblQuorum').remove();
      $('#txtQuorum').remove();
      $('#fldFiltros').remove();
      $('#lblProcesso').remove();
      $('#ancProcesso').remove();
    }
  }

function onSubmitForm() {
  return true;
}
  function exibe(sel,image){
    var cookiediv=cookie+'_'+sel.substr(1);
    var img;
    $(sel).toggle();
    if (image) {
      img=$(image);
    } else {
      img=$(sel).prev().find('img').first();
    }

    if (img.attr('src')=='<?=PaginaSEI::getInstance()->getIconeOcultar()?>'){
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeExibir()?>');
      infraCriarCookie(cookiediv,"Fechar",356);
    } else {
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeOcultar()?>');
      infraRemoverCookie(cookiediv);
    }
    //label.innerHTML='&'
  }
function salvarCookieIframe(){
//  var cookieifr=cookie+'_SJ_IFRAME_DOCUMENTO';
//  var chk=document.getElementById('chkSinExibirIframe');
//  if(!chk.checked){
//    infraCriarCookie(cookieifr,"N",356);
//  } else {
//    infraRemoverCookie(cookieifr);
//  }
  var form=document.getElementById('frmSessaoJulgamentoCadastro');
  form.submit();
}

function verResumo(link){
  window.open(link, 'janelaResumoSessao');
  // a = document.createElement('a');
  // a.href = link;
  // a.target = 'janelaResumoSessao';
  // infraPosicionarAbaNavegador(a);
}


  function atualizarIconeBloqueio(idItem,strTitle,strOver,strOut,bolAtivo) {
    var a = $('#trItem'+idItem).find('[data-icon="bloqueio"]');
    var ib64=new infraBase64();
    a.attr('title',ib64.decodificar(strTitle));
    a.attr('onmouseover',ib64.decodificar(strOver));
    a.attr('onmouseout',ib64.decodificar(strOut));
    var img=a.find('img');
    var str=img.attr('src');
    if(bolAtivo){
      str=str.replace('acesso_restrito2.svg','acesso_restrito1.svg');
    } else {
      str=str.replace('acesso_restrito1.svg','acesso_restrito2.svg');
    }
    img.attr('src',str);
  }

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=$strLinkAction?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
?>
  <div class="row">

    <div class="col-6 col-xl-5 order-1">
      <div class="row">
        <div class="col-12">
          <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
          <select id="selColegiado" name="selColegiado" onchange="limparTela();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
            <?= $strItensSelColegiado ?>
          </select>
        </div>
      </div>
      <div class="row my-1">
        <div class="col-12 col-lg-8">
          <label id="lblSessao" for="selSessao" accesskey="" class="infraLabelObrigatorio">Data:</label>
          <select id="selSessao" name="selSessao" onchange="this.form.submit();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"> </select>
        </div>

        <? if ($idSessaoJulgamento!=null) { ?>
          <div class="col">
            <label id="lblQuorum" for="txtQuorum" class="infraLabelObrigatorio">Qu�rum m�nimo:</label>
            <input type="text" id="txtQuorum" name="txtQuorum" class="infraText" value="<?= $objSessaoJulgamentoDTO->getNumQuorumMinimoColegiado(); ?>" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>
          </div>
          <? } ?>
      </div>
    </div>

<?

if ($idSessaoJulgamento!=null) { ?>
    <div class="col-6 col-xl-2 my-1 order-12 <?=$bolExibeProcesso?'order-xl-2':'order-xl-4'?> align-self-end">
    <? if ($bolExibeProcesso) { ?>
        <label id="lblProcesso" class="infraLabelObrigatorio">Processo:</label>
        <?= $strLinkProcesso; ?>
    <? } ?>
    </div>
    <?
    SessaoJulgamentoINT::montarDivFiltros('col-3 col-xl-2 order-3 align-self-center');
    ?>

    <div class="col-3 order-10 align-self-center">
      <label id="lblStatus" class="infraLabelObrigatorio">Situa��o:</label>
      <label id="lblStatus2" class="infraLabelObrigatorio" style="<?= $strStyle; ?>"><?= PaginaSEI::tratarHTML($strSituacaoSessao); ?></label>

    </div>

    <input type="hidden" id="hdnIdSessaoJulgamento" name="hdnIdSessaoJulgamento"
           value="<?= $objSessaoJulgamentoDTO->getNumIdSessaoJulgamento(); ?>"/>


  </div>
<?

    if ($chkSinExibirIframe=='S') {
      ?>
      <div id="divConteudo" class="col-12 px-0">
      <div id="divArvore"></div>
      <div id="divAreaPauta">
    <?
    }
    PaginaSEI::getInstance()->abrirAreaDados('3.4em');
    ?><label id="lblTabelaColegiado" class="infraLabelTitulo infraCorBarraSuperior"><img onclick="exibe('#divMembros',this);" alt="Exibir/Ocultar Membros" title="Exibir/Ocultar Membros" src="<?=PaginaSEI::getInstance()->getIconeOcultar()?>" />
  <? if($bolExibirCheckboxMembros) { ?>
  <img src="/infra_css/svg/check.svg" id="imgmembrosCheck" title="Selecionar Tudo" alt="Selecionar Tudo" class="infraImg" onclick="infraSelecaoMultipla('membros');">
  <? } ?>
  &nbsp;Membros do Colegiado
    <div class="resumo"><?=$numRegistrosMembros?> Membros<?=$strPresentes?></div>
    </label>

        <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?><div id='divMembros'><?
    PaginaSEI::getInstance()->montarAreaTabela($strResultadoMembros, $numRegistrosMembros,true);
    ?>
    <br/></div>

    <?
    // inicio blocos de sessao
    foreach ($arrObjSessaoBlocoDTO as $objSessaoBlocoDTO) {
      $idSessaoBloco=$objSessaoBlocoDTO->getNumIdSessaoBloco();
      $strDescricao=$objSessaoBlocoDTO->getStrDescricao();
      $numRegistrosBloco=0;
      if (isset($arrObjTabelaHtmlDTO[$idSessaoBloco])) {
        foreach ($arrObjTabelaHtmlDTO[$idSessaoBloco] as $objTabelaHtmlDTO) {
          $numRegistrosBloco+=$objTabelaHtmlDTO->getNumRegistros();
        }
      }
      SessaoJulgamentoINT::montarBarraBloco($idSessaoBloco, $bolPermiteSelecaoItens, $bolCheckItensNaoVotados, $strDescricao, $numRegistrosBloco);

      //monta conte�do do bloco (pauta/mesa/ref/etc)
      echo "<div id='divBloco$idSessaoBloco'>";

      /** @var TabelaHtmlDTO[][] $arrObjTabelaHtmlDTO */
      if (isset($arrObjTabelaHtmlDTO[$idSessaoBloco])) {
        foreach ($arrObjTabelaHtmlDTO[$idSessaoBloco] as $objTabelaHtmlDTO) {
          PaginaSEI::getInstance()->montarAreaTabela($objTabelaHtmlDTO->getStrHtml(), $objTabelaHtmlDTO->getNumRegistros(), true);
        }
      } else {
        PaginaSEI::getInstance()->montarAreaTabela('', null, true);
        echo '<br />';
      }
      echo '</div>';
    }

    if ($chkSinExibirIframe==='S') {
      ?>
      </div>
      <div id="divRedimensionar" class="dragme"></div>
      <div id="divAreaDocumento" class="cinza">
        <div id="divDocumentoConteudo" class="sombreado" style="height: 100%;">
          <div id="divHtml" style="display: contents">
            <iframe id="ifrDocumento" name="ifrDocumento" onload="" src="about:blank" frameborder="0"></iframe>
          </div>
        </div>
      </div>
    </div>
    <?
    }

} else {
  PaginaSEI::getInstance()->fecharAreaDados();
}
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>