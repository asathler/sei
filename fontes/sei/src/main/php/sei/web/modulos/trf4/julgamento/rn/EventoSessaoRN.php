<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class EventoSessaoRN extends InfraRN {

  public static $TE_DISPOSITIVO=1;
  public static $TE_DESTAQUE=2;
  public static $TE_COMENTARIO=3; //migrado para destaques
  public static $TE_ENCERRAMENTO=4;
  public static $TE_INCLUSAO_ITEM=5;
  public static $TE_LANCAMENTO_VOTO=6;
  public static $TE_ALTERACAO_STATUS_ITEM=7; //retirada/diligencia/adiado/vista
  public static $TE_ALTERACAO_DOC_DISPONIBILIZADO=8;
  public static $TE_VOTO_CANCELADO=9;
  public static $TE_STATUS_SESSAO=10; //situacao sess�o /horario votacao(virtual)
  public static $TE_ORDENACAO_PROCESSOS=11;
  public static $TE_ALTERACAO_PRESENCA=12;
  public static $TE_REVISAO_ITEM=13;
  public static $TE_FALHA_SINCRONISMO=-1; //solicitar reload da p�gina, cache expirado reiniciou contagem
  public static $TEMPO_CACHE_EVENTOS=43200; //12 horas

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private static function buscarRangeEventosSessao(int $numIdSessaoJulgamento):array
  {
    $strCache = 'MD_JULGAR_EVENTOS_'.$numIdSessaoJulgamento;
    $strEventos = CacheSEI::getInstance()->getAtributo($strCache);
    if($strEventos==null || preg_match('/\d+\/\d+/',$strEventos)!==1){
      $ts=InfraData::getTimestamp(InfraData::getStrDataHoraAtual());
      $strEventos="$ts/$ts";
      CacheSEI::getInstance()->setAtributo($strCache, $strEventos, self::$TEMPO_CACHE_EVENTOS);
      return [$ts,$ts];
    }

    return explode('/',$strEventos);
  }
  public static function buscarUltimoEvento(EventoSessaoDTO $objEventoSessaoDTO){
    $rangeEventos=self::buscarRangeEventosSessao($objEventoSessaoDTO->getNumIdSessaoJulgamento());
    return $rangeEventos[1];
  }

  public static function lancar(EventoSessaoDTO $objEventoSessaoDTO) {
    try{

      $strCache = 'MD_JULGAR_EVENTOS_'.$objEventoSessaoDTO->getNumIdSessaoJulgamento();
      $arrRangeEventos = self::buscarRangeEventosSessao($objEventoSessaoDTO->getNumIdSessaoJulgamento());
      $arrRangeEventos[1]++;
      CacheSEI::getInstance()->setAtributo($strCache, implode('/',$arrRangeEventos), self::$TEMPO_CACHE_EVENTOS);
      $numUltimoEvento=$arrRangeEventos[1];

      $strCacheItem=$strCache.'_'.$numUltimoEvento;

      $arrCache=array();
      $arrCache['seq']=$numUltimoEvento;
      $arrCache['ts']=InfraData::getStrDataHoraAtual();
      $arrCache['tipo_evento']=$objEventoSessaoDTO->getNumTipoEvento();
      $arrCache['id_item']=$objEventoSessaoDTO->getNumIdItemSessaoJulgamento();
      $arrCache['descricao']=utf8_encode($objEventoSessaoDTO->getStrDescricao());

      CacheSEI::getInstance()->setAtributo($strCacheItem, json_encode($arrCache, JSON_THROW_ON_ERROR), CacheSEI::getInstance()->getNumTempo());

      return $numUltimoEvento;


    }catch(Exception $e){
      throw new InfraException('Erro lancando EventoSessao.',$e);
    }
  }


  protected function limparControlado(EventoSessaoDTO $objEventoSessaoDTO){
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('evento_sessao_limpar',__METHOD__,$objEventoSessaoDTO);


      $strCache = 'MD_JULGAR_EVENTOS_'.$objEventoSessaoDTO->getNumIdSessaoJulgamento();
      $numUltimoEvento = CacheSEI::getInstance()->getAtributo($strCache);
      if($numUltimoEvento==null){
        return;
      }

      for($i=$numUltimoEvento; $i>0; --$i){
        CacheSEI::getInstance()->removerAtributo($strCache.'_'.$i);
      }
      CacheSEI::getInstance()->removerAtributo($strCache);

    }catch(Exception $e){
      throw new InfraException('Erro removendo EventoSessao.',$e);
    }
  }

  /**
   * @param EventoSessaoDTO $objEventoSessaoDTO
   * @return string json
   * @throws InfraException
   */
  public static function listarJSON(EventoSessaoDTO $objEventoSessaoDTO) {
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('evento_sessao_listar',__METHOD__,$objEventoSessaoDTO);

      $strCache = 'MD_JULGAR_EVENTOS_'.$objEventoSessaoDTO->getNumIdSessaoJulgamento();
      list($numPrimerioEvento,$numUltimoEvento) = self::buscarRangeEventosSessao($objEventoSessaoDTO->getNumIdSessaoJulgamento());

      $numSeqEvento=$objEventoSessaoDTO->getNumSeqEvento();

      if($numSeqEvento==$numUltimoEvento){
        return '[]';
      }
      if($numSeqEvento>$numUltimoEvento||$numSeqEvento<$numPrimerioEvento){
        return '[{seq:0,ts:"'.InfraData::getStrDataHoraAtual().'",tipo_evento:'.self::$TE_FALHA_SINCRONISMO.'}]';
      }

      $ret='[';
      $concat='';
      for($i=$numSeqEvento+1; $i<=$numUltimoEvento; ++$i){
        $tmp=CacheSEI::getInstance()->getAtributo($strCache.'_'.$i);
        if($tmp!=null){
          $ret.=$concat.$tmp;
          $concat=',';
        }
      }
      $ret.=']';

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando EventoSessao.',$e);
    }
  }


}
?>