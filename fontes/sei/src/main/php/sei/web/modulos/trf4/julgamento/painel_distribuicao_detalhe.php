<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 20/10/2014 - criado por bcu
 *
 * Vers�o no CVS: $Id$
 */

try {
  require_once __DIR__ . '/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $strParametros='&sta_distribuicao='.$_GET['sta_distribuicao'];

  $strIdContato = PaginaSEI::getInstance()->recuperarCampo('hdnIdContato');
  $strDataFinal = PaginaSEI::getInstance()->recuperarCampo('txtDataFinal');
  $strDataInicial = PaginaSEI::getInstance()->recuperarCampo('txtDataInicial');
  $numIdTipoProcedimento = PaginaSEI::getInstance()->recuperarCampo('selTipoProcedimento', 'null');

  $strNomeContato = PaginaSEI::getInstance()->recuperarCampo('txtContato');
  $strDescricaoAutuacao = PaginaSEI::getInstance()->recuperarCampo('txtAutuacao');
  $numIdTipoMateria=PaginaSEI::getInstance()->recuperarCampo('selTipoMateria', 'null');
  $strHdnTipo = PaginaSEI::getInstance()->recuperarCampo('hdnTipo');

  if (isset($_GET['id_colegiado'])) {
    $idColegiado=$_GET['id_colegiado'];
  } else {
    $idColegiado=null;
  }
  if (isset($_GET['id_unidade'])) {
    $idUnidade=$_GET['id_unidade'];
  } else {
    $idUnidade='';
  }

  if($idColegiado=='null'||$idColegiado=='') {
    $idColegiado=null;
  }


  $bolGlobal=SessaoSEI::getInstance()->verificarPermissao('painel_distribuicao_global');


  switch ($_GET['acao']) {

    case 'painel_distribuicao_detalhar':

      PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

      if (InfraString::isBolVazia($_GET['sta_distribuicao'])) {
        throw new InfraException('Estado da distribui��o n�o informado.');
      }

      $strTitulo = 'Detalhe de Distribui��o de Processos';
      break;

    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }

  $arrComandos = array();

  $objPainelDistribuicaoDTO = new PainelDistribuicaoDTO();
  if (!$bolGlobal) {
    $objPainelDistribuicaoDTO->setNumIdUnidadeRelator(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
  } else if ($idUnidade!=''){
    $objPainelDistribuicaoDTO->setNumIdUnidadeRelator($idUnidade);
  }

  if ($idUnidade!=''){
    $objUnidadeDTO=new UnidadeDTO();
    $objUnidadeDTO->setNumIdUnidade($idUnidade);
    $objUnidadeDTO->retStrSigla();
    $objUnidadeRN=new UnidadeRN();
    $objUnidadeDTO=$objUnidadeRN->consultarRN0125($objUnidadeDTO);
//    $strTitulo .= ' ('.$objUnidadeDTO->getStrSigla().')';
  }
  if(!InfraString::isBolVazia($strDataInicial)) {
    $objPainelDistribuicaoDTO->adicionarCriterio(array('SituacaoAtual'),array(InfraDTO::$OPER_MAIOR_IGUAL),array($strDataInicial),null);
  }
  if(!InfraString::isBolVazia($strDataFinal)) {
    $objPainelDistribuicaoDTO->adicionarCriterio(array('SituacaoAtual'),array(InfraDTO::$OPER_MENOR_IGUAL),array($strDataFinal.' 23:59:59'),null);
  }
  if($numIdTipoProcedimento!='null'){
    $objPainelDistribuicaoDTO->setNumIdTipoProcedimento($numIdTipoProcedimento);
  }
  if($numIdTipoMateria!='null'){
    $objPainelDistribuicaoDTO->setNumIdTipoMateria($numIdTipoMateria);
  }
  if(!InfraString::isBolVazia($strDescricaoAutuacao)){
    $objPainelDistribuicaoDTO->setStrPalavrasPesquisa($strDescricaoAutuacao);

  }
  if($strIdContato){
    $objPainelDistribuicaoDTO->setStrStaParticipacaoParticipante(ParticipanteRN::$TP_INTERESSADO);
    $objPainelDistribuicaoDTO->setNumIdContatoParticipante($strIdContato);
  }

  $objPainelDistribuicaoDTO->setStrStaUltimo(array(DistribuicaoRN::$SD_ULTIMA,DistribuicaoRN::$SD_ULTIMA_JULGADO),InfraDTO::$OPER_IN);
  $objPainelDistribuicaoDTO->retDblIdProcedimento();
  $objPainelDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
  $objPainelDistribuicaoDTO->retNumIdUnidadeRelator();
  $objPainelDistribuicaoDTO->retStrNomeUsuarioRelator();
  $objPainelDistribuicaoDTO->retStrSiglaUnidadeRelator();
  $objPainelDistribuicaoDTO->retStrDescricaoUnidadeRelator();
  $objPainelDistribuicaoDTO->retStrStaDistribuicao();
  $objPainelDistribuicaoDTO->retDthDistribuicao();
  $objPainelDistribuicaoDTO->retDthSituacaoAtual();
  $objPainelDistribuicaoDTO->retStrProtocoloFormatado();
  $objPainelDistribuicaoDTO->retStrStaNivelAcessoGlobal();
  $objPainelDistribuicaoDTO->setStrStaDistribuicao(explode(',',$_GET['sta_distribuicao']), InfraDTO::$OPER_IN);

  $objDistribuicaoRN = new DistribuicaoRN();


  $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
  $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();

  if($idColegiado!=null){
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($idColegiado);
  }
  $objColegiadoComposicaoDTO->retNumIdColegiadoColegiadoVersao();
  $objColegiadoComposicaoDTO->retNumIdUnidade();
  $objColegiadoComposicaoDTO->retNumIdUsuario();
  $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
  $objColegiadoComposicaoDTO->setDistinct(true);
  $objColegiadoComposicaoDTO->setOrdNumIdColegiadoColegiadoVersao(InfraDTO::$TIPO_ORDENACAO_ASC);

  $arrObjColegiadoComposicaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
  $arrColegiados=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdColegiadoColegiadoVersao',true);
  foreach ($arrColegiados as $chave=>$arrComposicao) {
    $arrColegiados[$chave]=InfraArray::converterArrInfraDTO($arrComposicao,'IdUsuario','IdUnidade');
  }


  if($bolGlobal){
    $objPainelDistribuicaoDTO->setNumIdUnidadeResponsavelColegiado(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
  }
  if ($idColegiado!=null){
    $objPainelDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($idColegiado);
  }

  $objDistribuicaoRN=new DistribuicaoRN();
  $arrObjEstadoPainelDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
  $arrObjEstadoDistribuicao=InfraArray::indexarArrInfraDTO($arrObjEstadoPainelDistribuicaoDTO,'StaDistribuicao');

  PaginaSEI::getInstance()->prepararOrdenacao($objPainelDistribuicaoDTO, 'IdProcedimento', InfraDTO::$TIPO_ORDENACAO_DESC);
  PaginaSEI::getInstance()->prepararPaginacao($objPainelDistribuicaoDTO);


  $arrObjPainelDistribuicaoDTO = $objDistribuicaoRN->listarPainel($objPainelDistribuicaoDTO);
  $arrIdProtocolosSigilosos= array();
  $arrIdProtocolosSigilososPermitidos= array();
  foreach($arrObjPainelDistribuicaoDTO as $dto){
    if($dto->getStrStaNivelAcessoGlobal() === ProtocoloRN::$NA_SIGILOSO){
      $arrIdProtocolosSigilosos[] = $dto->getDblIdProcedimento();
    }
  }
  if(InfraArray::contar($arrIdProtocolosSigilosos)){
    $objPesquisaProtocoloDTO=new PesquisaProtocoloDTO();
    $objPesquisaProtocoloDTO->setDblIdProtocolo($arrIdProtocolosSigilosos);
    $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_PROCEDIMENTOS);
    $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_AUTORIZADO);
    $objProtocoloRN=new ProtocoloRN();
    $arrObjProtocoloDTO=$objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);
    foreach ($arrObjProtocoloDTO as $objProtocoloDTO) {
      if($objProtocoloDTO->getNumCodigoAcesso()>0) $arrIdProtocolosSigilososPermitidos[]=$objProtocoloDTO->getDblIdProtocolo();
    }
  }
  $arrObjProtocoloDTO = array();
  $baseDTO = new ProtocoloDTO();
  $baseDTO->setDblIdProtocolo(null);
  $baseDTO->setStrStaNivelAcessoGlobal(null);
  foreach ($arrObjPainelDistribuicaoDTO as $objPainelDistribuicaoDTOBanco) {
    /** @var PainelDistribuicaoDTO $objPainelDistribuicaoDTOBanco  */
    $objProtocoloDTO = clone $baseDTO;
    $objProtocoloDTO->setDblIdProtocolo($objPainelDistribuicaoDTOBanco->getDblIdProcedimento());
    $objProtocoloDTO->setStrStaNivelAcessoGlobal($objPainelDistribuicaoDTOBanco->getStrStaNivelAcessoGlobal());
    $arrObjProtocoloDTO[] = $objProtocoloDTO;
  }
  $objAnotacaoRN = new AnotacaoRN();
  $objAnotacaoRN->complementar($arrObjProtocoloDTO);

  $arrObjProtocoloDTO = InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO, 'IdProtocolo');

  PaginaSEI::getInstance()->processarPaginacao($objPainelDistribuicaoDTO);
  $numRegistros = InfraArray::contar($arrObjPainelDistribuicaoDTO);

  if ($numRegistros > 0){

    $bolCheck = true;
    $bolAcaoImprimir = true;
    $bolAcaoProcedimentoTrabalhar = SessaoSEI::getInstance()->verificarPermissao('procedimento_trabalhar');
    $bolAcaoDocumentoVisualizar = SessaoSEI::getInstance()->verificarPermissao('documento_visualizar');

    if ($bolAcaoImprimir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="imprimirTabela();" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir</button>';

    }

    if (SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar')) {
      $strLinkGerarDistribuicao = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=distribuicao_gerar&acao_origem=procedimento_controlar&acao_retorno=procedimento_controlar&pagina_simples=1');
      $arrComandos[] = '<button type="button" accesskey="R" id="btnRedistribuir" value="Redistribuir" onclick="redistribuir();" class="infraButton"><span class="infraTeclaAtalho">R</span>edistribuir</button>';
    }

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Processos.';
    $strCaptionTabela = 'Processos';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }

    if ($bolGlobal) {
      $strResultado .= '<th class="infraTh" width="17%">'.PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO,'Unidade','IdUnidadeRelator',$arrObjPainelDistribuicaoDTO).'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh" width="27%">'.PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO,'Processo','IdProcedimento',$arrObjPainelDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="25%">Partes</th>'."\n";
    $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO,'Situa��o','StaDistribuicao',$arrObjPainelDistribuicaoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="12%">'.PaginaSEI::getInstance()->getThOrdenacao($objPainelDistribuicaoDTO,'Data','SituacaoAtual',$arrObjPainelDistribuicaoDTO).'</th>'."\n";

    if($bolGlobal){
      $strResultado .= '<th class="infraTh">A��es</th>' . "\n";
    }

    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      $numIdColegiado=$arrObjPainelDistribuicaoDTO[$i]->getNumIdColegiadoColegiadoVersao();
      if ($arrObjPainelDistribuicaoDTO[$i]->getStrStaDistribuicao()!=DistribuicaoRN::$STA_JULGADO &&
          (!isset($arrColegiados[$numIdColegiado][$arrObjPainelDistribuicaoDTO[$i]->getNumIdUnidadeRelator()]) ||
              $arrObjPainelDistribuicaoDTO[$i]->getNumIdUsuarioRelator()!=$arrColegiados[$numIdColegiado][$arrObjPainelDistribuicaoDTO[$i]->getNumIdUnidadeRelator()]
          )) {
        $strResultado .= '<tr class="trVermelha">';
      } elseif($arrObjPainelDistribuicaoDTO[$i]->getStrStaDistribuicao()==DistribuicaoRN::$STA_PEDIDO_VISTA && (
              !in_array($arrObjPainelDistribuicaoDTO[$i]->getObjPedidoVistaDTO()->getNumIdUsuario(),$arrColegiados[$numIdColegiado]) ||
              !isset($arrColegiados[$numIdColegiado][$arrObjPainelDistribuicaoDTO[$i]->getObjPedidoVistaDTO()->getNumIdUnidade()]))) {
        $strResultado .= '<tr class="trVermelha">';
      } else {
        $strResultado .= $strCssTr;
      }

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjPainelDistribuicaoDTO[$i]->getDblIdProcedimento(),$arrObjPainelDistribuicaoDTO[$i]->getStrProtocoloFormatado()).'</td>';
      }
      if ($bolGlobal) {
        $strResultado .= '<td align="center"><a alt="'.PaginaSEI::tratarHTML($arrObjPainelDistribuicaoDTO[$i]->getStrDescricaoUnidadeRelator()).'" title="'.PaginaSEI::tratarHTML($arrObjPainelDistribuicaoDTO[$i]->getStrDescricaoUnidadeRelator()).'" class="ancoraSigla">'.PaginaSEI::tratarHTML($arrObjPainelDistribuicaoDTO[$i]->getStrSiglaUnidadeRelator()).'</a></td>';
      }
      $strResultado .= '<td align="center">';

      if ($arrObjPainelDistribuicaoDTO[$i]->getStrStaNivelAcessoGlobal() === ProtocoloRN::$NA_SIGILOSO) {
        $class = 'processoVisualizadoSigiloso';
        $bolLink = isset($arrIdProtocolosPermitidos[$arrObjPainelDistribuicaoDTO[$i]->getDblIdProcedimento()]);
      } else {
        $class = 'protocoloNormal';
        $bolLink = true;
      }
      $strStaDistribuicao=$arrObjPainelDistribuicaoDTO[$i]->getStrStaDistribuicao();
      $strTipoProcedimento=PaginaSEI::tratarHTML($arrObjPainelDistribuicaoDTO[$i]->getStrNomeTipoProcedimento());
      $strResultado .= AnotacaoINT::montarIconeAnotacao($arrObjProtocoloDTO[$arrObjPainelDistribuicaoDTO[$i]->getDblIdProcedimento()]->getObjAnotacaoDTO(), false, $arrObjPainelDistribuicaoDTO[$i]->getDblIdProcedimento(), $strParametros);
      $strLink=$bolLink?'onclick="abrirDocumento(\''. SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $arrObjPainelDistribuicaoDTO[$i]->getDblIdProcedimento()) . '\');" ':'';
      $strResultado .= '<a href="javascript:void(0);" '.$strLink.'tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() .'\');" alt="' . $strTipoProcedimento . '" title="' . $strTipoProcedimento . '" class="'.$class.'">' . PaginaSEI::tratarHTML($arrObjPainelDistribuicaoDTO[$i]->getStrProtocoloFormatado()) . '</a>';
      $strResultado .= '</td>';
      
      //partes
      $strResultado .='<td align="justified">';
      $strResultado .=ParteProcedimentoINT::montarHtmlPartes($arrObjPainelDistribuicaoDTO[$i]->getArrObjParteProcedimentoDTO());
      $strResultado .='</td>';
      
      $strResultado .= '<td align="center">' . PaginaSEI::tratarHTML($arrObjEstadoDistribuicao[$strStaDistribuicao]->getStrDescricao());
//      if (in_array($strStaDistribuicao,array(DistribuicaoRN::$STA_PEDIDO_VISTA,DistribuicaoRN::$STA_EM_MESA)) && ( $idUnidade==null || $arrObjPainelDistribuicaoDTO[$i]->getNumIdUnidadeRelator() == $idUnidade)){
      if ($arrObjPainelDistribuicaoDTO[$i]->isSetObjPedidoVistaDTO() && in_array($strStaDistribuicao,array(DistribuicaoRN::$STA_PEDIDO_VISTA,DistribuicaoRN::$STA_EM_MESA))){
        $strResultado.= ' por ';
        $strNomeUsuario=PaginaSEI::tratarHTML($arrObjPainelDistribuicaoDTO[$i]->getObjPedidoVistaDTO()->getStrNomeUsuario());
        $strResultado .= '<a '.$strLink.'tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() .'" alt="' . $strNomeUsuario . '" title="' . $strNomeUsuario . '" class="ancoraSigla" style="font-size:1em">' . $strNomeUsuario . '</a>';
      }
      $strResultado .= '</td>';


      $strResultado .= '<td align="center">'.$arrObjPainelDistribuicaoDTO[$i]->getDthSituacaoAtual().'</td>';
      if($bolGlobal){
        $strResultado.='<td align="center">';
        //a�oes
        //controlador.php?acao=distribuicao_gerar&acao_origem=procedimento_controlar&pagina_simples=1
        if(!$arrObjPainelDistribuicaoDTO[$i]->isSetObjPedidoVistaDTO()) {
          $strLink = SessaoJulgamentoINT::montarLink('distribuicao_gerar', PaginaSEI::getInstance()->getAcaoRetorno(), '', '&acao_origem=painel_distribuicao&pagina_simples=1&id_procedimento=' . $arrObjPainelDistribuicaoDTO[$i]->getDblIdProcedimento() . '&id_colegiado=' . $arrObjPainelDistribuicaoDTO[$i]->getNumIdColegiadoColegiadoVersao());
          $strResultado .= SessaoJulgamentoINT::montarAncoraHref(MdJulgarIcone::DISTRIBUICAO, 'Redistribuir', $strLink);
        } else {
          $strLink = SessaoJulgamentoINT::montarLink('pedido_vista_devolver_secretaria', 'painel_distribuicao_detalhar', '', '&id_distribuicao=' . $arrObjPainelDistribuicaoDTO[$i]->getNumIdDistribuicao() . '&id_pedido_vista=' . $arrObjPainelDistribuicaoDTO[$i]->getObjPedidoVistaDTO()->getNumIdPedidoVista() . '&arvore=1');
          $strResultado .= SessaoJulgamentoINT::montarAncoraHref(MdJulgarIcone::DISTRIBUICAO_DEVOLVER, 'Devolver para a Secretaria', $strLink);
        }

        $strResultado.='</td>';
      }

      $strResultado .= '</tr>'."\n";

    }
    $strResultado .= '</table>';
  }


  $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="window.parent.infraFecharJanelaModal();" class="infraButton" style="width:8em"><span class="infraTeclaAtalho">F</span>echar</button>';

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
//<script>
  function imprimirTabela(){
    var bolRemoverCheck = false;
    var bolRemoverAcoes = false;
    var div = document.getElementById('infraDivImpressao');
    var box;
    var numSelecionados = $('input[id^="chkInfraItem"]:checked').not(':disabled').length;
    if (numSelecionados==0) {
      alert('Nenhum registro selecionado.');
      return;
    }
    div.innerHTML = document.getElementById('divInfraAreaTabela').innerHTML;

    if (numSelecionados>0){

      var tab = div.getElementsByTagName('table');

      if (tab.length>0){
        infraAtualizarCaption(tab[0],numSelecionados);
      }
    }

    // Pega checks da div original porque os copiados n�o contem informa��o de
    // sele��o
    var boxs = document.getElementById('divInfraAreaTabela').getElementsByTagName("input");

    $('#divInfraAreaGlobal').removeClass('d-flex').hide();


    var ths = document.getElementById('infraDivImpressao').getElementsByTagName("th");
    var img;
    if (ths.length>0) {
      bolRemoverCheck = true;
      for (var i = 0; i<ths.length; i++) {
        // Verifica se � o check box da infra e apaga o TH
        img = ths[i].getElementsByTagName("img");
        if (img.length>0) {
          if (img[0].id=='imgInfraCheck') {
            ths[i].style.display = 'none';
          }
        }
      }
      // Apaga todos os THs de a��es
      for (i = 0; i<ths.length;i++){
        // Se a ultima coluna � de a��es
        if (infraTrim(ths[i].innerHTML)=='A��es'){
          bolRemoverAcoes = true;
          // Apaga coluna header
          ths[i].style.display='none';
        }
      }

      var classNameLinha = 'infraTrEscura';

      if (bolRemoverCheck || bolRemoverAcoes){

        // Apaga ultimos tds
        var trs = document.getElementById('infraDivImpressao').getElementsByTagName("tr");
        for(i=0;i < trs.length;i++){

          if (trs[i].className != 'infraTrOrdenacao'){

            tds = trs[i].getElementsByTagName("td");
            if (tds.length > 0){

              // Pega check box da primeira coluna
              box = tds[0].getElementsByTagName("input");
              if (box.length > 0){
                // Verifica se o checkbox original esta marcado
                // ja que os checboxes copiados n�o levam esta informacao
                for(var j=0;j < boxs.length;j++){
                  if (boxs[j].id==box[0].id){
                    if (!boxs[j].checked || boxs[j].disabled){
                      // se n�o esta marcado apaga linha
                      trs[i].style.display='none';
                    }
                    // Se ja achou o id n�o adianta continuar a varredura
                    break;
                  }
                }

                if (trs[i].style.display!='none'){
                  trs[i].className = classNameLinha;
                  classNameLinha = (classNameLinha=='infraTrClara')?'infraTrEscura':'infraTrClara';
                }

                if (bolRemoverCheck){
                  // apaga coluna do checkbox
                  tds[0].style.display='none';
                }
                // apaga linhas onde a primeira coluna nao tem checkbox (exceto a
                // linha de cabecalho)
              }else if (i > 0){
                trs[i].style.display='none';
              }

              if (bolRemoverAcoes){
                // apaga coluna de a��es
                tds[tds.length-1].style.display='none';
              }
            }
          }else{

            var tds = trs[i].getElementsByTagName("td");
            for(var k=0;k < tds.length;k++){
              if (tds[k].className=='infraTdSetaOrdenacao'){
                tds[k].style.display = 'none';
              }
            }

          }
        }
      }
    }

    window.print();

    // chama restaura��o via setTimeout para sincronizar a caixa de impressao
    // no Firefox (mostrava a caixa depois que tinha restaurado)
    self.setTimeout('restaurarImpressao()', 1000);

  }
  function restaurarImpressao(){
    document.getElementById('infraDivImpressao').innerHTML = '';
    $('#divInfraAreaGlobal').removeClass('d-flex').show();
  }

function inicializar(){
  document.getElementById('btnFechar').focus();
  infraEfeitoTabelas();
}

<?if (SessaoSEI::getInstance()->verificarPermissao('distribuicao_gerar')) {?>
function redistribuir(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum processo selecionado.');
    return false;
  }

  document.getElementById('hdnInfraItemId').value = '';
  document.getElementById('frmPainelDistribuicaoDetalhe').action = '<?=$strLinkGerarDistribuicao?>';
  document.getElementById('frmPainelDistribuicaoDetalhe').submit();
}
<?}

//na fun��o abrirDocumento n�o pode ser modal, pois sen�o fica distorcido dentro de outro modal
?>

function abrirDocumento(link) {
  infraAbrirJanela(link, '_blank', 850, 550, 'location=0,status=1,resizable=1,scrollbars=1',false);
}

//</script>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
  <form id="frmPainelDistribuicaoDetalhe" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
    <?
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    //PaginaSEI::getInstance()->abrirAreaDados('5em');
    //PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
    PaginaSEI::getInstance()->montarAreaDebug();
    PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
    ?>
  </form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>