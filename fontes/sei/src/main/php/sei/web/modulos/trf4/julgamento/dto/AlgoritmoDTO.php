<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 10/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class AlgoritmoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'algoritmo';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdAlgoritmo','id_algoritmo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'Contador','contador');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'StaAlgoritmo','sta_algoritmo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdOrigem','id_origem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdColegiado','id_colegiado');

    $this->configurarPK('IdAlgoritmo',InfraDTO::$TIPO_PK_NATIVA);

  }
}
?>