<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  //SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_procedimento_sessao','arvore','id_colegiado','id_distribuicao','id_sessao_julgamento_origem','id_item_sessao_julgamento_origem'));

  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

  $strDesabilitar = '';
  $strParametros='';
  if (isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }
  if (isset($_GET['id_sessao_julgamento'])){
    $idSessaoJulgamento=$_GET['id_sessao_julgamento'];
  } else {
    $idSessaoJulgamento=$_POST['selSessao'];
  }

  if (isset($_GET['id_colegiado'])){
    $idColegiado=$_GET['id_colegiado'];
  } else {
    $idColegiado='';
  }
  if (isset($_GET['id_distribuicao'])){
    $idDistribuicao=$_GET['id_distribuicao'];
  } else {
    $idDistribuicao=null;
  }
  if (!isset($_GET['id_sessao_julgamento_origem'])){
    $_GET['id_sessao_julgamento_origem']=$idSessaoJulgamento;
    $_GET['id_item_sessao_julgamento_origem']=$_GET['id_item_sessao_julgamento'];
  }
  if (isset($_GET['num_seq'])) {
    $numOrdemSequencial=$_GET['num_seq'];
  } else {
    //calcular num_seq
    $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
    $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinOrdenarItens('S');
    $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
    $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
    $objSessaoJulgamentoDTO=$objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);
    $arrObjItemSessaoJulgamentoDTO=$objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      if($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()==$_GET['id_item_sessao_julgamento']){
        $numOrdemSequencial=$objItemSessaoJulgamentoDTO->getNumOrdem();
        break;
      }
    }


  }
  $strParametros.='&num_seq='.$numOrdemSequencial;
  $strParametros.='&id_sessao_julgamento='.$idSessaoJulgamento;
  $strParametros.='&id_item_sessao_julgamento='.$_GET['id_item_sessao_julgamento'];

  $strComandoFinalizar='<button type="button" accesskey="" id="btnDispositivo" name="btnDispositivo" value="Dispositivo" onclick="finalizar();" class="infraButton">Dispositivo</button>';
  $arrComandos = array();
  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  switch($_GET['acao']){
    case 'item_sessao_julgamento_julgar':

      $strTitulo = 'Julgamento';

      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  /** @var ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO */
  $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);

  $bolSessaoVirtual=($objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getStrSinVirtualTipoSessao()=='S');

  $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();

  $objItemSessaoJulgamentoDTOBanco->setNumIdDistribuicaoAgrupadorDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicaoAgrupadorDistribuicao());
  $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao(array(ItemSessaoJulgamentoRN::$STA_SESSAO_CANCELADA, ItemSessaoJulgamentoRN::$STA_IGNORADA, ItemSessaoJulgamentoRN::$STA_RETIRADO, ItemSessaoJulgamentoRN::$STA_ADIADO, ItemSessaoJulgamentoRN::$STA_DILIGENCIA), InfraDTO::$OPER_NOT_IN);
  $objItemSessaoJulgamentoDTOBanco->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_DESC);
  $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retDthSessaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retStrSinManual();
  $objItemSessaoJulgamentoDTOBanco->retStrDescricaoSessaoBloco();
  $arrObjItemSessaoJulgamentoDTOBanco = $objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTOBanco);
  $strItensSessoes='';
  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $arrObjSituacaoSessaoJulgamentoDTO=$objSessaoJulgamentoRN->listarValoresSituacaoSessao();
  $arrObjSituacaoSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjSituacaoSessaoJulgamentoDTO,'StaSituacao');
  if(count($arrObjItemSessaoJulgamentoDTOBanco)>1) {
    foreach ($arrObjItemSessaoJulgamentoDTOBanco as $objItemSessaoJulgamentoDTOBanco) {
      $strDescricaoSessao = $objItemSessaoJulgamentoDTOBanco->getDthSessaoSessaoJulgamento();
      $strDescricaoSessao .= ' - ' . $arrObjSituacaoSessaoJulgamentoDTO[$objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()]->getStrDescricao();
      $strLink = SessaoJulgamentoINT::montarLink($objItemSessaoJulgamentoDTOBanco->getStrSinManual()==='S' ? 'item_sessao_julgamento_manual' : 'item_sessao_julgamento_julgar', $_GET['acao_retorno'], $objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento(), '&id_sessao_julgamento=' . $objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());

      $strItensSessoes .= '<option value="' . base64_encode($strLink) . '"';
      if ($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento()==$_GET['id_item_sessao_julgamento']) {
        $strItensSessoes .= ' selected="selected"';
      }
      $strItensSessoes .= '>' . $strDescricaoSessao . "</option>\n";
    }
  }

  $objSessaoBlocoDTO=CacheSessaoJulgamentoRN::getObjSessaoBlocoDTO($objItemSessaoJulgamentoDTO->getNumIdSessaoBloco());
  $strTitulo .= ' - '.$objSessaoBlocoDTO->getStrDescricao().' '.$numOrdemSequencial;

  /** @var ColegiadoComposicaoDTO[] $arrObjColegiadoComposicaoDTO */
  $arrObjColegiadoComposicaoDTO = $objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO();
  $numIdColegiadoVersaoSessao=$objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getNumIdColegiadoVersao();
  $arrObjJulgamentoParteDTO=$objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
  $qtdPartes=InfraArray::contar($arrObjJulgamentoParteDTO);
  $strItensSelParte='';
  $strDispositivo=$objItemSessaoJulgamentoDTO->getStrDispositivo();
  $arrUsuariosEmpate=array();
  $idUsuarioDesempate=null;
  $i=0;
  $idParte=$arrObjJulgamentoParteDTO[0]->getNumIdJulgamentoParte();

  if($qtdPartes>1 && isset($_POST['selParte'])){
    $idParte=$_POST['selParte'];
    for($i=0;$i<$qtdPartes;$i++){
      if ($arrObjJulgamentoParteDTO[$i]->getNumIdJulgamentoParte()==$idParte) {
        break;
      }
    }
  }
  $objJulgamentoParteDTO=$arrObjJulgamentoParteDTO[$i];

  $bolExibirResumo=false;
  $arrObjResumoVotacaoDTO=$objJulgamentoParteDTO->getArrObjResumoVotacaoDTO();
  $arrObjVotoParteDTO=$objJulgamentoParteDTO->getArrObjVotoParteDTO();
  $bolFaltamVotos=$objJulgamentoParteDTO->getBolFaltamVotos();
  $bolPedidoVista=$objJulgamentoParteDTO->getBolPedidoVista();
  if (!$bolFaltamVotos){
    if ( $objJulgamentoParteDTO->getBolPossuiEmpate()){
      $arrUsuariosEmpate=$objJulgamentoParteDTO->getArrUsuariosEmpate();
      $idUsuarioDesempate=$objJulgamentoParteDTO->getNumIdUsuarioDesempate();
    } else if(!$bolPedidoVista) {
      $idUsuarioAcordao=$objJulgamentoParteDTO->getNumUsuarioVencedor();
    }
  }

  $arrObjVotoParteDTO=InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO,'IdUsuario');
  $bolExibirVotoAssociado=false;
  foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
    if($objVotoParteDTO->getStrStaVotoParte()===VotoParteRN::$STA_DIVERGE){
      $bolExibirVotoAssociado=true;
      break;
    }
  }

  $arrObjBloqItemSessaoUnidadeDTO=$objItemSessaoJulgamentoDTO->getArrObjBloqueioItemSessUnidadeDTO();
  $arrUnidadesBloqueadas=array();
  if(count($arrObjBloqItemSessaoUnidadeDTO)){
    /** @var BloqueioItemSessUnidadeDTO $objBloqItemSessaoUnidadeDTO */
    foreach ($arrObjBloqItemSessaoUnidadeDTO as $objBloqItemSessaoUnidadeDTO){
      if($objBloqItemSessaoUnidadeDTO->getStrStaTipo()==BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO){
        $arrUnidadesBloqueadas[]=$objBloqItemSessaoUnidadeDTO->getNumIdUnidade();
      }
    }
  }

  foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO2) {
    $strItensSelParte .= '<option value="' . $objJulgamentoParteDTO2->getNumIdJulgamentoParte() . '"';
    if ($idParte == $objJulgamentoParteDTO2->getNumIdJulgamentoParte()) {
      $strItensSelParte .= ' selected="selected"';
    }
    $strItensSelParte .= '>' . $objJulgamentoParteDTO2->getStrDescricao() . "</option>\n";
  }


  $objSessaoJulgamentoDTO=$objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO();

  $strRessalvas=$objItemSessaoJulgamentoRN->montarTextoRessalvas($objItemSessaoJulgamentoDTO);
  $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
  $staDistribuicao=$objItemSessaoJulgamentoDTO->getStrStaDistribuicao();
  $bolAdmin=SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar');
  $bolReferendo=$objSessaoBlocoDTO->getStrStaTipoItem()==TipoSessaoBlocoRN::$STA_REFERENDO;

  $arrObjPresencaSessaoDTO = $objItemSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();
  $idUsuarioSessao = $objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao();
  $bolVotar= $bolAdmin && $staSessao == SessaoJulgamentoRN::$ES_ABERTA && (isset($arrObjPresencaSessaoDTO[$idUsuarioSessao]) || $bolReferendo) &&
      !in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(),array(ItemSessaoJulgamentoRN::$STA_ADIADO,ItemSessaoJulgamentoRN::$STA_DILIGENCIA)) &&
      $staDistribuicao!=DistribuicaoRN::$STA_JULGADO &&
      $objItemSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()==SessaoSEI::getInstance()->getNumIdUnidadeAtual();
  $bolEditarRessalvas= $staSessao==SessaoJulgamentoRN::$ES_ENCERRADA && SessaoSEI::getInstance()->verificarPermissao('voto_parte_alterar_ressalva');

  $bolPermissaoDocumento=false;

  $arrObjItemSessaoDocumentoDTO=$objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
  $arrIdDocumentos=array();
  if($arrObjItemSessaoDocumentoDTO){
    $arrIdDocumentos=InfraArray::converterArrInfraDTO($arrObjItemSessaoDocumentoDTO,'IdDocumento');
    $arrObjItemSessaoDocumentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoDocumentoDTO,'IdUsuario',true);
    $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
    $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_DOCUMENTOS);
    $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_AUTORIZADO);
    $objPesquisaProtocoloDTO->setDblIdProtocolo($arrIdDocumentos,InfraDTO::$OPER_IN);

    $objProtocoloRN = new ProtocoloRN();
    $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);
    if($arrObjProtocoloDTO){
      $arrObjProtocoloDTO=InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO,'IdProtocolo');
    }
  }

  if (!isset($arrObjItemSessaoDocumentoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao()],
      $arrObjItemSessaoDocumentoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()])){
    //documento n�o foi disponibilizado pelo relator ou pelo usu�rio da sess�o
    $bolVotar=false;
  }

  //se estiver em diligencia oculta votos
  if($objItemSessaoJulgamentoDTO->getStrStaSituacao()==ItemSessaoJulgamentoRN::$STA_DILIGENCIA){
    $arrObjVotoParteDTO=array();
  }

  $strLinkVotar = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=voto_parte_cadastrar&acao_origem=' . $_GET['acao'] . '&id_item_sessao_julgamento=' . $_GET['id_item_sessao_julgamento'].'&id_julgamento_parte='.$idParte);
  $strLinkSustentacao = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sustentacao_oral_consultar&acao_origem=' . $_GET['acao'].'&id_item_sessao_julgamento=' . $_GET['id_item_sessao_julgamento']);

  if ($bolVotar){
    $arrComandos[]='<button type="button" accesskey="" id="btnVoto" value="Votar" onclick="acaoVotoMultiplo();" class="infraButton">Votar</button>';
    $arrComandos[]='<button type="button" accesskey="" id="btnManual" value="Manual" onclick="acaoJulgamentoManual();" class="infraButton">Converter para Manual</button>';
  }
  $arrComandos[]='<button type="button" accesskey="" id="btnSustentacaoOral" value="Sustentacao" onclick="acaoSustentacao();" class="infraButton">Sustenta��o Oral</button>';
  $objVotoParteRN=new VotoParteRN();
  $arrEstadoVotoParte=InfraArray::indexarArrInfraDTO($objVotoParteRN->listarValoresVotoParte(),'StaVotoParte');


  $bolPermiteFinalizacao=$bolAdmin && (
       $staSessao === SessaoJulgamentoRN::$ES_ENCERRADA ||
      ($staSessao === SessaoJulgamentoRN::$ES_ABERTA &&
          ($objItemSessaoJulgamentoDTO->getStrSinPermiteFinalizacao()==='S' || $objItemSessaoJulgamentoDTO->getStrStaSituacao()==ItemSessaoJulgamentoRN::$STA_RETIRADO) )
      );
  $arrUsuariosVotantes = InfraArray::converterArrInfraDTO($arrObjVotoParteDTO, 'IdUsuario');
  $arrObjColegiadoComposicaoDTOTabela=array();
  //tabela a ser exibida, votantes, relator e (presentes n�o bloqueados)

  foreach ($arrObjColegiadoComposicaoDTO as $id=>$objColegiadoComposicaoDTO) {
    $numIdUsuario=$objColegiadoComposicaoDTO->getNumIdUsuario();
    if (isset($arrObjVotoParteDTO[$numIdUsuario]) ||
        $numIdUsuario==$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao() ||
        ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && $objColegiadoComposicaoDTO->getStrSinPresente()==='S' && !in_array($objColegiadoComposicaoDTO->getNumIdUnidade(),$arrUnidadesBloqueadas))
    ){
      $arrObjColegiadoComposicaoDTOTabela[]=$objColegiadoComposicaoDTO;
    }
  }

  $bolExibirResultado=!$bolFaltamVotos && !$bolPedidoVista && in_array($staSessao,array(SessaoJulgamentoRN::$ES_FINALIZADA,SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA));

  $numRegistrosMembros = InfraArray::contar($arrObjColegiadoComposicaoDTOTabela);
  if ($numRegistrosMembros>0) {
    $strResultadoMembros = '';
    $strSumarioTabela = 'Tabela de membros do colegiado.';
    $strCaptionTabela = 'membros do colegiado';

    $strResultadoMembros .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
    $strResultadoMembros .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistrosMembros) . '</caption>';
    $strResultadoMembros .= '<tr>';
    $strResultadoMembros .= '<th class="infraTh" ';
    if (!$bolVotar) {
      $strResultadoMembros .= 'style="display:none;" ';
    }
    $strResultadoMembros .= 'width="70">' . PaginaSEI::getInstance()->getThCheck();
    $strResultadoMembros .= '<label id="lblCheckVotoBranco" for="lnkCheckVotoBranco" accesskey=";"></label>' . "\n";
    $strResultadoMembros .= '<a href="javascript:void(0);" id="lnkCheckVotoBranco" onclick="$(\'input[type=checkbox][data-voto=vazio]:not(:checked)\').each(function(){this.click()});" tabindex="1001">';
    $strResultadoMembros .= '<img src="'.MdJulgarIcone::SELECIONAR_RESTANTES.'" id="imgInfraCheck" title="Selecionar Votos Faltantes" alt="Selecionar Votos Faltantes" class="infraImg">';
    $strResultadoMembros .= '</a>';
    $strResultadoMembros .= '</th>' . "\n";
    //$strResultadoMembros .= '<th class="infraTh">Tipo</th>' . "\n";
    $strResultadoMembros .= '<th class="infraTh">Nome</th>' . "\n";
    $strResultadoMembros .= '<th class="infraTh">Documento</th>' . "\n";
    $strResultadoMembros .= '<th class="infraTh">Voto</th>' . "\n";
    if ($bolExibirResultado) {
      $strResultadoMembros .= '<th class="infraTh">Resultado</th>' . "\n";
    }

    if ($bolVotar || $bolEditarRessalvas) {
      $strResultadoMembros .= '<th class="infraTh">A��es</th>' . "\n";
    }
    $strResultadoMembros .= '</tr>' . "\n";
    $strCssTr = '';
    for ($i = 0; $i<$numRegistrosMembros; $i ++) {

      $idUsuario = $arrObjColegiadoComposicaoDTOTabela[$i]->getNumIdUsuario();
      $staJulgamento = '';
      if (isset($arrObjVotoParteDTO[$idUsuario])) {
        $staJulgamento = $arrObjVotoParteDTO[$idUsuario]->getStrStaVotoParte();
      }
      $bolRelator = ($idUsuario==$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao());

      switch ($staJulgamento) {
        case VotoParteRN::$STA_ACOMPANHA:
          $strCssTr = 'trAcompanha';
          break;
        case VotoParteRN::$STA_ABSTENCAO:
          $strCssTr = 'trAbstencao';
          break;
        case VotoParteRN::$STA_DIVERGE:
          $strCssTr = 'trDiverge';
          break;
        case VotoParteRN::$STA_PEDIDO_VISTA:
          $strCssTr = 'trPedidoVista';
//          $bolPermiteFinalizacao=false;
          break;
        default:
          $strCssTr = 'infraTrClara';
          if ($bolRelator) {
            $strCssTr = 'infraTrAcessada';
          }
      }
      if ($bolExibirResultado) {
//        $strCssTr = 'infraTrClara';

        if ($idUsuario==$idUsuarioAcordao || in_array($idUsuario, $arrUsuariosEmpate)) {
          $strCssTr = 'trEmpate';
          if ($idUsuario==$idUsuarioDesempate) {
            $strCssTr = 'trDesempate';
          }
        }
      }
      $strResultadoMembros .= '<tr class="' . $strCssTr . '">';
      if ($bolVotar) {
        if ($bolRelator || $arrObjColegiadoComposicaoDTOTabela[$i]->getStrSinPresente()==='N') {
          $strResultadoMembros .= '<td></td>';
          PaginaSEI::getInstance()->getTrCheck($i, $idUsuario, $idUsuario);
        } else {
          $strAtributos = '';
          if ($staJulgamento=='' || $staJulgamento==VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO) {
            $strAtributos = 'data-voto="vazio"';
          }
          $strResultadoMembros .= '<td valign="top" style="text-align: center;">' . PaginaSEI::getInstance()->getTrCheck($i, $idUsuario, $idUsuario, 'N', 'Infra', $strAtributos) . '</td>';
        }
      }
      //$strResultadoMembros .= '<td align="center">' . $arrObjColegiadoComposicaoDTOTabela[$i]->getStrNomeTipoMembroColegiado() . '</td>';
      $strResultadoMembros .= '<td>' . $arrObjColegiadoComposicaoDTOTabela[$i]->getStrNomeUsuario();
      //se n�o fizer mais parte do colegiado colocar obs
      if($arrObjColegiadoComposicaoDTOTabela[$i]->isSetBolComposicaoAnterior()){
        $strResultadoMembros.= ' (N�o pertence mais ao colegiado)';
      }
      $strResultadoMembros .= '</td>';
// *incluir documentos
      $strDocumento = '';
      if (isset($arrObjItemSessaoDocumentoDTO[$idUsuario])) {
        foreach ($arrObjItemSessaoDocumentoDTO[$idUsuario] as $objItemSessaoDocumentoDTO) {
          /* @var ItemSessaoDocumentoDTO $objItemSessaoDocumentoDTO  */
          if ($strDocumento!='') {
            $strDocumento .= '<br />';
          }
          if (isset($arrObjProtocoloDTO[$objItemSessaoDocumentoDTO->getDblIdDocumento()])) {
            $strDocumento .= '<a href="javascript:void(0);" onclick="abrirDocumento(\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=documento_visualizar&id_documento=' . $objItemSessaoDocumentoDTO->getDblIdDocumento()) . '\',this);" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objItemSessaoDocumentoDTO->getStrNomeSerie()) . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
          } else {
            $strDocumento .= '<a href="javascript:void(0);" onclick="alert(\'Sem acesso ao documento.\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objItemSessaoDocumentoDTO->getStrNomeSerie()) . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
          }
        }
      }
      $strResultadoMembros .= "<td style='text-align: center'>${strDocumento}</td>";
      if ($bolRelator) {
        $strResultadoMembros .= '<td style="text-align: center"><b>Relator</b>';
      } else {
        $strResultadoMembros .= '<td style="text-align: center">';
        if ($staJulgamento!='' && $staJulgamento!=VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO) {
          $strResultadoMembros .= PaginaSEI::tratarHTML($arrEstadoVotoParte[$staJulgamento]->getStrDescricao());
        }
        if ($bolExibirVotoAssociado && $staJulgamento==VotoParteRN::$STA_ACOMPANHA) {
          $strResultadoMembros .= ' (';
          if ($arrObjVotoParteDTO[$idUsuario]->getNumIdVotoParteAssociado()==null) {
            $strResultadoMembros .= 'Relator';
          } else {
            $strResultadoMembros .= PaginaSEI::tratarHTML($arrObjVotoParteDTO[$idUsuario]->getStrNomeUsuarioAssociado());
          }
          $strResultadoMembros .= ')';
        }
      }
      if ($staJulgamento!='' && $arrObjVotoParteDTO[$idUsuario]->getNumIdProvimento()!=null) {
        $strResultadoMembros .= '&nbsp;<a href="javascript:void(0);" style="vertical-align:middle;" ' . PaginaSEI::montarTitleTooltip($arrObjVotoParteDTO[$idUsuario]->getStrComplemento(), InfraString::transformarCaixaAlta($arrObjVotoParteDTO[$idUsuario]->getStrConteudoProvimento())) . '><img src="'.MdJulgarIcone::BALAO1.'" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>';
      }
      $strResultadoMembros .= '</td>';
      if ($bolExibirResultado) {
        $strResultadoMembros .= '<td style="text-align: center">';
        if ($idUsuario==$idUsuarioDesempate) {
          $strResultadoMembros .= 'Voto de Desempate';
        } else if (in_array($idUsuario, $arrUsuariosEmpate)) {
          $strResultadoMembros .= 'Empate';
        } else if ($idUsuario==$idUsuarioAcordao) {
          $strResultadoMembros .= 'Vencedor';
        }
        $strResultadoMembros .= '</td>';
      }

      if ($bolVotar || $bolEditarRessalvas) {

        //$strDocumento='<a><img src="modulos/trf4/julgamento/imagens/transparente.png" class="infraImg" /></a>&nbsp;';
        $strDocumento = '';
        $strResultadoMembros .= '<td style="text-align: center">';
        if ($bolVotar && $arrObjColegiadoComposicaoDTOTabela[$i]->getStrSinPresente()=='S') {
          $strResultadoMembros .= '<a href="javascript:void(0)" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="'.MdJulgarIcone::VOTAR.'" onclick="acaoVotar(' . $idUsuario . ')" title="Voto" alt="Voto" class="infraImg" /></a>&nbsp;';
        }


        if ($staJulgamento!='') {
          $idDocumento = null;
          if ($idDocumento!=null) {
            $strDocumento = '<a href="javascript:void(0);" onclick="infraAbrirJanelaModal(\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=documento_visualizar&id_documento=' . $idDocumento) . '\',\'Documento\',800,600);" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" title="Documento Associado" alt="Documento Associado"  ><img src="'.Icone::DOCUMENTO_INTERNO.'" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>&nbsp;';
          }
          if ($bolEditarRessalvas) {
            $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=voto_parte_alterar_ressalva&acao_origem=' . $_GET['acao'] . '&id_voto_parte=' . $arrObjVotoParteDTO[$idUsuario]->getNumIdVotoParte());
            $strResultadoMembros .= '<a href="javascript:void(0)" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="'.MdJulgarIcone::VOTAR.'" onclick="acaoEditarRessalva(\'' . $strLink . '\');" title="Editar Ressalva" alt="Editar Ressalva" class="infraImg" /></a>&nbsp;';
          }
        }
        if ($bolVotar && !$bolFaltamVotos && !$bolPedidoVista && InfraArray::contar($arrUsuariosEmpate)>0 && $idUsuario==$objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getNumIdUsuarioPresidente()) {
          $strResultadoMembros .= '<a href="javascript:void(0)" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="'.MdJulgarIcone::MEDALHA.'" onclick="acaoDesempate();" title="Voto de desempate" alt="Voto de desempate" class="infraImg" /></a>&nbsp;';
        }
        $strResultadoMembros .= $strDocumento;

        $strResultadoMembros .= '</td>';
      }
      $strResultadoMembros .= '</tr>' . "\n";
    }
    $strResultadoMembros .= '</table>';
  }

  $numRegistrosResumo = InfraArray::contar($arrObjResumoVotacaoDTO);
  if($numRegistrosResumo){
    $strResultadoResumo = '';
    $strSumarioTabela = 'Tabela de resumo de vota��o.';
    $strCaptionTabela = 'resumo da vota��o';
    $strResultadoResumo .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
    $strResultadoResumo .= '<caption class="infraCaption">' . PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela, $numRegistrosResumo) . '</caption>';
    $strResultadoResumo .= '<tr>';
    $strResultadoResumo .= '<th style="text-align: center" class="infraTh">Votos</th>' . "\n";
    $strResultadoResumo .= '<th class="infraTh">Tipo</th>' . "\n";
    $strResultadoResumo .= '<th class="infraTh">Membros</th>' . "\n";
    $strResultadoResumo .= '<th class="infraTh">Provimento</th>' . "\n";
    $strResultadoResumo .= '<th class="infraTh">Acompanham</th>' . "\n";
    $strResultadoResumo .= '</tr>' . "\n";
    $strCssTr = '<tr class="infraTrEscura">';
    for ($i = 0; $i<$numRegistrosResumo; $i ++) {

      /** @var ResumoVotacaoDTO $objResumoVotacaoDTO */
      $objResumoVotacaoDTO=$arrObjResumoVotacaoDTO[$i];


      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      $strResultadoResumo .= $strCssTr;


      $strResultadoResumo .= '<td style="text-align: center">' . $objResumoVotacaoDTO->getNumVotos() . '</td>';
      $strResultadoResumo .= '<td style="text-align: center">';
      switch($objResumoVotacaoDTO->getStrTipo()){
        case VotoParteRN::$STA_DIVERGE:
          $strResultadoResumo.='Diverge';
          break;
        case VotoParteRN::$STA_ACOMPANHA:
          $strResultadoResumo.='Relator';
          break;
        case VotoParteRN::$STA_ABSTENCAO:
          $strResultadoResumo.='Absten��o';
          break;
        case VotoParteRN::$STA_AGUARDA_VISTA:
          $strResultadoResumo.='Aguarda Vista';
          break;
        case VotoParteRN::$STA_IMPEDIMENTO:
          $strResultadoResumo.='Impedimento';
          break;
        case VotoParteRN::$STA_PEDIDO_VISTA:
          $strResultadoResumo.='Pedido de Vista';
          break;
        case VotoParteRN::$STA_AUSENTE:
          $strResultadoResumo.='Ausente';
          break;
      }
      $strResultadoResumo .= '</td><td width="25%">';

      foreach ($objResumoVotacaoDTO->getArrMembros() as $strMembro) {
        $strResultadoResumo .= SeiINT::montarItemCelula($strMembro,'');
      }
      $strResultadoResumo .= '</td>';
      $strResultadoResumo .= '<td width="25%">' . $objResumoVotacaoDTO->getStrProvimento();
      if(!InfraString::isBolVazia($objResumoVotacaoDTO->getStrComplemento())){
        $strResultadoResumo .= ' ('.$objResumoVotacaoDTO->getStrComplemento().  ')';
      }
      $strResultadoResumo .= '</td>';


      $strResultadoResumo .= '<td width="25%">';
      foreach ($objResumoVotacaoDTO->getArrAcompanham() as $strMembro) {
        $strResultadoResumo .= SeiINT::montarItemCelula($strMembro,'');
      }
      $strResultadoResumo .= '</td>';
      $strResultadoResumo .= '</tr>' . "\n";
    }
    $strResultadoResumo .= '</table>';
  }
  
  if($bolPermiteFinalizacao) {
    array_unshift($arrComandos,$strComandoFinalizar);
  }
  if (!in_array($staSessao,array(SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_FINALIZADA))) {
    $arrComandos[]= '<button type="submit" accesskey="" id="btnAtualizar" name="btnAtualizar" value="Atualizar"  class="infraButton">Atualizar</button>';
  }

  if(PaginaSEI::getInstance()->getAcaoRetorno()==='resumo_pauta'){
    $arrComandos[] = '<button type="button" accesskey="" id="btnFechar" name="btnFechar" value="Fechar" onclick="window.close();" class="infraButton">Fechar</button>';
  }else {
    $arrComandos[] = '<button type="button" accesskey="" id="btnFechar" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&num_seq='.$numOrdemSequencial.'&id_sessao_julgamento='.$_GET['id_sessao_julgamento_origem'].'&id_item_sessao_julgamento='.$_GET['id_item_sessao_julgamento_origem'].PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento_origem'])).'\';" class="infraButton">Fechar</button>';
  }


  $strItensSelColegiado=ColegiadoINT::montarSelectNomePermitidos('','',$idColegiado);
  $strLinkAjaxSessao=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=sessao_julgamento');
  $strLinkAction=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros);
  $strLinkFinalizar=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=item_sessao_julgamento_finalizar&acao_origem='.$_GET['acao'].'&acao_retorno='.PaginaSEI::getInstance()->getAcaoRetorno().$strParametros);
  $strLinkDesempate=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=julgamento_parte_desempatar&acao_origem='.$_GET['acao'].'&id_julgamento_parte='.$objJulgamentoParteDTO->getNumIdJulgamentoParte().'&id_item_sessao_julgamento='.$_GET['id_item_sessao_julgamento']);
  $strLinkProcesso = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
  $strLinkConverterManual = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=item_sessao_julgamento_manual&converter=1&acao_origem='.$_GET['acao'].'&acao_retorno=sessao_julgamento_consultar'.$strParametros);

  if($objItemSessaoJulgamentoDTO->getBolFaltaQuorum() && $objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getStrStaSituacao()==SessaoJulgamentoRN::$ES_ABERTA){
    $objInfraException=new InfraException();
    $objInfraException->lancarValidacao('N�o possui n�mero de votos v�lidos para fechar quorum.');
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>

  li {list-style-position: inside;}
#lblColegiado {position:absolute;left:0;top:0;width:40%;}
#txtColegiado {position:absolute;left:0;top:38%;width:40%;}

#lblProcesso {position:absolute;left:43%;top:0;width:30%;}
#ancProcesso {position:absolute;left:43%;top:38%;}

#lblParte {position:absolute;left:76%;top:0;width:22%;}
#selParte {position:absolute;left:76%;top:38%;width:22%;}

#lblSessao {position:relative;left:0;top:0;width:22%;}
#selSessao {position:relative;left:0;top:0;width:50%;}

#lblTabelaColegiado {position:absolute;left:0;top:80%;width:99%;}

.infraTrAcessada td *{color:#000;}
.trAcompanha   { background-color: #c6efce;}
.trDiverge     { background-color: #ffc7ce;}
.trEmpate    { background-color: #000000;}
.trEmpate * { color:#fff }
.trDesempate    { background-color: #16609a;}
.trDesempate * { color:#fff }

.trAbstencao   { background-color: #A0A0A0;}
.trPedidoVista { background-color: #000000;}
.trPedidoVista * { color:#fff }

    .sombreado {
        overflow:hidden;
        background-color:white;
        height: 100%;
        padding:10px;
        border-radius: 5px;
        box-shadow: 0 0.125rem 0.5rem rgba(0, 0, 0, .3), 0 0.0625rem 0.125rem rgba(0, 0, 0, .2);
    <? if (PaginaSEI::getInstance()->isBolNavegadorSafariIpad()){?>
        overflow: scroll !important;
        -webkit-overflow-scrolling:touch;
    <? }?>
    }
    .cinza {background-color: #e0e0e0;padding: 1rem;}
    #ifrDocumento{height: 100%;width: 100%}

  #divConteudo {height:22em;width:100%;border-top:1px solid #ccc;padding-top:3px;}
  #divAreaPauta {float:left;width:40%;height:99%;overflow:auto;}
  #divDocumentoConteudo {float:left;width:56%;height:99%;}
  #divRedimensionar {
    float: left;
    display: inline;
    top: 0;
    height: 99%;
    overflow: auto;
    cursor: w-resize;
    width: 5px;
    background: white url(imagens/barra_redimensionamento.gif) repeat-y center;
  }
  #divArvore {
    height: 100%;
    width: 100%;
    position: absolute;
    margin-left: 0;
    left: 0;
    top: 0;
    display: none;
    background-color: yellow;
    opacity: 0.0;
    filter: alpha(opacity=0);
  }
  #lblDispositivo, #lblRessalvas {position:absolute;left:0;top:0;width:99%;font-size: 1.4em;}
  #lblDispositivoTexto, #lblRessalvasTexto {font-size: 1.4em;}
  label.infraLabelTitulo {font-weight:normal;}
  .tdAcessada {background-color: #ffff99 !important;}
  .tdAcessada * {color: #000 !important;}

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
  var nn6=document.getElementById&&!document.all;
  var isdrag=false;
  var x,tx;
  var dobj;
  var divLeftTamanhoInicial = null;
  var divRightTamanhoInicial = null;

  function redimensionarMenu(){
    var wGlobal = document.getElementById('divInfraAreaGlobal').offsetWidth;
    var wMenu = document.getElementById('divInfraAreaTelaE').offsetWidth;
    var wLivre = wGlobal - wMenu;
    if (wLivre > 0){
      document.getElementById("divAreaPauta").style.width = Math.floor(0.40*wLivre) + 'px';
      document.getElementById("divDocumentoConteudo").style.width = Math.floor(0.59*wLivre) + 'px';
    }
    redimensionar();
  }
  function abrirDocumento(link,el){
    var ifr=document.getElementById('ifrDocumento');
    if (ifr==null){
      ifr=parent.document.getElementById('ifrVisualizacao');
    }
    ifr.src=link;
    $('.tdAcessada').removeClass('tdAcessada');
    $(el).closest('td').addClass('tdAcessada');
  }
  function trocarParte(){
    $('input[type=checkbox]:checked').each(function(){this.click()});
   document.getElementById('frmItemSessaoJulgamentoJulgar').submit();
  }
  function redimensionar(){

    if (INFRA_IOS){
      if (document.getElementById("divAreaPauta").offsetHeight < 100){
        document.getElementById("divAreaPauta").style.height = '100px';
      }

      if (document.getElementById("divRedimensionar").offsetHeight < 100){
        document.getElementById("divRedimensionar").style.height ='100px';
      }
    }

    var hTela = infraClientHeight();

    if (hTela > 0){

      var hDivGlobal = document.getElementById('divInfraAreaGlobal').scrollHeight;
      var PosYConteudo = infraOffsetTopTotal(document.getElementById('divConteudo'));

      var hRedimensionamento = 0;

      if (hTela > hDivGlobal){
        hRedimensionamento = hTela - PosYConteudo;
      }else{
        hRedimensionamento = hDivGlobal - PosYConteudo;
      }

      if (hRedimensionamento > 0 && hRedimensionamento < 1920){ //FullHD

        hRedimensionamento = hRedimensionamento - 10;

        document.getElementById('divConteudo').style.height = hRedimensionamento + 'px';
        document.getElementById("divAreaPauta").style.height = hRedimensionamento + 'px';
        document.getElementById('divRedimensionar').style.height = hRedimensionamento + 'px';
        document.getElementById("divDocumentoConteudo").style.height = hRedimensionamento + 'px';

        if (!INFRA_IOS){
          document.getElementById("divAreaPauta").style.width = '40%';
          document.getElementById("divDocumentoConteudo").style.width = '59%';
        }else{
          var wConteudo = document.getElementById('divConteudo').offsetWidth;
          document.getElementById("divAreaPauta").style.width = Math.floor(0.4*wConteudo) + 'px';
          document.getElementById("divDocumentoConteudo").style.width = Math.floor(0.59*wConteudo) + 'px';
        }
      }
    }
  }
  function movemouse(e) {

    if (e == null) { e = window.event }
    if (e.button <= 1 && isdrag){

      var tamanhoRedimensionamento = nn6 ? tx + e.clientX - x : tx + event.clientX - x;

      var tamanhoLeft = 0;
      var tamanhoRight = 0;

      if (tamanhoRedimensionamento > 0){
        tamanhoLeft = (divLeftTamanhoInicial + tamanhoRedimensionamento);
        tamanhoRight = (divRightTamanhoInicial - tamanhoRedimensionamento);
      }else{
        tamanhoLeft = (divLeftTamanhoInicial - Math.abs(tamanhoRedimensionamento));
        tamanhoRight = (divRightTamanhoInicial + Math.abs(tamanhoRedimensionamento));
      }

      if (tamanhoLeft < 0 || tamanhoRight < 0){
        if (tamanhoRedimensionamento > 0){
          tamanhoLeft = 0;
          tamanhoRight = (divLeftTamanhoInicial - divRightTamanhoInicial) ;
        }else{
          tamanhoLeft = (divLeftTamanhoInicial - divRightTamanhoInicial);
          tamanhoRight = 0;
        }
      }

      if(tamanhoLeft > 50 && tamanhoRight > 100){
        document.getElementById("divAreaPauta").style.width = tamanhoLeft + 'px';
        document.getElementById("divDocumentoConteudo").style.width = tamanhoRight + 'px';
      }
    }
    return false;
  }
  function selectmouse(e){

    document.getElementById("divArvore").style.display = 'block';

    var fobj       = nn6 ? e.target : event.srcElement;
    var topelement = nn6 ? "HTML" : "BODY";
    while (fobj.tagName != topelement && fobj.className != "dragme") {
      fobj = nn6 ? fobj.parentNode : fobj.parentElement;
    }

    if (fobj.className=="dragme") {
      isdrag = true;
      dobj = fobj;
      tx = parseInt(dobj.style.left+0);
      x = nn6 ? e.clientX : event.clientX;
      divLeftTamanhoInicial = document.getElementById("divAreaPauta").offsetWidth;
      divRightTamanhoInicial = document.getElementById("divDocumentoConteudo").offsetWidth;

      if (!INFRA_IOS){
        document.onmousemove=movemouse;
      } else {
        document.ontouchmove=movemouse;
      }
      return false;
    }

  }
  function dropmouse(){
    isdrag=false;
    document.getElementById("divArvore").style.display = 'none';
  }
function inicializar(){
    var lnkMenu=document.getElementById("lnkInfraMenuSistema");
    if(lnkMenu) {
      infraAdicionarEvento(document.getElementById("lnkInfraMenuSistema"), 'click', redimensionarMenu);
    }
  infraAdicionarEvento(window,'resize',redimensionar);
  if (!INFRA_IOS){
    document.getElementById("divRedimensionar").onmousedown = selectmouse;
    document.onmouseup = dropmouse;
    document.body.onmouseleave = dropmouse;
  }
  infraFlagResize=true;
  infraOcultarMenuSistemaEsquema();
  infraDesabilitarCamposAreaDados();
  $('label.infraLabelTitulo img').css('visibility','');
  var sel=document.getElementById('selParte');
  if(sel) { sel.disabled=false; }
  document.getElementById('btnFechar').focus();
  infraEfeitoTabelas();
  redimensionar();
  $('.protocoloNormal').eq(0).click();
  <?
    if($_POST['hdnDispositivo']=='N'){
      ?>
  exibe('#divDispositivo');
      <?
    }
  ?>
}
  function exibe(sel,image){
    var img;
    $(sel).toggle();
    if (image) {
      img=$(image);
    } else {
      img=$(sel).prev().find('img');
    }

    if (img.attr('src')=='<?=PaginaSEI::getInstance()->getIconeExibir()?>'){
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeOcultar()?>');
      if (sel=='#divDispositivo'){
        $('#hdnDispositivo').val('N');
      }
    } else {
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeExibir()?>');
      if (sel=='#divDispositivo') {
        $('#hdnDispositivo').val('S');
      }
    }
    //label.innerHTML='&'
    redimensionar();
  }
  function finalizar() {
    document.getElementById('frmItemSessaoJulgamentoJulgar').action='<?=$strLinkFinalizar?>';
    document.getElementById('hdnFinalizar').value='Dispositivo';
    document.getElementById('frmItemSessaoJulgamentoJulgar').submit();
  }
  function acaoSustentacao(){
    infraAbrirJanelaModal('<?=$strLinkSustentacao?>',800,420);
  }
  function trocarSessao(el){
    document.location.href=atob(el.value);
  }
  <? if ($bolVotar){ ?>
  function acaoVotar(id){
    infraAbrirJanelaModal('about:blank',800,500,false);
    document.getElementById('hdnInfraItemId').value=id;
    var form=document.getElementById('frmItemSessaoJulgamentoJulgar');
    form.target='modal-frame';
    form.action='<?=$strLinkVotar?>';
    form.submit();
    form.action='<?=$strLinkAction?>';
    form.target='_self';
  }
  function acaoDesempate(){
    infraAbrirJanelaModal('<?=$strLinkDesempate?>',800,420);
  }
  function acaoVotoMultiplo(){
    if (document.getElementById('hdnInfraItensSelecionados').value==''){
      alert('Nenhum membro selecionado.');
      return;
    }
    infraAbrirJanelaModal('about:blank',800,500,false);
    document.getElementById('hdnInfraItemId').value='';
    var form=document.getElementById('frmItemSessaoJulgamentoJulgar');
    form.target='modal-frame';
    form.action='<?=$strLinkVotar?>';
    form.submit();
    form.action='<?=$strLinkAction?>';
    form.target='_self';
  }
  function acaoJulgamentoManual(){
    if (!confirm("ATEN��O: esta opera��o n�o poder� ser desfeita.\nConfirma convers�o para julgamento manual?")){
      return;
    }
    document.getElementById('hdnInfraItemId').value='';
    var form=document.getElementById('frmItemSessaoJulgamentoJulgar');
    form.action='<?=$strLinkConverterManual?>';
    form.submit();
  }
  <? } ?>
  <? if ($bolEditarRessalvas){ ?>
  function acaoEditarRessalva(link){
    infraAbrirJanelaModal(link,800,500);
  }

  <? } ?>

function onSubmitForm() {
  return true;
}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmItemSessaoJulgamentoJulgar" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>

  <label id="lblColegiado" for="txtColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <input type="text" id="txtColegiado" name="txtColegiado" class="infraText" value="<?=PaginaSEI::tratarHTML($objSessaoJulgamentoDTO->getStrNomeColegiado()); ?>" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>

  <label id="lblProcesso" for="ancProcesso" accesskey="" class="infraLabelObrigatorio">Processo:</label>
  <a id="ancProcesso" href="<?=$strLinkProcesso?>" target="_blank" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" alt="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>" title="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>"><?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?></a>

  <input type="hidden" id="hdnFinalizar" name="hdnFinalizar" value="" />

  <? if ($qtdPartes>1) {?>

  <label id="lblParte" for="selParte" accesskey="" class="infraLabelObrigatorio">Item:</label>
  <select id="selParte" name="selParte"  onchange="trocarParte();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
    <?=$strItensSelParte?>
  </select>


  <?}
  PaginaSEI::getInstance()->fecharAreaDados();
  if ($strDispositivo!=null){
    PaginaSEI::getInstance()->abrirAreaDados('3.3em');
    ?>
    <label id="lblDispositivo" class="infraLabelTitulo"><img onclick="exibe('#divDispositivo',this);" src="<?=PaginaSEI::getInstance()->getIconeExibir()?>" />&nbsp;Dispositivo</label>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?>
      <div id="divDispositivo">
    <label id="lblDispositivoTexto" class="infraLabelOpcional"> <?=nl2br(PaginaSEI::tratarHTML($strDispositivo));?></label>

    <br />
    <br />
      </div>
    <input type="hidden" id="hdnDispositivo" name="hdnDispositivo" value="<?=$_POST['hdnDispositivo'];?>">
    <?
  }
  if ($strRessalvas!=null){
    PaginaSEI::getInstance()->abrirAreaDados('3.3em');
    ?>
    <label id="lblRessalvas" class="infraLabelTitulo"><img onclick="exibe('#divRessalvas',this);" src="<?=PaginaSEI::getInstance()->getIconeExibir()?>" />&nbsp;Ressalvas</label>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?>
    <div id="divRessalvas">
      <label id="lblRessalvasTexto" class="infraLabelOpcional"> <?=nl2br(PaginaSEI::tratarHTML($strRessalvas));?></label>

      <br />
      <br />
    </div>
    <?
  }
  ?>
  <div id="divConteudo">
    <div id="divArvore"></div>
    <div id="divAreaPauta">
      <?
      if ($strItensSessoes!='') {?>

        <label id="lblSessao" for="selSessao" accesskey="" class="infraLabelObrigatorio">Data da Sess�o:</label>
        <select id="selSessao" name="selSessao"  onchange="trocarSessao(this);" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
          <?=$strItensSessoes?>
        </select>


      <?}
  PaginaSEI::getInstance()->montarAreaTabela($strResultadoMembros, $numRegistrosMembros);
      ?>
      <br>
      <?
      if($numRegistrosResumo) {
        PaginaSEI::getInstance()->montarAreaTabela($strResultadoResumo, $numRegistrosResumo);
      }
  ?></div>
    <div id="divRedimensionar" class="dragme"></div>
    <div id="divDocumentoConteudo" class="cinza">
      <iframe id="ifrDocumento" frameborder="0" class="sombreado" src=""></iframe>
    </div>
</div>
</form>
<?
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>

<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>