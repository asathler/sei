<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 08/07/2016 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.38.0
*
* Vers�o no SVN: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('motivo_ausencia_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objMotivoAusenciaDTO = new MotivoAusenciaDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'motivo_ausencia_cadastrar':
      $strTitulo = 'Novo Motivo de Aus�ncia';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarMotivoAusencia" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objMotivoAusenciaDTO->setNumIdMotivoAusencia(null);
      $objMotivoAusenciaDTO->setStrDescricao($_POST['txtDescricao']);
      $objMotivoAusenciaDTO->setStrSinAtivo('S');

      if (isset($_POST['sbmCadastrarMotivoAusencia'])) {
        try{
          $objMotivoAusenciaRN = new MotivoAusenciaRN();
          $objMotivoAusenciaDTO = $objMotivoAusenciaRN->cadastrar($objMotivoAusenciaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo de Aus�ncia "'.$objMotivoAusenciaDTO->getStrDescricao().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_motivo_ausencia='.$objMotivoAusenciaDTO->getNumIdMotivoAusencia().PaginaSEI::getInstance()->montarAncora($objMotivoAusenciaDTO->getNumIdMotivoAusencia())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_ausencia_alterar':
      $strTitulo = 'Alterar Motivo de Aus�ncia';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarMotivoAusencia" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_motivo_ausencia'])){
        $objMotivoAusenciaDTO->setNumIdMotivoAusencia($_GET['id_motivo_ausencia']);
        $objMotivoAusenciaDTO->retTodos();
        $objMotivoAusenciaRN = new MotivoAusenciaRN();
        $objMotivoAusenciaDTO = $objMotivoAusenciaRN->consultar($objMotivoAusenciaDTO);
        if ($objMotivoAusenciaDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objMotivoAusenciaDTO->setNumIdMotivoAusencia($_POST['hdnIdMotivoAusencia']);
        $objMotivoAusenciaDTO->setStrDescricao($_POST['txtDescricao']);
        $objMotivoAusenciaDTO->setStrSinAtivo('S');
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoAusenciaDTO->getNumIdMotivoAusencia())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarMotivoAusencia'])) {
        try{
          $objMotivoAusenciaRN = new MotivoAusenciaRN();
          $objMotivoAusenciaRN->alterar($objMotivoAusenciaDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo de Aus�ncia "'.$objMotivoAusenciaDTO->getStrDescricao().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoAusenciaDTO->getNumIdMotivoAusencia())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_ausencia_consultar':
      $strTitulo = 'Consultar Motivo de Aus�ncia';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_motivo_ausencia'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objMotivoAusenciaDTO->setNumIdMotivoAusencia($_GET['id_motivo_ausencia']);
      $objMotivoAusenciaDTO->setBolExclusaoLogica(false);
      $objMotivoAusenciaDTO->retTodos();
      $objMotivoAusenciaRN = new MotivoAusenciaRN();
      $objMotivoAusenciaDTO = $objMotivoAusenciaRN->consultar($objMotivoAusenciaDTO);
      if ($objMotivoAusenciaDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>
#lblDescricao {position:absolute;left:0%;top:0%;width:80%;}
#txtDescricao {position:absolute;left:0%;top:40%;width:80%;}

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='motivo_ausencia_cadastrar'){
    document.getElementById('txtDescricao').focus();
  } else if ('<?=$_GET['acao']?>'=='motivo_ausencia_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
}

function validarCadastro() {
  if (infraTrim(document.getElementById('txtDescricao').value)=='') {
    alert('Informe a descri��o.');
    document.getElementById('txtDescricao').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmMotivoAusenciaCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblDescricao" for="txtDescricao" accesskey="" class="infraLabelObrigatorio">Descri��o:</label>
  <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="<?=PaginaSEI::tratarHTML($objMotivoAusenciaDTO->getStrDescricao());?>" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
<?
PaginaSEI::getInstance()->fecharAreaDados();
?>
  <input type="hidden" id="hdnIdMotivoAusencia" name="hdnIdMotivoAusencia" value="<?=$objMotivoAusenciaDTO->getNumIdMotivoAusencia();?>" />
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
//  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>