<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class EleicaoINT extends InfraINT {

  public static function montarSelectIdentificacao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdItemSessaoJulgamento=''){
    $objEleicaoDTO = new EleicaoDTO();
    $objEleicaoDTO->retNumIdEleicao();
    $objEleicaoDTO->retStrIdentificacao();

    if ($numIdItemSessaoJulgamento!==''){
      $objEleicaoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    }

    $objEleicaoDTO->setOrdStrIdentificacao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objEleicaoRN = new EleicaoRN();
    $arrObjEleicaoDTO = $objEleicaoRN->listar($objEleicaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjEleicaoDTO, 'IdEleicao', 'Identificacao');
  }

  public static function montarSelectStaSituacao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objEleicaoRN = new EleicaoRN();

    $arrObjEleicaoEleicaoDTO = InfraArray::indexarArrInfraDTO($objEleicaoRN->listarValoresSituacao(),'StaSituacao');

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjEleicaoEleicaoDTO, 'StaSituacao', 'Descricao');

  }

  public static function montarTooltipVotacao($arrObjColegiadoComposicaoDTO, $strTitulo){
    $txt = '';
    $numRegistros = InfraArray::contar($arrObjColegiadoComposicaoDTO);
    if ($numRegistros == 0){
      $txt = 'Nenhum registro encontrado.';
    }else{
      foreach($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO){
        if ($txt != ''){
          $txt .= "\n";
        }
        $txt .= $objColegiadoComposicaoDTO->getStrNomeUsuario();
      }
    }
    return '<a href="#" '.PaginaSEI::montarTitleTooltip($txt, $strTitulo) . ' class="ancoraPadraoAzul">'.$numRegistros.'</a>';
  }
}
