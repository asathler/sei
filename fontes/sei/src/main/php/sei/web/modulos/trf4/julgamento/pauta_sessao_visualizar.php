<?
/*
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/09/2008 - criado por marcio_db
*
*
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(false);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEIExterna::getInstance()->validarLink();


  PaginaSEIExterna::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SEM_MENU);

  SessaoSEIExterna::getInstance()->validarPermissao($_GET['acao']);

  $idSessaoJulgamento=$_GET['id_sessao_julgamento'];

  $strTitulo='Pauta de Julgamento';
  switch($_GET['acao']){

    case 'pauta_sessao_visualizar':

      //validar permiss�o...
      $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
      $objSessaoJulgamentoRN=new SessaoJulgamentoRN();

      $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
      $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');

      /** @var SessaoJulgamentoDTO $objSessaoJulgamentoDTO */
      $objSessaoJulgamentoDTO=$objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);


      if($objSessaoJulgamentoDTO==null){
        //n�o encontrado
        throw new InfraException('Sess�o de julgamento n�o foi encontrada.');
      }
      $staSituacao=$objSessaoJulgamentoDTO->getStrStaSituacao();
      if(in_array($staSituacao,array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA,SessaoJulgamentoRN::$ES_PREVISTA))){
        throw new InfraException('Situa��o da Sess�o de Julgamento n�o permite acesso � pauta.');
      }
      $objInfraParametro=new InfraParametro(BancoSEI::getInstance());

      if($staSituacao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA){
        $dblIdDocumentoPauta=$objSessaoJulgamentoDTO->getDblIdDocumentoPauta();
        if($dblIdDocumentoPauta==null){
          throw new InfraException('Pauta da Sess�o de Julgamento n�o foi disponibilizada.');
        }

        $criterio=$objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_EVENTO_PAUTA_FECHADA,0);
        $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
        $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_DOCUMENTOS);
        $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_TODOS);
        $objPesquisaProtocoloDTO->setDblIdProtocolo($dblIdDocumentoPauta);

        $objProtocoloRN = new ProtocoloRN();
        /** @var ProtocoloDTO[] $arrObjProtocoloDTO */
        $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);

        $objProtocoloDTO=$arrObjProtocoloDTO[0];

        if(($criterio==0 && $objProtocoloDTO->getStrSinAssinado()=='N' )||($criterio==1 && $objProtocoloDTO->getStrSinPublicado()=='N')){
          throw new InfraException('Pauta da Sess�o de Julgamento n�o foi disponibilizada.');
        }

      }

     $objEditorRN=new EditorRN();


      $strResultado = $objSessaoJulgamentoRN->gerarTabelaPauta($objSessaoJulgamentoDTO);
//      InfraPagina::montarHeaderDownload(null, null, 'Content-Type: text/html; charset=iso-8859-1');

      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

}catch(Exception $e){
  PaginaSEIExterna::getInstance()->processarExcecao($e);
}

PaginaSEIExterna::getInstance()->montarDocType();
PaginaSEIExterna::getInstance()->abrirHtml();
PaginaSEIExterna::getInstance()->abrirHead();
PaginaSEIExterna::getInstance()->montarMeta();
PaginaSEIExterna::getInstance()->montarTitle(PaginaSEIExterna::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEIExterna::getInstance()->montarStyle();
PaginaSEIExterna::getInstance()->abrirStyle();
if(0){?><style type="text/css" ><?}
?>

label.infraLabelTitulo {position:absolute;left:0;top:0;width:99%;font-weight:normal;}
    .resumo {float:right;color:white;font-size:0.9em;padding-right: 3px;position: relative;top:2px;}
<?
if(0){?></style><?}
PaginaSEIExterna::getInstance()->fecharStyle();
PaginaSEIExterna::getInstance()->montarJavaScript();
PaginaSEIExterna::getInstance()->abrirJavaScript();
if(0){?><script><?}
  ?>

  function inicializar(){
    infraEfeitoTabelas();
  }

  function onSubmitForm() {
    return validarForm();
  }

  function validarForm() {
    return true;
  }

  <?
  if(0){?></script><?}
PaginaSEIExterna::getInstance()->fecharJavaScript();
PaginaSEIExterna::getInstance()->fecharHead();
PaginaSEIExterna::getInstance()->abrirBody($strTitulo,'');

 echo $strResultado;
  //PaginaSEIExterna::getInstance()->montarAreaDebug();
  //    PaginaSEIExterna::getInstance()->montarBarraComandosInferior($arrComandos);
PaginaSEIExterna::getInstance()->fecharBody();
PaginaSEIExterna::getInstance()->fecharHtml();
?>