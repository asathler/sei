<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class ParteProcedimentoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarDblIdProcedimento(ParteProcedimentoDTO $objParteProcedimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objParteProcedimentoDTO->getDblIdProcedimento())){
      $objInfraException->adicionarValidacao('Processo n�o informado.');
    }
  }

  private function validarNumIdContato(ParteProcedimentoDTO $objParteProcedimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objParteProcedimentoDTO->getNumIdContato())){
      $objInfraException->adicionarValidacao('Contato n�o informado.');
    }
  }

  private function validarNumIdQualificacaoParte(ParteProcedimentoDTO $objParteProcedimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objParteProcedimentoDTO->getNumIdQualificacaoParte())){
      $objInfraException->adicionarValidacao('Qualifica��o n�o informada.');
    }
  }

  private function validarNumIdUnidade(ParteProcedimentoDTO $objParteProcedimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objParteProcedimentoDTO->getNumIdUnidade())){
      $objInfraException->adicionarValidacao('Unidade n�o informada.');
    }
  }

  protected function cadastrarControlado(ParteProcedimentoDTO $objParteProcedimentoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('parte_procedimento_cadastrar',__METHOD__,$objParteProcedimentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarDblIdProcedimento($objParteProcedimentoDTO, $objInfraException);
      $this->validarNumIdContato($objParteProcedimentoDTO, $objInfraException);
      $this->validarNumIdQualificacaoParte($objParteProcedimentoDTO, $objInfraException);
      $this->validarNumIdUnidade($objParteProcedimentoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objParteProcedimentoBD = new ParteProcedimentoBD($this->getObjInfraIBanco());
      $ret = $objParteProcedimentoBD->cadastrar($objParteProcedimentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Parte.',$e);
    }
  }

  protected function alterarControlado(ParteProcedimentoDTO $objParteProcedimentoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('parte_procedimento_alterar',__METHOD__,$objParteProcedimentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objParteProcedimentoDTO->isSetDblIdProcedimento()){
        $this->validarDblIdProcedimento($objParteProcedimentoDTO, $objInfraException);
      }
      if ($objParteProcedimentoDTO->isSetNumIdContato()){
        $this->validarNumIdContato($objParteProcedimentoDTO, $objInfraException);
      }
      if ($objParteProcedimentoDTO->isSetNumIdQualificacaoParte()){
        $this->validarNumIdQualificacaoParte($objParteProcedimentoDTO, $objInfraException);
      }
      if ($objParteProcedimentoDTO->isSetNumIdUnidade()){
        $this->validarNumIdUnidade($objParteProcedimentoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objParteProcedimentoBD = new ParteProcedimentoBD($this->getObjInfraIBanco());
      $objParteProcedimentoBD->alterar($objParteProcedimentoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Parte.',$e);
    }
  }

  protected function excluirControlado($arrObjParteProcedimentoDTO){
    try {

      //Valida Permissao
      //permite que usu�rio sem acesso ao m�dulo possa excluir processo autuado
      if(SessaoSEI::getInstance()->verificarPermissao('parte_procedimento_excluir')){
        SessaoSEI::getInstance()->validarAuditarPermissao('parte_procedimento_excluir',__METHOD__,$arrObjParteProcedimentoDTO);
      }
      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objParteProcedimentoBD = new ParteProcedimentoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjParteProcedimentoDTO); $i< $iMax; $i++){
        $objParteProcedimentoBD->excluir($arrObjParteProcedimentoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Parte.',$e);
    }
  }

  protected function consultarConectado(ParteProcedimentoDTO $objParteProcedimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('parte_procedimento_consultar',__METHOD__,$objParteProcedimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objParteProcedimentoBD = new ParteProcedimentoBD($this->getObjInfraIBanco());
      $ret = $objParteProcedimentoBD->consultar($objParteProcedimentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Parte.',$e);
    }
  }

  protected function listarConectado(ParteProcedimentoDTO $objParteProcedimentoDTO) {
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('parte_procedimento_listar',__METHOD__,$objParteProcedimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objParteProcedimentoBD = new ParteProcedimentoBD($this->getObjInfraIBanco());
      $ret = $objParteProcedimentoBD->listar($objParteProcedimentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Partes.',$e);
    }
  }

  protected function contarConectado(ParteProcedimentoDTO $objParteProcedimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('parte_procedimento_listar',__METHOD__,$objParteProcedimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objParteProcedimentoBD = new ParteProcedimentoBD($this->getObjInfraIBanco());
      $ret = $objParteProcedimentoBD->contar($objParteProcedimentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Partes.',$e);
    }
  }
}
?>