<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 19/05/2023 - criado por sabino.colab
 *
 * Vers�o do Gerador de C�digo: 1.43.2
 */

try {
    require_once dirname(__FILE__) . '/../../SEI.php';

    session_start();

    //////////////////////////////////////////////////////////////////////////////
    //InfraDebug::getInstance()->setBolLigado(false);
    //InfraDebug::getInstance()->setBolDebugInfra(true);
    //InfraDebug::getInstance()->limpar();
    //////////////////////////////////////////////////////////////////////////////

    SessaoSEI::getInstance()->validarLink();

    PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);

    PaginaSEI::getInstance()->verificarSelecao('md_ia_avaliacao_selecionar');

    SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

    $objMdIaAvaliacaoDTO = new MdIaAvaliacaoDTO();

    $strDesabilitar = '';

    $arrComandos = array();

    $objProcedimentoRN = new ProcedimentoRN();

    $objMdIaAvaliacaoRN = new MdIaAvaliacaoRN();

    if($_POST['hdnIdProcedimento'] != "") {
        $idProcedimento = $_POST['hdnIdProcedimento'];
    } else {
        $idProcedimento = $_GET['id_procedimento'];
    }
    $objProcedimentoBaseDTO = new ProcedimentoDTO();
    $objProcedimentoBaseDTO->setDblIdProcedimento($idProcedimento);
    $objProcedimentoBaseDTO->retStrProtocoloProcedimentoFormatado();
    $objProcedimentoBaseDTO->retStrNomeTipoProcedimento();

    $objProcedimentoBaseDTO = $objProcedimentoRN->consultarRN0201($objProcedimentoBaseDTO);

    $itensSimilares = $objMdIaAvaliacaoRN->conexaoApiRecomendacaoSimilaridade(array($_GET['id_procedimento'], $objProcedimentoBaseDTO->getStrProtocoloProcedimentoFormatado()));

    switch ($_GET['acao']) {
        case 'md_ia_avaliacao_cadastrar':
            $strTitulo = 'Avalia��o de Similaridade entre Processos - SEI IA';
            $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=arvore_visualizar&acao_origem='.$_GET['acao'].'&arvore=1&id_protocolo=' . $_GET['id_procedimento']) . '\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
            $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarMdIaAvaliacao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>ubmeter Avalia��o</button>';

            if (isset($_POST['sbmCadastrarMdIaAvaliacao'])) {
                try {

                    $arrayObjetosAvaliacao = [];
                    
                    for ($i = 1; $i <= $_POST["hdnNumeroElementos"]; $i++) {
                        $arrayMdIaAvaliacaoDTO['id_prot_base'] = preg_replace('/[^0-9]/', '', $objProcedimentoBaseDTO->getStrProtocoloProcedimentoFormatado());
                        $arrayMdIaAvaliacaoDTO['desc_prot_base'] = utf8_encode($objProcedimentoBaseDTO->getStrNomeTipoProcedimento());
                        $arrayMdIaAvaliacaoDTO['id_prot_recommend'] = preg_replace('/[^0-9]/', '', $_POST['hdnIdProcessRecomend' . $i]);
                        $arrayMdIaAvaliacaoDTO['desc_prot_recommend'] = utf8_encode($_POST['hdnDescProcessRecomend' . $i]);
                        $arrayMdIaAvaliacaoDTO['like_flag'] = $_POST['hdnAproved' . $i];
                        $arrayMdIaAvaliacaoDTO['racional'] = utf8_encode($_POST['txtRacional' . $i]);
                        $arrayMdIaAvaliacaoDTO['similarity'] = preg_replace('/(?<=\d),+(?=\d)/', '.', $_POST['hdnSimilarity' . $i]);
                        $arrayMdIaAvaliacaoDTO['ranking'] = $_POST['hdnIdRanking' . $i];
                        $arrayMdIaAvaliacaoDTO['sugesty'] = utf8_encode($_POST['txaSugestoes']);
                        $arrayMdIaAvaliacaoDTO['api_version'] = "0.0.1";
                        $arrayMdIaAvaliacaoDTO['app_version'] = "0.0.1";
                        $arrayMdIaAvaliacaoDTO['api_recommender_url'] = "/process-recommenders/weighted-mlt-recommender/recommendations/{id_protocolo}";
                        $arrayMdIaAvaliacaoDTO['id_username'] = SessaoSEI::getInstance()->getNumIdUsuario();
                        $arrayMdIaAvaliacaoDTO['valid'] = "true";
                        $arrayObjetosAvaliacao[] = $arrayMdIaAvaliacaoDTO;
                    }
                    $objMdIaAvaliacaoDTO = $objMdIaAvaliacaoRN->submeteAvaliacao(json_encode($arrayObjetosAvaliacao));

                    PaginaSEI::getInstance()->adicionarMensagem('Avalia��o cadastrada com sucesso.');
                    header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=arvore_visualizar&acao_origem=' . $_GET['acao'] . '&id_procedimento=' . $_POST['hdnIdProcedimento']));
                    die;
                } catch (Exception $e) {
                    PaginaSEI::getInstance()->processarExcecao($e);
                }
            }
            break;

        default:
            throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
    }

} catch (Exception $e) {
    PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema() . ' - ' . $strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
include_once('md_ia_avaliacao_cadastro_css.php');
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo, 'onload="inicializar();"');
?>
    <form id="frmMdIaAvaliacaoCadastro" method="post" onsubmit="return OnSubmitForm();"
          action="<?= SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao'] . '&acao_origem=' . $_GET['acao']) ?>">
        <?
        if ($itensSimilares->recommendation != "" && $itensSimilares->recommendation != "404") {
            PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
        }
        //PaginaSEI::getInstance()->montarAreaValidacao();
        ?>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
                <fieldset class="infraFieldset form-control mb-3 py-3" style="height: auto">
                    <legend class="infraLegend">Orienta��es Gerais</legend>
                    <ol class="m-0 pl-4">
                        <li class="requisito">Esta � a primeira funcionalidade do SEI IA, que, utilizando t�cnicas de intelig�ncia artificial, apresenta recomenda��o de processos similares a partir do conte�do dos documentos e metadados. Novas funcionalidades j� est�o em desenvolvimento para o SEI IA e ser�o divulgadas em momento oportuno.
                        </li>
                        <li class="requisito">A presente tela serve para ajudar na valida��o e na melhoria do modelo de intelig�ncia artificial para a recomenda��o de processos similares.
                        </li>
                        <li class="requisito">Uma das fases cruciais para a constru��o de bons modelos de Intelig�ncia Artificial � treinamento e feedback dos usu�rios ao avaliar resultados. Assim, precisamos muito da colabora��o dos servidores para validar o m�ximo de processos, em termos de relev�ncia das recomenda��es dos processos similares ao presente processo, a partir do conte�do dos documentos e metadados.
                        </li>
                        <li class="requisito">A avalia��o deve confirmar os processos similares (polegar para cima) e, igualmente importante, marcar os processos n�o similares (polegar para baixo). Caso a ordem de similaridade dos processos listados possa ser melhorada, os avaliadores ainda devem mudar o Ranking na primeira coluna.
                        </li>
                        <li class="requisito">Na coluna Racional devem ser descritas as justificativas para cada avalia��o por processo, sendo mais importante detalhar os casos em que as recomenda��es dos processos foram negativas. Os avaliadores tamb�m devem escrever um texto no campo "Sugest�es", propondo eventuais melhorias sobre o conjunto de processos recomendados ou sugerindo processos que deveriam ter sido listados como similares e n�o foram.
                        </li>
                    </ol>
                </fieldset>
                <br/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?php
                if ($itensSimilares->recommendation != "" && $itensSimilares != "404") {
                ?>
                <table class="infraTable " id="tabela_ordenada">
                    <thead>
                    <tr>
                        <th class="infraTh" width="5%">Ranking</th>
                        <th class="infraTh" width="14%">Processo</th>
                        <th class="infraTh" width="35%">Tipo de Processo</th>
                        <th class="infraTh" width="6%">Avalia��o</th>
                        <th class="infraTh">Racional</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $contador = 0;

                    foreach ($itensSimilares->recommendation as $arrayItemSimilar) {

                        $objProcedimentoDTO = new ProcedimentoDTO();

                        $objProcedimentoDTO->setDblIdProcedimento($arrayItemSimilar->id_protocolo);
                        $objProcedimentoDTO->retStrProtocoloProcedimentoFormatado();
                        $objProcedimentoDTO->retStrNomeTipoProcedimento();

                        $objProcedimentoDTO = $objProcedimentoRN->consultarRN0201($objProcedimentoDTO);
                        $contador++;
                        ?>
                        <tr data-index="<?= $contador ?>" data-position="<?= $contador ?>">
                            <td class="idRanking">
                                <?= $contador ?><i class="gg-arrows-v mr-2"></i>
                                <input type="hidden" id="hdnIdRanking<?= $contador ?>"
                                       name="hdnIdRanking<?= $contador ?>" value="<?= $contador ?>"/>
                            </td>
                            <td>
                                <a target="_blank"
                                   href="<?= SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&id_procedimento=' . $arrayItemSimilar->id_protocolo) ?> "><?= $objProcedimentoDTO->getStrProtocoloProcedimentoFormatado() ?></a>
                                <input type="hidden" id="hdnIdProcessRecomend<?= $contador ?>"
                                       name="hdnIdProcessRecomend<?= $contador ?>"
                                       value="<?= $objProcedimentoDTO->getStrProtocoloProcedimentoFormatado() ?>"/>
                            </td>
                            <td>
                                <?= $objProcedimentoDTO->getStrNomeTipoProcedimento() ?>
                                <input type="hidden" id="hdnDescProcessRecomend<?= $contador ?>"
                                       name="hdnDescProcessRecomend<?= $contador ?>"
                                       value="<?= $objProcedimentoDTO->getStrNomeTipoProcedimento() ?>"/>
                            </td>
                            <td class="text-center">
                                <div class="rounded-pill p-2 d-flex justify-content-around align-items-center"
                                     style="background: #EEE;">
                                    <span class="btn_thumbs up bubbly-button"></span><span style="color:#BBB">|</span>
                                    <span class="btn_thumbs down bubbly-button"></span>
                                    <input type="hidden" class="hdnAproved" id="hdnAproved<?= $contador ?>"
                                           name="hdnAproved<?= $contador ?>" value=""/>
                                </div>
                            </td>
                            <td>
                                <input type="text" class="form-control" id="txtRacional<?= $contador ?>"
                                       name="txtRacional<?= $contador ?>" maxlength="250"/>
                                <input type="hidden" id="hdnSimilarity<?= $contador ?>"
                                       name="hdnSimilarity<?= $contador ?>" value="<?= $arrayItemSimilar->score ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    } elseif($itensSimilares != "404") {
                    ?>
                        <div class="alert alert-danger">
                            <label class="infraLabelOpcional">
                                O recurso espec�fico do SEI IA est� indispon�vel no momento.
                            </label>
                        </div>
                    <?php
                    } else {
                        ?>
                        <div class="alert alert-warning">
                            <label class="infraLabelOpcional">
                                Este Processo ainda est� pendente de processamento pelo SEI IA.
                            </label>
                        </div>
                        <?php
                    }
                    ?>
                    <input type="hidden" id="hdnNumeroElementos" name="hdnNumeroElementos" value="<?= $contador ?>"/>
                    <input type="hidden" id="hdnIdProcedimento" name="hdnIdProcedimento"
                           value="<?= $_GET['id_procedimento'] ?>"/>

                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <?php
        if ($itensSimilares->recommendation != "") {
            ?>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <label for="txaSugestoes" class="infraLabelOpcional">
                        Sugest�es
                    </label>
                    <label for="txaSugestoes" class="infraLabelOpcional">
                    </label>
                    <textarea class="infraTextArea form-control" name="txaSugestoes" id="txaSugestoes" rows="3"
                              cols="150"
                              onkeypress="return infraMascaraTexto(this, event, 500);"
                              maxlength="500"
                              tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"></textarea>
                </div>
            </div>
            <?
            //PaginaSEI::getInstance()->montarAreaDebug();
            PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
        }
        ?>
    </form>
<?
require_once "md_ia_avaliacao_cadastro_js.php";
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
