<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class DistribuicaoINT extends InfraINT {

  public static function montarSelectIdDistribuicao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $dblIdProcedimento='', $numIdColegiadoVersao='', $numIdUsuarioRelator='', $numIdUsuario='', $numIdUnidade=''){
    $objDistribuicaoDTO = new DistribuicaoDTO();
    $objDistribuicaoDTO->retNumIdDistribuicao();
    $objDistribuicaoDTO->retNumIdDistribuicao();

    if ($dblIdProcedimento!==''){
      $objDistribuicaoDTO->setDblIdProcedimento($dblIdProcedimento);
    }

    if ($numIdColegiadoVersao!==''){
      $objDistribuicaoDTO->setNumIdColegiadoVersao($numIdColegiadoVersao);
    }

    if ($numIdUsuarioRelator!==''){
      $objDistribuicaoDTO->setNumIdUsuarioRelator($numIdUsuarioRelator);
    }

    if ($numIdUsuario!==''){
      $objDistribuicaoDTO->setNumIdUsuario($numIdUsuario);
    }

    if ($numIdUnidade!==''){
      $objDistribuicaoDTO->setNumIdUnidade($numIdUnidade);
    }

    $objDistribuicaoDTO->setOrdNumIdDistribuicao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objDistribuicaoRN = new DistribuicaoRN();
    $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjDistribuicaoDTO, 'IdDistribuicao', 'IdDistribuicao');
  }

  public static function montarSelectStaDistribuicao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objDistribuicaoRN = new DistribuicaoRN();

    $arrObjDistribuicaoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjDistribuicaoDistribuicaoDTO, 'StaDistribuicao', 'Descricao');

  }
  public static function montarSelectDistribuicaoCancelamento($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $dblIdProcedimento=''){
    $objDistribuicaoDTO = new DistribuicaoDTO();
    $objDistribuicaoDTO->retNumIdDistribuicao();
    $objDistribuicaoDTO->retNumIdDistribuicao();
    $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
    $objDistribuicaoDTO->retStrNomeColegiado();
    $objDistribuicaoDTO->retDthDistribuicao();
    $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
    $objDistribuicaoDTO->setNumIdUnidadeResponsavelColegiado(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objDistribuicaoDTO->setStrStaDistribuicao(array(DistribuicaoRN::$STA_CANCELADO,DistribuicaoRN::$STA_PAUTADO,DistribuicaoRN::$STA_EM_MESA,DistribuicaoRN::$STA_PARA_REFERENDO,DistribuicaoRN::$STA_JULGADO,DistribuicaoRN::$STA_PEDIDO_VISTA),InfraDTO::$OPER_NOT_IN);


    if ($dblIdProcedimento!==''){
      $objDistribuicaoDTO->setDblIdProcedimento($dblIdProcedimento);
    }
    $objDistribuicaoDTO->setOrdDthDistribuicao(InfraDTO::$TIPO_ORDENACAO_DESC);

    $objDistribuicaoRN = new DistribuicaoRN();
    $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);

    foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTO){
      $objDistribuicaoDTO->setStrNomeColegiado($objDistribuicaoDTO->getDthDistribuicao().' - '.$objDistribuicaoDTO->getStrNomeColegiado());
    }

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjDistribuicaoDTO, 'IdDistribuicao', 'NomeColegiado');
  }

  public static function montarSelectNomeTipoProcedimento($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdColegiado=null){

    $objDistribuicaoDTO=new DistribuicaoDTO();
    if($numIdColegiado!=null){
      $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
    }
    $objDistribuicaoDTO->retNumIdTipoProcedimento();
    $objDistribuicaoDTO->retStrNomeTipoProcedimento();
    $objDistribuicaoDTO->setDistinct(true);

    $objDistribuicaoRN=new DistribuicaoRN();
    $arrObjDistribuicaoDTO=$objDistribuicaoRN->listar($objDistribuicaoDTO);


    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjDistribuicaoDTO, 'IdTipoProcedimento', 'NomeTipoProcedimento');
  }
}
?>