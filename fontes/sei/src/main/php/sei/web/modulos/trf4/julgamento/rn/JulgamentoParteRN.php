<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class JulgamentoParteRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdItemSessaoJulgamento(JulgamentoParteDTO $objJulgamentoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Item da Sess�o n�o informado.');
    }
  }

  private function validarStrDescricao(JulgamentoParteDTO $objJulgamentoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objJulgamentoParteDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao('Descri��o n�o informada.');
    }else{
      $objJulgamentoParteDTO->setStrDescricao(trim($objJulgamentoParteDTO->getStrDescricao()));

      if (strlen($objJulgamentoParteDTO->getStrDescricao())>4000){
        $objInfraException->adicionarValidacao('Descri��o possui tamanho superior a 4000 caracteres.');
      }
    }
  }

  private function validarNumIdUsuarioDesempate(JulgamentoParteDTO $objJulgamentoParteDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objJulgamentoParteDTO->getNumIdUsuarioDesempate())){
      $objJulgamentoParteDTO->setNumIdUsuarioDesempate(null);
    }else{

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);

      $arrObjJulgamentoParteDTO=InfraArray::indexarArrInfraDTO($objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO(),'IdJulgamentoParte');
      if (!isset($arrObjJulgamentoParteDTO[$objJulgamentoParteDTO->getNumIdJulgamentoParte()])){
        $objInfraException->adicionarValidacao('Item do Julgamento n�o localizado');
        return;
      }
      $objJulgamentoParteDTO2=$arrObjJulgamentoParteDTO[$objJulgamentoParteDTO->getNumIdJulgamentoParte()];

      if ($objJulgamentoParteDTO2->getBolPossuiEmpate()){
        $arrUsuariosEmpate=$objJulgamentoParteDTO2->getArrUsuariosEmpate();
        if (InfraArray::contar($arrUsuariosEmpate)<=1){
          $objInfraException->adicionarValidacao('Erro obtendo votos empatados');
        }
      } else {
        $objInfraException->adicionarValidacao('Item do Julgamento n�o possui empate');
      }
    }
  }

  protected function cadastrarControlado(JulgamentoParteDTO $objJulgamentoParteDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_parte_cadastrar',__METHOD__,$objJulgamentoParteDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdItemSessaoJulgamento($objJulgamentoParteDTO, $objInfraException);
      $this->validarStrDescricao($objJulgamentoParteDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objJulgamentoParteBD = new JulgamentoParteBD($this->getObjInfraIBanco());
      $ret = $objJulgamentoParteBD->cadastrar($objJulgamentoParteDTO);

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->setStrDispositivo(null);
      $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Parte.',$e);
    }
  }

  protected function alterarControlado(JulgamentoParteDTO $objJulgamentoParteDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_parte_alterar',__METHOD__,$objJulgamentoParteDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objJulgamentoParteDTO->isSetNumIdItemSessaoJulgamento()){
        $this->validarNumIdItemSessaoJulgamento($objJulgamentoParteDTO, $objInfraException);
      }
      if ($objJulgamentoParteDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objJulgamentoParteDTO, $objInfraException);
      }
      if ($objJulgamentoParteDTO->isSetNumIdUsuarioDesempate()){
        $this->validarNumIdUsuarioDesempate($objJulgamentoParteDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objJulgamentoParteBD = new JulgamentoParteBD($this->getObjInfraIBanco());
      $objJulgamentoParteBD->alterar($objJulgamentoParteDTO);

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->setStrDispositivo(null);
      $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO);
      if ($objJulgamentoParteDTO->isSetNumIdUsuarioDesempate()){
        //verifica se deve gerar dispositivo
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
        $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);
        if ($objItemSessaoJulgamentoDTO->getStrSinPermiteFinalizacao()==='S') {
          $strDispositivo=$objItemSessaoJulgamentoRN->gerarTextoDispositivo($objItemSessaoJulgamentoDTO);
          $objItemSessaoJulgamentoDTO2=new ItemSessaoJulgamentoDTO();
          $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
          $objItemSessaoJulgamentoDTO2->setStrDispositivo($strDispositivo);
          $objItemSessaoJulgamentoDTO2->setNumIdUsuarioRelatorAcordaoDistribuicao($objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao());
          $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO2);
        }
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Item do Julgamento.',$e);
    }
  }

  protected function excluirControlado($arrObjJulgamentoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_parte_excluir',__METHOD__,$arrObjJulgamentoParteDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objVotoParteRN=new VotoParteRN();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objJulgamentoParteBD = new JulgamentoParteBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjJulgamentoParteDTO); $i< $iMax; $i++){

        $objJulgamentoParteDTO=new JulgamentoParteDTO();
        $objJulgamentoParteDTO->setNumIdJulgamentoParte($arrObjJulgamentoParteDTO[$i]->getNumIdJulgamentoParte());
        $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
        $objJulgamentoParteDTO=$this->consultar($objJulgamentoParteDTO);

        if (!$arrObjJulgamentoParteDTO[$i]->isSetStrSinForcarExclusao() || $arrObjJulgamentoParteDTO[$i]->getStrSinForcarExclusao()!='S'){
          if ($this->contar($objJulgamentoParteDTO)<=1){
            $objInfraException->lancarValidacao('N�o � poss�vel excluir todas as fra��es de julgamento de um processo.');
          }
        }

        //excluir votos
        $objVotoParteDTO=new VotoParteDTO();
        $objVotoParteDTO->setNumIdJulgamentoParte($arrObjJulgamentoParteDTO[$i]->getNumIdJulgamentoParte());
        $objVotoParteDTO->retNumIdVotoParte();
        $objVotoParteDTO->retNumIdVotoParteAssociado();
        if ($objVotoParteRN->contar($objVotoParteDTO)){
          $objInfraException->lancarValidacao('N�o � poss�vel excluir: fra�ao de julgamento possui votos.');
        }
        /*
        $arrObjVotoParteDTO=$objVotoParteRN->listar($objVotoParteDTO);

        $arrVP1=array();
        $arrVP2=array();
        foreach ($arrObjVotoParteDTO as $key=>$dto) {
          if($dto->getNumIdVotoParteAssociado()!=null){
            $arrVP1[]=$dto;
          }  else {
            $arrVP2[]=$dto;
          }
        }
        if (InfraArray::contar($arrVP1)>0) {
          $objVotoParteRN->excluir($arrVP1);
        }
        if (InfraArray::contar($arrVP2)>0) {
          $objVotoParteRN->excluir($arrVP2);
        }
*/

        $objJulgamentoParteBD->excluir($arrObjJulgamentoParteDTO[$i]);
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objJulgamentoParteDTO->getNumIdItemSessaoJulgamento());
        $objItemSessaoJulgamentoDTO->setStrDispositivo(null);
        $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO);
      }

      //Auditoria
      //verifica se deve gerar dispositivo
      //se for for�ar exclus�o n�o gerar dispositivo - retirada de pauta
      if ($iMax>0 && (!$arrObjJulgamentoParteDTO[0]->isSetStrSinForcarExclusao() || $arrObjJulgamentoParteDTO[0]->getStrSinForcarExclusao()!='S')) {
        $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrObjJulgamentoParteDTO[0]->getNumIdItemSessaoJulgamento());
        $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);
        if ($objItemSessaoJulgamentoDTO->getStrSinPermiteFinalizacao()==='S' && $objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getStrStaSituacao()==SessaoJulgamentoRN::$ES_ABERTA) {
          $strDispositivo = $objItemSessaoJulgamentoRN->gerarTextoDispositivo($objItemSessaoJulgamentoDTO);
          $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
          $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($arrObjJulgamentoParteDTO[0]->getNumIdItemSessaoJulgamento());
          $objItemSessaoJulgamentoDTO2->setStrDispositivo($strDispositivo);
          $objItemSessaoJulgamentoDTO2->setNumIdUsuarioRelatorAcordaoDistribuicao($objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao());
          $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO2);
        }
      }

    }catch(Exception $e){
      throw new InfraException('Erro excluindo fra��o de Julgamento.',$e);
    }
  }

  protected function consultarConectado(JulgamentoParteDTO $objJulgamentoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_parte_consultar',__METHOD__,$objJulgamentoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objJulgamentoParteBD = new JulgamentoParteBD($this->getObjInfraIBanco());
      $ret = $objJulgamentoParteBD->consultar($objJulgamentoParteDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando fra��o de Julgamento.',$e);
    }
  }

  protected function listarConectado(JulgamentoParteDTO $objJulgamentoParteDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_parte_listar',__METHOD__,$objJulgamentoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objJulgamentoParteBD = new JulgamentoParteBD($this->getObjInfraIBanco());
      $ret = $objJulgamentoParteBD->listar($objJulgamentoParteDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando fra��es de Julgamento.',$e);
    }
  }

  protected function contarConectado(JulgamentoParteDTO $objJulgamentoParteDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('julgamento_parte_listar',__METHOD__,$objJulgamentoParteDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objJulgamentoParteBD = new JulgamentoParteBD($this->getObjInfraIBanco());
      $ret = $objJulgamentoParteBD->contar($objJulgamentoParteDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando fra��es de Julgamento',$e);
    }
  }

}
?>