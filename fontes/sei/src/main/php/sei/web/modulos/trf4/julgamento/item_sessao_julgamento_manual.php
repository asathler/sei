<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 27/10/2014 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.33.1
 *
 * Vers�o no CVS: $Id$
 */

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
//  InfraDebug::getInstance()->setBolLigado(false);
//  InfraDebug::getInstance()->setBolDebugInfra(true);
//  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
//  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('arvore','id_sessao_julgamento_origem','id_item_sessao_julgamento_origem'));

  $strDesabilitar = '';
  $strParametros='';
  if (isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }
  if (isset($_GET['id_sessao_julgamento'])){
    $idSessaoJulgamento=$_GET['id_sessao_julgamento'];
  } else {
    $idSessaoJulgamento=$_POST['selSessao'];
  }
  if (!isset($_GET['id_sessao_julgamento_origem'])){
    $_GET['id_sessao_julgamento_origem']=$idSessaoJulgamento;
    $_GET['id_item_sessao_julgamento_origem']=$_GET['id_item_sessao_julgamento'];
  }
  if (isset($_GET['num_seq'])) {
    $numOrdemSequencial=$_GET['num_seq'];
  } else {
    //calcular num_seq
    $objPesquisaSessaoJulgamentoDTO = new PesquisaSessaoJulgamentoDTO();
    $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinOrdenarItens('S');
    $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
    $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
    $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);
    $arrObjItemSessaoJulgamentoDTO = $objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      if ($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()==$_GET['id_item_sessao_julgamento']) {
        $numOrdemSequencial = $objItemSessaoJulgamentoDTO->getNumOrdem();
        break;
      }
    }
  }
  $strParametros.='&num_seq='.$numOrdemSequencial;
  $strParametros.='&id_sessao_julgamento='.$idSessaoJulgamento;
  $strParametros.='&id_item_sessao_julgamento='.$_GET['id_item_sessao_julgamento'];
  $arrComandos = array();


  $bolAdmin = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar');

  switch($_GET['acao']){
    case 'item_sessao_julgamento_manual':
      $strTitulo = 'Julgamento Manual';


      $arrComandos[] = '<button type="button" accesskey="" id="btnFechar" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&num_seq='.$numOrdemSequencial.'&id_sessao_julgamento='.$_GET['id_sessao_julgamento_origem'].'&id_item_sessao_julgamento='.$_GET['id_item_sessao_julgamento_origem'].PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento_origem'])).'\';" class="infraButton">Fechar</button>';
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      if($_POST['btnDispositivo']=='Dispositivo') {
        unset($_POST['txaDispositivo']);
      }
      $strDispositivo=$_POST['txaDispositivo'];
      $objItemSessaoJulgamentoDTO->setStrDispositivo($strDispositivo);
      $objItemSessaoJulgamentoDTO->setStrStaSituacao($_POST['rdoSituacao']);
      $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($_POST['selRelatorAcordao']);

      if($_GET['converter']==1){
        try{
          $objItemSessaoJulgamentoDTOBanco=new ItemSessaoJulgamentoDTO();
          $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
          $objItemSessaoJulgamentoDTOBanco=$objItemSessaoJulgamentoRN->converterManual($objItemSessaoJulgamentoDTOBanco);
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }

      if($_POST['btnSalvar']=='Salvar') {

        try{
          $objItemSessaoJulgamentoRN->julgarManual($objItemSessaoJulgamentoDTO);

          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento'])));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $objItemSessaoJulgamentoDTOBanco=new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
  $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
  $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retStrDescricaoSessaoBloco();
  $objItemSessaoJulgamentoDTOBanco=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTOBanco);

  $strTitulo .= ' '.$objItemSessaoJulgamentoDTOBanco->getStrDescricaoSessaoBloco().' '.$numOrdemSequencial;


  $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);
  if (!isset($_POST['txaDispositivo'])){
    $strDispositivo=$objItemSessaoJulgamentoDTO->getStrDispositivo();
    if ($strDispositivo==null || $_POST['btnDispositivo']=='Dispositivo') {
//      if ($_POST['btnDispositivo']=='Dispositivo' && $_POST['selRelatorAcordao']!='null'){
      $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($_POST['selRelatorAcordao']);
//      }
      $objDistribuicaoDTO=new DistribuicaoDTO();
      $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      $objDistribuicaoDTO->retNumIdUsuarioRelatorAcordao();
      $objDistribuicaoRN=new DistribuicaoRN();
      $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);
      $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao());
    }
  }

  if (isset($_POST['rdoSituacao'])){
    $strStaSituacao=$_POST['rdoSituacao'];
    $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($_POST['selRelatorAcordao']);
  } else {
    $strStaSituacao=$objItemSessaoJulgamentoDTOBanco->getStrStaSituacao();
    if($strStaSituacao==ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA){
      $objPedidoVistaDTO=new PedidoVistaDTO();
      $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      if(!in_array($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento(),array(SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_FINALIZADA))){
        $objPedidoVistaDTO->setStrSinPendente('S');
      }
      $objPedidoVistaDTO->setDthDevolucao(null);
      $objPedidoVistaDTO->retNumIdUsuario();
      $objPedidoVistaRN=new PedidoVistaRN();
      $objPedidoVistaDTO=$objPedidoVistaRN->consultar($objPedidoVistaDTO);
      if($objPedidoVistaDTO){
        $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($objPedidoVistaDTO->getNumIdUsuario());
      }
    } else {
      $objDistribuicaoDTO=new DistribuicaoDTO();
      $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      $objDistribuicaoDTO->retNumIdUsuarioRelatorAcordao();
      $objDistribuicaoRN=new DistribuicaoRN();
      $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);
      $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao());
    }
  }

  $objColegiadoVersaoDTO=new ColegiadoVersaoDTO();
  $objColegiadoVersaoRN=new ColegiadoVersaoRN();
  $objColegiadoVersaoDTO->setNumIdColegiado($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
  $objColegiadoVersaoDTO->setStrSinUltima('S');
  $objColegiadoVersaoDTO->retNumIdColegiadoVersao();
  $objColegiadoVersaoDTO=$objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);

  $numIdColegiadoVersao=$objColegiadoVersaoDTO->getNumIdColegiadoVersao();
  $numIdUsuarioRelatorAcordao=$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao();
  $strItensSelRelatorAcordao=ColegiadoComposicaoINT::montarSelectNome('null','&nbsp;',$numIdUsuarioRelatorAcordao,$numIdColegiadoVersao);

  $objSessaoJulgamentoDTO=$objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO();
  $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
  $staDistribuicao=$objItemSessaoJulgamentoDTO->getStrStaDistribuicao();
  $strComandoSalvar=null;
  if($bolAdmin && $objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_ABERTA) {
    $strComandoSalvar = '<button type="submit" accesskey="" id="btnSalvar" name="btnSalvar" value="Salvar" class="infraButton">Salvar</button>';
    array_unshift($arrComandos,$strComandoSalvar);
  }
  $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();

  $objItemSessaoJulgamentoDTOBanco->setNumIdDistribuicaoAgrupadorDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicaoAgrupadorDistribuicao());
  $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao(array(ItemSessaoJulgamentoRN::$STA_SESSAO_CANCELADA, ItemSessaoJulgamentoRN::$STA_IGNORADA, ItemSessaoJulgamentoRN::$STA_RETIRADO, ItemSessaoJulgamentoRN::$STA_ADIADO, ItemSessaoJulgamentoRN::$STA_DILIGENCIA), InfraDTO::$OPER_NOT_IN);
  $objItemSessaoJulgamentoDTOBanco->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_DESC);
  $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retDthSessaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTOBanco->retStrSinManual();
  $arrObjItemSessaoJulgamentoDTOBanco = $objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTOBanco);
  $strItensSessoes='';
  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $arrObjSituacaoSessaoJulgamentoDTO=$objSessaoJulgamentoRN->listarValoresSituacaoSessao();
  $arrObjSituacaoSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjSituacaoSessaoJulgamentoDTO,'StaSituacao');
  if(count($arrObjItemSessaoJulgamentoDTOBanco)>1) {
    foreach ($arrObjItemSessaoJulgamentoDTOBanco as $objItemSessaoJulgamentoDTOBanco) {
      $strDescricaoSessao = $objItemSessaoJulgamentoDTOBanco->getDthSessaoSessaoJulgamento();
      $strDescricaoSessao .= ' - ' . $arrObjSituacaoSessaoJulgamentoDTO[$objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()]->getStrDescricao();
      $strLink = SessaoJulgamentoINT::montarLink($objItemSessaoJulgamentoDTOBanco->getStrSinManual()==='S' ? 'item_sessao_julgamento_manual' : 'item_sessao_julgamento_julgar', $_GET['acao_retorno'], $objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento(), '&id_sessao_julgamento=' . $objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());

      $strItensSessoes .= '<option value="' . base64_encode($strLink) . '"';
      if ($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento()==$_GET['id_item_sessao_julgamento']) {
        $strItensSessoes .= ' selected="selected"';
      }
      $strItensSessoes .= '>' . $strDescricaoSessao . "</option>\n";
    }
  }




  $strLinkProcesso = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

  #lblColegiado {position:absolute;left:0%;top:0%;width:40%;}
  #txtColegiado {position:absolute;left:0%;top:38%;width:40%;}

  #lblProcesso {position:absolute;left:43%;top:0%;width:20%;}
  #ancProcesso {position:absolute;left:43%;top:38%;}

  #lblRelatorAcordao {position:absolute;left:43%;top:0%;width:29%;}
  #selRelatorAcordao {position:absolute;left:43%;top:30%;width:29%;}

  #lblDispositivo {position:absolute;left:0%;top:0%;width:95%;}
  #txaDispositivo {position:absolute;left:0%;top:3.5%;width:95%;font-size:1.4em}

  #lblSessao {position:relative;left:0%;top:0;width:22%;}
  #selSessao {position:relative;left:0%;width:50%;margin: 3px 0;}

  #fldSituacao {height:70%;left: 0;position: absolute;width: 33%;}
  #divOptJulgado {left:15%;top:<?=(PaginaSEI::getInstance()->isBolAjustarTopFieldset()?'13%':'47%')?>;position: absolute;}
  #divOptPedidoVista {left:55%;top:<?=(PaginaSEI::getInstance()->isBolAjustarTopFieldset()?'13%':'47%')?>;position: absolute;}


<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
//PaginaSEI::getInstance()->abrirJavaScript();
?>
  <script type="text/javascript" charset="iso-8859-1">
    var objAjaxSessao;

    function inicializar(){
      infraOcultarMenuSistemaEsquema();
      document.getElementById('txtColegiado').readOnly=true;
      infraEfeitoTabelas();
      <?if($strComandoSalvar==null){?>
      infraDesabilitarCamposAreaDados();
      <?}?>
    }
    function trocarSessao(el){
      document.location.href=atob(el.value);
    }
    function configurarSituacao(){
      var val=$('input[name=rdoSituacao]:checked').val();
      if (val==='<?= ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO ?>') { //julgado
        $('#lblRelatorAcordao').text('Relator para Ac�rd�o:');
      } else { //pedido de vista
        $('#lblRelatorAcordao').text('Pedido de vista para:');
      }
    }

    /**
     * @return {boolean}
     */
    function OnSubmitForm() {
      var sel=$('#selRelatorAcordao');
      if(sel.val()==='null'){
        var val=$('input[name=rdoSituacao]:checked').val();
        if (val==='<?= ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO ?>') { //julgado
          alert('Selecione o Relator para ac�rd�o.');
        } else { //pedido de vista
          alert('Selecione o usu�rio que pediu vista.')
        }
        return false;
      }
      return true;
    }
  </script>
<?
//PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
  <form id="frmItemSessaoJulgamentoJulgar" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao_origem'].'&acao_retorno='.$_GET['acao_retorno'].$strParametros)?>">
    <?
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    //PaginaSEI::getInstance()->montarAreaValidacao();
    PaginaSEI::getInstance()->abrirAreaDados('5em');
    ?>

    <label id="lblColegiado" for="txtColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
    <input type="text" id="txtColegiado" name="txtColegiado" class="infraText" value="<?=PaginaSEI::tratarHTML($objSessaoJulgamentoDTO->getStrNomeColegiado()); ?>" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>

    <label id="lblProcesso" for="ancProcesso" accesskey="" class="infraLabelObrigatorio">Processo:</label>
    <a id="ancProcesso" href="<?=$strLinkProcesso?>" target="_blank" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" alt="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>" title="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>"><?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?></a>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    if(in_array($strStaSituacao,array(ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA,ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO,ItemSessaoJulgamentoRN::$STA_NORMAL,ItemSessaoJulgamentoRN::$STA_JULGADO))) {
      PaginaSEI::getInstance()->abrirAreaDados('7em');
      ?>

      <fieldset id="fldSituacao" class="infraFieldset">
        <legend class="infraLegend">&nbsp;Situa��o&nbsp;</legend>

        <div id="divOptJulgado" class="infraDivRadio">
          <input type="radio" name="rdoSituacao" id="optJulgado"
                 value="<?= ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO ?>"
                 onclick="configurarSituacao();" <?= ($strStaSituacao !== ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA ? 'checked="checked"' : '') ?>
                 class="infraRadio"/>
          <span id="spnJulgado"><label id="lblJulgado" for="optJulgado" class="infraLabelRadio"
                                       tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">Julgado</label></span>
        </div>

        <div id="divOptPedidoVista" class="infraDivRadio">
          <input type="radio" name="rdoSituacao" id="optPedidoVista"
                 value="<?= ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA ?>"
                 onclick="configurarSituacao();" <?= ($strStaSituacao == ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA ? 'checked="checked"' : '') ?>
                 class="infraRadio"/>
          <span id="spnPedidoVista"><label id="lblPedidoVista" for="optPedidoVista" class="infraLabelRadio"
                                           tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">Pedido de Vista</label></span>
        </div>

      </fieldset>

      <label id="lblRelatorAcordao" for="selRelatorAcordao" accesskey="" class="infraLabelObrigatorio">Relator para
        Ac�rd�o:</label>
      <select id="selRelatorAcordao" name="selRelatorAcordao" onchange="" class="infraSelect"
              tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
        <?= $strItensSelRelatorAcordao ?>
      </select>
      <?
      PaginaSEI::getInstance()->fecharAreaDados();
      if ($strItensSessoes!='') {?>

        <label id="lblSessao" for="selSessao" accesskey="" class="infraLabelObrigatorio">Data da Sess�o:</label>
        <select id="selSessao" name="selSessao"  onchange="trocarSessao(this);" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
          <?=$strItensSessoes?>
        </select>


      <?}
    }
    PaginaSEI::getInstance()->abrirAreaDados('55em');
    ?>
    <label id="lblDispositivo" for="txaDispositivo" class="infraLabelObrigatorio">Dispositivo:</label>
    <textarea id="txaDispositivo" name="txaDispositivo" rows="20"  class="infraTextarea"  tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?=$strDispositivo;?></textarea>
    <br />
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?>
  </form>
<?
//PaginaSEI::getInstance()->montarAreaDebug();
//PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
?>

<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>