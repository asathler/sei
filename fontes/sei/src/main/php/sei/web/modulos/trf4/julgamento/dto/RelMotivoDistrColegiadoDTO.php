<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/12/2018 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class RelMotivoDistrColegiadoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'rel_motivo_distr_colegiado';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdColegiado', 'id_colegiado');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdMotivoDistribuicao', 'id_motivo_distribuicao');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'DescricaoMotivoDistribuicao', 'descricao', 'motivo_distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaTipoMotivoDistribuicao', 'sta_tipo', 'motivo_distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SinAtivoMotivoDistribuicao', 'sin_ativo', 'motivo_distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'NomeColegiado', 'nome', 'colegiado');

    $this->configurarPK('IdColegiado',InfraDTO::$TIPO_PK_INFORMADO);
    $this->configurarPK('IdMotivoDistribuicao',InfraDTO::$TIPO_PK_INFORMADO);

    $this->configurarFK('IdColegiado', 'colegiado', 'id_colegiado');
    $this->configurarFK('IdMotivoDistribuicao', 'motivo_distribuicao', 'id_motivo_distribuicao');
  }
}
?>