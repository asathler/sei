<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
//  InfraDebug::getInstance()->setBolLigado(false);
//  InfraDebug::getInstance()->setBolDebugInfra(true);
//  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
//  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento','id_sessao_julgamento','arvore'));

  $strDesabilitar = '';
  $strParametros='';
  if (isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }

  $arrComandos = array();


  switch($_GET['acao']){
    case 'item_sessao_julgamento_finalizar':
      $strTitulo = 'Dispositivo';

      $arrComandos[] = '<button type="submit" accesskey="" id="btnSalvar" name="btnSalvar" value="Salvar" class="infraButton">Salvar</button>';
      $arrComandos[] = '<button type="submit" accesskey="" id="btnDispositivo" name="btnDispositivo" value="Dispositivo" class="infraButton">Voltar ao original autom�tico</button>';
      $arrComandos[] = '<button type="button" accesskey="" id="btnFechar" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao'].'&montar_visualizacao=1'.$strParametros).'\';" class="infraButton">Fechar</button>';
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
      if($_POST['btnDispositivo']=='Dispositivo') {
        unset($_POST['txaDispositivo']);
      }
      $strDispositivo=$_POST['txaDispositivo'];
      $objItemSessaoJulgamentoDTO->setStrDispositivo($strDispositivo);
      $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($_POST['selRelatorAcordao']);


      if($_POST['btnSalvar']=='Salvar') {

        try{
          $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Dispositivo alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento'])));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }




  $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);
  if (!isset($_POST['txaDispositivo'])){
    $strDispositivo=$objItemSessaoJulgamentoDTO->getStrDispositivo();
    if ($strDispositivo==null || $_POST['btnDispositivo']=='Dispositivo') {
//      if ($_POST['btnDispositivo']=='Dispositivo' && $_POST['selRelatorAcordao']!='null'){
        $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($_POST['selRelatorAcordao']);
//      }
      $strDispositivo=$objItemSessaoJulgamentoRN->gerarTextoDispositivo($objItemSessaoJulgamentoDTO);
//      $objDistribuicaoDTO=new DistribuicaoDTO();
//      $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
//      $objDistribuicaoDTO->retNumIdUsuarioRelatorAcordao();
//      $objDistribuicaoRN=new DistribuicaoRN();
//      $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);
//      $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($objDistribuicaoDTO->getNumIdUsuarioRelatorAcordao());
    }
  }

  $objColegiadoVersaoDTO=new ColegiadoVersaoDTO();
  $objColegiadoVersaoRN=new ColegiadoVersaoRN();
  $objColegiadoVersaoDTO->setNumIdColegiado($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
  $objColegiadoVersaoDTO->setStrSinUltima('S');
  $objColegiadoVersaoDTO->retNumIdColegiadoVersao();
  $objColegiadoVersaoDTO=$objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);

  $numIdColegiadoVersao=$objColegiadoVersaoDTO->getNumIdColegiadoVersao();
  $numIdUsuarioRelatorAcordao=$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao();
  $strItensSelRelatorAcordao=ColegiadoComposicaoINT::montarSelectNome('null','(Definir automaticamente)',$numIdUsuarioRelatorAcordao,$numIdColegiadoVersao);

  $bolRelatorComposicaoAnterior=$objItemSessaoJulgamentoDTO->getBolRelatorComposicaoAnterior();

  $objSessaoJulgamentoDTO=$objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO();
  $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
  $staDistribuicao=$objItemSessaoJulgamentoDTO->getStrStaDistribuicao();

//  array_unshift($arrComandos,$strComandoFinalizar);

  $strLinkProcesso = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

#lblColegiado {position:absolute;left:0%;top:0%;width:40%;}
#txtColegiado {position:absolute;left:0%;top:38%;width:40%;}

#lblProcesso {position:absolute;left:43%;top:0%;width:20%;}
#ancProcesso {position:absolute;left:43%;top:38%;}

#lblRelatorAcordao {position:absolute;left:66%;top:0%;width:29%;}
#selRelatorAcordao {position:absolute;left:66%;top:38%;width:29%;}

#lblDispositivo {position:absolute;left:0%;top:0%;width:95%;}
#txaDispositivo {position:absolute;left:0%;top:3.5%;width:95%;font-size:1.4em}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
//PaginaSEI::getInstance()->abrirJavaScript();
?>
<script type="text/javascript" charset="iso-8859-1">
  var objAjaxSessao;

function inicializar(){
  infraOcultarMenuSistemaEsquema();
  infraDesabilitarCamposAreaDados();
  var txa= document.getElementById('txaDispositivo');
  txa.disabled=false;
  txa.readOnly=false;
  txa.focus();
  $('#selRelatorAcordao').prop('disabled',false);
  infraEfeitoTabelas();
}

  function finalizar() {
    if (confirm('Confirma finaliza��o do julgamento?')){
      document.getElementById('hdnFinalizar').value='Finalizar';
      document.getElementById('frmItemSessaoJulgamentoJulgar').submit();
    }
  }


  /**
   * @return {boolean}
   */
  function OnSubmitForm() {
  return true;
}
</script>
<?
//PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmItemSessaoJulgamentoJulgar" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao_origem'].'&acao_retorno='.$_GET['acao_retorno'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>

  <label id="lblColegiado" for="txtColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <input type="text" id="txtColegiado" name="txtColegiado" class="infraText" value="<?=PaginaSEI::tratarHTML($objSessaoJulgamentoDTO->getStrNomeColegiado()); ?>" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>

  <label id="lblProcesso" for="ancProcesso" accesskey="" class="infraLabelObrigatorio">Processo:</label>
  <a id="ancProcesso" href="<?=$strLinkProcesso?>" target="_blank" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" alt="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>" title="<?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?>"><?=PaginaSEI::tratarHTML($objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo())?></a>

<? if ($numIdUsuarioRelatorAcordao!=null || $bolRelatorComposicaoAnterior) {?>
  <label id="lblRelatorAcordao" for="selRelatorAcordao" accesskey="" class="infraLabelOpcional">Relator para Ac�rd�o:</label>
  <select id="selRelatorAcordao" name="selRelatorAcordao" onchange="" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
    <?=$strItensSelRelatorAcordao?>
  </select>
  <?}
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->abrirAreaDados('55em');
  ?>
  <label id="lblDispositivo" for="txaDispositivo" class="infraLabelObrigatorio">Dispositivo:</label>
  <textarea id="txaDispositivo" name="txaDispositivo" rows="25"  class="infraTextarea"  tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"><?=$strDispositivo;?></textarea>
  <br />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  ?>
</form>
<?
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>

<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>