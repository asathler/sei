<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*/

require_once __DIR__.'/../../../../SEI.php';

class SessaoJulgamentoRN extends InfraRN {

  public static $ES_PAUTA_ABERTA = 'A';
  public static $ES_PAUTA_FECHADA = 'F';
  public static $ES_ENCERRADA = 'E';
  public static $ES_ABERTA = 'B';
  public static $ES_SUSPENSA = 'S';
  public static $ES_CANCELADA = 'X';
  public static $ES_FINALIZADA = 'Z';
  public static $ES_PREVISTA = 'P';

  public function __construct(){
    parent::__construct();
  }

  public static function isAdministradorSessao(SessaoJulgamentoDTO $objSessaoJulgamentoDTO): bool
  {
    return (SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') &&
        SessaoSEI::getInstance()->getNumIdUnidadeAtual()==$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado());
  }
  public static function permiteDefinirPresencas(SessaoJulgamentoDTO $objSessaoJulgamentoDTO): bool
  {
    return in_array($objSessaoJulgamentoDTO->getStrStaSituacao(), array(SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_SUSPENSA)) &&
        self::isAdministradorSessao($objSessaoJulgamentoDTO);
  }

  public static function permiteSelecaoItens(SessaoJulgamentoDTO $objSessaoJulgamentoDTO):bool
  {
    return in_array($objSessaoJulgamentoDTO->getStrStaSituacao(), array(SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_ABERTA)) &&
        self::isAdministradorSessao($objSessaoJulgamentoDTO);
  }
  /**
   * @param $dthSessao
   * @return string
   */
  public function formatarDataHoraSessao($dthSessao)
  {
    $arrData=InfraData::decomporData($dthSessao);
    $ret=$arrData[InfraData::$DIA].'-'.$arrData[InfraData::$MES].'-'.$arrData[InfraData::$ANO];
    $ret.=', '.$arrData[InfraData::$HOR].'h';
    if($arrData[InfraData::$MIN]>0){
      $ret.=$arrData[InfraData::$MIN].'min';
    }

    return $ret;
  }
  /**
   * @param $dthSessao
   * @return string
   */
  public function formatarHoraSessao($dthSessao)
  {
    $arrData=InfraData::decomporData($dthSessao);
    $ret=$arrData[InfraData::$HOR].'h';
    if($arrData[InfraData::$MIN]>0){
      $ret.=$arrData[InfraData::$MIN].'min';
    }

    return $ret;
  }

  /**
   * @return BancoSEI
   */
  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  /**
   * @return array
   * @throws InfraException
   */
  public function listarValoresSituacaoSessao(){
    try {

      $objArrSituacaoSessaoJulgamentoDTO = array();

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_PREVISTA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Prevista');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_PAUTA_ABERTA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Pauta Aberta');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_PAUTA_FECHADA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Pauta Fechada');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_ABERTA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Aberta');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_ENCERRADA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Encerrada');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_FINALIZADA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Finalizada');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_SUSPENSA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Suspensa');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      $objSituacaoSessaoJulgamentoDTO = new SituacaoSessaoJulgamentoDTO();
      $objSituacaoSessaoJulgamentoDTO->setStrStaSituacao(self::$ES_CANCELADA);
      $objSituacaoSessaoJulgamentoDTO->setStrDescricao('Cancelada');
      $objArrSituacaoSessaoJulgamentoDTO[] = $objSituacaoSessaoJulgamentoDTO;

      return $objArrSituacaoSessaoJulgamentoDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando valores de Sessao.',$e);
    }
  }


  /**
   * @param $estadoAtual
   * @param $proximoEstado
   * @return bool
   * @throws InfraException
   */
  private function validarProximoEstado($estadoAtual, $proximoEstado) {

    if ($estadoAtual==$proximoEstado) return true;
    switch ($estadoAtual) {
      case self::$ES_PREVISTA:
        if ($proximoEstado==self::$ES_PAUTA_ABERTA) return true;
        break;
      case self::$ES_PAUTA_ABERTA:
        if ($proximoEstado==self::$ES_PAUTA_FECHADA) return true;
        break;
      case self::$ES_PAUTA_FECHADA:
        if (in_array($proximoEstado,array(self::$ES_PAUTA_ABERTA,self::$ES_ABERTA))) return true;
      break;
      case self::$ES_SUSPENSA:
      if ($proximoEstado==self::$ES_ABERTA) return true;
        break;
      case self::$ES_ABERTA:
        if (in_array($proximoEstado,array(self::$ES_ENCERRADA,self::$ES_SUSPENSA))) return true;
        break;
      case self::$ES_ENCERRADA:
        if ($proximoEstado==self::$ES_FINALIZADA) return true;
        break;
      case self::$ES_CANCELADA:
        break;
      default:
        throw new InfraException('Altera��o de Situa��o da Sess�o n�o permitida.');
    }
    return false;
  }

  /**
   * @param $estadoAtual
   * @return array
   * @throws InfraException
   */
  public function listarValoresSituacaoSessaoFiltrado($estadoAtual){
    try {
      $arrCompleto = $this->listarValoresSituacaoSessao();
      $arrCompleto=InfraArray::indexarArrInfraDTO($arrCompleto,'StaSituacao');
      $objArrSituacaoSessaoJulgamentoDTO=array();
      foreach ($arrCompleto as $key => $value){
        if ($this->validarProximoEstado($estadoAtual,$key))
          $objArrSituacaoSessaoJulgamentoDTO[]=$value;
      }
      return $objArrSituacaoSessaoJulgamentoDTO;
    }  catch(Exception $e){
      throw new InfraException('Erro listando valores de Sessao.',$e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param InfraException $objInfraException
   */
  private function validarDthSessao(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getDthSessao())){
      $objSessaoJulgamentoDTO->setDthSessao(null);
    }else{
      $dthSessao=$objSessaoJulgamentoDTO->getDthSessao();
      if (InfraArray::contar(explode(':',$dthSessao))==2){
        $objSessaoJulgamentoDTO->setDthSessao($dthSessao.':00');
      }
      if (!InfraData::validarDataHora($objSessaoJulgamentoDTO->getDthSessao())){
        $objInfraException->adicionarValidacao('Data/Hora da Sess�o inv�lida.');
      }

      $dto = new SessaoJulgamentoDTO();
      $dto->retNumIdSessaoJulgamento();
      $dto->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento(),InfraDTO::$OPER_DIFERENTE);
      $dto->setNumIdColegiado($objSessaoJulgamentoDTO->getNumIdColegiado());
      $dto->setDthSessao($objSessaoJulgamentoDTO->getDthSessao());
      $dto->setStrStaSituacao(self::$ES_CANCELADA, InfraDTO::$OPER_DIFERENTE);

      $dto = $this->consultar($dto);
      if ($dto != NULL){
        $objInfraException->adicionarValidacao('Existe outra Sess�o do mesmo Colegiado para a mesma data/hora.');
      }

      if (ConfiguracaoSEI::getInstance()->getValor('SEI','Producao') && InfraData::compararDatasSimples(InfraData::getStrDataAtual(),$objSessaoJulgamentoDTO->getDthSessao())<0){
        $objInfraException->adicionarValidacao('Data da sess�o n�o pode estar no passado.');
      }
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @param InfraException $objInfraException
   * @throws InfraException
   */
  private function validarStrStaSituacao(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getStrStaSituacao())){
      $objInfraException->adicionarValidacao('Situa��o n�o informada.');
    }else{
      if (!in_array($objSessaoJulgamentoDTO->getStrStaSituacao(),InfraArray::converterArrInfraDTO($this->listarValoresSituacaoSessao(),'StaSituacao'))){
        $objInfraException->adicionarValidacao('Situa��o inv�lida.');
      }
    }
  }
  private function validarNumIdColegiado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getNumIdColegiado())){
      $objInfraException->adicionarValidacao('Colegiado n�o informado.');
    }
  }
  private function validarNumIdTipoSessao(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getNumIdTipoSessao())){
      $objInfraException->adicionarValidacao('Tipo de sess�o n�o informado.');
    } else {
      $objTipoSessaoDTO=new TipoSessaoDTO();
      $objTipoSessaoRN=new TipoSessaoRN();
      $objTipoSessaoDTO->setNumIdTipoSessao($objSessaoJulgamentoDTO->getNumIdTipoSessao());
      $objTipoSessaoDTO->retStrSinVirtual();
      $objTipoSessaoDTO=$objTipoSessaoRN->consultar($objTipoSessaoDTO);
      if($objTipoSessaoDTO==null){
        $objInfraException->lancarValidacao('Tipo de sess�o n�o encontrado.');
      }
      $objSessaoJulgamentoDTO->setStrSinVirtualTipoSessao($objTipoSessaoDTO->getStrSinVirtual());
    }
  }
  private function validarDblIdDocumentoPauta(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getDblIdDocumentoPauta())){
      $objInfraException->adicionarValidacao('Documento de Pauta n�o informado.');
    } else {
      $objDocumentoDTO=new DocumentoDTO();
      $objDocumentoDTO->setDblIdDocumento($objSessaoJulgamentoDTO->getDblIdDocumentoPauta());
      $objDocumentoDTO->retDblIdProcedimento();
      $objDocumentoDTO->retDblIdDocumento();
      $objDocumentoRN=new DocumentoRN();
      $objDocumentoDTO=$objDocumentoRN->consultarRN0005($objDocumentoDTO);
      if($objDocumentoDTO==null){
        $objInfraException->adicionarValidacao('Documento n�o encontrado.');
      }elseif ($objDocumentoDTO->getDblIdProcedimento()!=$objSessaoJulgamentoDTO->getDblIdProcedimento()){
        $objInfraException->adicionarValidacao('Documento n�o pertence ao processo da sess�o');
      }

    }
  }
  private function validarNumIdUsuarioPresidente(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getNumIdUsuarioPresidente())){
      $objInfraException->adicionarValidacao('Presidente n�o informado.');
    }
  }
  private function validarDthInicio(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getDthInicio())){
      $objSessaoJulgamentoDTO->setDthInicio(null);
    }else{
      if (!InfraData::validarDataHora($objSessaoJulgamentoDTO->getDthInicio())){
        $objInfraException->adicionarValidacao('Inicio inv�lido.');
      }
      if (ConfiguracaoSEI::getInstance()->getValor('SEI','Producao') && InfraData::compararDatasSimples($objSessaoJulgamentoDTO->getDthInicio(),$objSessaoJulgamentoDTO->getDthSessao())<0){
        $objInfraException->adicionarValidacao('Data de In�cio da Sess�o n�o pode ser anterior a data da Sess�o.');
      }
    }
  }
  private function validarDthFim(SessaoJulgamentoDTO $objSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSessaoJulgamentoDTO->getDthFim())){
      $objSessaoJulgamentoDTO->setDthFim(null);
    }else{
      if (!InfraData::validarDataHora($objSessaoJulgamentoDTO->getDthFim())){
        $objInfraException->adicionarValidacao('Fim inv�lido.');
      }
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return SessaoJulgamentoDTO
   * @throws InfraException
   */
  protected function cadastrarControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_cadastrar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();


      $this->validarStrStaSituacao($objSessaoJulgamentoDTO, $objInfraException);
      $this->validarNumIdTipoSessao($objSessaoJulgamentoDTO, $objInfraException);
      $this->validarNumIdColegiado($objSessaoJulgamentoDTO, $objInfraException);
      $this->validarDthSessao($objSessaoJulgamentoDTO, $objInfraException);
      $this->validarDthInicio($objSessaoJulgamentoDTO, $objInfraException);
      $this->validarDthFim($objSessaoJulgamentoDTO, $objInfraException);

      if ($objSessaoJulgamentoDTO->getStrStaSituacao()!=self::$ES_PREVISTA){
        $objInfraException->adicionarValidacao('Situa��o da Sess�o n�o permitida para cadastramento.');
      }
      $objInfraException->lancarValidacoes();

      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $ret = $objSessaoJulgamentoBD->cadastrar($objSessaoJulgamentoDTO);

//      $this->cadastrarSessaoBloco($ret->getNumIdSessaoJulgamento(), $ret->getNumIdTipoSessao());


      $this->lancarAndamento($objSessaoJulgamentoDTO);
      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Sess�o de Julgamento.',$e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @throws InfraException
   */
  protected function alterarControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_alterar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objSessaoJulgamentoDTOBanco=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTOBanco->retTodos();
      $objSessaoJulgamentoDTOBanco=$this->consultar($objSessaoJulgamentoDTOBanco);
      $strStaAtual=$objSessaoJulgamentoDTOBanco->getStrStaSituacao();
      $strStaNovo=$strStaAtual;

      if ($objSessaoJulgamentoDTO->isSetStrStaSituacao() && $objSessaoJulgamentoDTO->getStrStaSituacao() != $strStaAtual){
        $this->validarStrStaSituacao($objSessaoJulgamentoDTO, $objInfraException);
        $strStaNovo=$objSessaoJulgamentoDTO->getStrStaSituacao();
        if (!$this->validarProximoEstado($strStaAtual,$strStaNovo)){
          $objInfraException->adicionarValidacao('Sess�o n�o pode ser alterada para esta Situa��o.');
        }
        if ($strStaNovo==self::$ES_ABERTA && ConfiguracaoSEI::getInstance()->getValor('SEI','Producao') && InfraData::compararDatasSimples(InfraData::getStrDataAtual(),$objSessaoJulgamentoDTOBanco->getDthSessao())<0){
          $objInfraException->adicionarValidacao('Sess�o n�o pode ser aberta antes da data definida.');
        }
        $this->lancarAndamento($objSessaoJulgamentoDTO);
      } else {
        $objSessaoJulgamentoDTO->setStrStaSituacao($strStaAtual);
      }



      if ($objSessaoJulgamentoDTO->isSetNumIdColegiado() && $objSessaoJulgamentoDTO->getNumIdColegiado()!=$objSessaoJulgamentoDTOBanco->getNumIdColegiado()){
        $objInfraException->adicionarValidacao('O Colegiado da Sess�o n�o pode ser alterado.');
      }
      $objSessaoJulgamentoDTO->setNumIdColegiado($objSessaoJulgamentoDTOBanco->getNumIdColegiado());

      if ($objSessaoJulgamentoDTO->isSetDthSessao() && $objSessaoJulgamentoDTO->getDthSessao()!=$objSessaoJulgamentoDTOBanco->getDthSessao()){
        $this->validarDthSessao($objSessaoJulgamentoDTO, $objInfraException);
        // pode alterar se for pauta_aberta ou pauta_fechada
        if (in_array($strStaAtual,array(self::$ES_ABERTA,self::$ES_SUSPENSA,self::$ES_CANCELADA,self::$ES_ENCERRADA,self::$ES_FINALIZADA))){
          $objInfraException->adicionarValidacao('A Data da Sess�o n�o pode ser alterada.');
        }
      } else {
        $objSessaoJulgamentoDTO->setDthSessao($objSessaoJulgamentoDTOBanco->getDthSessao());
      }
      if ($objSessaoJulgamentoDTO->isSetDthInicio() && $objSessaoJulgamentoDTO->getDthInicio()!=$objSessaoJulgamentoDTOBanco->getDthInicio() ){
        $this->validarDthInicio($objSessaoJulgamentoDTO, $objInfraException);
        if ($strStaNovo!=self::$ES_ABERTA && !($strStaNovo==self::$ES_SUSPENSA && $objSessaoJulgamentoDTO->getDthInicio()==null)){
          $objInfraException->adicionarValidacao('O Inicio da Sess�o n�o pode ser alterado, Sess�o n�o est� aberta.');
        }
      } else {
        $objSessaoJulgamentoDTO->setDthInicio($objSessaoJulgamentoDTOBanco->getDthInicio());
      }
      if ($objSessaoJulgamentoDTO->isSetDthFim() && $objSessaoJulgamentoDTO->getDthFim()!=$objSessaoJulgamentoDTOBanco->getDthFim()){
        $this->validarDthFim($objSessaoJulgamentoDTO, $objInfraException);
        if ($strStaNovo!=self::$ES_ENCERRADA && !($strStaNovo==self::$ES_SUSPENSA && $objSessaoJulgamentoDTO->getDthInicio()==null)){
          $objInfraException->adicionarValidacao('O Fim da Sess�o n�o pode ser alterado, Sess�o n�o est� encerrada.');
        }
      }
      if ($objSessaoJulgamentoDTO->isSetNumIdTipoSessao()&& $objSessaoJulgamentoDTO->getNumIdTipoSessao()!=$objSessaoJulgamentoDTOBanco->getNumIdTipoSessao()){
        if($objSessaoJulgamentoDTOBanco->getStrStaSituacao()!=self::$ES_PREVISTA){
          $objInfraException->adicionarValidacao('N�o � poss�vel alterar o tipo de sess�o ap�s abrir a pauta.');
        }
        $this->validarNumIdTipoSessao($objSessaoJulgamentoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Sess�o de Julgamento.',$e);
    }
  }

  protected function associarDocumentoPautaControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_associar_doc_pauta', __METHOD__, $objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objSessaoJulgamentoDTOBanco = new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTOBanco->retDblIdDocumentoPauta();
      $objSessaoJulgamentoDTOBanco->retStrSinVirtualTipoSessao();
      $objSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objSessaoJulgamentoDTOBanco->retDblIdProcedimento();
      $objSessaoJulgamentoDTOBanco = $this->consultar($objSessaoJulgamentoDTOBanco);

      if ($objSessaoJulgamentoDTOBanco==null) {
        $objInfraException->lancarValidacao('Sess�o de julgamento n�o encontrada.');
      }
      if ($objSessaoJulgamentoDTOBanco->getDblIdProcedimento()==null) {
        $objInfraException->lancarValidacao('Sess�o de julgamento n�o possui processo gerado.');
      }

      $strStaSituacao = $objSessaoJulgamentoDTOBanco->getStrStaSituacao();
      $objSessaoJulgamentoDTO->setDblIdProcedimento($objSessaoJulgamentoDTOBanco->getDblIdProcedimento());

      $this->validarDblIdDocumentoPauta($objSessaoJulgamentoDTO, $objInfraException);
      if ($objSessaoJulgamentoDTOBanco->getDblIdDocumentoPauta()!=null && !in_array($strStaSituacao, array(self::$ES_PAUTA_ABERTA, self::$ES_PAUTA_FECHADA))) {
        $objInfraException->adicionarValidacao('O documento de pauta n�o pode ser alterado.');
      }

      $objInfraException->lancarValidacoes();

      $objSessaoJulgamentoDTOBanco->setDblIdDocumentoPauta($objSessaoJulgamentoDTO->getDblIdDocumentoPauta());
      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTOBanco);

      //Auditoria

    } catch (Exception $e) {
      throw new InfraException('Erro alterando Sess�o de Julgamento.', $e);
    }
  }
  /**
   * @param $arrObjSessaoJulgamentoDTO
   * @throws InfraException
   */
  protected function excluirControlado($arrObjSessaoJulgamentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_excluir',__METHOD__,$arrObjSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAndamentoSessaoRN=new AndamentoSessaoRN();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();

      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO) {
        $objSessaoJulgamentoDTOBanco=new SessaoJulgamentoDTO();
        $objSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
        $objSessaoJulgamentoDTOBanco->retStrStaSituacao();
        $objSessaoJulgamentoDTOBanco=$this->consultar($objSessaoJulgamentoDTOBanco);

        if (!in_array($objSessaoJulgamentoDTOBanco->getStrStaSituacao(),array(self::$ES_PAUTA_ABERTA,self::$ES_PREVISTA))) {
          $objInfraException->lancarValidacao('Sess�o n�o pode mais ser exclu�da.');
        }

        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
        $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
        if ($objItemSessaoJulgamentoRN->contar($objItemSessaoJulgamentoDTO)>0){
          $objInfraException->lancarValidacao('Sess�o possui itens.');
        }

        $objAndamentoSessaoDTO=new AndamentoSessaoDTO();
        $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objAndamentoSessaoDTO->retNumIdAndamentoSessao();
        $arrObjAndamentoSessaoDTO=$objAndamentoSessaoRN->listar($objAndamentoSessaoDTO);
        if (InfraArray::contar($arrObjAndamentoSessaoDTO)>0) {
          $objAndamentoSessaoRN->excluir($arrObjAndamentoSessaoDTO);
        }

        $objSessaoBlocoDTO=new SessaoBlocoDTO();
        $objSessaoBlocoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objSessaoBlocoDTO->retNumIdSessaoBloco();
        $objSessaoBlocoRN=new SessaoBlocoRN();
        $objSessaoBlocoRN->excluir($objSessaoBlocoRN->listar($objSessaoBlocoDTO));

        $objSessaoJulgamentoBD->excluir($objSessaoJulgamentoDTO);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Sess�o de Julgamento.',$e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return SessaoJulgamentoDTO|null
   * @throws InfraException
   */
  protected function consultarConectado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){
    try {

      //Valida Permissao
      //SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_consultar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $ret = $objSessaoJulgamentoBD->consultar($objSessaoJulgamentoDTO);
      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Sess�o de Julgamento.',$e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return SessaoJulgamentoDTO[]
   * @throws InfraException
   */
  protected function listarConectado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_listar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $ret = $objSessaoJulgamentoBD->listar($objSessaoJulgamentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Sess�es de Julgamento.',$e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return string|null
   * @throws InfraException
   */
  protected function contarConectado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_listar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $ret = $objSessaoJulgamentoBD->contar($objSessaoJulgamentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Sess�es de Julgamento.',$e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return object|string|null
   * @throws InfraException
   */
  protected function bloquearControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_consultar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $ret = $objSessaoJulgamentoBD->bloquear($objSessaoJulgamentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Sess�o de Julgamento.',$e);
    }
  }

  protected function alterarEstadoInternoControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_alterar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if (!$objSessaoJulgamentoDTO->isSetStrStaSituacao()){
        $objInfraException->lancarValidacao('Estado da Sess�o n�o definido');
      }

      $staSituacaoNova=$objSessaoJulgamentoDTO->getStrStaSituacao();
      //n�o permitir altera��o em concluidos
      $objSessaoJulgamentoDTOBanco=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objSessaoJulgamentoDTOBanco->retNumIdColegiado();
      $objSessaoJulgamentoDTOBanco->retNumIdColegiadoVersao();
      $objSessaoJulgamentoDTOBanco->retDthInicio();
      $objSessaoJulgamentoDTOBanco->retNumIdTipoSessao();
      $objSessaoJulgamentoDTOBanco->retStrSinVirtualTipoSessao();
      $objSessaoJulgamentoDTOBanco->retNumIdUsuarioPresidente();
      $objSessaoJulgamentoDTOBanco->retStrNomePresidenteSessao();
      $objSessaoJulgamentoDTOBanco=$this->consultar($objSessaoJulgamentoDTOBanco);

      $bolVirtual=$objSessaoJulgamentoDTOBanco->getStrSinVirtualTipoSessao()==='S';
      $staSituacaoAtual=$objSessaoJulgamentoDTOBanco->getStrStaSituacao();
      if (in_array($staSituacaoAtual,array(self::$ES_ENCERRADA,self::$ES_CANCELADA,self::$ES_FINALIZADA))) {
        $objInfraException->lancarValidacao('Sess�o conclu�da n�o permite altera��o.');
      }
      if (!$this->validarProximoEstado($staSituacaoAtual,$staSituacaoNova)) {
        $objInfraException->lancarValidacao('Altera��o da Situa��o da Sess�o n�o permitida.');
      }
      if($staSituacaoAtual==self::$ES_PREVISTA && $staSituacaoNova==self::$ES_PAUTA_ABERTA){
        $objSessaoBlocoDTO=new SessaoBlocoDTO();
        $objSessaoBlocoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
        $objSessaoBlocoDTO->retNumIdSessaoBloco();
        $objSessaoBlocoRN=new SessaoBlocoRN();
        $arrObjSessaoBlocoDTO=$objSessaoBlocoRN->listar($objSessaoBlocoDTO);
        if(count($arrObjSessaoBlocoDTO)){
          $objSessaoBlocoRN->excluir($arrObjSessaoBlocoDTO);
        }
        $this->cadastrarSessaoBloco($objSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento(),$objSessaoJulgamentoDTOBanco->getNumIdTipoSessao());
      }
      if ($staSituacaoNova==self::$ES_PAUTA_FECHADA){

        $this->gerarDocumentoPauta($objSessaoJulgamentoDTO);
        $objSessaoJulgamentoDTOBanco->setStrHashPauta($objSessaoJulgamentoDTO->getStrHashPauta());
        $objSessaoJulgamentoDTOBanco->setDblIdDocumentoPauta($objSessaoJulgamentoDTO->getDblIdDocumentoPauta());
      }
      //define a vers�o do colegiado utilizada na sessao
      if ($objSessaoJulgamentoDTO->getStrStaSituacao()==self::$ES_ABERTA){
        if ($objSessaoJulgamentoDTOBanco->getNumIdColegiadoVersao()==null) {
          $objColegiadoVersaoDTO=new ColegiadoVersaoDTO();
          $objColegiadoVersaoRN=new ColegiadoVersaoRN();
          $objColegiadoVersaoDTO->setNumIdColegiado($objSessaoJulgamentoDTOBanco->getNumIdColegiado());
          $objColegiadoVersaoDTO->setStrSinUltima('S');
          $objColegiadoVersaoDTO->retNumIdColegiadoVersao();
          $objColegiadoVersaoDTO->retStrSinEditavel();
          $objColegiadoVersaoDTO=$objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);
          if($objColegiadoVersaoDTO->getStrSinEditavel()=='S'){
            $objColegiadoVersaoDTO->setStrSinEditavel('N');
            $objColegiadoVersaoRN->alterar($objColegiadoVersaoDTO);
          }
          $objSessaoJulgamentoDTOBanco->setNumIdColegiadoVersao($objColegiadoVersaoDTO->getNumIdColegiadoVersao());
        }

        if ($objSessaoJulgamentoDTOBanco->getDthInicio()==null) {
          $objSessaoJulgamentoDTOBanco->setDthInicio(InfraData::getStrDataHoraAtual());
        }
        //se ainda n�o estiver definido, copiar id presidente da sess�o do colegiado
        if ($objSessaoJulgamentoDTOBanco->getNumIdUsuarioPresidente() == null) {
          $objColegiadoDTO = new ColegiadoDTO();
          $objColegiadoRN = new ColegiadoRN();
          $objColegiadoDTO->setNumIdColegiado($objSessaoJulgamentoDTOBanco->getNumIdColegiado());
          $objColegiadoDTO->retNumIdUsuarioPresidente();
          $objColegiadoDTO->retStrNomePresidente();
          $objColegiadoDTO = $objColegiadoRN->consultar($objColegiadoDTO);
          $objSessaoJulgamentoDTOBanco->setNumIdUsuarioPresidente($objColegiadoDTO->getNumIdUsuarioPresidente());
          $objSessaoJulgamentoDTOBanco->setStrNomePresidenteSessao($objColegiadoDTO->getStrNomePresidente());
        }

        $objPresencaSessaoDTO=new PresencaSessaoDTO();
        $objPresencaSessaoRN=new PresencaSessaoRN();
        $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objPresencaSessaoDTO->setNumIdUsuario($objSessaoJulgamentoDTOBanco->getNumIdUsuarioPresidente());
        $objPresencaSessaoDTO->setDthSaida(null);
        $objPresencaSessaoDTO->retNumIdPresencaSessao();
        if ($objPresencaSessaoRN->contar($objPresencaSessaoDTO)==0){
          $objInfraException->lancarValidacao('N�o � permitido iniciar a sess�o sem o Presidente.');
        }
      }
      $objSessaoJulgamentoDTOBanco->setStrStaSituacao($objSessaoJulgamentoDTO->getStrStaSituacao());

      $this->lancarAndamento($objSessaoJulgamentoDTOBanco);
      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTOBanco);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Sess�o de Julgamento.',$e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @throws InfraException
   */
  public function alterarEstado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){
    $bolAcumulacaoPrevia = FeedSEIProtocolos::getInstance()->isBolAcumularFeeds();
    FeedSEIProtocolos::getInstance()->setBolAcumularFeeds(true);
    $this->alterarEstadoInterno($objSessaoJulgamentoDTO);
    if (!$bolAcumulacaoPrevia){
      FeedSEIProtocolos::getInstance()->setBolAcumularFeeds(false);
      FeedSEIProtocolos::getInstance()->indexarFeeds();
    }
  }

  /**
   * @param PesquisaSessaoJulgamentoDTO $parObjPesquisaSessaoJulgamentoDTO
   * @return SessaoJulgamentoDTO | null
   * @throws InfraException
   */
  protected function pesquisarConectado(PesquisaSessaoJulgamentoDTO $parObjPesquisaSessaoJulgamentoDTO){

    $numIdSessaoJulgamento=$parObjPesquisaSessaoJulgamentoDTO->getNumIdSessaoJulgamento();

    $objSessaoJulgamentoDTO=CacheSessaoJulgamentoRN::getObjSessaoJulgamentoDTO($numIdSessaoJulgamento);
    if ($objSessaoJulgamentoDTO==null){
      return null;
    }

    $idColegiadoVersao = $objSessaoJulgamentoDTO->getNumIdColegiadoVersao();
    if ($objSessaoJulgamentoDTO->getNumIdUsuarioPresidente() == null) {
      $objSessaoJulgamentoDTO->setNumIdUsuarioPresidente($objSessaoJulgamentoDTO->getNumIdUsuarioPresidenteColegiado());
    }

    if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinOrdenarItens() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinOrdenarItens() === 'S') {
      $parObjPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
    }

    if(!$objSessaoJulgamentoDTO->isSetArrObjColegiadoComposicaoDTO() && $parObjPesquisaSessaoJulgamentoDTO->isSetStrSinComposicao() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinComposicao()==='S'){
      //lista atuais membros do colegiado
      $arrObjColegiadoComposicaoDTO=CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($numIdSessaoJulgamento);
      $objSessaoJulgamentoDTO->setArrObjColegiadoComposicaoDTO($arrObjColegiadoComposicaoDTO);
    }

    if(!$objSessaoJulgamentoDTO->isSetArrObjPresencaSessaoDTO() && $parObjPesquisaSessaoJulgamentoDTO->isSetStrSinPresenca() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinPresenca()==='S'){
      $objSessaoJulgamentoDTO->setArrObjPresencaSessaoDTO(CacheSessaoJulgamentoRN::getArrObjPresencaSessaoDTO($numIdSessaoJulgamento));
    }

    if($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinItemSessao() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinItemSessao()==='S' && $parObjPesquisaSessaoJulgamentoDTO!=null){
      //lista itens da sess�o
      if($objSessaoJulgamentoDTO->isSetArrObjItemSessaoJulgamentoDTO()){
        $arrObjItemSessaoJulgamentoDTO=$objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
      } else {
        $arrObjItemSessaoJulgamentoDTO = CacheSessaoJulgamentoRN::getArrObjItemSessaoJulgamentoDTO($numIdSessaoJulgamento,true);
      }

      $objSessaoJulgamentoDTO->setArrObjPedidoVistaDTO(array());
      if (InfraArray::contar($arrObjItemSessaoJulgamentoDTO)>0) {

        $arrIdItemSessaoJulgamento = CacheSessaoJulgamentoRN::getArrIdItemSessaoJulgamentoDTO($numIdSessaoJulgamento);
        CacheSessaoJulgamentoRN::getArrObjBloqueioItemSessUnidadeDTO($arrIdItemSessaoJulgamento);
        $bolVotos=false;
        $arrObjDestaqueDTO=null;

        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinAnotacao() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinAnotacao() === 'S') {
          //buscar anota��es dos processos
          $arrObjProtocoloDTO = CacheSessaoJulgamentoRN::getArrObjProtocoloDTO($numIdSessaoJulgamento);
          foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
            $objItemSessaoJulgamentoDTO->setObjAnotacaoDTO($arrObjProtocoloDTO[$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()]->getObjAnotacaoDTO());
          }
        }

        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinEleicoes() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinEleicoes() === 'S') {
          if (InfraArray::contar($arrIdItemSessaoJulgamento) > 0) {

            $objEleicaoDTO = new EleicaoDTO();
            $objEleicaoDTO->retNumIdEleicao();
            $objEleicaoDTO->retNumIdItemSessaoJulgamento();
            $objEleicaoDTO->retStrIdentificacao();
            $objEleicaoDTO->retStrStaSituacao();
            $objEleicaoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento, InfraDTO::$OPER_IN);
            $objEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

            $objEleicaoRN = new EleicaoRN();
            $arrObjEleicaoDTO = InfraArray::indexarArrInfraDTO($objEleicaoRN->listar($objEleicaoDTO), 'IdItemSessaoJulgamento', true);

            foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
              if (isset($arrObjEleicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()])){
                $objItemSessaoJulgamentoDTO->setArrObjEleicaoDTO($arrObjEleicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()]);
              }else{
                $objItemSessaoJulgamentoDTO->setArrObjEleicaoDTO(null);
              }
            }
          }

        }

        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinDestaque() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinDestaque() === 'S') {
          CacheSessaoJulgamentoRN::getArrObjDestaqueDTO($arrIdItemSessaoJulgamento);
        }

        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinRevisao() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinRevisao() === 'S') {
          CacheSessaoJulgamentoRN::complementarItensRevisao($arrIdItemSessaoJulgamento);
        }
        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinPedidoVista() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinPedidoVista() == 'S') {
          $bolVotos=true;
          $objSessaoJulgamentoDTO->setArrObjPedidoVistaDTO(CacheSessaoJulgamentoRN::getArrObjPedidoVistaDTO($arrIdItemSessaoJulgamento));
        }

        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinVotos() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinVotos() == 'S') {
          $bolVotos=true;
        }
        if ($bolVotos) {
          CacheSessaoJulgamentoRN::getArrObjJulgamentoParteDTO($arrIdItemSessaoJulgamento);
          CacheSessaoJulgamentoRN::getArrObjVotoParteDTO($arrIdItemSessaoJulgamento);
        }

        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinDocumentos() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinDocumentos() == 'S') {
          CacheSessaoJulgamentoRN::getArrObjItemSessaoDocumentoDTO($arrIdItemSessaoJulgamento);
        }

        //processamento do item
        foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
          $objItemSessaoJulgamentoDTO->setStrProvimento(null);
          $objItemSessaoJulgamentoDTO->setStrComplemento(null);
        }

        if ($parObjPesquisaSessaoJulgamentoDTO->isSetStrSinOrdenarItens() && $parObjPesquisaSessaoJulgamentoDTO->getStrSinOrdenarItens() == 'S') {

          $arrObjItemSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdSessaoBloco', true);
          $arrObjSessaoBlocoDTO = array_values(CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($numIdSessaoJulgamento));
          InfraArray::ordenarArrInfraDTO($arrObjSessaoBlocoDTO,'Ordem',InfraArray::$TIPO_ORDENACAO_ASC);
          $dthInicioSessao=$objSessaoJulgamentoDTO->getDthInicio();

          $arrObjItemSessaoJulgamentoDTOOrdenado=[];
          foreach ($arrObjSessaoBlocoDTO as $objSessaoBlocoDTO) {
            $numIdSessaoBloco=$objSessaoBlocoDTO->getNumIdSessaoBloco();
            $staTipoItem=$objSessaoBlocoDTO->getStrStaTipoItem();
            if(!isset($arrObjItemSessaoJulgamentoDTO[$numIdSessaoBloco])) {
              continue;
            }

            $arrMesaSessaoAberta=[];
            //separa itens adicionados ap�s abertura da sess�o
            if($dthInicioSessao!=null && $staTipoItem==TipoSessaoBlocoRN::$STA_MESA) {
              /**
               * @var ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO2
               */
              foreach ($arrObjItemSessaoJulgamentoDTO[$numIdSessaoBloco] as $chave=>$objItemSessaoJulgamentoDTO) {
                if(InfraData::compararDataHorasSimples($dthInicioSessao,$objItemSessaoJulgamentoDTO->getDthInclusao())>=1){
                  $objItemSessaoJulgamentoDTO->setStrNomeUsuarioRelator($arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()]->getStrNomeUsuario());
                  $arrMesaSessaoAberta[]=$objItemSessaoJulgamentoDTO;
                  unset($arrObjItemSessaoJulgamentoDTO[$numIdSessaoBloco][$chave]);
                }
              }
            }
            $i=1;
            if($staTipoItem==TipoSessaoBlocoRN::$STA_PAUTA || $staTipoItem==TipoSessaoBlocoRN::$STA_MESA){
              $arrObjItemSessaoJulgamentoDTOBlocoAtual=InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO[$numIdSessaoBloco], 'IdUsuarioSessao', true);
              foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
                if (isset($arrObjItemSessaoJulgamentoDTOBlocoAtual[$objColegiadoComposicaoDTO->getNumIdUsuario()])){
                  foreach ($arrObjItemSessaoJulgamentoDTOBlocoAtual[$objColegiadoComposicaoDTO->getNumIdUsuario()] as $objItemSessaoJulgamentoDTO) {
                    $objItemSessaoJulgamentoDTO->setNumOrdem($i++);
                    $arrObjItemSessaoJulgamentoDTOOrdenado[]=$objItemSessaoJulgamentoDTO;
                  }
                }
              }
              foreach ($arrMesaSessaoAberta as $objItemSessaoJulgamentoDTO) {
                $objItemSessaoJulgamentoDTO->setNumOrdem($i++);
                $arrObjItemSessaoJulgamentoDTOOrdenado[]=$objItemSessaoJulgamentoDTO;
              }
            } else {
              foreach ($arrObjItemSessaoJulgamentoDTO[$numIdSessaoBloco] as $objItemSessaoJulgamentoDTO) {
                $objItemSessaoJulgamentoDTO->setNumOrdem($i++);
                $arrObjItemSessaoJulgamentoDTOOrdenado[]=$objItemSessaoJulgamentoDTO;
              }
            }
          }
          $arrObjItemSessaoJulgamentoDTO=$arrObjItemSessaoJulgamentoDTOOrdenado;
        }
      }
      $objSessaoJulgamentoDTO->setArrObjItemSessaoJulgamentoDTO($arrObjItemSessaoJulgamentoDTO);
    }
    return $objSessaoJulgamentoDTO;

  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @throws InfraException
   */
  protected function cancelarControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {
    try {
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_cancelar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objSessaoJulgamentoDTOBanco=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objSessaoJulgamentoDTOBanco->retDblIdProcedimento();
      $objSessaoJulgamentoDTOBanco->retDthSessao();
      $objSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objSessaoJulgamentoDTOBanco=$this->consultar($objSessaoJulgamentoDTOBanco);

      if (!in_array($objSessaoJulgamentoDTOBanco->getStrStaSituacao(),array(self::$ES_PAUTA_ABERTA,self::$ES_PAUTA_FECHADA))) {
        $objInfraException->lancarValidacao('Esta sess�o n�o pode mais ser cancelada.');
      }

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_IGNORADA,InfraDTO::$OPER_DIFERENTE);
      $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
      $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
        $objItemSessaoJulgamentoDTO->setStrMotivoRetirada('Sess�o de Julgamento cancelada.');
        $objItemSessaoJulgamentoDTO->setStrSinCancelamentoSessao('S');
      }
      $objItemSessaoJulgamentoRN->retirar($arrObjItemSessaoJulgamentoDTO);

      $objSessaoJulgamentoDTOBanco->setStrStaSituacao(self::$ES_CANCELADA);

      if ($objSessaoJulgamentoDTOBanco->getDblIdProcedimento()!=null) {
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_IGNORADA,InfraDTO::$OPER_DIFERENTE);
        $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
        $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
        $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
        $objItemSessaoJulgamentoDTO->retStrNomeColegiado();
        $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
        $objItemSessaoJulgamentoDTO->retStrNomeUsuarioRelator();
        $objItemSessaoJulgamentoDTO->retStrNomeTipoProcedimento();
        $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

        foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
          $objItemSessaoJulgamentoDTO->setDthSessaoSessaoJulgamento($objSessaoJulgamentoDTOBanco->getDthSessao());
          $objItemSessaoJulgamentoRN->gerarCertidaoCancelamento($objItemSessaoJulgamentoDTO);
        }
        $this->gerarDocumentoCancelamento($objSessaoJulgamentoDTOBanco);
      }

      $this->lancarAndamento($objSessaoJulgamentoDTOBanco);
      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTOBanco);
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_DOCBLOQ');

    } catch (Exception $e) {
      throw new InfraException('Erro cancelando Sess�o de Julgamento.', $e);
    }
  }
  protected function encerrarControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {
    $arrCorBarraProgresso=array('cor_fundo'=>'#5c9ccc','cor_borda'=>'#4297d7');
    $objInfraBarraProgresso=null;
    $dataEncerramento=InfraData::getStrDataHoraAtual();
    if(InfraBarraProgresso2::getInstances()!==null) {
      $objInfraBarraProgresso = InfraBarraProgresso2::newInstance('itens',$arrCorBarraProgresso);
    }

    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_encerrar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
      $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinOrdenarItens('S');

      $objSessaoJulgamentoDTOBanco = $this->pesquisar($objPesquisaSessaoJulgamentoDTO);
      $arrObjItemSessaoJulgamentoDTO=$objSessaoJulgamentoDTOBanco->getArrObjItemSessaoJulgamentoDTO();
      $arrIdDistribuicao=InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdDistribuicao');

      $numTotalItens=InfraArray::contar($arrObjItemSessaoJulgamentoDTO);

      if ($objSessaoJulgamentoDTOBanco->getStrStaSituacao()!= self::$ES_ABERTA) {
        $objInfraException->lancarValidacao('Esta sess�o n�o est� Aberta.');
      }

      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setNumMin(0);
        $objInfraBarraProgresso->setNumMax($numTotalItens);
      }
      $objPedidoVistaRN = new PedidoVistaRN();
      $objDistribuicaoRN=new DistribuicaoRN();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $numRegistrosAtual=1;

      if($numTotalItens>0) {
        $objPedidoVistaDTO = new PedidoVistaDTO();

        $objPedidoVistaDTO->setNumIdDistribuicao($arrIdDistribuicao, InfraDTO::$OPER_IN);
        $objPedidoVistaDTO->setDthDevolucao(null);
        $objPedidoVistaDTO->retNumIdDistribuicao();
        $objPedidoVistaDTO->retNumIdUsuario();
        $objPedidoVistaDTO->setStrSinPendente('N');
        $arrObjPedidoVista = InfraArray::indexarArrInfraDTO($objPedidoVistaRN->listar($objPedidoVistaDTO), 'IdDistribuicao');
      }

      foreach ($arrObjItemSessaoJulgamentoDTO as $chave=>$objItemSessaoJulgamentoDTO) {
        /* @var $objItemSessaoJulgamentoDTO ItemSessaoJulgamentoDTO */
        if($objInfraBarraProgresso) {
          $objInfraBarraProgresso->setStrRotulo('Encerrando julgamento ' . $numRegistrosAtual++ . ' de ' . $numTotalItens . '...');
          $objInfraBarraProgresso->moverProximo();
        }

        if(isset($arrObjPedidoVista[$objItemSessaoJulgamentoDTO->getNumIdDistribuicao()]) &&
           in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(),array(ItemSessaoJulgamentoRN::$STA_DILIGENCIA,
               ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO,ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA)) ){
          $arrObjPedidoVista[$objItemSessaoJulgamentoDTO->getNumIdDistribuicao()]->setDthDevolucao($dataEncerramento);
          $objPedidoVistaRN->devolver($arrObjPedidoVista[$objItemSessaoJulgamentoDTO->getNumIdDistribuicao()]);
        }
        $objDistribuicaoDTO = new DistribuicaoDTO();
        $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
        $staDistribuicaoAnterior=$objItemSessaoJulgamentoDTO->getStrStaDistribuicaoAnterior();
        switch($objItemSessaoJulgamentoDTO->getStrStaSituacao()){
          case ItemSessaoJulgamentoRN::$STA_ADIADO:
            $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_ADIADO);


            //se staDistribuicaoAnterior era pedido de vista, verifica andamento para ver se volta para vista ou vai para relator
            if ($staDistribuicaoAnterior==DistribuicaoRN::$STA_PEDIDO_VISTA){
              $objAtributoAndamentoSessaoRN=new AtributoAndamentoSessaoRN();
              $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
              $objAtributoAndamentoSessaoDTO->setNumIdSessaoJulgamentoAndamentoSessao($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
              $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
              $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
              $objAtributoAndamentoSessaoDTO->setNumMaxRegistrosRetorno(1);
              $objAtributoAndamentoSessaoDTO->setOrdDthExecucaoAndamentoSessao(InfraDTO::$TIPO_ORDENACAO_DESC);
              $objAtributoAndamentoSessaoDTO->retNumIdAndamentoSessao();
              $objAtributoAndamentoSessaoDTO=$objAtributoAndamentoSessaoRN->consultar($objAtributoAndamentoSessaoDTO);
              if($objAtributoAndamentoSessaoDTO) {
                $objAtributoAndamentoSessaoDTO->setStrChave('ENCAMINHAMENTO');
                $objAtributoAndamentoSessaoDTO->retStrValor();
                $objAtributoAndamentoSessaoDTO = $objAtributoAndamentoSessaoRN->consultar($objAtributoAndamentoSessaoDTO);
                if ($objAtributoAndamentoSessaoDTO && $objAtributoAndamentoSessaoDTO->getStrValor()=='R') {
                  //volta para relator, devolver pedido de vista
                  $objPedidoVistaRN=new PedidoVistaRN();
                  $objPedidoVistaDTO=$arrObjPedidoVista[$objItemSessaoJulgamentoDTO->getNumIdDistribuicao()];
                  $objPedidoVistaDTO->setDthDevolucao($dataEncerramento);
                  $objPedidoVistaDTO->setNumIdUsuario($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao());
                  $objPedidoVistaRN->devolver($objPedidoVistaDTO);
                  break;
                }
                //cancelar disponibilizacao de documento do usuario_sessao (voto-vista)
                $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
                $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
                $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
                $objItemSessaoDocumentoDTO->setNumIdUsuario($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao());
                $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
                $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
                if ($arrObjItemSessaoDocumentoDTO) {
                  $objItemSessaoDocumentoRN->excluir($arrObjItemSessaoDocumentoDTO);
                }
                //cancelar voto-vista e relacionados, se houver
                $objVotoParteRN=new VotoParteRN();
                $objVotoParteDTO=new VotoParteDTO();
                $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
                $objVotoParteDTO->setNumIdUsuario($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao());
                $objVotoParteDTO->setNumIdSessaoVoto($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
                $objVotoParteDTO->retNumIdVotoParte();
                $arrObjVotoParteDTO=$objVotoParteRN->listar($objVotoParteDTO);
                if($arrObjVotoParteDTO){
                  //consultar votos relacionados (pode ter mais de um se for julgamento fracionado)
                  foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
                    $objVotoParteDTO2=new VotoParteDTO();
                    $objVotoParteDTO2->setNumIdVotoParteAssociado($objVotoParteDTO->getNumIdVotoParte());
                    $objVotoParteDTO2->retNumIdVotoParte();
                    $arrObjVotoParteDTO2=$objVotoParteRN->listar($objVotoParteDTO2);
                    if($arrObjVotoParteDTO2) {
                      $objVotoParteRN->excluir($arrObjVotoParteDTO2);
                    }
                  }
                  $objVotoParteRN->excluir($arrObjVotoParteDTO);
                }
              }
              $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_PEDIDO_VISTA);
            }
            else {
              //verifica se voto do relator foi confirmado, se estiver como PROV_DISPONIBILIZADO, exclui voto e retira documento
              $objVotoParteRN=new VotoParteRN();
              $objVotoParteDTO=new VotoParteDTO();
              $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
              $objVotoParteDTO->setNumIdUsuario($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao());
              $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO);
              $objVotoParteDTO->retNumIdVotoParte();
              $objVotoParteDTO=$objVotoParteRN->consultar($objVotoParteDTO);
              if($objVotoParteDTO){
                $objVotoParteRN->excluir([$objVotoParteDTO]);
                //cancelar disponibilizacao de documento do usuario_sessao (rel_voto)
                $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
                $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
                $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
                $objItemSessaoDocumentoDTO->setNumIdUsuario($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao());
                $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
                $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
                if ($arrObjItemSessaoDocumentoDTO) {
                  $objItemSessaoDocumentoRN->excluir($arrObjItemSessaoDocumentoDTO);
                }
              }
            }

            break;
          case ItemSessaoJulgamentoRN::$STA_DILIGENCIA:
            $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_DILIGENCIA);
            $bolReabrirProcesso=true;
            $objSeiRN=new SeiRN();
            $objEntradaConsultarProcedimentoAPI=new EntradaConsultarProcedimentoAPI();
            $objEntradaConsultarProcedimentoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
            $objEntradaConsultarProcedimentoAPI->setSinRetornarUnidadesProcedimentoAberto('S');
            $objSaidaConsultarProcedimentoAPI= $objSeiRN->consultarProcedimento($objEntradaConsultarProcedimentoAPI);
            $arrObjUnidadeAPI=$objSaidaConsultarProcedimentoAPI->getUnidadesProcedimentoAberto();

            $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
            foreach ($arrObjUnidadeAPI as $objUnidadeProcedimentoAbertoAPI) {
              /* @var UnidadeProcedimentoAbertoAPI $objUnidadeProcedimentoAbertoAPI */
              if($objUnidadeProcedimentoAbertoAPI->getUnidade()->getIdUnidade()==$numIdUnidadeAtual){
                $bolReabrirProcesso=false;
                break;
              }
            }
            if ($bolReabrirProcesso){
              $objEntradaReabrirProcessoAPI=new EntradaReabrirProcessoAPI();
              $objEntradaReabrirProcessoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
              $objSeiRN->reabrirProcesso($objEntradaReabrirProcessoAPI);
            }

            $objAtributoAndamentoAPI=new AtributoAndamentoAPI();
            $objAtributoAndamentoAPI->setIdOrigem($objSessaoJulgamentoDTOBanco->getNumIdColegiado());
            $objAtributoAndamentoAPI->setNome('COLEGIADO');
            $objAtributoAndamentoAPI->setValor($objSessaoJulgamentoDTOBanco->getStrNomeColegiado());

            $objEntradaLancarAndamentoAPI=new EntradaLancarAndamentoAPI();
            $objEntradaLancarAndamentoAPI->setIdTarefaModulo(TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_CONVERSAO_DILIGENCIA);
            $objEntradaLancarAndamentoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
            $objEntradaLancarAndamentoAPI->setAtributos(array($objAtributoAndamentoAPI));

            $objSeiRN->lancarAndamento($objEntradaLancarAndamentoAPI);


            break;
          case ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO:
            if($objItemSessaoJulgamentoDTO->getStrDispositivo()==null){
              $objInfraException->lancarValidacao('Processo '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo().' n�o possui dispositivo.');
            }
            $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_JULGADO);
            $objItemSessaoJulgamentoRN->finalizarJulgamento($objItemSessaoJulgamentoDTO); //ja altera para julgado se ok
            break;
          case ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA:
            if($objItemSessaoJulgamentoDTO->getStrDispositivo()==null){
              $objInfraException->lancarValidacao('Processo '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo().' n�o possui dispositivo.');
            }
            $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_PEDIDO_VISTA);

            $objPedidoVistaDTO = new PedidoVistaDTO();
            $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
            $objPedidoVistaDTO->setDthDevolucao(null);
            $objPedidoVistaDTO->setStrSinPendente('S');
            $objPedidoVistaDTO->retNumIdPedidoVista();
            $objPedidoVistaDTO=$objPedidoVistaRN->consultar($objPedidoVistaDTO);

            $objPedidoVistaRN->confirmar($objPedidoVistaDTO);


            break;
          case ItemSessaoJulgamentoRN::$STA_RETIRADO:
            break;
          default:
            $strIdentificacaoItem=$objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco().' ';
            $strIdentificacaoItem.=$objItemSessaoJulgamentoDTO->getNumOrdem();
            $objInfraException->lancarValidacao('Processo '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo()." [$strIdentificacaoItem] pendente.");
        }

        if ($objItemSessaoJulgamentoDTO->getStrStaSituacao()!=ItemSessaoJulgamentoRN::$STA_RETIRADO) {
          $objDistribuicaoRN->alterarEstado($objDistribuicaoDTO);
        }

      }
      $objSessaoJulgamentoDTOBanco->setStrStaSituacao(self::$ES_ENCERRADA);
      $objSessaoJulgamentoDTOBanco->setDthFim($dataEncerramento);
      $this->lancarAndamento($objSessaoJulgamentoDTOBanco);
      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTOBanco);
      CacheSEI::getInstance()->removerAtributo('TRF4_SJ_DOCBLOQ');
      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setStrRotulo('Processamento conclu�do...');
        $objInfraBarraProgresso->moverProximo();
        sleep(2);
      }

      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ENCERRAMENTO);
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento(null);
      $objEventoSessaoDTO->setStrDescricao('');
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      EventoSessaoRN::lancar($objEventoSessaoDTO);

    } catch (Exception $e) {
      throw new InfraException('Erro encerrando Sess�o de Julgamento.', $e);
    }
  }
  public function finalizar(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){
    $bolAcumulacaoPrevia = FeedSEIProtocolos::getInstance()->isBolAcumularFeeds();
    FeedSEIProtocolos::getInstance()->setBolAcumularFeeds(true);
    $this->finalizarInterno($objSessaoJulgamentoDTO);
    if (!$bolAcumulacaoPrevia){
      FeedSEIProtocolos::getInstance()->setBolAcumularFeeds(false);
      FeedSEIProtocolos::getInstance()->indexarFeeds();
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @throws InfraException
   */
  protected function finalizarInternoControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {
    $arrCorBarraProgresso=array('cor_fundo'=>'#5c9ccc','cor_borda'=>'#4297d7');
    $objInfraBarraProgresso=null;
    if(InfraBarraProgresso2::getInstances()!==null) {
      $objInfraBarraProgresso = InfraBarraProgresso2::newInstance('itens',$arrCorBarraProgresso);
    }

    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_finalizar',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
      $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinOrdenarItens('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinPedidoVista('S');

      $objSessaoJulgamentoDTOBanco = $this->pesquisar($objPesquisaSessaoJulgamentoDTO);
      $arrObjColegiadoComposicaoDTO=$objSessaoJulgamentoDTOBanco->getArrObjColegiadoComposicaoDTO();
      $arrObjItemSessaoJulgamentoDTO=$objSessaoJulgamentoDTOBanco->getArrObjItemSessaoJulgamentoDTO();

      $this->bloquear($objSessaoJulgamentoDTOBanco);
      if ($objSessaoJulgamentoDTOBanco->getStrStaSituacao()!= self::$ES_ENCERRADA) {
        if ($objSessaoJulgamentoDTOBanco->getStrStaSituacao() == self::$ES_FINALIZADA) {
          $objInfraException->lancarValidacao('Esta sess�o j� foi finalizada.');
        } else {
          throw new InfraException('Esta sess�o n�o est� Encerrada.');
        }
      }

      $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUsuario');


      //validar registro de ausencias
      $arrTitulares=array();
      foreach ($arrObjColegiadoComposicaoDTO as $idUsuario=>$objColegiadoComposicaoDTO) {
        if($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR){
          $arrTitulares[$idUsuario]=false;
        }
      }
      $objPresencaSessaoDTO=new PresencaSessaoDTO();
      $objPresencaSessaoRN = new PresencaSessaoRN();
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objPresencaSessaoDTO->adicionarCriterio(array('Saida','Saida'),array(InfraDTO::$OPER_MAIOR,InfraDTO::$OPER_IGUAL),array($objPresencaSessaoDTO->getObjInfraAtributoDTO('InicioSessaoJulgamento'),null),InfraDTO::$OPER_LOGICO_OR);
      $objPresencaSessaoDTO->retNumIdUsuario();
      $objPresencaSessaoDTO->setDistinct(true);
      $arrObjPresencaSessaoDTO = $objPresencaSessaoRN->listar($objPresencaSessaoDTO);

      if (InfraArray::contar($arrObjPresencaSessaoDTO) > 0) {
        foreach ($arrObjPresencaSessaoDTO as $objPresencaSessaoDTO) {
          unset($arrTitulares[$objPresencaSessaoDTO->getNumIdUsuario()]);
        }
      }

      if(InfraArray::contar($arrTitulares)>0) {
        $objAusenciaSessaoDTO = new AusenciaSessaoDTO();
        $objAusenciaSessaoRN = new AusenciaSessaoRN();
        $objAusenciaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objAusenciaSessaoDTO->retTodos();
        $objAusenciaSessaoDTO->retStrDescricaoMotivoAusencia();
        $arrObjAusenciaSessaoDTO = $objAusenciaSessaoRN->listar($objAusenciaSessaoDTO);
        if($arrObjAusenciaSessaoDTO){
          $arrObjAusenciaSessaoDTO=InfraArray::indexarArrInfraDTO($arrObjAusenciaSessaoDTO,'IdUsuario');
        }
        foreach ($arrObjAusenciaSessaoDTO as $objAusenciaSessaoDTO) {
          unset($arrTitulares[$objAusenciaSessaoDTO->getNumIdUsuario()]);
        }
      }
      if(!isset($arrObjAusenciaSessaoDTO)) {
        $arrObjAusenciaSessaoDTO=array();
      }
      if(InfraArray::contar($arrTitulares)>0) {
        $objInfraException->lancarValidacao('Falta registrar motivo de aus�ncia de membros.');
      }


      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setNumMin(0);
        $objInfraBarraProgresso->setNumMax(InfraArray::contar($arrObjItemSessaoJulgamentoDTO));
      }
      //buscar pedidos de vista

      $arrObjPedidosVistaDTO=$objSessaoJulgamentoDTOBanco->getArrObjPedidoVistaDTO();
//      InfraArray::indexarArrInfraDTO($arrObjPedidosVistaDTO,'IdDistribuicao');

      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objDistribuicaoRN=new DistribuicaoRN();
      $numRegistrosAtual=1;
      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
        /* @var $objItemSessaoJulgamentoDTO ItemSessaoJulgamentoDTO */
        if($objInfraBarraProgresso) {
          $objInfraBarraProgresso->setStrRotulo('Finalizando julgamento ' . $numRegistrosAtual++ . ' de ' . InfraArray::contar($arrObjItemSessaoJulgamentoDTO) . '...');
          $objInfraBarraProgresso->moverProximo();
        }
        $strRessalvas=$objItemSessaoJulgamentoRN->montarTextoRessalvas($objItemSessaoJulgamentoDTO);
        switch($objItemSessaoJulgamentoDTO->getStrStaSituacao()){
          case ItemSessaoJulgamentoRN::$STA_ADIADO:
//            $objItemSessaoJulgamentoDTO->setStrDispositivo('Julgamento adiado.');
            $strRessalvas='';
            break;
          case ItemSessaoJulgamentoRN::$STA_DILIGENCIA:
//            $objItemSessaoJulgamentoDTO->setStrDispositivo('Julgamento convertido em dilig�ncia.');
            $strRessalvas='';
            //enviar para usu�rio que efetuou distribuicao
            $objDistribuicaoDTO = new DistribuicaoDTO();
            $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
            $objDistribuicaoDTO->retNumIdUsuario();
            $objDistribuicaoDTO->retNumIdUnidade();
            $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);
            $this->encaminharProcesso($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao(),$objDistribuicaoDTO->getNumIdUsuario(),$objDistribuicaoDTO->getNumIdUnidade());
            break;
          case ItemSessaoJulgamentoRN::$STA_JULGADO:
            //se relator_acordao definido encaminhar processo para ele
            if ($objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao()!=null){
              $objColegiadoComposicaoDTO=$arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao()];
              if($objColegiadoComposicaoDTO==null){
                $strDescricaoItem=$objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco();
                $strDescricaoItem.= ' '.$objItemSessaoJulgamentoDTO->getNumOrdem();
                throw new InfraException($strDescricaoItem.': N�o � poss�vel definir como relator de ac�rd�o membro que n�o faz mais parte do colegiado.');
              }
              $objItemSessaoJulgamentoDTO->setNumIdUnidadeRelatorAcordaoDistribuicao($objColegiadoComposicaoDTO->getNumIdUnidade());
              $this->encaminharProcesso($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao(),$objColegiadoComposicaoDTO->getNumIdUsuario(),$objColegiadoComposicaoDTO->getNumIdUnidade());
            } else{
              $objItemSessaoJulgamentoDTO->setNumIdUnidadeRelatorAcordaoDistribuicao($objItemSessaoJulgamentoDTO->getNumIdUnidadeRelatorDistribuicao());
            }
//            $objItemSessaoJulgamentoRN->gerarAcordao($objItemSessaoJulgamentoDTO);
            break;
          case ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA:
            //encaminhar processo para o solicitante de vistas
            //cadastrar pedido vista
            $objPedidoVistaDTO=$arrObjPedidosVistaDTO[$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()];
                $objColegiadoComposicaoDTO=$arrObjColegiadoComposicaoDTO[$objPedidoVistaDTO->getNumIdUsuario()];
                $this->encaminharProcesso($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao(),$objColegiadoComposicaoDTO->getNumIdUsuario(),$objColegiadoComposicaoDTO->getNumIdUnidade());

            $objItemSessaoJulgamentoDTO->unSetArrObjJulgamentoParteDTO();
            $objItemSessaoJulgamentoDTO2=$objItemSessaoJulgamentoRN->acompanharJulgamento($objItemSessaoJulgamentoDTO);
            //se possuir dispositivo usa o atual, sen�o gera automaticamente.
            if ($objItemSessaoJulgamentoDTO->getStrDispositivo()==null) {
              $objItemSessaoJulgamentoDTO->setStrDispositivo($objItemSessaoJulgamentoRN->gerarTextoDispositivo($objItemSessaoJulgamentoDTO2));
              $objItemSessaoJulgamentoDTO3=new ItemSessaoJulgamentoDTO();
              $objItemSessaoJulgamentoDTO3->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
              $objItemSessaoJulgamentoDTO3->setStrDispositivo($objItemSessaoJulgamentoDTO->getStrDispositivo());
              $objItemSessaoJulgamentoDTO3->setNumIdUsuarioRelatorAcordaoDistribuicao($objItemSessaoJulgamentoDTO2->getNumIdUsuarioRelatorAcordaoDistribuicao());
              $objItemSessaoJulgamentoRN->alterar($objItemSessaoJulgamentoDTO3);
            }
            break;
          case ItemSessaoJulgamentoRN::$STA_RETIRADO:
            $strRessalvas='';
            break;
          default:
            $strIdentificacaoItem=$objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco().' ';
            $strIdentificacaoItem.=$objItemSessaoJulgamentoDTO->getNumOrdem();
            $objInfraException->lancarValidacao('Processo '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo()." [$strIdentificacaoItem] pendente.");
        }

        if($strRessalvas!==''){
          $objItemSessaoJulgamentoDTO->setStrDispositivo($objItemSessaoJulgamentoDTO->getStrDispositivo()."\n\nRessalvas:\n".$strRessalvas);
        }
        //gerar certid�o de julgamento
        $objItemSessaoJulgamentoDTO->setStrNomeColegiado($objSessaoJulgamentoDTOBanco->getStrNomeColegiado());
        $objItemSessaoJulgamentoDTO->setDthSessaoSessaoJulgamento($objSessaoJulgamentoDTOBanco->getDthSessao());
        $numIdUsuario=$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
        if(!isset($arrObjColegiadoComposicaoDTO[$numIdUsuario])){
          //buscar voto -> id_colegiado_versao != id_colegiado_versao da sessao atual
          //            -> id_usuario = numIdUsuario
          $objVotoParteDTO=new VotoParteDTO();
          $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
          $objVotoParteDTO->setNumIdUsuario($numIdUsuario);
          $objVotoParteDTO->retNumIdColegiadoVersaoSessaoJulgamentoVoto();
          $objVotoParteDTO->retNumIdUsuario();
          $objVotoParteDTO->setOrdDthVoto(InfraDTO::$TIPO_ORDENACAO_DESC);
          $objVotoParteDTO->setNumMaxRegistrosRetorno(1);
          $objVotoParteRN=new VotoParteRN();
          $objVotoParteDTO=$objVotoParteRN->consultar($objVotoParteDTO);
          if($objVotoParteDTO!=null){
            $numIdColegiadoVersao=  $objVotoParteDTO->getNumIdColegiadoVersaoSessaoJulgamentoVoto();
          } else {
            // julgamento manual - buscar a partir da distribuicao
            $objDistribuicaoDTO=new DistribuicaoDTO();
            $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
            $objDistribuicaoDTO->retNumIdColegiadoVersao();
            $objDistribuicaoRN=new DistribuicaoRN();
            $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);
            $numIdColegiadoVersao=$objDistribuicaoDTO->getNumIdColegiadoVersao();
          }


          $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
          $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($numIdColegiadoVersao);
          $objColegiadoComposicaoDTO->retTodos();
          $objColegiadoComposicaoDTO->retStrExpressaoCargo();
          $objColegiadoComposicaoDTO->retStrStaGeneroCargo();
          $objColegiadoComposicaoDTO->retStrNomeUsuario();
          $objColegiadoComposicaoDTO->setNumIdUsuario($numIdUsuario);
          $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
          $objColegiadoComposicaoDTO=$objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);
          $objColegiadoComposicaoDTO->setBolComposicaoAnterior(true);
          $objColegiadoComposicaoDTO->setStrSinPresente('N');
          $arrObjColegiadoComposicaoDTO[$numIdUsuario]=$objColegiadoComposicaoDTO;
        }
        $objItemSessaoJulgamentoDTO->setStrNomeUsuarioRelator($arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao()]->getStrNomeUsuario());
        $objItemSessaoJulgamentoRN->gerarCertidaoJulgamento($objItemSessaoJulgamentoDTO);
        //colocar no bloco de assinatura
//        $objRelBlocoProtocoloDTO = new RelBlocoProtocoloDTO();
//        $objRelBlocoProtocoloDTO->setDblIdProtocolo($objDocumentoDTO->getDblIdDocumento());
//        $objRelBlocoProtocoloDTO->setStrAnotacao(null);
//        $arrObjRelBlocoProtocoloDTO[] = $objRelBlocoProtocoloDTO;

      }

      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setStrRotulo('Processamento de Itens conclu�do.');
        $objInfraBarraProgresso = InfraBarraProgresso2::newInstance('sessao', $arrCorBarraProgresso);
        $objInfraBarraProgresso->setNumMin(0);
        $objInfraBarraProgresso->setNumMax(5);
      }


//      //gerar bloco de assinatura
      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setStrRotulo('Gerando Bloco de Assinatura...');
        $objInfraBarraProgresso->moverProximo();
      }
//      $objBlocoDTO = new BlocoDTO();
//      $objBlocoRN=new BlocoRN();
//      $objBlocoDTO->setNumIdBloco(null);
//      $objBlocoDTO->setStrStaTipo(BlocoRN::$TB_ASSINATURA);
//      $objBlocoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
//      $objBlocoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
//      $objBlocoDTO->setStrDescricao($objSessaoJulgamentoDTOBanco->getStrNomeColegiado().' - '.substr($objSessaoJulgamentoDTOBanco->getDthSessao(),0,-3));
//      $objBlocoDTO->setStrIdxBloco(null);
//      $objBlocoDTO->setStrStaEstado(BlocoRN::$TE_ABERTO);
//      $objBlocoDTO->setArrObjRelBlocoUnidadeDTO(array());
//      $objBlocoDTO = $objBlocoRN->cadastrarRN1273($objBlocoDTO);

      $objProcedimentoRN = new ProcedimentoRN();


      //relacionar processos dos itens
      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setStrRotulo('Relacionando Processos...');
        $objInfraBarraProgresso->moverProximo();
      }
      $objRelProtocoloProtocoloRN=new RelProtocoloProtocoloRN();
      $idProcedimentoSessao=$objSessaoJulgamentoDTOBanco->getDblIdProcedimento();
      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
        if (!in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(),array(ItemSessaoJulgamentoRN::$STA_RETIRADO,ItemSessaoJulgamentoRN::$STA_IGNORADA))) {
          $idProcedimentoItem=$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao();
          if($idProcedimentoItem==$idProcedimentoSessao) {//caso o processo do julgamento seja o mesmo do item
            continue;
          }
          $objRelProtocoloProtocoloDTO = new RelProtocoloProtocoloDTO();
          $objRelProtocoloProtocoloDTO->adicionarCriterio(array('IdProtocolo1','IdProtocolo2'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array($idProcedimentoSessao,$idProcedimentoItem),InfraDTO::$OPER_LOGICO_AND,'prim');
          $objRelProtocoloProtocoloDTO->adicionarCriterio(array('IdProtocolo1','IdProtocolo2'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array($idProcedimentoItem,$idProcedimentoSessao),InfraDTO::$OPER_LOGICO_AND,'sec');
          $objRelProtocoloProtocoloDTO->agruparCriterios(array('prim','sec'),InfraDTO::$OPER_LOGICO_OR);
          $objRelProtocoloProtocoloDTO->setStrStaAssociacao(RelProtocoloProtocoloRN::$TA_PROCEDIMENTO_RELACIONADO);
          if ($objRelProtocoloProtocoloRN->contarRN0843($objRelProtocoloProtocoloDTO)==0){
            $objRelProtocoloProtocoloDTO = new RelProtocoloProtocoloDTO();
            $objRelProtocoloProtocoloDTO->setDblIdProtocolo1($idProcedimentoSessao);
            $objRelProtocoloProtocoloDTO->setDblIdProtocolo2($idProcedimentoItem);
            $objRelProtocoloProtocoloDTO->setStrStaAssociacao(RelProtocoloProtocoloRN::$TA_PROCEDIMENTO_RELACIONADO);
            $objProcedimentoRN->relacionarProcedimentoRN1020($objRelProtocoloProtocoloDTO);
          }
        }
      }

      //----------------------------------------------------------------------------------
      // GERAR documento - Ata de Julgamento
      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setStrRotulo('Gerando Ata de Julgamento...');
        $objInfraBarraProgresso->moverProximo();
      }
      $objDocumentoDTO = $this->gerarAtaSessao($objSessaoJulgamentoDTOBanco, $arrObjPresencaSessaoDTO, $arrObjColegiadoComposicaoDTO, $arrObjAusenciaSessaoDTO, $arrObjItemSessaoJulgamentoDTO);

      //incluir documentos no bloco de assinatura.
      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setStrRotulo('Incluindo Documentos no Bloco de Assinatura...');
        $objInfraBarraProgresso->moverProximo();
      }
//
//      foreach ($arrObjRelBlocoProtocoloDTO as $objRelBlocoProtocoloDTO) {
//        $objRelBlocoProtocoloDTO->setNumIdBloco($objBlocoDTO->getNumIdBloco());
//      }
//      $objRelBlocoProtocoloDTO = new RelBlocoProtocoloDTO();
//      $objRelBlocoProtocoloDTO->setNumIdBloco($objBlocoDTO->getNumIdBloco());
//      $objRelBlocoProtocoloDTO->setDblIdProtocolo($objDocumentoDTO->getDblIdDocumento());
//      $objRelBlocoProtocoloDTO->setStrAnotacao(null);
//      $arrObjRelBlocoProtocoloDTO[] = $objRelBlocoProtocoloDTO;
//
//      $objRelBlocoProtocoloRN = new RelBlocoProtocoloRN();
//      $objRelBlocoProtocoloRN->cadastrarMultiplo($arrObjRelBlocoProtocoloDTO);

      $objSessaoJulgamentoDTOBanco->setDblIdDocumentoSessao($objDocumentoDTO->getDblIdDocumento());
      $objSessaoJulgamentoDTOBanco->setStrStaSituacao(self::$ES_FINALIZADA);
      $this->lancarAndamento($objSessaoJulgamentoDTOBanco);
      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTOBanco);
      if($objInfraBarraProgresso) {
        $objInfraBarraProgresso->setStrRotulo('Processamento conclu�do...');
        $objInfraBarraProgresso->moverProximo();
        sleep(2);
      }

    } catch (Exception $e) {
      throw new InfraException('Erro finalizando Sess�o de Julgamento.', $e);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @throws InfraException
   */
  protected function alterarPresidenteControlado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){

    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sessao_julgamento_alterar_presidente',__METHOD__,$objSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdUsuarioPresidente($objSessaoJulgamentoDTO,$objInfraException);

      $objInfraException->lancarValidacoes();

      //n�o permitir altera��o em concluidos
      $objSessaoJulgamentoDTOBanco=new SessaoJulgamentoDTO();
      $objSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objSessaoJulgamentoDTOBanco->retNumIdUsuarioPresidente();
      $objSessaoJulgamentoDTOBanco->retStrNomePresidenteSessao();
      $objSessaoJulgamentoDTOBanco=$this->consultar($objSessaoJulgamentoDTOBanco);

      $staAtual=$objSessaoJulgamentoDTOBanco->getStrStaSituacao();
      if (in_array($staAtual,array(self::$ES_ENCERRADA,self::$ES_FINALIZADA))) {
        $objInfraException->lancarValidacao('Sess�o conclu�da n�o permite altera��o.');
      }
      if (!in_array($staAtual,array(self::$ES_PAUTA_FECHADA,self::$ES_ABERTA,self::$ES_SUSPENSA))){
        $objInfraException->lancarValidacao('Sess�o n�o permite altera��o de Presidente.');
      }

      $objPresencaSessaoDTO=new PresencaSessaoDTO();
      $objPresencaSessaoRN=new PresencaSessaoRN();
      $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objPresencaSessaoDTO->setNumIdUsuario($objSessaoJulgamentoDTO->getNumIdUsuarioPresidente());
      $objPresencaSessaoDTO->setDthSaida(null);
      $objPresencaSessaoDTO->retNumIdUsuario();
      $objPresencaSessaoDTO->retStrNomeUsuario();

      $objPresencaSessaoDTO=$objPresencaSessaoRN->consultar($objPresencaSessaoDTO);
      if ($objPresencaSessaoDTO==null){
        $objInfraException->lancarValidacao('N�o � permitido definir Presidente ausente.');
      }

      $objSessaoJulgamentoDTOBanco->setNumIdUsuarioPresidente($objSessaoJulgamentoDTO->getNumIdUsuarioPresidente());
      $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
      $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTOBanco);

      //lancar andamento
      $objAndamentoSessaoDTO=new AndamentoSessaoDTO();
      $objAndamentoSessaoRN=new AndamentoSessaoRN();
      $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_PRESIDENTE_ALTERADO);

      $objAtributoAndamentoSessaoDTO->setStrChave('PRESIDENTE');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objSessaoJulgamentoDTO->getNumIdUsuarioPresidente());
      $objAtributoAndamentoSessaoDTO->setStrValor($objPresencaSessaoDTO->getStrNomeUsuario());
      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO(array($objAtributoAndamentoSessaoDTO));
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Presidente da Sess�o de Julgamento.',$e);
    }

  }

  private function lancarAndamento(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {
    $objAndamentoSessaoDTO=new AndamentoSessaoDTO();
    $objAndamentoSessaoRN=new AndamentoSessaoRN();
    $arrObjAtributoAndamentoSessaoDTO=array();
    $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
    $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_SITUACAO_SESSAO);

    $objAtributoAndamentoSessaoDTO->setStrChave('SITUACAO');
    $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objSessaoJulgamentoDTO->getStrStaSituacao());
    switch($objSessaoJulgamentoDTO->getStrStaSituacao()){
      case SessaoJulgamentoRN::$ES_ABERTA:
        $valor='Sess�o Aberta (Presidente '.$objSessaoJulgamentoDTO->getStrNomePresidenteSessao().')';
        break;
      case SessaoJulgamentoRN::$ES_ENCERRADA:
        $valor='Conclu�da';
        break;
      case SessaoJulgamentoRN::$ES_FINALIZADA:
        $valor='Finalizada';
        break;
      case SessaoJulgamentoRN::$ES_PAUTA_ABERTA:
        $valor='Pauta Aberta';
        break;
      case SessaoJulgamentoRN::$ES_PAUTA_FECHADA:
        $valor='Pauta Fechada';
        $objAtributoAndamentoSessaoDTO2=new AtributoAndamentoSessaoDTO();
        $objAtributoAndamentoSessaoDTO2->setStrChave('HASHPAUTA');
        $objAtributoAndamentoSessaoDTO2->setStrValor($objSessaoJulgamentoDTO->getStrHashPauta());
        $objAtributoAndamentoSessaoDTO2->setStrIdOrigem(null);
        $arrObjAtributoAndamentoSessaoDTO[]=$objAtributoAndamentoSessaoDTO2;
        break;
      case SessaoJulgamentoRN::$ES_SUSPENSA:
        $valor='Suspensa';
        break;
      case SessaoJulgamentoRN::$ES_CANCELADA:
        $valor='Cancelada';
        break;
      case SessaoJulgamentoRN::$ES_PREVISTA:
        $valor='Sess�o Prevista cadastrada';
        break;
      default:
        $valor='';
    }
    $objAtributoAndamentoSessaoDTO->setStrValor($valor);
    $arrObjAtributoAndamentoSessaoDTO[]=$objAtributoAndamentoSessaoDTO;
    $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
    $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

    $objEventoSessaoDTO = new EventoSessaoDTO();
    $objEventoSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objEventoSessaoDTO->setNumIdItemSessaoJulgamento(null);
    $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_STATUS_SESSAO);
    $objEventoSessaoDTO->setStrDescricao($valor);
    EventoSessaoRN::lancar($objEventoSessaoDTO);

  }

  /**
   * @param $idProcedimento
   * @param $idUsuario
   * @param $idUnidade
   * @throws InfraException
   */
  private function encaminharProcesso($idProcedimento, $idUsuario, $idUnidade){

    $objProcedimentoRN = new ProcedimentoRN();
    $objProcedimentoDTO=new ProcedimentoDTO();
    $objProcedimentoDTO->setDblIdProcedimento($idProcedimento);
    $objProcedimentoDTO->retStrProtocoloProcedimentoFormatado();
    $objProcedimentoDTO->retStrNomeTipoProcedimento();
    $objProcedimentoDTO->retStrStaNivelAcessoGlobalProtocolo();
    $objProcedimentoDTO=$objProcedimentoRN->consultarRN0201($objProcedimentoDTO);

    //n�o � permitido encaminhar sigilosos
    if($objProcedimentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO){
      return;
    }

    $objAtividadeRN=new AtividadeRN();
    $objPesquisaPendenciaDTO = new PesquisaPendenciaDTO();
    $objPesquisaPendenciaDTO->setDblIdProtocolo(array($idProcedimento));
    $objPesquisaPendenciaDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
    $objPesquisaPendenciaDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $arrObjProcedimentoDTO = $objAtividadeRN->listarPendenciasRN0754($objPesquisaPendenciaDTO);

    if (InfraArray::contar($arrObjProcedimentoDTO)==0) {
      $objReabrirProcessoDTO = new ReabrirProcessoDTO();
      $objReabrirProcessoDTO->setDblIdProcedimento($idProcedimento);
      $objReabrirProcessoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objReabrirProcessoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $arrObjAtividadeDTO = array($objProcedimentoRN->reabrirRN0966($objReabrirProcessoDTO));
      $strSinManterAberto = 'N';
    }else{
      $arrObjAtividadeDTO = $arrObjProcedimentoDTO[0]->getArrObjAtividadeDTO();
      $strSinManterAberto = 'S';
    }

    $objAtividadeDTO = new AtividadeDTO();
    $objAtividadeDTO->setDblIdProtocolo($idProcedimento);
    $objAtividadeDTO->setNumIdUsuario(null);
    $objAtividadeDTO->setNumIdUsuarioOrigem(SessaoSEI::getInstance()->getNumIdUsuario());
    $objAtividadeDTO->setNumIdUnidade($idUnidade);
    $objAtividadeDTO->setNumIdUnidadeOrigem(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objAtividadeDTO->setDtaPrazo(null);

    if ($objProcedimentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO){
      $objConcederCredencialDTO = new ConcederCredencialDTO();
      $objConcederCredencialDTO->setDblIdProcedimento($idProcedimento);
      $objConcederCredencialDTO->setNumIdUsuario($idUsuario);
      $objConcederCredencialDTO->setNumIdUnidade($idUnidade);
      $objConcederCredencialDTO->setArrAtividadesOrigem($arrObjAtividadeDTO);
      $objAtividadeRN->concederCredencial($objConcederCredencialDTO);
    } else {
      $objEnviarProcessoDTO = new EnviarProcessoDTO();
      $objEnviarProcessoDTO->setArrAtividadesOrigem($arrObjAtividadeDTO);
      $objEnviarProcessoDTO->setArrAtividades(array($objAtividadeDTO));
      $objEnviarProcessoDTO->setStrSinManterAberto($strSinManterAberto);
      $objEnviarProcessoDTO->setStrSinEnviarEmailNotificacao('N');
      $objEnviarProcessoDTO->setStrSinRemoverAnotacoes('N');
      $objAtividadeRN->enviarRN0023($objEnviarProcessoDTO);
    }
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return ProtocoloDTO
   * @throws InfraException
   */
  private function gerarProcedimento(SessaoJulgamentoDTO $objSessaoJulgamentoDTO){

    $objTipoProcedimentoDTO = new TipoProcedimentoDTO();
    $objTipoProcedimentoDTO->retNumIdTipoProcedimento();
    $objTipoProcedimentoDTO->retStrStaNivelAcessoSugestao();
    $objTipoProcedimentoDTO->retStrStaGrauSigiloSugestao();
    $objTipoProcedimentoDTO->retNumIdHipoteseLegalSugestao();
    $objTipoProcedimentoDTO->setNumIdTipoProcedimento($objSessaoJulgamentoDTO->getNumIdTipoProcedimentoColegiado());

    $objTipoProcedimentoRN = new TipoProcedimentoRN();
    $objTipoProcedimentoDTO = $objTipoProcedimentoRN->consultarRN0267($objTipoProcedimentoDTO);

    //Processo
    $objProcedimentoDTO = new ProcedimentoDTO();
    $objProcedimentoDTO->setDblIdProcedimento(null);
    $objProcedimentoDTO->setNumIdTipoProcedimento($objTipoProcedimentoDTO->getNumIdTipoProcedimento());
    $objProcedimentoDTO->setStrSinGerarPendencia('S');

    //protocolo
    $objProtocoloDTO = new ProtocoloDTO();
    $objProtocoloDTO->setStrDescricao($objSessaoJulgamentoDTO->getStrNomeColegiado().' - '.substr($objSessaoJulgamentoDTO->getDthSessao(),0,-3));
    $objProtocoloDTO->setNumIdUnidadeGeradora(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objProtocoloDTO->setNumIdUsuarioGerador(SessaoSEI::getInstance()->getNumIdUsuario());
    $objProtocoloDTO->setDtaGeracao(InfraData::getStrDataAtual());
    $objProtocoloDTO->setStrStaProtocolo(ProtocoloRN::$TP_PROCEDIMENTO);
    $objProtocoloDTO->setStrStaNivelAcessoLocal($objTipoProcedimentoDTO->getStrStaNivelAcessoSugestao());
    $objProtocoloDTO->setStrStaGrauSigilo($objTipoProcedimentoDTO->getStrStaGrauSigiloSugestao());
    $objProtocoloDTO->setNumIdHipoteseLegal($objTipoProcedimentoDTO->getNumIdHipoteseLegalSugestao());

    //Busca e adiciona os assuntos sugeridos para o tipo da sessao
    $objRelTipoProcedimentoAssuntoDTO = new RelTipoProcedimentoAssuntoDTO();
    $objRelTipoProcedimentoAssuntoDTO->retNumIdAssunto();
    $objRelTipoProcedimentoAssuntoDTO->retNumSequencia();
    $objRelTipoProcedimentoAssuntoDTO->setNumIdTipoProcedimento($objTipoProcedimentoDTO->getNumIdTipoProcedimento());

    $objRelTipoProcedimentoAssuntoRN = new RelTipoProcedimentoAssuntoRN();
    $arrObjRelTipoProcedimentoAssuntoDTO = $objRelTipoProcedimentoAssuntoRN->listarRN0192($objRelTipoProcedimentoAssuntoDTO);
    $arrObjAssuntoDTO = array();
    foreach($arrObjRelTipoProcedimentoAssuntoDTO as $objRelTipoProcedimentoAssuntoDTO){
      $objRelProtocoloAssuntoDTO = new RelProtocoloAssuntoDTO();
      $objRelProtocoloAssuntoDTO->setNumIdAssunto($objRelTipoProcedimentoAssuntoDTO->getNumIdAssunto());
      $objRelProtocoloAssuntoDTO->setNumSequencia($objRelTipoProcedimentoAssuntoDTO->getNumSequencia());
      $arrObjAssuntoDTO[] = $objRelProtocoloAssuntoDTO;
    }
    $objProtocoloDTO->setArrObjRelProtocoloAssuntoDTO($arrObjAssuntoDTO);

    //incluir secretaria do colegiado como interessado #205319
    $objUnidadeDTO=new UnidadeDTO();
    $objUnidadeDTO->setNumIdUnidade($objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado());
    $objUnidadeDTO->retNumIdContato();
    $objUnidadeRN=new UnidadeRN();
    $objUnidadeDTO=$objUnidadeRN->consultarRN0125($objUnidadeDTO);

    $objParticipanteDTO=new ParticipanteDTO();
    $objParticipanteDTO->setStrStaParticipacao(ParticipanteRN::$TP_INTERESSADO);
    $objParticipanteDTO->setNumIdContato($objUnidadeDTO->getNumIdContato());
    $objParticipanteDTO->setNumSequencia(0);
    $objProtocoloDTO->setArrObjParticipanteDTO(array($objParticipanteDTO));

    $objProtocoloDTO->setArrObjObservacaoDTO(array());
    $objProcedimentoDTO->setObjProtocoloDTO($objProtocoloDTO);

    $objProcedimentoRN = new ProcedimentoRN();
    $objProcedimentoDTO = $objProcedimentoRN->gerarRN0156($objProcedimentoDTO);

    //relacionar processos do mesmo colegiado/ano
    $anoAtual=substr(InfraData::getStrDataAtual(),6,4);
    $objSessaoJulgamentoDTO2=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO2->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento(),InfraDTO::$OPER_DIFERENTE);
    $objSessaoJulgamentoDTO2->setNumIdColegiado($objSessaoJulgamentoDTO->getNumIdColegiado());
    $objSessaoJulgamentoDTO2->adicionarCriterio(array('Sessao','Sessao'),array(InfraDTO::$OPER_MAIOR_IGUAL,InfraDTO::$OPER_MENOR_IGUAL),array('01/01/'.$anoAtual.' 00:00:00','31/12/'.$anoAtual.' 23:59:59'),InfraDTO::$OPER_LOGICO_AND);
    $objSessaoJulgamentoDTO2->retDblIdProcedimento();
    $objSessaoJulgamentoDTO2->setDblIdProcedimento(null,InfraDTO::$OPER_DIFERENTE);
    $arrObjSessaoJulgamentoDTO=$this->listar($objSessaoJulgamentoDTO2);
    foreach ($arrObjSessaoJulgamentoDTO as $objSessaoJulgamentoDTO2)	{
      $objRelProtocoloProtocoloDTO = new RelProtocoloProtocoloDTO();
      $objRelProtocoloProtocoloDTO->setDblIdProtocolo1($objSessaoJulgamentoDTO2->getDblIdProcedimento());
      $objRelProtocoloProtocoloDTO->setDblIdProtocolo2($objProcedimentoDTO->getDblIdProcedimento());
      $objRelProtocoloProtocoloDTO->setStrStaAssociacao(RelProtocoloProtocoloRN::$TA_PROCEDIMENTO_RELACIONADO);
      $objProcedimentoRN->relacionarProcedimentoRN1020($objRelProtocoloProtocoloDTO);
    }

    $objSessaoJulgamentoDTO2=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTO2->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objSessaoJulgamentoDTO2->setDblIdProcedimento($objProcedimentoDTO->getDblIdProcedimento());
    $objSessaoJulgamentoBD = new SessaoJulgamentoBD($this->getObjInfraIBanco());
    $objSessaoJulgamentoBD->alterar($objSessaoJulgamentoDTO2);


    return $objProtocoloDTO;

  }

  /**
   * @param SessaoJulgamentoDTO $parObjSessaoJulgamentoDTO
   * @return void |null
   * @throws InfraException
   */
  private function gerarDocumentoPauta(SessaoJulgamentoDTO $parObjSessaoJulgamentoDTO)
  {

    $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
    $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($parObjSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');

    $objSessaoJulgamentoDTO = $this->pesquisar($objPesquisaSessaoJulgamentoDTO);

    $bolSessaoVirtual=$objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S';
    $arrObjItemSessaoJulgamentoDTO=$objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
    if(!$bolSessaoVirtual){
      $arrObjSessaoBlocoDTO=CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($parObjSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $arrTipoItemSessaoBloco=InfraArray::converterArrInfraDTO($arrObjSessaoBlocoDTO,'StaTipoItem','IdSessaoBloco');
      //
      foreach ($arrObjItemSessaoJulgamentoDTO as $key=>$objItemSessaoJulgamentoDTO) {
        if($arrTipoItemSessaoBloco[$objItemSessaoJulgamentoDTO->getNumIdSessaoBloco()]!=TipoSessaoBlocoRN::$STA_PAUTA){
          unset($arrObjItemSessaoJulgamentoDTO[$key]);
        }
      }
      $objSessaoJulgamentoDTO->setArrObjItemSessaoJulgamentoDTO(array_values($arrObjItemSessaoJulgamentoDTO));
    }

    $strItens=$objSessaoJulgamentoDTO->getDthSessao();
    $i=0;
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      $strItens .= $i ++;
      $strItens .= $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
      $strItens .= $objItemSessaoJulgamentoDTO->getStrStaSituacao();
      $strItens .= $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
    }

    $strHash=sha1($strItens);
    $parObjSessaoJulgamentoDTO->setStrHashPauta($strHash);

    $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
    $objAtributoAndamentoSessaoRN=new AtributoAndamentoSessaoRN();
    $objAtributoAndamentoSessaoDTO->setNumIdSessaoJulgamentoAndamentoSessao($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objAtributoAndamentoSessaoDTO->setNumIdTarefaSessaoAndamentoSessao(TarefaSessaoRN::$TS_SITUACAO_SESSAO);
    $objAtributoAndamentoSessaoDTO->setStrChave('HASHPAUTA');
    $objAtributoAndamentoSessaoDTO->retStrValor();
    $objAtributoAndamentoSessaoDTO->setOrdDthExecucaoAndamentoSessao(InfraDTO::$TIPO_ORDENACAO_DESC);
    $objAtributoAndamentoSessaoDTO->setNumMaxRegistrosRetorno(1);
    $objAtributoAndamentoSessaoDTO=$objAtributoAndamentoSessaoRN->consultar($objAtributoAndamentoSessaoDTO);
    if($objAtributoAndamentoSessaoDTO!==null && $objAtributoAndamentoSessaoDTO->getStrValor()===$strHash){
      return;
    }

    $strConteudo = $this->gerarConteudoPauta($objSessaoJulgamentoDTO);
    if ($objSessaoJulgamentoDTO->getDblIdProcedimento()==null){
      //gerar processo do tipo ligado ao colegiado (descri��o=Colegiado - ANO)
      $objProtocoloDTO=$this->gerarProcedimento($objSessaoJulgamentoDTO);
      $objSessaoJulgamentoDTO->setDblIdProcedimento($objProtocoloDTO->getDblIdProtocolo());
    }

    $objInfraParametro=new InfraParametro($this->inicializarObjInfraIBanco());
    $objSeiRN=new SeiRN();
    $objDocumentoAPI=new DocumentoAPI();
    $objDocumentoAPI->setTipo(ProtocoloRN::$TP_DOCUMENTO_GERADO);
    $objDocumentoAPI->setIdProcedimento($objSessaoJulgamentoDTO->getDblIdProcedimento());
    $objDocumentoAPI->setIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_PAUTA_SESSAO));
    $objDocumentoAPI->setNumero(null);
    $objDocumentoAPI->setData(null);
    $objDocumentoAPI->setDescricao(null);
    $objDocumentoAPI->setNomeArquivo(null);
    $objDocumentoAPI->setRemetente(null);
    $objDocumentoAPI->setDestinatarios(array());
    $objDocumentoAPI->setObservacao(null);
    $objDocumentoAPI->setConteudo(base64_encode(utf8_encode(InfraUtil::filtrarISO88591($strConteudo))));
    $objDocumentoAPI->setNivelAcesso(ProtocoloRN::$NA_PUBLICO);
    $objDocumentoAPI->setSinBloqueado('N');

    $objDocumentoAPI=$objSeiRN->incluirDocumento($objDocumentoAPI);
    $parObjSessaoJulgamentoDTO->setDblIdDocumentoPauta($objDocumentoAPI->getIdDocumento());

  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   */
  private function gerarDocumentoCancelamento(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {

    $objSessaoJulgamentoDTOBanco=new SessaoJulgamentoDTO();
    $objSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
    $objSessaoJulgamentoDTOBanco->retStrStaSituacao();
    $objSessaoJulgamentoDTOBanco->retStrDescricaoTipoSessao();
    $objSessaoJulgamentoDTOBanco->retDthSessao();
    $objSessaoJulgamentoDTOBanco->retStrNomeColegiado();
    $objSessaoJulgamentoDTOBanco->retDblIdProcedimento();

    $objSessaoJulgamentoDTOBanco=$this->consultar($objSessaoJulgamentoDTOBanco);


    $strConteudo='<p class="Texto_Centralizado_Maiusculas">'.$objSessaoJulgamentoDTOBanco->getStrNomeColegiado().'</p>'."\n";
    $strConteudo.='<p class="Texto_Justificado_Recuo_Primeira_Linha">O Presidente do colegiado '.$objSessaoJulgamentoDTOBanco->getStrNomeColegiado().', TORNA P�BLICO '
        .'aos Senhores Advogados e demais partes interessadas que a sess�o ';

    //busca nome do tipo de sess�o
    $strConteudo .= InfraString::transformarCaixaBaixa($objSessaoJulgamentoDTOBanco->getStrDescricaoTipoSessao());

    $strConteudo.=' <strong>agendada para '.substr($objSessaoJulgamentoDTOBanco->getDthSessao(),0,-3)
        .', n�o ser� realizada.</strong></p>'."\n";

    $objInfraParametro=new InfraParametro($this->inicializarObjInfraIBanco());

    $objSeiRN=new SeiRN();
    $objDocumentoAPI=new DocumentoAPI();
    $objDocumentoAPI->setTipo(ProtocoloRN::$TP_DOCUMENTO_GERADO);
    $objDocumentoAPI->setIdProcedimento($objSessaoJulgamentoDTOBanco->getDblIdProcedimento());
    $objDocumentoAPI->setIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_EDITAL_CANCELAMENTO_SESSAO));
    $objDocumentoAPI->setNumero(null);
    $objDocumentoAPI->setData(null);
    $objDocumentoAPI->setDescricao(null);
    $objDocumentoAPI->setNomeArquivo(null);
    $objDocumentoAPI->setRemetente(null);
    $objDocumentoAPI->setDestinatarios(array());
    $objDocumentoAPI->setObservacao(null);
    $objDocumentoAPI->setConteudo(base64_encode(utf8_encode(InfraUtil::filtrarISO88591($strConteudo))));
    $objDocumentoAPI->setNivelAcesso(ProtocoloRN::$NA_PUBLICO);
    $objDocumentoAPI->setSinBloqueado('N');

    $objSeiRN->incluirDocumento($objDocumentoAPI);

  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTOBanco
   * @param PresencaSessaoDTO[] $arrObjPresencaSessaoDTO
   * @param ColegiadoComposicaoDTO[] $arrObjColegiadoComposicaoDTO
   * @param AndamentoSessaoDTO[] $arrObjAusenciaSessaoDTO
   * @param ItemSessaoJulgamentoDTO[] $arrObjItemSessaoJulgamentoDTO
   * @return DocumentoDTO
   * @throws InfraException
   */
  private function gerarAtaSessao(SessaoJulgamentoDTO $objSessaoJulgamentoDTOBanco, $arrObjPresencaSessaoDTO, $arrObjColegiadoComposicaoDTO, $arrObjAusenciaSessaoDTO, $arrObjItemSessaoJulgamentoDTO)
  {
    $objInfraParametro = new InfraParametro($this->inicializarObjInfraIBanco());
    $objDocumentoRN = new DocumentoRN();

    $objProtocoloDTO = new ProtocoloDTO();
    $objProtocoloDTO->setDblIdProtocolo(null);
    $objProtocoloDTO->setStrStaNivelAcessoLocal(ProtocoloRN::$NA_PUBLICO);
    $objProtocoloDTO->setStrDescricao(null);
    $objProtocoloDTO->setDtaGeracao(InfraData::getStrDataAtual());
    $objProtocoloDTO->setArrObjRelProtocoloAssuntoDTO(array());
    $objProtocoloDTO->setArrObjParticipanteDTO(array());
    $objProtocoloDTO->setArrObjObservacaoDTO(array());

    $objDocumentoDTO = new DocumentoDTO();
    $objDocumentoDTO->setDblIdProcedimento($objSessaoJulgamentoDTOBanco->getDblIdProcedimento());
    $objDocumentoDTO->setDblIdDocumento(null);
    $objDocumentoDTO->setNumIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_ATA_SESSAO));
    $objDocumentoDTO->setDblIdDocumentoEdoc(null);
    $objDocumentoDTO->setDblIdDocumentoEdocBase(null);
    $objDocumentoDTO->setNumIdUnidadeResponsavel(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objDocumentoDTO->setStrNumero(null);
    $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_EDITOR_INTERNO);

    $objColegiadoDTO = new ColegiadoDTO();
    $objColegiadoRN = new ColegiadoRN();
    $objColegiadoDTO->setNumIdColegiado($objSessaoJulgamentoDTOBanco->getNumIdColegiado());
    $objColegiadoDTO->retStrNome();
    $objColegiadoDTO->retNumIdUsuarioPresidente();
    $objColegiadoDTO->retStrNomePresidente();
    $objColegiadoDTO->retNumIdContatoSecretario();
    $objColegiadoDTO = $objColegiadoRN->consultar($objColegiadoDTO);

    $objContatoDTO = new ContatoDTO();
    $objContatoRN = new ContatoRN();
    $objContatoDTO->setNumIdContato($objColegiadoDTO->getNumIdContatoSecretario());
    $objContatoDTO->retStrNome();
    $objContatoDTO->retStrStaGenero();
    $objContatoDTO = $objContatoRN->consultarRN0324($objContatoDTO);

    $objSustentacatoOralDTO = new SustentacaoOralDTO();
    $objSustentacatoOralRN = new SustentacaoOralRN();
    $objSustentacatoOralDTO->setNumIdSessaoJulgamentoItem($objSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
    $objSustentacatoOralDTO->retNumIdSustentacaoOral();
    $objSustentacatoOralDTO->retNumIdItemSessaoJulgamento();
    $objSustentacatoOralDTO->retStrDescricaoQualificacaoParte();
    $objSustentacatoOralDTO->retStrNomeContato();
    $arrObjSustentacaoOralDTO = $objSustentacatoOralRN->listar($objSustentacatoOralDTO);
    $arrObjSustentacaoOralDTO = InfraArray::indexarArrInfraDTO($arrObjSustentacaoOralDTO, 'IdItemSessaoJulgamento', true);

    $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
    $objAtributoAndamentoSessaoRN = new AtributoAndamentoSessaoRN();
    $objAtributoAndamentoSessaoDTO->setNumIdTarefaSessaoAndamentoSessao(TarefaSessaoRN::$TS_PRESIDENTE_ALTERADO);
    $objAtributoAndamentoSessaoDTO->setNumIdSessaoJulgamentoAndamentoSessao($objSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
    $objAtributoAndamentoSessaoDTO->setStrChave('PRESIDENTE');
    $objAtributoAndamentoSessaoDTO->retStrIdOrigem();
    $objAtributoAndamentoSessaoDTO->retDthExecucaoAndamentoSessao();
    $objAtributoAndamentoSessaoDTO->retStrValor();
    $objAtributoAndamentoSessaoDTO->setOrdDthExecucaoAndamentoSessao(InfraDTO::$TIPO_ORDENACAO_ASC);
    $arrObjAtributoAndamentoSessaoDTO = $objAtributoAndamentoSessaoRN->listar($objAtributoAndamentoSessaoDTO);

    $arrIdProcedimentos = InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdProcedimentoDistribuicao');

    if (count($arrIdProcedimentos)) {
      $objParticipanteDTO = new ParticipanteDTO();
      $objParticipanteDTO->retDblIdProtocolo();
      $objParticipanteDTO->retStrNomeContato();
      $objParticipanteDTO->setDblIdProtocolo($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $objParticipanteDTO->setStrStaParticipacao(ParticipanteRN::$TP_INTERESSADO);
      $objParticipanteDTO->setOrdNumSequencia(InfraDTO::$TIPO_ORDENACAO_ASC);

      $objParticipanteRN = new ParticipanteRN();
      $arrObjParticipanteDTO = $objParticipanteRN->listarRN0189($objParticipanteDTO);
      $arrObjParticipanteDTO = InfraArray::indexarArrInfraDTO($arrObjParticipanteDTO, 'IdProtocolo', true);

      $objParteProcedimentoDTO = new ParteProcedimentoDTO();
      $objParteProcedimentoRN = new ParteProcedimentoRN();
      $objParteProcedimentoDTO->retTodos();
      $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
      $objParteProcedimentoDTO->retStrNomeContato();
      $objParteProcedimentoDTO->setDblIdProcedimento($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $arrObjParteProcedimentoDTO = $objParteProcedimentoRN->listar($objParteProcedimentoDTO);
      $arrObjParteProcedimentoDTO = InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO, 'IdProcedimento', true);

      $objAutuacaoDTO = new AutuacaoDTO();
      $objAutuacaoRN = new AutuacaoRN();
      $objAutuacaoDTO->retTodos();
      $objAutuacaoDTO->retStrDescricaoTipoMateria();
      $objAutuacaoDTO->setDblIdProcedimento($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $arrObjAutuacaoDTO = $objAutuacaoRN->listar($objAutuacaoDTO);
      $arrObjAutuacaoDTO = InfraArray::indexarArrInfraDTO($arrObjAutuacaoDTO, 'IdProcedimento');
    }
    $strParagrafoRecuo = '<p class="Texto_Justificado_Recuo_Primeira_Linha">';
    $strParagrafoProcesso = '<p class="Texto_Justificado">';
    $strConteudo = '';

    $strConteudo .= '<p class="Texto_Centralizado_Maiusculas">';
    $strConteudo .= $objColegiadoDTO->getStrNome();
    $strConteudo .= '</p>';

    $strConteudo .= $strParagrafoRecuo . 'Ata da Sess�o ';

    //busca nome do tipo de sess�o
    $strConteudo .= $objSessaoJulgamentoDTOBanco->getStrDescricaoTipoSessao();

    $strConteudo .= ', em ' . $this->formatarDataHoraSessao($objSessaoJulgamentoDTOBanco->getDthSessao()) . '.</p>' . "\n";
    $dthInicio=$objSessaoJulgamentoDTOBanco->getDthInicio();
    $dthFim=$objSessaoJulgamentoDTOBanco->getDthFim();
    if(substr($dthInicio,0,10)===substr($dthFim,0,10)){
      $bolExibirData=false;
    } else {
      $bolExibirData=true;
    }
    if ($arrObjAtributoAndamentoSessaoDTO == null) {
      $strConteudo .= $strParagrafoRecuo . 'Presidente: ';
      $numIdUsuarioPresidente=$objColegiadoDTO->getNumIdUsuarioPresidente();
      $strCargo = $arrObjColegiadoComposicaoDTO[$numIdUsuarioPresidente]->getStrExpressaoCargo();
      if ($strCargo != null) {
        $strConteudo .= $strCargo . ' ';
      }
      $strConteudo .= $objColegiadoDTO->getStrNomePresidente() . '</p>' . "\n";
    } elseif (InfraArray::contar($arrObjAtributoAndamentoSessaoDTO)==1 &&
        InfraData::compararDataHora($arrObjAtributoAndamentoSessaoDTO[0]->getDthExecucaoAndamentoSessao(), $dthInicio) > 0 ) {
      $strConteudo .= $strParagrafoRecuo . 'Presidente: ';
      $numIdUsuarioPresidente=$arrObjAtributoAndamentoSessaoDTO[0]->getStrIdOrigem();
      $strCargo = $arrObjColegiadoComposicaoDTO[$numIdUsuarioPresidente]->getStrExpressaoCargo();
      if ($strCargo != null) {
        $strConteudo .= $strCargo . ' ';
      }
      $strConteudo .= $arrObjAtributoAndamentoSessaoDTO[0]->getStrValor() . '</p>' . "\n";
    } else {
      // mais de um presidente
      $arrPresidentes=array();
      foreach ($arrObjAtributoAndamentoSessaoDTO as $objAtributoAndamentoSessaoDTO) {
        if(InfraData::compararDataHora($objAtributoAndamentoSessaoDTO->getDthExecucaoAndamentoSessao(), $dthInicio) > 0 ){
          $arrPresidentes[0]=array($objAtributoAndamentoSessaoDTO->getStrIdOrigem(),$dthInicio);
        } else{
          if (!isset($arrPresidentes[0])){
            $arrPresidentes[0]=array($objColegiadoDTO->getNumIdUsuarioPresidente(),$dthInicio);
          } else {
            $arrPresidentes[] = array($objAtributoAndamentoSessaoDTO->getStrIdOrigem(), $objAtributoAndamentoSessaoDTO->getDthExecucaoAndamentoSessao());
          }
        }
      }
      $qtd=InfraArray::contar($arrPresidentes);
      $strConteudo .= '<table style="border:0;margin-left:1.18in;margin-right:auto;"><tbody><tr><td>&nbsp;Presidentes:</td>';
      for($i=0;$i<$qtd;$i++){
        if($i!=0){
          $strConteudo.='<tr><td>&nbsp;</td>';
        }
        $strConteudo.='<td>';
        if(($i+1)==$qtd){
          $final=$dthFim;
        } else {
          $final=$arrPresidentes[$i+1][1];
        }
        $strCargo = $arrObjColegiadoComposicaoDTO[$arrPresidentes[$i][0]]->getStrExpressaoCargo();
        if ($strCargo != null) {
          $strConteudo .= $strCargo . ' ';
        }
        $strConteudo.=$arrObjColegiadoComposicaoDTO[$arrPresidentes[$i][0]]->getStrNomeUsuario().' (';
        if($bolExibirData){
          $strConteudo.=$arrPresidentes[$i][1].' a '.$final.')';
        } else {
          $strConteudo.=substr($arrPresidentes[$i][1],11).' a '.substr($final,11).')';
        }
        $strConteudo.='</td></tr>';
      }
      $strConteudo.='</tbody></table>';
    }

    if($objContatoDTO->getStrStaGenero()==ContatoRN::$TG_FEMININO){
      $strConteudo .= $strParagrafoRecuo . 'Secret�ria: ' . $objContatoDTO->getStrNome() . '</p>' . "\n";
    }else {
      $strConteudo .= $strParagrafoRecuo . 'Secret�rio: ' . $objContatoDTO->getStrNome() . '</p>' . "\n";
    }

    $strConteudo .= $strParagrafoRecuo . '�s ' . $this->formatarHoraSessao($objSessaoJulgamentoDTOBanco->getDthInicio()) . ', foi aberta a sess�o.</p>';
    $strConteudo .= $strParagrafoRecuo . 'Presentes os Excelent�ssimos ';
    $arrIdUsuariosPresentes = InfraArray::converterArrInfraDTO($arrObjPresencaSessaoDTO, 'IdUsuario');
    $strSeparador = '';
    foreach ($arrObjColegiadoComposicaoDTO as $key => $objColegiadoComposicaoDTO) {
      if (!array_key_exists($key, $arrObjAusenciaSessaoDTO) && in_array($key, $arrIdUsuariosPresentes)) {
        $strConteudo .= $strSeparador;
        $strConteudo .= $objColegiadoComposicaoDTO->getStrExpressaoCargo() . ' ';
        $strConteudo .= $objColegiadoComposicaoDTO->getStrNomeUsuario();
//        if ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado() == TipoMembroColegiadoRN::$TMC_SUPLENTE) {
//          $strConteudo .= ' <b>(Suplente)</b>';
//        } elseif ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado() == TipoMembroColegiadoRN::$TMC_EVENTUAL){
//          $strConteudo .= ' <b>(Eventual)</b>';
//        }
        $strSeparador = ', ';
      }
    }
    $strConteudo .= '</p>' . "\n";

    if (InfraArray::contar($arrObjAusenciaSessaoDTO)) {
      $strConteudo .= $strParagrafoRecuo . 'Ausentes os Excelent�ssimos ';
      $strSeparador = '';
      foreach ($arrObjColegiadoComposicaoDTO as $key => $objColegiadoComposicaoDTO) {
        if (array_key_exists($key, $arrObjAusenciaSessaoDTO)) {
          $objAusenciaSessaoDTO=$arrObjAusenciaSessaoDTO[$key];
          $strConteudo .= $strSeparador;
          $objColegiadoComposicaoDTO = $arrObjColegiadoComposicaoDTO[$objAusenciaSessaoDTO->getNumIdUsuario()];
          $strCargo = $objColegiadoComposicaoDTO->getStrExpressaoCargo();
          if ($strCargo != null) {
            $strConteudo .= $strCargo . ' ';
          }
          $strConteudo .= $objColegiadoComposicaoDTO->getStrNomeUsuario();
          $strConteudo .= ' - Motivo: ' . $objAusenciaSessaoDTO->getStrDescricaoMotivoAusencia();
          $strSeparador = ', ';
        }
      }
      $strConteudo .= '</p>' . "\n";
    }
    $strConteudo .= '<p></p>' . "\n";
    $strConteudo .= $strParagrafoRecuo . 'N�o havendo impugna��o, foi aprovada a Ata da sess�o anterior.</p>' . "\n";
    $seq = 1;
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      $strConteudo .= $strParagrafoProcesso . '<b>' . str_pad($seq++, 5, '0', STR_PAD_LEFT) . ' - Processo: ';
      $strConteudo .= '<span contenteditable="false" style="text-indent:0;"><a class="ancora_sei" id="lnkSei' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao() . '" style="text-indent:0;">' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a></span>';
      $strConteudo .= ' - ' . $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento() . '</b></p>' . "\n";
      $dblIdProcedimento=$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao();
      //inclui sustenta��o oral
      if(isset($arrObjSustentacaoOralDTO[$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()])){
        $strConteudo .= $strParagrafoRecuo . 'Sustenta��o Oral: ';
        $strSeparador = '';

        /** @var SustentacaoOralDTO $objSustentacaoOralDTO */
        foreach ($arrObjSustentacaoOralDTO[$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento()] as $objSustentacaoOralDTO) {
          $strConteudo .= $strSeparador;
          $strQualificacao = $objSustentacaoOralDTO->getStrDescricaoQualificacaoParte();
          if ($strQualificacao != null) {
            $strConteudo .= $strQualificacao . ' ';
          }
          $strConteudo .= $objSustentacaoOralDTO->getStrNomeContato();
          $strSeparador = ', ';
        }
        $strConteudo .= '</p>' . "\n";
      }
      if(isset($arrObjAutuacaoDTO[$dblIdProcedimento]) && $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricaoTipoMateria()!=null){
        $strConteudo.='</p>'."\n".'<p class="Texto_Justificado_Recuo_Primeira_Linha">Tipo da Mat�ria: ';
        $strConteudo .= $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricaoTipoMateria();
      }
      if(isset($arrObjParteProcedimentoDTO[$dblIdProcedimento])){
        $strConteudo.='</p>'."\n".'<p class="Texto_Justificado_Recuo_Primeira_Linha">Partes: ';
        $j=InfraArray::contar($arrObjParteProcedimentoDTO[$dblIdProcedimento]);
        foreach ($arrObjParteProcedimentoDTO[$dblIdProcedimento] as $objParteProcedimentoDTO) {
          $strConteudo.=$objParteProcedimentoDTO->getStrNomeContato().' ('.$objParteProcedimentoDTO->getStrDescricaoQualificacaoParte().')';
          if(--$j>0){
            $strConteudo .= $j>1 ? ', ' : ' e ';
          }
        }
      } else if(isset($arrObjParticipanteDTO[$dblIdProcedimento])){
        $strConteudo.='</p>'."\n".'<p class="Texto_Justificado_Recuo_Primeira_Linha">Interessados: ';
        $j=InfraArray::contar($arrObjParticipanteDTO[$dblIdProcedimento]);
        foreach ($arrObjParticipanteDTO[$dblIdProcedimento] as $objParticipanteDTO) {
          $strConteudo.=$objParticipanteDTO->getStrNomeContato();
          if(--$j>0){
            $strConteudo .= $j>1 ? ', ' : ' e ';
          }
        }
      }
      if(isset($arrObjAutuacaoDTO[$dblIdProcedimento]) && $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricao()!=null){
        $strConteudo.='</p>'."\n".'<p class="Texto_Justificado_Recuo_Primeira_Linha">Descri��o: ';
        $strConteudo .= $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricao();
      }
      $arrTextoDispositivo = explode("\n", $objItemSessaoJulgamentoDTO->getStrDispositivo());
      foreach ($arrTextoDispositivo as $strLinhaDispositivo) {
        $strConteudo .= $strParagrafoRecuo . $strLinhaDispositivo . '</p>';
      }
      $strConteudo .= '<p>&nbsp;</p>';
    }

    $strConteudo .= $strParagrafoRecuo . 'Encerrou-se a sess�o �s ' . $this->formatarHoraSessao($objSessaoJulgamentoDTOBanco->getDthFim()) . '.</p>' . "\n";
    //$strConteudo.=$strParagrafoRecuo.'Porto Alegre, '.InfraData::formatarExtenso(InfraData::getStrDataHoraAtual()).'.</p>';


    $objDocumentoDTO->setStrConteudo($strConteudo);

    $objDocumentoDTO->setObjProtocoloDTO($objProtocoloDTO);

    $objAtividadeDTO = new AtividadeDTO();
    $objAtividadeRN = new AtividadeRN();
    $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objAtividadeDTO->setDblIdProtocolo($objSessaoJulgamentoDTOBanco->getDblIdProcedimento());
    $objAtividadeDTO->setDthConclusao(null);

    $objSeiRN = new SeiRN();

    if ($objAtividadeRN->contarRN0035($objAtividadeDTO) == 0) {
      //reabertura autom�tica
      $objEntradaReabrirProcessoAPI = new EntradaReabrirProcessoAPI();
      $objEntradaReabrirProcessoAPI->setIdProcedimento($objSessaoJulgamentoDTOBanco->getDblIdProcedimento());
      $objSeiRN->reabrirProcesso($objEntradaReabrirProcessoAPI);
    }

    $objDocumentoDTO = $objDocumentoRN->cadastrarRN0003($objDocumentoDTO);
    return $objDocumentoDTO;
  }

  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return string
   * @throws InfraException
   */
  protected function gerarConteudoPautaConectado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {
    $bolSessaoVirtual=$objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S';
    $arrObjColegiadoComposicaoDTO=$objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO();
    $arrObjItemSessaoJulgamentoDTO=$objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
    $arrIdProcedimentos = InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdProcedimentoDistribuicao');
    $arrObjParticipanteDTO = array();
    $arrObjParteProcedimentoDTO = array();
    $arrObjAutuacaoDTO = array();
    if (InfraArray::contar($arrIdProcedimentos)>0) {
      $objParticipanteDTO = new ParticipanteDTO();
      $objParticipanteDTO->retDblIdProtocolo();
      $objParticipanteDTO->retStrNomeContato();
      $objParticipanteDTO->setDblIdProtocolo($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $objParticipanteDTO->setStrStaParticipacao(ParticipanteRN::$TP_INTERESSADO);
      $objParticipanteDTO->setOrdNumSequencia(InfraDTO::$TIPO_ORDENACAO_ASC);

      $objParticipanteRN = new ParticipanteRN();
      $arrObjParticipanteDTO = $objParticipanteRN->listarRN0189($objParticipanteDTO);
      $arrObjParticipanteDTO = InfraArray::indexarArrInfraDTO($arrObjParticipanteDTO, 'IdProtocolo', true);

      $objParteProcedimentoDTO = new ParteProcedimentoDTO();
      $objParteProcedimentoRN = new ParteProcedimentoRN();
      $objParteProcedimentoDTO->retTodos();
      $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
      $objParteProcedimentoDTO->retStrNomeContato();
      $objParteProcedimentoDTO->setDblIdProcedimento($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $arrObjParteProcedimentoDTO = $objParteProcedimentoRN->listar($objParteProcedimentoDTO);
      $arrObjParteProcedimentoDTO = InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO, 'IdProcedimento', true);

      $objAutuacaoDTO = new AutuacaoDTO();
      $objAutuacaoRN = new AutuacaoRN();
      $objAutuacaoDTO->retTodos();
      $objAutuacaoDTO->retStrDescricaoTipoMateria();
      $objAutuacaoDTO->setDblIdProcedimento($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $arrObjAutuacaoDTO = $objAutuacaoRN->listar($objAutuacaoDTO);
      $arrObjAutuacaoDTO = InfraArray::indexarArrInfraDTO($arrObjAutuacaoDTO, 'IdProcedimento');

    }
    $strVirtual = $bolSessaoVirtual ? 'VIRTUAL ' : '';
    $strConteudo = '<p class="Texto_Centralizado_Maiusculas">' . $objSessaoJulgamentoDTO->getStrNomeColegiado() . '</p>' . "\n";
    $strConteudo .= '<p class="Texto_Centralizado_Maiusculas">Sess�o de Julgamento ' . $strVirtual . 'de ' . substr($objSessaoJulgamentoDTO->getDthSessao(), 0, - 3) . '</p>' . "\n";
    $strConteudo .= '<p class="Texto_Centralizado_Maiusculas">&nbsp;</p>' . "\n";


    $arrObjSessaoBlocoDTO=CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $arrObjSessaoBlocoDTO=array_values($arrObjSessaoBlocoDTO);
    InfraArray::ordenarArrInfraDTO($arrObjSessaoBlocoDTO,'Ordem',InfraArray::$TIPO_ORDENACAO_ASC);

    foreach ($arrObjSessaoBlocoDTO as $objSessaoBlocoDTO) {
      $arrPauta = InfraArray::filtrarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdSessaoBloco', $objSessaoBlocoDTO->getNumIdSessaoBloco());
      if(count($arrPauta)==0){
        continue;
      }
      $strBloco=$objSessaoBlocoDTO->getStrDescricao();

      $strConteudo .= '<p style="background-color: lightgray;text-align: center;font-weight: bold;font-size: 12pt; margin: 0;">' . $strBloco . '</p>' . "\n";
      $strConteudo .= '<p style="margin:0">&nbsp;</p>' . "\n";

      $arrPauta = InfraArray::indexarArrInfraDTO($arrPauta, 'IdUsuarioSessao', true);
      $i = 1;
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        if (isset($arrPauta[$objColegiadoComposicaoDTO->getNumIdUsuario()])) {
          if ($objSessaoBlocoDTO->getStrStaTipoItem()!==TipoSessaoBlocoRN::$STA_REFERENDO) {
            $strConteudo .= '<p class="Texto_Justificado_Recuo_Primeira_Linha"><b>' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . "</b></p>\n";
          }
          $arrPautaMembro = $arrPauta[$objColegiadoComposicaoDTO->getNumIdUsuario()];
          foreach ($arrPautaMembro as $objItemSessaoJulgamentoDTO) {
            $strConteudo .= '<p class="Texto_Justificado_Recuo_Primeira_Linha">' . str_pad($i ++, 3, '0', STR_PAD_LEFT) . ') ';
            if ($objItemSessaoJulgamentoDTO->getStrStaSituacao()===ItemSessaoJulgamentoRN::$STA_RETIRADO) {
              $strConteudo .= 'Retirado';
            } else if($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO){
              $strConteudo .= $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo().' - Sigiloso';
            } else {
              $dblIdProcedimento = $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao();
              $strConteudo .= '<span contenteditable="false" style="text-indent:0;"><a class="ancora_sei" id="lnkSei' . $dblIdProcedimento . '" style="text-indent:0;">' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a></span>';
              $strConteudo .= ' - ' . $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento();
              if (isset($arrObjAutuacaoDTO[$dblIdProcedimento]) && $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricaoTipoMateria()!=null) {
                $strConteudo .= '</p>' . "\n" . '<p class="Texto_Justificado_Recuo_Primeira_Linha">Tipo da Mat�ria: ';
                $strConteudo .= $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricaoTipoMateria();
              }
              if (isset($arrObjParteProcedimentoDTO[$dblIdProcedimento])) {
                $strConteudo .= '</p>' . "\n" . '<p class="Texto_Justificado_Recuo_Primeira_Linha">Partes: ';
                $j = InfraArray::contar($arrObjParteProcedimentoDTO[$dblIdProcedimento]);
                foreach ($arrObjParteProcedimentoDTO[$dblIdProcedimento] as $objParteProcedimentoDTO) {
                  $strConteudo .= $objParteProcedimentoDTO->getStrNomeContato() . ' (' . $objParteProcedimentoDTO->getStrDescricaoQualificacaoParte() . ')';
                  if (-- $j>0) {
                    $strConteudo .= $j>1 ? ', ' : ' e ';
                  }
                }
              } else if (isset($arrObjParticipanteDTO[$dblIdProcedimento])) {
                $strConteudo .= '</p>' . "\n" . '<p class="Texto_Justificado_Recuo_Primeira_Linha">Interessados: ';
                $j = InfraArray::contar($arrObjParticipanteDTO[$dblIdProcedimento]);
                foreach ($arrObjParticipanteDTO[$dblIdProcedimento] as $objParticipanteDTO) {
                  $strConteudo .= $objParticipanteDTO->getStrNomeContato();
                  if (-- $j>0) {
                    $strConteudo .= $j>1 ? ', ' : ' e ';
                  }
                }
              }
              if (isset($arrObjAutuacaoDTO[$dblIdProcedimento]) && $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricao()!=null) {
                $strConteudo .= '</p>' . "\n" . '<p class="Texto_Justificado_Recuo_Primeira_Linha">Descri��o: ';
                $strConteudo .= $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricao();
              }
            }
            $strConteudo .= ".</p>\n";
          }
          $strConteudo .= "<p>&nbsp;</p>\n";
        }
      }
    }
    return $strConteudo;
  }
  /**
   * @param SessaoJulgamentoDTO $objSessaoJulgamentoDTO
   * @return string
   * @throws InfraException
   */
  protected function gerarTabelaPautaConectado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO)
  {
    $numIdSessaoJulgamento=$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento();

    $bolSessaoEncerrada=in_array($objSessaoJulgamentoDTO->getStrStaSituacao(),[SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_FINALIZADA,SessaoJulgamentoRN::$ES_CANCELADA]);
    $bolSessaoVirtual=$objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S';
    $arrObjColegiadoComposicaoDTO=CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($numIdSessaoJulgamento);
    $arrObjItemSessaoJulgamentoDTO=CacheSessaoJulgamentoRN::getArrObjItemSessaoJulgamentoDTO($numIdSessaoJulgamento, true);
    $arrIdProcedimentos = InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdProcedimentoDistribuicao');
    $arrObjParticipanteDTO = array();
    $arrObjParteProcedimentoDTO = array();
    $arrObjAutuacaoDTO = array();
    if (InfraArray::contar($arrIdProcedimentos)>0) {
      $objParticipanteDTO = new ParticipanteDTO();
      $objParticipanteDTO->retDblIdProtocolo();
      $objParticipanteDTO->retStrNomeContato();
      $objParticipanteDTO->setDblIdProtocolo($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $objParticipanteDTO->setStrStaParticipacao(ParticipanteRN::$TP_INTERESSADO);
      $objParticipanteDTO->setOrdNumSequencia(InfraDTO::$TIPO_ORDENACAO_ASC);

      $objParticipanteRN = new ParticipanteRN();
      $arrObjParticipanteDTO = $objParticipanteRN->listarRN0189($objParticipanteDTO);
      $arrObjParticipanteDTO = InfraArray::indexarArrInfraDTO($arrObjParticipanteDTO, 'IdProtocolo', true);

      $objParteProcedimentoDTO = new ParteProcedimentoDTO();
      $objParteProcedimentoRN = new ParteProcedimentoRN();
      $objParteProcedimentoDTO->retTodos();
      $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
      $objParteProcedimentoDTO->retStrNomeContato();
      $objParteProcedimentoDTO->setDblIdProcedimento($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $arrObjParteProcedimentoDTO = $objParteProcedimentoRN->listar($objParteProcedimentoDTO);
      $arrObjParteProcedimentoDTO = InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO, 'IdProcedimento', true);

      $objAutuacaoDTO = new AutuacaoDTO();
      $objAutuacaoRN = new AutuacaoRN();
      $objAutuacaoDTO->retTodos();
      $objAutuacaoDTO->retStrDescricaoTipoMateria();
      $objAutuacaoDTO->setDblIdProcedimento($arrIdProcedimentos, InfraDTO::$OPER_IN);
      $arrObjAutuacaoDTO = $objAutuacaoRN->listar($objAutuacaoDTO);
      $arrObjAutuacaoDTO = InfraArray::indexarArrInfraDTO($arrObjAutuacaoDTO, 'IdProcedimento');

    }
    $strVirtual = $bolSessaoVirtual ? 'VIRTUAL ' : '';
    $strComplemento='';
    if($objSessaoJulgamentoDTO->getStrStaSituacao()==SessaoJulgamentoRN::$ES_CANCELADA){
      $strComplemento='<br>(CANCELADA)';
    }
    $strConteudo='<div class="infraAreaDadosDinamica">';
    $strConteudo .= '<p style="text-transform: uppercase;font-size: 2em;text-align: center;font-weight: bold;">' . $objSessaoJulgamentoDTO->getStrNomeColegiado() . '</p>' . "\n";
    $strConteudo .= '<p style="text-transform: uppercase;font-size: 1.6em;text-align: center;">Sess�o de Julgamento ' . $strVirtual . 'de ' . substr($objSessaoJulgamentoDTO->getDthSessao(), 0, - 3) .$strComplemento. '</p>' . "\n";
    $strConteudo.='</div>';

    $arrObjSessaoBlocoDTO=CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($numIdSessaoJulgamento);
    $arrObjItemSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdSessaoBloco',true);

    foreach ($arrObjSessaoBlocoDTO as $objSessaoBlocoDTO) {
      if(!isset($arrObjItemSessaoJulgamentoDTO[$objSessaoBlocoDTO->getNumIdSessaoBloco()])){
        continue;
      }
      $arrPauta=$arrObjItemSessaoJulgamentoDTO[$objSessaoBlocoDTO->getNumIdSessaoBloco()];
      $bolMesaAposInicio=false;
      $strBloco = $objSessaoBlocoDTO->getStrDescricao();
      $strCaption = 'Tabela de '.$strBloco;
      $strMensagem='';
      if($objSessaoBlocoDTO->getStrStaTipoItem()==TipoSessaoBlocoRN::$STA_MESA){
        $strMensagem="A rela��o dos processos apresentados em mesa est� sujeita � altera��o";

        if(!$bolSessaoVirtual && $objSessaoJulgamentoDTO->getDthInicio()!=null) {
          $dthInicioSessao = $objSessaoJulgamentoDTO->getDthInicio();
          /**
           * @var ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
           */
          foreach ($arrPauta as $objItemSessaoJulgamentoDTO) {
            if (InfraData::compararDataHorasSimples($dthInicioSessao, $objItemSessaoJulgamentoDTO->getDthInclusao())>=1) {
              $objItemSessaoJulgamentoDTO->setStrNomeUsuarioRelator($arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()]->getStrNomeUsuario());
              $objItemSessaoJulgamentoDTO->setNumIdUsuarioSessao(- 1);
              $bolMesaAposInicio = true;
            }
          }
        }
      } elseif ($objSessaoBlocoDTO->getStrStaTipoItem()==TipoSessaoBlocoRN::$STA_REFERENDO){
        $strMensagem='A rela��o dos processos apresentados para referendo est� sujeita � altera��o';
      }


      //barra
      $strConteudo.='<div class="infraAreaDados" style="height: 3.5em">';
      $strConteudo .= '<label class="infraLabelTitulo">' . $strBloco;
      $strConteudo.='<div class="resumo">';
      $numRegistros=count($arrPauta);
      if ($numRegistros==1) {
        $strConteudo .= '1 Item';
      } else {
        $strConteudo .= $numRegistros . ' Itens';
      }
      $strConteudo.= '</div></label>' . "\n";
      $strConteudo.='</div>';

      if($bolMesaAposInicio){
        $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoDTO->setNumIdUsuario(-1);
        $objColegiadoComposicaoDTO->setStrNomeUsuario('Processos colocados em mesa ap�s a abertura da sess�o');
        $arrObjColegiadoComposicaoDTO[]=$objColegiadoComposicaoDTO;
      }

      $arrPauta = InfraArray::indexarArrInfraDTO($arrPauta, 'IdUsuarioSessao', true);
      $i = 1;
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        if (isset($arrPauta[$objColegiadoComposicaoDTO->getNumIdUsuario()])) {
          if ($objSessaoBlocoDTO->getStrStaTipoItem()!==TipoSessaoBlocoRN::$STA_REFERENDO) {
            $strConteudo .= '<label class="infraLabelObrigatorio">' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . '&nbsp;</label>';
          }
          $strConteudo .= '<table width="99%" class="infraTable" summary="' . $strCaption . '">' . "\n";
          $strConteudo .= '<thead><tr>';
          $strConteudo .= '<th class="infraTh" style="width:5%;text-align: center">Ordem</th>' . "\n";
          $strConteudo .= '<th class="infraTh" style="text-align: center">Processo</th>' . "\n";
          $strConteudo .= '</tr></thead>' . "\n";
          $strConteudo .= '<tbody>' . "\n";
          $strCssTr='';
          $arrPautaMembro = $arrPauta[$objColegiadoComposicaoDTO->getNumIdUsuario()];
          foreach ($arrPautaMembro as $objItemSessaoJulgamentoDTO) {
            $strCssTr = ($strCssTr=='<tr class="infraTrClara">') ? '<tr class="infraTrEscura">' : '<tr class="infraTrClara">';
            $strConteudo .= $strCssTr;

            //ordem
            $strConteudo .= '<td style="text-align: center">' . str_pad($i ++, 3, '0', STR_PAD_LEFT) . '</td>';
            //proc

            if ($objItemSessaoJulgamentoDTO->getStrStaSituacao()===ItemSessaoJulgamentoRN::$STA_RETIRADO) {
              $strConteudo .= '<td>Retirado</td>';
            } else if($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo()==ProtocoloRN::$NA_SIGILOSO){
              $strConteudo .= '<td><b>N�mero:</b>'.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo().' - Sigiloso</td>';
            } else {
              $strConteudo.='<td>';
              if($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()==-1){
                $strConteudo.='<b>Pautado por:&nbsp;</b> '.$objItemSessaoJulgamentoDTO->getStrNomeUsuarioRelator().'<br>';
              }
              $strConteudo .= $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo();
              $strConteudo .= ' - ' . $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento();
              $dblIdProcedimento=$objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao();

              if (isset($arrObjAutuacaoDTO[$dblIdProcedimento]) && $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricaoTipoMateria()!=null) {
                $strConteudo .= '<br><b>Tipo de Mat�ria:&nbsp;</b>'.$arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricaoTipoMateria();
              }

              if (isset($arrObjParteProcedimentoDTO[$dblIdProcedimento])) {
                $strConteudo.='<br><b>Partes:&nbsp;</b>';
                $j = InfraArray::contar($arrObjParteProcedimentoDTO[$dblIdProcedimento]);
                foreach ($arrObjParteProcedimentoDTO[$dblIdProcedimento] as $objParteProcedimentoDTO) {
                  $strConteudo .= $objParteProcedimentoDTO->getStrNomeContato() . ' (' . $objParteProcedimentoDTO->getStrDescricaoQualificacaoParte() . ')';
                  if (-- $j>0) {
                    $strConteudo .= $j==1?' e ':', ';
                  }
                }
              } else if (isset($arrObjParticipanteDTO[$dblIdProcedimento])) {
                $strConteudo.='<br><b>Interessados:&nbsp;</b>';
                $j = InfraArray::contar($arrObjParticipanteDTO[$dblIdProcedimento]);
                foreach ($arrObjParticipanteDTO[$dblIdProcedimento] as $objParticipanteDTO) {
                  $strConteudo .= $objParticipanteDTO->getStrNomeContato();
                  if (-- $j>0) {
                    $strConteudo .= $j==1?' e ':', ';
                  }
                }
              }

              if (isset($arrObjAutuacaoDTO[$dblIdProcedimento]) && $arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricao()!=null) {
                $strConteudo .= '<br><b>Descri��o:&nbsp;</b>'.$arrObjAutuacaoDTO[$dblIdProcedimento]->getStrDescricao();
              }
              $strConteudo.='</td>';
            }
            $strConteudo .= "</tr>\n";
          }
          $strConteudo.='</tbody></table>';
          $strConteudo .= "<p style='margin: 0'>&nbsp;</p>\n";

        }
      }
      if(!$bolSessaoVirtual && !$bolSessaoEncerrada && $strMensagem!=''){
        $strConteudo.="<p style='font-size: 12px;margin-top:0'>*$strMensagem</p>\n";
      }
    }
    return $strConteudo;
  }

  public function permiteIncluirDestaque(SessaoJulgamentoDTO $objSessaoJulgamentoDTO):bool {
    return in_array($objSessaoJulgamentoDTO->getStrStaSituacao(),array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA,SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA));
  }

  /**
   * @param int $numIdSessaoJulgamento
   * @param int $numIdTipoSessao
   */
  private function cadastrarSessaoBloco($numIdSessaoJulgamento, $numIdTipoSessao): void
  {
    $objTipoSessaoBlocoDTO = new TipoSessaoBlocoDTO();
    $objTipoSessaoBlocoRN = new TipoSessaoBlocoRN();
    $objTipoSessaoBlocoDTO->setNumIdTipoSessao($numIdTipoSessao);
    $objTipoSessaoBlocoDTO->retTodos();

    $objSessaoBlocoRN = new SessaoBlocoRN();
    $arrObjTipoSessaoBlocoDTO = $objTipoSessaoBlocoRN->listar($objTipoSessaoBlocoDTO);
    foreach ($arrObjTipoSessaoBlocoDTO as $objTipoSessaoBlocoDTO) {
      $objSessaoBlocoDTO = new SessaoBlocoDTO();
      $objSessaoBlocoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
      $objSessaoBlocoDTO->setNumOrdem($objTipoSessaoBlocoDTO->getNumOrdem());
      $objSessaoBlocoDTO->setStrDescricao($objTipoSessaoBlocoDTO->getStrDescricao());
      $objSessaoBlocoDTO->setStrSinAgruparMembro($objTipoSessaoBlocoDTO->getStrSinAgruparMembro());
      $objSessaoBlocoDTO->setStrStaTipoItem($objTipoSessaoBlocoDTO->getStrStaTipoItem());
      $objSessaoBlocoDTO->setNumIdSessaoBloco(null);

      $objSessaoBlocoRN->cadastrar($objSessaoBlocoDTO);
    }
  }


}
?>