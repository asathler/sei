<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class EleicaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'eleicao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdEleicao', 'id_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdItemSessaoJulgamento', 'id_item_sessao_julgamento');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Identificacao', 'identificacao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'Descricao', 'descricao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'StaSituacao', 'sta_situacao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Quantidade', 'quantidade');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Ordem', 'ordem');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'SinSecreta', 'sin_secreta');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM, 'IdUsuario');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR, 'ObjOpcaoEleicaoDTO');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'DescricaoSituacao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR, 'ObjColegiadoComposicaoDTOVotos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR, 'ObjColegiadoComposicaoDTOAguardando');

    $this->configurarPK('IdEleicao',InfraDTO::$TIPO_PK_NATIVA);
  }
}
