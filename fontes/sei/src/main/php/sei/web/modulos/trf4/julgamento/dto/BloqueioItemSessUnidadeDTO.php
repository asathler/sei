<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 06/06/2019 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class BloqueioItemSessUnidadeDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'bloqueio_item_sess_unidade';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdBloqueioItemSessUnidade', 'id_bloqueio_item_sess_unidade');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdDistribuicao', 'id_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DBL, 'IdDocumento', 'id_documento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR, 'StaTipo', 'sta_tipo');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUnidade', 'id_unidade');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUnidade','sigla','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'DescricaoUnidade','descricao','unidade');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DBL, 'IdProcedimentoDistribuicao', 'id_procedimento', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaDistribuicaoDistribuicao', 'sta_distribuicao', 'distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaUltimoDistribuicao', 'sta_ultimo', 'distribuicao');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'NumIdUnidade');

    $this->configurarPK('IdBloqueioItemSessUnidade',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdDocumento','protocolo','id_protocolo',InfraDTO::$TIPO_FK_OPCIONAL);
    $this->configurarFK('IdItemSessaoJulgamento','item_sessao_julgamento','id_item_sessao_julgamento');
    $this->configurarFK('IdDistribuicao', 'distribuicao', 'id_distribuicao');
    $this->configurarFK('IdUnidade','unidade','id_unidade');
  }
}
?>