<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  $objJulgamentoParteDTO = new JulgamentoParteDTO();

  $strDesabilitar = '';
  $strParametros='&id_item_sessao_julgamento='.$_GET['id_item_sessao_julgamento'];
  if(isset($_GET['id_sessao_julgamento'])) {
    $strParametros .= '&id_sessao_julgamento=' . $_GET['id_sessao_julgamento'];
  }
  if(isset($_GET['id_procedimento'])) {
    $strParametros .= '&id_procedimento=' . $_GET['id_procedimento'];
  }


  $arrIdItemSessaoJulgamento = explode(',', $_GET['id_item_sessao_julgamento']);
  $bolMultiplasSessoes = count($arrIdItemSessaoJulgamento)>1;
  if (isset($_POST['selItemSessaoJulgamento'])){
    $numIdItemSessaoJulgamento=$_POST['selItemSessaoJulgamento'];
  } else if($bolMultiplasSessoes){
    $numIdItemSessaoJulgamento = null;
  } else {
    $numIdItemSessaoJulgamento=$arrIdItemSessaoJulgamento[0];
  }

  $arrComandos = array();
  $arrAcoesRemover=array();
  $bolAdmin=false;

  switch($_GET['acao']){

    case 'julgamento_parte_cadastrar':
      $strTitulo = 'Cadastrar Fra��o de Julgamento';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarJulgamentoParte" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      $arrPartes=array();
      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      if(!$bolMultiplasSessoes){
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento[0]);
      } else {
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento,InfraDTO::$OPER_IN);
      }
      $objItemSessaoJulgamentoDTO->retStrNomeColegiado();
      $objItemSessaoJulgamentoDTO->retDthSessaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->retNumIdUsuarioRelatorDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdUnidadeResponsavelColegiado();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTOBanco) {
        $strSessao=$objItemSessaoJulgamentoDTOBanco->getStrNomeColegiado().' - '.substr($objItemSessaoJulgamentoDTOBanco->getDthSessaoSessaoJulgamento(),0,16);
        $objItemSessaoJulgamentoDTOBanco->setStrNomeColegiado($strSessao);
        if($numIdItemSessaoJulgamento==$objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento()){
          $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoDTOBanco;
        }
        $strProcesso = $objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo();
      }
      if ($bolMultiplasSessoes) {
        $strItensSelSessaoJulgamento = InfraINT::montarSelectArrInfraDTO('null', '&nbsp;', $numIdItemSessaoJulgamento, $arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento', 'NomeColegiado');
      } else {
        $strItensSelSessaoJulgamento = InfraINT::montarSelectArrInfraDTO(null, '', $numIdItemSessaoJulgamento, $arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento', 'NomeColegiado');
      }

      if($numIdItemSessaoJulgamento) {



        if ($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_ABERTA &&
            SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') &&
            $objItemSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()==SessaoSEI::getInstance()->getNumIdUnidadeAtual()
        ) {
          $bolAdmin = true;
        }

        $objVotoParteDTO = new VotoParteDTO();
        $objVotoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
        $objVotoParteDTO->retStrNomeUsuario();
        $objVotoParteDTO->retNumIdUsuario();
        $objVotoParteDTO->retNumIdProvimento();
        $objVotoParteDTO->retStrStaVotoParte();
        $objVotoParteDTO->retNumIdJulgamentoParte();
        $objVotoParteDTO->retStrConteudoProvimento();
        $objVotoParteDTO->retStrComplemento();
        $objVotoParteDTO->retNumIdUsuarioVotoParte();
        $objVotoParteRN = new VotoParteRN();
        $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);
        $arrObjVotoParteDTO = InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO, 'IdJulgamentoParte', true);

        if (!isset($_POST['sbmAlterarJulgamentoParte'])) {
          $objJulgamentoParteDTO = new JulgamentoParteDTO();
          $objJulgamentoParteDTO->retNumIdJulgamentoParte();
          $objJulgamentoParteDTO->retStrDescricao();
          $objJulgamentoParteDTO->retNumOrdem();
          $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
          $objJulgamentoParteDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_DESC);
          $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);

          $objJulgamentoParteRN = new JulgamentoParteRN();
          $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
          if (InfraArray::contar($arrObjJulgamentoParteDTO)>0) {
            $numIdUsuarioRelator = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
            foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
              $numIdJulgamentoParte = $objJulgamentoParteDTO->getNumIdJulgamentoParte();
              $strVotos = '';
              $arrVotos = $arrObjVotoParteDTO[$numIdJulgamentoParte];
              if ($arrVotos) {
                $strVotos = JulgamentoParteINT::montarTdVotos($arrVotos, $numIdUsuarioRelator);
                if ($bolAdmin || (InfraArray::contar($arrVotos)==1 && $arrVotos[0]->getNumIdUsuario()==$numIdUsuarioRelator)) {
                  $arrAcoesRemover[] = $numIdJulgamentoParte;
                }
              } else {
                $arrAcoesRemover[] = $numIdJulgamentoParte;
              }
              $arrPartes[] = array($numIdJulgamentoParte, $objJulgamentoParteDTO->getStrDescricao(), $strVotos);
            }
          }
          $strPartes = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrPartes);
        } else {


          $arr = PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnPartes']);
          $numOrdem = count($arr);
          foreach ($arr as $linha) {
            $objJulgamentoParteDTO = new JulgamentoParteDTO();
            $objJulgamentoParteDTO->setStrDescricao($linha[1]);
            $objJulgamentoParteDTO->setNumIdJulgamentoParte($linha[0]);
            $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
            $objJulgamentoParteDTO->setNumOrdem($numOrdem --);
            $arrPartes[] = $objJulgamentoParteDTO;
          }

          $strPartes = $_POST['hdnPartes'];
        }

        if ($_GET['acao_origem']!='sessao_julgamento_consultar') {
          $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\'' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . PaginaSEI::getInstance()->getAcaoRetorno() . '&acao_origem=' . $_GET['acao'] . $strParametros . PaginaSEI::getInstance()->montarAncora($objJulgamentoParteDTO->getNumIdJulgamentoParte())) . '\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
        }

        if (isset($_POST['sbmAlterarJulgamentoParte'])) {
          try {
            $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
            $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();

            $objItemSessaoJulgamentoDTO->setArrObjJulgamentoParteDTO($arrPartes);
            $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
            $objItemSessaoJulgamentoRN->fracionarJulgamento($objItemSessaoJulgamentoDTO);
            PaginaSEI::getInstance()->adicionarMensagem('Fra��es de julgamento do processo alterados com sucesso.');
            $bolExecucaoOK = true;
            $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . PaginaSEI::getInstance()->getAcaoRetorno() . '&acao_origem=' . $_GET['acao'] . $strParametros);
//          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($objJulgamentoParteDTO->getNumIdJulgamentoParte())));
//          die;
          } catch (Exception $e) {
            PaginaSEI::getInstance()->processarExcecao($e);
          }
        }
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }



}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

  #divInfraAreaTela {display:none;}

  #lblItemSessaoJulgamento {position:absolute;left:0%;top:0%;width:50%;}
  #selItemSessaoJulgamento {position:absolute;left:0%;top:38%;width:50%;}

  #lblProcesso {position:absolute;left:53%;top:0%;width:30%;}
  #txtProcesso {position:absolute;left:53%;top:38%;width:30%;}

#lblDescricao {position:absolute;left:0%;top:0%;width:50%;}
#txtDescricao {position:absolute;left:0%;top:38%;width:50%;}
  #btnAdicionar {position:absolute;left:53%;top:38%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

  var objTabelaPartes;

function inicializar(){
  <?if($bolExecucaoOK){ ?>
  fechar_pagina('<?=PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento']);?>','<?=$strLinkRetorno?>');
  return;
  <?}?>

  <?if($numIdItemSessaoJulgamento){?>


  document.getElementById('divInfraAreaTela').style.display = 'block';

  if ('<?=$_GET['acao']?>'=='julgamento_parte_cadastrar'){
    document.getElementById('txtDescricao').focus();
  } else if ('<?=$_GET['acao']?>'=='julgamento_parte_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
  objTabelaPartes = new infraTabelaDinamica('tblPartes','hdnPartes', <?=$_GET['acao']=='julgamento_parte_consultar'?'false,false,false':'true,false,true';?>);
  objTabelaPartes.inserirNoInicio = false;
  objTabelaPartes.alterar= function (arr) {
    document.getElementById('hdnIdItem').value = arr[0];
    document.getElementById('txtDescricao').value = arr[1];
    document.getElementById('hdnVotos').value = arr[2];
  };
  //Monta a��es para remover anexos
  <? if (InfraArray::contar($arrAcoesRemover)>0){
  foreach($arrAcoesRemover as $id) {
  ?>
  objTabelaPartes.adicionarAcoes('<?=$id?>','',false,true);
  <?
  }
  }
  ?>
  objTabelaPartes.remover=function(arr){
    if(this.tbl.rows.length===2){
      alert('Deve existir no m�nimo um item.');
      return false;
    }
    return true;
  };
  objTabelaPartes.gerarEfeitoTabela=true;
  <?}?>
}

function validarCadastro() {


  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

function adicionar(){

  var txt=document.getElementById('txtDescricao');
  var hdn=document.getElementById('hdnIdItem');
  var hdnVotos=document.getElementById('hdnVotos');
  var id = ((hdn.value!=='') ? hdn.value : 'N' + (new Date()).getTime());
  var descricao = txt.value;
  if (infraTrim(descricao)=='') {
    alert('Informe a descri��o.');
    txt.focus();
    return false;
  }
  objTabelaPartes.adicionar([id, descricao,hdnVotos.value]);
  <?if(!$bolAdmin) {?>
  if(hdnVotos.value==='' && hdn.value==''){
    objTabelaPartes.adicionarAcoes(id,'',false,true);
  }
  <?} else {?>
  if (hdn.value=='') {
    objTabelaPartes.adicionarAcoes(id, '', false, true);
  }
  <?} ?>
  //depois de incluir limpa os input
  txt.value = '';
  hdn.value = '';
  hdnVotos.value = '';
  txt.focus();
}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmJulgamentoParteCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblItemSessaoJulgamento" for="selItemSessaoJulgamento" accesskey="" class="infraLabelOpcional">Sess�o de Julgamento:</label>
  <select id="selItemSessaoJulgamento" name="selItemSessaoJulgamento" onchange="this.form.submit();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
    <?= $strItensSelSessaoJulgamento ?>
  </select>

  <label id="lblProcesso" for="txtProcesso" accesskey="" class="infraLabelOpcional">Processo:</label>
  <input type="text" id="txtProcesso" name="txtProcesso" readonly class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" value="<?=$strProcesso?>"/>

  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  if($numIdItemSessaoJulgamento){
  PaginaSEI::getInstance()->abrirAreaDados('5em');
  ?>

  <label id="lblDescricao" for="txtDescricao" accesskey="" class="infraLabelObrigatorio">Descri��o:</label>
  <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <input type="button" id="btnAdicionar" onclick="adicionar();" name="btnAdicionar" value="Adicionar" class="infraButton" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  ?>

  <div id="divTabelaPartes" class="infraAreaTabela">
    <table id="tblPartes" class="infraTable" style="width:99%">
      <caption class="infraCaption"><?=PaginaSEI::getInstance()->gerarCaptionTabela('Fra��es de Julgamento do Processo',0)?></caption>
      <tr>
        <th style="display:none;">ID</th>
        <th class="infraTh" style="width:42.5%;">Descri��o</th>
        <th class="infraTh" style="width:42.5%;">Votos</th>
        <? if($_GET['acao']!='julgamento_parte_consultar') {?>
          <th class="infraTh">A��es</th>
        <? }?>
      </tr>
    </table>
    <input type="hidden" id="hdnPartes" name="hdnPartes" value="<?=$strPartes?>" />
    <input type="hidden" id="hdnIdItem" value="" />
    <input type="hidden" id="hdnVotos" value="" />
  </div>
  <?}?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>