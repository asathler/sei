<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/06/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.34.0
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('provimento_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objProvimentoDTO = new ProvimentoDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'provimento_cadastrar':
      $strTitulo = 'Novo Provimento';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarProvimento" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objProvimentoDTO->setNumIdProvimento(null);
      $objProvimentoDTO->setStrConteudo($_POST['txaConteudo']);
      $objProvimentoDTO->setStrSinAtivo('S');

      if (isset($_POST['sbmCadastrarProvimento'])) {
        try{
          $objProvimentoRN = new ProvimentoRN();
          $objProvimentoDTO = $objProvimentoRN->cadastrar($objProvimentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Provimento "'.$objProvimentoDTO->getStrConteudo().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_provimento='.$objProvimentoDTO->getNumIdProvimento().PaginaSEI::getInstance()->montarAncora($objProvimentoDTO->getNumIdProvimento())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'provimento_alterar':
      $strTitulo = 'Alterar Provimento';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarProvimento" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_provimento'])){
        $objProvimentoDTO->setNumIdProvimento($_GET['id_provimento']);
        $objProvimentoDTO->retTodos();
        $objProvimentoRN = new ProvimentoRN();
        $objProvimentoDTO = $objProvimentoRN->consultar($objProvimentoDTO);
        if ($objProvimentoDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objProvimentoDTO->setNumIdProvimento($_POST['hdnIdProvimento']);
        $objProvimentoDTO->setStrConteudo($_POST['txaConteudo']);
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objProvimentoDTO->getNumIdProvimento())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarProvimento'])) {
        try{
          $objProvimentoRN = new ProvimentoRN();
          $objProvimentoRN->alterar($objProvimentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Provimento "'.$objProvimentoDTO->getStrConteudo().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objProvimentoDTO->getNumIdProvimento())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'provimento_consultar':
      $strTitulo = 'Consultar Provimento';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_provimento'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objProvimentoDTO->setNumIdProvimento($_GET['id_provimento']);
      $objProvimentoDTO->setBolExclusaoLogica(false);
      $objProvimentoDTO->retTodos();
      $objProvimentoRN = new ProvimentoRN();
      $objProvimentoDTO = $objProvimentoRN->consultar($objProvimentoDTO);
      if ($objProvimentoDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblConteudo {position:absolute;left:0%;top:0%;width:95%;}
#txaConteudo {position:absolute;left:0%;top:6%;width:95%;}
#lblAviso {position:absolute;top:0;left:0;width:95%;font-size:1.4em;color:red}
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
function inicializar(){
  if ('<?=$_GET['acao']?>'=='provimento_cadastrar'){
    document.getElementById('txaConteudo').focus();
  } else if ('<?=$_GET['acao']?>'=='provimento_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
}

function validarCadastro() {
  if (infraTrim(document.getElementById('txaConteudo').value)=='') {
    alert('Informe o Conte�do.');
    document.getElementById('txaConteudo').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmProvimentoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
if($_GET['acao']==='provimento_alterar'){
  PaginaSEI::getInstance()->abrirAreaDados('7em');
?>
  <label id="lblAviso" name="lblAviso">ATEN��O: Qualquer altera��o realizada no texto deste provimento ir� retroagir para alterar a proclama��o dos processos que com ele j� foram julgados (considere o uso da desativa��o com cria��o de novo provimento).</label>
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
}
PaginaSEI::getInstance()->abrirAreaDados('30em');
?>
  <label id="lblConteudo" for="txaConteudo" class="infraLabelObrigatorio">Conte�do:</label>
  <textarea id="txaConteudo" name="txaConteudo" class="infraTextarea" rows="5" onkeypress="return infraMascaraTexto(this,event,4000);" maxlength="4000" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>"><?=$objProvimentoDTO->getStrConteudo();?></textarea>

  <input type="hidden" id="hdnIdProvimento" name="hdnIdProvimento" value="<?=$objProvimentoDTO->getNumIdProvimento();?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>