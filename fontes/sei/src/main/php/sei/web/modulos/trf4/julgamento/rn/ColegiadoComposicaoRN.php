<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoComposicaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdColegiadoVersao(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoComposicaoDTO->getNumIdColegiadoVersao())){
      $objInfraException->adicionarValidacao('Id Colegiado Vers�o n�o informado.');
    }
  }

  private function validarNumIdUnidade(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoComposicaoDTO->getNumIdUnidade())){
      $objInfraException->adicionarValidacao('Id Unidade n�o informado.');
    }
  }
  private function validarNumIdTipoMembroColegiado(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado())){
      $objInfraException->adicionarValidacao('Tipo de membro do colegiado n�o informado.');
    }
    if($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR){
      $objColegiadoComposicaoDTO->setStrSinHabilitado('S');
    }
  }
  private function validarNumOrdem(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoComposicaoDTO->getNumOrdem())){
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }
  private function validarDblPeso(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
    $peso=$objColegiadoComposicaoDTO->getDblPeso();
    if (InfraString::isBolVazia($peso)){
      $objInfraException->adicionarValidacao('Peso n�o informado.');
    } else if ($peso<0 || $peso>1){
      $objInfraException->adicionarValidacao('Peso deve ser um valor entre 0,0 e 1,0.');
    }
    $objColegiadoComposicaoDTO->setDblPeso(ceil($peso*10)/10);
  }
  private function validarNumIdUsuario(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoComposicaoDTO->getNumIdUsuario())){
      $objInfraException->adicionarValidacao('Id Relator n�o informado.');
    }
  }
  private function validarStrSinRodada(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
  	if (InfraString::isBolVazia($objColegiadoComposicaoDTO->getStrSinRodada())){
  		$objInfraException->adicionarValidacao('Sinalizador da Rodada n�o informado.');
  	}
  }
  private function validarStrSinHabilitado(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objColegiadoComposicaoDTO->getStrSinHabilitado())){
      $objInfraException->adicionarValidacao('Sinalizador de Habilitado n�o informado.');
    }
  }

  protected function cadastrarControlado(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_composicao_cadastrar',__METHOD__,$objColegiadoComposicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdColegiadoVersao($objColegiadoComposicaoDTO, $objInfraException);
      $this->validarNumIdTipoMembroColegiado($objColegiadoComposicaoDTO, $objInfraException);
      $this->validarNumIdUnidade($objColegiadoComposicaoDTO, $objInfraException);
      $this->validarNumIdUsuario($objColegiadoComposicaoDTO, $objInfraException);
      $this->validarStrSinRodada($objColegiadoComposicaoDTO, $objInfraException);
      $this->validarStrSinHabilitado($objColegiadoComposicaoDTO, $objInfraException);
      $this->validarNumOrdem($objColegiadoComposicaoDTO, $objInfraException);
      $this->validarDblPeso($objColegiadoComposicaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objColegiadoComposicaoBD = new ColegiadoComposicaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoComposicaoBD->cadastrar($objColegiadoComposicaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Colegiado Composi��o.',$e);
    }
  }

  protected function alterarControlado(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_composicao_alterar',__METHOD__,$objColegiadoComposicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objColegiadoComposicaoDTO->isSetNumIdColegiadoVersao()){
        $this->validarNumIdColegiadoVersao($objColegiadoComposicaoDTO, $objInfraException);
      }
      if ($objColegiadoComposicaoDTO->isSetNumIdUnidade()){
        $this->validarNumIdUnidade($objColegiadoComposicaoDTO, $objInfraException);
      }
      if ($objColegiadoComposicaoDTO->isSetNumIdUsuario()){
        $this->validarNumIdUsuario($objColegiadoComposicaoDTO, $objInfraException);
      }
      if ($objColegiadoComposicaoDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objColegiadoComposicaoDTO, $objInfraException);
      }
      if ($objColegiadoComposicaoDTO->isSetDblPeso()){
        $this->validarDblPeso($objColegiadoComposicaoDTO, $objInfraException);
      }
      if ($objColegiadoComposicaoDTO->isSetStrSinRodada()){
      	$this->validarStrSinRodada($objColegiadoComposicaoDTO, $objInfraException);
      }
      if ($objColegiadoComposicaoDTO->isSetStrSinHabilitado()){
        $this->validarStrSinHabilitado($objColegiadoComposicaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objColegiadoComposicaoBD = new ColegiadoComposicaoBD($this->getObjInfraIBanco());
      $objColegiadoComposicaoBD->alterar($objColegiadoComposicaoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Colegiado Composi��o.',$e);
    }
  }

  protected function excluirControlado($arrObjColegiadoComposicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_composicao_excluir',__METHOD__,$arrObjColegiadoComposicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoComposicaoBD = new ColegiadoComposicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjColegiadoComposicaoDTO);$i++){


        $objColegiadoComposicaoDTOBanco=new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoDTOBanco->setNumIdColegiadoComposicao($arrObjColegiadoComposicaoDTO[$i]->getNumIdColegiadoComposicao());
        $objColegiadoComposicaoDTOBanco->retNumIdColegiadoColegiadoVersao();
        $objColegiadoComposicaoDTOBanco->retStrNomeUsuario();
        $objColegiadoComposicaoDTOBanco->retNumIdUsuario();
        $objColegiadoComposicaoDTOBanco->retStrDescricaoUnidade();
        $objColegiadoComposicaoDTOBanco=$this->consultar($objColegiadoComposicaoDTOBanco);

        //validar se membro est� presente em alguma sess�o n�o finalizada
        $objPresencaSessaoDTO=new PresencaSessaoDTO();
        $objPresencaSessaoRN=new PresencaSessaoRN();
        $objPresencaSessaoDTO->setNumIdColegiadoSessaoJulgamento($objColegiadoComposicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
        $objPresencaSessaoDTO->setDthSaida(null);
        $objPresencaSessaoDTO->setStrStaSituacaoSessaoJulgamento(array(SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_FINALIZADA),InfraDTO::$OPER_NOT_IN);
        $objPresencaSessaoDTO->setNumIdUsuario($objColegiadoComposicaoDTOBanco->getNumIdUsuario());
        $objPresencaSessaoDTO->retNumIdPresencaSessao();
        if ($objPresencaSessaoRN->contar($objPresencaSessaoDTO)>0){
          $objInfraException->lancarValidacao('O usu�rio "'.$objColegiadoComposicaoDTOBanco->getStrNomeUsuario().'" est� presente em uma sess�o n�o encerrada.');
        }
        //validar se possui ou processo distribu�do para o relator antes de excluir
//        $objDistribuicaoDTO=new DistribuicaoDTO();
//        $objDistribuicaoRN=new DistribuicaoRN();
//        $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($objColegiadoComposicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
//        $objDistribuicaoDTO->setNumIdUsuarioRelator($objColegiadoComposicaoDTOBanco->getNumIdUsuario());
//        $objDistribuicaoDTO->setStrStaUltimo(DistribuicaoRN::$SD_ULTIMA);
//        $objDistribuicaoDTO->setStrStaDistribuicao(array(DistribuicaoRN::$STA_JULGADO,DistribuicaoRN::$STA_CANCELADO),InfraDTO::$OPER_NOT_IN);
//        $objDistribuicaoDTO->retNumIdDistribuicao();
//        $qtd=$objDistribuicaoRN->contar($objDistribuicaoDTO);
//        if($qtd>0){
//          $objInfraException->lancarValidacao('O usu�rio "'.$objColegiadoComposicaoDTOBanco->getStrNomeUsuario().'" possui '.$qtd.' processo(s) para relatoria na unidade "'.$objColegiadoComposicaoDTOBanco->getStrDescricaoUnidade().'".');
//        }
//
//        //validar se possui pedido de vista para o membro antes de excluir
//        $objPedidoVistaDTO=new PedidoVistaDTO();
//        $objPedidoVistaRN=new PedidoVistaRN();
//        $objPedidoVistaDTO->setNumIdUsuario($objColegiadoComposicaoDTOBanco->getNumIdUsuario());
//        $objPedidoVistaDTO->setDthDevolucao(null);
//        $objPedidoVistaDTO->setNumIdColegiadoColegiadoVersao($objColegiadoComposicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
//        $objPedidoVistaDTO->retNumIdPedidoVista();
//        $qtd=$objPedidoVistaRN->contar($objPedidoVistaDTO);
//        if($qtd>0){
//          $objInfraException->lancarValidacao('O usu�rio "'.$objColegiadoComposicaoDTOBanco->getStrNomeUsuario().'" possui '.$qtd.' pedido(s) de vista na unidade "'.$objColegiadoComposicaoDTOBanco->getStrDescricaoUnidade().'".');
//        }

        $objColegiadoComposicaoBD->excluir($arrObjColegiadoComposicaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Colegiado Composi��o.',$e);
    }
  }

  protected function consultarConectado(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_composicao_consultar',__METHOD__,$objColegiadoComposicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoComposicaoBD = new ColegiadoComposicaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoComposicaoBD->consultar($objColegiadoComposicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Colegiado Composi��o.',$e);
    }
  }

  protected function listarConectado(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO) {
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_composicao_listar',__METHOD__,$objColegiadoComposicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoComposicaoBD = new ColegiadoComposicaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoComposicaoBD->listar($objColegiadoComposicaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Colegiado Composi��es.',$e);
    }
  }

  protected function contarConectado(ColegiadoComposicaoDTO $objColegiadoComposicaoDTO){
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('colegiado_composicao_listar',__METHOD__,$objColegiadoComposicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objColegiadoComposicaoBD = new ColegiadoComposicaoBD($this->getObjInfraIBanco());
      $ret = $objColegiadoComposicaoBD->contar($objColegiadoComposicaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Colegiado Composi��es.',$e);
    }
  }
}
?>