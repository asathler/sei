<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/12/2018 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ . '/../../../../SEI.php';

class RelMotivoDistrColegiadoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdColegiado(RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelMotivoDistrColegiadoDTO->getNumIdColegiado())){
      $objInfraException->adicionarValidacao(' n�o informad.');
    }
  }

  private function validarNumIdMotivoDistribuicao(RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objRelMotivoDistrColegiadoDTO->getNumIdMotivoDistribuicao())){
      $objInfraException->adicionarValidacao(' n�o informad.');
    }
  }

  protected function cadastrarControlado(RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_motivo_distr_colegiado_cadastrar',__METHOD__,$objRelMotivoDistrColegiadoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdColegiado($objRelMotivoDistrColegiadoDTO, $objInfraException);
      $this->validarNumIdMotivoDistribuicao($objRelMotivoDistrColegiadoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objRelMotivoDistrColegiadoBD = new RelMotivoDistrColegiadoBD($this->getObjInfraIBanco());
      $ret = $objRelMotivoDistrColegiadoBD->cadastrar($objRelMotivoDistrColegiadoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando .',$e);
    }
  }

  protected function alterarControlado(RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('rel_motivo_distr_colegiado_alterar',__METHOD__,$objRelMotivoDistrColegiadoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objRelMotivoDistrColegiadoDTO->isSetNumIdColegiado()){
        $this->validarNumIdColegiado($objRelMotivoDistrColegiadoDTO, $objInfraException);
      }
      if ($objRelMotivoDistrColegiadoDTO->isSetNumIdMotivoDistribuicao()){
        $this->validarNumIdMotivoDistribuicao($objRelMotivoDistrColegiadoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objRelMotivoDistrColegiadoBD = new RelMotivoDistrColegiadoBD($this->getObjInfraIBanco());
      $objRelMotivoDistrColegiadoBD->alterar($objRelMotivoDistrColegiadoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando .',$e);
    }
  }

  protected function excluirControlado($arrObjRelMotivoDistrColegiadoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_motivo_distr_colegiado_excluir',__METHOD__,$arrObjRelMotivoDistrColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelMotivoDistrColegiadoBD = new RelMotivoDistrColegiadoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjRelMotivoDistrColegiadoDTO);$i++){
        $objRelMotivoDistrColegiadoBD->excluir($arrObjRelMotivoDistrColegiadoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo .',$e);
    }
  }

  protected function consultarConectado(RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_motivo_distr_colegiado_consultar',__METHOD__,$objRelMotivoDistrColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelMotivoDistrColegiadoBD = new RelMotivoDistrColegiadoBD($this->getObjInfraIBanco());
      $ret = $objRelMotivoDistrColegiadoBD->consultar($objRelMotivoDistrColegiadoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando .',$e);
    }
  }

  protected function listarConectado(RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_motivo_distr_colegiado_listar',__METHOD__,$objRelMotivoDistrColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelMotivoDistrColegiadoBD = new RelMotivoDistrColegiadoBD($this->getObjInfraIBanco());
      $ret = $objRelMotivoDistrColegiadoBD->listar($objRelMotivoDistrColegiadoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando .',$e);
    }
  }

  protected function contarConectado(RelMotivoDistrColegiadoDTO $objRelMotivoDistrColegiadoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('rel_motivo_distr_colegiado_listar',__METHOD__,$objRelMotivoDistrColegiadoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objRelMotivoDistrColegiadoBD = new RelMotivoDistrColegiadoBD($this->getObjInfraIBanco());
      $ret = $objRelMotivoDistrColegiadoBD->contar($objRelMotivoDistrColegiadoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando .',$e);
    }
  }
}
?>