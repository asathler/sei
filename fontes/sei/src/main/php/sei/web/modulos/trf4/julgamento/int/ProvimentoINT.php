<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/06/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.34.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ProvimentoINT extends InfraINT {

  public static function montarSelectConteudo($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objProvimentoDTO = new ProvimentoDTO();
    $objProvimentoDTO->retNumIdProvimento();
    $objProvimentoDTO->retStrConteudo();

    $objProvimentoDTO->setOrdStrConteudo(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objProvimentoRN = new ProvimentoRN();
    $arrObjProvimentoDTO = $objProvimentoRN->listar($objProvimentoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjProvimentoDTO, 'IdProvimento', 'Conteudo');
  }
}
?>