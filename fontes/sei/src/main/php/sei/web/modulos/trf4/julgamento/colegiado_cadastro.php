<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('colegiado_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);


  $objColegiadoDTO = new ColegiadoDTO();

  $strDesabilitar = '';
  $bolConsultaHistorico=false;
  $arrComandos = array();

  switch($_GET['acao']){
    case 'colegiado_cadastrar':
      $strTitulo = 'Novo Colegiado';

      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarColegiado" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objColegiadoDTO->setNumIdColegiado(null);
      $objColegiadoDTO->setStrNome($_POST['txtNome']);
      $objColegiadoDTO->setStrArtigo($_POST['txtArtigo']);
			$objColegiadoDTO->setStrSigla($_POST['txtSigla']);
			$objColegiadoDTO->setNumIdTipoProcedimento($_POST['selTipoProcedimento']);
			$objColegiadoDTO->setNumIdUsuarioPresidente($_POST['infraRadioPresidente']);
			$objColegiadoDTO->setNumIdUsuarioSecretario($_POST['hdnIdUsuarioSecretario']);
      $objColegiadoDTO->setNumIdUnidadeResponsavel($_POST['selUnidadeResponsavel']);
			$objColegiadoDTO->setStrNomeSecretario($_POST['hdnNomeUsuarioSecretario']);
      $objColegiadoDTO->setNumQuorumMinimo($_POST['txtQuorum']);
      $objColegiadoDTO->setStrStaAlgoritmoDistribuicao($_POST['selAlgoritmo']);
      $objColegiadoDTO->setStrSinAtivo('S');

      $arr = PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnColegiado']);

      $arrColegiado = array();
			$ordem=InfraArray::contar($arr);

      foreach($arr as $linha){
      	$objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
      	$objColegiadoComposicaoDTO->setNumIdUnidade($linha[1]);
      	$objColegiadoComposicaoDTO->setNumIdUsuario($linha[0]);
				$objColegiadoComposicaoDTO->setNumOrdem($ordem--);
        $objColegiadoComposicaoDTO->setNumIdTipoMembroColegiado($linha[5]);
        $objColegiadoComposicaoDTO->setNumIdCargo($linha[8]);
        $objColegiadoComposicaoDTO->setStrSinHabilitado($linha[9]);
        if($linha[5]==TipoMembroColegiadoRN::$TMC_TITULAR){
          $objColegiadoComposicaoDTO->setDblPeso(InfraUtil::prepararDbl($linha[7]));
        } else {
          $objColegiadoComposicaoDTO->setDblPeso('0.0');
        }
      	$arrColegiado[] = $objColegiadoComposicaoDTO;
      }

      $objColegiadoDTO->setArrObjColegiadoComposicaoDTO($arrColegiado);


      $arr =PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnObservadores']);

      $arrObservadores=array();
      foreach ($arr as $linha) {
        $objRelColegiadoUsuarioDTO=new RelColegiadoUsuarioDTO();
        $objRelColegiadoUsuarioDTO->setNumIdUsuario($linha[0]);
        $arrObservadores[]=$objRelColegiadoUsuarioDTO;
      }
      $objColegiadoDTO->setArrObjRelColegiadoUsuarioDTO($arrObservadores);


      $strObservadores= $_POST['hdnObservadores'];
      $strComposicaoColegiado = $_POST['hdnColegiado'];


      if (isset($_POST['sbmCadastrarColegiado'])) {
        try{
          $objColegiadoRN = new ColegiadoRN();
          $objColegiadoDTO = $objColegiadoRN->cadastrar($objColegiadoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Colegiado "'.$objColegiadoDTO->getStrNome().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_colegiado='.$objColegiadoDTO->getNumIdColegiado().PaginaSEI::getInstance()->montarAncora($objColegiadoDTO->getNumIdColegiado())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'colegiado_alterar':
      $strTitulo = 'Alterar Colegiado';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarColegiado" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_colegiado'])){
        $objColegiadoDTO->setNumIdColegiado($_GET['id_colegiado']);
        $objColegiadoDTO->retTodos();
        $objColegiadoRN = new ColegiadoRN();
        $objColegiadoDTO = $objColegiadoRN->consultarCompleto($objColegiadoDTO);
        if ($objColegiadoDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
      	$objColegiadoDTO->setNumIdColegiado($_POST['hdnIdColegiado']);
      	$objColegiadoDTO->setStrNome($_POST['txtNome']);
        $objColegiadoDTO->setStrArtigo($_POST['txtArtigo']);
				$objColegiadoDTO->setStrSigla($_POST['txtSigla']);
				$objColegiadoDTO->setNumIdTipoProcedimento($_POST['selTipoProcedimento']);
				$objColegiadoDTO->setNumIdUsuarioPresidente($_POST['infraRadioPresidente']);
				$objColegiadoDTO->setNumIdUsuarioSecretario($_POST['hdnIdUsuarioSecretario']);
        $objColegiadoDTO->setNumIdUnidadeResponsavel($_POST['selUnidadeResponsavel']);
				$objColegiadoDTO->setStrNomeSecretario($_POST['txtSecretario']);
        $objColegiadoDTO->setNumQuorumMinimo($_POST['txtQuorum']);
        $objColegiadoDTO->setStrStaAlgoritmoDistribuicao($_POST['selAlgoritmo']);

      	$arr = PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnColegiado']);

	      $arrColegiado = array();
        $ordem=InfraArray::contar($arr);
	      foreach($arr as $linha){
	      	$objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
	      	$objColegiadoComposicaoDTO->setNumIdUnidade($linha[1]);
	      	$objColegiadoComposicaoDTO->setNumIdUsuario($linha[0]);
          $objColegiadoComposicaoDTO->setNumOrdem($ordem--);
          $objColegiadoComposicaoDTO->setNumIdTipoMembroColegiado($linha[5]);
          $objColegiadoComposicaoDTO->setNumIdCargo($linha[8]);
          $objColegiadoComposicaoDTO->setStrSinHabilitado($linha[9]);
          if($linha[5]==TipoMembroColegiadoRN::$TMC_TITULAR){
            $objColegiadoComposicaoDTO->setDblPeso(InfraUtil::prepararDbl($linha[7]));
          } else {
            $objColegiadoComposicaoDTO->setDblPeso('0.0');
          }

	      	$arrColegiado[] = $objColegiadoComposicaoDTO;
	      }

	      $objColegiadoDTO->setArrObjColegiadoComposicaoDTO($arrColegiado);
        $arr =PaginaSEI::getInstance()->getArrItensTabelaDinamica($_POST['hdnObservadores']);

        $arrObservadores=array();
        foreach ($arr as $linha) {
          $objRelColegiadoUsuarioDTO=new RelColegiadoUsuarioDTO();
          $objRelColegiadoUsuarioDTO->setNumIdColegiado($_POST['hdnIdColegiado']);
          $objRelColegiadoUsuarioDTO->setNumIdUsuario($linha[0]);
          $arrObservadores[]=$objRelColegiadoUsuarioDTO;
        }
        $objColegiadoDTO->setArrObjRelColegiadoUsuarioDTO($arrObservadores);
        $strObservadores= $_POST['hdnObservadores'];
	      $strComposicaoColegiado = $_POST['hdnColegiado'];
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objColegiadoDTO->getNumIdColegiado())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarColegiado'])) {
        try{
          $objColegiadoRN = new ColegiadoRN();
          $objColegiadoRN->alterar($objColegiadoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Colegiado "'.$objColegiadoDTO->getStrNome().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objColegiadoDTO->getNumIdColegiado())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'colegiado_consultar':
      $strTitulo = 'Consultar Colegiado';
      $objColegiadoDTO->setNumIdColegiado($_GET['id_colegiado']);
      $objColegiadoDTO->setBolExclusaoLogica(false);
      $objColegiadoDTO->retTodos();
      $objColegiadoRN = new ColegiadoRN();
      if (isset($_GET['id_colegiado_versao'])) {
      	$objColegiadoDTO->setNumIdColegiadoVersao($_GET['id_colegiado_versao']);
        $bolConsultaHistorico=true;
        $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_colegiado='.$_GET['id_colegiado'].PaginaSEI::getInstance()->montarAncora($_GET['id_colegiado_versao'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      } else {
        $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_colegiado='.$_GET['id_colegiado'].PaginaSEI::getInstance()->montarAncora($_GET['id_colegiado'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      }
      $objColegiadoDTO = $objColegiadoRN->consultarCompleto($objColegiadoDTO);
      if ($objColegiadoDTO===null){
        throw new InfraException('Registro n�o encontrado.');
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


  if($bolConsultaHistorico){
    $objColegiadoVersaoDTO=new ColegiadoVersaoDTO();
    $objColegiadoVersaoRN=new ColegiadoVersaoRN();
    $objColegiadoVersaoDTO->setNumIdColegiadoVersao($_GET['id_colegiado_versao']);
    $objColegiadoVersaoDTO->retDthVersao();
    $objColegiadoVersaoDTO=$objColegiadoVersaoRN->consultar($objColegiadoVersaoDTO);
    $strTitulo.=' - Vers�o de '.$objColegiadoDTO->getDthCriacaoVersao();
  }

  if (!isset($_POST['hdnColegiado']) && $objColegiadoDTO->getNumIdColegiado()!=null){
		$arrObjColegiadoComposicaoDTO=$objColegiadoDTO->getArrObjColegiadoComposicaoDTO();
  	$arrComposicaoColegiado = array();

  	if (InfraArray::contar($arrObjColegiadoComposicaoDTO)>0){
	  	foreach($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO){
	  		$arrComposicaoColegiado[] = array($objColegiadoComposicaoDTO->getNumIdUsuario(),
	  				$objColegiadoComposicaoDTO->getNumIdUnidade(),
	  				$objColegiadoComposicaoDTO->getStrNomeUsuario().' ('.$objColegiadoComposicaoDTO->getStrSiglaUsuario().')',
            $objColegiadoComposicaoDTO->getStrSiglaUnidade(),
            $objColegiadoComposicaoDTO->getStrExpressaoCargo()==null?'':$objColegiadoComposicaoDTO->getStrExpressaoCargo(),
            $objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado(),
            $objColegiadoComposicaoDTO->getStrNomeTipoMembroColegiado(),
            $objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR?InfraUtil::formatarDbl($objColegiadoComposicaoDTO->getDblPeso(),1):'',
            $objColegiadoComposicaoDTO->getNumIdCargo(),
            $objColegiadoComposicaoDTO->getStrSinHabilitado(),
            $objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR?'':($objColegiadoComposicaoDTO->getStrSinHabilitado()=='S'?'Sim':'N�o'));
	  	}
  	}
  	$strComposicaoColegiado = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrComposicaoColegiado);

    $arrObjRelColegiadoUsuarioDTO=$objColegiadoDTO->getArrObjRelColegiadoUsuarioDTO();
    $arrObservadores=array();
    if(InfraArray::contar($arrObjRelColegiadoUsuarioDTO)>0){
      foreach ($arrObjRelColegiadoUsuarioDTO as $objRelColegiadoUsuarioDTO) {
        $arrObservadores[]=array(
            $objRelColegiadoUsuarioDTO->getNumIdUsuario(),
            $objRelColegiadoUsuarioDTO->getStrSiglaUsuario(),
            $objRelColegiadoUsuarioDTO->getStrNomeUsuario()
        );
      }
    }
    $strObservadoresColegiado = PaginaSEI::getInstance()->gerarItensTabelaDinamica($arrObservadores);
  }
  $strLinkAjaxUnidade = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=usuario_unidades_permissao');
	$strLinkAjaxUsuario = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=usuario_auto_completar');
  $strLinkAjaxUsuarioExterno = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=usuario_externo_auto_completar');
  $strLinkAjaxCargo = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=cargo_auto_completar');
  $strLinkAjaxMembro = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=usuario_unidade_julgamento');
  $strItensSelTipo=TipoMembroColegiadoINT::montarSelectNome('null','&nbsp;',null);
	$strItensSelTipoProcedimento = TipoProcedimentoINT::montarSelectNome('null','&nbsp;',$objColegiadoDTO->getNumIdTipoProcedimento(),true);
  $strItensSelUnidadeResponsavel = UnidadeINT::montarSelectSiglaDescricao('null', '&nbsp', $objColegiadoDTO->getNumIdUnidadeResponsavel());
  $strItensSelAlgoritmo = AlgoritmoINT::montarSelectStaAlgoritmo('null','&nbsp;',$objColegiadoDTO->getStrStaAlgoritmoDistribuicao());

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>

#lblSigla {position:absolute;left:0%;top:0%;width:15%;}
#txtSigla {position:absolute;left:0%;top:8%;width:15%;}

#lblArtigo {position:absolute;left:16%;top:0%;width:4%;}
#txtArtigo {position:absolute;left:16%;top:8%;width:4%;}

#lblNome {position:absolute;left:21%;top:0%;width:49%;}
#txtNome {position:absolute;left:21%;top:8%;width:48%;}

#lblTipoProcedimento {position:absolute;left:0%;top:20%;width:69%;}
#selTipoProcedimento {position:absolute;left:0%;top:28%;width:69%;}

#lblUnidadeResponsavel {position:absolute;left:0%;top:40%;width:69%;}
#selUnidadeResponsavel {position:absolute;left:0%;top:48%;width:69%;}

#lblSecretario {position:absolute;left:0%;top:60%;width:68%;}
#txtSecretario {position:absolute;left:0%;top:68%;width:68%;}

#lblQuorum {position:absolute;left:0%;top:80%;width:15%;}
#txtQuorum {position:absolute;left:0%;top:88%;width:15%;}

#lblAlgoritmo {position:absolute;left:17%;top:80%;width:52%;}
#selAlgoritmo {position:absolute;left:17%;top:88%;width:52%;}

#lblComposicao {position:absolute;top:0%;width:95%;margin-top:.3em;}

#lblUsuario {position:absolute;left:0%;top:19%;width:68%;}
#txtUsuario {position:absolute;left:0%;top:29%;width:68%;}

#lblUnidade {position:absolute;left:0%;top:46%;width:69%;}
#selUnidade {position:absolute;left:0%;top:56%;width:69%;}

#lblCargo {position:absolute;left:0%;top:72%;width:40%;}
#txtCargo {position:absolute;left:0%;top:82%;width:40%;}

#lblTipoMembroColegiado {position:absolute;left:42%;top:72%;width:15%;}
#selTipoMembroColegiado {position:absolute;left:42%;top:82%;width:15%;}

#lblPeso {position:absolute;left:58%;top:72%;width:10%;}
#txtPeso {position:absolute;left:58%;top:82%;width:10%;}

#btnAdicionar {position:absolute;left:73%;top:80%;}

#divTabelaComposicaoColegiado {margin-top:1em;}
#tblColegiado {width:95%;}

#divTabelaObservadores {margin-top:1em;}
#tblObservadores {width:95%;}
  #lblObservadores {position:absolute;top:15%;width:95%;}
  #lblUsuarioExterno {position:absolute;left:0%;top:48%;width:68%;}
  #txtUsuarioExterno {position:absolute;left:0%;top:68%;width:68%;}
  #btnAdicionarObservador {position:absolute;left:73%;top:65%;}

  #divSinHabilitado {position:absolute;left:58%;top:82%;}
<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
//PaginaSEI::getInstance()->abrirJavaScript();
?>
<script type="text/javascript" charset="iso-8859-1" >
<!--//--><![CDATA[//><!--

var objTabelaColegiado,objTabelaObsevadores,
    objAutoCompletarUsuario,objAutoCompletarUsuario2,objAutoCompletarCargo,objAutoCompletarUsuarioExterno,
    objAjaxUnidade;

function inicializar(){

  <? if($_GET['acao']=='colegiado_cadastrar'){ ?>
    document.getElementById('txtSigla').focus();
  <? } else if ($_GET['acao']=='colegiado_consultar'){ ?>
    infraDesabilitarCamposAreaDados();
  <? }else{ ?>
    document.getElementById('btnCancelar').focus();
  <? }?>

  infraEfeitoTabelas();

  var config={
    gerarEfeitoTabela:true,
    flagAlterar:<?=$_GET['acao']=='colegiado_consultar'?'false':'true'?>,
    flagRemover:<?=$_GET['acao']=='colegiado_consultar'?'false':'true'?>,
    flagOrdenar:true,
    nomeRadio:'infraRadioPresidente',
    idRadioSelecionado:'<?=$objColegiadoDTO->getNumIdUsuarioPresidente()?>'
  };
  var config_observadores={
    gerarEfeitoTabela:true,
    flagRemover:<?=$_GET['acao']=='colegiado_consultar'?'false':'true'?>
  }

  objTabelaColegiado = new infraTabelaDinamica2('tblColegiado','hdnColegiado', config);
  objTabelaColegiado.inserirNoInicio=false;
  objTabelaColegiado.alterar= function (arr) {
    document.getElementById('hdnIdUsuario').value=arr[1];
    document.getElementById('hdnIdUnidade').value=arr[2];
    document.getElementById('selTipoMembroColegiado').value=arr[6];
    document.getElementById('txtPeso').value=arr[8];
    if (arr[5] === '') {
      $('#hdnIdCargo').val('');
      objAutoCompletarCargo.limpar();
    } else {
      objAutoCompletarCargo.selecionar(arr[9], arr[5]);
    }
    objAutoCompletarUsuario2.selecionar(arr[1],arr[3]);
    document.getElementById('chkSinHabilitado').checked=arr[10]==='S';
    ajustarTipo();
  };

  <? if(!$bolConsultaHistorico){ ?>
  objTabelaObsevadores = new infraTabelaDinamica2('tblObservadores','hdnObservadores', config_observadores);
  objTabelaObsevadores.inserirNoInicio = false;
  objTabelaObsevadores.alterar= function (arr) {
    document.getElementById('hdnIdUsuarioExterno').value=arr[0];
    document.getElementById('hdnSigla').value=arr[1];
    document.getElementById('txtUsuarioExterno').value=arr[2];
  };


  objTabelaObsevadores.gerarEfeitoTabela=true;

 <? }
    if($_GET['acao']!='colegiado_consultar'){ ?>


	objAutoCompletarUsuario = new infraAjaxAutoCompletar('hdnIdUsuarioSecretario','txtSecretario','<?= $strLinkAjaxUsuario ?>');
	objAutoCompletarUsuario.limparCampo = true;
	objAutoCompletarUsuario.prepararExecucao = function(){
		return 'palavras_pesquisa='+document.getElementById('txtSecretario').value;
	};
  objAutoCompletarUsuario.processarResultado = function(id,descricao){
    if (id != ''){
      document.getElementById('hdnNomeUsuarioSecretario').value = descricao;
    }else{
      document.getElementById('hdnNomeUsuarioSecretario').value = '';
    }
  };

	objAutoCompletarUsuario.selecionar('<?=$objColegiadoDTO->getNumIdUsuarioSecretario()?>','<?=PaginaSEI::getInstance()->formatarParametrosJavaScript($objColegiadoDTO->getStrNomeSecretario(),false)?>');


  objAutoCompletarUsuario2 = new infraAjaxAutoCompletar('hdnIdUsuario','txtUsuario','<?= $strLinkAjaxUsuario ?>');
  objAutoCompletarUsuario2.limparCampo = true;
  objAutoCompletarUsuario2.prepararExecucao = function(){
    return 'palavras_pesquisa='+document.getElementById('txtUsuario').value;
  };
  objAutoCompletarUsuario2.processarResultado = function(id){
    if (id!=''){
      objAjaxUnidade.executar();
      document.getElementById('selUnidade').focus();
    }
  };

  objAutoCompletarUsuarioExterno = new infraAjaxAutoCompletar('hdnIdUsuarioExterno','txtUsuarioExterno','<?= $strLinkAjaxUsuarioExterno ?>');
  objAutoCompletarUsuarioExterno.limparCampo = true;
  objAutoCompletarUsuarioExterno.prepararExecucao = function(){
    return 'palavras_pesquisa='+document.getElementById('txtUsuarioExterno').value;
  };

  objAutoCompletarCargo = new infraAjaxAutoCompletar('hdnIdCargo','txtCargo','<?= $strLinkAjaxCargo ?>');
  objAutoCompletarCargo.limparCampo = true;
  objAutoCompletarCargo.prepararExecucao = function(){
    return 'palavras_pesquisa='+document.getElementById('txtCargo').value;
  };
  objAutoCompletarCargo.processarResultado = function(id){
    if (id!=''){
      document.getElementById('selTipoMembroColegiado').focus();
    }
  };


  objAjaxUnidade = new infraAjaxMontarSelect('selUnidade','<?=$strLinkAjaxUnidade?>');
  objAjaxUnidade.mostrarAviso = false;
  objAjaxUnidade.tempoAviso = 1000;
  objAjaxUnidade.prepararExecucao = function(){
    return 'sin_todos=S&id_usuario='+document.getElementById('hdnIdUsuario').value+'&id_unidade='+document.getElementById('hdnIdUnidade').value;
  };

<? }?>
}

function adicionar(){
  if (infraTrim(document.getElementById('txtUsuario').value)=='') {
    alert('Informe o Usu�rio.');
    document.getElementById('txtUsuario').focus();
    return false;
  }
  if (infraTrim(document.getElementById('txtCargo').value)=='') {
    alert('Informe o Cargo.');
    document.getElementById('txtCargo').focus();
    return false;
  }
  if (!infraSelectSelecionado('selUnidade')) {
    alert('Selecione a Unidade.');
    document.getElementById('selUnidade').focus();
    return false;
  }
  var idTipoMembroColegiado=document.getElementById('selTipoMembroColegiado').value;
  if (infraTrim(idTipoMembroColegiado)==='null') {
    alert('Selecione o Tipo.');
    document.getElementById('selTipoMembroColegiado').focus();
    return false;
  }

  var peso=document.getElementById('txtPeso').value;
  var sinHabilitado,strHabilitado;
  if(idTipoMembroColegiado=='<?=TipoMembroColegiadoRN::$TMC_TITULAR?>'){
    if (infraTrim(peso)==='' || parseFloat(peso)>1 || parseFloat(peso)<0) {
      alert('Informe o Peso (de 0,0 a 1,0).');
      document.getElementById('txtPeso').focus();
      return false;
    }
    sinHabilitado='S';
    strHabilitado='';
  }else {
    peso='';
    sinHabilitado=document.getElementById('chkSinHabilitado').checked?'S':'N';
    strHabilitado=sinHabilitado==='S'?'Sim':'N�o';
  }

  var idUsuario=document.getElementById('hdnIdUsuario').value;
  var idCargo=document.getElementById('hdnIdCargo').value;
  var txtCargo=document.getElementById('txtCargo').value;
  var txtUsuario=document.getElementById('txtUsuario').value;
  var idUnidade=document.getElementById('selUnidade').value;
  var txtUnidade=document.getElementById('selUnidade').options[document.getElementById('selUnidade').selectedIndex].text;
  if (txtUnidade.indexOf(' - ')!=-1) {
    txtUnidade = txtUnidade.substring(0,txtUnidade.indexOf(' - '));
  }

  var txtTipoMembroColegiado=document.getElementById('selTipoMembroColegiado').options[document.getElementById('selTipoMembroColegiado').selectedIndex].text;

  objTabelaColegiado.adicionar([idUsuario,idUnidade,txtUsuario,txtUnidade,txtCargo,idTipoMembroColegiado,txtTipoMembroColegiado,peso,idCargo,sinHabilitado,strHabilitado]);


  objAutoCompletarCargo.limpar();
  objAutoCompletarUsuario2.limpar();
  document.getElementById('txtUsuario').focus();
  document.getElementById('selUnidade').options.length=0;
  document.getElementById('selUnidade').selectedIndex = -1;
  document.getElementById('selTipoMembroColegiado').value=null;
  document.getElementById('txtPeso').value='';
  ajustarTipo();
  document.getElementById('chkSinHabilitado').checked=false;
}

function adicionarObservador(){
  var txtUsuarioExterno=document.getElementById('txtUsuarioExterno').value;
  if (infraTrim(txtUsuarioExterno)=='') {
    alert('Informe o Usu�rio Observador.');
    document.getElementById('txtUsuarioExterno').focus();
    return false;
  }

  var idUsuario=document.getElementById('hdnIdUsuarioExterno').value;

  var i=txtUsuarioExterno.indexOf('(');
  objTabelaObsevadores.adicionar([idUsuario,txtUsuarioExterno.substring(i+1,txtUsuarioExterno.length-1),txtUsuarioExterno.substring(0,i)]);

  objAutoCompletarUsuarioExterno.limpar();
  document.getElementById('txtUsuarioExterno').focus();
}

function validarCadastro() {

  if (infraTrim(document.getElementById('txtSigla').value)=='') {
    alert('Informe a Sigla.');
    document.getElementById('txtSigla').focus();
    return false;
  }

  if (infraTrim(document.getElementById('txtNome').value)=='') {
    alert('Informe o Nome.');
    document.getElementById('txtNome').focus();
    return false;
  }

  if (!infraSelectSelecionado('selTipoProcedimento')){
    alert('Informe o Tipo de Processo.');
    document.getElementById('selTipoProcedimento').focus();
    return false;
  }

  if (infraTrim(document.getElementById('hdnIdUsuarioSecretario').value)==''){
    alert('Informe o Secret�rio.');
    document.getElementById('txtSecretario').focus();
    return false;
  }

  if (infraTrim(document.getElementById('txtQuorum').value)=='') {
    alert('Informe o Qu�rum M�nimo.');
    document.getElementById('txtQuorum').focus();
    return false;
  }

  if (!infraSelectSelecionado('selAlgoritmo')){
    alert('Informe o Algoritmo de Distribui��o.');
    document.getElementById('selAlgoritmo').focus();
    return false;
  }

  if (objTabelaColegiado.obterItens().length==0){
    alert('Informe a Composi��o.');
    return false;
  }

  if ($("input[name='infraRadioPresidente']:checked").length == 0){
    alert('Selecione o Presidente.');
    return false;
  }


  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

function ajustarTipo(){
  var idTipoMembroColegiado=document.getElementById('selTipoMembroColegiado').value;
  if(idTipoMembroColegiado=='<?=TipoMembroColegiadoRN::$TMC_TITULAR?>'){
    $('#lblPeso').show();
    $('#txtPeso').show();
    $('#divSinHabilitado').hide();

  } else {
    $('#lblPeso').hide();
    $('#txtPeso').hide();
    if (idTipoMembroColegiado!='null'){
      $('#divSinHabilitado').show();
    } else {
      $('#divSinHabilitado').hide();
    }
  }
}

//--><!]]>
</script>

<?
//PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmColegiadoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('25em');
?>

  <label id="lblSigla" for="txtSigla" accesskey="" class="infraLabelObrigatorio">Sigla:</label>
  <input type="text" id="txtSigla" name="txtSigla" class="infraText" value="<?=$objColegiadoDTO->getStrSigla();?>" onkeypress="return infraMascaraTexto(this,event,30);" maxlength="30" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
 
  <label id="lblArtigo" for="txtArtigo" accesskey="" class="infraLabelObrigatorio">Artigo:</label>
  <input type="text" id="txtArtigo" name="txtArtigo" class="infraText" value="<?=$objColegiadoDTO->getStrArtigo();?>" onkeypress="return infraMascaraTexto(this,event,1);" maxlength="1" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblNome" for="txtNome" accesskey="" class="infraLabelObrigatorio">Nome:</label>
  <input type="text" id="txtNome" name="txtNome" class="infraText" value="<?=$objColegiadoDTO->getStrNome();?>" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

	<label id="lblTipoProcedimento" for="selTipoProcedimento" accesskey="" class="infraLabelObrigatorio">Tipo de Processo:</label>
	<select id="selTipoProcedimento" name="selTipoProcedimento" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
		<?=$strItensSelTipoProcedimento?>
	</select>

  <label id="lblUnidadeResponsavel" for="selUnidadeResponsavel" accesskey="" class="infraLabelObrigatorio">Unidade Respons�vel para Distribui��o:</label>
  <select id="selUnidadeResponsavel" name="selUnidadeResponsavel" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelUnidadeResponsavel?>
  </select>

	<label id="lblSecretario" for="txtSecretario" accesskey="" class="infraLabelObrigatorio">Secret�rio:</label>
	<input type="text" id="txtSecretario" name="txtSecretario" class="infraText" value="<?=$objColegiadoDTO->getStrNomeSecretario();?>" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblQuorum" for="txtQuorum" accesskey="" class="infraLabelObrigatorio">Qu�rum M�nimo:</label>
  <input type="text" id="txtQuorum" name="txtQuorum" class="infraText" value="<?=$objColegiadoDTO->getNumQuorumMinimo();?>" onkeypress="return infraMascaraNumero(this,event,2);" maxlength="2" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblAlgoritmo" for="selAlgoritmo" accesskey="" class="infraLabelObrigatorio">Algoritmo de Distribui��o:</label>
  <select id="selAlgoritmo" name="selAlgoritmo" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelAlgoritmo?>
  </select>

  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  ?>

  <?
  if ($_GET['acao']!='colegiado_consultar'){
    PaginaSEI::getInstance()->abrirAreaDados('18em');?>
  <label id="lblComposicao" accesskey="" class="infraLabelTitulo">&nbsp;&nbsp;Composi��o</label>

  <label id="lblUsuario" for="txtUsuario" accesskey="" class="infraLabelOpcional">Usu�rio:</label>
  <input type="text" id="txtUsuario" name="txtUsuario" class="infraText" value="" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblUnidade" for="selUnidade" accesskey="" class="infraLabelOpcional">Unidade:</label>
  <select id="selUnidade" name="selUnidade" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
  </select>

  <label id="lblCargo" for="txtCargo" accesskey="" class="infraLabelOpcional">Cargo:</label>
  <input type="text" id="txtCargo" name="txtCargo" class="infraText" value="" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <label id="lblTipoMembroColegiado" for="selTipoMembroColegiado" accesskey="" class="infraLabelOpcional">Tipo:</label>
  <select id="selTipoMembroColegiado" name="selTipoMembroColegiado" class="infraSelect" onchange="ajustarTipo();" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelTipo;?>
  </select>

  <label id="lblPeso" for="txtPeso" accesskey="P" class="infraLabelOpcional" style="display: none;">Peso:</label>
  <input type="text" id="txtPeso" name="txtPeso" class="infraText" value="" onkeypress="return infraMascaraDecimais(this,'.',',',event,1,2,false);" style="display: none;" maxlength="3" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

    <div id="divSinHabilitado" class="infraDivCheckbox" style="display: none;">
      <input type="checkbox" id="chkSinHabilitado" name="chkSinHabilitado" class="infraCheckbox"  tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>"/>
      <label id="lblSinHabilitado" for="chkSinHabilitado" accesskey="" class="infraLabelCheckbox">Acesso habilitado</label>
    </div>

  <input type="button" id="btnAdicionar" onclick="adicionar();" name="btnAdicionar" value="Atualizar" class="infraButton" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
<?
    PaginaSEI::getInstance()->fecharAreaDados();
  }
?>

<div id="divTabelaComposicaoColegiado" class="infraAreaTabela">
  <table id="tblColegiado" class="infraTable">
    <caption class="infraCaption"><?=PaginaSEI::getInstance()->gerarCaptionTabela("Composi��o do Colegiado",0)?></caption>
		<tr>
      <th class="infraTh" style="width:5%;">Presidente</th>
		  <th style="display:none;">ID</th>
		  <th style="display:none;">IdUnidade</th>
			<th class="infraTh" style="width:35%;">Membro</th>
      <th class="infraTh" align="center" style="width:10%;">Unidade</th>
      <th class="infraTh">Cargo</th>
      <th style="display:none;">IdTipoMembroColegiado</th>
      <th class="infraTh" align="center" style="width:10%;">Tipo</th>

      <th class="infraTh" align="center" style="width:5%;">Peso</th>
      <th style="display:none;">IdCargo</th>
      <th style="display:none;">SinHabilitado</th>
      <th class="infraTh" align="center" style="width:5%;">Habilitado</th>
			<? if($_GET['acao']!='colegiado_consultar') {?>
			<th class="infraTh" style="width:10%;">A��es</th>
      <? }?>
		</tr>
  </table>
  <input type="hidden" id="hdnIdUnidade" name="hdnIdUnidade" value="" />
	<input type="hidden" id="hdnIdUsuarioSecretario" name="hdnIdUsuarioSecretario" value="<?=$objColegiadoDTO->getNumIdUsuarioSecretario()?>" />
  <input type="hidden" id="hdnNomeUsuarioSecretario" name="hdnNomeUsuarioSecretario" value="<?=$objColegiadoDTO->getStrNomeSecretario()?>" />
  <input type="hidden" id="hdnIdUsuario" name="hdnIdUsuario" value="" />
  <input type="hidden" id="hdnIdCargo" name="hdnIdCargo" value="" />
  <input type="hidden" id="hdnIdUsuarioPresidente" name="hdnIdUsuarioPresidente" value="<?=$objColegiadoDTO->getNumIdUsuarioPresidente()?>" />
  <input type="hidden" id="hdnIdColegiado" name="hdnIdColegiado" value="<?=$objColegiadoDTO->getNumIdColegiado()?>" />
  <input type="hidden" id="hdnColegiado" name="hdnColegiado" value="<?=$strComposicaoColegiado?>" />
</div>
  <?
  if ($_GET['acao']!='colegiado_consultar'){
    PaginaSEI::getInstance()->abrirAreaDados('9em');?>
    <label id="lblObservadores" accesskey="" class="infraLabelTitulo">&nbsp;&nbsp;Observadores Externos</label>

    <label id="lblUsuarioExterno" for="txtUsuarioExterno" accesskey="" class="infraLabelOpcional">Usu�rio:</label>
    <input type="text" id="txtUsuarioExterno" name="txtUsuarioExterno" class="infraText" value="" onkeypress="return infraMascaraTexto(this,event,100);" maxlength="100" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

    <input type="button" id="btnAdicionarObservador" onclick="adicionarObservador();" name="btnAdicionarObservador" value="Adicionar" class="infraButton" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
  }
  if (!$bolConsultaHistorico) {
  ?>
  <div id="divTabelaObservadores" class="infraAreaTabela">
    <table id="tblObservadores" class="infraTable">
      <caption class="infraCaption"><?= PaginaSEI::getInstance()->gerarCaptionTabela("Observadores", 0) ?></caption>
      <tr>
        <th style="display:none;">ID</th>
        <th class="infraTh" style="width:40%;">Sigla</th>
        <th class="infraTh">Nome</th>
        <? if ($_GET['acao'] != 'colegiado_consultar') { ?>
          <th class="infraTh" style="width:10%;">A��es</th>
        <? } ?>
      </tr>
    </table>
    <input type="hidden" id="hdnIdUsuarioExterno" value=""/>
    <input type="hidden" id="hdnObservadores" name="hdnObservadores" value="<?= $strObservadoresColegiado ?>"/>
  </div>
  <br/>
  <?
  }
  PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>