<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->prepararSelecao('sessao_julgamento_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->salvarCamposPost(array('selStaSessao','selColegiado'));

  $numIdColegiadoVersao=null;
  $strParametros='';

  switch($_GET['acao']){
    case 'sessao_julgamento_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjSessaoJulgamentoDTO = array();
        $objSessaoJulgamentoDTOBase = new SessaoJulgamentoDTO();
        $objSessaoJulgamentoDTOBase->setNumIdSessaoJulgamento(null);
        foreach ($arrStrIds as $numIdSessaoJulgamento) {
          $objSessaoJulgamentoDTO = clone $objSessaoJulgamentoDTOBase;
          $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);
          $arrObjSessaoJulgamentoDTO[] = $objSessaoJulgamentoDTO;
        }
        $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
        $objSessaoJulgamentoRN->excluir($arrObjSessaoJulgamentoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;


    case 'sessao_julgamento_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Sess�o de Julgamento','Selecionar Sess�es de Julgamento');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='sessao_julgamento_cadastrar' && isset($_GET['id_sessao_julgamento'])) {
        PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_sessao_julgamento']);
      }
      break;

    case 'sessao_julgamento_listar':
      $strTitulo = 'Sess�es de Julgamento';
      if(isset($_GET['id_colegiado_versao'])){
        $numIdColegiadoVersao=$_GET['id_colegiado_versao'];
        $strParametros.='&id_colegiado_versao='.$numIdColegiadoVersao;
        PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();
  $arrComandos[] = '<button type="submit" accesskey="P" id="sbmPesquisar" name="sbmPesquisar" value="Pesquisar" class="infraButton"><span class="infraTeclaAtalho">P</span>esquisar</button>';

  if ($_GET['acao'] == 'sessao_julgamento_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_cadastrar');
  if ($bolAcaoCadastrar){
    $arrComandos[] = '<button type="button" accesskey="N" id="btnNova" value="Nova" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ova</button>';
  }

  $numIdColegiado = PaginaSEI::getInstance()->recuperarCampo('selColegiado');

  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
  $objSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
  $objSessaoJulgamentoDTO->retDthSessao();
  $objSessaoJulgamentoDTO->retStrStaSituacao();
  $objSessaoJulgamentoDTO->retStrDescricaoTipoSessao();
  $objSessaoJulgamentoDTO->retDthInicio();
  $objSessaoJulgamentoDTO->retDthFim();
  $objSessaoJulgamentoDTO->retStrSiglaColegiado();
  $objSessaoJulgamentoDTO->retStrNomeColegiado();
  $objSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
  $objSessaoJulgamentoDTO->retDblIdProcedimento();
  $objSessaoJulgamentoDTO->retDblIdDocumentoPauta();
  $objSessaoJulgamentoDTO->retStrProtocoloFormatadoDocumentoPauta();
  if($numIdColegiadoVersao==null){
    $objSessaoJulgamentoDTO->setNumIdUnidadeResponsavelColegiado(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    if ($numIdColegiado!==''){
      $objSessaoJulgamentoDTO->setNumIdColegiado($numIdColegiado);
    }

  } else {
    $objSessaoJulgamentoDTO->setNumIdColegiadoVersao($numIdColegiadoVersao);
  }



  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $arrObjSituacaoSessaoDTO = $objSessaoJulgamentoRN->listarValoresSituacaoSessao();
  $arrObjSituacaoSessao=InfraArray::indexarArrInfraDTO($arrObjSituacaoSessaoDTO,'StaSituacao');


  if($numIdColegiadoVersao==null) {
    $arrStaSessao = PaginaSEI::getInstance()->recuperarCampo('selStaSessao', array(
        SessaoJulgamentoRN::$ES_PREVISTA,
        SessaoJulgamentoRN::$ES_ABERTA,
        SessaoJulgamentoRN::$ES_PAUTA_ABERTA,
        SessaoJulgamentoRN::$ES_PAUTA_FECHADA,
        SessaoJulgamentoRN::$ES_ENCERRADA));
  } else {
    $arrStaSessao = PaginaSEI::getInstance()->recuperarCampo('selStaSessao', array(
        SessaoJulgamentoRN::$ES_SUSPENSA,
        SessaoJulgamentoRN::$ES_PREVISTA,
        SessaoJulgamentoRN::$ES_CANCELADA,
        SessaoJulgamentoRN::$ES_ABERTA,
        SessaoJulgamentoRN::$ES_FINALIZADA,
        SessaoJulgamentoRN::$ES_PAUTA_ABERTA,
        SessaoJulgamentoRN::$ES_PAUTA_FECHADA,
        SessaoJulgamentoRN::$ES_ENCERRADA));
  }
  if(InfraArray::contar($arrStaSessao)>0) {
    $objSessaoJulgamentoDTO->setStrStaSituacao($arrStaSessao, InfraDTO::$OPER_IN);
  } else {
    $objSessaoJulgamentoDTO->setStrStaSituacao(array(SessaoJulgamentoRN::$ES_FINALIZADA,SessaoJulgamentoRN::$ES_CANCELADA),InfraDTO::$OPER_NOT_IN);
  }

  PaginaSEI::getInstance()->prepararOrdenacao($objSessaoJulgamentoDTO, 'Sessao', InfraDTO::$TIPO_ORDENACAO_DESC);

  PaginaSEI::getInstance()->prepararPaginacao($objSessaoJulgamentoDTO);

  $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
  $arrObjSessaoJulgamentoDTO = $objSessaoJulgamentoRN->listar($objSessaoJulgamentoDTO);

  PaginaSEI::getInstance()->processarPaginacao($objSessaoJulgamentoDTO);
  $numRegistros = InfraArray::contar($arrObjSessaoJulgamentoDTO);

  if ($numRegistros > 0){

    $bolCheck = false;

    if ($_GET['acao']=='sessao_julgamento_selecionar'){
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_alterar');
      $bolAcaoImprimir = false;
      $bolAcaoExcluir = false;
      $bolAcaoAbrir = false;
      $bolCheck = true;
    }else{
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_alterar');
      $bolAcaoImprimir = true;
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_excluir');
    }

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_excluir&acao_origem='.$_GET['acao']);
    }

    $strResultado = '';

    $strSumarioTabela = 'Tabela de Sess�es de Julgamento.';
    $strCaptionTabela = 'Sess�es de Julgamento';

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh" width="15%">'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'Data','Sessao',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" >'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'Colegiado','SiglaColegiado',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" >'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'Tipo','DescricaoTipoSessao',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" >'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'In�cio','Inicio',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" >'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'Fim','Fim',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" >'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'Processo','ProtocoloFormatadoProtocolo',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" >'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'Pauta','ProtocoloFormatadoDocumentoPauta',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    $strResultado .= '<th class="infraTh" width="15%">'.PaginaSEI::getInstance()->getThOrdenacao($objSessaoJulgamentoDTO,'Situa��o','StaSituacao',$arrObjSessaoJulgamentoDTO).'</th>'."\n";
    if($numIdColegiadoVersao==null) {
      $strResultado .= '<th class="infraTh" width="15%">A��es</th>' . "\n";
    }
    $strResultado .= '</tr>'."\n";
    $strCssTr='';
    for($i = 0;$i < $numRegistros; $i++){

      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      if (!in_array($arrObjSessaoJulgamentoDTO[$i]->getStrStaSituacao(),array(SessaoJulgamentoRN::$ES_FINALIZADA,SessaoJulgamentoRN::$ES_SUSPENSA,SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_ENCERRADA)) &&
          InfraData::compararDatasSimples($arrObjSessaoJulgamentoDTO[$i]->getDthSessao(),InfraData::getStrDataAtual())>0) {
        $strResultado .='<tr class="trVermelha">';
      } else if($arrObjSessaoJulgamentoDTO[$i]->getStrStaSituacao()==SessaoJulgamentoRN::$ES_ENCERRADA){
        $strResultado .='<tr class="trAzul">';
      } else {
        $strResultado .= $strCssTr;
      }

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjSessaoJulgamentoDTO[$i]->getNumIdSessaoJulgamento(),$arrObjSessaoJulgamentoDTO[$i]->getNumIdSessaoJulgamento()).'</td>';
      }
      $strResultado .= '<td align="center">'.substr($arrObjSessaoJulgamentoDTO[$i]->getDthSessao(),0,-3).'</td>';
      $strResultado .= '<td align="center"><a alt="'.PaginaSEI::tratarHTML($arrObjSessaoJulgamentoDTO[$i]->getStrNomeColegiado()).'" title="'.PaginaSEI::tratarHTML($arrObjSessaoJulgamentoDTO[$i]->getStrNomeColegiado()).'" class="ancoraSigla">'.PaginaSEI::tratarHTML($arrObjSessaoJulgamentoDTO[$i]->getStrSiglaColegiado()).'</a></td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML($arrObjSessaoJulgamentoDTO[$i]->getStrDescricaoTipoSessao()).'</td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML(substr($arrObjSessaoJulgamentoDTO[$i]->getDthInicio(),0,16)).'</td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML(substr($arrObjSessaoJulgamentoDTO[$i]->getDthFim(),0,16)).'</td>';
      $strResultado .= '<td align="center">';
      $dblIdProcedimento=$arrObjSessaoJulgamentoDTO[$i]->getDblIdProcedimento();
      if($dblIdProcedimento!=null){
        $strProtocoloFormatado=PaginaSEI::tratarHTML($arrObjSessaoJulgamentoDTO[$i]->getStrProtocoloFormatadoProtocolo());
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_procedimento='.$dblIdProcedimento).'" target="_blank" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'" alt="'.$strProtocoloFormatado.'" title="'.$strProtocoloFormatado.'" class="protocoloNormal"  style="font-size:1em !important">'.$strProtocoloFormatado.'</a>';
      }

      $strResultado .= '</td>';
      $strResultado .= '<td align="center">';
      $dblIdDocumento=$arrObjSessaoJulgamentoDTO[$i]->getDblIdDocumentoPauta();
      if($dblIdDocumento!=null){
        $strProtocoloDocumento=PaginaSEI::tratarHTML($arrObjSessaoJulgamentoDTO[$i]->getStrProtocoloFormatadoDocumentoPauta());
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_procedimento='.$dblIdProcedimento.'&id_documento='.$dblIdDocumento).'" target="_blank" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'" alt="'.$strProtocoloDocumento.'" title="'.$strProtocoloDocumento.'" class="protocoloNormal"  style="font-size:1em !important">'.$strProtocoloDocumento.'</a>';
      }

      $strResultado .= '</td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML($arrObjSituacaoSessao[$arrObjSessaoJulgamentoDTO[$i]->getStrStaSituacao()]->getStrDescricao()).'</td>';
      if($numIdColegiadoVersao==null) {
        $strResultado .= '<td align="center">';

        $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i, $arrObjSessaoJulgamentoDTO[$i]->getNumIdSessaoJulgamento());
        $staSessao = $arrObjSessaoJulgamentoDTO[$i]->getStrStaSituacao();

        if ($bolAcaoConsultar) {
          $strResultado .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_consultar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_sessao_julgamento=' . $arrObjSessaoJulgamentoDTO[$i]->getNumIdSessaoJulgamento()) . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="' . PaginaSEI::getInstance()->getIconeConsultar() . '" title="Visualizar Sess�o de Julgamento" alt="Visualizar Sess�o de Julgamento" class="infraImg" /></a>&nbsp;';
        }

        if ($bolAcaoAlterar && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PREVISTA, SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA))) {
          $strResultado .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_alterar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_sessao_julgamento=' . $arrObjSessaoJulgamentoDTO[$i]->getNumIdSessaoJulgamento()) . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="' . PaginaSEI::getInstance()->getIconeAlterar() . '" title="Alterar Sess�o de Julgamento" alt="Alterar Sess�o de Julgamento" class="infraImg" /></a>&nbsp;';
        }

        if ($bolAcaoExcluir) {
          $strId = $arrObjSessaoJulgamentoDTO[$i]->getNumIdSessaoJulgamento();
          $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($strId);
        }

        if ($bolAcaoExcluir && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PREVISTA))) {
          $strResultado .= '<a href="' . PaginaSEI::getInstance()->montarAncora($strId) . '" onclick="acaoExcluir(\'' . $strId . '\',\'' . substr($arrObjSessaoJulgamentoDTO[$i]->getDthSessao(), 0, -3) . '\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="' . PaginaSEI::getInstance()->getIconeExcluir() . '" title="Excluir Sess�o de Julgamento" alt="Excluir Sess�o de Julgamento" class="infraImg" /></a>&nbsp;';
        }
        $strResultado .= '</td>';
      }
      $strResultado .= '</tr>'."\n";
    }
    $strResultado .= '</table>';
  }
  if ($_GET['acao'] == 'sessao_julgamento_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }

  $strOptionsStaSessao='';
  $numStaSessao = InfraArray::contar($arrObjSituacaoSessaoDTO);
  for($i=0;$i<$numStaSessao;$i++){

    $strOptionsStaSessao.='<option value="'.$arrObjSituacaoSessaoDTO[$i]->getStrStaSituacao().'"';
    if (in_array($arrObjSituacaoSessaoDTO[$i]->getStrStaSituacao(),$arrStaSessao)) {
      $strOptionsStaSessao.=' selected="selected"';
    }
    $strOptionsStaSessao.='>'.$arrObjSituacaoSessaoDTO[$i]->getStrDescricao().'</option>';
  }

  if($numIdColegiadoVersao==null) {
    $strItensSelColegiado = ColegiadoINT::montarSelectNomeAdministrados('', 'Todos', $numIdColegiado);
  } else {
    $strItensSelColegiado=ColegiadoINT::montarSelectNomeVersao($numIdColegiadoVersao);
  }
}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
#lblColegiado {position:absolute;left:0%;top:10%;width:25%;}
#selColegiado {position:absolute;left:10%;top:0%;width:60%;}

  #lblStaSessao {position:absolute;left:0%;top:0%;width:9%;}
  #selStaSessao, .multipleSelect {position:absolute;left:10%;top:0%;width:40%;}

tr.trAzul{
  background-color: #9cd0f5;
}
<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
$(document).ready(function() {
  $("#selStaSessao").show().multipleSelect({
    filter: false,
    selectAllText: 'Selecionar todas',
    allSelected: 'Todas selecionadas',
    countSelected: '# de % selecionadas',
    minimumCountSelected: 3,
    selectAll: true

  });
});


  function inicializar(){
  if ('<?=$_GET['acao']?>'=='sessao_julgamento_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('sbmPesquisar').focus();
  }
  infraEfeitoTabelas();
}

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclus�o da Sess�o de Julgamento de \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmSessaoJulgamentoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmSessaoJulgamentoLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhuma Sess�o de Julgamento selecionada.');
    return;
  }
  if (confirm("Confirma exclus�o das Sess�es de Julgamento selecionadas?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmSessaoJulgamentoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmSessaoJulgamentoLista').submit();
  }
}
<? } ?>
function onSubmitForm() {

  var a=$("#selStaSessao").multipleSelect("getSelects");

  if (a.length==0) {
  alert('Nenhuma Situa��o selecionada.');
  return false;
  }
  infraExibirAviso(true);
  return true;
}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmSessaoJulgamentoLista" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->abrirAreaDados('3em');
  ?>
  <label id="lblColegiado" for="selColegiado" accesskey="o" class="infraLabelOpcional">C<span class="infraTeclaAtalho">o</span>legiado:</label>
  <select id="selColegiado" name="selColegiado" onchange="this.form.submit();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
  <?=$strItensSelColegiado?>
  </select>

  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->abrirAreaDados('4em','style="overflow:visible;"');
  ?>

  <label id="lblStaSessao" for="selStaSessao" accesskey="" class="infraLabelObrigatorio">Situa��es:</label>
  <select multiple id="selStaSessao" name="selStaSessao[]" class="infraSelect multipleSelect" style="display: none" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strOptionsStaSessao?>
  </select>

  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>