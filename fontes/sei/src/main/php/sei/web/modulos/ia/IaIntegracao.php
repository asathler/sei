<?php

class IaIntegracao extends SeiIntegracao
{

    public function __construct()
    {

    }

    public function getNome()
    {
        return 'SEI Ia';
    }

    public function getVersao()
    {
        return '0.1';
    }

    public function getInstituicao()
    {
        return 'Anatel - Agência Nacional de Telecomunicações';
    }

    public function montarBotaoProcesso(ProcedimentoAPI $objProcedimentoAPI)
    {

        $arrBotoes = array();
        $bolProcedimentoAberto = $objProcedimentoAPI->getSinAberto() == 'S';
        $bolPermissaoAcesso = $objProcedimentoAPI->getCodigoAcesso() > 0;
        $bolAcaoAvaliacaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('md_ia_avaliacao_cadastrar');

        $mdIaAvaliacaoRN = new MdIaAvaliacaoRN();

        $bolProcessoIndexado = $mdIaAvaliacaoRN->consultarIndexacaoProcesso($objProcedimentoAPI->getIdProcedimento());
        if ($bolAcaoAvaliacaoCadastrar && $bolPermissaoAcesso && $bolProcessoIndexado != "false") {

            $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=md_ia_avaliacao_cadastrar&arvore=1&id_procedimento=' . $_GET['id_procedimento']);

            $imgIcone = "modulos/ia/imagens/md_ia_icone.svg?11";
            $title = "Avaliar Similaridade";

            $strBotaoListarExpedicao = '<a href="' . $strLink . '"class="botaoSEI">';
            $strBotaoListarExpedicao .= '<img class="infraCorBarraSistema" src="' . $imgIcone . '" alt="' . $title . '" title="' . $title . '">';
            $strBotaoListarExpedicao .= '</a>';

            $arrBotoes[] = $strBotaoListarExpedicao;
        }

        return $arrBotoes;
    }

    public function processarControlador($strAcao)
    {
        switch ($strAcao) {
            case 'md_ia_avaliacao_cadastrar' :
                require_once dirname(__FILE__) . '/md_ia_avaliacao_cadastro.php';
                return true;
        }

        return false;
    }
}