<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 04/10/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class SustentacaoOralRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdItemSessaoJulgamento(SustentacaoOralDTO $objSustentacaoOralDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSustentacaoOralDTO->getNumIdItemSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Item da Sess�o n�o informado.');
    }
  }

  private function validarNumIdParteProcedimento(SustentacaoOralDTO $objSustentacaoOralDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objSustentacaoOralDTO->getNumIdParteProcedimento())){
      $objInfraException->adicionarValidacao('Parte n�o informada.');
    }
  }

  protected function cadastrarControlado(SustentacaoOralDTO $objSustentacaoOralDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sustentacao_oral_cadastrar',__METHOD__,$objSustentacaoOralDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdItemSessaoJulgamento($objSustentacaoOralDTO, $objInfraException);
      $this->validarNumIdParteProcedimento($objSustentacaoOralDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objParteProcedimentoDTO=new ParteProcedimentoDTO();
      $objParteProcedimentoRN=new ParteProcedimentoRN();
      $objParteProcedimentoDTO->setNumIdParteProcedimento($objSustentacaoOralDTO->getNumIdParteProcedimento());
      $objParteProcedimentoDTO->retTodos();
      $objParteProcedimentoDTO=$objParteProcedimentoRN->consultar($objParteProcedimentoDTO);
      if($objParteProcedimentoDTO==null){
        $objInfraException->lancarValidacao('Parte n�o encontrada.');
      }

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objSustentacaoOralDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTO->retStrStaSituacao();
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      if($objItemSessaoJulgamentoDTO==null){
        $objInfraException->lancarValidacao('Item da sess�o n�o encontrado.');
      }
      //validar se processo do item=processo parte
      if($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao()!=$objParteProcedimentoDTO->getDblIdProcedimento()){
        $objInfraException->lancarValidacao('Parte interessada de outro processo.');
      }
      //validar situacao sessao/item
      if($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA){
        $objInfraException->lancarValidacao('Sess�o n�o est� aberta.');
      }
      //validar qualificacao=advogado
//      if($objParteProcedimentoDTO->getNumIdQualificacaoParte()!=QualificacaoParteRN::$TP_ADVOGADO){
//        $objInfraException->lancarValidacao('Somente Advogados podem fazer sustenta��o oral.');
//      }

      $objSustentacaoOralDTOBanco=new SustentacaoOralDTO();
      $objSustentacaoOralDTOBanco->setNumIdItemSessaoJulgamento($objSustentacaoOralDTO->getNumIdItemSessaoJulgamento());
      $objSustentacaoOralDTOBanco->setNumIdContato($objParteProcedimentoDTO->getNumIdContato());
      if($this->contar($objSustentacaoOralDTOBanco)!=0){
        $objInfraException->lancarValidacao('Sustenta��o oral j� cadastrada para esta parte.');
      }
      $objSustentacaoOralDTOBanco->setNumIdQualificacaoParte($objParteProcedimentoDTO->getNumIdQualificacaoParte());

      $objSustentacaoOralBD = new SustentacaoOralBD($this->getObjInfraIBanco());
      $ret = $objSustentacaoOralBD->cadastrar($objSustentacaoOralDTOBanco);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Sustenta��o Oral.',$e);
    }
  }

  protected function alterarControlado(SustentacaoOralDTO $objSustentacaoOralDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('sustentacao_oral_alterar',__METHOD__,$objSustentacaoOralDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objSustentacaoOralDTO->isSetNumIdItemSessaoJulgamento()){
        $this->validarNumIdItemSessaoJulgamento($objSustentacaoOralDTO, $objInfraException);
      }
      if ($objSustentacaoOralDTO->isSetNumIdQualificacaoParte()){
        $this->validarNumIdQualificacaoParte($objSustentacaoOralDTO, $objInfraException);
      }
      if ($objSustentacaoOralDTO->isSetNumIdContato()){
        $this->validarNumIdContato($objSustentacaoOralDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objSustentacaoOralBD = new SustentacaoOralBD($this->getObjInfraIBanco());
      $objSustentacaoOralBD->alterar($objSustentacaoOralDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Sustenta��o Oral.',$e);
    }
  }

  protected function excluirControlado($arrObjSustentacaoOralDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sustentacao_oral_excluir',__METHOD__,$arrObjSustentacaoOralDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSustentacaoOralBD = new SustentacaoOralBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjSustentacaoOralDTO);$i++){
        $objSustentacaoOralBD->excluir($arrObjSustentacaoOralDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Sustenta��o Oral.',$e);
    }
  }

  protected function consultarConectado(SustentacaoOralDTO $objSustentacaoOralDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sustentacao_oral_consultar',__METHOD__,$objSustentacaoOralDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSustentacaoOralBD = new SustentacaoOralBD($this->getObjInfraIBanco());
      $ret = $objSustentacaoOralBD->consultar($objSustentacaoOralDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Sustenta��o Oral.',$e);
    }
  }

  protected function listarConectado(SustentacaoOralDTO $objSustentacaoOralDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sustentacao_oral_listar',__METHOD__,$objSustentacaoOralDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSustentacaoOralBD = new SustentacaoOralBD($this->getObjInfraIBanco());
      $ret = $objSustentacaoOralBD->listar($objSustentacaoOralDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Sustenta��es Orais.',$e);
    }
  }

  protected function contarConectado(SustentacaoOralDTO $objSustentacaoOralDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('sustentacao_oral_listar',__METHOD__,$objSustentacaoOralDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objSustentacaoOralBD = new SustentacaoOralBD($this->getObjInfraIBanco());
      $ret = $objSustentacaoOralBD->contar($objSustentacaoOralDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Sustenta��es Orais.',$e);
    }
  }
}
?>