<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class JulgamentoParteDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'julgamento_parte';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdJulgamentoParte','id_julgamento_parte');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdItemSessaoJulgamento','id_item_sessao_julgamento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'Ordem','ordem');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_STR,'Descricao','descricao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuarioDesempate','id_usuario_desempate');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdDocumentoItem','id_documento','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdSessaoJulgamento','id_sessao_julgamento','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdDistribuicaoItemSessaoJulgamento','id_distribuicao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaSituacaoItem','sta_situacao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SinManualItemSessaoJulgamento','sin_manual','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUsuarioSessaoItem','id_usuario_sessao','item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'StaSituacaoSessaoJulgamento','sta_situacao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DTH,'SessaoSessaoJulgamento','dth_sessao','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUsuarioRelatorDistribuicao','id_usuario_relator','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdUsuarioRelatorAcordaoDistribuicao','id_usuario_relator_acordao','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM,'IdColegiadoSessaoJulgamento','id_colegiado','sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_DBL,'IdProcedimentoDistribuicao','id_procedimento','distribuicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdTipoSessaoJulgamento', 'id_tipo_sessao', 'sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'SinVirtualTipoSessao','sin_virtual','tipo_sessao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_NUM, 'IdSessaoBlocoItem', 'id_sessao_bloco', 'item_sessao_julgamento');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR, 'StaTipoItemSessaoBloco', 'sta_tipo_item', 'sessao_bloco');

    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'SinForcarExclusao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjVotoParteDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'ObjResumoVotacaoDTO');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'PossuiEmpate');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'FaltamVotos');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_BOL,'PedidoVista');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_ARR,'UsuariosEmpate');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'UsuarioVencedor');

    $this->configurarFK('IdItemSessaoJulgamento','item_sessao_julgamento','id_item_sessao_julgamento');
    $this->configurarFK('IdDistribuicaoItemSessaoJulgamento','distribuicao','id_distribuicao');
    $this->configurarFK('IdSessaoJulgamento','sessao_julgamento','id_sessao_julgamento');
    $this->configurarFK('IdTipoSessaoJulgamento', 'tipo_sessao', 'id_tipo_sessao');
    $this->configurarFK('IdSessaoBlocoItem', 'sessao_bloco', 'id_sessao_bloco');

    $this->configurarPK('IdJulgamentoParte',InfraDTO::$TIPO_PK_NATIVA);

  }
}
?>