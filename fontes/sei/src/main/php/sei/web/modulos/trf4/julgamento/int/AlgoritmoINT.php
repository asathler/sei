<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 10/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class AlgoritmoINT extends InfraINT {

  public static function montarSelectIdUsuario($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdUsuario='', $numIdOrigem='', $numIdColegiado=''){
    $objAlgoritmoDTO = new AlgoritmoDTO();
    $objAlgoritmoDTO->retNumIdAlgoritmo();
    $objAlgoritmoDTO->retNumIdUsuario();

    if ($numIdUsuario!==''){
      $objAlgoritmoDTO->setNumIdUsuario($numIdUsuario);
    }

    if ($numIdOrigem!==''){
      $objAlgoritmoDTO->setNumIdOrigem($numIdOrigem);
    }

    if ($numIdColegiado!==''){
      $objAlgoritmoDTO->setNumIdColegiado($numIdColegiado);
    }

    $objAlgoritmoDTO->setOrdNumIdUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objAlgoritmoRN = new AlgoritmoRN();
    $arrObjAlgoritmoDTO = $objAlgoritmoRN->listar($objAlgoritmoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjAlgoritmoDTO, 'IdAlgoritmo', 'IdUsuario');
  }

  public static function montarSelectStaAlgoritmo($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objAlgoritmoRN = new AlgoritmoRN();

    $arrObjTipoDTO = $objAlgoritmoRN->listarValoresAlgoritmo();

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjTipoDTO, 'StaTipo', 'Descricao');

  }
}
?>