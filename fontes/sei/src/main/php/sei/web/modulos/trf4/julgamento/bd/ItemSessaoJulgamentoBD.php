<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 11/11/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ItemSessaoJulgamentoBD extends InfraBD {

  public function __construct(InfraIBanco $objInfraIBanco){
  	 parent::__construct($objInfraIBanco);
  }

  public function obterItensJulgamentoFracionado($numIdSessaoJulgamento): array
  {
    try{
      $objInfraIBanco=$this->getObjInfraIBanco();
      $sql = 'select j.id_item_sessao_julgamento from julgamento_parte j join item_sessao_julgamento i on j.id_item_sessao_julgamento=i.id_item_sessao_julgamento 
where i.id_sessao_julgamento='.$objInfraIBanco->formatarGravacaoNum($numIdSessaoJulgamento).' group by id_item_sessao_julgamento having count(id_julgamento_parte)>1';
      $rs = $objInfraIBanco->consultarSql($sql);

      if(InfraArray::contar($rs)){
        $arrObjItemSessaoJulgamentoDTO=array();
        foreach ($rs as $item) {
          $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
          $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($item['id_item_sessao_julgamento']);
          $arrObjItemSessaoJulgamentoDTO[]=$objItemSessaoJulgamentoDTO;
        }
        return $arrObjItemSessaoJulgamentoDTO;
      }
      return [];

    }catch(Exception $e){
      throw new InfraException('Erro itens com julgamento fracionado.',$e);
    }
  }
}
?>