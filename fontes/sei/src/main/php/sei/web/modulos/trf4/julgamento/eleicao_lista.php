<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4ª REGIÃO
*
* 15/05/2020 - criado por mga
*
* Versão do Gerador de Código: 1.42.0
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento'));

  PaginaSEI::getInstance()->prepararSelecao('eleicao_selecionar');

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  switch($_GET['acao']){
    case 'eleicao_excluir':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjEleicaoDTO = array();
        for ($i=0;$i<count($arrStrIds);$i++){
          $objEleicaoDTO = new EleicaoDTO();
          $objEleicaoDTO->setNumIdEleicao($arrStrIds[$i]);
          $arrObjEleicaoDTO[] = $objEleicaoDTO;
        }
        $objEleicaoRN = new EleicaoRN();
        $objEleicaoRN->excluir($arrObjEleicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

/* 
    case 'eleicao_desativar':
      try{
        $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
        $arrObjEleicaoDTO = array();
        for ($i=0;$i<count($arrStrIds);$i++){
          $objEleicaoDTO = new EleicaoDTO();
          $objEleicaoDTO->setNumIdEleicao($arrStrIds[$i]);
          $arrObjEleicaoDTO[] = $objEleicaoDTO;
        }
        $objEleicaoRN = new EleicaoRN();
        $objEleicaoRN->desativar($arrObjEleicaoDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
      die;

    case 'eleicao_reativar':
      $strTitulo = 'Reativar Escrutínios Eletrônicos';
      if ($_GET['acao_confirmada']=='sim'){
        try{
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjEleicaoDTO = array();
          for ($i=0;$i<count($arrStrIds);$i++){
            $objEleicaoDTO = new EleicaoDTO();
            $objEleicaoDTO->setNumIdEleicao($arrStrIds[$i]);
            $arrObjEleicaoDTO[] = $objEleicaoDTO;
          }
          $objEleicaoRN = new EleicaoRN();
          $objEleicaoRN->reativar($arrObjEleicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Operação realizada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        } 
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao']));
        die;
      } 
      break;

 */
    case 'eleicao_selecionar':
      $strTitulo = PaginaSEI::getInstance()->getTituloSelecao('Selecionar Escrutínio Eletrônico','Selecionar Escrutínios Eletrônicos');

      //Se cadastrou alguem
      if ($_GET['acao_origem']=='eleicao_cadastrar'){
        if (isset($_GET['id_eleicao'])){
          PaginaSEI::getInstance()->adicionarSelecionado($_GET['id_eleicao']);
        }
      }
      break;

    case 'eleicao_listar':
      $strTitulo = 'Escrutínios Eletrônicos';
      break;

    default:
      throw new InfraException("Ação '".$_GET['acao']."' não reconhecida.");
  }

  //recuperar sessao, distribuicao e colegiado
  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);

  $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
  $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

  $bolPermiteEleicoes = true;
  if (in_array($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento(), array(SessaoJulgamentoRN::$ES_CANCELADA,SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_FINALIZADA))){
    $bolPermiteEleicoes = false;
  }

  $arrComandos = array();
  if ($_GET['acao'] == 'eleicao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="T" id="btnTransportarSelecao" value="Transportar" onclick="infraTransportarSelecao();" class="infraButton"><span class="infraTeclaAtalho">T</span>ransportar</button>';
  }

  if ($bolPermiteEleicoes) {
    $arrComandos[] = '<button type="submit" id="sbmAtualizar" name="sbmAtualizar" value="Atualizar" class="infraButton">Atualizar</button>';

    $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('eleicao_cadastrar');
    if ($bolAcaoCadastrar) {
      $arrComandos[] = '<button type="button" accesskey="N" id="btnNova" value="Novo" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ovo</button>';
    }
  }

  $objEleicaoDTO = new EleicaoDTO();
  $objEleicaoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);

  //PaginaSEI::getInstance()->prepararPaginacao($objEleicaoDTO);

  $objEleicaoRN = new EleicaoRN();
  $arrObjEleicaoDTO = $objEleicaoRN->listarSessao($objEleicaoDTO);

  //PaginaSEI::getInstance()->processarPaginacao($objEleicaoDTO);
  $numRegistros = count($arrObjEleicaoDTO);

  if ($numRegistros > 0){


    $bolCheck = false;

    if ($_GET['acao']=='eleicao_selecionar'){
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('eleicao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('eleicao_alterar');
      $bolAcaoClonar = false;
      $bolAcaoImprimir = false;
      //$bolAcaoGerarPlanilha = false;
      $bolAcaoExcluir = false;
      $bolAcaoDesativar = false;
      $bolCheck = true;
/*     }else if ($_GET['acao']=='eleicao_reativar'){
      $bolAcaoReativar = SessaoSEI::getInstance()->verificarPermissao('eleicao_reativar');
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('eleicao_consultar');
      $bolAcaoAlterar = false;
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('eleicao_excluir');
      $bolAcaoDesativar = false;
 */    }else{
      $bolAcaoReativar = false;
      $bolAcaoConsultar = SessaoSEI::getInstance()->verificarPermissao('eleicao_consultar');
      $bolAcaoAlterar = SessaoSEI::getInstance()->verificarPermissao('eleicao_alterar');
      $bolAcaoClonar = SessaoSEI::getInstance()->verificarPermissao('eleicao_clonar');
      $bolAcaoImprimir = true;
      //$bolAcaoGerarPlanilha = SessaoSEI::getInstance()->verificarPermissao('infra_gerar_planilha_tabela');
      $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('eleicao_excluir');
      $bolAcaoDesativar = SessaoSEI::getInstance()->verificarPermissao('eleicao_desativar');
    }

    if ($bolAcaoExcluir){
      $bolCheck = true;
      $arrComandos[] = '<button type="button" accesskey="E" id="btnExcluir" value="Excluir" onclick="acaoExclusaoMultipla();" class="infraButton"><span class="infraTeclaAtalho">E</span>xcluir</button>';
      $strLinkExcluir = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_excluir&acao_origem='.$_GET['acao']);
    }


    $strResultado = '';

    /* if ($_GET['acao']!='eleicao_reativar'){ */
      $strSumarioTabela = 'Tabela de Escrutínios Eletrônicos.';
      $strCaptionTabela = 'Escrutínios Eletrônicos';
    /* }else{
      $strSumarioTabela = 'Tabela de Escrutínios Eletrônicos Inativas.';
      $strCaptionTabela = 'Escrutínios Eletrônicos Inativas';
    } */

    $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
    $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
    $strResultado .= '<tr>';
    if ($bolCheck) {
      $strResultado .= '<th class="infraTh" width="1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
    }
    $strResultado .= '<th class="infraTh" width="8%">Ordem</th>'."\n";
    $strResultado .= '<th class="infraTh">Identificação</th>'."\n";
    $strResultado .= '<th class="infraTh" width="8%">Secreto</th>'."\n";
    $strResultado .= '<th class="infraTh" width="8%">Situação</th>'."\n";
    $strResultado .= '<th class="infraTh" width="8%">Votos</th>'."\n";
    $strResultado .= '<th class="infraTh" width="8%">Aguardando</th>'."\n";
    $strResultado .= '<th class="infraTh">Ações</th>'."\n";
    $strResultado .= '</tr>'."\n";
    $strCssTr='';

    for($i = 0;$i < $numRegistros; $i++){

      $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
      $strResultado .= $strCssTr;

      if ($bolCheck){
        $strResultado .= '<td valign="top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjEleicaoDTO[$i]->getNumIdEleicao(),$arrObjEleicaoDTO[$i]->getStrIdentificacao()).'</td>';
      }
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML($arrObjEleicaoDTO[$i]->getNumOrdem()).'</td>';
      $strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjEleicaoDTO[$i]->getStrIdentificacao()).'</td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML($arrObjEleicaoDTO[$i]->getStrSinSecreta()).'</td>';
      $strResultado .= '<td align="center">'.PaginaSEI::tratarHTML($arrObjEleicaoDTO[$i]->getStrDescricaoSituacao()).'</td>';
      $strResultado .= '<td align="center">'.EleicaoINT::montarTooltipVotacao($arrObjEleicaoDTO[$i]->getArrObjColegiadoComposicaoDTOVotos(),'Votos').'</td>';
      $strResultado .= '<td align="center">'.EleicaoINT::montarTooltipVotacao($arrObjEleicaoDTO[$i]->getArrObjColegiadoComposicaoDTOAguardando(),'Aguardando').'</td>';
      $strResultado .= '<td align="center">';

      $strResultado .= PaginaSEI::getInstance()->getAcaoTransportarItem($i,$arrObjEleicaoDTO[$i]->getNumIdEleicao());

      if ($bolAcaoConsultar){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_consultar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_eleicao='.$arrObjEleicaoDTO[$i]->getNumIdEleicao()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeConsultar().'" title="Consultar Escrutínio Eletrônico" alt="Consultar Escrutínio Eletrônico" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoAlterar){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_eleicao='.$arrObjEleicaoDTO[$i]->getNumIdEleicao()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Escrutínio Eletrônico" alt="Alterar Escrutínio Eletrônico" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoClonar){
        $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=eleicao_clonar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_eleicao='.$arrObjEleicaoDTO[$i]->getNumIdEleicao()).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeClonar().'" title="Clonar Escrutínio Eletrônico" alt="Clonar Escrutínio Eletrônico" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoDesativar || $bolAcaoReativar || $bolAcaoExcluir){
        $strId = $arrObjEleicaoDTO[$i]->getNumIdEleicao();
        $strDescricao = PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjEleicaoDTO[$i]->getStrIdentificacao());
      }
/* 
      if ($bolAcaoDesativar){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoDesativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeDesativar().'" title="Desativar Escrutínio Eletrônico" alt="Desativar Escrutínio Eletrônico" class="infraImg" /></a>&nbsp;';
      }

      if ($bolAcaoReativar){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoReativar(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeReativar().'" title="Reativar Escrutínio Eletrônico" alt="Reativar Escrutínio Eletrônico" class="infraImg" /></a>&nbsp;';
      }
 */

      if ($bolAcaoExcluir){
        $strResultado .= '<a href="'.PaginaSEI::getInstance()->montarAncora($strId).'" onclick="acaoExcluir(\''.$strId.'\',\''.$strDescricao.'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Escrutínio Eletrônico" alt="Excluir Escrutínio Eletrônico" class="infraImg" /></a>&nbsp;';
      }

      $strResultado .= '</td></tr>'."\n";
    }
    $strResultado .= '</table>';
  }

  /*
  if ($_GET['acao'] == 'eleicao_selecionar'){
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFecharSelecao" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }else{
    $arrComandos[] = '<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\'" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
  }
  */

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
<?if(0){?><style><?}?>

<?if(0){?></style><?}?>
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
<?if(0){?><script type="text/javascript"><?}?>

function inicializar(){
  if ('<?=$_GET['acao']?>'=='eleicao_selecionar'){
    infraReceberSelecao();
    document.getElementById('btnFecharSelecao').focus();
  }else{
    document.getElementById('btnNova').focus();
  }
  infraEfeitoTabelas(true);
}

<? if ($bolAcaoDesativar){ ?>
function acaoDesativar(id,desc){
  if (confirm("Confirma desativação do Escrutínio Eletrônico \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmEleicaoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmEleicaoLista').submit();
  }
}

function acaoDesativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Escrutínio Eletrônico selecionado.');
    return;
  }
  if (confirm("Confirma desativação dos Escrutínios Eletrônicos selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmEleicaoLista').action='<?=$strLinkDesativar?>';
    document.getElementById('frmEleicaoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoReativar){ ?>
function acaoReativar(id,desc){
  if (confirm("Confirma reativação do Escrutínio Eletrônico \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmEleicaoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmEleicaoLista').submit();
  }
}

function acaoReativacaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Escrutínio Eletrônico selecionado.');
    return;
  }
  if (confirm("Confirma reativação dos Escrutínios Eletrônicos selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmEleicaoLista').action='<?=$strLinkReativar?>';
    document.getElementById('frmEleicaoLista').submit();
  }
}
<? } ?>

<? if ($bolAcaoExcluir){ ?>
function acaoExcluir(id,desc){
  if (confirm("Confirma exclusão do Escrutínio Eletrônico \""+desc+"\"?")){
    document.getElementById('hdnInfraItemId').value=id;
    document.getElementById('frmEleicaoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmEleicaoLista').submit();
  }
}

function acaoExclusaoMultipla(){
  if (document.getElementById('hdnInfraItensSelecionados').value==''){
    alert('Nenhum Escrutínio Eletrônico selecionado.');
    return;
  }
  if (confirm("Confirma exclusão dos Escrutínios Eletrônicos selecionados?")){
    document.getElementById('hdnInfraItemId').value='';
    document.getElementById('frmEleicaoLista').action='<?=$strLinkExcluir?>';
    document.getElementById('frmEleicaoLista').submit();
  }
}
<? } ?>

<?if(0){?></script><?}?>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmEleicaoLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros);
  //PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
