<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 05/06/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.34.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ProvimentoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarStrConteudo(ProvimentoDTO $objProvimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objProvimentoDTO->getStrConteudo())){
      $objInfraException->adicionarValidacao('Conte�do n�o informado.');
    }else{
      $objProvimentoDTO->setStrConteudo(trim($objProvimentoDTO->getStrConteudo()));

      if (strlen($objProvimentoDTO->getStrConteudo())>4000){
        $objInfraException->adicionarValidacao('Conte�do possui tamanho superior a 4000 caracteres.');
      }

      $dto = new ProvimentoDTO();
      $dto->setNumIdProvimento($objProvimentoDTO->getNumIdProvimento(),InfraDTO::$OPER_DIFERENTE);
      $dto->setStrConteudo($objProvimentoDTO->getStrConteudo());

      if ($this->contar($dto)){
        $objInfraException->adicionarValidacao('Existe outro Provimento cadastrado com o mesmo Conte�do.');
      }
    }
  }
  private function validarStrSinAtivo(ProvimentoDTO $objProvimentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objProvimentoDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else{
      if (!InfraUtil::isBolSinalizadorValido($objProvimentoDTO->getStrSinAtivo())){
        $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
      }
    }
  }


  protected function cadastrarControlado(ProvimentoDTO $objProvimentoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('provimento_cadastrar',__METHOD__,$objProvimentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrConteudo($objProvimentoDTO, $objInfraException);
      $this->validarStrSinAtivo($objProvimentoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      $ret = $objProvimentoBD->cadastrar($objProvimentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Provimento.',$e);
    }
  }

  protected function alterarControlado(ProvimentoDTO $objProvimentoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('provimento_alterar',__METHOD__,$objProvimentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objProvimentoDTO->isSetStrConteudo()){
        $this->validarStrConteudo($objProvimentoDTO, $objInfraException);
      }
      if ($objProvimentoDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objProvimentoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      $objProvimentoBD->alterar($objProvimentoDTO);

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Provimento.',$e);
    }
  }

  protected function excluirControlado($arrObjProvimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('provimento_excluir',__METHOD__,$arrObjProvimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjProvimentoDTO);$i++){
        $objProvimentoBD->excluir($arrObjProvimentoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Provimento.',$e);
    }
  }

  protected function consultarConectado(ProvimentoDTO $objProvimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('provimento_consultar',__METHOD__,$objProvimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      $ret = $objProvimentoBD->consultar($objProvimentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Provimento.',$e);
    }
  }

  protected function listarConectado(ProvimentoDTO $objProvimentoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('provimento_listar',__METHOD__,$objProvimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      $ret = $objProvimentoBD->listar($objProvimentoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Provimentos.',$e);
    }
  }

  protected function contarConectado(ProvimentoDTO $objProvimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('provimento_listar',__METHOD__,$objProvimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      $ret = $objProvimentoBD->contar($objProvimentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Provimentos.',$e);
    }
  }
  protected function desativarControlado($arrObjProvimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('provimento_desativar',__METHOD__,$arrObjProvimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjProvimentoDTO); $i< $iMax; $i++){
        $objProvimentoBD->desativar($arrObjProvimentoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Provimento.',$e);
    }
  }

  protected function reativarControlado($arrObjProvimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('provimento_reativar',__METHOD__,$arrObjProvimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjProvimentoDTO); $i< $iMax; $i++){
        $objProvimentoBD->reativar($arrObjProvimentoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Provimento.',$e);
    }
  }
  protected function bloquearControlado(ProvimentoDTO $objProvimentoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('qualificacao_parte_consultar',__METHOD__,$objProvimentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objProvimentoBD = new ProvimentoBD($this->getObjInfraIBanco());
      $ret = $objProvimentoBD->bloquear($objProvimentoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Qualifica��o.',$e);
    }
  }
}
?>