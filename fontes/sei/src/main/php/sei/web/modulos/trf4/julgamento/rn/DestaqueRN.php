<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class DestaqueRN extends InfraRN {

  public static $TV_SESSAO_ATUAL='A';
  public static $TV_SESSOES_ANTERIORES='O';
  public static $TV_TODAS_SESSOES='T';
  
  //tipos de acesso aos destaque
  public static $TA_PUBLICO='P';
  public static $TA_RESTRITO='R';

  //tipos de destaque
  public static $TD_DIVERGENCIA='D';
  public static $TD_RETIRADA='R';
  public static $TD_VISTA='V';
  public static $TD_COMENTARIO='C';
  public static $TD_IMPEDIMENTO='I';
  public static $TD_SUSTENTACAO_ORAL='S';
  public static $TD_ACOMPANHA_RELATOR='A';
  public static $TD_ACOMPANHA_DIVERGENCIA='B';
  public static $TD_ABSTER='E';
  public static $TD_RESSALVA='Z'; //#207730
  public static $TD_DISCORDA_JULG_VIRTUAL='X';
  public static $TD_COMENTARIO_RESTRITO='T';
  public static $TD_AGUARDA_VISTA='G';



  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  /**
   * @return TipoDestaqueDTO[]
   * @throws InfraException
   */
  public function listarValoresTipoDestaque(): array
  {
    try {

      $objArrTipoDestaqueDTO = array();

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_ACOMPANHA_RELATOR);
      $objTipoDestaqueDTO->setStrDescricao('Acompanhar Relator');
      $objTipoDestaqueDTO->setStrTooltip('Acompanhar Relator');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_ACOMPANHA_RELATOR);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_ACOMPANHA_DIVERGENCIA);
      $objTipoDestaqueDTO->setStrDescricao('Acompanhar Diverg�ncia');
      $objTipoDestaqueDTO->setStrTooltip('Acompanha Diverg�ncia');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_ACOMPANHA_DIVERGENCIA);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_DIVERGENCIA);
      $objTipoDestaqueDTO->setStrDescricao('Divergir');
      $objTipoDestaqueDTO->setStrTooltip('Divergir');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_DIVERGENCIA);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_RESSALVA);
      $objTipoDestaqueDTO->setStrDescricao('Ressalva');
      $objTipoDestaqueDTO->setStrTooltip('Ressalva');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_RESSALVA);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_VISTA);
      $objTipoDestaqueDTO->setStrDescricao('Pedido de Vista');
      $objTipoDestaqueDTO->setStrTooltip('Pedido de Vista');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_VISTA);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_AGUARDA_VISTA);
      $objTipoDestaqueDTO->setStrDescricao('Aguarda Vista');
      $objTipoDestaqueDTO->setStrTooltip('Aguarda Vista');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_AGUARDA_VISTA);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_COMENTARIO);
      $objTipoDestaqueDTO->setStrDescricao('Coment�rio');
      $objTipoDestaqueDTO->setStrTooltip('Coment�rio');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_COMENTARIO);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_COMENTARIO_RESTRITO);
      $objTipoDestaqueDTO->setStrDescricao('Coment�rio Interno');
      $objTipoDestaqueDTO->setStrTooltip('Coment�rio Interno');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::COMENTARIOS_INTERNOS);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_IMPEDIMENTO);
      $objTipoDestaqueDTO->setStrDescricao('Impedimento/Suspei��o');
      $objTipoDestaqueDTO->setStrTooltip('Impedimento/Suspei��o');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_IMPEDIMENTO);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_ABSTER);
      $objTipoDestaqueDTO->setStrDescricao('Abster');
      $objTipoDestaqueDTO->setStrTooltip('Abster');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_ABSTER);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_RETIRADA);
      $objTipoDestaqueDTO->setStrDescricao('Retirar');
      $objTipoDestaqueDTO->setStrTooltip('Retirar');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_RETIRADA);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_SUSTENTACAO_ORAL);
      $objTipoDestaqueDTO->setStrDescricao('Sustenta��o Oral');
      $objTipoDestaqueDTO->setStrTooltip('Sustenta��o Oral');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_SUSTENTACAO_ORAL);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;

      $objTipoDestaqueDTO = new TipoDestaqueDTO();
      $objTipoDestaqueDTO->setStrStaTipo(self::$TD_DISCORDA_JULG_VIRTUAL);
      $objTipoDestaqueDTO->setStrDescricao('Discord�ncia com o julgamento virtual');
      $objTipoDestaqueDTO->setStrTooltip('Discorda de Julgamento Virutal');
      $objTipoDestaqueDTO->setStrIcone(MdJulgarIcone::DESTAQUE_DISCORDA_JULG_VIRT);
      $objArrTipoDestaqueDTO[] = $objTipoDestaqueDTO;



      return $objArrTipoDestaqueDTO;

    }catch(Exception $e){
      throw new InfraException('Erro listando Tipos de Destaque.',$e);
    }
  }

  private function validarNumIdItemSessaoJulgamento(DestaqueDTO $objDestaqueDTO, InfraException $objInfraException): void
  {
    if (InfraString::isBolVazia($objDestaqueDTO->getNumIdItemSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Item de Julgamento n�o informado.');
    }
  }

  private function validarStrDescricao(DestaqueDTO $objDestaqueDTO, InfraException $objInfraException): void
  {
    if (InfraString::isBolVazia($objDestaqueDTO->getStrDescricao())){
      if($objDestaqueDTO->getStrStaAcesso()!=self::$TA_PUBLICO || $objDestaqueDTO->getStrStaTipo()==self::$TD_COMENTARIO){
        $objInfraException->adicionarValidacao('Descri��o n�o informada.');
      } else {
        $objDestaqueDTO->setStrDescricao(null);
      }
    }else{
      $objDestaqueDTO->setStrDescricao(trim($objDestaqueDTO->getStrDescricao()));
    }
  }
  private function validarStrSinBloqueado(DestaqueDTO $objDestaqueDTO, InfraException $objInfraException): void
  {
    if (InfraString::isBolVazia($objDestaqueDTO->getStrSinBloqueado())){
      $objInfraException->adicionarValidacao('Sinalizador de Bloqueio n�o informado.');
    }else if (!InfraUtil::isBolSinalizadorValido($objDestaqueDTO->getStrSinBloqueado())){
      $objInfraException->adicionarValidacao('Sinalizador de Bloqueio inv�lido.');
    }
  }
  private function validarStrStaAcesso(DestaqueDTO $objDestaqueDTO, InfraException $objInfraException): void
  {
    if (InfraString::isBolVazia($objDestaqueDTO->getStrStaAcesso())){
      $objInfraException->adicionarValidacao('Tipo de Acesso n�o informado.');
    }else if (!in_array($objDestaqueDTO->getStrStaAcesso(),array(self::$TA_PUBLICO,self::$TA_RESTRITO),true)){
      $objInfraException->adicionarValidacao('Tipo de Acesso inv�lido.');
    }
  }
  private function validarStrStaTipo(DestaqueDTO $objDestaqueDTO, InfraException $objInfraException): void
  {
    if (InfraString::isBolVazia($objDestaqueDTO->getStrStaTipo())){
      $objInfraException->adicionarValidacao('Tipo de Destaque/Coment�rio n�o informado.');
    }else if (!in_array($objDestaqueDTO->getStrStaTipo(),InfraArray::converterArrInfraDTO($this->listarValoresTipoDestaque(),'StaTipo'))){
      $objInfraException->adicionarValidacao('Tipo de Destaque/Coment�rio inv�lido.');
    }
  }

  protected function cadastrarControlado(DestaqueDTO $objDestaqueDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_cadastrar',__METHOD__,$objDestaqueDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdItemSessaoJulgamento($objDestaqueDTO, $objInfraException);
      $this->validarStrSinBloqueado($objDestaqueDTO, $objInfraException);
      $this->validarStrStaAcesso($objDestaqueDTO, $objInfraException);
      $this->validarStrStaTipo($objDestaqueDTO, $objInfraException);
      $this->validarStrDescricao($objDestaqueDTO, $objInfraException);
      $objDestaqueDTO->setDthDestaque(InfraData::getStrDataHoraAtual());
      $objDestaqueDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objDestaqueDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());

      //permitir multiplos destaques/comentarios por usuario #181328
//      $objDestaqueDTOBanco=new DestaqueDTO();
//      $objDestaqueDTOBanco->setNumIdItemSessaoJulgamento($objDestaqueDTO->getNumIdItemSessaoJulgamento());
//      $objDestaqueDTOBanco->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
//      $objDestaqueDTOBanco->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
//      $objDestaqueDTOBanco->retNumIdDestaque();
//      $objDestaqueDTOBanco->setStrStaAcesso($objDestaqueDTO->getStrStaAcesso());
//      $objDestaqueDTOBanco->setStrSinBloqueado('N');

//      if ($this->contar($objDestaqueDTOBanco)>0){
//        $strTipoDestaque=$objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_RESTRITO?'Coment�rio':'Destaque';
//        $objInfraException->lancarValidacao("Existe um $strTipoDestaque edit�vel deste tipo.");
//      }


      $objInfraException->lancarValidacoes();

      $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objDestaqueDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retDthSessaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrSinVirtualTipoSessao();
      $objItemSessaoJulgamentoDTO->retNumIdUnidadeResponsavelColegiado();
      $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
      $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

      if ($objDestaqueDTO->getStrStaAcesso()===self::$TA_RESTRITO &&
          in_array($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento(),array(SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_FINALIZADA),true))
      {
        $objInfraException->adicionarValidacao('Situa��o da Sess�o de Julgamento n�o permite cria��o de coment�rios.');
      }
      if ($objDestaqueDTO->getStrStaAcesso()===self::$TA_PUBLICO) {
        if(!in_array($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento(),array(SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA))){
          $objInfraException->adicionarValidacao('Situa��o da Sess�o de Julgamento n�o permite cria��o de Destaques.');
        } else if($objItemSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S' &&
            InfraData::compararDataHora($objItemSessaoJulgamentoDTO->getDthSessaoSessaoJulgamento(),InfraData::getStrDataHoraAtual())>=0){
          $objInfraException->adicionarValidacao('Hor�rio de vota��o da Sess�o de Julgamento encerrado.');
        }
      }


      if($objItemSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual() && !$this->verificarUnidadeColegiado($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento())){
        $objInfraException->adicionarValidacao('Unidade atual n�o faz parte do Colegiado.');
      }
      $objInfraException->lancarValidacoes();

      $objDestaqueBD = new DestaqueBD($this->getObjInfraIBanco());
      $ret = $objDestaqueBD->cadastrar($objDestaqueDTO);

      $objRelDestaqueUsuarioDTO=new RelDestaqueUsuarioDTO();
      $objRelDestaqueUsuarioRN=new RelDestaqueUsuarioRN();
      $objRelDestaqueUsuarioDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objRelDestaqueUsuarioDTO->setNumIdDestaque($ret->getNumIdDestaque());
      $objRelDestaqueUsuarioRN->cadastrar($objRelDestaqueUsuarioDTO);

      //lancar eventos para atualizar clientes
      $objEventoSessaoDTO = new EventoSessaoDTO();
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_DESTAQUE);
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objDestaqueDTO->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setStrDescricao(SessaoSEI::getInstance()->getNumIdUsuario());
      EventoSessaoRN::lancar($objEventoSessaoDTO);
      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Destaque/Coment�rio.',$e);
    }
  }
  protected function marcarLeituraControlado(DestaqueDTO $objDestaqueDTO): void
  {
    try {
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_marcar_leitura', __METHOD__, $objDestaqueDTO);

      $numIdUsuario = SessaoSEI::getInstance()->getNumIdUsuario();

      $objRelDestaqueUsuarioRN = new RelDestaqueUsuarioRN();
      $objRelDestaqueUsuarioDTO = new RelDestaqueUsuarioDTO();
      $objRelDestaqueUsuarioDTO->setNumIdUsuario($numIdUsuario);
      $objRelDestaqueUsuarioDTO->setNumIdDestaque($objDestaqueDTO->getNumIdDestaque());
      if($objRelDestaqueUsuarioRN->contar($objRelDestaqueUsuarioDTO)==0){
        $objRelDestaqueUsuarioRN->cadastrar($objRelDestaqueUsuarioDTO);
      }

    } catch (Exception $e) {
      throw new InfraException('Erro confirmando leitura do Destaque/Coment�rio.', $e);
    }
  }
  protected function marcarLeituraTodosControlado(DestaqueDTO $objDestaqueDTO)
  {
    try {
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_marcar_leitura', __METHOD__, $objDestaqueDTO);

      $numIdUsuario = SessaoSEI::getInstance()->getNumIdUsuario();

      $objDestaqueDTOBanco=new DestaqueDTO();
      $objDestaqueDTOBanco->setNumIdItemSessaoJulgamento($objDestaqueDTO->getNumIdItemSessaoJulgamento());
      $objDestaqueDTOBanco->retNumIdDestaque();
      $arrObjDestaqueDTO=$this->listar($objDestaqueDTOBanco);

      if(count($arrObjDestaqueDTO)==0){
        return;
      }
      $arrNumIdDestaque=InfraArray::converterArrInfraDTO($arrObjDestaqueDTO,'IdDestaque','IdDestaque');

      $objRelDestaqueUsuarioRN = new RelDestaqueUsuarioRN();
      $objRelDestaqueUsuarioDTO = new RelDestaqueUsuarioDTO();
      $objRelDestaqueUsuarioDTO->setNumIdUsuario($numIdUsuario);
      $objRelDestaqueUsuarioDTO->setNumIdDestaque($arrNumIdDestaque,InfraDTO::$OPER_IN);
      $objRelDestaqueUsuarioDTO->retNumIdDestaque();
      $arrObjRelDestaqueUsuarioDTO=$objRelDestaqueUsuarioRN->listar($objRelDestaqueUsuarioDTO);
      foreach ($arrObjRelDestaqueUsuarioDTO as $objRelDestaqueUsuarioDTO) {
        unset($arrNumIdDestaque[$objRelDestaqueUsuarioDTO->getNumIdDestaque()]);
      }

      $objRelDestaqueUsuarioDTO = new RelDestaqueUsuarioDTO();
      $objRelDestaqueUsuarioDTO->setNumIdUsuario($numIdUsuario);
      foreach ($arrNumIdDestaque as $numIdDestaque) {
        $objRelDestaqueUsuarioDTO->setNumIdDestaque($numIdDestaque);
        $objRelDestaqueUsuarioRN->cadastrar($objRelDestaqueUsuarioDTO);
      }

    } catch (Exception $e) {
      throw new InfraException('Erro confirmando leitura do Destaque/Coment�rio.', $e);
    }
  }
  protected function desmarcarLeituraControlado(DestaqueDTO $objDestaqueDTO)
  {
    try {
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_desmarcar_leitura', __METHOD__, $objDestaqueDTO);

      $numIdUsuario = SessaoSEI::getInstance()->getNumIdUsuario();

      $objRelDestaqueUsuarioRN = new RelDestaqueUsuarioRN();
      $objRelDestaqueUsuarioDTO = new RelDestaqueUsuarioDTO();
      $objRelDestaqueUsuarioDTO->setNumIdUsuario($numIdUsuario);
      $objRelDestaqueUsuarioDTO->setNumIdDestaque($objDestaqueDTO->getNumIdDestaque());
      $objRelDestaqueUsuarioDTO->retNumIdDestaque();
      $objRelDestaqueUsuarioDTO->retNumIdUsuario();
      $objRelDestaqueUsuarioRN->excluir($objRelDestaqueUsuarioRN->listar($objRelDestaqueUsuarioDTO));

    } catch (Exception $e) {
      throw new InfraException('Erro marcando Destaque/Coment�rio como n�o lido.', $e);
    }
  }

  protected function visualizarControlado(DestaqueDTO $objDestaqueDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_visualizar',__METHOD__,$objDestaqueDTO);

      $objDestaqueDTO->retNumIdUnidade();
      $objDestaqueDTO->retStrSinBloqueado();
      $objDestaqueDTO->retNumIdDestaque();
      $objDestaqueDTO->retNumIdItemSessaoJulgamento();
      $objDestaqueDTO->retNumIdSessaoJulgamentoItem();
      $objDestaqueDTO->retStrDescricao();
      $objDestaqueDTO->retDthDestaque();
      $objDestaqueDTO->retStrStaAcesso();
      $objDestaqueDTO->retNumIdUnidade();
      $objDestaqueDTO->retNumIdUsuario();
      $objDestaqueDTO->retStrSiglaUnidade();
      $objDestaqueDTO->retStrDescricaoUnidade();
      $objDestaqueDTO->retStrSiglaUsuario();
      $objDestaqueDTO->retStrNomeUsuario();
      $objDestaqueDTO->retStrStaTipo();
      $objDestaqueDTO->setOrdNumIdDestaque(InfraDTO::$TIPO_ORDENACAO_ASC);
//      $objDestaqueDTO->setOrdStrSiglaUnidade(InfraDTO::$TIPO_ORDENACAO_ASC);
//      $objDestaqueDTO->setOrdStrSiglaUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

      $numIdUsuario=SessaoSEI::getInstance()->getNumIdUsuario();
      $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();

      $objDestaqueDTO->setNumIdUsuarioRelDestaqueUsuario($numIdUsuario);
      $objDestaqueDTO->retNumIdUsuarioRelDestaqueUsuario();

      if ($objDestaqueDTO->getStrStaTipoVisualizacao()!==self::$TV_SESSAO_ATUAL) {
        $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objDestaqueDTO->getNumIdItemSessaoJulgamento());
        $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
        $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
        $objItemSessaoJulgamentoDTO->retDthSessaoSessaoJulgamento();
        $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
        $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
        $objDestaqueDTO->setDblIdProcedimentoDistribuicao($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
        $objDestaqueDTO->setNumIdColegiadoSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
        $objDestaqueDTO->retDthSessaoSessaoJulgamento();
        $objDestaqueDTO->setOrdDthSessaoSessaoJulgamento(InfraDTO::$TIPO_ORDENACAO_DESC);
        switch($objDestaqueDTO->getStrStaTipoVisualizacao()){
          case self::$TV_SESSOES_ANTERIORES:
            $objDestaqueDTO->setNumIdItemSessaoJulgamento($objDestaqueDTO->getNumIdItemSessaoJulgamento(),InfraDTO::$OPER_DIFERENTE);
            $objDestaqueDTO->setDthSessaoSessaoJulgamento($objItemSessaoJulgamentoDTO->getDthSessaoSessaoJulgamento(),InfraDTO::$OPER_MENOR);
            break;
          case self::$TV_TODAS_SESSOES:
            $objDestaqueDTO->unSetNumIdItemSessaoJulgamento();
        }
      }
      $arrObjDestaqueDTO=$this->listar($objDestaqueDTO);

//      $objRelDestaqueUsuarioRN=new RelDestaqueUsuarioRN();
      foreach ($arrObjDestaqueDTO as $key => $objDestaqueDTOBanco) {
        if ($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_RESTRITO && $objDestaqueDTOBanco->getNumIdUnidade()!=$numIdUnidadeAtual) {
          unset($arrObjDestaqueDTO[$key]);
          continue;
        }
        if ($objDestaqueDTOBanco->getNumIdUsuarioRelDestaqueUsuario()==null) {
          $arrObjDestaqueDTO[$key]->setStrSinVisualizado('N');
          //marca como visualizado para a pr�xima consulta
//            $objRelDestaqueUsuarioDTO=new RelDestaqueUsuarioDTO();
//            $objRelDestaqueUsuarioDTO->setNumIdUsuario($numIdUsuario);
//            $objRelDestaqueUsuarioDTO->setNumIdDestaque($objDestaqueDTOBanco->getNumIdDestaque());
//            $objRelDestaqueUsuarioRN->cadastrar($objRelDestaqueUsuarioDTO);
        } else {
          $arrObjDestaqueDTO[$key]->setStrSinVisualizado('S');
        }
        if ($objDestaqueDTOBanco->getStrSinBloqueado()==='N' &&
            ($objDestaqueDTOBanco->getNumIdUnidade()!=$numIdUnidadeAtual ||
                ($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_RESTRITO && $objDestaqueDTOBanco->getNumIdUsuario()!=$numIdUsuario))) {
          //foi visualizado por outra unidade. bloqueia registro. ou case seja restrito, foi visualizado por outro usu�rio
          $objDestaqueDTOBanco->setStrSinBloqueado('S');
          $objDestaqueBD = new DestaqueBD($this->getObjInfraIBanco());
          $objDestaqueBD->alterar($objDestaqueDTOBanco);
        }
      }
      //Regras de Negocio

      return array_values($arrObjDestaqueDTO);

    }catch(Exception $e){
      throw new InfraException('Erro visualizando Destaques.',$e);
    }

}
  protected function alterarControlado(DestaqueDTO $objDestaqueDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('destaque_alterar',__METHOD__,$objDestaqueDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $objDestaqueDTOBanco=new DestaqueDTO();
      $objDestaqueDTOBanco->setNumIdDestaque($objDestaqueDTO->getNumIdDestaque());
      $objDestaqueDTOBanco->retNumIdUnidade();
      $objDestaqueDTOBanco->retNumIdUsuario();
      $objDestaqueDTOBanco->retStrSinBloqueado();
      $objDestaqueDTOBanco->retStrStaAcesso();
      $objDestaqueDTOBanco->retStrDescricao();
      $objDestaqueDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objDestaqueDTOBanco->retNumIdItemSessaoJulgamento();
      $objDestaqueDTOBanco->retNumIdSessaoJulgamentoItem();
      $objDestaqueDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objDestaqueDTOBanco->retStrSinVirtualTipoSessao();
      $objDestaqueDTOBanco->retDthSessaoSessaoJulgamento();
      $objDestaqueDTOBanco=$this->consultar($objDestaqueDTOBanco);

      if ($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_RESTRITO &&
          in_array($objDestaqueDTOBanco->getStrStaSituacaoSessaoJulgamento(),array(SessaoJulgamentoRN::$ES_ENCERRADA,SessaoJulgamentoRN::$ES_FINALIZADA),true))
      {
        $objInfraException->adicionarValidacao('Situa��o da Sess�o de Julgamento n�o permite altera��o de Coment�rios.');
      }
      if ($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_PUBLICO && !in_array($objDestaqueDTOBanco->getStrStaSituacaoSessaoJulgamento(),array(SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA))){
        $objInfraException->adicionarValidacao('Situa��o da Sess�o de Julgamento n�o permite altera��o de Destaques.');
      }
      if($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_PUBLICO && $objDestaqueDTOBanco->getStrSinVirtualTipoSessao()=='S' &&
          InfraData::compararDataHora($objDestaqueDTOBanco->getDthSessaoSessaoJulgamento(),InfraData::getStrDataHoraAtual())>=0){
        $objInfraException->adicionarValidacao('Hor�rio de vota��o da Sess�o de Julgamento encerrado.');
      }
      if($objDestaqueDTOBanco->getStrSinBloqueado()==='S'){
        if ($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_PUBLICO){
          $objInfraException->lancarValidacao("Destaque j� foi visualizado por outra Unidade.");
        }
        //coment�rios: permite altera��o e exclui confirma��es de leitura
        if($objDestaqueDTOBanco->getStrDescricao()!=$objDestaqueDTO->getStrDescricao()) {
          $objRelDestaqueUsuarioDTO=new RelDestaqueUsuarioDTO();
          $objRelDestaqueUsuarioDTO->setNumIdDestaque($objDestaqueDTO->getNumIdDestaque());
          $objRelDestaqueUsuarioDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario(),InfraDTO::$OPER_DIFERENTE);
          $objRelDestaqueUsuarioDTO->retTodos();
          $objRelDestaqueUsuarioRN=new RelDestaqueUsuarioRN();
          $objRelDestaqueUsuarioRN->excluir($objRelDestaqueUsuarioRN->listar($objRelDestaqueUsuarioDTO));
        }
      }

      if ($objDestaqueDTOBanco->getNumIdUnidade()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()){
        $objInfraException->lancarValidacao("Destaque n�o � da unidade atual.");
      }
      if ($objDestaqueDTOBanco->getNumIdUsuario()!=SessaoSEI::getInstance()->getNumIdUsuario()){
        $objInfraException->lancarValidacao("Destaque n�o � do usu�rio atual.");
      }
      if(!$this->verificarUnidadeColegiado($objDestaqueDTOBanco->getNumIdColegiadoSessaoJulgamento())){
        $objColegiadoDTO=new ColegiadoDTO();
        $objColegiadoDTO->retNumIdUnidadeResponsavel();
        $objColegiadoDTO->setNumIdColegiado($objDestaqueDTOBanco->getNumIdColegiadoSessaoJulgamento());
        $objColegiadoRN=new ColegiadoRN();
        $objColegiadoDTO=$objColegiadoRN->consultar($objColegiadoDTO);
        if($objColegiadoDTO->getNumIdUnidadeResponsavel()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()){
          $objInfraException->lancarValidacao('Unidade atual n�o faz parte do Colegiado.');
        }
      }
      $objDestaqueDTO->unSetNumIdItemSessaoJulgamento();
      $objDestaqueDTO->unSetNumIdUnidade();
      $objDestaqueDTO->unSetNumIdUsuario();
      $objDestaqueDTO->setStrStaAcesso($objDestaqueDTOBanco->getStrStaAcesso());
      $objDestaqueDTO->setDthDestaque(InfraData::getStrDataHoraAtual());

      $this->validarStrStaTipo($objDestaqueDTO, $objInfraException);
      $this->validarStrDescricao($objDestaqueDTO, $objInfraException);

      $objInfraException->lancarValidacoes();
      if ($objDestaqueDTO->getStrStaTipo()==self::$TD_COMENTARIO_RESTRITO){
        $objDestaqueDTO->setStrStaAcesso(self::$TA_RESTRITO);
      }else{
        $objDestaqueDTO->setStrStaAcesso(self::$TA_PUBLICO);
      }
      if($objDestaqueDTO->getStrStaAcesso()!=$objDestaqueDTOBanco->getStrStaAcesso()){
        if($objDestaqueDTO->getStrStaAcesso()==self::$TA_RESTRITO) {
          $objInfraException->lancarValidacao('N�o � permitido alterar coment�rio restrito em um destaque p�blico.');
        } else {
          $objInfraException->lancarValidacao('N�o � permitido alterar destaque p�blico em coment�rio restrito.');
        }
      }
      $objDestaqueBD = new DestaqueBD($this->getObjInfraIBanco());
      $objDestaqueBD->alterar($objDestaqueDTO);
//lancar eventos para atualizar clientes
      $objEventoSessaoDTO = new EventoSessaoDTO();
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_DESTAQUE);
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objDestaqueDTOBanco->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objDestaqueDTOBanco->getNumIdSessaoJulgamentoItem());
      $objEventoSessaoDTO->setStrDescricao(SessaoSEI::getInstance()->getNumIdUsuario());
      EventoSessaoRN::lancar($objEventoSessaoDTO);
      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Destaque/Coment�rio.',$e);
    }
  }

  protected function excluirControlado($arrObjDestaqueDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_excluir',__METHOD__,$arrObjDestaqueDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDestaqueBD = new DestaqueBD($this->getObjInfraIBanco());
      for($i=0, $iMax = InfraArray::contar($arrObjDestaqueDTO); $i<$iMax; $i++){

        $objDestaqueDTOBanco=new DestaqueDTO();
        $objDestaqueDTOBanco->setNumIdDestaque($arrObjDestaqueDTO[$i]->getNumIdDestaque());
        $objDestaqueDTOBanco->retNumIdUnidade();
        $objDestaqueDTOBanco->retNumIdUsuario();
        $objDestaqueDTOBanco->retStrSinBloqueado();
        $objDestaqueDTOBanco->retStrStaAcesso();
        $objDestaqueDTOBanco->retNumIdColegiadoSessaoJulgamento();
        $objDestaqueDTOBanco->retStrStaSituacaoSessaoJulgamento();
        $objDestaqueDTOBanco->retStrSinVirtualTipoSessao();
        $objDestaqueDTOBanco->retDthSessaoSessaoJulgamento();
        $objDestaqueDTOBanco->retNumIdSessaoJulgamentoItem();
        $objDestaqueDTOBanco->retNumIdItemSessaoJulgamento();
        $objDestaqueDTOBanco=$this->consultar($objDestaqueDTOBanco);

        $strTipoDestaque=$objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_RESTRITO?'Coment�rio':'Destaque';

        if (!$arrObjDestaqueDTO[$i]->isSetStrSinForcarExclusao() || $arrObjDestaqueDTO[$i]->isSetStrSinForcarExclusao()!=='S'){
          if ($objDestaqueDTOBanco->getNumIdUnidade()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()){
            $objInfraException->lancarValidacao("$strTipoDestaque n�o � da unidade atual.");
          }
          if ($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_PUBLICO && !in_array($objDestaqueDTOBanco->getStrStaSituacaoSessaoJulgamento(),array(SessaoJulgamentoRN::$ES_PAUTA_FECHADA,SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_SUSPENSA))){
            $objInfraException->lancarValidacao('Situa��o da Sess�o de Julgamento n�o permite exclus�o de Destaques.');
          }
          if($objDestaqueDTOBanco->getStrStaAcesso()===self::$TA_PUBLICO && $objDestaqueDTOBanco->getStrSinVirtualTipoSessao()=='S' &&
              InfraData::compararDataHora($objDestaqueDTOBanco->getDthSessaoSessaoJulgamento(),InfraData::getStrDataHoraAtual())>=0){
            $objInfraException->lancarValidacao('Hor�rio de vota��o da Sess�o de Julgamento encerrado.');
          }
        }

        $objRelDestaqueUsuarioDTO=new RelDestaqueUsuarioDTO();
        $objRelDestaqueUsuarioDTO->setNumIdDestaque($arrObjDestaqueDTO[$i]->getNumIdDestaque());
        $objRelDestaqueUsuarioDTO->retTodos();
        $objRelDestaqueUsuarioRN=new RelDestaqueUsuarioRN();
        $arrObjRelDestaqueUsuarioDTO=$objRelDestaqueUsuarioRN->listar($objRelDestaqueUsuarioDTO);
        if (InfraArray::contar($arrObjRelDestaqueUsuarioDTO)>0){
          $objRelDestaqueUsuarioRN->excluir($arrObjRelDestaqueUsuarioDTO);
        }
        $objDestaqueBD->excluir($arrObjDestaqueDTO[$i]);
        //lancar eventos para atualizar clientes
        $objEventoSessaoDTO = new EventoSessaoDTO();
        $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_DESTAQUE);
        $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objDestaqueDTOBanco->getNumIdItemSessaoJulgamento());
        $objEventoSessaoDTO->setNumIdSessaoJulgamento($objDestaqueDTOBanco->getNumIdSessaoJulgamentoItem());
        $objEventoSessaoDTO->setStrDescricao(SessaoSEI::getInstance()->getNumIdUsuario());
        EventoSessaoRN::lancar($objEventoSessaoDTO);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Destaque/Coment�rio.',$e);
    }
  }

  protected function consultarConectado(DestaqueDTO $objDestaqueDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_consultar',__METHOD__,$objDestaqueDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDestaqueBD = new DestaqueBD($this->getObjInfraIBanco());
      $ret = $objDestaqueBD->consultar($objDestaqueDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Destaque/Coment�rio.',$e);
    }
  }

  protected function listarConectado(DestaqueDTO $objDestaqueDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_listar',__METHOD__,$objDestaqueDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDestaqueBD = new DestaqueBD($this->getObjInfraIBanco());
      $ret = $objDestaqueBD->listar($objDestaqueDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Destaques/Coment�rios.',$e);
    }
  }

  protected function contarConectado(DestaqueDTO $objDestaqueDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('destaque_listar',__METHOD__,$objDestaqueDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objDestaqueBD = new DestaqueBD($this->getObjInfraIBanco());
      $ret = $objDestaqueBD->contar($objDestaqueDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Destaques/Coment�rios.',$e);
    }
  }

  private function verificarUnidadeColegiado($idColegiado){
    $objColegiadoDTO=new ColegiadoDTO();
    $objColegiadoDTO->setNumIdColegiado($idColegiado);
    $objColegiadoRN=new ColegiadoRN();
    return $objColegiadoRN->verificaUnidadeAtual($objColegiadoDTO);
  }
}
?>