<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ImpedimentoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'impedimento';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdImpedimento','id_impedimento');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdDistribuicao','id_distribuicao');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdUsuario','id_usuario');
    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM,'IdMotivoImpedimento','id_motivo_impedimento');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'MotivoImpedimento','descricao','motivo_distribuicao');

    $this->configurarPK('IdImpedimento',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdMotivoImpedimento', 'motivo_distribuicao', 'id_motivo_distribuicao');
  }
}
?>