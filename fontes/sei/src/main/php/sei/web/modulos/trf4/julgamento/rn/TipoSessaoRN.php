<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 06/04/2020 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__.'/../../../../SEI.php';

class TipoSessaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(): InfraIBanco
  {
    return BancoSEI::getInstance();
  }

  private function validarStrDescricao(TipoSessaoDTO $objTipoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoDTO->getStrDescricao())){
      $objInfraException->adicionarValidacao(' n�o informad.');
    }else{
      $objTipoSessaoDTO->setStrDescricao(trim($objTipoSessaoDTO->getStrDescricao()));

      if (strlen($objTipoSessaoDTO->getStrDescricao())>50){
        $objInfraException->adicionarValidacao(' possui tamanho superior a 50 caracteres.');
      }
    }
  }

  private function validarStrSinVirtual(TipoSessaoDTO $objTipoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoDTO->getStrSinVirtual())){
      $objInfraException->adicionarValidacao('Sinalizador de  n�o informado.');
    }else if (!InfraUtil::isBolSinalizadorValido($objTipoSessaoDTO->getStrSinVirtual())){
      $objInfraException->adicionarValidacao('Sinalizador de  inv�lid.');
    }
  }

  private function validarStrSinAtivo(TipoSessaoDTO $objTipoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objTipoSessaoDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica n�o informado.');
    }else if (!InfraUtil::isBolSinalizadorValido($objTipoSessaoDTO->getStrSinAtivo())){
      $objInfraException->adicionarValidacao('Sinalizador de Exclus�o L�gica inv�lido.');
    }
  }

  /**
   * @param TipoSessaoDTO $objTipoSessaoDTO
   * @param InfraException $objInfraException
   */
  private function validarArrObjTipoSessaoBloco(TipoSessaoDTO $objTipoSessaoDTO, InfraException $objInfraException)
  {
    $arrObjTipoSessaoBlocoDTO=$objTipoSessaoDTO->getArrObjTipoSessaoBlocoDTO();

    if(InfraArray::contar($arrObjTipoSessaoBlocoDTO)==0){
      $objInfraException->adicionarValidacao('Tipo de sess�o deve ter pelo menos um bloco.');
    }

  }

  protected function cadastrarControlado(TipoSessaoDTO $objTipoSessaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_cadastrar',__METHOD__,$objTipoSessaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarStrDescricao($objTipoSessaoDTO, $objInfraException);
      $this->validarStrSinVirtual($objTipoSessaoDTO, $objInfraException);
      $this->validarStrSinAtivo($objTipoSessaoDTO, $objInfraException);
      $this->validarArrObjTipoSessaoBloco($objTipoSessaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBD->cadastrar($objTipoSessaoDTO);

      $numIdTipoSessao=$ret->getNumIdTipoSessao();
      //cadastrar tipo_sessao_bloco
      $arrObjTipoSessaoBlocoDTO = $objTipoSessaoDTO->getArrObjTipoSessaoBlocoDTO();
      $objTipoSessaoBlocoRN=new TipoSessaoBlocoRN();
      /** @var TipoSessaoBlocoDTO[] $arrObjTipoSessaoBlocoDTO */
      foreach ($arrObjTipoSessaoBlocoDTO as $objTipoSessaoBlocoDTO) {
        $objTipoSessaoBlocoDTO->setNumIdTipoSessao($numIdTipoSessao);
        $objTipoSessaoBlocoDTO->setNumIdTipoSessaoBloco(null);
        $objTipoSessaoBlocoRN->cadastrar($objTipoSessaoBlocoDTO);
      }
      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Tipo de Sess�o.',$e);
    }
  }

  protected function alterarControlado(TipoSessaoDTO $objTipoSessaoDTO){
    try {

      //Valida Permissao
  	   SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_alterar',__METHOD__,$objTipoSessaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objTipoSessaoDTO->isSetStrDescricao()){
        $this->validarStrDescricao($objTipoSessaoDTO, $objInfraException);
      }
      if ($objTipoSessaoDTO->isSetStrSinVirtual()){
        $this->validarStrSinVirtual($objTipoSessaoDTO, $objInfraException);
      }
      if ($objTipoSessaoDTO->isSetStrSinAtivo()){
        $this->validarStrSinAtivo($objTipoSessaoDTO, $objInfraException);
      }
      if($objTipoSessaoDTO->isSetArrObjTipoSessaoBlocoDTO()) {
        $this->validarArrObjTipoSessaoBloco($objTipoSessaoDTO, $objInfraException);
      }


      $objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      $objTipoSessaoBD->alterar($objTipoSessaoDTO);

      if($objTipoSessaoDTO->isSetArrObjTipoSessaoBlocoDTO()) {
        $numIdTipoSessao=$objTipoSessaoDTO->getNumIdTipoSessao();
        //excluir antigos
        $objTipoSessaoBlocoDTO=new TipoSessaoBlocoDTO();
        $objTipoSessaoBlocoDTO->setNumIdTipoSessao($numIdTipoSessao);
        $objTipoSessaoBlocoDTO->retNumIdTipoSessaoBloco();
        $objTipoSessaoBlocoRN=new TipoSessaoBlocoRN();
        $objTipoSessaoBlocoRN->excluir($objTipoSessaoBlocoRN->listar($objTipoSessaoBlocoDTO));
        //recadastrar
        $arrObjTipoSessaoBlocoDTO = $objTipoSessaoDTO->getArrObjTipoSessaoBlocoDTO();
        $objTipoSessaoBlocoRN=new TipoSessaoBlocoRN();
        /** @var TipoSessaoBlocoDTO[] $arrObjTipoSessaoBlocoDTO */
        foreach ($arrObjTipoSessaoBlocoDTO as $objTipoSessaoBlocoDTO) {
          $objTipoSessaoBlocoDTO->setNumIdTipoSessao($numIdTipoSessao);
          $objTipoSessaoBlocoDTO->setNumIdTipoSessaoBloco(null);
          $objTipoSessaoBlocoRN->cadastrar($objTipoSessaoBlocoDTO);
        }
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro alterando Tipo de Sess�o.',$e);
    }
  }

  protected function excluirControlado($arrObjTipoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_excluir',__METHOD__,$arrObjTipoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      foreach ($arrObjTipoSessaoDTO as $iValue) {
        //excluir antigos
        $objTipoSessaoBlocoDTO=new TipoSessaoBlocoDTO();
        $objTipoSessaoBlocoDTO->setNumIdTipoSessao($iValue->getNumIdTipoSessao());
        $objTipoSessaoBlocoDTO->retNumIdTipoSessaoBloco();
        $objTipoSessaoBlocoRN=new TipoSessaoBlocoRN();
        $objTipoSessaoBlocoRN->excluir($objTipoSessaoBlocoRN->listar($objTipoSessaoBlocoDTO));
        $objTipoSessaoBD->excluir($iValue);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Tipo de Sess�o.',$e);
    }
  }

  protected function consultarConectado(TipoSessaoDTO $objTipoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_consultar',__METHOD__,$objTipoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();
      if($objTipoSessaoDTO->isRetArrObjTipoSessaoBlocoDTO()){
        $objTipoSessaoDTO->retNumIdTipoSessao();
      }
      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBD->consultar($objTipoSessaoDTO);


      if($ret && $objTipoSessaoDTO->isRetArrObjTipoSessaoBlocoDTO()){
        $objTipoSessaoBlocoDTO=new TipoSessaoBlocoDTO();
        $objTipoSessaoBlocoDTO->setNumIdTipoSessao($ret->getNumIdTipoSessao());
        $objTipoSessaoBlocoDTO->retTodos();
        $objTipoSessaoBlocoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_DESC);

        $objTipoSessaoBlocoRN=new TipoSessaoBlocoRN();
        $arrObjTipoSessaoBlocoDTO=$objTipoSessaoBlocoRN->listar($objTipoSessaoBlocoDTO);
        $ret->setArrObjTipoSessaoBlocoDTO($arrObjTipoSessaoBlocoDTO);
      }
      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Tipo de Sess�o.',$e);
    }
  }

  protected function listarConectado(TipoSessaoDTO $objTipoSessaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_listar',__METHOD__,$objTipoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBD->listar($objTipoSessaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Tipos de Sess�o.',$e);
    }
  }

  protected function contarConectado(TipoSessaoDTO $objTipoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_listar',__METHOD__,$objTipoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBD->contar($objTipoSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Tipos de Sess�o.',$e);
    }
  }

  protected function desativarControlado($arrObjTipoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_desativar',__METHOD__,$arrObjTipoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      foreach ($arrObjTipoSessaoDTO as $iValue) {
        $objTipoSessaoBD->desativar($iValue);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro desativando Tipo de Sess�o.',$e);
    }
  }

  protected function reativarControlado($arrObjTipoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_reativar',__METHOD__,$arrObjTipoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      foreach ($arrObjTipoSessaoDTO as $iValue) {
        $objTipoSessaoBD->reativar($iValue);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro reativando Tipo de Sess�o.',$e);
    }
  }

  protected function bloquearControlado(TipoSessaoDTO $objTipoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('tipo_sessao_consultar',__METHOD__,$objTipoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objTipoSessaoBD = new TipoSessaoBD($this->getObjInfraIBanco());
      $ret = $objTipoSessaoBD->bloquear($objTipoSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Tipo de Sess�o.',$e);
    }
  }


}
?>