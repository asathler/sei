<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 10/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class TabelaHtmlDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return null;
  }

  public function montar() {

    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'IdSessaoBloco');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_NUM,'Registros');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Html');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'IdTabela');

  }
}
?>