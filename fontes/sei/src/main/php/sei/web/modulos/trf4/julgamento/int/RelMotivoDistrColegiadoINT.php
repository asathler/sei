<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/12/2018 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class RelMotivoDistrColegiadoINT extends InfraINT {

  public static function montarSelectIdColegiado($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdColegiado='', $numIdMotivoDistribuicao=''){
    $objRelMotivoDistrColegiadoDTO = new RelMotivoDistrColegiadoDTO();
    $objRelMotivoDistrColegiadoDTO->retNumIdColegiado();
    $objRelMotivoDistrColegiadoDTO->retNumIdMotivoDistribuicao();
    $objRelMotivoDistrColegiadoDTO->retNumIdColegiado();
    $objRelMotivoDistrColegiadoDTO->retStrNomeColegiado();

    if ($numIdColegiado!==''){
      $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($numIdColegiado);
    }

    if ($numIdMotivoDistribuicao!==''){
      $objRelMotivoDistrColegiadoDTO->setNumIdMotivoDistribuicao($numIdMotivoDistribuicao);
    }

    $objRelMotivoDistrColegiadoDTO->setOrdNumIdColegiado(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objRelMotivoDistrColegiadoRN = new RelMotivoDistrColegiadoRN();
    $arrObjRelMotivoDistrColegiadoDTO = $objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjRelMotivoDistrColegiadoDTO, 'IdColegiado', 'NomeColegiado');
  }
  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $strStaTipo, $numIdColegiado){
    $objRelMotivoDistrColegiadoDTO = new RelMotivoDistrColegiadoDTO();
    $objRelMotivoDistrColegiadoDTO->retNumIdColegiado();
    $objRelMotivoDistrColegiadoDTO->retNumIdMotivoDistribuicao();

    $objRelMotivoDistrColegiadoDTO->retStrDescricaoMotivoDistribuicao();
    $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($numIdColegiado);
    $objRelMotivoDistrColegiadoDTO->setStrStaTipoMotivoDistribuicao($strStaTipo);

    if ($strValorItemSelecionado!=null){
      $objRelMotivoDistrColegiadoDTO->adicionarCriterio(array('SinAtivoMotivoDistribuicao','IdMotivoDistribuicao'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    } else {
      $objRelMotivoDistrColegiadoDTO->setStrSinAtivoMotivoDistribuicao('S');
    }

    $objRelMotivoDistrColegiadoDTO->setOrdNumIdColegiado(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objRelMotivoDistrColegiadoRN = new RelMotivoDistrColegiadoRN();
    $arrObjRelMotivoDistrColegiadoDTO = $objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjRelMotivoDistrColegiadoDTO, 'IdMotivoDistribuicao', 'DescricaoMotivoDistribuicao');
  }
  public static function autoCompletarRelMotivoDistribuicaoColegiado($idColegiado,$staTipo, $strPalavrasPesquisa){


    $objRelMotivoDistrColegiadoDTO=new RelMotivoDistrColegiadoDTO();
    $objRelMotivoDistrColegiadoRN=new RelMotivoDistrColegiadoRN();

    $objRelMotivoDistrColegiadoDTO->setStrStaTipoMotivoDistribuicao($staTipo);
    $objRelMotivoDistrColegiadoDTO->setNumIdColegiado($idColegiado);
    $objRelMotivoDistrColegiadoDTO->retStrDescricaoMotivoDistribuicao();
    $objRelMotivoDistrColegiadoDTO->setStrSinAtivoMotivoDistribuicao('S');
    $objRelMotivoDistrColegiadoDTO->retNumIdMotivoDistribuicao();
    $objRelMotivoDistrColegiadoDTO->setStrDescricaoMotivoDistribuicao('%'.$strPalavrasPesquisa.'%',InfraDTO::$OPER_LIKE);

    $objRelMotivoDistrColegiadoDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);
    $objRelMotivoDistrColegiadoDTO->setNumMaxRegistrosRetorno(50);

    $arrObjRelMotivoDistrColegiadoDTO = $objRelMotivoDistrColegiadoRN->listar($objRelMotivoDistrColegiadoDTO);

    return $arrObjRelMotivoDistrColegiadoDTO;
  }
}
?>