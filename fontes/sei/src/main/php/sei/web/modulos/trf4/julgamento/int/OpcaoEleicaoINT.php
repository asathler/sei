<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class OpcaoEleicaoINT extends InfraINT {

  public static function montarSelectIdentificacao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdEleicao){

    $objOpcaoEleicaoDTO = new OpcaoEleicaoDTO();
    $objOpcaoEleicaoDTO->retNumIdOpcaoEleicao();
    $objOpcaoEleicaoDTO->retStrIdentificacao();
    $objOpcaoEleicaoDTO->setNumIdEleicao($numIdEleicao);
    $objOpcaoEleicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objOpcaoEleicaoRN = new OpcaoEleicaoRN();
    $arrObjOpcaoEleicaoDTO = $objOpcaoEleicaoRN->listar($objOpcaoEleicaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjOpcaoEleicaoDTO, 'IdOpcaoEleicao', 'Identificacao');
  }
}
