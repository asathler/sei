<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class OpcaoEleicaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdEleicao(OpcaoEleicaoDTO $objOpcaoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objOpcaoEleicaoDTO->getNumIdEleicao())){
      $objInfraException->adicionarValidacao('Escrut�nio Eletr�nico n�o informado.');
    }
  }

  private function validarStrIdentificacao(OpcaoEleicaoDTO $objOpcaoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objOpcaoEleicaoDTO->getStrIdentificacao())){
      $objInfraException->adicionarValidacao('Identifica��o n�o informada.');
    }else{
      $objOpcaoEleicaoDTO->setStrIdentificacao(trim($objOpcaoEleicaoDTO->getStrIdentificacao()));

      if (strlen($objOpcaoEleicaoDTO->getStrIdentificacao())>4000){
        $objInfraException->adicionarValidacao('Identifica��o possui tamanho superior a 4000 caracteres.');
      }

      $dto = new OpcaoEleicaoDTO();
      $dto->setNumMaxRegistrosRetorno(1);
      $dto->retStrIdentificacao();
      $dto->retStrIdentificacaoEleicao();
      $dto->retNumIdOpcaoEleicao();
      $dto->setNumIdOpcaoEleicao($objOpcaoEleicaoDTO->getNumIdOpcaoEleicao(),InfraDTO::$OPER_DIFERENTE);
      $dto->setNumIdEleicao($objOpcaoEleicaoDTO->getNumIdEleicao());
      $dto->setStrIdentificacao($objOpcaoEleicaoDTO->getStrIdentificacao());
      if (($dto=$this->consultar($dto))!=null){
        $objInfraException->lancarValidacao('J� existe uma op��o "'.$dto->getStrIdentificacao().'" cadastrada no Escrut�nio Eletr�nico "'.$dto->getStrIdentificacaoEleicao().'".');
      }
    }
  }

  private function validarNumOrdem(OpcaoEleicaoDTO $objOpcaoEleicaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objOpcaoEleicaoDTO->getNumOrdem())){
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }

  protected function cadastrarControlado(OpcaoEleicaoDTO $objOpcaoEleicaoDTO) {
    try{

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_cadastrar', __METHOD__, $objOpcaoEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdEleicao($objOpcaoEleicaoDTO, $objInfraException);
      $this->validarStrIdentificacao($objOpcaoEleicaoDTO, $objInfraException);
      $this->validarNumOrdem($objOpcaoEleicaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objOpcaoEleicaoBD->cadastrar($objOpcaoEleicaoDTO);

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Op��o de Vota��o.',$e);
    }
  }

  protected function alterarControlado(OpcaoEleicaoDTO $objOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_alterar', __METHOD__, $objOpcaoEleicaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objOpcaoEleicaoDTO->isSetNumIdEleicao()){
        $this->validarNumIdEleicao($objOpcaoEleicaoDTO, $objInfraException);
      }
      if ($objOpcaoEleicaoDTO->isSetStrIdentificacao()){
        $this->validarStrIdentificacao($objOpcaoEleicaoDTO, $objInfraException);
      }
      if ($objOpcaoEleicaoDTO->isSetNumOrdem()){
        $this->validarNumOrdem($objOpcaoEleicaoDTO, $objInfraException);
      }

      $objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      $objOpcaoEleicaoBD->alterar($objOpcaoEleicaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro alterando Op��o de Vota��o.',$e);
    }
  }

  protected function excluirControlado($arrObjOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_excluir', __METHOD__, $arrObjOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjOpcaoEleicaoDTO);$i++){
        $objOpcaoEleicaoBD->excluir($arrObjOpcaoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Op��o de Vota��o.',$e);
    }
  }

  protected function consultarConectado(OpcaoEleicaoDTO $objOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_consultar', __METHOD__, $objOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objOpcaoEleicaoBD->consultar($objOpcaoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Op��o de Vota��o.',$e);
    }
  }

  protected function listarConectado(OpcaoEleicaoDTO $objOpcaoEleicaoDTO) {
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_listar', __METHOD__, $objOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objOpcaoEleicaoBD->listar($objOpcaoEleicaoDTO);

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Op��es de Vota��o.',$e);
    }
  }

  protected function contarConectado(OpcaoEleicaoDTO $objOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_listar', __METHOD__, $objOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objOpcaoEleicaoBD->contar($objOpcaoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Op��es de Vota��o.',$e);
    }
  }
/* 
  protected function desativarControlado($arrObjOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_desativar', __METHOD__, $arrObjOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjOpcaoEleicaoDTO);$i++){
        $objOpcaoEleicaoBD->desativar($arrObjOpcaoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro desativando Op��o de Vota��o.',$e);
    }
  }

  protected function reativarControlado($arrObjOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_reativar', __METHOD__, $arrObjOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      for($i=0;$i<count($arrObjOpcaoEleicaoDTO);$i++){
        $objOpcaoEleicaoBD->reativar($arrObjOpcaoEleicaoDTO[$i]);
      }

    }catch(Exception $e){
      throw new InfraException('Erro reativando Op��o de Vota��o.',$e);
    }
  }

  protected function bloquearControlado(OpcaoEleicaoDTO $objOpcaoEleicaoDTO){
    try {

      SessaoSEI::getInstance()->validarAuditarPermissao('opcao_eleicao_consultar', __METHOD__, $objOpcaoEleicaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objOpcaoEleicaoBD = new OpcaoEleicaoBD($this->getObjInfraIBanco());
      $ret = $objOpcaoEleicaoBD->bloquear($objOpcaoEleicaoDTO);

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro bloqueando Op��o de Vota��o.',$e);
    }
  }

 */
}
