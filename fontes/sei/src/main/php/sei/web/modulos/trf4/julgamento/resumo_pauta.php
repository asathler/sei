<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__ . '/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_sessao_julgamento'));

  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
  $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
  $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
  $objEditorRN = new EditorRN();


  $strTitulo = 'Resumo de Pauta';

  $strDesabilitar = '';
  $idSessaoJulgamento = $_GET['id_sessao_julgamento'];


  $arrComandos = array();
  $arrComandosSessao = array();
  $idUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();

  switch ($_GET['acao']) {
    case 'resumo_pauta':
      PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SEM_MENU);
      break;

    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }

  $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
  $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
  $objPesquisaSessaoJulgamentoDTO->setStrSinAnotacao('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinDestaque('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinVotos('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinPedidoVista('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinPresenca('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinOrdenarItens('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinDocumentos('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinRevisao('S');
  $objPesquisaSessaoJulgamentoDTO->setStrSinEleicoes('S');

  $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);
  if ($objSessaoJulgamentoDTO == null) {
    throw new InfraException('Sess�o de Julgamento n�o encontrada.');
  }

  $strTitulo.= ' - '.$objSessaoJulgamentoDTO->getStrSiglaColegiado().' - '.$objSessaoJulgamentoDTO->getDthSessao();
  $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
  $bolAdmin = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') && SessaoSEI::getInstance()->getNumIdUnidadeAtual()==$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado();

  $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(), 'IdUsuario');
  $arrObjItemSessaoJulgamentoDTO = $objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
  $arrObjSituacaoSessaoDTO = $objSessaoJulgamentoRN->listarValoresSituacaoSessao();

  $numTotalItens = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
  $arrObjSituacaoSessaoDTO = InfraArray::indexarArrInfraDTO($arrObjSituacaoSessaoDTO, 'StaSituacao');

  $bolUnidadePertenceColegiado = false;
  foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
    if ($objColegiadoComposicaoDTO->getNumIdUnidade() == $idUnidadeAtual) {
      $bolUnidadePertenceColegiado = true;
      break;
    }
  }

  if (!$bolUnidadePertenceColegiado && $idUnidadeAtual != $objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()) {
    throw new InfraException('Usu�rio n�o pertence ao colegiado desta sess�o.');
  }

  $arrIdProcedimentos=InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdProcedimentoDistribuicao');
  $arrObjParticipanteDTO=array();
  if(InfraArray::contar($arrIdProcedimentos)>0){
    $objParticipanteDTO = new ParticipanteDTO();
    $objParticipanteDTO->retDblIdProtocolo();
    $objParticipanteDTO->retStrNomeContato();
    $objParticipanteDTO->setDblIdProtocolo($arrIdProcedimentos,InfraDTO::$OPER_IN);
    $objParticipanteDTO->setStrStaParticipacao(ParticipanteRN::$TP_INTERESSADO);
    $objParticipanteDTO->setOrdNumSequencia(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objParticipanteRN = new ParticipanteRN();
    $arrObjParticipanteDTO = $objParticipanteRN->listarRN0189($objParticipanteDTO);
    $arrObjParticipanteDTO=InfraArray::indexarArrInfraDTO($arrObjParticipanteDTO,'IdProtocolo',true);
    $objParteProcedimentoDTO=new ParteProcedimentoDTO();
    $objParteProcedimentoRN=new ParteProcedimentoRN();
    $objParteProcedimentoDTO->retTodos();
    $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
    $objParteProcedimentoDTO->retStrNomeContato();
    $objParteProcedimentoDTO->setDblIdProcedimento($arrIdProcedimentos,InfraDTO::$OPER_IN);
    $arrObjParteProcedimentoDTO=$objParteProcedimentoRN->listar($objParteProcedimentoDTO);
    $arrObjParteProcedimentoDTO=InfraArray::indexarArrInfraDTO($arrObjParteProcedimentoDTO,'IdProcedimento',true);

    $objAutuacaoDTO=new AutuacaoDTO();
    $objAutuacaoRN=new AutuacaoRN();
    $objAutuacaoDTO->retTodos();
    $objAutuacaoDTO->retStrDescricaoTipoMateria();
    $objAutuacaoDTO->setDblIdProcedimento($arrIdProcedimentos,InfraDTO::$OPER_IN);
    $arrObjAutuacaoDTO=$objAutuacaoRN->listar($objAutuacaoDTO);
    $arrObjAutuacaoDTO=InfraArray::indexarArrInfraDTO($arrObjAutuacaoDTO,'IdProcedimento');
  }
  $arrObjProtocoloDTO=CacheSessaoJulgamentoRN::pesquisaAcessoDocumentos($arrObjItemSessaoJulgamentoDTO);

  $arrSituacaoItem = InfraArray::converterArrInfraDTO($objItemSessaoJulgamentoRN->listarValoresStaSituacao(), 'Descricao', 'StaTipo');
  $idUsuarioPresidenteColegiado = $objSessaoJulgamentoDTO->getNumIdUsuarioPresidente();

  $arrObjSessaoBlocoDTO=CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($idSessaoJulgamento);

  $bolAcaoRegistrarAnotacao = SessaoSEI::getInstance()->verificarPermissao('anotacao_registrar');
  /// montar tabelas de itens

  $strCssDocumentos = '';

  $arrResultadoBlocos=array();
  $arrNumRegistrosBlocos=array();

  /** @var ItemSessaoJulgamentoDTO[] $arrObjItemSessaoJulgamentoDTO */
  foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
    $objItemSessaoJulgamentoDTO->setStrStaSituacaoSessaoJulgamento($staSessao);
    $idSessaoBloco=$objItemSessaoJulgamentoDTO->getNumIdSessaoBloco();
    $staItemAtual=$arrObjSessaoBlocoDTO[$idSessaoBloco]->getStrStaTipoItem();
    $objItemSessaoJulgamentoDTO->setStrStaTipoItemSessaoBloco($staItemAtual);

    if(!isset($arrResultadoBlocos[$idSessaoBloco])){
      $arrResultadoBlocos[$idSessaoBloco]='';
      $arrNumRegistrosBlocos[$idSessaoBloco]=0;
    }

    $objColegiadoComposicaoDTO=$arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()];
    $strResultadoBloco=$arrResultadoBlocos[$idSessaoBloco];
    $strResultadoBloco .= ResumoPautaINT::montarExibicaoItem($objItemSessaoJulgamentoDTO, $objSessaoJulgamentoDTO, $objColegiadoComposicaoDTO, $arrObjAutuacaoDTO, $arrObjParteProcedimentoDTO, $arrObjParticipanteDTO, $arrObjProtocoloDTO, $bolAdmin, $arrSituacaoItem);
    $arrResultadoBlocos[$idSessaoBloco]=$strResultadoBloco;
    $arrNumRegistrosBlocos[$idSessaoBloco]++;
  }

  $strCssDocumentos=$objEditorRN->montarCssEditor(null);
  $strLinkAjaxDispositivo=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=dispositivo_item');
  $strLinkAjaxEventos=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=eventos_sessao&id_sessao_julgamento='.$idSessaoJulgamento);
  $strLinkAjaxDadosSessao=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=dados_sessao&sin_exibir_iframe=N&acao_origem=resumo_pauta');

  $arrParametros=CacheSessaoJulgamentoRN::getArrParametrosModulo();
  $bolAtualizacaoAutomatica=false;//SessaoSEI::getInstance()->getStrSinAcessibilidade()=='N' && $arrParametros[MdJulgarConfiguracaoRN::$PAR_ATUALIZACAO_AUTOMATICA]==1;

  $objEventoSessaoDTO = new EventoSessaoDTO();
  $objEventoSessaoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
  $numUltimoEvento = EventoSessaoRN::buscarUltimoEvento($objEventoSessaoDTO);

  $strDescricaoSessao='Sess�o de '.substr($objSessaoJulgamentoDTO->getDthSessao(),0,-3);
  if ($staSessao===SessaoJulgamentoRN::$ES_ABERTA){
    $arrComandos[]='<button type="submit" accesskey="A" id="btnAtualizar" name="btnAtualizar" value="Atualizar"  class="infraButton"><span class="infraTeclaAtalho">A</span>tualizar</button>';
  }
  $arrComandos[]='<button type="button" accesskey="I" id="btnImprimir" value="Imprimir" onclick="imprimir();" class="infraButton"><span class="infraTeclaAtalho">I</span>mprimir Visualiza��o</button>';
  $arrComandos[]='<button type="button" accesskey="T" id="btnImprimirTudo" value="ImprimirTudo" onclick="imprimir(true);" class="infraButton">Imprimir <span class="infraTeclaAtalho">T</span>udo</button>';
  $arrComandos[]='<button type="button" accesskey="F" id="btnFechar" value="Fechar" onclick="window.close();" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
echo $strCssDocumentos;

?>
  .lnksel{background-color: yellow;}
  .divRotulo {padding-top:.25rem;font-weight: bold; min-width: 120px;}
  .ancora_sei{font-size: 1em;color:blue;}
  .ancora_sei:visited{color:purple;}

    #divChkDestaque {padding-top:3px}
    #lblDestaque {margin-top: 3px;}

  #fldFiltros {position: absolute; float: left;height:8em;width: 16%;border:1px solid #aaa;min-width: 175px;}

  .oculta {display:none!important;}
  #divInfraBarraComandosSuperior {clear: inherit; display: block;}

  @media print {
      body{
          font-size: 14px;
      }
      table {
          margin: 0
      }

      .divAreaGlobal {
          display: block !important;
      }

      tr td th {
          page-break-inside: auto;
      }

      #divInfraAreaTelaD {
          padding: 0 !important
      }
  }
  .infraNotificacao { border-radius: 1px;  }
  #cabecalhoResumo {width:100%;left:0;position:relative;margin:0;text-align: center;z-index: -1;}
  #cabecalhoResumo p {font-size: 20px; margin: 5px 0;}
  .divDestaque {margin-bottom: 3px;}

  .documentoResumo {border:1px solid green;}
  iframe {width:100%;height: 30px;}

  .divPublicacao {font-weight: 500; text-align: left; font-size: 9pt; border: 2px solid #777; position: relative;
    margin: 10px 0; width:30%; left: 67%; padding: 4px;}

  .trItem {border: 1px black dotted; background-color: #fbffa4; }
  .tblSelecionada {background-color: #a4c5ff;  }
  .tdSeq {width:7%;text-align: center;position: relative}
  .tdSeq div {text-align: center;}
  .tdDadosItem p {font-size: 12px; margin: 2px 0;}
  .tdStatus {text-align: center;color: red;font-weight: bold}
  .spnNegrito {font-weight: bold}
  .pDispositivo {border: 2px solid black; padding:1em; background-color: white;font-size: 13px!important;}

  label.infraLabelTitulo {position:absolute;top:0;width:100%;font-weight:normal;font-size:1rem;}

  a.espacamento{padding: 0 0.1em;}
  .resumo {float:right;color:white;padding-right: .3em;position: relative;}
  img.flashit{
    -webkit-animation: flash linear 2s infinite;
    animation: flash linear 2s infinite;
  }
  @-webkit-keyframes flash {
    0% { opacity: 1; }
    50% { opacity: .1; }
    100% { opacity: 1; }
  }
  @keyframes flash {
    0% { opacity: 1; }
    50% { opacity: .1; }
    100% { opacity: 1; }
  }
<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
var objAjaxDispositivo,idInterval=null;
  var cookie='<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>';
var idAtual=null,ultimoEvento=<?=$numUltimoEvento?>,lastResult;

  objAjaxDispositivo=new infraAjaxComplementar(null,'<?=$strLinkAjaxDispositivo;?>');
  objAjaxDispositivo.prepararExecucao=function(){
    return 'id_item_sessao_julgamento=' + idAtual;
  };
  objAjaxDispositivo.processarResultado=function(arr){
    if (arr!=null){

      var img=document.getElementById('imgAtDisp'+arr['IdItemSessaoJulgamento']);
      img.src='<?=MdJulgarIcone::ATUALIZAR_DISPOSITIVO?>';
      var pdispositivo=document.getElementById('pDispositivo'+arr['IdItemSessaoJulgamento']);
      if (arr['Dispositivo']!==undefined){
        var ib=new infraBase64();
        pdispositivo.innerText=ib.decodificar(arr['Dispositivo']);
        pdispositivo.style.display='';
      } else {
        pdispositivo.style.display='none';
      }
    }
  };

  //-----------processamento de eventos------------------------
  var eventoDispositivo=function (evento){
    var texto=evento.descricao;
    var p=$('#pDispositivo'+evento.id_item);
    if(texto!==''){
      p.show().html(texto);
    } else {
      p.hide().text('');
    }
  };
  var eventoDestaque=function (evento){

  };
  var processarEventos=function(data){
    var ev,texto,p;
    for (ev in data){
      switch (data[ev].tipo_evento){
        case <?=EventoSessaoRN::$TE_DISPOSITIVO?>:
          eventoDispositivo(data[ev]);
          break;
        case <?=EventoSessaoRN::$TE_DESTAQUE?>:
          eventoDestaque(data[ev]);
          break;
        case <?=EventoSessaoRN::$TE_ENCERRAMENTO?>:
          clearInterval(idInterval);
          idInterval=null;
          break;
        case <?=EventoSessaoRN::$TE_INCLUSAO_ITEM?>:
          if(confirm("Inclu�do novo processo na sess�o.\nDeseja atualizar o resumo?")){
            document.getElementById('frmSessaoJulgamentoCadastro').submit();
          }
          break;
      }

      if(data[ev].seq!==null){
        ultimoEvento=data[ev].seq;
      }
    }
  };
  function buscarEventos(){
    $.post( "<?=$strLinkAjaxEventos;?>",{'UltimoEvento':ultimoEvento} ,processarEventos);
  }


  function inicializar(){
    $('#divInfraAreaTela').addClass('d-print-inline');
    var cookie='<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>';
    var divs=['divPauta','divMesa','divReferendo'];
    for(var i=0;i<4;i++) {
      if (infraLerCookie(cookie +'_'+ divs[i]) == 'Fechar') {
        exibe('#' + divs[i]);
      }
    }
    var filtros=infraLerCookie(cookie +'_filtros');
    if(filtros){
      if (filtros.indexOf('D') !== -1) {
        document.getElementById('chkDestaque').click();
      }
      if (filtros.indexOf('R') !== -1) {
        document.getElementById('chkRevisao').click();
      }
    }
    infraFlagResize=true;
    infraEfeitoTabelas();
    <?if($staSessao==SessaoJulgamentoRN::$ES_ABERTA||$staSessao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA){?>
    idInterval=setInterval(buscarEventos,5000);
    <?}?>
  }

  function exibe(sel,image){
    var cookie='<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>'+'_'+sel.substr(1);
    var img;
    $(sel).toggle();
    if (image) {
      img=$(image);
    } else {
      img=$(sel).prev().find('img');
    }

    if (img.attr('src')==='<?=PaginaSEI::getInstance()->getIconeExibir()?>'){
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeOcultar()?>');
      infraCriarCookie(cookie,"Fechar",356);
    } else {
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeExibir()?>');
      infraRemoverCookie(cookie);
    }
    //label.innerHTML='&'
  }
  function exibirDoc(item,iddoc,el){
    var doc= $('#doc'+item);
    var rect=doc.get(0).getBoundingClientRect();
    $('.lnksel').removeClass('lnksel');
    if(iddoc!=null){
      doc.show().find('.documentoResumo').hide();
      $('#dd'+iddoc).show();
      event.stopPropagation();

      $(el).addClass('lnksel');
    } else if (doc.is(':visible')){
      doc.hide();
    } else {
      doc.show().find('.documentoResumo').show();
    }
    if (rect.top<0 || rect.bottom>infraClientHeight()) {
      location.href = '#item' + item;
    }
    $('.tblSelecionada').removeClass('tblSelecionada').css('backgroundColor','');
    $('#item'+item).animate({backgroundColor: "#c2ddff"},500).addClass('tblSelecionada');


  }
  function marcarTrAcessadaElemento(el){
  }
  function imprimir(todos) {
    var divComandos=$('#divInfraBarraComandosSuperior').hide();
    if(todos){
      var selecao=$('.tblSelecionada').removeClass('tblSelecionada');
      var docs=$('div [id^="doc"]').show();
      var paragrafos=$('p [id^="doc"]').show();
      window.print();
      docs.hide();
      paragrafos.hide();
      selecao.click();
    } else {
      window.print();
    }
    divComandos.show();
  }

  function abrirJanela(url,obj){
    $('.tblSelecionada').removeClass('tblSelecionada');
    $(obj).closest('.trItem').addClass('tblSelecionada');
    infraAbrirJanelaModal(url,800,600,false);
    event.preventDefault();
    event.stopPropagation();
  }

  function atualizarDispositivo(elem,id,event){
    idAtual=id;
    elem.src='<?=PaginaSEI::getInstance()->getIconeAguardar()?>';
    objAjaxDispositivo.executar();
    event.stopPropagation();
  }

  function processarFiltros(){
    var bolDestaques=$('#chkDestaque').prop('checked');
    var bolRevisado=$('#chkRevisao').prop('checked');
    var tituloResumo=$('#pResumo');
    var divs=$('div[id^="divBloco"]');

    //remove filtros
    $('.oculta').removeClass('oculta');

    if(bolDestaques || bolRevisado){
      var filtro='';
      if(bolDestaques){
        filtro+='D';
      }
      if(bolRevisado){
        filtro+='R';
      }
      infraCriarCookie(cookie +'_filtros_destaque',filtro,356);

      if(bolDestaques){
        $('.tdSeq[data-destaques="0"]').parent().addClass('oculta');
      }
      if(bolRevisado){
        $('.tdSeq[data-revisado="S"]').parent().addClass('oculta');
      }
      tituloResumo.text('RESUMO - PROCESSOS FILTRADOS');
    } else {
      infraRemoverCookie(cookie +'_filtros_destaque');
      tituloResumo.text('RESUMO');
    }

    divs.each(function(){
      var a=$(this).find('.infraAreaTabela').not('.oculta').length;
      var resumo=$(this).prev().find('.resumo');
      if(a===0){
        resumo.text('');
      } else if(a===1){
        resumo.text('1 Item');
      }else {
        resumo.text(a+ ' Itens');
      }

    });

  }

  function atualizarIconeComentarios(idItem,qtd,bolNaoLido) {
    var svg = $('#svgComentario'+idItem);
    svg.find('text').text(qtd);
    svg.find('circle').css('fill',bolNaoLido?'red':'#0056ff')
    if (window.opener && typeof window.opener.atualizarIconeComentarios === 'function'){
      window.opener.atualizarIconeComentarios(idItem,qtd,bolNaoLido);
    }
  }
<?


if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody('Resumo de Pauta','onload="inicializar();"');
?>
<form id="frmSessaoJulgamentoCadastro" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  //PaginaSEI::getInstance()->montarAreaValidacao();
  SessaoJulgamentoINT::montarDivFiltros('row px-3');
  ?>

  <div id="cabecalhoResumo">
    <div style="text-align: center;display: inline-block;">
      <p style="text-transform: uppercase;margin-top:0;"><?=$objSessaoJulgamentoDTO->getStrNomeColegiado();?></p>
      <p><?=$strDescricaoSessao;?></p>
      <p id="pResumo">RESUMO</p>
    </div>
  </div>

  <br/>

  <div id="divConteudo">


  <?
  foreach ($arrObjSessaoBlocoDTO as $objSessaoBlocoDTO) {
    $strDescricao=$objSessaoBlocoDTO->getStrDescricao();
    $numIdSessaoBloco=$objSessaoBlocoDTO->getNumIdSessaoBloco();
    SessaoJulgamentoINT::montarBarraBloco($numIdSessaoBloco, false, false, $strDescricao, $arrNumRegistrosBlocos[$numIdSessaoBloco]??0);
    echo "<div id='divBloco$numIdSessaoBloco'>";
    if ($arrNumRegistrosBlocos[$numIdSessaoBloco]) {
     echo $arrResultadoBlocos[$numIdSessaoBloco];
    } else {
      PaginaSEI::getInstance()->montarAreaTabela('', null, true);
      echo '<br />';
    }
    echo '</div>';
  }


  echo '</div>';//fecha divConteudo

  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>