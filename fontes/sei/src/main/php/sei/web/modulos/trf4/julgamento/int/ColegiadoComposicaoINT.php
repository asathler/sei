<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2014 - criado por mkr@trf4.jus.br
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class ColegiadoComposicaoINT extends InfraINT {

  public static function montarSelectNome($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdColegiadoVersao='', $numIdUnidade='', $numIdUsuario=''){
    $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoDTO->retNumIdUsuario();
    $objColegiadoComposicaoDTO->retStrNomeUsuario();

    if ($numIdColegiadoVersao!==''){
      $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($numIdColegiadoVersao);
    }

    if ($numIdUnidade!==''){
      $objColegiadoComposicaoDTO->setNumIdUnidade($numIdUnidade);
    }

    if ($numIdUsuario!==''){
      $objColegiadoComposicaoDTO->setNumIdUsuario($numIdUsuario);
    }

    $objColegiadoComposicaoDTO->setStrSinHabilitado('S');
    $objColegiadoComposicaoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
    $arrObjColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjColegiadoComposicaoDTO, 'IdUsuario', 'NomeUsuario');
  }

  public static function getArrUnidadesColegiado($idColegiadoVersao)
  {
    if ($idColegiadoVersao==null) return null;
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
    $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($idColegiadoVersao);
    $objColegiadoComposicaoDTO->retNumIdUnidade();
    $objColegiadoComposicaoDTO->setDistinct(true);
    $arrObjColegiadoComposiacaoDTO=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
    if (InfraArray::contar($arrObjColegiadoComposiacaoDTO)){
      return InfraArray::converterArrInfraDTO($arrObjColegiadoComposiacaoDTO,'IdUnidade');
    }
    return null;

  }

  public static function getIdUsuarioMembroUnidadeAtual($numIdColegiado,$numIdColegiadoVersao=null)
  {
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
    $objColegiadoComposicaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objColegiadoComposicaoDTO->retNumIdUsuario();
    if ($numIdColegiadoVersao==null){
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
    } else {
     $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($numIdColegiadoVersao); 
    }
    $objColegiadoComposicaoDTO=$objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);
    return ($objColegiadoComposicaoDTO==null?null:$objColegiadoComposicaoDTO->getNumIdUsuario());
  }
  public static function montarTabelaUnidades($numIdColegiado,$arrNumIdUnidadesOcultas,$arrNumIdUnidadesSelecionadas)
  {
    if(!is_array($arrNumIdUnidadesSelecionadas)){
      $arrNumIdUnidadesSelecionadas=array();
    }
    $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
    $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
    $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
    $objColegiadoComposicaoDTO->setNumIdUnidade($arrNumIdUnidadesOcultas,InfraDTO::$OPER_NOT_IN);
    $objColegiadoComposicaoDTO->retNumIdUnidade();
    $objColegiadoComposicaoDTO->retStrSiglaUnidade();
    $objColegiadoComposicaoDTO->retStrDescricaoUnidade();
    $objColegiadoComposicaoDTO->retStrNomeTipoMembroColegiado();
    PaginaSEI::getInstance()->prepararOrdenacao($objColegiadoComposicaoDTO, 'SiglaUnidade', InfraDTO::$TIPO_ORDENACAO_ASC);

    $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();

    $arrObjColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
    $numRegistros = InfraArray::contar($arrObjColegiadoComposicaoDTO);
    $strResultado = '';
    if ($numRegistros > 0){

      $strSumarioTabela = 'Tabela de Unidades.';
      $strCaptionTabela = 'Unidades';

      $strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
//      $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
      $strResultado .= '<tr>';
      $strResultado .= '<th class="infraTh" style="width:1%">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
      $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoComposicaoDTO,'Sigla','SiglaUnidade',$arrObjColegiadoComposicaoDTO).'</th>'."\n";
      $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoComposicaoDTO,'Descri��o','DescricaoUnidade',$arrObjColegiadoComposicaoDTO).'</th>'."\n";
      $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThOrdenacao($objColegiadoComposicaoDTO,'Titularidade','NomeTipoMembroColegiado',$arrObjColegiadoComposicaoDTO).'</th>'."\n";
      $strResultado .= '</tr>'."\n";
      $strCssTr='';
      for($i = 0;$i < $numRegistros; $i++){

        $strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
        $strResultado .= $strCssTr;
        $strResultado .= '<td style="vertical-align: top">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjColegiadoComposicaoDTO[$i]->getNumIdUnidade(),
                UnidadeINT::formatarSiglaDescricao($arrObjColegiadoComposicaoDTO[$i]->getStrSiglaUnidade(),$arrObjColegiadoComposicaoDTO[$i]->getStrDescricaoUnidade()),
                in_array($arrObjColegiadoComposicaoDTO[$i]->getNumIdUnidade(),$arrNumIdUnidadesSelecionadas)?'S':'N').'</td>';
        $strResultado .= '<td style="width: 15%">'.$arrObjColegiadoComposicaoDTO[$i]->getStrSiglaUnidade().'</td>';
        $strResultado .= '<td>'.$arrObjColegiadoComposicaoDTO[$i]->getStrDescricaoUnidade().'</td>';
        $strResultado .= '<td>'.$arrObjColegiadoComposicaoDTO[$i]->getStrNomeTipoMembroColegiado().'</td>';
        $strResultado .= '</tr>'."\n";
      }
      $strResultado .= '</table>';
    }
    return $strResultado;
  }
}
?>