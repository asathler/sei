<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 27/10/2014 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.33.1
 *
 * Vers�o no CVS: $Id$
 */

try {
  require_once __DIR__ . '/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
  $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
  $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();
  $objDistribuicaoRN = new DistribuicaoRN();

  $strTitulo = 'Incluir Processo em Sess�o de Julgamento';
  $arrIdDistribuicao = explode(',', $_GET['id_distribuicao']);
  $bolMultiplasDistribuicoes = count($arrIdDistribuicao)>1;


  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_distribuicao', 'id_procedimento_sessao', 'id_item_sessao_julgamento', 'arvore', 'id_colegiado'));

  $strParametros = '';
  $numIdColegiado = null;
  if (isset($_GET['arvore'])) {
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }

  if (isset($_POST['selSessao'])){
    $idSessaoJulgamento=$_POST['selSessao'];
  } else {
    $idSessaoJulgamento = $_GET['id_sessao_julgamento'];
  }
  $strParametros .= '&id_sessao_julgamento=' . $idSessaoJulgamento;

  if (isset($_GET['id_colegiado'])) {
    $numIdColegiado = $_GET['id_colegiado'];
  }

  $arrComandos = array();
  $idUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();

  switch ($_GET['acao']) {

    case 'sessao_julgamento_pautar': //vem da arvore

      $idDistribuicao = null;
      $objDistribuicaoDTO = new DistribuicaoDTO();
      if ($bolMultiplasDistribuicoes) {
        $objDistribuicaoDTO->setNumIdDistribuicao($arrIdDistribuicao, InfraDTO::$OPER_IN);
        $numIdColegiado = $_POST['selColegiado'] ?? null;
      } else {
        $idDistribuicao = $arrIdDistribuicao[0];
        $objDistribuicaoDTO->setNumIdDistribuicao($idDistribuicao);
      }
      $objDistribuicaoDTO->retStrStaDistribuicao();
      $objDistribuicaoDTO->retNumIdUnidadeRelator();
      $objDistribuicaoDTO->retNumIdDistribuicao();
      $objDistribuicaoDTO->retStrSiglaColegiado();
      $objDistribuicaoDTO->retStrNomeColegiado();
      $objDistribuicaoDTO->retNumIdColegiadoColegiadoVersao();
      $arrObjDistribuicaoDTO = $objDistribuicaoRN->listar($objDistribuicaoDTO);

      $arrEstadoDistribuicao = InfraArray::converterArrInfraDTO($objDistribuicaoRN->listarValoresEstadoDistribuicao(), 'Descricao', 'StaDistribuicao');

      foreach ($arrObjDistribuicaoDTO as $objDistribuicaoDTOBanco) {
        if (($numIdColegiado && $objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao()==$numIdColegiado) ||
            ($idDistribuicao && $objDistribuicaoDTOBanco->getNumIdDistribuicao()==$idDistribuicao)) {
          $idDistribuicao = $objDistribuicaoDTOBanco->getNumIdDistribuicao();
          $objDistribuicaoDTO = $objDistribuicaoDTOBanco;
        }
        $strDescricaoColegiado = $objDistribuicaoDTOBanco->getStrNomeColegiado();
        $strDescricaoColegiado .= ' (' . $objDistribuicaoDTOBanco->getStrSiglaColegiado() . ') - ';
        $staDistribuicao=$objDistribuicaoDTOBanco->getStrStaDistribuicao();
        $strDescricaoColegiado .= $arrEstadoDistribuicao[$staDistribuicao];
        $objDistribuicaoDTOBanco->setStrNomeColegiado($strDescricaoColegiado);
      }
      if ($idDistribuicao!=null) {
        $staDistribuicao = $objDistribuicaoDTO->getStrStaDistribuicao();

        if (isset($_POST['btnSalvar'])) {
          $arrStrIds = PaginaSEI::getInstance()->getArrStrItensSelecionados();
          $arrObjBloqueioItemSessUnidadeDTO = array();
          for ($i = 0, $iMax = count($arrStrIds); $i<$iMax; $i ++) {
            $objBloqueioItemSessUnidadeDTO = new BloqueioItemSessUnidadeDTO();
            $objBloqueioItemSessUnidadeDTO->setNumIdUnidade($arrStrIds[$i]);
            $arrObjBloqueioItemSessUnidadeDTO[] = $objBloqueioItemSessUnidadeDTO;
          }
          $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
          $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento(null);
          $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
          $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($idDistribuicao);
          $objItemSessaoJulgamentoDTO->setNumIdSessaoBloco($_POST['selSessaoBloco']);
          $objItemSessaoJulgamentoDTO->setStrDispositivo(null);
          $objItemSessaoJulgamentoDTO->setNumIdMotivoMesa($_POST['selMotivoMesa']);
          $objItemSessaoJulgamentoDTO->setStrStaSituacao(ItemSessaoJulgamentoRN::$STA_NORMAL);
          $objItemSessaoJulgamentoDTO->setArrObjBloqueioItemSessUnidadeDTO($arrObjBloqueioItemSessUnidadeDTO);
          try {
            $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->cadastrar($objItemSessaoJulgamentoDTO);
            PaginaSEI::getInstance()->adicionarMensagem('Pauta "' . $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento() . '" cadastrada com sucesso.');
            header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . PaginaSEI::getInstance()->getAcaoRetorno() . '&acao_origem=' . $_GET['acao'] . '&acao_retorno=arvore_visualizar&id_item_sessao_julgamento_julgamento=' . $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento() . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao() . '&arvore=1&resultado=1&atualizar_arvore=1' . PaginaSEI::getInstance()->montarAncora($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento())));
            die;
          } catch (Exception $e) {
            PaginaSEI::getInstance()->processarExcecao($e);
          }
        }
        $bolCadastrarItemSessao = in_array($staDistribuicao, array(DistribuicaoRN::$STA_DISTRIBUIDO, DistribuicaoRN::$STA_REDISTRIBUIDO, DistribuicaoRN::$STA_ADIADO, DistribuicaoRN::$STA_PEDIDO_VISTA, DistribuicaoRN::$STA_DILIGENCIA, DistribuicaoRN::$STA_RETIRADO_PAUTA));
        if (!$bolCadastrarItemSessao) {
          if($idSessaoJulgamento==null){
            $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
            $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
            $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($idDistribuicao);
            $objItemSessaoJulgamentoDTO->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_DESC);
            $objItemSessaoJulgamentoDTO->setNumMaxRegistrosRetorno(1);
            $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
            $idSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento();
          }
          header('Location: ' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_acompanhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=arvore_visualizar&id_sessao_julgamento=' . $idSessaoJulgamento));
          die;
        }
        $strItensSelMotivoMesa = MotivoMesaINT::montarSelectDescricao('null', '&nbsp;', $_POST['selMotivoMesa']);
      }
      break;


    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }

  $bolExibeTabelaBloqueio = false;
  if ($idSessaoJulgamento=='null') {
    $idSessaoJulgamento = null;
  }

  if ($idSessaoJulgamento!=null) {

    $objPesquisaSessaoJulgamentoDTO = new PesquisaSessaoJulgamentoDTO();
    $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
    $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');

    $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);

    if ($objSessaoJulgamentoDTO==null) {
      throw new InfraException('Sess�o de Julgamento n�o encontrada.');
    }

    $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(), 'IdUsuario');


    $numIdColegiado = $objSessaoJulgamentoDTO->getNumIdColegiado();
    $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();

    $bolUnidadePertenceColegiado = false;
    $idUsuarioPresidenteColegiado = $objSessaoJulgamentoDTO->getNumIdUsuarioPresidenteColegiado();
    $bolUnidadePresidente = false;

    foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
      if ($objColegiadoComposicaoDTO->getNumIdUnidade()==$idUnidadeAtual) {
        $bolUnidadePertenceColegiado = true;
        if ($objColegiadoComposicaoDTO->getNumIdUsuario()==$idUsuarioPresidenteColegiado) {
          $bolUnidadePresidente = true;
        }
        break;
      }
    }

    if (!$bolUnidadePertenceColegiado && $idUnidadeAtual!=$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()) {
      throw new InfraException('Usu�rio n�o pertence ao colegiado desta sess�o.');
    }

    $strItensSessaoBloco=SessaoBlocoINT::montarSelectInclusaoSessao($idSessaoJulgamento,$staDistribuicao,$bolUnidadePresidente);

    $arrComandos[] = '<button type="submit" accesskey="" id="btnSalvar" name="btnSalvar" value="Salvar" class="infraButton">Salvar</button>';
    if ($idUnidadeAtual==$objSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado() || $idUnidadeAtual==$objDistribuicaoDTO->getNumIdUnidadeRelator()) {

      $objBloqueioItemSessUnidadeDTO = new BloqueioItemSessUnidadeDTO();
      $objBloqueioItemSessUnidadeRN = new BloqueioItemSessUnidadeRN();
      $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($idDistribuicao);
      $objBloqueioItemSessUnidadeDTO->retTodos();
      $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);
      $arrObjBloqueioItemSessUnidadeDTO = $objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO);
      $arrIdUnidadesSelecionadas = InfraArray::converterArrInfraDTO($arrObjBloqueioItemSessUnidadeDTO, 'IdUnidade');
      $bolExibeTabelaBloqueio = true;
      $arrIdUnidadesBloqueio = array();
      $arrIdUnidadesBloqueio[] = $idUnidadeAtual;
      $arrIdUnidadesBloqueio[] = $objDistribuicaoDTO->getNumIdUnidadeRelator();
      $arrIdUnidadesBloqueio = array_unique($arrIdUnidadesBloqueio);
      $strTabelaUnidadesBloqueio = ColegiadoComposicaoINT::montarTabelaUnidades($numIdColegiado, $arrIdUnidadesBloqueio, $arrIdUnidadesSelecionadas);

    }
  }

  //montar arr colegiados para disponibiliza��o/consulta

  if ($bolMultiplasDistribuicoes) {
    $strItensSelColegiado = InfraINT::montarSelectArrInfraDTO('null', '&nbsp;', $numIdColegiado, $arrObjDistribuicaoDTO, 'IdColegiadoColegiadoVersao', 'NomeColegiado');
  } else {
    $strItensSelColegiado = InfraINT::montarSelectArrInfraDTO(null, '', $numIdColegiado, $arrObjDistribuicaoDTO, 'IdColegiadoColegiadoVersao', 'NomeColegiado');
  }


  $strLinkAjaxSessao = SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=sessao_julgamento');


} catch (Exception $e) {
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema() . ' - ' . $strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}?>

      #selColegiado { width: 60%; margin-top: 2px; }

      #lblSessao {position:absolute;width:23%;display: none}
      #selSessao {position:absolute;top:40%;width:23%;display: none}

      #lblSessaoBloco {position: absolute;left: 25%;width: 20%;}
      #selSessaoBloco {position: absolute;left: 25%;top: 40%;width: 20%;}

      #lblMotivoMesa {position: absolute;left: 47%;width: 40%;}
      #selMotivoMesa {position: absolute;left: 47%;top: 40%;width: 40%;}

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
  var objAjaxSessao;

  function inicializar() {

    infraOcultarMenuSistemaEsquema();
    objAjaxSessao = new infraAjaxMontarSelectDependente('selColegiado', 'selSessao', '<?=$strLinkAjaxSessao?>');
    objAjaxSessao.mostrarAviso = false;
    objAjaxSessao.tempoAviso = 1000;
    objAjaxSessao.prepararExecucao = function () {
      var ret = 'IdColegiado=' + document.getElementById('selColegiado').value;
      ret = ret + '&IdSessaoJulgamento=<?=$idSessaoJulgamento?>';
      ret = ret + '&BolInclusao=true';
      return ret;
    };

    if(document.getElementById('selColegiado').value!='null'){
      $('#lblSessao').show();
      $('#selSessao').show();
      objAjaxSessao.executar();
    }

    <? if(!$bolMultiplasDistribuicoes){?>
    document.getElementById('selColegiado').disabled = true;
    <?}?>
    $('label.infraLabelTitulo img').css('visibility', '');


    infraEfeitoTabelas();

    document.getElementById('selSessao').disabled = false;
    var selSessaoBloco = document.getElementById('selSessaoBloco');
    if (selSessaoBloco) {
      selSessaoBloco.disabled = false;
      exibeMotivoMesa(selSessaoBloco);
    }
  }


  function onSubmitForm() {
    return true;
  }

  function exibeMotivoMesa(objSelect) {
    if ($(objSelect).find(':selected').attr('statipoitem')=='<?=TipoSessaoBlocoRN::$STA_MESA?>') {
      $('#selMotivoMesa').show().attr('disabled', null);
      $('#lblMotivoMesa').show();
    } else {
      $('#selMotivoMesa').hide().attr('disabled', 'disabled');
      $('#lblMotivoMesa').hide();
    }
  }

<?if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo, 'onload="inicializar();"');
?>
  <form id="frmItemSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?= SessaoSEI::getInstance()->assinarLink('controlador.php?acao=' . $_GET['acao'] . '&acao_origem=' . $_GET['acao'] . $strParametros) ?>">
    <?
    PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
    //PaginaSEI::getInstance()->montarAreaValidacao();
    PaginaSEI::getInstance()->abrirAreaDados('5em');
    ?>

    <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
    <select id="selColegiado" name="selColegiado" onchange="this.form.submit();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
      <?= $strItensSelColegiado ?>
    </select>
    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    PaginaSEI::getInstance()->abrirAreaDados('5em');
?>

    <label id="lblSessao" for="selSessao" accesskey="" class="infraLabelObrigatorio">Data:</label>
    <select id="selSessao" name="selSessao" onchange="this.form.submit();" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"> </select>
    <? if ($idSessaoJulgamento!=null) {
      ?>
      <label id="lblSessaoBloco" for="selSessaoBloco" accesskey="" class="infraLabelObrigatorio">Tipo:</label>
      <select id="selSessaoBloco" name="selSessaoBloco" class="infraSelect" onchange="exibeMotivoMesa(this)" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
        <?= $strItensSessaoBloco ?>
      </select><label id="lblMotivoMesa" for="selMotivoMesa" accesskey="" class="infraLabelObrigatorio">Motivo:</label>
      <select id="selMotivoMesa" name="selMotivoMesa" class="infraSelect" tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>">
        <?= $strItensSelMotivoMesa ?>
      </select>
      <input type="hidden" id="hdnIdSessaoJulgamento" name="hdnIdSessaoJulgamento" value="<?= $objSessaoJulgamentoDTO->getNumIdSessaoJulgamento() ?>"/>
      <?
    }
    PaginaSEI::getInstance()->fecharAreaDados();
    if ($bolExibeTabelaBloqueio) {
      PaginaSEI::getInstance()->AbrirAreaDados(null,'style="margin-top:1em"');
      ?>
      <label id="lblTabelaBloqueio" for="" accesskey="" class="infraLabelOpcional">Bloquear acesso �s seguintes unidades:</label>
      <?
      PaginaSEI::getInstance()->montarAreaTabela($strTabelaUnidadesBloqueio, 1);
      PaginaSEI::getInstance()->fecharAreaDados();
    }
    //PaginaSEI::getInstance()->montarAreaDebug();
    //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
    ?>
  </form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>