<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 15/05/2020 - criado por mga
*
* Vers�o do Gerador de C�digo: 1.42.0
*/

require_once __DIR__.'/../../../../SEI.php';

class VotoEleicaoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return 'voto_eleicao';
  }

  public function montar() {

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdVotoEleicao', 'id_voto_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdEleicao', 'id_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'IdUsuario', 'id_usuario');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_DTH, 'VotoEleicao', 'dth_voto_eleicao');

    $this->adicionarAtributoTabela(InfraDTO::$PREFIXO_NUM, 'Ordem', 'ordem');

    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'IdentificacaoEleicao','identificacao','eleicao');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'SiglaUsuario','sigla','usuario');
    $this->adicionarAtributoTabelaRelacionada(InfraDTO::$PREFIXO_STR,'NomeUsuario','nome','usuario');

    $this->configurarPK('IdVotoEleicao',InfraDTO::$TIPO_PK_NATIVA);

    $this->configurarFK('IdEleicao', 'eleicao', 'id_eleicao');
    $this->configurarFK('IdUsuario', 'usuario', 'id_usuario');
  }
}
