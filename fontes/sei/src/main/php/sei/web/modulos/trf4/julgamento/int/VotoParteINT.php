<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/01/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class VotoParteINT extends InfraINT {

  public static function montarSelectIdUsuario($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdJulgamentoParte='', $numIdVotoParteAssociado='', $numIdUsuario=''){
    $objVotoParteDTO = new VotoParteDTO();
    $objVotoParteDTO->retNumIdVotoParte();
    $objVotoParteDTO->retNumIdUsuario();

    if ($numIdJulgamentoParte!==''){
      $objVotoParteDTO->setNumIdJulgamentoParte($numIdJulgamentoParte);
    }

    if ($numIdVotoParteAssociado!==''){
      $objVotoParteDTO->setNumIdVotoParteAssociado($numIdVotoParteAssociado);
    }

    if ($numIdUsuario!==''){
      $objVotoParteDTO->setNumIdUsuario($numIdUsuario);
    }

    $objVotoParteDTO->setOrdNumIdUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objVotoParteRN = new VotoParteRN();
    $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjVotoParteDTO, 'IdVotoParte', 'IdUsuario');
  }

  public static function montarSelectStaVotoParte($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $bolMultiplo=false, $bolPedidoVista=false){
    $objVotoParteRN = new VotoParteRN();
    $arrObjEstadoVotoParteDTO = $objVotoParteRN->listarValoresVotoParte();
    if($bolMultiplo){
      foreach ($arrObjEstadoVotoParteDTO as $chave=>$objEstadoVotoParteDTO) {
        switch($objEstadoVotoParteDTO->getStrStaVotoParte()){
          case VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO:
          case VotoParteRN::$STA_DIVERGE:
          case VotoParteRN::$STA_PEDIDO_VISTA:
            unset($arrObjEstadoVotoParteDTO[$chave]);
        }
      }
    }
    if(!$bolPedidoVista){
      foreach ($arrObjEstadoVotoParteDTO as $chave=>$objEstadoVotoParteDTO) {
        switch($objEstadoVotoParteDTO->getStrStaVotoParte()){
          case VotoParteRN::$STA_AGUARDA_VISTA:
            unset($arrObjEstadoVotoParteDTO[$chave]);
        }
      }
    }
    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjEstadoVotoParteDTO, 'StaVotoParte', 'Descricao');
  }
  public static function montarSelectStaVotoParteReferendo($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado)
  {
    $objVotoParteRN = new VotoParteRN();
    $arrObjEstadoVotoParteDTO = $objVotoParteRN->listarValoresVotoParte();
    foreach ($arrObjEstadoVotoParteDTO as $chave => $objEstadoVotoParteDTO) {
      switch ($objEstadoVotoParteDTO->getStrStaVotoParte()) {
        case VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO:
        case VotoParteRN::$STA_PEDIDO_VISTA:
        case VotoParteRN::$STA_AGUARDA_VISTA:
          unset($arrObjEstadoVotoParteDTO[$chave]);
      }
    }
    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjEstadoVotoParteDTO, 'StaVotoParte', 'Descricao');
  }
}
?>