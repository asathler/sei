<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 12/12/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__ . '/../../../../SEI.php';

class AndamentoSessaoRN extends InfraRN {

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco(){
    return BancoSEI::getInstance();
  }

  private function validarNumIdTarefaSessao(AndamentoSessaoDTO $objAndamentoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAndamentoSessaoDTO->getNumIdTarefaSessao())){
      $objInfraException->adicionarValidacao('Tarefa n�o informada.');
    }
  }
  private function validarNumIdSessaoJulgamento(AndamentoSessaoDTO $objAndamentoSessaoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objAndamentoSessaoDTO->getNumIdSessaoJulgamento())){
      $objInfraException->adicionarValidacao('Sess�o de Julgamento n�o informada.');
    }
  }

  protected function lancarControlado(AndamentoSessaoDTO $objAndamentoSessaoDTO) {
    try{

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('andamento_sessao_lancar',__METHOD__,$objAndamentoSessaoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdTarefaSessao($objAndamentoSessaoDTO, $objInfraException);
      $this->validarNumIdSessaoJulgamento($objAndamentoSessaoDTO, $objInfraException);

      $objInfraException->lancarValidacoes();

      $objAndamentoSessaoDTO->setDthExecucao(InfraData::getStrDataHoraAtual());
      $objAndamentoSessaoDTO->setNumIdUsuario(SessaoSEI::getInstance()->getNumIdUsuario());
      $objAndamentoSessaoDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());

      $objAndamentoSessaoBD = new AndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAndamentoSessaoBD->cadastrar($objAndamentoSessaoDTO);


      if ($objAndamentoSessaoDTO->isSetArrObjAtributoAndamentoSessaoDTO()){

        $objAtributoAndamentoSessaoRN = new AtributoAndamentoSessaoRN();

        foreach ($objAndamentoSessaoDTO->getArrObjAtributoAndamentoSessaoDTO() as $objAtributoAndamentoSessaoDTO){
          $objAtributoAndamentoSessaoDTO->setNumIdAndamentoSessao($ret->getNumIdAndamentoSessao());
          $objAtributoAndamentoSessaoRN->cadastrar($objAtributoAndamentoSessaoDTO);
        }
      }

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro cadastrando Andamento da Sess�o.',$e);
    }
  }
  protected function excluirControlado($arrObjAndamentoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('andamento_sessao_excluir',__METHOD__,$arrObjAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();
      $objAtributoAndamentoSessaoRN=new AtributoAndamentoSessaoRN();
      $objAndamentoSessaoBD = new AndamentoSessaoBD($this->getObjInfraIBanco());
      for($i=0;$i<InfraArray::contar($arrObjAndamentoSessaoDTO);$i++){
        $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
        $objAtributoAndamentoSessaoDTO->setNumIdAndamentoSessao($arrObjAndamentoSessaoDTO[$i]->getNumIdAndamentoSessao());
        $objAtributoAndamentoSessaoDTO->retNumIdAtributoAndamentoSessao();
        $arrObjAtributoAndamentoSessaoDTO=$objAtributoAndamentoSessaoRN->listar($objAtributoAndamentoSessaoDTO);
        if($arrObjAtributoAndamentoSessaoDTO){
          $objAtributoAndamentoSessaoRN->excluir($arrObjAtributoAndamentoSessaoDTO);
        }
        $objAndamentoSessaoBD->excluir($arrObjAndamentoSessaoDTO[$i]);
      }

      //Auditoria

    }catch(Exception $e){
      throw new InfraException('Erro excluindo Andamento da Sess�o.',$e);
    }
  }
  protected function consultarConectado(AndamentoSessaoDTO $objAndamentoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('andamento_sessao_consultar',__METHOD__,$objAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAndamentoSessaoBD = new AndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAndamentoSessaoBD->consultar($objAndamentoSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro consultando Andamento da Sess�o.',$e);
    }
  }
  protected function listarConectado(AndamentoSessaoDTO $objAndamentoSessaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('andamento_sessao_listar',__METHOD__,$objAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAndamentoSessaoBD = new AndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAndamentoSessaoBD->listar($objAndamentoSessaoDTO);

      //Auditoria

      return $ret;

    }catch(Exception $e){
      throw new InfraException('Erro listando Andamentos da Sess�o.',$e);
    }
  }
  protected function consultarHistoricoConectado(AndamentoSessaoDTO $objAndamentoSessaoDTO) {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('andamento_sessao_listar',__METHOD__,$objAndamentoSessaoDTO);

      $arrObjAndamentoSessaoDTO=InfraArray::indexarArrInfraDTO($this->listarConectado($objAndamentoSessaoDTO),'IdAndamentoSessao');

      $objAtributoAndamentoSessaoDTO=new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoRN=new AtributoAndamentoSessaoRN();
      $objAtributoAndamentoSessaoDTO->setNumIdAndamentoSessao(InfraArray::converterArrInfraDTO($arrObjAndamentoSessaoDTO,'IdAndamentoSessao'),InfraDTO::$OPER_IN);
      $objAtributoAndamentoSessaoDTO->retStrValor();
      $objAtributoAndamentoSessaoDTO->retStrChave();
      $objAtributoAndamentoSessaoDTO->retNumIdAndamentoSessao();

      $arrObjAtributoAndamentoSessaoDTO=$objAtributoAndamentoSessaoRN->listar($objAtributoAndamentoSessaoDTO);

      foreach ($arrObjAtributoAndamentoSessaoDTO as $objAtributoAndamentoSessaoDTO) {
        $idAndamento=$objAtributoAndamentoSessaoDTO->getNumIdAndamentoSessao();
        $nomeTarefa=$arrObjAndamentoSessaoDTO[$idAndamento]->getStrNomeTarefaSessao();
        $arrObjAndamentoSessaoDTO[$idAndamento]->setStrNomeTarefaSessao(str_replace('@'.$objAtributoAndamentoSessaoDTO->getStrChave().'@',$objAtributoAndamentoSessaoDTO->getStrValor(),$nomeTarefa));
      }


      return array_values($arrObjAndamentoSessaoDTO);

    }catch(Exception $e){
      throw new InfraException('Erro listando Hist�rico da Sess�o.',$e);
    }
  }
  protected function contarConectado(AndamentoSessaoDTO $objAndamentoSessaoDTO){
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('andamento_sessao_listar',__METHOD__,$objAndamentoSessaoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objAndamentoSessaoBD = new AndamentoSessaoBD($this->getObjInfraIBanco());
      $ret = $objAndamentoSessaoBD->contar($objAndamentoSessaoDTO);

      //Auditoria

      return $ret;
    }catch(Exception $e){
      throw new InfraException('Erro contando Andamentos da Sess�o.',$e);
    }
  }

}
?>