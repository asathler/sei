<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 17/07/2014 - criado por bcu
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  //PaginaSEI::getInstance()->salvarCamposPost(array(''));

  //PaginaSEI::getInstance()->setTipoSelecao(InfraPagina::$TIPO_SELECAO_MULTIPLA);

  $arrComandos = array();


  //Filtrar par�metros
  $strParametros = '';
	$mensagem='';

	$numIdColegiado = $_POST['selColegiado'];
	if ($numIdColegiado=='null') {
    $numIdColegiado = null;
  }


  switch($_GET['acao']){

    case 'distribuicao_simular':

   	  $strTitulo = 'Simular Distribui��o de Processos';

   	  $arrComandos = array();

 	  	$strItensSelColegiado = ColegiadoINT::montarSelectNome('null','&nbsp;',$numIdColegiado);
			$strItensSelAlgoritmo = AlgoritmoINT::montarSelectStaAlgoritmo(null,null,$_POST['selAlgoritmo']);

      $arrComandos[] = '<button type="submit" accesskey="D" name="sbmDistribuir" id="sbmDistribuir" value="Distribuir" class="infraButton"><span class="infraTeclaAtalho">D</span>istribuir</button>';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&resultado=0'.$strParametros).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';

      if (isset($_POST['sbmDistribuir'])){
     	  try{

					if ($numIdColegiado!=null){
						$objColegiadoDTO=new ColegiadoDTO();
						$objColegiadoRN=new ColegiadoRN();
						$objColegiadoDTO->setNumIdColegiado($numIdColegiado);
						$objColegiadoDTO=$objColegiadoRN->consultarCompleto($objColegiadoDTO);
						if ($objColegiadoDTO==null){
							throw new InfraException("Colegiado n�o encontrado.");
						}
						$arrObjColegiadoComposicaoDTO=$objColegiadoDTO->getArrObjColegiadoComposicaoDTO();
					} else {
						$arrObjColegiadoComposicaoDTO=array();
					}

					$numMembros = InfraArray::contar($arrObjColegiadoComposicaoDTO);

					if ($numMembros){

            $staAlgoritmo=$_POST['selAlgoritmo'];
            $arrObjDistribuicaoDTO=array();
            switch($staAlgoritmo){
              case AlgoritmoRN::$ALG_PESO:
              case AlgoritmoRN::$ALG_RODADA:
                $objDistribuicaoDTO=new DistribuicaoDTO();
                $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
                $objDistribuicaoDTO->setArrObjImpedimentoDTO(array());
                $objDistribuicaoDTO->setNumQuantidade($_POST['txtQuantidade']);
                $objDistribuicaoDTO->setStrStaAlgoritmoDistribuicao($staAlgoritmo);
                $objDistribuicaoDTO->setStrSinPrevencao('N');
                $arrObjDistribuicaoDTO[]=$objDistribuicaoDTO;
                break;

              case AlgoritmoRN::$ALG_RODADA_TIPO:
                $objTipoProcedimentoDTO=new TipoProcedimentoDTO();
                $objTipoProcedimentoDTO->retNumIdTipoProcedimento();
                $objTipoProcedimentoDTO->setNumMaxRegistrosRetorno(10);
                $objTipoProcedimentoRN=new TipoProcedimentoRN();
                $arrObjTipoProcedimentoDTO=$objTipoProcedimentoRN->listarRN0244($objTipoProcedimentoDTO);
                foreach ($arrObjTipoProcedimentoDTO as $objTipoProcedimentoDTO) {
                  $objDistribuicaoDTO=new DistribuicaoDTO();
                  $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
                  $objDistribuicaoDTO->setArrObjImpedimentoDTO(array());
                  $objDistribuicaoDTO->setNumQuantidade($_POST['txtQuantidade']);
                  $objDistribuicaoDTO->setStrStaAlgoritmoDistribuicao($staAlgoritmo);
                  $objDistribuicaoDTO->setNumIdTipoProcedimento($objTipoProcedimentoDTO->getNumIdTipoProcedimento());
                  $objDistribuicaoDTO->setStrSinPrevencao('N');
                  $arrObjDistribuicaoDTO[]=$objDistribuicaoDTO;
                }
              break;
              case AlgoritmoRN::$ALG_RODADA_MATERIA:
                $objTipoMateriaDTO=new TipoMateriaDTO();
                $objTipoMateriaDTO->retNumIdTipoMateria();
                $objTipoMateriaDTO->setNumMaxRegistrosRetorno(10);
                $objTipoMateriaRN=new TipoMateriaRN();
                $arrObjTipoMateriaDTO=$objTipoMateriaRN->listar($objTipoMateriaDTO);
                if(InfraArray::contar($arrObjTipoMateriaDTO)==0){
                  $objInfraException=new InfraException();
                  $objInfraException->lancarValidacao('Nenhum tipo de mat�ria cadastrado.');
                }
                foreach ($arrObjTipoMateriaDTO as $objTipoMateriaDTO) {
                  $objDistribuicaoDTO=new DistribuicaoDTO();
                  $objDistribuicaoDTO->setNumIdColegiadoColegiadoVersao($numIdColegiado);
                  $objDistribuicaoDTO->setArrObjImpedimentoDTO(array());
                  $objDistribuicaoDTO->setNumQuantidade($_POST['txtQuantidade']);
                  $objDistribuicaoDTO->setStrStaAlgoritmoDistribuicao($staAlgoritmo);
                  $objDistribuicaoDTO->setNumIdTipoMateriaAutuacao($objTipoMateriaDTO->getNumIdTipoMateria());
                  $objDistribuicaoDTO->setStrSinPrevencao('N');
                  $arrObjDistribuicaoDTO[]=$objDistribuicaoDTO;
                }
                break;
            }

						//executa a simula��o


						$objAlgoritmoRN=new AlgoritmoRN();
						$arrResultado=$objAlgoritmoRN->simularDistribuicao($arrObjDistribuicaoDTO);


						$strResultado = '';
						$strSumarioTabela = 'Tabela de Membros do Colegiado.';
						$strCaptionTabela = 'Membros do Colegiado';

						$strResultado .= '<table width="99%" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
						$strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numMembros).'</caption>';
						$strResultado .= '<tr>';
						$strResultado .= '<th width="45%" class="infraTh">Nome</th>'."\n";

            switch($staAlgoritmo){
              case AlgoritmoRN::$ALG_PESO:
                $strResultado .= '<th class="infraTh" width="15%">Peso</th>'."\n";
                $strResultado .= '<th class="infraTh" width="15%">Quantidade</th>'."\n";
                break;
              case AlgoritmoRN::$ALG_RODADA:
                $strResultado .= '<th class="infraTh" width="15%">Quantidade</th>'."\n";
                break;

              case AlgoritmoRN::$ALG_RODADA_TIPO:
                $numTipoProcedimento=InfraArray::contar($arrObjTipoProcedimentoDTO);
                for($i=1;$i<=$numTipoProcedimento;$i++){
                  $strResultado .= '<th class="infraTh">Tipo '. ($i) .'</th>'."\n";
                }
                $strResultado .= '<th class="infraTh" width="15%">Total</th>'."\n";
                break;
              case AlgoritmoRN::$ALG_RODADA_MATERIA:
                $numTipoMateria=InfraArray::contar($arrObjTipoMateriaDTO);
                for($i=1;$i<=$numTipoMateria;$i++){
                  $strResultado .= '<th class="infraTh">Tipo '. ($i) .'</th>'."\n";
                }
                $strResultado .= '<th class="infraTh" width="15%">Total</th>'."\n";
                break;
            }


						$strResultado .= '</tr>'."\n";
						$strCssTr='';

						$arrItensSelecionados=PaginaSEI::getInstance()->getArrStrItensSelecionados();

						InfraArray::ordenarArrInfraDTO($arrObjColegiadoComposicaoDTO,'Ordem',InfraArray::$TIPO_ORDENACAO_ASC);

						for($i = 0;$i < $numMembros; $i++){

							$strCssTr = ($strCssTr=='<tr class="infraTrClara">')?'<tr class="infraTrEscura">':'<tr class="infraTrClara">';
							$strResultado .= $strCssTr;

							$idUsuario=$arrObjColegiadoComposicaoDTO[$i]->getNumIdUsuario();
							$strResultado .= '<td>'.PaginaSEI::tratarHTML($arrObjColegiadoComposicaoDTO[$i]->getStrNomeUsuario());
							if ($arrObjColegiadoComposicaoDTO[$i]->getNumIdTipoMembroColegiado()!=TipoMembroColegiadoRN::$TMC_TITULAR){
								$strResultado .= ' <b>('.PaginaSEI::tratarHTML($arrObjColegiadoComposicaoDTO[$i]->getStrNomeTipoMembroColegiado()).')</b>';
							}
							$strResultado .= '</td>';

              switch($staAlgoritmo) {
                case AlgoritmoRN::$ALG_PESO:
                  $strResultado .= '<td align="center">' . $arrObjColegiadoComposicaoDTO[$i]->getDblPeso() . '</td>';
                  $strResultado .= '<td align="center">';
                  if (isset($arrResultado[$idUsuario])) {
                    $qtd = $arrResultado[$idUsuario];
                    $strResultado .= $qtd;
                    if ($qtd > 0) {
                      $strResultado .= ' (' . InfraUtil::formatarDbl($qtd * 100 / $_POST['txtQuantidade'], 2) . '%)';
                    }
                  }
                  $strResultado .= '</td>' . "\n";
                  break;
                case AlgoritmoRN::$ALG_RODADA:
                  $strResultado .= '<td align="center">';
                  if (isset($arrResultado[$idUsuario])) {
                    $qtd = $arrResultado[$idUsuario];
                    $strResultado .= $qtd;
                    if ($qtd > 0) {
                      $strResultado .= ' (' . InfraUtil::formatarDbl($qtd * 100 / $_POST['txtQuantidade'], 2) . '%)';
                    }
                  }
                  $strResultado .= '</td>' . "\n";
                  break;

                case AlgoritmoRN::$ALG_RODADA_TIPO:
                  $total = 0;
                  foreach ($arrObjTipoProcedimentoDTO as $objTipoProcedimentoDTO) {
                    $idTipoProcedimento = $objTipoProcedimentoDTO->getNumIdTipoProcedimento();
                    $strResultado .= '<td align="center">';
                    if (isset($arrResultado[$idTipoProcedimento][$idUsuario])) {
                      $qtd = $arrResultado[$idTipoProcedimento][$idUsuario];
                      $strResultado .= $qtd;
                      if ($qtd > 0) {
                        $total += $qtd;
                        $strResultado .= ' (' . InfraUtil::formatarDbl($qtd * 100 / $_POST['txtQuantidade'], 2) . '%)';
                      }
                    }
                    $strResultado .= '</td>' . "\n";
                  }
                  $qtdTipoProcedimento = InfraArray::contar($arrObjTipoProcedimentoDTO);
                  $strResultado .= '<td align="center">';
                  $strResultado .= $total;
                  if ($total > 0) {
                    $strResultado .= ' (' . InfraUtil::formatarDbl($total * 100 / $_POST['txtQuantidade'] / $qtdTipoProcedimento, 2) . '%)';
                  }
                  $strResultado .= '</td>' . "\n";
                  break;
                case AlgoritmoRN::$ALG_RODADA_MATERIA:
                  $total = 0;
                  foreach ($arrObjTipoMateriaDTO as $objTipoMateriaDTO) {
                    $idTipoMateria = $objTipoMateriaDTO->getNumIdTipoMateria();
                    $strResultado .= '<td align="center">';
                    if (isset($arrResultado[$idTipoMateria][$idUsuario])) {
                      $qtd = $arrResultado[$idTipoMateria][$idUsuario];
                      $strResultado .= $qtd;
                      if ($qtd > 0) {
                        $total += $qtd;
                        $strResultado .= ' (' . InfraUtil::formatarDbl($qtd * 100 / $_POST['txtQuantidade'], 2) . '%)';
                      }
                    }
                    $strResultado .= '</td>' . "\n";
                  }
                  $qtdTipoMateria = InfraArray::contar($arrObjTipoMateriaDTO);
                  $strResultado .= '<td align="center">';
                  $strResultado .= $total;
                  if ($total > 0) {
                    $strResultado .= ' (' . InfraUtil::formatarDbl($total * 100 / $_POST['txtQuantidade'] / $qtdTipoMateria, 2) . '%)';
                  }
                  $strResultado .= '</td>' . "\n";
                  break;
              }

							$strResultado .= '</tr>'."\n";
						}
						$strResultado .= '</table>';
					}
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
	    }

      break;

    	default:
        throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblColegiado {position:absolute;left:0%;top:0%;width:40%;}
#selColegiado {position:absolute;left:0%;top:38%;width:40%;}

#lblAlgoritmo {position:absolute;left:43%;top:0%;width:35%;}
#selAlgoritmo {position:absolute;left:43%;top:38%;width:35%;}

#lblQuantidade {position:absolute;left:80%;top:0%;width:10%;}
#txtQuantidade {position:absolute;left:80%;top:38%;width:10%;}

.hidden{ display:none;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
//<script>

function inicializar(){
  document.getElementById('selColegiado').focus();
}

function onSubmitForm(){

  if (!infraSelectSelecionado(document.getElementById('selColegiado'))){
    alert('Selecione um Colegiado.');
    document.getElementById('selColegiado').focus();
    return false;
  }

  var qtd=document.getElementById('txtQuantidade');
	if (infraTrim(qtd.value)=='') {
	  alert('Informe a Quantidade.');
	  document.getElementById('txtQuantidade').focus();
	  return false;
	}
	return true;
}

//</script>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmEnviarNotificacao" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>" style="display:inline;">
<?
//PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <select id="selColegiado" name="selColegiado" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
  <?=$strItensSelColegiado;?>
  </select>
	<label id="lblAlgoritmo" for="selAlgoritmo" accesskey="" class="infraLabelObrigatorio">Algoritmo:</label>
	<select id="selAlgoritmo" name="selAlgoritmo"  class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
		<?=$strItensSelAlgoritmo;?>
	</select>
  <label id="lblQuantidade" for="txtQuantidade" accesskey="" class="infraLabelObrigatorio">Quantidade:</label>
  <input type="text" id="txtQuantidade" name="txtQuantidade" class="infraText" value="<?=PaginaSEI::tratarHTML($_POST['txtQuantidade']);?>" onkeypress="infraMascaraNumero(this,event,4);" maxlength="4" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

<?
  PaginaSEI::getInstance()->fecharAreaDados();
//PaginaSEI::getInstance()->montarAreaValidacao();
  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numMembros);

?>
</form>
<?
PaginaSEI::getInstance()->montarAreaDebug();
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>