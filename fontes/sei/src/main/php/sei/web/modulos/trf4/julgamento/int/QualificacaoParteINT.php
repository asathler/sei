<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 09/08/2017 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class QualificacaoParteINT extends InfraINT {

  public static function montarSelectDescricao($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objQualificacaoParteDTO = new QualificacaoParteDTO();
    $objQualificacaoParteDTO->retNumIdQualificacaoParte();
    $objQualificacaoParteDTO->retStrDescricao();

    if ($strValorItemSelecionado!=null){
      $objQualificacaoParteDTO->setBolExclusaoLogica(false);
      $objQualificacaoParteDTO->adicionarCriterio(array('SinAtivo','IdQualificacaoParte'),array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),array('S',$strValorItemSelecionado),InfraDTO::$OPER_LOGICO_OR);
    }

    $objQualificacaoParteDTO->setOrdStrDescricao(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objQualificacaoParteRN = new QualificacaoParteRN();
    $arrObjQualificacaoParteDTO = $objQualificacaoParteRN->listar($objQualificacaoParteDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjQualificacaoParteDTO, 'IdQualificacaoParte', 'Descricao');
  }
}
?>