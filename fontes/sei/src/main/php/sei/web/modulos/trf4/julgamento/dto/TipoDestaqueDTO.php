<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 01/08/2014 - criado por mga@trf4.gov.br
*
*/

require_once __DIR__ .'/../../../../SEI.php';

class TipoDestaqueDTO extends InfraDTO {

  public function getStrNomeTabela() {
    return null;
  }

  public function montar() {
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'StaTipo');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Descricao');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Icone');
    $this->adicionarAtributo(InfraDTO::$PREFIXO_STR,'Tooltip');
  }
}
?>