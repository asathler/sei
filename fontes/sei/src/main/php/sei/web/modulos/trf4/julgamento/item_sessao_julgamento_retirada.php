<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  ////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setTipoPagina(InfraPagina::$TIPO_PAGINA_SIMPLES);

  $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoRN = new ItemSessaoJulgamentoRN();

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_distribuicao','id_sessao_julgamento','id_procedimento_sessao','id_item_sessao_julgamento','id_distribuicao_sessao','id_colegiado','arvore','multiplo'));

  $strDesabilitar = '';
  //Filtrar par�metros
  $strParametros = '&id_procedimento='.$_GET['id_procedimento_sessao'];
  $bolPedidoVistaAnterior=false;
  $strTitulo='';


  $mensagem='';
  $arrComandos = array();


  switch($_GET['acao']){
    case 'item_sessao_julgamento_adiar':
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAdiarItemSessaoJulgamento" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strTitulo = 'Adiar Julgamento do Processo';

      $numIdItemSessaoJulgamento=$_GET['id_item_sessao_julgamento'];
      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTO->retNumIdUsuarioSessao();
      $objItemSessaoJulgamentoDTO->retNumIdUsuarioRelatorDistribuicao();
      $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTO->retStrStaDistribuicaoAnterior();
      $objItemSessaoJulgamentoDTO->retStrStaSituacao();
      $objItemSessaoJulgamentoDTO->retStrDispositivo();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
      $objItemSessaoJulgamentoDTO = $objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
      $strTitulo.= ' '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo();
      $numIdSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento();

      $arrMensagens=[];
      $staSituacaoItem=$objItemSessaoJulgamentoDTO->getStrStaSituacao();
      //*SE TIVER VOTO VISTA N�O PERMITE
      $objInfraExceprion=new InfraException();

      if($objItemSessaoJulgamentoDTO->getStrDispositivo()!=null){
        $arrMensagens[]='J� existe dispositivo gerado que ser� substitu�do ao salvar.';
      }
      $strItensSelEncaminhamento='<option value="null">&nbsp;</option>';
      $bolPedidoVistaAnterior = $objItemSessaoJulgamentoDTO->getStrStaDistribuicaoAnterior()==DistribuicaoRN::$STA_PEDIDO_VISTA;
      if($bolPedidoVistaAnterior){
        $strItensSelEncaminhamento.='<option value="R">Relator</option>';
        $arrObjColegiadoComposicaoDTO=CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($numIdSessaoJulgamento);
        $strNomeMembroVista=$arrObjColegiadoComposicaoDTO[$objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao()]->getStrNomeUsuario();
        $strItensSelEncaminhamento.='<option value="V">'.$strNomeMembroVista.' (Pedido de Vista)</option>';
      }

      $objVotoParteRN=new VotoParteRN();
      $objVotoParteDTO=new VotoParteDTO();
      $objVotoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
      $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO);
      $objVotoParteDTO->retNumIdUsuario();
      $objVotoParteDTO->retStrStaVotoParte();
      $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $arrObjVotoParteDTO=$objVotoParteRN->listar($objVotoParteDTO);

      if($arrObjVotoParteDTO!=null){
        $arrMensagens[]='Voto n�o foi confirmado e n�o ser� levado para a pr�xima sess�o.';
      }


      //*se tiver voto PROVIMENTO DISPONIBILIZADO (relator ou vista), pede confirma��o, retira documento e voto
      //*se era pedido vista, confirma se adiamento ir� para vista ou relator. se for vista e tiver voto j� lan�ado, ser� retirado
      if (isset($_POST['sbmAdiarItemSessaoJulgamento'])){
        try{
          $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
          if($bolPedidoVistaAnterior){
            $selEncaminhamento=$_POST['selEncaminhamento'];
            if($selEncaminhamento!='R' && $selEncaminhamento!='V'){
              $objInfraExceprion->lancarValidacao('Selecione para quem dever� ser encaminhado o processo.');
            }
            $objItemSessaoJulgamentoDTO->setStrComplemento($selEncaminhamento);
          }

          $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
          $objItemSessaoJulgamentoDTO->setStrMotivoRetirada($_POST['txaMotivo']);
          $objItemSessaoJulgamentoRN->adiar($objItemSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Processo adiado com sucesso.');
          $bolExecucaoOK=true;
          $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento']));
//          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_sessao_julgamento='.$_GET['id_sessao_julgamento'].$strParametros));
//          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }

      break;

    case 'item_sessao_julgamento_retirar':

      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmRetirarItemSessaoJulgamento" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrIds=[];

      if($_GET['multiplo']==1){
        $strTitulo = 'Retirar Processos da Sess�o';
        if (isset($_POST['hdnIdItens'])){
          $arrIds=explode(',',$_POST['hdnIdItens']);
        } else {
          $arrTabelasCheck=explode(',',$_POST['hdnInfraSelecoes']);
          $arrIds=array();
          foreach ($arrTabelasCheck as $tabela) {
            if ($tabela!=='Infra') $arrIds=array_merge($arrIds,PaginaSEI::getInstance()->getArrStrItensSelecionados($tabela));
          }
          $_POST['hdnIdItens']=implode(',',$arrIds);
        }

        if(isset($_POST['hdnProcedimentos'])){
          $arrProtocolosOrigem = PaginaSEI::getInstance()->getArrValuesSelect($_POST['hdnProcedimentos']);
          $objItemSessaoJulgamentoDTO->setDblIdProcedimentoDistribuicao($arrProtocolosOrigem,InfraDTO::$OPER_IN);
        }


        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($arrIds,InfraDTO::$OPER_IN);
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(array(ItemSessaoJulgamentoRN::$STA_RETIRADO,ItemSessaoJulgamentoRN::$STA_ADIADO),InfraDTO::$OPER_NOT_IN);
        $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
        $objItemSessaoJulgamentoDTO->retNumIdItemSessaoJulgamento();
        $arrObjItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->listar($objItemSessaoJulgamentoDTO);

        $arrIds=InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdItemSessaoJulgamento','IdProcedimentoDistribuicao');

        $strItensSelProcedimentos = ProcedimentoINT::conjuntoCompletoFormatadoRI0903(array_keys($arrIds));

        if(InfraArray::contar($arrIds)===0){
          throw new InfraException('Nenhum Item Selecionado ou j� retirados.');
        }
      } else {
        if(isset($_GET['id_distribuicao_sessao'])){
          $idDistribuicao=$_GET['id_distribuicao_sessao'];
        } else {
          $idDistribuicao=$_GET['id_distribuicao'];
        }
        $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($_GET['id_sessao_julgamento']);
        $objItemSessaoJulgamentoDTO->setNumIdDistribuicao($idDistribuicao);
        $objItemSessaoJulgamentoDTO->setStrStaSituacao(array(ItemSessaoJulgamentoRN::$STA_RETIRADO,ItemSessaoJulgamentoRN::$STA_ADIADO),InfraDTO::$OPER_NOT_IN);
        $objItemSessaoJulgamentoDTO->retTodos();
        $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
        $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
        $objItemSessaoJulgamentoDTO->retStrDescricaoSessaoBloco();
        $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);

        if($objItemSessaoJulgamentoDTO===null){
          throw new InfraException('O processo j� foi retirado da sess�o.');
        }

        $bolPossuiAndamento=$objItemSessaoJulgamentoDTO->getStrStaSituacao()!==ItemSessaoJulgamentoRN::$STA_NORMAL;

        //TODO: 206462 usar artigo da sessao_bloco
        $strTitulo = 'Retirar de '.$objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco();
        $strTitulo.= ' o Processo '.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo();
      }


      if (isset($_POST['sbmRetirarItemSessaoJulgamento'])) {
        try{
          $arrObjItemSessaoJulgamentoDTO=array();
          if($_GET['multiplo']==1){
            foreach ($arrIds as $idItemSessaoJulgamento) {
              $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
              $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($idItemSessaoJulgamento);
              $objItemSessaoJulgamentoDTO->setNumIdSessaoJulgamento($_GET['id_sessao_julgamento']);
              $objItemSessaoJulgamentoDTO->setStrMotivoRetirada($_POST['txaMotivo']);
              $arrObjItemSessaoJulgamentoDTO[]=$objItemSessaoJulgamentoDTO;
            }
          } else {
            $objItemSessaoJulgamentoDTO->setStrMotivoRetirada($_POST['txaMotivo']);
            $arrObjItemSessaoJulgamentoDTO[]=$objItemSessaoJulgamentoDTO;
          }

          $objItemSessaoJulgamentoRN->retirar($arrObjItemSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Item retirado com sucesso.');
          $bolExecucaoOK=true;
          $strLinkRetorno = SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento']));
          //header('Location: '.$strLinkRetorno);
          //die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  //
  if ($_GET['acao_origem']==='sessao_julgamento_acompanhar'){
    $strLinkRetorno=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_acompanhar&acao_origem='.$_GET['acao'].$strParametros);
    $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.$strLinkRetorno.'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
  } else {
    $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="fechar();" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
  }
  //location.href = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&id_procedimento='.$_GET['id_procedimento'].'&montar_visualizacao=1');

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>

#divInfraAreaTela {display:none;}

#divMotivo {height:10em;display:none;}
#lblMotivo {position:absolute;left:0%;top:0%;width:90%;}
#txaMotivo {position:absolute;left:0%;top:18%;width:90%;}

#divProcedimentos {height:10em;display:none;}
#lblProcedimentos {position:absolute;left:0%;top:0%;}
#selProcedimentos {position:absolute;left:0%;top:18%;width:89%;}
#imgExcluirProcedimentos {position:absolute;left:90%;top:18%;}

#selEncaminhamento {width:50%;}

  .lblAviso {display:block;width:95%;font-size:1.4em;color:red}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->adicionarJavaScript(MdJulgarIntegracao::DIRETORIO_MODULO.'/js/julgamento.js');
PaginaSEI::getInstance()->abrirJavaScript();
?>
//<script>
  var objLupaProcedimentos = null;
function fechar(){
  fechar_pagina('','about:blank');
}
function inicializar(){
<?if($bolExecucaoOK){ ?>
  fechar_pagina('<?=PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento'])?>','<?=$strLinkRetorno?>');
  return;
<?}else{?>

  document.getElementById('divInfraAreaTela').style.display = 'block';

  <?if($_GET['multiplo']==1){ ?>
  objLupaProcedimentos = new infraLupaSelect('selProcedimentos','hdnProcedimentos',null);
  <?}?>

  <?if($_GET['multiplo']==1){ ?>
    document.getElementById('divProcedimentos').style.display = 'block';
  <?}?>

  document.getElementById('divMotivo').style.display = 'block';

  document.getElementById('txaMotivo').focus();

  infraEfeitoTabelas();
  <?}?>
}

function validarCadastro() {
  <?if($_GET['multiplo']==1){ ?>
  if (infraTrim(document.getElementById('hdnProcedimentos').value)=='') {
    alert('Nenhum processo informado.');
    document.getElementById('selProcedimentos').focus();
    return false;
  }
  <?}?>
  if (infraTrim(document.getElementById('txaMotivo').value)=='') {
    alert('Informe o Motivo.');
    document.getElementById('txaMotivo').focus();
    return false;
  }
  var e=document.getElementById('selEncaminhamento');
  if(e && e.value=='null'){
    alert('Informe para quem ser� destinado o processo a ser adiado.');
    e.focus();
    return false;
  }
<? if ($bolPossuiAndamento) { ?>
    return confirm("ATEN��O!\n\nExistem votos registrados para este processo.\n\nConfirma retirada da pauta?");
<?} else {?>
  return true;
<?}?>
}

function onSubmitForm() {
  <?if($_GET['acao']=='item_sessao_julgamento_diligenciar'){ ?>
  return true;
  <?} else { ?>
  return validarCadastro();
  <?}?>
}

function finalizar() {

}
//</script>
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();" onunload="finalizar();"');
?>
<form id="frmItemSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
if($arrMensagens){
  foreach ($arrMensagens as $strMensagem) {
    echo "<label class='lblAviso'>$strMensagem</label>";
  }
}
if($bolPedidoVistaAnterior){
  ?>
  <div id="divEncaminhamento" class="infraAreaDados">
    <label id="lblEncaminhamento" for="selEncaminhamento" class="infraLabelObrigatorio">Encaminhar para:</label>
    <select id="selEncaminhamento" name="selEncaminhamento" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
      <?=$strItensSelEncaminhamento?>
    </select>
  </div>
  <?
}
  ?>
  <div id="divProcedimentos" class="infraAreaDados">
    <label id="lblProcedimentos" for="selProcedimentos" class="infraLabelObrigatorio">Processos:</label>
    <select id="selProcedimentos" name="selProcedimentos" size="4" class="infraSelect" multiple="multiple" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
      <?=$strItensSelProcedimentos?>
    </select>
    <img id="imgExcluirProcedimentos" onclick="objLupaProcedimentos.remover();" src="<?=PaginaSEI::getInstance()->getIconeRemover()?>" alt="Remover Processos Selecionados" title="Remover Processos Selecionados" class="infraImg" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  </div>

  <div id="divMotivo" class="infraAreaDados">
    <label id="lblMotivo" for="txaMotivo" accesskey="" class="infraLabelObrigatorio">Motivo:</label>
    <textarea id="txaMotivo" name="txaMotivo" class="infraTextarea" rows="3" onkeypress="return infraMascaraTexto(this,event,4000);" maxlength="4000"  tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"></textarea>
  </div>

  <input type="hidden" id="hdnIdItemSessaoJulgamento" name="hdnIdItemSessaoJulgamento" value=""/>
  <input type="hidden" id="hdnProcedimentos" name="hdnProcedimentos" value="<?= $_POST['hdnProcedimentos'] ?>"/>
  <input type="hidden" id="hdnIdItens" name="hdnIdItens" value="<?= $_POST['hdnIdItens']?>">
  <?
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>