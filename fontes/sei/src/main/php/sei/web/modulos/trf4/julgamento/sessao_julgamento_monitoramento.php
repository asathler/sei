<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/



try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();
//  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  PaginaSEI::getInstance()->setBolAutoRedimensionar(false);

  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
  $objDistribuicaoRN = new DistribuicaoRN();

  $strTitulo = 'Sess�o de Julgamento - MONITORAMENTO';

//  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_distribuicao','id_sessao_julgamento','id_procedimento_sessao','arvore','id_colegiado'));

  $strParametros='';

  if (isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }
  if (isset($_POST['selSessao'])){
    $idSessaoJulgamento=$_POST['selSessao'];
    $_GET['id_sessao_julgamento']=$idSessaoJulgamento;
  } else {
    $idSessaoJulgamento=$_GET['id_sessao_julgamento'];
  }

  $idColegiado = isset($_GET['id_colegiado']) ? $_GET['id_colegiado'] : null;



  $arrComandos = array();
  $arrComandosSessao=array();

  $idUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();
  $bolAdmin = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar');

  switch($_GET['acao']) {
    case 'sessao_julgamento_monitorar': //vem da lista de sess�es da COJAD
      break;

    default:
      throw new InfraException("A��o '" . $_GET['acao'] . "' n�o reconhecida.");
  }


  $strComandoFechar='';
//  if (PaginaSEI::getInstance()->getAcaoRetorno()=='sessao_julgamento_listar'){
    $strComandoFechar = '<button type="button" accesskey="" id="btnVoltar" name="btnVoltar" value="Voltar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&montar_visualizacao=1'.$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_sessao_julgamento'])).'\';" class="infraButton">Voltar</button>';
//  }


  if ($idSessaoJulgamento=='null') {
    $idSessaoJulgamento = null;
  }

  if ($idSessaoJulgamento!=null) {

    $objPesquisaSessaoJulgamentoDTO=new PesquisaSessaoJulgamentoDTO();
    $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
    $objPesquisaSessaoJulgamentoDTO->setStrSinAnotacao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinDestaque('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinItemSessao('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinVotos('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinPedidoVista('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinPresenca('S');
    $objPesquisaSessaoJulgamentoDTO->setStrSinDocumentos('S');

    $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);

    if ($objSessaoJulgamentoDTO == null) {
      throw new InfraException('Sess�o de Julgamento n�o encontrada.');
    }

    $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(),'IdUsuario');
    $arrObjItemSessaoJulgamentoDTO = $objSessaoJulgamentoDTO->getArrObjItemSessaoJulgamentoDTO();
    $arrObjItemSessaoJulgamentoDTO=InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO,'IdItemSessaoJulgamento');
    $numTotalItens=InfraArray::contar($arrObjItemSessaoJulgamentoDTO);

    $arrObjSituacaoSessaoDTO = $objSessaoJulgamentoRN->listarValoresSituacaoSessao();
    $arrObjSituacaoSessaoDTO = InfraArray::indexarArrInfraDTO($arrObjSituacaoSessaoDTO, 'StaSituacao');

    if($numTotalItens>0){
      $objJulgamentoParteDTO=new JulgamentoParteDTO();
      $objJulgamentoParteDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
      $objJulgamentoParteDTO->retTodos();
      $objJulgamentoParteRN=new JulgamentoParteRN();
      $arrObjJulgamentoParteDTO=$objJulgamentoParteRN->listar($objJulgamentoParteDTO);
      $arrObjJulgamentoParteDTO=InfraArray::indexarArrInfraDTO($arrObjJulgamentoParteDTO,'IdJulgamentoParte');

      $objVotoParteDTO=new VotoParteDTO();
      $objVotoParteDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
      $objVotoParteDTO->retTodos();
      $objVotoParteDTO->retStrConteudoProvimento();
      $objVotoParteDTO->retStrNomeUsuarioAssociado();
      $objVotoParteDTO->retStrNomeUsuario();
      $objVotoParteDTO->retNumIdUsuarioVotoParte();

      $objVotoParteRN=new VotoParteRN();
      $arrObjVotoParteDTO=$objVotoParteRN->listar($objVotoParteDTO);
      if($arrObjVotoParteDTO){
        $arrObjVotoParteDTO=InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO,'IdJulgamentoParte',true);
        foreach ($arrObjVotoParteDTO as $idJulgamentoParte=>$arrObjVotoParteDTO2) {
          $arrObjJulgamentoParteDTO[$idJulgamentoParte]->setArrObjVotoParteDTO($arrObjVotoParteDTO2);
        }
      }
      $arrObjJulgamentoParteDTO=InfraArray::indexarArrInfraDTO($arrObjJulgamentoParteDTO,'IdItemSessaoJulgamento',true);
      foreach ($arrObjJulgamentoParteDTO as $idItemSessaoJulgamento=>$arrObjJulgamentoParteDTO2) {
        $arrObjItemSessaoJulgamentoDTO[$idItemSessaoJulgamento]->setArrObjJulgamentoParteDTO($arrObjJulgamentoParteDTO2);
      }
    }

    $arrObjEstadoDistribuicaoDTO = $objDistribuicaoRN->listarValoresEstadoDistribuicao();
    $arrObjEstadoDistribuicaoDTO = InfraArray::indexarArrInfraDTO($arrObjEstadoDistribuicaoDTO, 'StaDistribuicao');



    $idColegiado=$objSessaoJulgamentoDTO->getNumIdColegiado();
    $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();
    $strSituacaoSessao = $arrObjSituacaoSessaoDTO[$staSessao]->getStrDescricao();
    $arrSituacaoItem=InfraArray::converterArrInfraDTO($objItemSessaoJulgamentoRN->listarValoresStaSituacao(),'Descricao','StaTipo');

    $bolExibeProcesso=($objSessaoJulgamentoDTO->getDblIdProcedimento()!=null);
    if ($bolExibeProcesso) {
      $strLinkProcesso = '<a id="ancProcesso" href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objSessaoJulgamentoDTO->getDblIdProcedimento()) . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" title="' . $objSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '">' . $objSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a>';
    }
    switch ($staSessao) {
      case SessaoJulgamentoRN::$ES_ABERTA:
      case SessaoJulgamentoRN::$ES_FINALIZADA:
        $strStyle = 'color:#000;background:#c6efce;';
        break;
      case SessaoJulgamentoRN::$ES_ENCERRADA:
        $strStyle = 'color:#000;background:#9fd8f5;';
        break;

      case SessaoJulgamentoRN::$ES_PAUTA_ABERTA:
        $strStyle = 'color:#000;background:#ffec9c;';
        break;
      case SessaoJulgamentoRN::$ES_PAUTA_FECHADA:
        if ($objSessaoJulgamentoDTO->getNumQuorum()>=$objSessaoJulgamentoDTO->getNumQuorumMinimoColegiado()){
          $strStyle = 'color:#000;background:#c6efce;';
        } else {
          $strStyle = 'color:#000;background:#ffc7ce;';
        }
        break;
      case SessaoJulgamentoRN::$ES_SUSPENSA:
        $strStyle = 'color:#000;background:#ffc7ce;';
        break;
      default:
        $strStyle = '';
    }

    //----------------------------- montar tabelas de membros

    $numRegistrosMembros = InfraArray::contar($arrObjColegiadoComposicaoDTO);
    if ($numRegistrosMembros > 0) {
      PaginaSEI::getInstance()->getThCheck('', 'Infra');

      $strResultadoMembros = '';
      $staSessao=$objSessaoJulgamentoDTO->getStrStaSituacao();


      $arrTitularesAusentes=array();
      $arrObjPresencaSessaoDTO = $objSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();
      $arrPresencaSessao = InfraArray::converterArrInfraDTO($arrObjPresencaSessaoDTO, 'Entrada', 'IdUsuario');

      if ($staSessao==SessaoJulgamentoRN::$ES_ENCERRADA & $bolAdmin){
        foreach ($arrObjColegiadoComposicaoDTO as $id => $objColegiadoComposicaoDTO) {
          if($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR && !isset($arrPresencaSessao[$objColegiadoComposicaoDTO->getNumIdUsuario()])){
            $arrTitularesAusentes[$id]=array('parcial'=>false,'motivo'=>null);
          }
        }
        if(InfraArray::contar($arrTitularesAusentes)>0){
          $objPresencaSessaoDTO=new PresencaSessaoDTO();
          $objPresencaSessaoRN=new PresencaSessaoRN();
          $objPresencaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
          $objPresencaSessaoDTO->setDthSaida($objSessaoJulgamentoDTO->getDthInicio(),InfraDTO::$OPER_MAIOR);
          $objPresencaSessaoDTO->setNumIdUsuario(array_keys($arrTitularesAusentes),InfraDTO::$OPER_IN);
          $objPresencaSessaoDTO->retNumIdUsuario();
          $objPresencaSessaoDTO->setDistinct(true);
          $arrObjPresencaSessaoDTO2=$objPresencaSessaoRN->listar($objPresencaSessaoDTO);
          if(InfraArray::contar($arrObjPresencaSessaoDTO2)>0){
            foreach ($arrObjPresencaSessaoDTO2 as $objPresencaSessaoDTO) {
              $arrTitularesAusentes[$objPresencaSessaoDTO->getNumIdUsuario()]['parcial']=true;
            }
          }
          $objAusenciaSessaoDTO=new AusenciaSessaoDTO();
          $objAusenciaSessaoRN=new AusenciaSessaoRN();
          $objAusenciaSessaoDTO->setNumIdSessaoJulgamento($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
          $objAusenciaSessaoDTO->setNumIdUsuario(array_keys($arrTitularesAusentes),InfraDTO::$OPER_IN);
          $objAusenciaSessaoDTO->retTodos();
          $objAusenciaSessaoDTO->retStrDescricaoMotivoAusencia();
          $arrObjAusenciaSessaoDTO=$objAusenciaSessaoRN->listar($objAusenciaSessaoDTO);
          foreach ($arrObjAusenciaSessaoDTO as $objAusenciaSessaoDTO) {
            $arrTitularesAusentes[$objAusenciaSessaoDTO->getNumIdUsuario()]['motivo']=$objAusenciaSessaoDTO->getStrDescricaoMotivoAusencia();
          }

        }
      }

      $strSumarioTabela = 'Tabela de Membros do Colegiado.';

      $strResultadoMembros .= '<table width="99%" class="infraTable" summary="' . $strSumarioTabela . '">' . "\n";
      $strResultadoMembros .= '<tr>';
      $strResultadoMembros .= '<th class="infraTh">Nome</th>' . "\n";
      $strResultadoMembros .= '<th class="infraTh">Id Usu�rio</th>' . "\n";
      $strResultadoMembros .= '<th class="infraTh">Sigla</th>' . "\n";
      $strResultadoMembros .= '<th class="infraTh">Presente</th>' . "\n";
      $strResultadoMembros .= '</tr>' . "\n";
      $strCssTr = '';
      $i = 0;
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        $presente = isset($arrPresencaSessao[$objColegiadoComposicaoDTO->getNumIdUsuario()]);
        $strCssTr = ($strCssTr == 'infraTrClara') ? 'infraTrEscura' : 'infraTrClara';
        $strResultadoMembros .= '<tr class="';
        $strResultadoMembros .= $strCssTr;
        $strResultadoMembros .= '">';

        $strResultadoMembros .= '<td>' . $objColegiadoComposicaoDTO->getStrNomeUsuario();
        if ($objColegiadoComposicaoDTO->getNumIdTipoMembroColegiado() == TipoMembroColegiadoRN::$TMC_SUPLENTE) {
          $strResultadoMembros .= ' (Suplente)';
        }
        //colocar img de presidente se for o caso:
        $bolPresidente = ($objColegiadoComposicaoDTO->getNumIdUsuario() == $objSessaoJulgamentoDTO->getNumIdUsuarioPresidente());
        if ($bolPresidente) {
          $strResultadoMembros .= ' <b>(Presidente)</b>';
        }
        $strResultadoMembros .= '</td>';
        $strResultadoMembros .= '<td align="center">'.$objColegiadoComposicaoDTO->getNumIdUsuario().'</td>';
        $strResultadoMembros .= '<td align="center">'.$objColegiadoComposicaoDTO->getStrSiglaUsuario().'</td>';

        $strResultadoMembros .= '<td align="center">' . ($presente ? $arrPresencaSessao[$objColegiadoComposicaoDTO->getNumIdUsuario()] : 'N�o');
        if (!$presente) {
          $balao = $arrTitularesAusentes[$objColegiadoComposicaoDTO->getNumIdUsuario()]['motivo'];
          $strResultadoMembros .= '<a id="ancAusencia' . $objColegiadoComposicaoDTO->getNumIdUsuario() . '"';
          if ($balao == null) {
            $strResultadoMembros .= ' style="display:none;"';
          }
          $strResultadoMembros .= ' href="javascript:void(0);" ' . PaginaSEI::montarTitleTooltip($balao) . '><img src="'.MdJulgarIcone::BALAO1.'" class="infraImg" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" /></a>&nbsp;';
        }

          $strResultadoMembros .= '</td>';

        $strResultadoMembros .= '</tr>' . "\n";
      }
      $strResultadoMembros .= '</table>';
    }

    //----------------------------- montar tabelas de itens

    if($numTotalItens>0) {

      $arrIdDocumentos = array();
      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
        $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
        if (is_array($arrObjItemSessaoDocumentoDTO)) {
          foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
            $arrIdDocumentos[] = $objItemSessaoDocumentoDTO->getDblIdDocumento();
          }
        }
      }
      if (InfraArray::contar($arrIdDocumentos)) {
        $objPesquisaProtocoloDTO = new PesquisaProtocoloDTO();
        $objPesquisaProtocoloDTO->setStrStaTipo(ProtocoloRN::$TPP_DOCUMENTOS);
        $objPesquisaProtocoloDTO->setStrStaAcesso(ProtocoloRN::$TAP_AUTORIZADO);
        $objPesquisaProtocoloDTO->setDblIdProtocolo($arrIdDocumentos);

        $objProtocoloRN = new ProtocoloRN();
        $arrObjProtocoloDTO = $objProtocoloRN->pesquisarRN0967($objPesquisaProtocoloDTO);
        if ($arrObjProtocoloDTO != null) {
          $arrObjProtocoloDTO = InfraArray::indexarArrInfraDTO($arrObjProtocoloDTO, 'IdProtocolo');
        }
      }
      unset($arrIdDocumentos);

      $arrObjItemSessaoJulgamentoDTO = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'StaItemSessaoJulgamento', true);
      $arrPauta = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO[ItemSessaoJulgamentoRN::$SI_PAUTA], 'IdUsuarioRelatorDistribuicao', true);
      $arrMesa = InfraArray::indexarArrInfraDTO($arrObjItemSessaoJulgamentoDTO[ItemSessaoJulgamentoRN::$SI_MESA], 'IdUsuarioSessao', true);
      $arrReferendo = $arrObjItemSessaoJulgamentoDTO[ItemSessaoJulgamentoRN::$SI_REFERENDO];

      //pauta
      $numRegistrosPautaTotal = 0;
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        if (isset($arrPauta[$objColegiadoComposicaoDTO->getNumIdUsuario()])) {
          $bolUnidadeSessao = $objColegiadoComposicaoDTO->getNumIdUnidade() == $idUnidadeAtual;
          $arrObjItemSessaoJulgamentoDTO = $arrPauta[$objColegiadoComposicaoDTO->getNumIdUsuario()];
          $idTabela = ItemSessaoJulgamentoRN::$SI_PAUTA . $objColegiadoComposicaoDTO->getNumIdUsuario();
          $numOrdemSequencial = $numRegistrosPautaTotal;
          $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
          $bolOrdenarProcessos = false;
          if ($numRegistros > 1 && (($bolUnidadeSessao && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA))) ||
                  ($bolAdmin && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_ABERTA))))
          ) {
            $bolOrdenarProcessos = true;
          }


//montartabela
          $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
          if($numRegistros==0){
            return '';
          }
          $strResultado = '';

          $strCaption = 'Tabela de Pauta do Relator';
          $strResultado = '<label class="infraLabelObrigatorio">' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . '&nbsp;</label>';

          $strResultado .= '<table width="99%" id="' . $idTabela . '" class="infraTable" summary="' . $strCaption . '">' . "\n";
          $strResultado .= '<tr>';
          $strResultado .= '<th rowspan="2" class="infraTh" width="5%">Ordem</th>' . "\n";
          $strResultado .= '<th class="infraTh" colspan="2">Processo</th>' . "\n";
          $strResultado .= '<th class="infraTh">Documento</th>' . "\n";
          $strResultado .= '<th class="infraTh">Situa��o</th>' . "\n";
          $strResultado .= '</tr>' . "\n";
          $strResultado .= '<tr>';
          $strResultado .= '<th class="infraTh" width="30%">Dispositivo</th>' . "\n";
          $strResultado .= '<th class="infraTh">Id Item</th>' . "\n";
          $strResultado .= '<th class="infraTh">Id Distribui��o</th>' . "\n";
          $strResultado .= '<th class="infraTh">JulgamentoParte</th>' . "\n";
          $strResultado .= '</tr>' . "\n";

          $strCssTr = '';
          for ($i = 0; $i < $numRegistros; $i++) {
            $objItemSessaoJulgamentoDTO=$arrObjItemSessaoJulgamentoDTO[$i];
            $numIdItemSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();

            //TR
            $strCssTr = ($strCssTr == '<tr class="infraTrClara"') ? '<tr class="infraTrEscura"' : '<tr class="infraTrClara"';
            $strResultado .= $strCssTr;
            $strResultado .= ' id="trItem' . $numIdItemSessaoJulgamento . '">';

            //Ordem
            $strResultado .= '<td rowspan="2" align="center">' . ++$numOrdemSequencial . '</td>';

            //Processo
            $strResultado .= '<td align="center" colspan="2" style="white-space: nowrap">';
            $strClasseProcesso = 'protocoloNormal';
            if ($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo() == ProtocoloRN::$NA_SIGILOSO) {
              $strClasseProcesso = 'processoVisualizadoSigiloso';
            }
            $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
            $strResultado .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objItemSessaoJulgamentoDTO->getStrDescricaoProtocolo(), $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . ' class="' . $strClasseProcesso . '"  style="font-size:1em !important">' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a>';
            $strResultado .= '</td>';

            //Documentos
            $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
            $strDocumento = '';
            if ($arrObjItemSessaoDocumentoDTO == null) {
              $arrObjItemSessaoDocumentoDTO = array();
            }
            foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
              /* @var $objItemSessaoDocumentoDTO ItemSessaoDocumentoDTO */
              if ($strDocumento != '') {
                $strDocumento .= '<br />';
              }
              $strTooltip = PaginaSEI::montarTitleTooltip($objItemSessaoDocumentoDTO->getStrNomeSerie(), $objItemSessaoDocumentoDTO->getStrNomeUsuario());
              if (isset($arrObjProtocoloDTO[$objItemSessaoDocumentoDTO->getDblIdDocumento()])) {
                $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=documento_visualizar&id_documento=' . $objItemSessaoDocumentoDTO->getDblIdDocumento());
                $strDocumento .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal"  style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
              } else {
                $strDocumento .= '<a href="javascript:void(0);" onclick="alert(\'Sem acesso ao documento.\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
              }
            }
            $strResultado .= '<td align="center">' . $strDocumento . '</td>';

            //Situa��o
            $strResultado .= '<td align="center">';
            if ($objItemSessaoJulgamentoDTO->getStrStaSituacao() != ItemSessaoJulgamentoRN::$STA_NORMAL) {
              $strResultado .= $arrSituacaoItem[$objItemSessaoJulgamentoDTO->getStrStaSituacao()];
            } else {
              $strResultado .= $arrObjEstadoDistribuicaoDTO[$objItemSessaoJulgamentoDTO->getStrStaDistribuicao()]->getStrDescricao();
            }
            $strResultado .= '</td>';
            $strResultado .= '</tr>' . "\n";
            ///linha2
            $strResultado .= $strCssTr;
            $strResultado .= ' id="trItem' . $numIdItemSessaoJulgamento . '" data-sta-item="'.$objItemSessaoJulgamentoDTO->getStrStaSituacao().'">';
            //Dispositivo
            $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getStrDispositivo() . '</td>';
            //Id Item
            $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento() . '</td>';
            //Id Distribui��o
            $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getNumIdDistribuicao() . '</td>';
            //JulgamentoParte
            $strResultado .= '<td align="center">' ;

            $arrObjJulgamentoParteDTO=$objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
            if($arrObjJulgamentoParteDTO){
              $strResultado.='<table class="infraTable"><tbody>';
              foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
                $strResultado.='<tr class="infraTrClara"><td>';
                $strResultado.=$objJulgamentoParteDTO->getNumIdJulgamentoParte().' ';
                $strResultado.=$objJulgamentoParteDTO->getStrDescricao();
                $strResultado.='</td>';
                $strResultado.='<td>';
                //votos
                if($objJulgamentoParteDTO->isSetArrObjVotoParteDTO()){
                  $arrObjVotoParteDTO=$objJulgamentoParteDTO->getArrObjVotoParteDTO();
                  $br='';
                  foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
                    $strResultado.=$br.$objVotoParteDTO->getNumIdUsuario();
                    $strResultado.=' '.$objVotoParteDTO->getStrStaVotoParte();
                    $strResultado.=' '.$objVotoParteDTO->getStrConteudoProvimento();
                    $strResultado.=' '.$objVotoParteDTO->getNumIdUsuarioVotoParte();
                    $strResultado.=' ('.$objVotoParteDTO->getDthVoto().')';
                    $br='<br>';
                  }

                }
                $strResultado.='</td>';
              }
              $strResultado.='</tbody></table>';
            }
            $strResultado.='</td>';

          }
          $strResultado .= '</table><br />';








          $numRegistrosPautaTotal += $numRegistros;
          $arrTabelasPauta[] = array($strResultado, $numRegistros, $idTabela);
        }
      }

      //mesa
      $numRegistrosMesaTotal = 0;
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        if (isset($arrMesa[$objColegiadoComposicaoDTO->getNumIdUsuario()])) {
          $bolUnidadeSessao = $objColegiadoComposicaoDTO->getNumIdUnidade() == $idUnidadeAtual;
          $arrObjItemSessaoJulgamentoDTO = $arrMesa[$objColegiadoComposicaoDTO->getNumIdUsuario()];
          $idTabela = ItemSessaoJulgamentoRN::$SI_MESA . $objColegiadoComposicaoDTO->getNumIdUsuario();
          $numOrdemSequencial = $numRegistrosMesaTotal;
          $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
          $bolOrdenarProcessos = false;
          if ($numRegistros > 1 && (($bolUnidadeSessao && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA))) ||
                  ($bolAdmin && in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_ABERTA))))
          ) {
            $bolOrdenarProcessos = true;
          }
          $arrObjItemSessaoJulgamentoDTO[0]->setArrObjColegiadoComposicaoDTO(array($objColegiadoComposicaoDTO));






//montartabela
          $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
          if($numRegistros==0){
            return '';
          }
          $strResultado = '';

          $strCaption = 'Tabela de Pauta do Relator';
          $strResultado = '<label class="infraLabelObrigatorio">' . $objColegiadoComposicaoDTO->getStrNomeUsuario() . '&nbsp;</label>';

          $strResultado .= '<table width="99%" id="' . $idTabela . '" class="infraTable" summary="' . $strCaption . '">' . "\n";
          $strResultado .= '<tr>';
          $strResultado .= '<th rowspan="2" class="infraTh" width="5%">Ordem</th>' . "\n";
          $strResultado .= '<th class="infraTh">Processo</th>' . "\n";
          $strResultado .= '<th class="infraTh">Motivo</th>' . "\n";
          $strResultado .= '<th class="infraTh">Documento</th>' . "\n";
          $strResultado .= '<th class="infraTh">Situa��o</th>' . "\n";
          $strResultado .= '</tr>' . "\n";
          $strResultado .= '<tr>';
          $strResultado .= '<th class="infraTh" width="30%">Dispositivo</th>' . "\n";
          $strResultado .= '<th class="infraTh">Id Item</th>' . "\n";
          $strResultado .= '<th class="infraTh">Id Distribui��o</th>' . "\n";
          $strResultado .= '<th class="infraTh">JulgamentoParte</th>' . "\n";
          $strResultado .= '</tr>' . "\n";

          $strCssTr = '';
          for ($i = 0; $i < $numRegistros; $i++) {
            $objItemSessaoJulgamentoDTO=$arrObjItemSessaoJulgamentoDTO[$i];
            $numIdItemSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();

            //TR
            $strCssTr = ($strCssTr == '<tr class="infraTrClara"') ? '<tr class="infraTrEscura"' : '<tr class="infraTrClara"';
            $strResultado .= $strCssTr;
            $strResultado .= ' id="trItem' . $numIdItemSessaoJulgamento . '">';

            //Ordem
            $strResultado .= '<td  rowspan="2" align="center">' . ++$numOrdemSequencial . '</td>';

            //Processo
            $strResultado .= '<td align="center" style="white-space: nowrap">';
            $strClasseProcesso = 'protocoloNormal';
            if ($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo() == ProtocoloRN::$NA_SIGILOSO) {
              $strClasseProcesso = 'processoVisualizadoSigiloso';
            }
            $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
            $strResultado .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objItemSessaoJulgamentoDTO->getStrDescricaoProtocolo(), $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . ' class="' . $strClasseProcesso . '"  style="font-size:1em !important">' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a>';
            $strResultado .= '</td>';
            //Motivo
            $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getStrDescricaoMotivoMesa() . '</td>';

            //Documentos
            $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
            $strDocumento = '';
            if ($arrObjItemSessaoDocumentoDTO == null) {
              $arrObjItemSessaoDocumentoDTO = array();
            }
            foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
              /* @var $objItemSessaoDocumentoDTO ItemSessaoDocumentoDTO */
              if ($strDocumento != '') {
                $strDocumento .= '<br />';
              }
              $strTooltip = PaginaSEI::montarTitleTooltip($objItemSessaoDocumentoDTO->getStrNomeSerie(), $objItemSessaoDocumentoDTO->getStrNomeUsuario());
              if (isset($arrObjProtocoloDTO[$objItemSessaoDocumentoDTO->getDblIdDocumento()])) {
                $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=documento_visualizar&id_documento=' . $objItemSessaoDocumentoDTO->getDblIdDocumento());
                $strDocumento .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal"  style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
              } else {
                $strDocumento .= '<a href="javascript:void(0);" onclick="alert(\'Sem acesso ao documento.\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
              }
            }
            $strResultado .= '<td align="center">' . $strDocumento . '</td>';

            //Situa��o
            $strResultado .= '<td align="center">';
            if ($objItemSessaoJulgamentoDTO->getStrStaSituacao() != ItemSessaoJulgamentoRN::$STA_NORMAL) {
              $strResultado .= $arrSituacaoItem[$objItemSessaoJulgamentoDTO->getStrStaSituacao()];
            } else {
              $strResultado .= $arrObjEstadoDistribuicaoDTO[$objItemSessaoJulgamentoDTO->getStrStaDistribuicao()]->getStrDescricao();
            }
            $strResultado .= '</td>';
            $strResultado .= '</tr>' . "\n";
            ///linha2
            $strResultado .= $strCssTr;
            $strResultado .= ' id="trItem' . $numIdItemSessaoJulgamento . '" data-sta-item="'.$objItemSessaoJulgamentoDTO->getStrStaSituacao().'">';
            //Dispositivo
            $strResultado .= '<td align="center">';
            $strResultado.= ' '.$objItemSessaoJulgamentoDTO->getStrDispositivo();
            $strResultado .= '</td>';
            //Id Item
            $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento() . '</td>';
            //Id Distribui��o
            $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getNumIdDistribuicao() . '</td>';
            //JulgamentoParte
            $strResultado .= '<td align="center">' ;

            $arrObjJulgamentoParteDTO=$objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
            if($arrObjJulgamentoParteDTO){
              $strResultado.='<table class="infraTable"><tbody>';
              foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
                $strResultado.='<tr class="infraTrClara"><td>';
                $strResultado.=$objJulgamentoParteDTO->getNumIdJulgamentoParte().' ';
                $strResultado.=$objJulgamentoParteDTO->getStrDescricao();
                $strResultado.='</td>';
                $strResultado.='<td>';
                //votos
                if($objJulgamentoParteDTO->isSetArrObjVotoParteDTO()){
                  $arrObjVotoParteDTO=$objJulgamentoParteDTO->getArrObjVotoParteDTO();
                  $br='';
                  foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
                    $strResultado.=$br.$objVotoParteDTO->getNumIdUsuario();
                    $strResultado.=' '.$objVotoParteDTO->getStrStaVotoParte();
                    $strResultado.=' '.$objVotoParteDTO->getStrConteudoProvimento();
                    $strResultado.=' '.$objVotoParteDTO->getNumIdUsuarioVotoParte();
                    $strResultado.=' ('.$objVotoParteDTO->getDthVoto().')';
                    $br='<br>';
                  }

                }
                $strResultado.='</td>';
              }
              $strResultado.='</tbody></table>';
            }
            $strResultado.='</td>';

          }
          $strResultado .= '</table><br />';









          $numRegistrosMesaTotal += $numRegistros;
          $arrTabelasMesa[] = array($strResultado, $numRegistros, $idTabela);


        }
      }
      //referendos
      if (InfraArray::contar($arrReferendo) > 0) {
        $arrObjItemSessaoJulgamentoDTO = $arrReferendo;
        $numOrdemSequencial = 0;
        $numRegistrosReferendo = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);





        //montartabela
        $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamentoDTO);
        if($numRegistros==0){
          return '';
        }
        $strResultado = '';

        $strCaption = 'Tabela de Referendos';
        $strResultado .= '<table width="99%" id="' . $idTabela . '" class="infraTable" summary="' . $strCaption . '">' . "\n";
        $strResultado .= '<tr>';
        $strResultado .= '<th rowspan="2" class="infraTh" width="5%">Ordem</th>' . "\n";
        $strResultado .= '<th class="infraTh" colspan="2">Processo</th>' . "\n";
        $strResultado .= '<th class="infraTh">Documento</th>' . "\n";
        $strResultado .= '<th class="infraTh">Situa��o</th>' . "\n";
        $strResultado .= '</tr>' . "\n";
        $strResultado .= '<tr>';
        $strResultado .= '<th class="infraTh" width="30%">Dispositivo</th>' . "\n";
        $strResultado .= '<th class="infraTh">Id Item</th>' . "\n";
        $strResultado .= '<th class="infraTh">Id Distribui��o</th>' . "\n";
        $strResultado .= '<th class="infraTh">JulgamentoParte</th>' . "\n";
        $strResultado .= '</tr>' . "\n";

        $strCssTr = '';
        for ($i = 0; $i < $numRegistros; $i++) {
          $objItemSessaoJulgamentoDTO=$arrObjItemSessaoJulgamentoDTO[$i];
          $numIdItemSessaoJulgamento=$objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();

          //TR
          $strCssTr = ($strCssTr == '<tr class="infraTrClara"') ? '<tr class="infraTrEscura"' : '<tr class="infraTrClara"';
          $strResultado .= $strCssTr;
          $strResultado .= ' id="trItem' . $numIdItemSessaoJulgamento . '">';

          //Ordem
          $strResultado .= '<td rowspan="2" align="center">' . ++$numOrdemSequencial . '</td>';

          //Processo
          $strResultado .= '<td align="center" colspan="2" style="white-space: nowrap">';
          $strClasseProcesso = 'protocoloNormal';
          if ($objItemSessaoJulgamentoDTO->getStrStaNivelAcessoGlobalProtocolo() == ProtocoloRN::$NA_SIGILOSO) {
            $strClasseProcesso = 'processoVisualizadoSigiloso';
          }
          $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=procedimento_trabalhar&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_procedimento=' . $objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
          $strResultado .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . PaginaSEI::montarTitleTooltip($objItemSessaoJulgamentoDTO->getStrDescricaoProtocolo(), $objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . ' class="' . $strClasseProcesso . '"  style="font-size:1em !important">' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '</a>';
          $strResultado .= '</td>';

          //Documentos
          $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
          $strDocumento = '';
          if ($arrObjItemSessaoDocumentoDTO == null) {
            $arrObjItemSessaoDocumentoDTO = array();
          }
          foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
            /* @var $objItemSessaoDocumentoDTO ItemSessaoDocumentoDTO */
            if ($strDocumento != '') {
              $strDocumento .= '<br />';
            }
            $strTooltip = PaginaSEI::montarTitleTooltip($objItemSessaoDocumentoDTO->getStrNomeSerie(), $objItemSessaoDocumentoDTO->getStrNomeUsuario());
            if (isset($arrObjProtocoloDTO[$objItemSessaoDocumentoDTO->getDblIdDocumento()])) {
              $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=documento_visualizar&id_documento=' . $objItemSessaoDocumentoDTO->getDblIdDocumento());
              $strDocumento .= '<a href="' . $strLink . '" target="_blank" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal"  style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
            } else {
              $strDocumento .= '<a href="javascript:void(0);" onclick="alert(\'Sem acesso ao documento.\');" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" ' . $strTooltip . ' class="protocoloNormal" style="font-size:1em !important">' . $objItemSessaoDocumentoDTO->getStrProtocoloDocumentoFormatado() . '</a>';
            }
          }
          $strResultado .= '<td align="center">' . $strDocumento . '</td>';

          //Situa��o
          $strResultado .= '<td align="center">';
          if ($objItemSessaoJulgamentoDTO->getStrStaSituacao() != ItemSessaoJulgamentoRN::$STA_NORMAL) {
            $strResultado .= $arrSituacaoItem[$objItemSessaoJulgamentoDTO->getStrStaSituacao()];
          } else {
            $strResultado .= $arrObjEstadoDistribuicaoDTO[$objItemSessaoJulgamentoDTO->getStrStaDistribuicao()]->getStrDescricao();
          }
          $strResultado .= '</td>';
          $strResultado .= '</tr>' . "\n";
          ///linha2
          $strResultado .= $strCssTr;
          $strResultado .= ' id="trItem' . $numIdItemSessaoJulgamento . '" data-sta-item="'.$objItemSessaoJulgamentoDTO->getStrStaSituacao().'">';
          //Dispositivo
          $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getStrDispositivo() . '</td>';
          //Id Item
          $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento() . '</td>';
          //Id Distribui��o
          $strResultado .= '<td align="center">' . $objItemSessaoJulgamentoDTO->getNumIdDistribuicao() . '</td>';
          //JulgamentoParte
          $strResultado .= '<td align="center">' ;

          $arrObjJulgamentoParteDTO=$objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
          if($arrObjJulgamentoParteDTO){
            $strResultado.='<table class="infraTable"><tbody>';
            foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
              $strResultado.='<tr class="infraTrClara"><td>';
              $strResultado.=$objJulgamentoParteDTO->getNumIdJulgamentoParte().' ';
              $strResultado.=$objJulgamentoParteDTO->getStrDescricao();
              $strResultado.='</td>';
              $strResultado.='<td>';
              //votos
              if($objJulgamentoParteDTO->isSetArrObjVotoParteDTO()){
                $arrObjVotoParteDTO=$objJulgamentoParteDTO->getArrObjVotoParteDTO();
                $br='';
                foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
                  $strResultado.=$br.$objVotoParteDTO->getNumIdUsuario();
                  $strResultado.=' '.$objVotoParteDTO->getStrStaVotoParte();
                  $strResultado.=' '.$objVotoParteDTO->getStrConteudoProvimento();
                  $strResultado.=' '.$objVotoParteDTO->getNumIdUsuarioVotoParte();
                  $strResultado.=' ('.$objVotoParteDTO->getDthVoto().')';
                  $br='<br>';
                }

              }
              $strResultado.='</td>';
            }
            $strResultado.='</tbody></table>';
          }
          $strResultado.='</td>';

        }
        $strResultado .= '</table><br />';

        $strResultadoReferendo=$strResultado;







      } else {
        PaginaSEI::getInstance()->getThCheck('', 'ref');
      }

    }


    //----------------------------- fim das tabelas

    if ($staSessao != SessaoJulgamentoRN::$ES_FINALIZADA) {
      array_unshift($arrComandos, '<button type="submit" accesskey="" id="btnAtualizar" name="btnAtualizar" value="Atualizar" class="infraButton">Atualizar</button>');
    }
  }

  $arrComandos[]=$strComandoFechar;

  $strItensSelColegiado=ColegiadoINT::montarSelectNomePermitidos(null,'',$idColegiado);
  $strLinkAjaxSessao=SessaoSEI::getInstance()->assinarLink('controlador_ajax.php?acao_ajax=sessao_julgamento');


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style type="text/css" ><?}
?>


#lblColegiado {position:absolute;left:0;top:0;width:37%;}
#selColegiado {position:absolute;left:0;top:20%;width:37%;}

#lblSessao {position:absolute;left:0;top:45%;width:23%;}
#selSessao {position:absolute;left:0;top:65%;width:23%;}

#lblQuorum {position:absolute;left:25%;top:45%;width:11%;}
#txtQuorum {position:absolute;left:25%;top:65%;width:11%;}

#lblProcesso  {position:absolute;left:38%;top:45%;width:20%;}
/*noinspection CssUnusedSymbol*/
#ancProcesso {position:absolute;left:38%;top:65%;}


#lblStatus {position:absolute;left:64%;top:0;}
#lblStatus2 {position:absolute;left:64%;top:20%;width:35%;text-align:center;font-size:20px;line-height:3em;border: 1px solid #AAA;}

#lblTabelaColegiado {position:absolute;left:0;top:0;width:99%;}
#lblTabelaPauta {position:absolute;left:0;top:0;width:99%;}
#lblTabelaMesa {position:absolute;left:0;top:0;width:99%;}
#lblTabelaReferendo {position:absolute;left:0;top:0;width:99%;}

label.infraLabelTitulo {font-weight:normal;}

/*noinspection CssUnusedSymbol*/
.presente{ background-color: #c6efce;}
/*noinspection CssUnusedSymbol*/
.ausente { background-color: #ffc7ce;}

.resumo {float:right;color:white;font-size:0.9em;padding-right: 3px;position: relative;top:2px;}
#divConteudo {height:22em;width:100%;border-top:1px solid #ccc;padding-top:3px;}
#divAreaPauta {float:left;width:40%;height:99%;overflow:auto;}
#ifrDocumento {float:left;width:60%;height:99%;}

/*noinspection CssUnknownTarget*/
#divRedimensionar {float:left;display:inline;top:0;height:99%;overflow:auto;cursor:w-resize;width:5px;
  background: white url(imagens/barra_redimensionamento.gif) repeat-y center;
}
#divArvore{height:100%;width:100%;position:absolute;margin-left:0;left:0;top:0;display:none;background-color:yellow;opacity:0.0;filter:alpha(opacity=0);}
/*noinspection CssUnusedSymbol*/
a.espacamento{padding: 0 0.1em;}

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
  var objAjaxSessao;
  var ie=document.all;
  var nn6=document.getElementById&&!document.all;
  var isdrag=false;
  var x,y;
  var dobj;
  var divLeftTamanhoInicial = null;
  var divRightTamanhoInicial = null;

  function redimensionarMenu() {
    var wGlobal = document.getElementById('divInfraAreaGlobal').offsetWidth;
    var wMenu = document.getElementById('divInfraAreaTelaE').offsetWidth;
    var wLivre = wGlobal - wMenu;
    if (wLivre > 0) {
      document.getElementById("divAreaPauta").style.width = Math.floor(0.40 * wLivre) + 'px';
      document.getElementById("ifrDocumento").style.width = Math.floor(0.58 * wLivre) + 'px';
    }
    redimensionar();
  }
  function redimensionar(){

    if (INFRA_IOS){
      if (document.getElementById("divAreaPauta").offsetHeight < 100){
        document.getElementById("divAreaPauta").style.height = '100px';
      }

      if (document.getElementById("divRedimensionar").offsetHeight < 100){
        document.getElementById("divRedimensionar").style.height ='100px';
      }
    }

    var hTela = infraClientHeight();

    if (hTela > 0){

      var hDivGlobal = document.getElementById('divInfraAreaGlobal').scrollHeight;
      var PosYConteudo = infraOffsetTopTotal(document.getElementById('divConteudo'));

      var hRedimensionamento = 0;

      if (hTela > hDivGlobal){
        hRedimensionamento = hTela - PosYConteudo;
      }else{
        hRedimensionamento = hDivGlobal - PosYConteudo;
      }

      if (hRedimensionamento > 0 && hRedimensionamento < 1920){ //FullHD

        hRedimensionamento = hRedimensionamento - 25;

        if (!INFRA_IOS){
          document.getElementById("divAreaPauta").style.width = '40%';
          document.getElementById("ifrDocumento").style.width = '58%';
        }else{
          var wConteudo = document.getElementById('divConteudo').offsetWidth;
          document.getElementById("divAreaPauta").style.width = Math.floor(0.4*wConteudo) + 'px';
          document.getElementById("ifrDocumento").style.width = Math.floor(0.58*wConteudo) + 'px';
        }

        document.getElementById('divConteudo').style.height = hRedimensionamento + 'px';
        document.getElementById("divAreaPauta").style.height = hRedimensionamento + 'px';
        document.getElementById('divRedimensionar').style.height = hRedimensionamento + 'px';
        document.getElementById("ifrDocumento").style.height = hRedimensionamento + 'px';


      }
    }
  }
  function movemouse(e) {

    if (e == null) { e = window.event }
    if (e.button <= 1 && isdrag){

      var tamanhoRedimensionamento = nn6 ? tx + e.clientX - x : tx + event.clientX - x;

      var tamanhoLeft = 0;
      var tamanhoRight = 0;

      if (tamanhoRedimensionamento > 0){
        tamanhoLeft = (divLeftTamanhoInicial + tamanhoRedimensionamento);
        tamanhoRight = (divRightTamanhoInicial - tamanhoRedimensionamento);
      }else{
        tamanhoLeft = (divLeftTamanhoInicial - Math.abs(tamanhoRedimensionamento));
        tamanhoRight = (divRightTamanhoInicial + Math.abs(tamanhoRedimensionamento));
      }

      if (tamanhoLeft < 0 || tamanhoRight < 0){
        if (tamanhoRedimensionamento > 0){
          tamanhoLeft = 0;
          tamanhoRight = (divLeftTamanhoInicial - divRightTamanhoInicial) ;
        }else{
          tamanhoLeft = (divLeftTamanhoInicial - divRightTamanhoInicial);
          tamanhoRight = 0;
        }
      }

      if(tamanhoLeft > 50 && tamanhoRight > 100){
        document.getElementById("divAreaPauta").style.width = tamanhoLeft + 'px';
        document.getElementById("ifrDocumento").style.width = tamanhoRight + 'px';
      }
    }
    return false;
  }
  function selectmouse(e){

    document.getElementById("divArvore").style.display = 'block';

    var fobj       = nn6 ? e.target : event.srcElement;
    var topelement = nn6 ? "HTML" : "BODY";
    while (fobj.tagName != topelement && fobj.className != "dragme") {
      fobj = nn6 ? fobj.parentNode : fobj.parentElement;
    }

    if (fobj.className=="dragme") {
      isdrag = true;
      dobj = fobj;
      tx = parseInt(dobj.style.left+0);
      x = nn6 ? e.clientX : event.clientX;
      divLeftTamanhoInicial = document.getElementById("divAreaPauta").offsetWidth;
      divRightTamanhoInicial = document.getElementById("ifrDocumento").offsetWidth;

      if (!INFRA_IOS){
        document.onmousemove=movemouse;
      } else {
        document.ontouchmove=movemouse;
      }
      return false;
    }

  }
  function dropmouse(){
    isdrag=false;
    var da=document.getElementById("divArvore");
    if(da) {
      da.style.display = 'none';
    }
  }
function abrirDocumento(link,el){
  var ifr=document.getElementById('ifrDocumento');
  if (ifr==null){
    ifr=parent.document.getElementById('ifrVisualizacao');
  }
  if (ifr==null){
    window.open(link,'_blank');
  } else {
    ifr.src=link;
  }

  $('.infraTrAcessada').removeClass('infraTrAcessada');
  $(el).closest('tr').addClass('infraTrAcessada');
}
  function inicializar(){
    var cookie='<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>';
    var divs=['divMembros','divPauta','divMesa','divReferendo'];
    for(var i=0;i<4;i++) {
      if (infraLerCookie(cookie +'_'+ divs[i]) === 'Fechar') {
        exibe('#' + divs[i]);
      }
    }
    infraFlagResize=true;

    infraOcultarMenuSistemaEsquema();
    objAjaxSessao=new infraAjaxMontarSelectDependente('selColegiado','selSessao','<?=$strLinkAjaxSessao;?>');
    objAjaxSessao.mostrarAviso = false;
    objAjaxSessao.tempoAviso = 1000;
    objAjaxSessao.prepararExecucao = function(){
      var ret='IdColegiado='+document.getElementById('selColegiado').value;
      ret=ret+'&IdSessaoJulgamento=<?=$idSessaoJulgamento?>';
      ret=ret+'&StaSituacao=<?=SessaoJulgamentoRN::$ES_PREVISTA?>,<?=SessaoJulgamentoRN::$ES_SUSPENSA?>,<?=SessaoJulgamentoRN::$ES_PAUTA_ABERTA?>,<?=SessaoJulgamentoRN::$ES_PAUTA_FECHADA?>,<?=SessaoJulgamentoRN::$ES_ABERTA?>,<?=SessaoJulgamentoRN::$ES_ENCERRADA?>,<?=SessaoJulgamentoRN::$ES_FINALIZADA?>';
      return ret;
    };

    objAjaxSessao.executar();
    infraDesabilitarCamposAreaDados();
      $('label.infraLabelTitulo img').css('visibility','');
    //document.getElementById('chkSinExibirIframe').disabled=false;
    if (document.getElementById('btnVoltar')!==null) {
      document.getElementById('btnVoltar').focus();
    }

    infraEfeitoTabelas();
    <?if ($_GET['acao']!='sessao_julgamento_acompanhar') {  ?>
    document.getElementById('selColegiado').disabled = false;
    document.getElementById('selSessao').disabled = false;
    <? }  ?>


  }

  function selecionaNaoVotados(sel){
    var div=$(sel);
    div.find('input:checked').click();
    div.find('tr[data-sta-item=N] input:not(:checked)').click();
  }
  function limparTela(){
    var a=document.getElementById('divConteudo');
    if (a) {
      $(a).empty();
      $('#lblStatus').remove();
      $('#lblStatus2').remove();
      $('#lblQuorum').remove();
      $('#txtQuorum').remove();
    }
  }

function onSubmitForm() {
  return true;
}
  function exibe(sel,image){
    var cookie='<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>'+'_'+sel.substr(1);
    var img;
    $(sel).toggle();
    if (image) {
      img=$(image);
    } else {
      img=$(sel).prev().find('img');
    }

    if (img.attr('src')=='<?=PaginaSEI::getInstance()->getIconeExibir()?>'){
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeOcultar()?>');
      infraCriarCookie(cookie,"Fechar",356);
    } else {
      img.attr('src','<?=PaginaSEI::getInstance()->getIconeExibir()?>');
      infraRemoverCookie(cookie);
    }
    //label.innerHTML='&'
  }
function salvarCookieIframe(){
  var cookie='<?=PaginaSEI::getInstance()->getStrPrefixoCookie()?>'+'_SJ_IFRAME_DOCUMENTO';
  var chk=document.getElementById('chkSinExibirIframe');
  if(!chk.checked){
    infraCriarCookie(cookie,"N",356);
  } else {
    infraRemoverCookie(cookie);
  }
  var form=document.getElementById('frmSessaoJulgamentoCadastro');
  form.submit();
}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmSessaoJulgamentoCadastro" method="post" onsubmit="return onSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('10em');
/*if (!isset($_GET['id_procedimento_sessao']) && $_GET['acao']!='sessao_julgamento_acompanhar'){
?>
  <div id="divSinExibirIframe" class="infraDivCheckbox">
    <input type="checkbox" id="chkSinExibirIframe" name="chkSinExibirIframe" class="infraCheckbox" <?=PaginaSEI::getInstance()->setCheckbox($chkSinExibirIframe)?>  tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" onclick="salvarCookieIframe()"/>
    <label id="lblSinExibirIframe" for="chkSinExibirIframe" class="infraLabelCheckbox">Exibir documentos nesta janela</label>
  </div>
<? } */?>
  <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <select id="selColegiado" name="selColegiado" onchange="limparTela();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
    <?=$strItensSelColegiado?>
  </select>

  <label id="lblSessao" for="selSessao" accesskey="" class="infraLabelObrigatorio">Data:</label>
  <select id="selSessao" name="selSessao" onchange="this.form.submit();" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" >
  </select>
<? if ($idSessaoJulgamento!=null) {
     ?>

    <label id="lblQuorum" for="txtQuorum" class="infraLabelObrigatorio">Qu�rum m�nimo:</label>
    <input type="text" id="txtQuorum" name="txtQuorum" class="infraText"
           value="<?= $objSessaoJulgamentoDTO->getNumQuorumMinimoColegiado(); ?>"
           tabindex="<?= PaginaSEI::getInstance()->getProxTabDados() ?>"/>

    <label id="lblStatus" class="infraLabelObrigatorio">Situa��o:</label>
    <label id="lblStatus2" class="infraLabelObrigatorio" style="<?= $strStyle; ?>"><?= PaginaSEI::tratarHTML($strSituacaoSessao); ?></label>


    <input type="hidden" id="hdnIdSessaoJulgamento" name="hdnIdSessaoJulgamento"
           value="<?= $objSessaoJulgamentoDTO->getNumIdSessaoJulgamento(); ?>"/>

    <?if($bolExibeProcesso){ ?>
      <label id="lblProcesso" class="infraLabelObrigatorio">Processo:</label>
      <?=$strLinkProcesso;?>

    <?}
    PaginaSEI::getInstance()->fecharAreaDados();


    PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?><label id="lblTabelaColegiado" class="infraLabelTitulo"><img onclick="exibe('#divMembros',this);" src="<?=PaginaSEI::getInstance()->getIconeExibir()?>" />&nbsp;Membros do Colegiado
    <div class="resumo"><?=$numRegistrosMembros?> Membros</div>
    </label>

        <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?><div id='divMembros'><?
    PaginaSEI::getInstance()->montarAreaTabela($strResultadoMembros, $numRegistrosMembros,true);
    ?>
    <br/></div>
    <?
    PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?>
    <label id="lblTabelaPauta" class="infraLabelTitulo">
      <img onclick="exibe('#divPauta',this);" src="<?=PaginaSEI::getInstance()->getIconeS()?>" />&nbsp;Pauta
      <div class="resumo"><?
        if ($numRegistrosPautaTotal) {
          if ($numRegistrosPautaTotal==1) {
            echo '1 Item';
          } else {
            echo $numRegistrosPautaTotal . ' Itens';
          }
        }
        ?></div>
    </label>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?><div id="divPauta"><?
    if (InfraArray::contar($arrTabelasPauta) > 0) {
      foreach ($arrTabelasPauta as $arrTabela) {
        PaginaSEI::getInstance()->montarAreaTabela($arrTabela[0], $arrTabela[1], true);
      }
    } else {
      PaginaSEI::getInstance()->montarAreaTabela('', null, true);
      echo '<br />';
    }
    ?></div><?
    PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?>

    <label id="lblTabelaMesa" class="infraLabelTitulo">
      <img onclick="exibe('#divMesa',this);" src="<?=PaginaSEI::getInstance()->getIconeExibir()?>" />&nbsp;Mesa
      <div class="resumo"><?
        if ($numRegistrosMesaTotal) {
          if ($numRegistrosMesaTotal == 1) {
            echo '1 Item';
          } else {
            echo $numRegistrosMesaTotal . ' Itens';
          }
        }
        ?></div></label>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?><div id="divMesa"><?
    if (InfraArray::contar($arrTabelasMesa) > 0) {
      foreach ($arrTabelasMesa as $arrTabela) {
        PaginaSEI::getInstance()->montarAreaTabela($arrTabela[0], $arrTabela[1], true);
      }
    } else {
      PaginaSEI::getInstance()->montarAreaTabela('', null, true);
      echo '<br />';
    }
?></div><?
    PaginaSEI::getInstance()->abrirAreaDados('3em');
    ?>

    <label id="lblTabelaReferendo" class="infraLabelTitulo">
      <img onclick="exibe('#divReferendo',this);" src="<?=PaginaSEI::getInstance()->getIconeExibir()?>" />&nbsp;Referendo
      <div class="resumo"><?
        if ($numRegistrosReferendo) {
          if ($numRegistrosReferendo == 1) {
            echo '1 Item';
          } else {
            echo $numRegistrosReferendo . ' Itens';
          }
        }
        ?></div></label>

    <?
    PaginaSEI::getInstance()->fecharAreaDados();
    ?><div id="divReferendo"><?

    PaginaSEI::getInstance()->montarAreaTabela($strResultadoReferendo, $numRegistrosReferendo, true);

?></div><?


} else {
  PaginaSEI::getInstance()->fecharAreaDados();
}
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>