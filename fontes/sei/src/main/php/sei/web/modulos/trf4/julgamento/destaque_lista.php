<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
//  InfraDebug::getInstance()->setBolLigado(false);
//  InfraDebug::getInstance()->setBolDebugInfra(true);
//  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->setTipoPagina(PaginaSEI::$TIPO_PAGINA_SIMPLES);

  $arrParametros=['id_item_sessao_julgamento','id_sessao_julgamento','id_procedimento_sessao','arvore','id_colegiado','id_distribuicao','sta_tipo', 'num_seq'];

  $strParametros='';
  foreach ($arrParametros as $strParametro) {
    if(isset($_GET[$strParametro])){
      $strParametros.='&'.$strParametro.'='.$_GET[$strParametro];
    }
  }


  switch($_GET['acao']){
    case 'destaque_excluir':
      try{
        $arrObjDestaqueDTO = array();
        $objDestaqueDTO = new DestaqueDTO();
        $objDestaqueDTO->setNumIdDestaque($_GET['id_destaque']);
        $arrObjDestaqueDTO[] = $objDestaqueDTO;
        $objDestaqueRN = new DestaqueRN();
        $objDestaqueRN->excluir($arrObjDestaqueDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      } 
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao'].$strParametros));
      die;
    case 'destaque_marcar_leitura':
      try{
        $objDestaqueDTO = new DestaqueDTO();
        $objDestaqueDTO->setNumIdDestaque($_GET['id_destaque']);
        $objDestaqueRN = new DestaqueRN();
        $objDestaqueRN->marcarLeitura($objDestaqueDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::montarAncora($_GET['id_destaque'])));
      die;
    case 'destaque_marcar_leitura_todos':
      try{
        $objDestaqueDTO = new DestaqueDTO();
        $objDestaqueDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
        $objDestaqueRN = new DestaqueRN();
        $objDestaqueRN->marcarLeituraTodos($objDestaqueDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao'].$strParametros));
      die;
    case 'destaque_desmarcar_leitura':
      try{
        $objDestaqueDTO = new DestaqueDTO();
        $objDestaqueDTO->setNumIdDestaque($_GET['id_destaque']);
        $objDestaqueRN = new DestaqueRN();
        $objDestaqueRN->desmarcarLeitura($objDestaqueDTO);
        PaginaSEI::getInstance()->adicionarMensagem('Opera��o realizada com sucesso.');
      }catch(Exception $e){
        PaginaSEI::getInstance()->processarExcecao($e);
      }
      header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::montarAncora($_GET['id_destaque'])));
      die;
    case 'destaque_listar':
      $strTitulo = 'Destaques';
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $arrComandos = array();


  $bolAcaoCadastrar = SessaoSEI::getInstance()->verificarPermissao('destaque_cadastrar');

  $objDestaqueDTO = new DestaqueDTO();
  $objDestaqueRN = new DestaqueRN();

  $objDestaqueDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);

  $objItemSessaoJulgamentoRN=new ItemSessaoJulgamentoRN();
  $objItemSessaoJulgamentoDTO=new ItemSessaoJulgamentoDTO();
  $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($_GET['id_item_sessao_julgamento']);
  $objItemSessaoJulgamentoDTO->retDblIdProcedimentoDistribuicao();
  $objItemSessaoJulgamentoDTO->retStrProtocoloFormatadoProtocolo();
  $objItemSessaoJulgamentoDTO->retNumIdColegiadoSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->retStrStaSituacaoSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->retNumIdUnidadeResponsavelColegiado();
  $objItemSessaoJulgamentoDTO->retNumIdSessaoJulgamento();
  $objItemSessaoJulgamentoDTO->retStrDescricaoSessaoBloco();
  $objItemSessaoJulgamentoDTO=$objItemSessaoJulgamentoRN->consultar($objItemSessaoJulgamentoDTO);
  if($objItemSessaoJulgamentoDTO==null){
    throw new InfraException('N�o foi poss�vel localizar este item na sess�o de julgamento.');
  }
  $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
  $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
  $objSessaoJulgamentoDTO->setStrStaSituacao($objItemSessaoJulgamentoDTO->getStrStaSituacaoSessaoJulgamento());
  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $bolPermiteNovoDestaque= $objSessaoJulgamentoRN->permiteIncluirDestaque($objSessaoJulgamentoDTO);
  $objColegiadoDTO=new ColegiadoDTO();
  $objColegiadoDTO->setNumIdColegiado($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
  $objColegiadoRN=new ColegiadoRN();
  $bolPertenceColegiado=$objColegiadoRN->verificaUnidadeAtual($objColegiadoDTO);
  if (!$bolPertenceColegiado){
    $bolPertenceColegiado=($objItemSessaoJulgamentoDTO->getNumIdUnidadeResponsavelColegiado()==SessaoSEI::getInstance()->getNumIdUnidadeAtual());
  }

  $strTitulo .= ' '.$objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco().' ';
  $strTitulo .= $_GET['num_seq']. ' ('.$objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo().')';

  $strTipo='destaques';

  if($_POST['hdnTipoVisualizacao']===DestaqueRN::$TV_SESSOES_ANTERIORES) {
    $objDestaqueDTO->setStrStaTipoVisualizacao(DestaqueRN::$TV_SESSOES_ANTERIORES);
    $strLinkTipoVisualizacao = '<a id="ancTipoVisualizacao" onclick="verDestaques(\''.DestaqueRN::$TV_SESSAO_ATUAL.'\');" class="ancoraPadraoPreta">Ver '.$strTipo.' desta sess�o</a>';
  } else {
    $objDestaqueDTO->setStrStaTipoVisualizacao(DestaqueRN::$TV_SESSAO_ATUAL);
    $objDestaqueDTOBanco=new DestaqueDTO();
    $objDestaqueDTOBanco->setNumMaxRegistrosRetorno(1);
    $objDestaqueDTOBanco->retNumIdDestaque();
    $objDestaqueDTOBanco->setDblIdProcedimentoDistribuicao($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
    $objDestaqueDTOBanco->setNumIdItemSessaoJulgamento($objDestaqueDTO->getNumIdItemSessaoJulgamento(),InfraDTO::$OPER_DIFERENTE);
    $objDestaqueDTOBanco->setNumIdColegiadoSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento());
    if ($objDestaqueRN->consultar($objDestaqueDTOBanco)!=null){
      $strLinkTipoVisualizacao = '<a id="ancTipoVisualizacao" onclick="verDestaques(\''.DestaqueRN::$TV_SESSOES_ANTERIORES.'\');" class="ancoraPadraoPreta">Ver '.$strTipo.' das sess�es anteriores</a>';
    }
  }


  $arrObjTipoDestaqueDTO=InfraArray::indexarArrInfraDTO($objDestaqueRN->listarValoresTipoDestaque(),'StaTipo');

  $arrObjDestaqueDTO = $objDestaqueRN->visualizar($objDestaqueDTO);

//  if($_POST['hdnTipoVisualizacao']===DestaqueRN::$TV_SESSOES_ANTERIORES) {
    $arrObjDestaqueDTO2=CacheSessaoJulgamentoRN::getArrObjDestaqueDTO($_GET['id_item_sessao_julgamento']);

    $strIconesDestaque=DestaqueINT::montarIconeDestaques($arrObjDestaqueDTO2[$_GET['id_item_sessao_julgamento']],$objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento(),$objSessaoJulgamentoDTO,$_GET['num_seq'], $_GET['id_item_sessao_julgamento'], $qtd);
//  } else {
//    $strIconesDestaque=DestaqueINT::montarIconeDestaques($arrObjDestaqueDTO,$objItemSessaoJulgamentoDTO->getNumIdColegiadoSessaoJulgamento(),$objSessaoJulgamentoDTO,$_GET['num_seq'], $_GET['id_item_sessao_julgamento'], $qtd);
//  }
  //
  $strComandoNovo = '<button type="button" accesskey="N" id="btnNovo" value="Novo" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=destaque_cadastrar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].$strParametros).'\'" class="infraButton"><span class="infraTeclaAtalho">N</span>ovo</button>';

  if (PaginaSEI::getInstance()->getAcaoRetorno()==='sessao_julgamento_pautar'){
    $strLinkRetorno=SessaoSEI::getInstance()->assinarLink('controlador.php?acao=sessao_julgamento_pautar&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($_GET['id_item_sessao_julgamento']));
    $arrComandos[] = '<button type="button" accesskey="V" name="btnVoltar" id="btnVoltar" value="Voltar" onclick="location.href=\''.$strLinkRetorno.'\';" class="infraButton"><span class="infraTeclaAtalho">V</span>oltar</button>';
  }

  $numRegistros = InfraArray::contar($arrObjDestaqueDTO);
  if($_POST['hdnTipoVisualizacao']===DestaqueRN::$TV_SESSOES_ANTERIORES) {
    $numRegistrosSessaoAtual = 0;
  } else {
    $numRegistrosSessaoAtual = $numRegistros;
  }
  $bolAcaoExcluir = SessaoSEI::getInstance()->verificarPermissao('destaque_excluir');
  $arrResultados=[];
  $arrNumRegistros=[];

  $arrObjColegiadoComposicaoDTO = CacheSessaoJulgamentoRN::getArrObjColegiadoComposicaoDTO($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
  $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUnidade');
  $numIdUnidadeAtual=SessaoSEI::getInstance()->getNumIdUnidadeAtual();

  $strResultado = '';
    if ($numRegistros > 0){
      $strLink = SessaoSEI::getInstance()->assinarLink('controlador.php?acao=destaque_marcar_leitura_todos&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . $strParametros);
      $arrComandos[] = '<button type="button" accesskey="T" name="btnMarcarTodos" id="btnMarcarTodos" value="Marcar Todos como Lidos" onclick="location.href=\''.$strLink.'\';" class="infraButton">Marcar <span class="infraTeclaAtalho">T</span>odos como Lidos</button>';



      $strSumarioTabela = 'Tabela de Destaques.';
      $strCaptionTabela = 'Destaques';

      $strResultado .= '<table width="99%" id="tblDestaques" class="infraTable" summary="'.$strSumarioTabela.'">'."\n";
      $strResultado .= '<caption class="infraCaption">'.PaginaSEI::getInstance()->gerarCaptionTabela($strCaptionTabela,$numRegistros).'</caption>';
      $strResultado .= '<tr>';
    $strResultado .= '<th class="infraTh" width="1%" style="display:none">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
      $strResultado .= '<th class="infraTh" style="width: 5%">Ordem</th>'."\n";
      $strResultado .= '<th class="infraTh" style="width: 25%" colspan="2">Tipo</th>' . "\n";
      $strResultado .= '<th class="infraTh" style="width: 18%">Unidade</th>' . "\n";
      $strResultado .= '<th class="infraTh" style="width: 18%">Usu�rio</th>'."\n";
      $strResultado .= '<th class="infraTh">Data/Hora</th>'."\n";
      $strResultado .= '<th class="infraTh" style="width: 15%">A��es</th>'."\n";
      $strResultado .= '</tr>'."\n";

      $strCssTr='';
      $bolPossuiItemNaoLido=false;
//      $numOrdem=$numRegistros;
      /** @var DestaqueDTO[] $arrObjDestaqueDTO */
      for($i = 0;$i < $numRegistros; $i++){
        $bolPossuiDescricao=$arrObjDestaqueDTO[$i]->getStrDescricao()!==null;
        $bolVisualizado=$arrObjDestaqueDTO[$i]->getStrSinVisualizado()==='S';

        if(!$bolPossuiItemNaoLido && !$bolVisualizado){
          $bolPossuiItemNaoLido=true;
        }
        $strClassVisualizado=$bolVisualizado?' visualizado':'';

        $strCssTr = ($strCssTr=='infraTrClara')?'infraTrEscura':'infraTrClara';

        if ($arrObjDestaqueDTO[$i]->getStrStaTipo()==$_GET['sta_tipo']){
          $strCssTr .= ' infraTrAcessada';
        }

        $strResultado .= '<tr name="trProcesso'.$arrObjDestaqueDTO[$i]->getNumIdDestaque().'" class="'.$strCssTr.$strClassVisualizado.' principal" style="font-size: 1.1em;">';

//        if(!$bolVisualizado){
//          $strCssDestaque = 'destaqueNovo';
//        }else{
//          $strCssDestaque = '';
//        }

        $objTipoDestaqueDTO = $arrObjTipoDestaqueDTO[$arrObjDestaqueDTO[$i]->getStrStaTipo()];

        $strResultado .= '<td valign="top" style="display: none">'.PaginaSEI::getInstance()->getTrCheck($i,$arrObjDestaqueDTO[$i]->getNumIdDestaque(),$objTipoDestaqueDTO->getStrDescricao()).'</td>';

        $strResultado .= '<td style="text-align: center">'.($i+1).'</td>';

        $strResultado .= '<td  align="center">';
        $strResultado .= '<div><img src="';
        if($arrObjDestaqueDTO[$i]->getStrStaAcesso()==DestaqueRN::$TA_RESTRITO){
          $strResultado .= MdJulgarIcone::prepararIconeBase64(MdJulgarIcone::COMENTARIOS_INTERNOS, $bolVisualizado ? MdJulgarIcone::COR_VERDE : MdJulgarIcone::COR_VERMELHO);
        } else {
          $strResultado .= MdJulgarIcone::prepararIconeBase64($objTipoDestaqueDTO->getStrIcone(), $bolVisualizado ? MdJulgarIcone::COR_VERDE : MdJulgarIcone::COR_VERMELHO);
        }
        $strResultado .= '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '" class="infraImg" /></div>';
        $strResultado .= '</td>';

        $strResultado .= '<td  align="left" style="font-weight:bold;">'.$objTipoDestaqueDTO->getStrDescricao().'</td>';

        $strResultado .= '<td style="text-align: center"><a style="font-size:1em" onclick="alert(\'' . PaginaSEI::getInstance()->formatarParametrosJavaScript($arrObjDestaqueDTO[$i]->getStrDescricaoUnidade()) . '\')" title="' . PaginaSEI::tratarHTML($arrObjDestaqueDTO[$i]->getStrDescricaoUnidade()) . '" class="ancoraSigla">' . PaginaSEI::tratarHTML($arrObjDestaqueDTO[$i]->getStrSiglaUnidade()) . '</a>';

        if($_POST['hdnTipoVisualizacao']===DestaqueRN::$TV_SESSOES_ANTERIORES && $arrObjDestaqueDTO[$i]->getNumIdSessaoJulgamentoItem()!=$_GET['id_sessao_julgamento']) {
          $strResultado .= '<br /> (Sess�o de '.substr($arrObjDestaqueDTO[$i]->getDthSessaoSessaoJulgamento(),0,-3).')';
        }

        $strResultado .= '</td>';
        $numIdUnidadeDestaque=$arrObjDestaqueDTO[$i]->getNumIdUnidade();
        if($numIdUnidadeDestaque!=$numIdUnidadeAtual && isset($arrObjColegiadoComposicaoDTO[$numIdUnidadeDestaque])) {
          $strNomeUsuario= $arrObjColegiadoComposicaoDTO[$numIdUnidadeDestaque]->getStrNomeUsuario();
          $strSiglaUsuario=$arrObjColegiadoComposicaoDTO[$numIdUnidadeDestaque]->getStrSiglaUsuario();
        } else {
          $strNomeUsuario=$arrObjDestaqueDTO[$i]->getStrNomeUsuario();
          $strSiglaUsuario=$arrObjDestaqueDTO[$i]->getStrSiglaUsuario();
        }
        $strResultado .= '<td style="text-align: center"><a onclick="alert(\'' . PaginaSEI::getInstance()->formatarParametrosJavaScript($strNomeUsuario) . '\')" title="'.PaginaSEI::tratarHTML($strNomeUsuario).'" class="ancoraSigla">'.PaginaSEI::tratarHTML($strSiglaUsuario). '</a></td>';
        $strResultado .= '<td style="text-align: center">'.$arrObjDestaqueDTO[$i]->getDthDestaque().'</td>';

        $strResultado .= '<td style="text-align: center">';
        //marcar como lido/n�o lido
        if(!$bolVisualizado) {
          $strResultado .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=destaque_marcar_leitura&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_destaque=' . $arrObjDestaqueDTO[$i]->getNumIdDestaque() . $strParametros) . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="'.MdJulgarIcone::VERIFICACAO1.'" title="Confirmar Leitura" alt="Confirmar Leitura" class="infraImg" /></a>&nbsp;';
        } else {
          $strResultado .= '<a href="' . SessaoSEI::getInstance()->assinarLink('controlador.php?acao=destaque_desmarcar_leitura&acao_origem=' . $_GET['acao'] . '&acao_retorno=' . $_GET['acao'] . '&id_destaque=' . $arrObjDestaqueDTO[$i]->getNumIdDestaque() . $strParametros) . '" tabindex="' . PaginaSEI::getInstance()->getProxTabTabela() . '"><img src="'.MdJulgarIcone::VERIFICACAO2.'" title="Marcar como n�o lido" alt="Marcar como n�o lido" class="infraImg" /></a>&nbsp;';
        }
//        if ($arrObjDestaqueDTO[$i]->getStrStaAcesso()===DestaqueRN::$TA_PUBLICO){
          if ($bolPermiteNovoDestaque && $bolAcaoExcluir && $bolPertenceColegiado && $arrObjDestaqueDTO[$i]->getStrSinBloqueado()==='N' && $arrObjDestaqueDTO[$i]->getNumIdUnidade()==SessaoSEI::getInstance()->getNumIdUnidadeAtual() && $arrObjDestaqueDTO[$i]->getNumIdUsuario()==SessaoSEI::getInstance()->getNumIdUsuario()){
            $strResultado .= '<a href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=destaque_alterar&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_destaque='.$arrObjDestaqueDTO[$i]->getNumIdDestaque().$strParametros).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeAlterar().'" title="Alterar Destaque" alt="Alterar Destaque" class="infraImg" /></a>&nbsp;';
          }
//        }
        //permite excluir a qualquer momento - pedido #141435
        if ($bolAcaoExcluir && $bolPertenceColegiado && $arrObjDestaqueDTO[$i]->getNumIdUnidade()==SessaoSEI::getInstance()->getNumIdUnidadeAtual()){
          $strResultado .= '<a href="javascript:void(0)" onclick="acaoExcluir(\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao=destaque_excluir&acao_origem='.$_GET['acao'].'&id_destaque='.$arrObjDestaqueDTO[$i]->getNumIdDestaque().$strParametros).'\');" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'"><img src="'.PaginaSEI::getInstance()->getIconeExcluir().'" title="Excluir Destaque" alt="Excluir Destaque" class="infraImg" /></a>';
        }

        $strResultado .= '</td>';
        $strResultado .="</tr>\n";

        if($bolPossuiDescricao){
          $strResultado .= '<tr name="trProcesso'.$arrObjDestaqueDTO[$i]->getNumIdDestaque().'" class="'.$strCssTr.$strClassVisualizado.'">';
          $style= 'padding:5px;';

          $strResultado .= '<td style="font-size:1.1em;border-top:0;' . $style . '">&nbsp;</td>';
          $strResultado .= '<td colspan="6" style="font-size:1.1em;border-top:0;' . $style . '">';
          $strResultado .= nl2br(PaginaSEI::tratarHTML($arrObjDestaqueDTO[$i]->getStrDescricao())).'</td>';
          $strResultado .="</tr>\n";
        }

      }
      $strResultado .= '</table>';

  }



  if ($bolAcaoCadastrar && $bolPertenceColegiado && $bolPermiteNovoDestaque){

    array_unshift($arrComandos,$strComandoNovo);

  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
} 

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
  td.destaque {color:#fff}
td.destaque a {color:#fff;text-decoration:none;font-size:1.1em;cursor:pointer}
td.destaqueNovo {color:#000;background-color:#f59f9f;}
td.destaqueNovo a {color:#000;text-decoration:none;font-size:1.1em;cursor:pointer}
img:focus {outline: none;}

  <?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>

function inicializar(){
  //infraEfeitoTabelas();
  if (window.parent && typeof window.parent.atualizarIconeDestaque === 'function'){
    window.parent.atualizarIconeDestaque(<?=$_GET['id_item_sessao_julgamento']?>,'<?=base64_encode($strIconesDestaque)?>');
  }

  seiGerarEfeitoTabelasRowSpan('tblDestaques');

  $('tr.infraTrAcessada img').first().focus();

}

function acaoExcluir(link){
  if (confirm("Confirma exclus�o do Destaque ?")){
    document.getElementById('frmDestaqueLista').action=link;
    document.getElementById('frmDestaqueLista').submit();
  }
}

function verDestaques(valor){
  document.getElementById('hdnTipoVisualizacao').value = valor;
  document.getElementById('frmDestaqueLista').submit();
}

function verTodos(){
  $('#divInfraAreaTabela').show();
  $('#lblSemRegistros').hide();
  $('tr').show();
  $('#ancNaoLidos').show();
  $('#ancTodos').hide();
  atualizaCaptionTabela();
}
function verNaoLidos(){
  $('.visualizado').hide();
  $('#ancNaoLidos').hide();
  $('#ancTodos').show();
  atualizaCaptionTabela();
}
function atualizaCaptionTabela(){
  var i=$('tr.principal:visible').length;
  var registros;
  if(i===0){
    $('#divInfraAreaTabela').hide();
    $('#lblSemRegistros').show();
  } else {
    $('#lblSemRegistros').hide();
    if(i===1){
      registros='1 registro';
    }else {
      registros=i+' registros';
    }
    $('caption').text('Lista de Destaques ('+registros+'):')
  }
}
<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmDestaqueLista" method="post" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
  <?
  PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
  PaginaSEI::getInstance()->abrirAreaDados('4em');
  echo $strLinkTipoVisualizacao.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
//  PaginaSEI::getInstance()->fecharAreaDados();
//  PaginaSEI::getInstance()->abrirAreaDados('3em');
  echo '<a id="ancTodos" onclick="verTodos();" class="ancoraPadraoPreta" style="display: none">Ver todos</a>';
  echo '<a id="ancNaoLidos" onclick="verNaoLidos();" class="ancoraPadraoPreta">Ver somente n�o lidos</a>';
  PaginaSEI::getInstance()->fecharAreaDados();


  PaginaSEI::getInstance()->montarAreaTabela($strResultado,$numRegistros,true);
  PaginaSEI::getInstance()->abrirAreaDados('3em');
  echo '<label id="lblSemRegistros" style="display: none">Nenhum registro encontrado.</label>';
  PaginaSEI::getInstance()->fecharAreaDados();
//  PaginaSEI::getInstance()->montarAreaDebug();
  PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
  <input type="hidden" id="hdnTipoVisualizacao" name="hdnTipoVisualizacao" value="<?=$objDestaqueDTO->getStrStaTipoVisualizacao();?>" />
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>