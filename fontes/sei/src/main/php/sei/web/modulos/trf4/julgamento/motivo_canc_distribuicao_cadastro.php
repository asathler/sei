<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 16/07/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.35.0
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('motivo_canc_distribuicao_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  $objMotivoCancDistribuicaoDTO = new MotivoCancDistribuicaoDTO();

  $strDesabilitar = '';

  $arrComandos = array();

  switch($_GET['acao']){
    case 'motivo_canc_distribuicao_cadastrar':
      $strTitulo = 'Novo Motivo de Cancelamento de Distribui��o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarMotivoCancDistribuicao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao(null);
      $objMotivoCancDistribuicaoDTO->setStrDescricao($_POST['txtDescricao']);
      $objMotivoCancDistribuicaoDTO->setStrSinAtivo('S');

      if (isset($_POST['sbmCadastrarMotivoCancDistribuicao'])) {
        try{
          $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
          $objMotivoCancDistribuicaoDTO = $objMotivoCancDistribuicaoRN->cadastrar($objMotivoCancDistribuicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo de Cancelamento de Distribui��o "'.$objMotivoCancDistribuicaoDTO->getStrDescricao().'" cadastrado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_motivo_canc_distribuicao='.$objMotivoCancDistribuicaoDTO->getNumIdMotivoCancDistribuicao().PaginaSEI::getInstance()->montarAncora($objMotivoCancDistribuicaoDTO->getNumIdMotivoCancDistribuicao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_canc_distribuicao_alterar':
      $strTitulo = 'Alterar Motivo de Cancelamento de Distribui��o';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarMotivoCancDistribuicao" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';

      if (isset($_GET['id_motivo_canc_distribuicao'])){
        $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($_GET['id_motivo_canc_distribuicao']);
        $objMotivoCancDistribuicaoDTO->retTodos();
        $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
        $objMotivoCancDistribuicaoDTO = $objMotivoCancDistribuicaoRN->consultar($objMotivoCancDistribuicaoDTO);
        if ($objMotivoCancDistribuicaoDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($_POST['hdnIdMotivoCancDistribuicao']);
        $objMotivoCancDistribuicaoDTO->setStrDescricao($_POST['txtDescricao']);
        $objMotivoCancDistribuicaoDTO->setStrSinAtivo('S');
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoCancDistribuicaoDTO->getNumIdMotivoCancDistribuicao())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_POST['sbmAlterarMotivoCancDistribuicao'])) {
        try{
          $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
          $objMotivoCancDistribuicaoRN->alterar($objMotivoCancDistribuicaoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Motivo de Cancelamento de Distribui��o "'.$objMotivoCancDistribuicaoDTO->getStrDescricao().'" alterado com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objMotivoCancDistribuicaoDTO->getNumIdMotivoCancDistribuicao())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'motivo_canc_distribuicao_consultar':
      $strTitulo = 'Consultar Motivo de Cancelamento de Distribui��o';
      $arrComandos[] = '<button type="button" accesskey="F" name="btnFechar" value="Fechar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($_GET['id_motivo_canc_distribuicao'])).'\';" class="infraButton"><span class="infraTeclaAtalho">F</span>echar</button>';
      $objMotivoCancDistribuicaoDTO->setNumIdMotivoCancDistribuicao($_GET['id_motivo_canc_distribuicao']);
      $objMotivoCancDistribuicaoDTO->setBolExclusaoLogica(false);
      $objMotivoCancDistribuicaoDTO->retTodos();
      $objMotivoCancDistribuicaoRN = new MotivoCancDistribuicaoRN();
      $objMotivoCancDistribuicaoDTO = $objMotivoCancDistribuicaoRN->consultar($objMotivoCancDistribuicaoDTO);
      if ($objMotivoCancDistribuicaoDTO===null){
        throw new InfraException("Registro n�o encontrado.");
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }


}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblDescricao {position:absolute;left:0%;top:0%;width:80%;}
#txtDescricao {position:absolute;left:0%;top:40%;width:80%;}

<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>
function inicializar(){
  if ('<?=$_GET['acao']?>'=='motivo_canc_distribuicao_cadastrar'){
    document.getElementById('txtDescricao').focus();
  } else if ('<?=$_GET['acao']?>'=='motivo_canc_distribuicao_consultar'){
    infraDesabilitarCamposAreaDados();
  }else{
    document.getElementById('btnCancelar').focus();
  }
  infraEfeitoTabelas();
}

function validarCadastro() {
  if (infraTrim(document.getElementById('txtDescricao').value)=='') {
    alert('Informe a Descri��o.');
    document.getElementById('txtDescricao').focus();
    return false;
  }

  return true;
}

function OnSubmitForm() {
  return validarCadastro();
}

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmMotivoCancDistribuicaoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('5em');
?>
  <label id="lblDescricao" for="txtDescricao" accesskey="" class="infraLabelObrigatorio">Descri��o:</label>
  <input type="text" id="txtDescricao" name="txtDescricao" class="infraText" value="<?=PaginaSEI::tratarHTML($objMotivoCancDistribuicaoDTO->getStrDescricao());?>" onkeypress="return infraMascaraTexto(this,event,250);" maxlength="250" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />

  <input type="hidden" id="hdnIdMotivoCancDistribuicao" name="hdnIdMotivoCancDistribuicao" value="<?=$objMotivoCancDistribuicaoDTO->getNumIdMotivoCancDistribuicao();?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>