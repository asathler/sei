<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 03/07/2021 - criado por alv77
*
* Vers�o do Gerador de C�digo: 1.39.0
*/

require_once __DIR__ .'/../../../../SEI.php';

class SessaoBlocoINT extends InfraINT {

  public static function montarSelectInclusaoSessao($numIdSessaoJulgamento,$strStaDistribuicao,$bolUnidadePresidente): string
  {

    //pauta -> s� pauta_aberta
    //mesa -> virtual ->pauta aberta
    //mesa -> presencial -> pauta_aberta, pauta_fechada, aberta
    //referendo -> pauta_aberta, pauta_fechada, aberta / n�o pode pedido vista / tem que ser unidade presidente



    $objSessaoBlocoDTO = new SessaoBlocoDTO();
    $objSessaoBlocoDTO->retNumIdSessaoBloco();
    $objSessaoBlocoDTO->retStrDescricao();

    $objSessaoBlocoDTO->retStrStaSituacaoSessao();
//    $objSessaoBlocoDTO->retStrSinPermitePautaFechada();
    $objSessaoBlocoDTO->retStrStaTipoItem();

    $objSessaoBlocoDTO->setNumIdSessaoJulgamento($numIdSessaoJulgamento);

    $objSessaoBlocoDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objSessaoBlocoRN = new SessaoBlocoRN();
    $arrObjSessaoBlocoDTO = $objSessaoBlocoRN->listar($objSessaoBlocoDTO);

    $arrObjSessaoBlocoDTO2=[];
    foreach ($arrObjSessaoBlocoDTO as $objSessaoBlocoDTO) {
      $staSituacaoSessao=$objSessaoBlocoDTO->getStrStaSituacaoSessao();
      $staTipoItem=$objSessaoBlocoDTO->getStrStaTipoItem();

      if ($staTipoItem==TipoSessaoBlocoRN::$STA_REFERENDO && (!$bolUnidadePresidente || $strStaDistribuicao==DistribuicaoRN::$STA_PEDIDO_VISTA)){
        //n�o permite referendo de pedido de vista ou que n�o seja da unidade do presidente
        continue;
      }

      if($staSituacaoSessao==SessaoJulgamentoRN::$ES_PAUTA_ABERTA){
        $arrObjSessaoBlocoDTO2[]=$objSessaoBlocoDTO;
        continue;
      }
      if($staTipoItem!=TipoSessaoBlocoRN::$STA_PAUTA &&($staSituacaoSessao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA || $staSituacaoSessao==SessaoJulgamentoRN::$ES_ABERTA)){
        $arrObjSessaoBlocoDTO2[]=$objSessaoBlocoDTO;
      }
    }


    return parent::montarSelectArrInfraDTO(null, null, '', $arrObjSessaoBlocoDTO2, 'IdSessaoBloco', 'Descricao',false,['StaTipoItem']);
  }

  public static function montarSelectStaTipoItem($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado){
    $objSessaoBlocoRN = new SessaoBlocoRN();

    $arrObjTipoItemSessaoBlocoDTO = $objSessaoBlocoRN->listarValoresTipoItem();

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjTipoItemSessaoBlocoDTO, 'StaTipoItem', 'Descricao');

  }
}
?>