<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 27/10/2014 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(true);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  PaginaSEI::getInstance()->verificarSelecao('sessao_julgamento_selecionar');

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);

  PaginaSEI::getInstance()->salvarCamposPost(array('selStaSituacao','selColegiado'));

  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_procedimento_sessao','arvore','id_colegiado'));

  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();

  $strDesabilitar = '';

  $arrComandos = array();
  $strParametros='';
  if (isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }
  if (isset($_GET['id_sessao_julgamento'])){
    $idSessaoJulgamento=$_GET['id_sessao_julgamento'];
    $strParametros.='&id_sessao_julgamento='.$idSessaoJulgamento;
  } else {
    $idSessaoJulgamento=$_POST['selSessao'];
  }
  $numIdTipoSessao=$_POST['selTipoSessao'];

  switch($_GET['acao']){
    case 'sessao_julgamento_cadastrar':
      $strTitulo = 'Nova Sess�o de Julgamento';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmCadastrarSessaoJulgamento" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao']).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento(null);
      $objSessaoJulgamentoDTO->setDthSessao($_POST['txtSessao']);
      $objSessaoJulgamentoDTO->setNumIdTipoSessao($numIdTipoSessao);
      $objSessaoJulgamentoDTO->setStrStaSituacao(SessaoJulgamentoRN::$ES_PREVISTA);


      $numIdColegiado = PaginaSEI::getInstance()->recuperarCampo('selColegiado');
      if ($numIdColegiado!==''){
        $objSessaoJulgamentoDTO->setNumIdColegiado($numIdColegiado);
      }else{
        $objSessaoJulgamentoDTO->setNumIdColegiado(null);
      }

      $objSessaoJulgamentoDTO->setDthInicio(null);
      $objSessaoJulgamentoDTO->setDthFim(null);

      if (isset($_POST['sbmCadastrarSessaoJulgamento'])) {
        try{
          $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
          $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->cadastrar($objSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Sess�o de Julgamento "'.$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento().'" cadastrada com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&id_sessao_julgamento='.$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento().PaginaSEI::getInstance()->montarAncora($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    case 'sessao_julgamento_alterar':
      $strTitulo = 'Alterar Sess�o de Julgamento';
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmAlterarSessaoJulgamento" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';
      $strDesabilitar = 'disabled="disabled"';
      $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
      $bolDisponibilizado = false;

      if (isset($_GET['id_sessao_julgamento'])){
        $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($_GET['id_sessao_julgamento']);
        $objSessaoJulgamentoDTO->retTodos();
        $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);
        $objSessaoJulgamentoDTO->setDthSessao(substr($objSessaoJulgamentoDTO->getDthSessao(),0,-3));
        if ($objSessaoJulgamentoDTO==null){
          throw new InfraException("Registro n�o encontrado.");
        }
      } else {
        $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($_POST['hdnIdSessaoJulgamento']);
        $objSessaoJulgamentoDTO->setDthSessao($_POST['txtSessao']);
        $objSessaoJulgamentoDTO->setNumIdTipoSessao($numIdTipoSessao);
        $objSessaoJulgamentoDTO->setNumIdColegiado($_POST['selColegiado']);
      }

      $arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" id="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento())).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';

      if (isset($_GET['sta_situacao'])) {
        try{
          $objSessaoJulgamentoDTO->setStrStaSituacao($_GET['sta_situacao']);
          $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
          $objSessaoJulgamentoRN->alterarEstado($objSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Sess�o de Julgamento "'.$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento().'" alterada com sucesso.');
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
        header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao_origem'].'&acao_origem='.$_GET['acao'].$strParametros.PaginaSEI::getInstance()->montarAncora($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento())));
        die;
      }

      if (isset($_POST['sbmAlterarSessaoJulgamento'])) {
        try{
          $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
          $objSessaoJulgamentoRN->alterar($objSessaoJulgamentoDTO);
          PaginaSEI::getInstance()->adicionarMensagem('Sess�o de Julgamento "'.$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento().'" alterada com sucesso.');
          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].PaginaSEI::getInstance()->montarAncora($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento())));
          die;
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensSelStaSituacao = SessaoJulgamentoINT::montarSelectStaSituacaoFiltrado('null','&nbsp;',$objSessaoJulgamentoDTO->getStrStaSituacao());
  $strItensSelTipoSessao = TipoSessaoINT::montarSelectDescricao('null','&nbsp;',$objSessaoJulgamentoDTO->getNumIdTipoSessao());
  $strItensSelColegiado = ColegiadoINT::montarSelectNomeAdministrados('null','&nbsp;',$objSessaoJulgamentoDTO->getNumIdColegiado());

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
if(0){?><style><?}
?>
#lblColegiado {position:absolute;left:0%;top:0%;width:60%;}
#selColegiado {position:absolute;left:0%;top:6%;width:60%;}

#lblTipoSessao {position:absolute;left:20%;top:15%;width:16%;}
#selTipoSessao {position:absolute;left:20%;top:21%;width:16%;}

#lblStaSituacao {position:absolute;left:0%;top:30%;width:30%;}
#selStaSituacao {position:absolute;left:0%;top:36%;width:30%;}

#lblSessao {position:absolute;left:0%;top:15%;width:15%;}
#txtSessao {position:absolute;left:0%;top:21%;width:15%;}
#imgCalSessao {position:absolute;left:16%;top:21%;}

<?
if(0){?></style><?}
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
if(0){?><script><?}
?>
function inicializar(){
  <?if ($_GET['acao']=='sessao_julgamento_cadastrar'){?>
    document.getElementById('selColegiado').focus();
  <?} else {
      if (!in_array($objSessaoJulgamentoDTO->getStrStaSituacao(),array(SessaoJulgamentoRN::$ES_PREVISTA,SessaoJulgamentoRN::$ES_PAUTA_ABERTA,SessaoJulgamentoRN::$ES_PAUTA_FECHADA))) { ?>
    infraDesabilitarCamposAreaDados();
  <?  } else { ?>

    document.getElementById('selColegiado').disabled=true;
    document.getElementById('selStaSituacao').disabled=true;

  <?
      }?>
    document.getElementById('btnCancelar').focus();
  <?}?>
  infraEfeitoTabelas();
}

function validarCadastro() {

  if (!infraSelectSelecionado('selColegiado')) {
    alert('Selecione um Colegiado.');
    document.getElementById('selColegiado').focus();
    return false;
  }

  if (infraTrim(document.getElementById('txtSessao').value)==''){
    alert('Informe a Data da Sess�o.');
    document.getElementById('txtSessao').focus();
    return false;
  }

  if (!infraValidarDataHora(document.getElementById('txtSessao'))){
    return false;
  }

  if (!infraSelectSelecionado('selTipoSessao')) {
    alert('Selecione o Tipo da Sess�o.');
    document.getElementById('selTipoSessao').focus();
    return false;
  }
  return true;
}

function OnSubmitForm() {
  if (validarCadastro()) {
    document.getElementById('selColegiado').disabled=false;
    return true;
  };
  return false;
}

<?
if(0){?></script><?}
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmSessaoJulgamentoCadastro" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'])?>">
<?
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('30em');
?>
  <label id="lblColegiado" for="selColegiado" accesskey="" class="infraLabelObrigatorio">Colegiado:</label>
  <select id="selColegiado" name="selColegiado" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelColegiado?>
  </select>

  <label id="lblSessao" for="txtSessao" accesskey="" class="infraLabelObrigatorio">Data:</label>
  <input type="text" id="txtSessao" name="txtSessao" onkeypress="return infraMascara(this, event,'##/##/#### ##:##')" class="infraText" value="<?=$objSessaoJulgamentoDTO->getDthSessao();?>" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>" />
  <img id="imgCalSessao" title="Selecionar Data" alt="Selecionar Data" src="<?=PaginaSEI::getInstance()->getIconeCalendario()?>" class="infraImg" onclick="infraCalendario('txtSessao',this,true);" />

  <label id="lblTipoSessao" for="selTipoSessao" accesskey="" class="infraLabelObrigatorio">Tipo:</label>
  <select id="selTipoSessao" name="selTipoSessao" class="infraSelect" onchange="exibeDatasVirtual()" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelTipoSessao?>
  </select>

<? if ($_GET['acao']=='sessao_julgamento_alterar') {?>
  <label id="lblStaSituacao" for="selStaSituacao" accesskey="" class="infraLabelObrigatorio">Situa��o da Sess�o:</label>
  <select id="selStaSituacao" name="selStaSituacao" class="infraSelect" disabled="disabled" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
    <?=$strItensSelStaSituacao?>
  </select>
<?}?>
  <input type="hidden" id="hdnIdSessaoJulgamento" name="hdnIdSessaoJulgamento" value="<?=$objSessaoJulgamentoDTO->getNumIdSessaoJulgamento();?>" />
  <?
  PaginaSEI::getInstance()->fecharAreaDados();
  //PaginaSEI::getInstance()->montarAreaDebug();
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>