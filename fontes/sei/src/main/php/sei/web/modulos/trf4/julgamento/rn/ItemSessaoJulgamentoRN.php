<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 11/11/2014 - criado por bcu
 *
 * Vers�o do Gerador de C�digo: 1.33.1
 *
 * Vers�o no CVS: $Id$
 */

require_once __DIR__ . '/../../../../SEI.php';

class ItemSessaoJulgamentoRN extends InfraRN
{

  public static $STA_NORMAL = 'N';
  public static $STA_JULGADO = 'J';
  public static $STA_EM_JULGAMENTO = 'M';
  public static $STA_DILIGENCIA = 'D';
  public static $STA_ADIADO = 'A';
  public static $STA_RETIRADO = 'R';
  public static $STA_PEDIDO_VISTA = 'V';
  public static $STA_SESSAO_CANCELADA = 'C';
  public static $STA_IGNORADA = 'I'; //somente quando retirado com pauta aberta e existiam destaques

  public static $FORM_PROTOCOLO = 'PROTOCOLO';
  public static $FORM_RELATOR = 'RELATOR';
  public static $FORM_RELATOR_ACORDAO = 'RELATOR_ACORDAO';
//  public static $FORM_RESSALVAS = 'RESSALVAS';
  public static $FORM_DISPOSITIVO = 'DISPOSITIVO';
  public static $FORM_NOME_COLEGIADO = 'NOME_COLEGIADO';
  public static $FORM_DTH_SESSAO = 'DTH_SESSAO';
  public static $FORM_DTA_CANCELAMENTO_SESSAO = 'DTA_CANCELAMENTO';

  public function __construct(){
    parent::__construct();
  }

  protected function inicializarObjInfraIBanco():BancoSEI{
    return BancoSEI::getInstance();
  }


  public function listarValoresStaSituacao(): array
  {
    try {

      $arrObjTipoDTO = array();

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_NORMAL);
      $objTipoDTO->setStrDescricao('Normal');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_ADIADO);
      $objTipoDTO->setStrDescricao('Adiado');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_PEDIDO_VISTA);
      $objTipoDTO->setStrDescricao('Pedido de Vista');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_DILIGENCIA);
      $objTipoDTO->setStrDescricao('Convertido em Dilig�ncia');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_EM_JULGAMENTO);
      $objTipoDTO->setStrDescricao('Em Julgamento');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_JULGADO);
      $objTipoDTO->setStrDescricao('Julgado');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_RETIRADO);
      $objTipoDTO->setStrDescricao('Retirado');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_SESSAO_CANCELADA);
      $objTipoDTO->setStrDescricao('Sess�o Cancelada');
      $arrObjTipoDTO[] = $objTipoDTO;

      $objTipoDTO = new TipoDTO();
      $objTipoDTO->setStrStaTipo(self::$STA_IGNORADA);
      $objTipoDTO->setStrDescricao('Item Ignorado');
      $arrObjTipoDTO[] = $objTipoDTO;
      return $arrObjTipoDTO;

    } catch (Exception $e) {
      throw new InfraException('Erro listando valores de Situa��o.', $e);
    }
  }

  private function validarNumIdSessaoJulgamento(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento())) {
      $objInfraException->adicionarValidacao('Sess�o n�o informada.');
    }
  }
  private function validarNumIdMotivoMesa(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getNumIdMotivoMesa())) {
      $objInfraException->adicionarValidacao('Motivo para inclus�o em Mesa n�o informado.');
    }
  }
  private function validarNumIdDistribuicao(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getNumIdDistribuicao())) {
      $objItemSessaoJulgamentoDTO->setNumIdDistribuicao(null);
    }
  }
  private function validarNumIdSessaoBloco(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    $numIdSessaoBloco=$objItemSessaoJulgamentoDTO->getNumIdSessaoBloco();
    if (InfraString::isBolVazia($numIdSessaoBloco)) {
      $objInfraException->adicionarValidacao('Bloco de Sess�o n�o informado.');
      return;
    }
    $arrObjSessaoBlocoDTO=CacheSessaoJulgamentoRN::getArrObjSessaoBlocoDTO($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    if(!isset($arrObjSessaoBlocoDTO[$numIdSessaoBloco])){
      $objInfraException->adicionarValidacao('Bloco de Sess�o inv�lido.');
    }
    $objItemSessaoJulgamentoDTO->setStrStaTipoItemSessaoBloco($arrObjSessaoBlocoDTO[$numIdSessaoBloco]->getStrStaTipoItem());
    $objItemSessaoJulgamentoDTO->setStrDescricaoSessaoBloco($arrObjSessaoBlocoDTO[$numIdSessaoBloco]->getStrDescricao());
  }
  private function validarNumOrdem(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getNumOrdem())) {
      $objInfraException->adicionarValidacao('Ordem n�o informada.');
    }
  }
  private function validarStrDispositivo(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getStrDispositivo())) {
      $objItemSessaoJulgamentoDTO->setStrDispositivo(null);
    } else {
      $objItemSessaoJulgamentoDTO->setStrDispositivo(trim($objItemSessaoJulgamentoDTO->getStrDispositivo()));
      //if (strlen($objItemSessaoJulgamentoDTO->getStrDispositivo())>4000) {
      //  $objInfraException->adicionarValidacao('Dispositivo possui tamanho superior a 4000 caracteres.');
      //}
    }
  }
  private function validarDthInclusao(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getDthInclusao())) {
      $objInfraException->adicionarValidacao('Data de Inclus�o n�o informada.');
    } else if (!InfraData::validarDataHora($objItemSessaoJulgamentoDTO->getDthInclusao())) {
      $objInfraException->adicionarValidacao('Data de Inclus�o inv�lida.');
    }
  }
  private function validarStrStaSituacao(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO, InfraException $objInfraException){
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getStrStaSituacao())) {
      $objInfraException->adicionarValidacao('Situa��o n�o informada.');
    } else if (!in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(), InfraArray::converterArrInfraDTO($this->listarValoresStaSituacao(), 'StaTipo'))) {
      $objInfraException->adicionarValidacao('Situa��o inv�lida.');
    }
  }

  protected function cadastrarControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {
      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_cadastrar', __METHOD__, $objItemSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      $this->validarNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO, $objInfraException);
      $this->validarNumIdDistribuicao($objItemSessaoJulgamentoDTO, $objInfraException);
      $this->validarNumIdSessaoBloco($objItemSessaoJulgamentoDTO, $objInfraException);
      $this->validarStrStaSituacao($objItemSessaoJulgamentoDTO, $objInfraException);

      if ($objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco()==TipoSessaoBlocoRN::$STA_MESA) {
        $this->validarNumIdMotivoMesa($objItemSessaoJulgamentoDTO, $objInfraException);
      } else {
        $objItemSessaoJulgamentoDTO->setNumIdMotivoMesa(null);
      }
      $objInfraException->lancarValidacoes();

      $objDistribuicaoDTOBanco = new DistribuicaoDTO();
      $objDistribuicaoRN = new DistribuicaoRN();
      $objDistribuicaoDTOBanco->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      $objDistribuicaoDTOBanco->retStrStaDistribuicao();
      $objDistribuicaoDTOBanco->retNumIdDistribuicaoAgrupador();
      $objDistribuicaoDTOBanco->retNumIdUsuarioRelator();
      $objDistribuicaoDTOBanco->retNumIdUnidadeRelator();
      $objDistribuicaoDTOBanco->retStrNomeColegiado();
      $objDistribuicaoDTOBanco->retDblIdProcedimento();
      $objDistribuicaoDTOBanco->retStrProtocoloFormatado();
      $objDistribuicaoDTOBanco->retNumIdColegiadoColegiadoVersao();
      $objDistribuicaoDTOBanco = $objDistribuicaoRN->consultar($objDistribuicaoDTOBanco);
      if ($objDistribuicaoDTOBanco===null) {
        $objInfraException->lancarValidacao('Distribui��o n�o encontrada.');
      }
      $staDistribuicao = $objDistribuicaoDTOBanco->getStrStaDistribuicao();
      $objItemSessaoJulgamentoDTO->setNumIdUsuarioSessao($objDistribuicaoDTOBanco->getNumIdUsuarioRelator());
      $objItemSessaoJulgamentoDTO->setNumIdUnidadeSessao(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
      $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
      $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
      $objColegiadoComposicaoDTO->retNumIdUsuario();
      $objColegiadoComposicaoDTO->retNumIdUnidade();
//      $objColegiadoComposicaoDTO->setNumIdUsuario($objDistribuicaoDTOBanco->getNumIdUsuarioRelator());
      $arrObjColegiadoComposicaoDTO = $objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
      $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'IdUsuario');
//      if (!isset($arrObjColegiadoComposicaoDTO[$objDistribuicaoDTOBanco->getNumIdUsuarioRelator()])) {
//        $objInfraException->lancarValidacao('Relator n�o faz mais parte do Colegiado.');
//      }


      $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
      $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
      $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objSessaoJulgamentoDTO->retStrStaSituacao();
      $objSessaoJulgamentoDTO->retStrSinVirtualTipoSessao();
      $objSessaoJulgamentoDTO->retDthSessao();
      $objSessaoJulgamentoDTO->retNumIdColegiadoVersao();
      $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);
      if ($objSessaoJulgamentoDTO==null) {
        $objInfraException->lancarValidacao('Sess�o de Julgamento n�o encontrada.');
      }
      $staSessao = $objSessaoJulgamentoDTO->getStrStaSituacao();
      $bolSessaoVirtual=($objSessaoJulgamentoDTO->getStrSinVirtualTipoSessao()=='S');

      $novaStaDistribuicao = null;
      $staTarefa = null;
      $objPedidoVistaDTO = null;
      switch ($objItemSessaoJulgamentoDTO->getStrStaTipoItemSessaoBloco()) {
        case TipoSessaoBlocoRN::$STA_PAUTA:
          $staTarefa = TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_INCLUSAO_PAUTA;
          $objItemSessaoJulgamentoDTO->setNumIdUsuarioSessao(ColegiadoComposicaoINT::getIdUsuarioMembroUnidadeAtual($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao()));
//          $objItemSessaoJulgamentoDTO->setNumIdUnidadeSessao($objDistribuicaoDTOBanco->getNumIdUnidadeRelator());
          $novaStaDistribuicao = DistribuicaoRN::$STA_PAUTADO;
          if ($staSessao!=SessaoJulgamentoRN::$ES_PAUTA_ABERTA) {
            $objInfraException->lancarValidacao('N�o foi poss�vel pautar o processo: Pauta da Sess�o de Julgamento n�o est� aberta.');
          }
          if (!in_array($staDistribuicao, array(DistribuicaoRN::$STA_DISTRIBUIDO,DistribuicaoRN::$STA_PEDIDO_VISTA, DistribuicaoRN::$STA_REDISTRIBUIDO, DistribuicaoRN::$STA_ADIADO, DistribuicaoRN::$STA_DILIGENCIA, DistribuicaoRN::$STA_RETIRADO_PAUTA))) {
            $objInfraException->lancarValidacao('N�o foi poss�vel pautar o processo: Situa��o da distribui��o n�o permite.');
          }
          if ($staDistribuicao==DistribuicaoRN::$STA_PEDIDO_VISTA) {
            //validar se unidade que pediu vista � a atual - n�o aceita pedido vista multiplo
            $objPedidoVistaDTO = new PedidoVistaDTO();
            $objPedidoVistaRN = new PedidoVistaRN();
            $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
            $objPedidoVistaDTO->setDthDevolucao(null);
            $objPedidoVistaDTO->retTodos();

            ////////////////////////////////////////////////////
            $objPedidoVistaDTO->setNumMaxRegistrosRetorno(1);
            $objPedidoVistaDTO->setOrdNumIdPedidoVista(InfraDTO::$TIPO_ORDENACAO_DESC);
            ////////////////////////////////////////////////////

            $objPedidoVistaDTO = $objPedidoVistaRN->consultar($objPedidoVistaDTO);
            if (SessaoSEI::getInstance()->getNumIdUnidadeAtual()!=$objPedidoVistaDTO->getNumIdUnidade()) {
              $objInfraException->lancarValidacao("N�o foi poss�vel colocar o processo em Pauta. Processo em vista por outra unidade.");
            }
            break;
          }
          break;
        case TipoSessaoBlocoRN::$STA_MESA:
          //buscar membro da unidade atual
          $objItemSessaoJulgamentoDTO->setNumIdUsuarioSessao(ColegiadoComposicaoINT::getIdUsuarioMembroUnidadeAtual($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao()));
          $strMesa=$objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco();
          $staTarefa = TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_INCLUSAO_MESA;
          $novaStaDistribuicao = DistribuicaoRN::$STA_EM_MESA;
          if($bolSessaoVirtual){
            if($staSessao!=SessaoJulgamentoRN::$ES_PAUTA_ABERTA){
              $objInfraException->lancarValidacao("N�o � poss�vel colocar em $strMesa de sess�o Virtual ap�s fechamento da pauta.");
            }
          } elseif (!in_array($staSessao, array(SessaoJulgamentoRN::$ES_PAUTA_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_FECHADA, SessaoJulgamentoRN::$ES_SUSPENSA,SessaoJulgamentoRN::$ES_ABERTA))) {
            $objInfraException->lancarValidacao("N�o foi poss�vel colocar o processo em $strMesa: Situa��o da Sess�o de Julgamento n�o permite.");
          }
          if ($staDistribuicao==DistribuicaoRN::$STA_PEDIDO_VISTA) {
            //validar se unidade que pediu vista � a atual - n�o aceita pedido vista multiplo
            $objPedidoVistaDTO = new PedidoVistaDTO();
            $objPedidoVistaRN = new PedidoVistaRN();
            $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
            $objPedidoVistaDTO->setDthDevolucao(null);
            $objPedidoVistaDTO->retTodos();

            ////////////////////////////////////////////////////
            $objPedidoVistaDTO->setNumMaxRegistrosRetorno(1);
            $objPedidoVistaDTO->setOrdNumIdPedidoVista(InfraDTO::$TIPO_ORDENACAO_DESC);
            ////////////////////////////////////////////////////

            $objPedidoVistaDTO = $objPedidoVistaRN->consultar($objPedidoVistaDTO);
            if (SessaoSEI::getInstance()->getNumIdUnidadeAtual()!=$objPedidoVistaDTO->getNumIdUnidade()) {
              $objInfraException->lancarValidacao("N�o foi poss�vel colocar o processo em $strMesa. Processo em vista por outra unidade.");
            }
            break;
          }
          if (in_array($staDistribuicao, array(DistribuicaoRN::$STA_DISTRIBUIDO, DistribuicaoRN::$STA_REDISTRIBUIDO, DistribuicaoRN::$STA_ADIADO, DistribuicaoRN::$STA_DILIGENCIA, DistribuicaoRN::$STA_RETIRADO_PAUTA))) {
            break;
          }
          $objInfraException->lancarValidacao("N�o foi poss�vel colocar em $strMesa o processo.");
          break;
        case TipoSessaoBlocoRN::$STA_REFERENDO:
          $staTarefa = TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_INCLUSAO_REFERENDO;
          $novaStaDistribuicao = DistribuicaoRN::$STA_PARA_REFERENDO;

          $objColegiadoDTO = new ColegiadoDTO();
          $objColegiadoDTO->setNumIdColegiado($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
          $objColegiadoDTO->retNumIdUsuarioPresidente();
          $objColegiadoRN = new ColegiadoRN();
          $objColegiadoDTO = $objColegiadoRN->consultar($objColegiadoDTO);

          if (!isset($arrObjColegiadoComposicaoDTO[$objColegiadoDTO->getNumIdUsuarioPresidente()]) || $arrObjColegiadoComposicaoDTO[$objColegiadoDTO->getNumIdUsuarioPresidente()]->getNumIdUnidade()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()) {
            $objInfraException->lancarValidacao('Somente a unidade do presidente definido no cadastro do colegiado pode incluir referendos.');
          }

          if ($staDistribuicao===DistribuicaoRN::$STA_DISTRIBUIDO || $staDistribuicao===DistribuicaoRN::$STA_REDISTRIBUIDO) {
            if ($staSessao==SessaoJulgamentoRN::$ES_PAUTA_ABERTA || $staSessao==SessaoJulgamentoRN::$ES_PAUTA_FECHADA || (!$bolSessaoVirtual && $staSessao==SessaoJulgamentoRN::$ES_ABERTA)) {
              break;
            }
          }
          $objInfraException->lancarValidacao('N�o foi poss�vel incluir o processo para referendo.');

      }


      $objDistribuicaoDTO = new DistribuicaoDTO();
      $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      $objDistribuicaoDTO->setStrStaDistribuicao($novaStaDistribuicao);
      $objDistribuicaoRN->alterar($objDistribuicaoDTO);

      //setar numero de ordem
      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->setNumMaxRegistrosRetorno(1);
      $objItemSessaoJulgamentoDTOBanco->setNumIdSessaoBloco($objItemSessaoJulgamentoDTO->getNumIdSessaoBloco());
      $objItemSessaoJulgamentoDTOBanco->setNumIdUsuarioSessao($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao());
      $objItemSessaoJulgamentoDTOBanco->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_DESC);
      $objItemSessaoJulgamentoDTOBanco->retNumOrdem();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);
      if ($objItemSessaoJulgamentoDTOBanco) {
        $objItemSessaoJulgamentoDTO->setNumOrdem($objItemSessaoJulgamentoDTOBanco->getNumOrdem() + 1);
      } else {
        $objItemSessaoJulgamentoDTO->setNumOrdem(1);
      }
      $objItemSessaoJulgamentoDTO->setDthInclusao(InfraData::getStrDataHoraAtual());
      $objItemSessaoJulgamentoDTO->setStrStaSituacao(self::$STA_NORMAL);


      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());

      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
      $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao(self::$STA_IGNORADA);
      $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);
      if ($objItemSessaoJulgamentoDTOBanco) {
        $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
        $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTO);
        $ret = $objItemSessaoJulgamentoDTO;
      } else {
        $objItemSessaoJulgamentoDTO->setStrSinManual('N');
        $ret = $objItemSessaoJulgamentoBD->cadastrar($objItemSessaoJulgamentoDTO);

        //s� verifica sess�o anterior se n�o houver aproveitamento de item_sessao
        $objJulgamentoParteRN = new JulgamentoParteRN();
        $objVotoParteRN = new VotoParteRN();

        $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();

        $objItemSessaoJulgamentoDTOBanco->setNumIdDistribuicaoAgrupadorDistribuicao($objDistribuicaoDTOBanco->getNumIdDistribuicaoAgrupador());
        $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($ret->getNumIdItemSessaoJulgamento(), InfraDTO::$OPER_DIFERENTE);
        $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao(array(self::$STA_SESSAO_CANCELADA, self::$STA_IGNORADA, self::$STA_RETIRADO,  self::$STA_DILIGENCIA), InfraDTO::$OPER_NOT_IN);
        $objItemSessaoJulgamentoDTOBanco->setNumMaxRegistrosRetorno(1);
        $objItemSessaoJulgamentoDTOBanco->setOrdDthInclusao(InfraDTO::$TIPO_ORDENACAO_DESC);
        $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
        $objItemSessaoJulgamentoDTOBanco->retStrSinManual();
        $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);
        //se possui sess�o anterior copiar dados.
        if ($objItemSessaoJulgamentoDTOBanco!=null) {
          //se for manual permanece manual
          $bolManual=$objItemSessaoJulgamentoDTOBanco->getStrSinManual()==='S';
          //se relator estiver pautando verifica se tem voto de outro relator (redistribu�do) para converter para manual
          if (!$bolManual && $staDistribuicao!=DistribuicaoRN::$STA_PEDIDO_VISTA) {
            $objVotoParteDTO=new VotoParteDTO();
            $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_ACOMPANHA);
            $objVotoParteDTO->setNumIdVotoParteAssociado(null);
            $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
            $objVotoParteDTO->setNumIdProvimento(null,InfraDTO::$OPER_DIFERENTE);
            $objVotoParteDTO->setNumIdUsuario($objItemSessaoJulgamentoDTO->getNumIdUsuarioSessao(),InfraDTO::$OPER_DIFERENTE);
            if($objVotoParteRN->contar($objVotoParteDTO)>0){
              $bolManual=true;
              //gera andamento de adiamento na sess�o
              $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
              $objAndamentoSessaoRN = new AndamentoSessaoRN();

              $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
              $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_CONVERSAO_JULGAMENTO_MANUAL);

              $arrObjAtributoAndamentoSessaoDTO = array();

              $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
              $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
              $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objDistribuicaoDTOBanco->getDblIdProcedimento());
              $objAtributoAndamentoSessaoDTO->setStrValor($objDistribuicaoDTOBanco->getStrProtocoloFormatado());
              $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

              $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
              $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);
            }
          }
          if($bolManual){
            $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
            $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($ret->getNumIdItemSessaoJulgamento());
            $objItemSessaoJulgamentoDTO2->setStrSinManual('S');
            $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTO2);
          }
          //copiar documentos
          $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
          $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
          $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
          $objItemSessaoDocumentoDTO->retTodos();
          $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
          foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
            $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($ret->getNumIdItemSessaoJulgamento());
            $objItemSessaoDocumentoDTO->setNumIdItemSessaoDocumento(null);
            $objItemSessaoDocumentoRN->acumular($objItemSessaoDocumentoDTO);
          }
          $objItemSessaoDocumentoRN->acumular(null);

          $objJulgamentoParteDTOBanco = new JulgamentoParteDTO();
          $objJulgamentoParteDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
          $objJulgamentoParteDTOBanco->retTodos();
          $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTOBanco);
          if (!$bolManual && InfraArray::contar($arrObjJulgamentoParteDTO)>0) {
            foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTOBanco) {
              $objVotoParteDTO = new VotoParteDTO();
              $objVotoParteDTO->setNumIdJulgamentoParte($objJulgamentoParteDTOBanco->getNumIdJulgamentoParte());
              $objVotoParteDTO->setStrStaVotoParte(array(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO, VotoParteRN::$STA_PEDIDO_VISTA), InfraDTO::$OPER_NOT_IN);
              $objVotoParteDTO->retTodos();
              $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);
              $objJulgamentoParteDTOBanco->setNumIdItemSessaoJulgamento($ret->getNumIdItemSessaoJulgamento());
              $dto = $objJulgamentoParteRN->cadastrar($objJulgamentoParteDTOBanco);
              foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
                $objVotoParteDTO->setNumIdJulgamentoParte($dto->getNumIdJulgamentoParte());
                $objVotoParteDTO->setNumIdItemSessaoJulgamento($dto->getNumIdItemSessaoJulgamento());
              }
              $objVotoParteRN->copiarVotos($arrObjVotoParteDTO);
            }
          } else {
            //cadastra parte do item para julgamento
            $objJulgamentoParteDTO = new JulgamentoParteDTO();
            $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($ret->getNumIdItemSessaoJulgamento());
            $objJulgamentoParteDTO->setStrDescricao('Integral');
            $objJulgamentoParteDTO->setNumOrdem(1);
            $objJulgamentoParteRN->cadastrar($objJulgamentoParteDTO);
          }
        } else {
          //cadastra parte do item para julgamento
          $objJulgamentoParteDTO = new JulgamentoParteDTO();
          $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($ret->getNumIdItemSessaoJulgamento());
          $objJulgamentoParteDTO->setStrDescricao('Integral');
          $objJulgamentoParteDTO->setNumOrdem(1);
          $objJulgamentoParteRN->cadastrar($objJulgamentoParteDTO);
        }
      }

      //se n�o for o relator, descarta a lista de bloqueios (admin altera pela tela de sess�o)
      if($objDistribuicaoDTOBanco->getNumIdUnidadeRelator()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()){
        $objItemSessaoJulgamentoDTO->unSetArrObjBloqueioItemSessUnidadeDTO();
      }

      //cadastra bloqueio das unidades
      if($objItemSessaoJulgamentoDTO->isSetArrObjBloqueioItemSessUnidadeDTO()){
        //excluir bloqueios anteriores
        $objBloqueioItemSessUnidadeRN=new BloqueioItemSessUnidadeRN();
        $objBloqueioItemSessUnidadeDTO=new BloqueioItemSessUnidadeDTO();
        $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
        $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);
        $objBloqueioItemSessUnidadeDTO->retTodos();
        $objBloqueioItemSessUnidadeRN->excluir($objBloqueioItemSessUnidadeRN->listar($objBloqueioItemSessUnidadeDTO));

        $arrObjBloqueioItemSessUnidadeDTO=$objItemSessaoJulgamentoDTO->getArrObjBloqueioItemSessUnidadeDTO();
        if(InfraArray::contar($arrObjBloqueioItemSessUnidadeDTO)>0){
          /** @var BloqueioItemSessUnidadeDTO $objBloqueioItemSessUnidadeDTO */
          foreach ($arrObjBloqueioItemSessUnidadeDTO as $objBloqueioItemSessUnidadeDTO) {
            $objBloqueioItemSessUnidadeDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
            $objBloqueioItemSessUnidadeDTO->setStrStaTipo(BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO);
            $objBloqueioItemSessUnidadeDTO->setDblIdDocumento(null);
            $objBloqueioItemSessUnidadeRN->cadastrar($objBloqueioItemSessUnidadeDTO);
          }
        }
      }


      //gera andamento de inclusao no processo
      $arrObjAtributoAndamentoDTO = array();
      $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
      $objAtributoAndamentoDTO->setStrNome('COLEGIADO');
      $objAtributoAndamentoDTO->setStrValor($objDistribuicaoDTOBanco->getStrNomeColegiado());
      $objAtributoAndamentoDTO->setStrIdOrigem($objDistribuicaoDTOBanco->getNumIdColegiadoColegiadoVersao());
      $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

      $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
      $objAtributoAndamentoDTO->setStrNome('SESSAO');
      $objAtributoAndamentoDTO->setStrValor(substr($objSessaoJulgamentoDTO->getDthSessao(), 0, - 3));
      $objAtributoAndamentoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

      $objAtividadeDTO = new AtividadeDTO();
      $objAtividadeDTO->setDblIdProtocolo($objDistribuicaoDTOBanco->getDblIdProcedimento());
      $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
      $objAtividadeDTO->setStrIdTarefaModuloTarefa($staTarefa);
      $objAtividadeDTO->setArrObjAtributoAndamentoDTO($arrObjAtributoAndamentoDTO);

      $objAtividadeRN = new AtividadeRN();
      $objAtividadeRN->gerarInternaRN0727($objAtividadeDTO);

      //gera andamento de inclusao na sess�o
      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();

      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_INCLUSAO_ITEM);

      $arrObjAtributoAndamentoSessaoDTO = array();

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objDistribuicaoDTOBanco->getDblIdProcedimento());
      $objAtributoAndamentoSessaoDTO->setStrValor($objDistribuicaoDTOBanco->getStrProtocoloFormatado());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('TIPO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO->getNumIdSessaoBloco());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrDescricaoSessaoBloco());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoRN=new EventoSessaoRN();
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($ret->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($ret->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_INCLUSAO_ITEM);
      $objEventoSessaoDTO->setStrDescricao('');
      $objEventoSessaoRN->lancar($objEventoSessaoDTO);

      $ret->setDblIdProcedimentoDistribuicao($objDistribuicaoDTOBanco->getDblIdProcedimento());
      return $ret;

    } catch (Exception $e) {
      throw new InfraException('Erro cadastrando Item da Sess�o.', $e);
    }
  }

  protected function alterarControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_alterar', __METHOD__, $objItemSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objItemSessaoJulgamentoDTO->isSetNumIdSessaoJulgamento()) {
        $this->validarNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO, $objInfraException);
      }

      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoBloco();
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrDispositivo();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioPresidenteSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

      if ($objItemSessaoJulgamentoDTO->isSetNumIdDistribuicao()) {
        $this->validarNumIdDistribuicao($objItemSessaoJulgamentoDTO, $objInfraException);
      }
      if ($objItemSessaoJulgamentoDTO->isSetNumOrdem()) {
        $this->validarNumOrdem($objItemSessaoJulgamentoDTO, $objInfraException);
      }

      if ($objItemSessaoJulgamentoDTO->isSetStrDispositivo()) {
        $this->validarStrDispositivo($objItemSessaoJulgamentoDTO, $objInfraException);
        if ($objItemSessaoJulgamentoDTO->getStrDispositivo()!=$objItemSessaoJulgamentoDTOBanco->getStrDispositivo()) {
          $objItemSessaoJulgamentoDTO->setNumIdUsuarioPresidente($objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioPresidenteSessaoJulgamento());
          //sessao encerrada n�o permite excluir dispositivo e audita a altera��o
          if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_ENCERRADA) {
            if ($objItemSessaoJulgamentoDTO->getStrDispositivo()==null) {
              $objInfraException->lancarValidacao('Dispositivo n�o pode ser apagado em sess�o encerrada');
            }
            $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
            $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
            $objItemSessaoJulgamentoDTO2->setStrDispositivoAnterior($objItemSessaoJulgamentoDTOBanco->getStrDispositivo());
            $objItemSessaoJulgamentoDTO2->setStrDispositivo($objItemSessaoJulgamentoDTO->getStrDispositivo());
            $this->alterarDispositivoEncerrado($objItemSessaoJulgamentoDTO2);
            $objItemSessaoJulgamentoDTO->unSetStrDispositivo();
            $objItemSessaoJulgamentoDTO->unSetNumIdUsuarioPresidente();
          } else if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA) {
            $objInfraException->lancarValidacao('Dispositivo n�o pode ser alterado. Situa��o da sess�o n�o permite.');
          }
        } else {
          $objItemSessaoJulgamentoDTO->unSetStrDispositivo();
        }
        if($objItemSessaoJulgamentoDTO->isSetNumIdUsuarioRelatorAcordaoDistribuicao() && $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao()!=null){
          $idUsuarioAcordao=$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao();
          $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
          $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
          $objColegiadoComposicaoDTO->setNumIdUsuario($idUsuarioAcordao);
          $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objItemSessaoJulgamentoDTOBanco->getNumIdColegiadoSessaoJulgamento());
          $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
          $objColegiadoComposicaoDTO->retNumIdUnidade();
          $objColegiadoComposicaoDTO->retNumIdUsuario();
          $objColegiadoComposicaoDTO=$objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);

          if($objColegiadoComposicaoDTO==null){
            throw new InfraException('Usu�rio Relator para ac�rd�o n�o foi encontrado no colegiado.');
          }
          $objDistribuicaoDTO=new DistribuicaoDTO();
          $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao());

          if($objColegiadoComposicaoDTO->getNumIdUsuario()==$objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioRelatorDistribuicao()){
            $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao(null);
            $objDistribuicaoDTO->setNumIdUnidadeRelatorAcordao(null);
          } else {
            $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao($objColegiadoComposicaoDTO->getNumIdUsuario());
            $objDistribuicaoDTO->setNumIdUnidadeRelatorAcordao($objColegiadoComposicaoDTO->getNumIdUnidade());
          }
          $objDistribuicaoRN=new DistribuicaoRN();
          $objDistribuicaoRN->alterar($objDistribuicaoDTO);
        }
      }
      if ($objItemSessaoJulgamentoDTO->isSetDthInclusao()) {
        $this->validarDthInclusao($objItemSessaoJulgamentoDTO, $objInfraException);
      }
      if ($objItemSessaoJulgamentoDTO->isSetNumIdSessaoBloco()) {
        $this->validarNumIdSessaoBloco($objItemSessaoJulgamentoDTO, $objInfraException);
      } else {
        $objItemSessaoJulgamentoDTO->setNumIdSessaoBloco($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoBloco());
      }

      if ($objItemSessaoJulgamentoDTO->isSetStrStaSituacao()) {
        $this->validarStrStaSituacao($objItemSessaoJulgamentoDTO, $objInfraException);
      }


      $objInfraException->lancarValidacoes();

      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTO);

      //lancar eventos para atualizar clientes
      if($objItemSessaoJulgamentoDTO->isSetStrDispositivo()){
        $objEventoSessaoDTO=new EventoSessaoDTO();
        $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_DISPOSITIVO);
        $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());

        $strDispositivo=$objItemSessaoJulgamentoDTO->getStrDispositivo();
        if($strDispositivo!=null){
          $pos=strrpos($strDispositivo,"\nPresentes:");
          if ($pos!==false) {
            $strDispositivo=substr($strDispositivo,0,$pos);
          }
        }

        $objEventoSessaoDTO->setStrDescricao(nl2br(trim($strDispositivo)));
        $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
        $objEventoSessaoRN=new EventoSessaoRN();
        $objEventoSessaoRN->lancar($objEventoSessaoDTO);
      }
      //Auditoria

    } catch (Exception $e) {
      throw new InfraException('Erro alterando Item da Sess�o.', $e);
    }
  }

  private function alterarDispositivoEncerrado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_alterar_dispositivo', __METHOD__, $objItemSessaoJulgamentoDTO);

      //Regras de Negocio
      $objInfraException = new InfraException();

      if ($objItemSessaoJulgamentoDTO->getStrDispositivo()==null) {
        $objInfraException->lancarValidacao('Dispositivo n�o pode ser apagado em sess�o encerrada');
      }

      $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO2->setStrDispositivo($objItemSessaoJulgamentoDTO->getStrDispositivo());

      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTO2);


    } catch (Exception $e) {
      throw new InfraException('Erro alterando Item da Sess�o.', $e);
    }
  }

  protected function excluirControlado($arrObjItemSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_excluir', __METHOD__, $arrObjItemSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      if (InfraArray::contar($arrObjItemSessaoJulgamentoDTO)==0) {
        return;
      }

      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());

      foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
        $objItemSessaoJulgamentoBD->excluir($objItemSessaoJulgamentoDTO);
      }

      //excluir documentos
      $arrIdItemSessaoJulgamento = InfraArray::converterArrInfraDTO($arrObjItemSessaoJulgamentoDTO, 'IdItemSessaoJulgamento');
      $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($arrIdItemSessaoJulgamento, InfraDTO::$OPER_IN);
      $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
      $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
      $objItemSessaoDocumentoRN->excluir($objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO));

      //Auditoria

    } catch (Exception $e) {
      throw new InfraException('Erro excluindo Item da Sess�o.', $e);
    }
  }

  protected function consultarConectado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_consultar', __METHOD__, $objItemSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      //Auditoria

      return $objItemSessaoJulgamentoBD->consultar($objItemSessaoJulgamentoDTO);
    } catch (Exception $e) {
      throw new InfraException('Erro consultando Item da Sess�o.', $e);
    }
  }

  protected function listarConectado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
//      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_listar',__METHOD__,$objItemSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      //Auditoria

      return $objItemSessaoJulgamentoBD->listar($objItemSessaoJulgamentoDTO);

    } catch (Exception $e) {
      throw new InfraException('Erro listando Itens da Sess�o.', $e);
    }
  }

  protected function contarConectado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_listar', __METHOD__, $objItemSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      //Auditoria

      return $objItemSessaoJulgamentoBD->contar($objItemSessaoJulgamentoDTO);
    } catch (Exception $e) {
      throw new InfraException('Erro contando Itens da Sess�o.', $e);
    }
  }

  protected function bloquearControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {

      //Valida Permissao
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_consultar', __METHOD__, $objItemSessaoJulgamentoDTO);

      //Regras de Negocio
      //$objInfraException = new InfraException();

      //$objInfraException->lancarValidacoes();

      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      //Auditoria

      return $objItemSessaoJulgamentoBD->bloquear($objItemSessaoJulgamentoDTO);
    } catch (Exception $e) {
      throw new InfraException('Erro bloqueando Item da Sess�o.', $e);
    }
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @throws InfraException
   *
   * Adia o julgamento do processo
   * -> sess�o deve estar aberta
   * -> processo deve estar em julgamento (sem pedido de vista) ou n�o iniciado o julgamento
   */
  protected function adiarControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_adiar', __METHOD__, $objItemSessaoJulgamentoDTO);

      $objInfraException = new InfraException();

      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioSessao();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTOBanco->retStrNomeColegiado();
      $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
//      $objItemSessaoJulgamentoDTOBanco->retNumIdUnidadeColegiadoComposicao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaTipoItemSessaoBloco();
      $objItemSessaoJulgamentoDTOBanco->retStrDescricaoSessaoBloco();
      $objItemSessaoJulgamentoDTOBanco->retStrStaDistribuicaoAnterior();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

      if ($objItemSessaoJulgamentoDTOBanco==null) {
        $objInfraException->lancarValidacao('Item da sess�o de julgamento n�o encontrado.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA) {
        $objInfraException->lancarValidacao('S� � permitido adiar julgamento com a sess�o Aberta.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaTipoItemSessaoBloco()==TipoSessaoBlocoRN::$STA_REFERENDO) {
        $objInfraException->lancarValidacao('N�o � poss�vel adiar '.InfraString::transformarCaixaBaixa($objItemSessaoJulgamentoDTOBanco->getStrDescricaoSessaoBloco()).'.');
      }


      switch ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacao()) {
        case ItemSessaoJulgamentoRN::$STA_NORMAL:
        case ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO:
          break;
        case ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA:
          $objInfraException->lancarValidacao('Pedido de vista solicitado nesta sess�o.');
          break;
        default:
          $objInfraException->lancarValidacao('N�o � poss�vel adiar este processo.');
      }

      if($objItemSessaoJulgamentoDTOBanco->getStrStaDistribuicaoAnterior()==DistribuicaoRN::$STA_PEDIDO_VISTA){
        $idOrigem='';
        switch($objItemSessaoJulgamentoDTO->getStrComplemento()){
          case 'R'://relator
            $idOrigem=$objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioRelatorDistribuicao();
            break;
          case 'V'://vista
            $idOrigem=$objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioSessao();
            break;
          default:
            $objInfraException->lancarValidacao('Encaminhamento do processo n�o selecionado');
        }
      }

      $objItemSessaoJulgamentoDTOBanco->setStrDispositivo("Julgamento adiado.\nMotivo: " . $objItemSessaoJulgamentoDTO->getStrMotivoRetirada());
      $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao(self::$STA_ADIADO);
      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTOBanco);

      //gera andamento de adiamento na sess�o
      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();

      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_PROCESSO_ADIADO);

      $arrObjAtributoAndamentoSessaoDTO = array();

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('MOTIVO');
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrMotivoRetirada());
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem(null);
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      if($objItemSessaoJulgamentoDTOBanco->getStrStaDistribuicaoAnterior()==DistribuicaoRN::$STA_PEDIDO_VISTA){
        $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
        $objAtributoAndamentoSessaoDTO->setStrChave('ENCAMINHAMENTO');
        $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrComplemento());
        $objAtributoAndamentoSessaoDTO->setStrIdOrigem($idOrigem);
        $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;
      }


      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoRN=new EventoSessaoRN();
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
      $objEventoSessaoDTO->setStrDescricao('Adiado');
      $objEventoSessaoRN->lancar($objEventoSessaoDTO);
    } catch (Exception $e) {
      throw new InfraException('Erro adiando julgamento do processo.', $e);
    }

  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @throws InfraException
   *
   * Cancela o adiamento do processo
   * -> sess�o deve estar aberta
   * -> processo deve estar adiado
   */
  protected function cancelarAdiamentoControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_adiar', __METHOD__, $objItemSessaoJulgamentoDTO);

      $objInfraException = new InfraException();

      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

      if ($objItemSessaoJulgamentoDTOBanco==null) {
        $objInfraException->lancarValidacao('Item da sess�o de julgamento n�o encontrado.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA) {
        $objInfraException->lancarValidacao('S� � permitido cancelar adiamento com a sess�o Aberta.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacao()!=ItemSessaoJulgamentoRN::$STA_ADIADO) {
        $objInfraException->lancarValidacao('N�o � poss�vel cancelar adiamento deste processo.');
      }

      $novoStaSituacao = ItemSessaoJulgamentoRN::$STA_NORMAL;

      $objVotoParteDTO = new VotoParteDTO();
      $objVotoParteRN = new VotoParteRN();
      $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO, InfraDTO::$OPER_DIFERENTE);
      $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      if ($objVotoParteRN->contar($objVotoParteDTO)>0) {
        $novoStaSituacao = ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO;
      }

      $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao($novoStaSituacao);
      $objItemSessaoJulgamentoDTOBanco->setStrDispositivo(null);
      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTOBanco);

      //gera andamento de adiamento na sess�o
      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();

      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_CANCELAR_ADIAMENTO);

      $arrObjAtributoAndamentoSessaoDTO = array();
      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoRN=new EventoSessaoRN();
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
      $objEventoSessaoDTO->setStrDescricao('Adiamento cancelado');
      $objEventoSessaoRN->lancar($objEventoSessaoDTO);
    } catch (Exception $e) {
      throw new InfraException('Erro cancelando adiamento do processo.', $e);
    }
  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @throws InfraException
   *
   * Converte o julgamento do processo em dilig�ncia
   * -> sess�o deve estar aberta
   * -> processo deve estar em julgamento (sem pedido de vista) ou n�o iniciado o julgamento
   */
  protected function converterDiligenciaControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_adiar', __METHOD__, $objItemSessaoJulgamentoDTO);

      $objInfraException = new InfraException();

      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTOBanco->retStrNomeColegiado();
      $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaTipoItemSessaoBloco();
      $objItemSessaoJulgamentoDTOBanco->retStrDescricaoSessaoBloco();
      $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

      if ($objItemSessaoJulgamentoDTOBanco==null) {
        $objInfraException->lancarValidacao('Item da sess�o de julgamento n�o encontrado.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA) {
        $objInfraException->lancarValidacao('S� � permitido converter em dilig�ncia com a sess�o Aberta.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaTipoItemSessaoBloco()==TipoSessaoBlocoRN::$STA_REFERENDO) {
        $objInfraException->lancarValidacao('N�o � poss�vel converter '.InfraString::transformarCaixaBaixa($objItemSessaoJulgamentoDTOBanco->getStrDescricaoSessaoBloco()).' em dilig�ncia.');
      }


      switch ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacao()) {
        case ItemSessaoJulgamentoRN::$STA_NORMAL:
          break;
        case ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA:
        case ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO:
//          $objVotoParteDTO = new VotoParteDTO();
//          $objVotoParteRN = new VotoParteRN();
//          $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
//          $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
//          if ($objVotoParteRN->contar($objVotoParteDTO) > 0) {
//            $objInfraException->lancarValidacao('Vota��o deste processo j� iniciada nesta sess�o.');
//          }
          break;
        default:
          $objInfraException->lancarValidacao('N�o � poss�vel converter este julgamento em dilig�ncia.');
      }

      $objItemSessaoJulgamentoDTOBanco->setStrDispositivo('Julgamento convertido em dilig�ncia.');
      $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao(self::$STA_DILIGENCIA);
      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTOBanco);

      //gera andamento de adiamento na sess�o
      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();

      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_CONVERSAO_DILIGENCIA);

      $arrObjAtributoAndamentoSessaoDTO = array();

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);
      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoRN=new EventoSessaoRN();
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
      $objEventoSessaoDTO->setStrDescricao('Dilig�ncia');
      $objEventoSessaoRN->lancar($objEventoSessaoDTO);
    } catch (Exception $e) {
      throw new InfraException('Erro convertendo julgamento do processo em dilig�ncia.', $e);
    }

  }

  /**
   * @param ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO
   * @throws InfraException
   *
   * Cancela a convers�o do julgamento do processo em dilig�ncia
   * -> sess�o deve estar aberta
   * -> processo deve estar em_diligencia
   */
  protected function cancelarDiligenciaControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_adiar', __METHOD__, $objItemSessaoJulgamentoDTO);

      $objInfraException = new InfraException();

      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTOBanco->retStrNomeColegiado();
      $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

      if ($objItemSessaoJulgamentoDTOBanco==null) {
        $objInfraException->lancarValidacao('Item da sess�o de julgamento n�o encontrado.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA) {
        $objInfraException->lancarValidacao('S� � permitido cancelar dilig�ncia com a sess�o Aberta.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacao()!=ItemSessaoJulgamentoRN::$STA_DILIGENCIA) {
        $objInfraException->lancarValidacao('N�o � poss�vel cancelar dilig�ncia deste processo.');
      }

      $novoStaSituacao = ItemSessaoJulgamentoRN::$STA_NORMAL;

      $objPedidoVistaRN = new PedidoVistaRN();
      $objPedidoVistaDTO = new PedidoVistaDTO();
      $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao());
      $objPedidoVistaDTO->setDthDevolucao(null);
      if ($objPedidoVistaRN->contar($objPedidoVistaDTO)>0) {
        $novoStaSituacao = ItemSessaoJulgamentoRN::$STA_PEDIDO_VISTA;
      } else {
        $objVotoParteDTO = new VotoParteDTO();
        $objVotoParteRN = new VotoParteRN();
        $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
        $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
        $objVotoParteDTO->setStrStaVotoParte(VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO, InfraDTO::$OPER_DIFERENTE);
        if ($objVotoParteRN->contar($objVotoParteDTO)>0) {
          $novoStaSituacao = ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO;
        }
      }
      $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao($novoStaSituacao);
      $objItemSessaoJulgamentoDTOBanco->setStrDispositivo(null);
      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTOBanco);

      //gera andamento de adiamento na sess�o
      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();

      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_CANCELAR_CONVERSAO_DILIGENCIA);

      $arrObjAtributoAndamentoSessaoDTO = array();

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoRN=new EventoSessaoRN();
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
      $objEventoSessaoDTO->setStrDescricao('Dilig�ncia cancelada');
      $objEventoSessaoRN->lancar($objEventoSessaoDTO);

    } catch (Exception $e) {
      throw new InfraException('Erro cancelando dilig�ncia.', $e);
    }

  }

  protected function retirarControlado(array $arrObjItemSessaoJulgamentoDTO)
  {
    foreach ($arrObjItemSessaoJulgamentoDTO as $objItemSessaoJulgamentoDTO) {
      $this->retirarInterno($objItemSessaoJulgamentoDTO);
    }

  }

  private function retirarInterno(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {

    $objInfraException = new InfraException();
    //validar motivo
    if (InfraString::isBolVazia($objItemSessaoJulgamentoDTO->getStrMotivoRetirada())) {
      $objInfraException->lancarValidacao('Motivo de retirada n�o informado.');
    }
    //validar estado da distribui��o se confere com o item
    $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objItemSessaoJulgamentoDTOBanco->retStrStaDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retStrStaDistribuicaoAnterior();
    $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
    $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retStrNomeColegiado();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioSessao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUnidadeSessao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUnidadeRelatorDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUnidadeResponsavelColegiado();
    $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoBloco();
    $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retStrDescricaoSessaoBloco();
    $objItemSessaoJulgamentoDTOBanco->retStrStaTipoItemSessaoBloco();
    $objItemSessaoJulgamentoDTOBanco->retStrSinAgruparMembroSessaoBloco();
    $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);


    if ($objItemSessaoJulgamentoDTO->isSetNumIdSessaoJulgamento() && $objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento()!=$objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento()) {
      $objInfraException->lancarValidacao('Item n�o pertence � sess�o informada.');
    }
    if ($objItemSessaoJulgamentoDTOBanco===null) {
      $objInfraException->lancarValidacao('Item n�o encontrado na sess�o.');
    }

    $bolAdmin = SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') && SessaoSEI::getInstance()->getNumIdUnidadeAtual()==$objItemSessaoJulgamentoDTOBanco->getNumIdUnidadeResponsavelColegiado();
    $idDistribuicao = $objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao();
    //$staDistribuicao=$objItemSessaoJulgamentoDTOBanco->getStrStaDistribuicao();
    $staTipoItem = $objItemSessaoJulgamentoDTOBanco->getStrStaTipoItemSessaoBloco();
    $staSituacao = $objItemSessaoJulgamentoDTOBanco->getStrStaSituacao();
    $staSessao = $objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento();
    $bolCancelamentoSessao = false;
    if (!$objItemSessaoJulgamentoDTO->isSetStrSinCancelamentoSessao() || $objItemSessaoJulgamentoDTO->getStrSinCancelamentoSessao()!=='S') {
      if (!in_array($staSessao, array(SessaoJulgamentoRN::$ES_ABERTA, SessaoJulgamentoRN::$ES_PAUTA_ABERTA))) {
        //#189770 permitir retirar referendos com pauta fechada
        if ($staSessao!=SessaoJulgamentoRN::$ES_PAUTA_FECHADA || $staTipoItem==TipoSessaoBlocoRN::$STA_PAUTA) {
          $objInfraException->lancarValidacao('Situa��o da Sess�o n�o permite retirada.');
        }
      }
      if ($staSituacao==self::$STA_RETIRADO) {
        $objInfraException->lancarValidacao('Processo j� foi retirado.');
      }
      if ($staSituacao==self::$STA_ADIADO) {
        $objInfraException->lancarValidacao('Processo foi adiado.');
      }
      if ($staSessao==SessaoJulgamentoRN::$ES_ABERTA && !$bolAdmin) {
        $objInfraException->lancarValidacao('Somente Administrador da Sess�o pode retirar item de Sess�o Aberta.');
      }

      if (!$bolAdmin && $objItemSessaoJulgamentoDTOBanco->getNumIdUnidadeSessao()!=SessaoSEI::getInstance()->getNumIdUnidadeAtual()) {
        if ($objItemSessaoJulgamentoDTOBanco->getNumIdUnidadeRelatorDistribuicao()!=$objItemSessaoJulgamentoDTOBanco->getNumIdUnidadeSessao()) {
          $objInfraException->lancarValidacao('Somente a Unidade do Relator ou Administrador da Sess�o podem retirar itens.');
        } else {
          $objInfraException->lancarValidacao('Somente a Unidade que pautou ou Administrador da Sess�o podem retirar itens.');
        }
      }
    } else {
      $bolCancelamentoSessao = true;
    }

    $valor=$objItemSessaoJulgamentoDTOBanco->getStrDescricaoSessaoBloco();
    switch ($staTipoItem) {
      case TipoSessaoBlocoRN::$STA_PAUTA:
        $staTarefa = TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_RETIRADA_PAUTA;
        break;
      case TipoSessaoBlocoRN::$STA_MESA:
        $staTarefa = TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_RETIRADA_MESA;
        break;
      case TipoSessaoBlocoRN::$STA_REFERENDO:
        $staTarefa = TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_RETIRADO_REFERENDO;
        break;
      default:
        throw new InfraException('Tipo de item inv�lido');
    }

    //para cancelamento de sess�o manter todos os dados (como exclusao de pauta)
    //excluir partes e votos (somente se pauta n�o tiver sido fechada e n�o tiver destaques) sen�o altera para STA_IGNORADA
    //se a pauta estiver aberta exclui item_sessao_julgamento e reordena, sen�o atualiza item para STA_RETIRADO
    //se a sess�o j� tiver fechado a pauta, deixa tudo como retirado
    if ($staSessao===SessaoJulgamentoRN::$ES_PAUTA_ABERTA  && !$objItemSessaoJulgamentoDTO->isSetStrSinCancelamentoSessao()) {
      //verificar se se possui destaques
      $objDestaqueDTO = new DestaqueDTO();
      $objDestaqueDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objDestaqueDTO->retNumIdDestaque();
      $objDestaqueRN = new DestaqueRN();
      $bolPossuiDestaque = $objDestaqueRN->contar($objDestaqueDTO)>0;
      if ($bolPossuiDestaque) {
        $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
        $objItemSessaoJulgamentoDTO2->setStrStaSituacao(self::$STA_IGNORADA);
        $this->alterar($objItemSessaoJulgamentoDTO2);
      } else {
        //exclui dados copiados anteriormente
        $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
        $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
        $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
        $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
        $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
        if ($arrObjItemSessaoDocumentoDTO) {
          $objItemSessaoDocumentoRN->excluir($arrObjItemSessaoDocumentoDTO);
        }
        $objJulgamentoParteDTO = new JulgamentoParteDTO();
        $objJulgamentoParteRN = new JulgamentoParteRN();
        $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
        $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
        $objJulgamentoParteDTO->retNumIdJulgamentoParte();
        $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
        if ($arrObjJulgamentoParteDTO) {
          $objVotoParteRN = new VotoParteRN();
          $objVotoParteDTO = new VotoParteDTO();
          $objVotoParteDTO->retNumIdVotoParte();
          $objVotoParteDTO->retNumIdVotoParteAssociado();
//          $objVotoParteDTO->adicionarCriterio(array('IdSessaoVoto', 'StaVotoParte'), array(InfraDTO::$OPER_DIFERENTE, InfraDTO::$OPER_IGUAL), array($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento(), VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO), InfraDTO::$OPER_LOGICO_OR);
//          $objVotoParteDTO->setNumIdSessaoVoto($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento(),InfraDTO::$OPER_DIFERENTE);
          foreach ($arrObjJulgamentoParteDTO as $dto) {
            $dto->setStrSinForcarExclusao('S');
            $objVotoParteDTO->setNumIdJulgamentoParte($dto->getNumIdJulgamentoParte());
            $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);
            $arrVP1 = array();
            $arrVP2 = array();
            foreach ($arrObjVotoParteDTO as $dto2) {
              if ($dto2->getNumIdVotoParteAssociado()!=null) {
                $arrVP1[] = $dto2;
              } else {
                $arrVP2[] = $dto2;
              }
            }
            if (InfraArray::contar($arrVP1)>0) {
              $objVotoParteRN->excluir($arrVP1);
            }
            if (InfraArray::contar($arrVP2)>0) {
              $objVotoParteRN->excluir($arrVP2);
            }

          }

          $objJulgamentoParteRN->excluir($arrObjJulgamentoParteDTO);
        }

        $objRevisaoItemDTO=new RevisaoItemDTO();
        $objRevisaoItemRN=new RevisaoItemRN();
        $objRevisaoItemDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
        $objRevisaoItemDTO->retNumIdRevisaoItem();
        $arrObjRevisaoItemDTO=$objRevisaoItemRN->listar($objRevisaoItemDTO);
        if(count($arrObjRevisaoItemDTO)){
          $objRevisaoItemRN->excluir($arrObjRevisaoItemDTO);
        }

        //excluir item
        $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
        $this->excluir(array($objItemSessaoJulgamentoDTO2));
      }

      //reordenar itens do relator se for mesa ou pauta
      if ($objItemSessaoJulgamentoDTOBanco->getStrSinAgruparMembroSessaoBloco()=='S') {
        $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
        $objItemSessaoJulgamentoDTO2->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
        $objItemSessaoJulgamentoDTO2->setNumIdUsuarioSessao($objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioSessao());
        $objItemSessaoJulgamentoDTO2->setNumIdSessaoBloco($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoBloco());
        $objItemSessaoJulgamentoDTO2->retNumIdItemSessaoJulgamento();
        $objItemSessaoJulgamentoDTO2->retNumOrdem();
        $objItemSessaoJulgamentoDTO2->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
        $arrObjItemSessaoJulgamento = $this->listar($objItemSessaoJulgamentoDTO2);
        $numRegistros = InfraArray::contar($arrObjItemSessaoJulgamento);

        for ($i = 0; $i<$numRegistros; $i ++) {
          if ($arrObjItemSessaoJulgamento[$i]->getNumOrdem()!=($i + 1)) {
            $arrObjItemSessaoJulgamento[$i]->setNumOrdem($i + 1);
            $this->alterar($arrObjItemSessaoJulgamento[$i]);
          }
        }
      }

    } else {
      //altera item para retirado mas mant�m na lista de itens da sess�o
      $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      //indisponibilizar documentos
      $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoDocumentoDTO->retNumIdItemSessaoDocumento();
      $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
      $objItemSessaoDocumentoRN->excluir($objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO));

      if ($bolCancelamentoSessao) {
        $objItemSessaoJulgamentoDTO2->setStrStaSituacao(self::$STA_SESSAO_CANCELADA);
        $staTarefa = TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_CANCELADA;
      } else {
        $objItemSessaoJulgamentoDTO2->setStrStaSituacao(self::$STA_RETIRADO);
        $strDispositivo = 'Processo retirado de ' . $valor . '. Motivo: ' . $objItemSessaoJulgamentoDTO->getStrMotivoRetirada();
        $objItemSessaoJulgamentoDTO2->setStrDispositivo($strDispositivo);
      }
      $this->alterar($objItemSessaoJulgamentoDTO2);
    }

    //exclui pedidos de vista pendentes
    $objPedidoVistaDTO = new PedidoVistaDTO();
    $objPedidoVistaRN = new PedidoVistaRN();
    $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao());
    $objPedidoVistaDTO->retNumIdPedidoVista();
    $objPedidoVistaDTO->setStrSinPendente('S');
    $objPedidoVistaRN->excluir($objPedidoVistaRN->listar($objPedidoVistaDTO));


    //alterar distribuicao - retorna para estado anterior
    $objDistribuicaoDTO = new DistribuicaoDTO();
    $objDistribuicaoRN = new DistribuicaoRN();
    $objDistribuicaoDTO->setNumIdDistribuicao($idDistribuicao);
    //se for PAUTA, n�o era pedido de vista (relator=usuario_sessao), a sess�o estiver aberta e for retirado colocar como STA_RETIRADO_PAUTA
    if ($staTipoItem==TipoSessaoBlocoRN::$STA_PAUTA && $staSessao==SessaoJulgamentoRN::$ES_ABERTA && $objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioRelatorDistribuicao()==$objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioSessao()) {
      $objDistribuicaoDTO->setStrStaDistribuicao(DistribuicaoRN::$STA_RETIRADO_PAUTA);
    } else {
      $objDistribuicaoDTO->setStrStaDistribuicao($objItemSessaoJulgamentoDTOBanco->getStrStaDistribuicaoAnterior());
    }
    $objDistribuicaoRN->alterar($objDistribuicaoDTO);

    //gera andamento de retirada
    $arrObjAtributoAndamentoDTO = array();
    $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
    $objAtributoAndamentoDTO->setStrNome('COLEGIADO');
    $objAtributoAndamentoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrNomeColegiado());
    $objAtributoAndamentoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getNumIdColegiadoSessaoJulgamento());
    $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

    if (!$bolCancelamentoSessao) {
      $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
      $objAtributoAndamentoDTO->setStrNome('MOTIVO');
      $objAtributoAndamentoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrMotivoRetirada());
      $objAtributoAndamentoDTO->setStrIdOrigem(null);
      $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;
    }

    $objAtividadeDTO = new AtividadeDTO();
    $objAtividadeDTO->setDblIdProtocolo($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
    $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objAtividadeDTO->setStrIdTarefaModuloTarefa($staTarefa);
    $objAtividadeDTO->setArrObjAtributoAndamentoDTO($arrObjAtributoAndamentoDTO);

    $objAtividadeRN = new AtividadeRN();
    $objAtividadeRN->gerarInternaRN0727($objAtividadeDTO);

    //gera andamento de retirada na sess�o
    if (!$bolCancelamentoSessao) {
      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();

      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_RETIRADA_ITEM);

      $arrObjAtributoAndamentoSessaoDTO = array();

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('TIPO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoBloco());
      $objAtributoAndamentoSessaoDTO->setStrValor($valor);
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('MOTIVO');
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTO->getStrMotivoRetirada());
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem(null);
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);

      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);

      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoRN=new EventoSessaoRN();
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
      $objEventoSessaoDTO->setStrDescricao('Retirado');
      $objEventoSessaoRN->lancar($objEventoSessaoDTO);
    }
    //Auditoria

  }

  protected function acompanharJulgamentoConectado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {

    $objInfraException = new InfraException();

    $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
    $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorAcordaoDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retStrStaDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioPresidenteColegiado();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUnidadeResponsavelColegiado();
    $objItemSessaoJulgamentoDTOBanco->retStrNomeColegiado();
    $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicaoAgrupadorDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retStrArtigoColegiado();
    $objItemSessaoJulgamentoDTOBanco->retTodos();

    /** @var ItemSessaoJulgamentoDTO $ret */
    $ret = $this->consultar($objItemSessaoJulgamentoDTOBanco);
    if ($ret==null) {
      $objInfraException->lancarValidacao('Item da Sess�o de Julgamento n�o encontrado.');
    }

    $objBloqItemSessaoUnidadeDTO=new BloqueioItemSessUnidadeDTO();
    $objBloqItemSessaoUnidadeRN=new BloqueioItemSessUnidadeRN();
    $objBloqItemSessaoUnidadeDTO->setNumIdDistribuicao($ret->getNumIdDistribuicao());
    $objBloqItemSessaoUnidadeDTO->retTodos();
    $arrObjBloqItemSessaoUnidadeDTO=$objBloqItemSessaoUnidadeRN->listar($objBloqItemSessaoUnidadeDTO);
    $ret->setArrObjBloqueioItemSessUnidadeDTO($arrObjBloqItemSessaoUnidadeDTO);



    $arrIdUnidadesBloqueadas=array();
    if(count($arrObjBloqItemSessaoUnidadeDTO)){
      /** @var BloqueioItemSessUnidadeDTO $objBloqItemSessaoUnidadeDTO */
      foreach ($arrObjBloqItemSessaoUnidadeDTO as $objBloqItemSessaoUnidadeDTO){
        if($objBloqItemSessaoUnidadeDTO->getStrStaTipo()==BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO){
          $arrIdUnidadesBloqueadas[]=$objBloqItemSessaoUnidadeDTO->getNumIdUnidade();
        }
      }
    }

    if ($objItemSessaoJulgamentoDTO->isSetArrObjItemSessaoDocumentoDTO()) {
      $ret->setArrObjItemSessaoDocumentoDTO($objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO());
    } else {
      $objItemSessaoDocumentoDTO = new ItemSessaoDocumentoDTO();
      $objItemSessaoDocumentoRN = new ItemSessaoDocumentoRN();
      $objItemSessaoDocumentoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoDocumentoDTO->retTodos();
      $objItemSessaoDocumentoDTO->retStrNomeSerie();
      $objItemSessaoDocumentoDTO->retStrProtocoloDocumentoFormatado();
      $arrObjItemSessaoDocumentoDTO = $objItemSessaoDocumentoRN->listar($objItemSessaoDocumentoDTO);
      $ret->setArrObjItemSessaoDocumentoDTO($arrObjItemSessaoDocumentoDTO);
    }

    $idSessao = $ret->getNumIdSessaoJulgamento();
    $strSinPermiteFinalizacao = 'S';
    $bolFaltaQuorum=false;

    $objSessaoJulgamentoRN = new SessaoJulgamentoRN();
    $objPesquisaSessaoJulgamentoDTO = new PesquisaSessaoJulgamentoDTO();
    $objPesquisaSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessao);

    if ($objItemSessaoJulgamentoDTO->isSetArrObjColegiadoComposicaoDTO() && $objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO()!=null) {
      $arrObjColegiadoComposicaoDTO = $objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO();
      $arrObjPresencaSessaoDTO = $objItemSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();
      $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);

    } else {
      $objPesquisaSessaoJulgamentoDTO->setStrSinComposicao('S');
      $objPesquisaSessaoJulgamentoDTO->setStrSinPresenca('S');

      $objSessaoJulgamentoDTO = $objSessaoJulgamentoRN->pesquisar($objPesquisaSessaoJulgamentoDTO);

      $arrObjColegiadoComposicaoDTO = $objSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO();
      $arrObjPresencaSessaoDTO = $objSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();
      $objSessaoJulgamentoDTO->unSetArrObjColegiadoComposicaoDTO();
      $objSessaoJulgamentoDTO->unSetArrObjPresencaSessaoDTO();
    }
    $ret->setObjSessaoJulgamentoDTO($objSessaoJulgamentoDTO);

    $ret->setArrObjPresencaSessaoDTO($arrObjPresencaSessaoDTO);
    $arrPresentes=array();
    foreach ($arrObjPresencaSessaoDTO as $objPresencaSessaoDTO) {
      if($objPresencaSessaoDTO->getDthSaida()==null) {
        $arrPresentes[$objPresencaSessaoDTO->getNumIdUsuario()] = $objPresencaSessaoDTO->getDthEntrada();
      }
    }

    //reordena lista de membros (como lista circular), colocando o relador em primeiro
    //conta numero de titulares
    $numTitularesColegiado = 0;
    $ordem = 1000;
    foreach ($arrObjColegiadoComposicaoDTO as $objJulgamentoParteDTO) {
      if ($objJulgamentoParteDTO->getNumOrdem()>$ordem) {
        $objJulgamentoParteDTO->setNumOrdem($objJulgamentoParteDTO->getNumOrdem() - $ordem);
      }
    }
    $arrObjColegiadoComposicaoDTO=array_values($arrObjColegiadoComposicaoDTO);
    InfraArray::ordenarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'Ordem', InfraArray::$TIPO_ORDENACAO_ASC);
    $idRelator = $ret->getNumIdUsuarioRelatorDistribuicao();
    foreach ($arrObjColegiadoComposicaoDTO as $objJulgamentoParteDTO) {
      if ($objJulgamentoParteDTO->getNumIdTipoMembroColegiado()==TipoMembroColegiadoRN::$TMC_TITULAR) {
        $numTitularesColegiado ++;
      }
      if ($objJulgamentoParteDTO->getNumIdUsuario()==$idRelator) {
        $ordem = 0;
      }
      $objJulgamentoParteDTO->setNumOrdem($objJulgamentoParteDTO->getNumOrdem() + $ordem);
      if (!isset($arrPresentes[$objJulgamentoParteDTO->getNumIdUsuario()])) {
        $objJulgamentoParteDTO->setStrSinPresente('N');
      } else {
        $objJulgamentoParteDTO->setStrSinPresente('S');
      }
    }


    $ret->setNumTitularesColegiado($numTitularesColegiado);
    InfraArray::ordenarArrInfraDTO($arrObjColegiadoComposicaoDTO, 'Ordem', InfraArray::$TIPO_ORDENACAO_ASC);
    $arrObjColegiadoComposicaoDTO=InfraArray::indexarArrInfraDTO($arrObjColegiadoComposicaoDTO,'IdUsuario');

    if ($objItemSessaoJulgamentoDTO->isSetArrObjPedidoVistaDTO()) {
      $ret->setArrObjPedidoVistaDTO($objItemSessaoJulgamentoDTO->getArrObjPedidoVistaDTO());
    } else {
      //lista pedidos de vista do processo
      $objPedidoVistaDTO = new PedidoVistaDTO();
      $objPedidoVistaRN = new PedidoVistaRN();
      $objPedidoVistaDTO->setNumIdDistribuicao($ret->getNumIdDistribuicao());
      if($objSessaoJulgamentoDTO->getStrStaSituacao()==SessaoJulgamentoRN::$ES_ENCERRADA){
        $objPedidoVistaDTO->adicionarCriterio(array('Devolucao','Devolucao'),
            array(InfraDTO::$OPER_IGUAL,InfraDTO::$OPER_IGUAL),
            array($objSessaoJulgamentoDTO->getDthFim(),null),
            InfraDTO::$OPER_LOGICO_OR);
      } else {
        $objPedidoVistaDTO->setDthDevolucao(null);
      }
      $objPedidoVistaDTO->retTodos();
      $ret->setArrObjPedidoVistaDTO($objPedidoVistaRN->listar($objPedidoVistaDTO));
    }


    if ($objItemSessaoJulgamentoDTO->isSetArrObjJulgamentoParteDTO()) {
      $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
    } else {
      //lista julgamento_parte e voto_parte relacionados
      $objJulgamentoParteDTO = new JulgamentoParteDTO();
      $objJulgamentoParteRN = new JulgamentoParteRN();
      $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objJulgamentoParteDTO->setOrdNumOrdem(InfraDTO::$TIPO_ORDENACAO_ASC);
      $objJulgamentoParteDTO->retTodos();
      $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
    }

    $objVotoParteRN = new VotoParteRN();
    $arrIdUsuarioVista = array();
    $objVotoParteDTOBase = new VotoParteDTO();
    $objVotoParteDTOBase->retTodos();
    $objVotoParteDTOBase->retStrNomeUsuario();
    $objVotoParteDTOBase->retStrConteudoProvimento();
    $objVotoParteDTOBase->retStrNomeUsuarioAssociado();
    $objVotoParteDTOBase->retNumIdUsuarioVotoParte();

    $objContagemVotosDTOBase = new ContagemVotosDTO();

    $objContagemVotosDTOBase->setNumIdSessaoAtual($idSessao);
    $objContagemVotosDTOBase->setNumIdUsuarioRelator($ret->getNumIdUsuarioRelatorDistribuicao());
    $objContagemVotosDTOBase->setNumTitularesColegiado($numTitularesColegiado);
    $objContagemVotosDTOBase->setArrIdUnidadesBloqueadas($arrIdUnidadesBloqueadas);
    $objContagemVotosDTOBase->setNumQuorumMinimo($objSessaoJulgamentoDTO->getNumQuorumMinimoColegiado());


    foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {
      if ($objJulgamentoParteDTO->isSetArrObjVotoParteDTO()) {
        $arrObjVotoParteDTO = $objJulgamentoParteDTO->getArrObjVotoParteDTO();
      } else {
        $objVotoParteDTO = clone $objVotoParteDTOBase;
        $objVotoParteDTO->setNumIdJulgamentoParte($objJulgamentoParteDTO->getNumIdJulgamentoParte());
        $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);
        $objJulgamentoParteDTO->setArrObjVotoParteDTO($arrObjVotoParteDTO);
      }
      foreach ($arrObjVotoParteDTO as $objVotoParteDTOBanco) {
        $numIdUsuario=$objVotoParteDTOBanco->getNumIdUsuario();
        if ($objVotoParteDTOBanco->getStrStaVotoParte()==VotoParteRN::$STA_PEDIDO_VISTA) {
          $arrIdUsuarioVista[$numIdUsuario] = $objVotoParteDTOBanco->getNumIdUsuario();
        }
        ///consultar membro que n�o est� mais presente no colegiado
        if(!isset($arrObjColegiadoComposicaoDTO[$numIdUsuario])){
          $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
          $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($objVotoParteDTOBanco->getNumIdSessaoVoto());
          $objSessaoJulgamentoDTO->retNumIdColegiadoVersao();
          $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
          $objSessaoJulgamentoDTO=$objSessaoJulgamentoRN->consultar($objSessaoJulgamentoDTO);
          $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
          $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($objSessaoJulgamentoDTO->getNumIdColegiadoVersao());
          $objColegiadoComposicaoDTO->retTodos();
          $objColegiadoComposicaoDTO->retStrExpressaoCargo();
          $objColegiadoComposicaoDTO->retStrStaGeneroCargo();
          $objColegiadoComposicaoDTO->retStrNomeUsuario();
//          $objColegiadoComposicaoDTO->setNumIdUsuario($numIdUsuario);
          $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
          $arrObjColegiadoComposicaoDTOAnterior=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
          foreach ($arrObjColegiadoComposicaoDTOAnterior as $objColegiadoComposicaoDTO) {
            $numIdUsuarioComposicaoAnterior=$objColegiadoComposicaoDTO->getNumIdUsuario();
            if(!isset($arrObjColegiadoComposicaoDTO[$numIdUsuarioComposicaoAnterior])){
              $objColegiadoComposicaoDTO->setBolComposicaoAnterior(true);
              $objColegiadoComposicaoDTO->setStrSinPresente('N');
              $arrObjColegiadoComposicaoDTO[$numIdUsuarioComposicaoAnterior]=$objColegiadoComposicaoDTO;
            }
          }
        }
      }

      $objContagemVotosDTOBase->setArrObjColegiadoComposicaoDTO($arrObjColegiadoComposicaoDTO);


      $objContagemVotosDTO = clone $objContagemVotosDTOBase;
      $objContagemVotosDTO->setArrObjVotoParteDTO($arrObjVotoParteDTO);
      $objContagemVotosDTO = $this->calcularVotos($objContagemVotosDTO);
      $objJulgamentoParteDTO->setBolFaltamVotos($objContagemVotosDTO->getBolFaltamVotos());
      $objJulgamentoParteDTO->setNumUsuarioVencedor($objContagemVotosDTO->getNumIdUsuarioAcordao());
      $objJulgamentoParteDTO->setBolPedidoVista($objContagemVotosDTO->getBolPedidoVista());
      $objJulgamentoParteDTO->setArrObjResumoVotacaoDTO($objContagemVotosDTO->getArrObjResumoVotacaoDTO());
      $bolEmpate = $objContagemVotosDTO->getBolEmpate();
      $objJulgamentoParteDTO->setBolPossuiEmpate($bolEmpate);
      if ($bolEmpate) {
        $objJulgamentoParteDTO->setArrUsuariosEmpate($objContagemVotosDTO->getArrUsuariosEmpate());
      }
      if ($objContagemVotosDTO->getBolFaltamVotos() || $objContagemVotosDTO->getBolPedidoVistaAnterior() || ($bolEmpate && $objJulgamentoParteDTO->getNumIdUsuarioDesempate()==null)) {
        $strSinPermiteFinalizacao = 'N';
        if($objContagemVotosDTO->getBolFaltaQuorum()){
          $bolFaltaQuorum=true;
        }
      }
    }
    //usuario da sess�o n�o pode pedir vista, se anterior deve votar.
    if (isset($arrIdUsuarioVista[$ret->getNumIdUsuarioSessao()])) {
      $strSinPermiteFinalizacao = 'N';
    }

    $ret->setStrSinPermiteFinalizacao($strSinPermiteFinalizacao);
    $ret->setArrObjJulgamentoParteDTO($arrObjJulgamentoParteDTO);
    $ret->setArrObjColegiadoComposicaoDTO($arrObjColegiadoComposicaoDTO);
    $ret->setBolFaltaQuorum($bolFaltaQuorum);
    //relator

    $objColegiadoComposicaoDTO=$arrObjColegiadoComposicaoDTO[$ret->getNumIdUsuarioRelatorDistribuicao()];
    //se null buscar composi��o da vers�o no qual houve distribui��o ...
    if($objColegiadoComposicaoDTO==null){
      $objDistribuicaoDTO=new DistribuicaoDTO();
      $objDistribuicaoDTO->setNumIdDistribuicao($ret->getNumIdDistribuicao());
      $objDistribuicaoDTO->retNumIdColegiadoVersao();
      $objDistribuicaoRN=new DistribuicaoRN();
      $objDistribuicaoDTO=$objDistribuicaoRN->consultar($objDistribuicaoDTO);
      $objColegiadoComposicaoDTO=new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($objDistribuicaoDTO->getNumIdColegiadoVersao());
      $objColegiadoComposicaoDTO->retTodos();
      $objColegiadoComposicaoDTO->retStrExpressaoCargo();
      $objColegiadoComposicaoDTO->retStrStaGeneroCargo();
      $objColegiadoComposicaoDTO->retStrNomeUsuario();
      $objColegiadoComposicaoRN=new ColegiadoComposicaoRN();
      $arrObjColegiadoComposicaoDTOAnterior=$objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO);
      foreach ($arrObjColegiadoComposicaoDTOAnterior as $objColegiadoComposicaoDTO) {
        $numIdUsuarioComposicaoAnterior=$objColegiadoComposicaoDTO->getNumIdUsuario();
        if(!isset($arrObjColegiadoComposicaoDTO[$numIdUsuarioComposicaoAnterior])){
          $objColegiadoComposicaoDTO->setBolComposicaoAnterior(true);
          $objColegiadoComposicaoDTO->setStrSinPresente('N');
          $arrObjColegiadoComposicaoDTO[$numIdUsuarioComposicaoAnterior]=$objColegiadoComposicaoDTO;
        }
      }
    }
    if($objColegiadoComposicaoDTO->isSetBolComposicaoAnterior()){
      $ret->setBolRelatorComposicaoAnterior($objColegiadoComposicaoDTO->getBolComposicaoAnterior());
    } else {
      $ret->setBolRelatorComposicaoAnterior(false);
    }


    $objParteProcedimentoDTO = new ParteProcedimentoDTO();
    $objParteProcedimentoRN = new ParteProcedimentoRN();
    $objParteProcedimentoDTO->setDblIdProcedimento($ret->getDblIdProcedimentoDistribuicao());
    $objParteProcedimentoDTO->retTodos();
    $objParteProcedimentoDTO->retStrDescricaoQualificacaoParte();
    $objParteProcedimentoDTO->retStrNomeContato();
    $ret->setArrObjParteProcedimentoDTO($objParteProcedimentoRN->listar($objParteProcedimentoDTO));

    $objSustentacaoOralDTO = new SustentacaoOralDTO();
    $objSustentacaoOralRN = new SustentacaoOralRN();
    $objSustentacaoOralDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objSustentacaoOralDTO->retTodos(true);
    $ret->setArrObjSustentacaoOralDTO($objSustentacaoOralRN->listar($objSustentacaoOralDTO));

    return $ret;
  }

  protected function finalizarJulgamentoControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {

    $objInfraException = new InfraException();
    $objItemSessaoJulgamentoDTO2 = $this->acompanharJulgamento($objItemSessaoJulgamentoDTO);
    $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO2->getArrObjJulgamentoParteDTO();

    $idUsuarioRelator = $objItemSessaoJulgamentoDTO2->getNumIdUsuarioRelatorDistribuicao();

    if ($objItemSessaoJulgamentoDTO->getStrSinManual()==='N') {
      foreach ($arrObjJulgamentoParteDTO as $objJulgamentoParteDTO) {

        $arrObjVotoParteDTO = $objJulgamentoParteDTO->getArrObjVotoParteDTO();
        $arrObjVotoParteDTO = InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO, 'IdUsuario');
        //validar se voto do relator possui provimento
        if (!isset($arrObjVotoParteDTO[$idUsuarioRelator]) || $arrObjVotoParteDTO[$idUsuarioRelator]->getStrStaVotoParte()!=VotoParteRN::$STA_ACOMPANHA) {
          $objInfraException->lancarValidacao('N�o foi poss�vel finalizar o julgamento do processo porque falta provimento do voto do Relator.');
        }

      }
    }
    //se relator for vencedor n�o tem necessidade de relator para ac�rd�o
//    if($objItemSessaoJulgamentoDTO2->getBolRelatorComposicaoAnterior() && $objItemSessaoJulgamentoDTO2->getNumIdUsuarioRelatorAcordaoDistribuicao()==null){
//      $objInfraException->lancarValidacao('Processo '.$objItemSessaoJulgamentoDTO2->getStrProtocoloFormatadoProtocolo().' n�o possui relator para ac�rd�o definido.');
//    }
    //setar sin_julgado e presidente da sess�o
    $objItemSessaoJulgamentoDTO3 = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO3->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objItemSessaoJulgamentoDTO3->setStrStaSituacao(self::$STA_JULGADO);
    $objItemSessaoJulgamentoDTO3->setStrDispositivo($objItemSessaoJulgamentoDTO->getStrDispositivo());
    //$objItemSessaoJulgamentoDTO3->setNumIdUsuarioPresidente($objItemSessaoJulgamentoDTO2->getNumIdUsuarioPresidente());
    $this->alterar($objItemSessaoJulgamentoDTO3);

    //lancar andamento no processo e na sessao
    $arrObjAtributoAndamentoDTO = array();
    $objAtributoAndamentoDTO = new AtributoAndamentoDTO();
    $objAtributoAndamentoDTO->setStrNome('COLEGIADO');
    $objAtributoAndamentoDTO->setStrValor($objItemSessaoJulgamentoDTO2->getStrNomeColegiado());
    $objAtributoAndamentoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO2->getNumIdColegiadoSessaoJulgamento());
    $arrObjAtributoAndamentoDTO[] = $objAtributoAndamentoDTO;

    $objAtividadeDTO = new AtividadeDTO();
    $objAtividadeDTO->setDblIdProtocolo($objItemSessaoJulgamentoDTO2->getDblIdProcedimentoDistribuicao());
    $objAtividadeDTO->setNumIdUnidade(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objAtividadeDTO->setStrIdTarefaModuloTarefa(TarefaSessaoRN::$TI_SESSAO_JULGAMENTO_ENCERRADA);
    $objAtividadeDTO->setArrObjAtributoAndamentoDTO($arrObjAtributoAndamentoDTO);

    $objAtividadeRN = new AtividadeRN();
    $objAtividadeRN->gerarInternaRN0727($objAtividadeDTO);

    //gera andamento de julgamento na sess�o
    $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
    $objAndamentoSessaoRN = new AndamentoSessaoRN();

    $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO2->getNumIdSessaoJulgamento());
    $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_PROCESSO_JULGADO);

    $arrObjAtributoAndamentoSessaoDTO = array();


    $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
    $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
    $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTO2->getDblIdProcedimentoDistribuicao());
    $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTO2->getStrProtocoloFormatadoProtocolo());
    $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

    $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
    $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);
    $objEventoSessaoDTO=new EventoSessaoDTO();
    $objEventoSessaoRN=new EventoSessaoRN();
    $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO2->getNumIdSessaoJulgamento());
    $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO2->getNumIdItemSessaoJulgamento());
    $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
    $objEventoSessaoDTO->setStrDescricao('Julgado');
    $objEventoSessaoRN->lancar($objEventoSessaoDTO);

  }

  /**
   * @param ItemSessaoJulgamentoDTO[] $arrObjItemSessaoJulgamentoDTO
   */
  protected function alterarOrdemControlado(array $arrObjItemSessaoJulgamentoDTO):void
  {
    foreach ($arrObjItemSessaoJulgamentoDTO as $dto) {
      $objItemSessaoJulgamentoDTO = CacheSessaoJulgamentoRN::getObjItemSessaoJulgamentoDTO($dto->getNumIdItemSessaoJulgamento());
      $this->alterar($dto);
      if($dto->getNumOrdem()!=$objItemSessaoJulgamentoDTO->getNumOrdem()){
        $objEventoSessaoDTO=new EventoSessaoDTO();
        $objEventoSessaoRN=new EventoSessaoRN();
        $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
        $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
        $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ORDENACAO_PROCESSOS);
        $objEventoSessaoDTO->setStrDescricao('Processo reordenado');
        $objEventoSessaoRN->lancar($objEventoSessaoDTO);
      }
    }

  }

  protected function julgarMultiploControlado($arrObjItemSessaoJulgamentoDTO)
  {
    $objInfraException = new InfraException();
    $objVotoParteRN = new VotoParteRN();

    $objVotoParteDTOBase = new VotoParteDTO();
    $objVotoParteDTOBase->setNumIdJulgamentoParte(null);
    $objVotoParteDTOBase->setNumIdUsuario(null);
    $objVotoParteDTOBase->setStrStaVotoParte(VotoParteRN::$STA_ACOMPANHA);
    $objVotoParteDTOBase->setStrRessalva(null);
    $objVotoParteDTOBase->setNumIdVotoParteAssociado(null);
    $objVotoParteDTOBase->setNumIdUsuarioVotoParte(null);
    $objVotoParteDTOBase->setDthVoto(InfraData::getStrDataHoraAtual());

    $arrObjColegiadoComposicaoDTO = null;
    $arrObjPresencaSessaoDTO = null;

    foreach ($arrObjItemSessaoJulgamentoDTO as $dto) {
      $objItemSessaoJulgamentoDTO = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO->setNumIdItemSessaoJulgamento($dto->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO->setArrObjColegiadoComposicaoDTO($arrObjColegiadoComposicaoDTO);
      $objItemSessaoJulgamentoDTO->setArrObjPresencaSessaoDTO($arrObjPresencaSessaoDTO);
      $objItemSessaoJulgamentoDTO = $this->acompanharJulgamento($objItemSessaoJulgamentoDTO);

      if ($objItemSessaoJulgamentoDTO==null) {
        $objInfraException->lancarValidacao('Item de Julgamento n�o localizado, atualize a tela da sess�o.');
      }
      if ($objItemSessaoJulgamentoDTO->getStrSinManual()==='S') {
        $objInfraException->adicionarValidacao('Julgamento do Processo [' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '] foi convertido para manual.');
        continue;
      }
      if ($arrObjColegiadoComposicaoDTO==null) {
        $arrObjPresencaSessaoDTO = $objItemSessaoJulgamentoDTO->getArrObjPresencaSessaoDTO();
      }
      $arrObjColegiadoComposicaoDTO = $objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO();

      switch ($objItemSessaoJulgamentoDTO->getStrStaSituacao()) {
        case self::$STA_ADIADO:
          $objInfraException->adicionarValidacao('Julgamento do Processo [' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '] foi adiado.');
          break;
        case self::$STA_DILIGENCIA:
          $objInfraException->adicionarValidacao('Julgamento do Processo [' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '] foi convertido em dilig�ncia.');
          break;
        case self::$STA_RETIRADO:
          $objInfraException->adicionarValidacao('Processo [' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '] foi retirado da sess�o.');
          break;
        case self::$STA_SESSAO_CANCELADA:
          $objInfraException->lancarValidacao('Sess�o de Julgamento cancelada.');
          break;
      }
      $objInfraException->lancarValidacoes();
      $arrObjItemSessaoDocumentoDTO = $objItemSessaoJulgamentoDTO->getArrObjItemSessaoDocumentoDTO();
      $numIdUsuarioRelator = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
      $bolDocumentoDisponibilizado = false;
      foreach ($arrObjItemSessaoDocumentoDTO as $objItemSessaoDocumentoDTO) {
        if ($objItemSessaoDocumentoDTO->getNumIdUsuario()==$numIdUsuarioRelator) {
          $bolDocumentoDisponibilizado = true;
          break;
        }
      }
      if (!$bolDocumentoDisponibilizado) {
        $objInfraException->lancarValidacao('N�o � poss�vel votar, processo [' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '] sem documento disponibilizado.');
      }
      $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
      if (InfraArray::contar($arrObjJulgamentoParteDTO)>1) {
        $objInfraException->lancarValidacao('N�o � poss�vel votar, processo [' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . '] foi fracionado.');
      }

      $objJulgamentoParteDTO = $arrObjJulgamentoParteDTO[0];
      $arrObjVotoParteDTO = $objJulgamentoParteDTO->getArrObjVotoParteDTO();
      $numIdProvimentoAnterior = null;
      foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
        if ($objVotoParteDTO->getNumIdUsuario()==$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao()) {
          $numIdProvimentoAnterior = $objVotoParteDTO->getNumIdProvimento();
        } else if ($objVotoParteDTO->getStrStaVotoParte()!=VotoParteRN::$STA_ACOMPANHA) {
          $objInfraException = new InfraException();
          $objInfraException->lancarValidacao('Processo ' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . ' j� possui voto diferente registrado.');
        }
      }
      if ($numIdProvimentoAnterior==null && $dto->getNumIdProvimento()==null) {
        $objInfraException->lancarValidacao('Processo ' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . ' n�o possui provimento selecionado.');
      }
      //efetuar voto para todos os presentes - acompanham o relator - unanimidade

      $objVotoParteDTOBase->setNumIdJulgamentoParte($objJulgamentoParteDTO->getNumIdJulgamentoParte());
      $arrObjVotoParteDTO = array();
      foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
        if ($objColegiadoComposicaoDTO->getStrSinPresente()==='S') {
          $objVotoParteDTO2 = clone $objVotoParteDTOBase;
          $objVotoParteDTO2->setNumIdUsuario($objColegiadoComposicaoDTO->getNumIdUsuario());
          if ($objColegiadoComposicaoDTO->getNumIdUsuario()==$objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao()) {
            if ($dto->getNumIdProvimento()==null) {
              continue;
            }
            $objVotoParteDTO2->setNumIdProvimento($dto->getNumIdProvimento());
            $objVotoParteDTO2->setStrComplemento($dto->getStrComplemento());
            $objVotoParteDTO2->setStrSinVencedor('S');
          } else {
            $objVotoParteDTO2->setStrSinVencedor('N');
          }
          $objVotoParteDTO2->setNumIdSessaoVoto($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
          $arrObjVotoParteDTO[] = $objVotoParteDTO2;
        }
      }
      $objVotoParteRN->registrar($arrObjVotoParteDTO);
      $objJulgamentoParteDTO->setArrObjVotoParteDTO($arrObjVotoParteDTO);

      $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTO2->setNumIdUsuarioPresidente($objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getNumIdUsuarioPresidente());
      $this->alterar($objItemSessaoJulgamentoDTO2);
    }
  }

  protected function converterManualControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    try {
      SessaoSEI::getInstance()->validarAuditarPermissao('item_sessao_julgamento_manual', __METHOD__, $objItemSessaoJulgamentoDTO);

      $objInfraException = new InfraException();

      $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
      $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrProtocoloFormatadoProtocolo();
      $objItemSessaoJulgamentoDTOBanco->retStrNomeColegiado();
      $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retDblIdProcedimentoDistribuicao();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
      $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
      $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

      if ($objItemSessaoJulgamentoDTOBanco==null) {
        $objInfraException->lancarValidacao('Item da sess�o de julgamento n�o encontrado.');
      }
      if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA) {
        $objInfraException->lancarValidacao('S� � permitido converter para julgamento manual com a sess�o Aberta.');
      }
      if (!in_array($objItemSessaoJulgamentoDTOBanco->getStrStaSituacao(), array(self::$STA_NORMAL, self::$STA_EM_JULGAMENTO, self::$STA_PEDIDO_VISTA, self::$STA_JULGADO))) {
        $objInfraException->lancarValidacao('N�o � poss�vel converter para julgamento manual. Situa��o n�o permitida.');
      }

      $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao(self::$STA_EM_JULGAMENTO);
      $objItemSessaoJulgamentoDTOBanco->setStrSinManual('S');
      $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
      $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTOBanco);

      //excluir julgamento_parte adicionais deixar a 1a como integral
      $objJulgamentoParteDTO = new JulgamentoParteDTO();
      $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objJulgamentoParteDTO->setOrdNumIdJulgamentoParte(InfraDTO::$TIPO_ORDENACAO_ASC);
      $objJulgamentoParteDTO->retTodos();
      $objJulgamentoParteRN = new JulgamentoParteRN();
      $arrObjJulgamentoParteDTO = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
      $qtd = InfraArray::contar($arrObjJulgamentoParteDTO);
      if ($qtd>1) {
        $arrObjJulgamentoParteDTO[0]->setStrDescricao('Integral');
        $objJulgamentoParteRN->alterar($arrObjJulgamentoParteDTO[0]);
        $objVotoParteRN = new VotoParteRN();
        for ($i = 1; $i<$qtd; $i ++) {
          $objVotoParteDTO = new VotoParteDTO();
          $objVotoParteDTO->retNumIdVotoParte();
          $objVotoParteDTO->retNumIdVotoParteAssociado();
          $arrObjJulgamentoParteDTO[$i]->setStrSinForcarExclusao('S');
          $objVotoParteDTO->setNumIdJulgamentoParte($arrObjJulgamentoParteDTO[$i]->getNumIdJulgamentoParte());
          $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);
          $arrVP1 = array();
          $arrVP2 = array();
          foreach ($arrObjVotoParteDTO as $dto2) {
            if ($dto2->getNumIdVotoParteAssociado()!=null) {
              $arrVP1[] = $dto2;
            } else {
              $arrVP2[] = $dto2;
            }
          }
          if (InfraArray::contar($arrVP1)>0) {
            $objVotoParteRN->excluir($arrVP1);
          }
          if (InfraArray::contar($arrVP2)>0) {
            $objVotoParteRN->excluir($arrVP2);
          }
          $objJulgamentoParteRN->excluir(array($arrObjJulgamentoParteDTO[$i]));
        }
      }

      //gera andamento de adiamento na sess�o
      $objAndamentoSessaoDTO = new AndamentoSessaoDTO();
      $objAndamentoSessaoRN = new AndamentoSessaoRN();

      $objAndamentoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objAndamentoSessaoDTO->setNumIdTarefaSessao(TarefaSessaoRN::$TS_CONVERSAO_JULGAMENTO_MANUAL);

      $arrObjAtributoAndamentoSessaoDTO = array();

      $objAtributoAndamentoSessaoDTO = new AtributoAndamentoSessaoDTO();
      $objAtributoAndamentoSessaoDTO->setStrChave('PROCESSO');
      $objAtributoAndamentoSessaoDTO->setStrIdOrigem($objItemSessaoJulgamentoDTOBanco->getDblIdProcedimentoDistribuicao());
      $objAtributoAndamentoSessaoDTO->setStrValor($objItemSessaoJulgamentoDTOBanco->getStrProtocoloFormatadoProtocolo());
      $arrObjAtributoAndamentoSessaoDTO[] = $objAtributoAndamentoSessaoDTO;

      $objAndamentoSessaoDTO->setArrObjAtributoAndamentoSessaoDTO($arrObjAtributoAndamentoSessaoDTO);
      $objAndamentoSessaoRN->lancar($objAndamentoSessaoDTO);
      $objEventoSessaoDTO=new EventoSessaoDTO();
      $objEventoSessaoRN=new EventoSessaoRN();
      $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
      $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
      $objEventoSessaoDTO->setStrDescricao('Julgamento Manual');
      $objEventoSessaoRN->lancar($objEventoSessaoDTO);
    } catch (Exception $e) {
      throw new InfraException('Erro convertendo julgamento para manual', $e);
    }

  }

  protected function julgarManualControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    $objInfraException = new InfraException();

    $this->validarStrStaSituacao($objItemSessaoJulgamentoDTO, $objInfraException);

    if (!$objItemSessaoJulgamentoDTO->isSetNumIdUsuarioRelatorAcordaoDistribuicao()) {
      $objInfraException->adicionarValidacao('N�o informado o usu�rio destino do processo.');
    }

    $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objItemSessaoJulgamentoDTOBanco->retNumIdItemSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retNumIdColegiadoSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retStrStaSituacao();
    $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retStrSinManual();
    $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

    if ($objItemSessaoJulgamentoDTOBanco==null) {
      $objInfraException->lancarValidacao('Item de Sess�o n�o foi encontrado.');
    }

    if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()!=SessaoJulgamentoRN::$ES_ABERTA) {
      $objInfraException->lancarValidacao('N�o � poss�vel realizar julgamento manual, sess�o n�o est� aberta.');
    }

    $strStaSituacaoAtual = $objItemSessaoJulgamentoDTOBanco->getStrStaSituacao();
    if (!in_array($strStaSituacaoAtual, array(self::$STA_NORMAL, self::$STA_EM_JULGAMENTO, self::$STA_PEDIDO_VISTA, self::$STA_ADIADO, self::$STA_DILIGENCIA, self::$STA_JULGADO))) {
      $objInfraException->lancarValidacao('N�o � poss�vel alterar dispositivo. Situa��o n�o permitida.');
    }

    if ($objItemSessaoJulgamentoDTOBanco->getStrSinManual()!=='S') {
      $objInfraException->lancarValidacao('Item n�o est� marcado para julgamento manual.');
    }

    $staSituacao = $objItemSessaoJulgamentoDTO->getStrStaSituacao();
    if ($strStaSituacaoAtual==self::$STA_DILIGENCIA || $strStaSituacaoAtual==self::$STA_ADIADO) {
      $staSituacao = $strStaSituacaoAtual;
    }
    $objDistribuicaoDTO = new DistribuicaoDTO();
    $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao());
    $objDistribuicaoRN = new DistribuicaoRN();

    $objPedidoVistaRN = new PedidoVistaRN();
    $objPedidoVistaDTO = new PedidoVistaDTO();
    $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao());
    $objPedidoVistaDTO->setStrSinPendente('S');
    $objPedidoVistaDTO->setDthDevolucao(null);
    $objPedidoVistaDTO->retNumIdPedidoVista();
    $arrObjPedidoVistaDTO = $objPedidoVistaRN->listar($objPedidoVistaDTO);

    if ($arrObjPedidoVistaDTO) {
      $objPedidoVistaRN->excluir($arrObjPedidoVistaDTO);
    }

    switch ($staSituacao) {
      case self::$STA_EM_JULGAMENTO:
        $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao($objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao());
        break;
      case self::$STA_PEDIDO_VISTA:
        $idUsuarioVista = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao();
        $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao(null);
        if ($idUsuarioVista==null || $idUsuarioVista==$objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioRelatorDistribuicao()) {
          $objInfraException->lancarValidacao('N�o � permitido pedido de vista para o relator.');
        }

        $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
        $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
        $objColegiadoComposicaoDTO->setNumIdColegiadoColegiadoVersao($objItemSessaoJulgamentoDTOBanco->getNumIdColegiadoSessaoJulgamento());
        $objColegiadoComposicaoDTO->setStrSinUltimaColegiadoVersao('S');
        $objColegiadoComposicaoDTO->setNumIdUsuario($idUsuarioVista);
        $objColegiadoComposicaoDTO->retNumIdUnidade();
        $objColegiadoComposicaoDTO = $objColegiadoComposicaoRN->consultar($objColegiadoComposicaoDTO);

        $objPedidoVistaDTO = new PedidoVistaDTO();
        $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao());
        $objPedidoVistaDTO->setStrSinPendente('N');
        $objPedidoVistaDTO->setNumIdUsuario($idUsuarioVista);
        $objPedidoVistaDTO->setNumIdUnidade($objColegiadoComposicaoDTO->getNumIdUnidade());
        $objPedidoVistaDTO->setDthPedido(InfraData::getStrDataHoraAtual());
        $objPedidoVistaDTO->setDthDevolucao(null);
        $objPedidoVistaRN->cadastrar($objPedidoVistaDTO);

        break;
      case self::$STA_ADIADO:
      case self::$STA_DILIGENCIA:
        $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao(null);
        break;
      default:
        $objInfraException->lancarValidacao('Situa��o de item n�o prevista para julgamento manual.');
    }
    $objDistribuicaoRN->alterar($objDistribuicaoDTO);


    $objItemSessaoJulgamentoDTOBanco->setStrStaSituacao($staSituacao);
    $objItemSessaoJulgamentoDTOBanco->setStrDispositivo($objItemSessaoJulgamentoDTO->getStrDispositivo());
    $this->alterar($objItemSessaoJulgamentoDTOBanco);

    $objEventoSessaoDTO=new EventoSessaoDTO();
    $objEventoSessaoRN=new EventoSessaoRN();
    $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
    $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
    $objEventoSessaoDTO->setStrDescricao('Julgamento Manual');
    $objEventoSessaoRN->lancar($objEventoSessaoDTO);
  }

  public function gerarTextoDispositivo(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {

    $arrObjColegiadoComposicaoDTO = InfraArray::indexarArrInfraDTO($objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO(), 'IdUsuario');
    $arrGeneroMembros=array();
    foreach ($objItemSessaoJulgamentoDTO->getArrObjColegiadoComposicaoDTO() as $objColegiadoComposicaoDTO) {
      $numIdUsuario=$objColegiadoComposicaoDTO->getNumIdUsuario();
      $arrGeneroMembros[$numIdUsuario]=$arrObjColegiadoComposicaoDTO[$numIdUsuario]->getStrStaGeneroCargo();
    }

    $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
    $arrObjPedidoVistaDTO = $objItemSessaoJulgamentoDTO->getArrObjPedidoVistaDTO();
    $idUsuarioRelator = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorDistribuicao();
    /** @var SessaoJulgamentoDTO $objSessaoJulgamentoDTO */
    $objSessaoJulgamentoDTO=$objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO();
    if($objSessaoJulgamentoDTO->getStrStaSituacao()==SessaoJulgamentoRN::$ES_ABERTA){
      $idUsuarioPresidente = $objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getNumIdUsuarioPresidente();
    } else {
      $idUsuarioPresidente = $objItemSessaoJulgamentoDTO->getNumIdUsuarioPresidente();
    }


    if($idUsuarioPresidente==null){
      throw new InfraException('Sem Presidente identificado');
    }
    $arrObjDispositivoDTO = array();//[parte]=dados da parte

    $idUsuarioAcordaoInformado = $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao(); //informado na tela de gera��o de dispositivo

    $idUsuarioVistaAnterior = null;
    $idUsuarioVistaNovo = null;
    foreach ($arrObjPedidoVistaDTO as $objPedidoVistaDTO) {
      /* @var $objPedidoVistaDTO PedidoVistaDTO */
      if ($objPedidoVistaDTO->getStrSinPendente()==='N' &&
          ($objPedidoVistaDTO->getDthDevolucao()==null ||
              ($objSessaoJulgamentoDTO->getStrStaSituacao()==SessaoJulgamentoRN::$ES_ENCERRADA && $objSessaoJulgamentoDTO->getDthFim()==$objPedidoVistaDTO->getDthDevolucao())
          )
      ) {
        $idUsuarioVistaAnterior = $objPedidoVistaDTO->getNumIdUsuario();
        break;
      }
    }

    $arrObjBloqItemSessaoUnidadeDTO=$objItemSessaoJulgamentoDTO->getArrObjBloqueioItemSessUnidadeDTO();
    $arrIdUnidadesBloqueadas=array();
    if(count($arrObjBloqItemSessaoUnidadeDTO)){
      /** @var BloqueioItemSessUnidadeDTO $objBloqItemSessaoUnidadeDTO */
      foreach ($arrObjBloqItemSessaoUnidadeDTO as $objBloqItemSessaoUnidadeDTO){
        if($objBloqItemSessaoUnidadeDTO->getStrStaTipo()==BloqueioItemSessUnidadeRN::$TB_PROCEDIMENTO){
          $arrIdUnidadesBloqueadas[]=$objBloqItemSessaoUnidadeDTO->getNumIdUnidade();
        }
      }
    }
    $objVotoParteRN = new VotoParteRN();

    $objContagemVotosDTOBase = new ContagemVotosDTO();
    $objContagemVotosDTOBase->setNumIdUsuarioRelator($idUsuarioRelator);
    $objContagemVotosDTOBase->setNumIdSessaoAtual($objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
    $objContagemVotosDTOBase->setArrObjColegiadoComposicaoDTO($arrObjColegiadoComposicaoDTO);
    $objContagemVotosDTOBase->setNumTitularesColegiado($objItemSessaoJulgamentoDTO->getNumTitularesColegiado());
    $objContagemVotosDTOBase->setArrIdUnidadesBloqueadas($arrIdUnidadesBloqueadas);
    $objContagemVotosDTOBase->setNumQuorumMinimo($objSessaoJulgamentoDTO->getNumQuorumMinimoColegiado());

    $idParteVista = null;
    /** @var JulgamentoParteDTO $objJulgamentoParteDTO */
    foreach ($arrObjJulgamentoParteDTO as $key => $objJulgamentoParteDTO) {
      $objDispositivoDTO = new DispositivoDTO();
      $objDispositivoDTO->setStrNome($objJulgamentoParteDTO->getStrDescricao());

      $arrObjVotoParteDTO = $objJulgamentoParteDTO->getArrObjVotoParteDTO();
      $arrObjVotoParteDTO = InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO, 'IdUsuario');

      $objContagemVotosDTO = clone $objContagemVotosDTOBase;
      $objContagemVotosDTO->setArrObjVotoParteDTO($arrObjVotoParteDTO);
      $objContagemVotosDTO->setNumIdUsuarioDesempate($objJulgamentoParteDTO->getNumIdUsuarioDesempate());
      $objContagemVotosDTO = $this->calcularVotos($objContagemVotosDTO);

      $arrVotos = $objContagemVotosDTO->getArrVotos();

      if ($objContagemVotosDTO->getBolPedidoVista()) {
        $idUsuarioVistaNovo = $arrVotos[VotoParteRN::$STA_PEDIDO_VISTA][0];
        $idParteVista = $key;
      }

      $objDispositivoDTO->setStrRessalvas(implode($objContagemVotosDTO->getArrRessalvas()));

      if ($idUsuarioVistaAnterior && !isset($arrObjVotoParteDTO[$idUsuarioVistaAnterior])) {
        $strDispositivo='N�o foi poss�vel gerar dispositivo porque falta o voto-vista d';
        $strDispositivo.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioVistaAnterior]);
        if(substr($strDispositivo,-1)==','){
          $strDispositivo=substr($strDispositivo,0,-1);
        }
        $objDispositivoDTO->setStrDispositivo($strDispositivo . ".\n\n");
      }
      if (!isset($arrObjVotoParteDTO[$idUsuarioRelator])) {
        $strDispositivo='N�o foi poss�vel gerar dispositivo porque falta o voto d';
        $strDispositivo.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioRelator],false,true);
        if(substr($strDispositivo,-1)==','){
          $strDispositivo=substr($strDispositivo,0,-1);
        }
        $objDispositivoDTO->setStrDispositivo($strDispositivo.".\n\n");
      } elseif ($objContagemVotosDTO->getBolFaltamVotos() && !$objContagemVotosDTO->getBolPedidoVista()) {
        $objDispositivoDTO->setStrDispositivo("N�o foi poss�vel gerar dispositivo porque faltam votos.\n\n");
      }

      $idUsuarioVencedor = null;
      if ($objContagemVotosDTO->getBolPedidoVista()) {
        $objDispositivoDTO->setArrAguardam($arrVotos[VotoParteRN::$STA_AGUARDA_VISTA]);
      } else {
        $objDispositivoDTO->setArrAguardam([]);
      }

      if (!$objContagemVotosDTO->getBolDivergencia()) {
        $objDispositivoDTO->setStrCriterio('unanimidade');
        $idUsuarioVencedor = $idUsuarioRelator;
      } else {
        $objDispositivoDTO->setStrCriterio('maioria');
        //definir relator para acordao e encaminhar processo para a unidade do respectivo membro
        //se houve empate:
        if ($objContagemVotosDTO->getBolEmpate()) {
          if ($objJulgamentoParteDTO->getNumIdUsuarioDesempate()!=null) {
            $objDispositivoDTO->setStrCriterio('voto de desempate');
            $idUsuarioVencedor = $objJulgamentoParteDTO->getNumIdUsuarioDesempate();
          } else {
            $objDispositivoDTO->setStrDispositivo('Julgamento empatado.');
          }
        } else {
          $idUsuarioVencedor = $objContagemVotosDTO->getNumIdUsuarioAcordao();
        }
      }
      $objDispositivoDTO->setNumIdUsuarioVencedor($idUsuarioVencedor);

      if ($idUsuarioVencedor!=$idUsuarioRelator && $idUsuarioAcordaoInformado==null) {
        $idUsuarioAcordaoInformado = $idUsuarioVencedor;
      }
      if($idUsuarioAcordaoInformado!=null && $arrObjColegiadoComposicaoDTO[$idUsuarioAcordaoInformado]->isSetBolComposicaoAnterior() && $arrObjColegiadoComposicaoDTO[$idUsuarioAcordaoInformado]->getBolComposicaoAnterior()){
        $idUsuarioAcordaoInformado=null;
      }

      //busca texto provimento
      if (isset($arrObjVotoParteDTO[$idUsuarioVencedor])) {
        if (!$arrObjVotoParteDTO[$idUsuarioVencedor]->isSetStrConteudoProvimento()) {
          $objProvimentoDTO = new ProvimentoDTO();
          $objProvimentoRN = new ProvimentoRN();
          $objProvimentoDTO->setNumIdProvimento($arrObjVotoParteDTO[$idUsuarioVencedor]->getNumIdProvimento());
          $objProvimentoDTO->retStrConteudo();
          $objProvimentoDTO = $objProvimentoRN->consultar($objProvimentoDTO);
          $arrObjVotoParteDTO[$idUsuarioVencedor]->setStrConteudoProvimento($objProvimentoDTO->getStrConteudo());
        }
        $objDispositivoDTO->setStrProvimento(InfraString::transformarCaixaBaixa($arrObjVotoParteDTO[$idUsuarioVencedor]->getStrConteudoProvimento()) . ' ' . $arrObjVotoParteDTO[$idUsuarioVencedor]->getStrComplemento());
      } else {
        $objDispositivoDTO->setStrProvimento('N�O FOI DEFINIDA A DECIS�O');
      }

      //monta lista de acompanhantes e vencidos
      if (!$objDispositivoDTO->isSetStrDispositivo()) {
        if ($objDispositivoDTO->getStrCriterio()==='unanimidade') {
          $objVotoParteRN->registrarVotoVencedor($arrObjVotoParteDTO[$idUsuarioRelator]);
        } else {
          $arrIdUsuarioAcompanhantes = $objContagemVotosDTO->getArrAcompanham();
          $arrIdUsuarioVencidos = $objContagemVotosDTO->getArrDivergem();

          //vencedor
          if ($idUsuarioVencedor==null) {
            $objDispositivoDTO->setStrVencedor('Empate');
          } else {
            $objDispositivoDTO->setStrVencedor($this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioVencedor],$idUsuarioPresidente==$idUsuarioVencedor,$idUsuarioVencedor==$idUsuarioRelator,false));
            $objVotoParteRN->registrarVotoVencedor($arrObjVotoParteDTO[$idUsuarioVencedor]);
          }
          //acompanham
          $objDispositivoDTO->setStrAcompanham($this->obterStrUsuarios($arrIdUsuarioAcompanhantes, $arrObjColegiadoComposicaoDTO, $idUsuarioPresidente, $idUsuarioRelator));
          //vencidos
          $str = '';
          if ($idUsuarioVencedor!=$idUsuarioRelator) {
            $str=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioRelator],$idUsuarioPresidente==$idUsuarioRelator,true,false);
            $str.=' ';
          }
          $arrIdUsuarioVencidos = array_diff($arrIdUsuarioVencidos, array($idUsuarioRelator));
          if (InfraArray::contar($arrIdUsuarioVencidos)==0) {
            $str = substr($str, 0, - 2);
          } else {
            $str .= $this->obterStrUsuarios(array_values($arrIdUsuarioVencidos), $arrObjColegiadoComposicaoDTO, $idUsuarioPresidente, $idUsuarioRelator);
          }
          $objDispositivoDTO->setStrVencidos($str);
        }
        $objDispositivoDTO->setStrAbstencoes($this->obterStrUsuarios($arrVotos[VotoParteRN::$STA_ABSTENCAO], $arrObjColegiadoComposicaoDTO, $idUsuarioPresidente));
        $objDispositivoDTO->setStrImpedimentos($this->obterStrUsuarios($arrVotos[VotoParteRN::$STA_IMPEDIMENTO], $arrObjColegiadoComposicaoDTO, $idUsuarioPresidente));

      }
      $arrObjDispositivoDTO[$key] = $objDispositivoDTO;
    }
    $numPartes = InfraArray::contar($arrObjJulgamentoParteDTO);


//montar dispositivo
    $strRetorno = '';
    for ($i = 0; $i<$numPartes; $i ++) {
      /** @var DispositivoDTO $objDispositivoDTO */
      $objDispositivoDTO = $arrObjDispositivoDTO[$i];
      if ($i>0) {
        $strRetorno .= "\n";
      }
      if ($objDispositivoDTO->isSetStrDispositivo()) {
        if ($numPartes>1) {
          $strRetorno .=  $objDispositivoDTO->getStrNome() . ': ';
        }
        $strRetorno .= $objDispositivoDTO->getStrDispositivo() . "\n";
        continue;
      }

      $strDispositivo = '';
      if ($numPartes>1) {
        $strDispositivo .= $objDispositivoDTO->getStrNome() . ': ';
      }
      //devolveu vista
      if ($idUsuarioVistaAnterior!=null && $idUsuarioVistaAnterior!=$idUsuarioVistaNovo) {
        $strDispositivo .= 'Prosseguindo no julgamento, ap�s o voto-vista d';
        $strDispositivo .= $this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioVistaAnterior],$idUsuarioPresidente==$idUsuarioVistaAnterior) . ', ';
      }

      //pediu vista
      if ($idUsuarioVistaNovo!=null) {
        if ($idParteVista==$i) {
          //ninguem devolveu vista
          $arrObjVotoParteDTO = $arrObjJulgamentoParteDTO[$i]->getArrObjVotoParteDTO();
          foreach ($arrObjVotoParteDTO as $chave => $objVotoParteDTO) {
            $numIdUsuario=$objVotoParteDTO->getNumIdUsuario();
            if ($numIdUsuario==$idUsuarioVistaAnterior || $numIdUsuario==$idUsuarioVistaNovo ||
                $objVotoParteDTO->getNumIdSessaoVoto()!=$objItemSessaoJulgamentoDTO->getNumIdSessaoJulgamento() ||
                !in_array($objVotoParteDTO->getStrStaVotoParte(),array(VotoParteRN::$STA_ACOMPANHA,VotoParteRN::$STA_DIVERGE))
            ) {
              unset($arrObjVotoParteDTO[$chave]);
            }
          }
          $arrObjVotoParteDTO = array_values($arrObjVotoParteDTO);
          InfraArray::ordenarArrInfraDTO($arrObjVotoParteDTO, 'Voto', InfraArray::$TIPO_ORDENACAO_ASC);
          $numVotos=InfraArray::contar($arrObjVotoParteDTO);
          if($numVotos>0){
            if ($idUsuarioVistaAnterior==null) {
              $numIdUsuario=$arrObjVotoParteDTO[0]->getNumIdUsuario();
              $bolRelator=$numIdUsuario==$idUsuarioRelator;
              $bolPresidente=$idUsuarioPresidente==$numIdUsuario;
              $strDispositivo .= 'Ap�s o voto d';
              $strDispositivo .= $this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$numIdUsuario],$bolPresidente,$bolRelator);
              if(!$bolPresidente && substr($strDispositivo,-1)==','){
                $strDispositivo=substr($strDispositivo,0,-1);
              }
              if($numVotos==2){

                $strDispositivo .= ' e d';
                $numIdUsuario=$arrObjVotoParteDTO[1]->getNumIdUsuario();
                $strDispositivo.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$numIdUsuario],$idUsuarioPresidente==$numIdUsuario);
                if(substr($strDispositivo,-1)==','){
                  $strDispositivo=substr($strDispositivo,0,-1);
                }
                $strDispositivo.=', ';
              } else if ($numVotos>2) {
                if(substr($strDispositivo,-1)==','){
                  $strDispositivo=substr($strDispositivo,0,-1);
                }
                $strDispositivo .= ' e dos membros ';
                for ($j=1;$j<$numVotos;$j++){
                  $numIdUsuario=$arrObjVotoParteDTO[$j]->getNumIdUsuario();
                  $bolRelator=$numIdUsuario==$idUsuarioRelator;
                  $bolPresidente=$idUsuarioPresidente==$numIdUsuario;
                  $strDispositivo .= $this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$numIdUsuario],$bolPresidente,$bolRelator,false);

                  if($j+1<$numVotos) {
                    if(($j + 2)==$numVotos){
                      $strDispositivo .= ' e ' ;
                    } else {
                      if(substr($strDispositivo,-1)!=','){
                        $strDispositivo .=  ',';
                      }
                      $strDispositivo .=  ' ';
                    }

                  }
                }
              }
            } else if($numVotos==1){
              $strDispositivo .= 'votou ';
              $numIdUsuario=$arrObjVotoParteDTO[0]->getNumIdUsuario();
              $strDispositivo.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$numIdUsuario],$idUsuarioPresidente==$numIdUsuario);
              $strDispositivo.=', ap�s ';
            } else if ($numVotos>1) {
              $strDispositivo .= 'votaram ';

              for ($j=0;$j<$numVotos;$j++){
                $numIdUsuario=$arrObjVotoParteDTO[$j]->getNumIdUsuario();
                $strDispositivo.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$numIdUsuario],$idUsuarioPresidente==$numIdUsuario);
                $strDispositivo.= ($j+2)==$numVotos?' e ':', ';
              }
              $strDispositivo.='ap�s ';
            }
          }


          if ($strDispositivo=='') {
            $strDispositivo = 'Pediu vista ';
          } else {
            if(substr($strDispositivo,-2)!=', '){
              $strDispositivo.=', ';
            }
            $strDispositivo .= 'pediu vista ';
          }
          $strDispositivo.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioVistaNovo],$idUsuarioPresidente==$idUsuarioVistaNovo);

          if (InfraArray::contar($objDispositivoDTO->getArrAguardam())==1) {
            $strGenero = $this->obterStringGenero('o(a)', $arrGeneroMembros[$objDispositivoDTO->getArrAguardam()[0]]);
            $strDispositivo .= ', aguarda '.$strGenero.' ';
            $strDispositivo .= $this->obterStrUsuarios($objDispositivoDTO->getArrAguardam(), $arrObjColegiadoComposicaoDTO, $idUsuarioPresidente);
            $strDispositivo .= '.';
          } elseif (count($objDispositivoDTO->getArrAguardam())>1) {
            $strDispositivo .= ', aguardam os demais.';
          } else {
            $strDispositivo .='.';
          }
        }
      } else {
        $strArtigo=$objItemSessaoJulgamentoDTO->getStrArtigoColegiado();
        if ($strDispositivo==''|| substr($strDispositivo,-2)==': '){
          $strDispositivo .= strtoupper($strArtigo);
        } else {
          $strDispositivo .= strtolower($strArtigo);
        }
        if(InfraString::isBolVazia($strArtigo)){
          $strDispositivo .= 'O Colegiado ';
        } else {
          $strDispositivo.=' ';
        }
        $strDispositivo .= $objItemSessaoJulgamentoDTO->getStrNomeColegiado() . ', por ' . $objDispositivoDTO->getStrCriterio() . ', ';
        $strDispositivo .= rtrim($objDispositivoDTO->getStrProvimento());
        $strDispositivo .= ', nos termos do voto d';
        $idUsuarioVencedor=$objDispositivoDTO->getNumIdUsuarioVencedor();
        $strDispositivo.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioVencedor],$idUsuarioPresidente==$idUsuarioVencedor,$idUsuarioRelator==$idUsuarioVencedor);
        if(substr($strDispositivo,-1)==','){
          $strDispositivo=substr($strDispositivo,0,-1);
        }
        $strDispositivo .= '.';

//        if (isset($arrPartes[$i]['ressalvas'])){
//          $strDispositivo.="\n\n".'Ressalvas: '.$arrPartes[$i]['ressalvas'];
//        }
        if ($objDispositivoDTO->getStrCriterio()!=='unanimidade') {

          if ($objDispositivoDTO->getStrAcompanham()!='') {
            $strDispositivo .= "\n\n" . 'Acompanham: ' . $objDispositivoDTO->getStrAcompanham();
            if(substr($strDispositivo,-1)==','){
              $strDispositivo=substr($strDispositivo,0,-1);
            }
            $strDispositivo.= '.';
          }
          if ($objDispositivoDTO->getStrVencidos()!='') {
            $strDispositivo .= "\n\n" . 'Vencidos: ' . $objDispositivoDTO->getStrVencidos();
            if(substr($strDispositivo,-1)==','){
              $strDispositivo=substr($strDispositivo,0,-1);
            }
            $strDispositivo.= '.';
          }
        }
      }
      if ($objDispositivoDTO->getStrAbstencoes()!='') {
        $strDispositivo .= "\n\n" . 'Absten��es: ' . $objDispositivoDTO->getStrAbstencoes();
        if(substr($strDispositivo,-1)==','){
          $strDispositivo=substr($strDispositivo,0,-1);
        }
        $strDispositivo.= '.';
      }
      if ($objDispositivoDTO->getStrImpedimentos()!='') {
        $strDispositivo .= "\n\n" . 'Impedimentos: ' . $objDispositivoDTO->getStrImpedimentos();
        if(substr($strDispositivo,-1)==','){
          $strDispositivo=substr($strDispositivo,0,-1);
        }
        $strDispositivo.= '.';
      }


      $strRetorno .= $strDispositivo . "\n";
    }
    $strRetorno .= SustentacaoOralINT::montarTextoDispositivo($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    if ($idUsuarioAcordaoInformado!=null && $idUsuarioAcordaoInformado!=$idUsuarioRelator && in_array($objItemSessaoJulgamentoDTO->getStrStaSituacao(),array(ItemSessaoJulgamentoRN::$STA_EM_JULGAMENTO,ItemSessaoJulgamentoRN::$STA_JULGADO))) {
      $strRetorno .= "\n" . 'Relator para Ac�rd�o: ' . $this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$idUsuarioAcordaoInformado],$idUsuarioPresidente==$idUsuarioAcordaoInformado,$idUsuarioRelator==$idUsuarioAcordaoInformado,false);
      if(substr($strRetorno,-1)==','){
        $strRetorno=substr($strRetorno,0,-1);
      }
      $strRetorno.= '.' . "\n";
    }
    $strRetorno .= "\n" . 'Presentes: ';
    $strRetorno .= $this->obterStrUsuarios(array_keys($objContagemVotosDTO->getArrPresentes()), $arrObjColegiadoComposicaoDTO, $idUsuarioPresidente, $idUsuarioRelator, true);
    if(substr($strRetorno,-1)==','){
      $strRetorno=substr($strRetorno,0,-1);
    }
    $strRetorno.= '.' . "\n";



//    if (in_array($objItemSessaoJulgamentoDTO->getObjSessaoJulgamentoDTO()->getStrStaSituacao(),array(SessaoJulgamentoRN::$ES_ABERTA,SessaoJulgamentoRN::$ES_ENCERRADA))) {
//      $objDistribuicaoDTO = new DistribuicaoDTO();
//      $objDistribuicaoDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTO->getNumIdDistribuicao());
//      if ($idUsuarioAcordaoInformado==null) {
//        $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao(null);
//        $objDistribuicaoDTO->setNumIdUnidadeRelatorAcordao(null);
//      } else {
//        $objDistribuicaoDTO->setNumIdUsuarioRelatorAcordao($idUsuarioAcordaoInformado);
//        $objDistribuicaoDTO->setNumIdUnidadeRelatorAcordao($arrObjColegiadoComposicaoDTO[$idUsuarioAcordaoInformado]->getNumIdUnidade());
//      }
//      $objDistribuicaoRN = new DistribuicaoRN();
//      $objDistribuicaoRN->alterar($objDistribuicaoDTO);
//    }
    $objItemSessaoJulgamentoDTO->setNumIdUsuarioRelatorAcordaoDistribuicao($idUsuarioAcordaoInformado);

    return $strRetorno;
  }

  public function gerarConteudoCertidao(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {
    $bolCancelamento = false;
    if ($objItemSessaoJulgamentoDTO->isSetStrSinCancelamentoSessao() && $objItemSessaoJulgamentoDTO->getStrSinCancelamentoSessao()==='S') {
      $bolCancelamento = true;
    }
    $strXML = '<?xml version="1.0" encoding="iso-8859-1"?>' . "\n";
    $strXML .= '<documento>' . "\n";
    $strXML .= '<atributo nome="' . self::$FORM_PROTOCOLO . '" titulo="Processo">' . $objItemSessaoJulgamentoDTO->getStrProtocoloFormatadoProtocolo() . ' - ' . InfraString::formatarXML($objItemSessaoJulgamentoDTO->getStrNomeTipoProcedimento()) . '</atributo>' . "\n";
    $strXML .= '<atributo nome="' . self::$FORM_NOME_COLEGIADO . '" titulo="Colegiado">' . InfraString::formatarXML($objItemSessaoJulgamentoDTO->getStrNomeColegiado()) . '</atributo>' . "\n";
    $strXML .= '<atributo nome="' . self::$FORM_DTH_SESSAO . '" titulo="Data da Sess�o">' . $objItemSessaoJulgamentoDTO->getDthSessaoSessaoJulgamento() . '</atributo>' . "\n";
    $strXML .= '<atributo nome="' . self::$FORM_RELATOR . '" titulo="Relator">' . InfraString::formatarXML($objItemSessaoJulgamentoDTO->getStrNomeUsuarioRelator()) . '</atributo>' . "\n";
    if (!$bolCancelamento && $objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao()!=null) {
      $objUsuarioDTO = new UsuarioDTO();
      $objUsuarioRN = new UsuarioRN();
      $objUsuarioDTO->setNumIdUsuario($objItemSessaoJulgamentoDTO->getNumIdUsuarioRelatorAcordaoDistribuicao());
      $objUsuarioDTO->retStrNome();
      $objUsuarioDTO->setBolExclusaoLogica(false);
      $objUsuarioDTO = $objUsuarioRN->consultarRN0489($objUsuarioDTO);
      $strXML .= '<atributo nome="' . self::$FORM_RELATOR_ACORDAO . '" titulo="Relator do Acord�o">' . InfraString::formatarXML($objUsuarioDTO->getStrNome()) . '</atributo>' . "\n";
    }
    $strXML .= '<atributo nome="' . self::$FORM_DISPOSITIVO . '" titulo="Dispositivo">';
    if ($bolCancelamento) {
      $strXML .= 'Sess�o de Julgamento cancelada.';
    } else {
      $strXML .= InfraString::formatarXML($objItemSessaoJulgamentoDTO->getStrDispositivo());
    }
    $strXML .= '</atributo>' . "\n";
    if ($bolCancelamento) {
      $strXML .= '<atributo nome="' . self::$FORM_DTA_CANCELAMENTO_SESSAO . '" titulo="Data do cancelamento da Sess�o">' . InfraData::getStrDataAtual() . '</atributo>' . "\n";
    }
    $strXML .= '</documento>';

    return $strXML;
  }

  protected function gerarCertidaoJulgamentoControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): DocumentoDTO
  {

    $objInfraParametro = new InfraParametro($this->inicializarObjInfraIBanco());
    $objDocumentoRN = new DocumentoRN();

//TODO: converter para uso da API - falta permitir formul�rio_autom�tico

//    $objDocumentoAPI=new DocumentoAPI();
//    $objDocumentoAPI->setTipo(ProtocoloRN::$TP_DOCUMENTO_GERADO);
//    $objDocumentoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
//    $objDocumentoAPI->setIdSerie($objInfraParametro->getValor('ID_SERIE_CERTIDAO_JULGAMENTO'));
//    $objDocumentoAPI->setNumero(null);
//    $objDocumentoAPI->setData(null);
//    $objDocumentoAPI->setDescricao(null);
//    $objDocumentoAPI->setNomeArquivo(null);
//    $objDocumentoAPI->setRemetente(null);
//    $objDocumentoAPI->setDestinatarios(array());
//    $objDocumentoAPI->setObservacao(null);
//    $objDocumentoAPI->setConteudo(base64_encode(utf8_encode(InfraUtil::filtrarISO88591($this->gerarConteudoCertidao($objItemSessaoJulgamentoDTO)))));
//    $objDocumentoAPI->setNivelAcesso(ProtocoloRN::$NA_PUBLICO);
//    $objDocumentoAPI->setSinBloqueado('N');
//
//    $objSeiRN->incluirDocumento($objDocumentoAPI);

    $objProtocoloDTO = new ProtocoloDTO();
    $objProtocoloDTO->setDblIdProtocolo(null);
    $objProtocoloDTO->setStrStaNivelAcessoLocal(ProtocoloRN::$NA_PUBLICO);
    $objProtocoloDTO->setStrDescricao(null);
    $objProtocoloDTO->setDtaGeracao(InfraData::getStrDataAtual());
    $objProtocoloDTO->setArrObjRelProtocoloAssuntoDTO(array());
    $objProtocoloDTO->setArrObjParticipanteDTO(array());
    $objProtocoloDTO->setArrObjObservacaoDTO(array());

    $objDocumentoDTO = new DocumentoDTO();
    $objDocumentoDTO->setDblIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
    $objDocumentoDTO->setDblIdDocumento(null);
    $objDocumentoDTO->setNumIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_JULGAMENTO));
    $objDocumentoDTO->setDblIdDocumentoEdoc(null);
    $objDocumentoDTO->setDblIdDocumentoEdocBase(null);
    $objDocumentoDTO->setNumIdUnidadeResponsavel(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objDocumentoDTO->setStrNumero(null);
    $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_FORMULARIO_AUTOMATICO);
    $objDocumentoDTO->setStrConteudo(InfraUtil::filtrarISO88591($this->gerarConteudoCertidao($objItemSessaoJulgamentoDTO)));

    $objDocumentoDTO->setObjProtocoloDTO($objProtocoloDTO);
    $objDocumentoDTO = $objDocumentoRN->cadastrarRN0003($objDocumentoDTO);

    $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objItemSessaoJulgamentoDTO2->setDblIdDocumentoJulgamento($objDocumentoDTO->getDblIdDocumento());
    $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
    $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTO2);

    return $objDocumentoDTO;
  }
  protected function gerarAcordaoControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): DocumentoDTO
  {

    $objInfraParametro = new InfraParametro($this->inicializarObjInfraIBanco());
    $objDocumentoRN = new DocumentoRN();

//TODO: converter para uso da API

//    $objDocumentoAPI=new DocumentoAPI();
//    $objDocumentoAPI->setTipo(ProtocoloRN::$TP_DOCUMENTO_GERADO);
//    $objDocumentoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
//    $objDocumentoAPI->setIdSerie($objInfraParametro->getValor('ID_SERIE_CERTIDAO_JULGAMENTO'));
//    $objDocumentoAPI->setNumero(null);
//    $objDocumentoAPI->setData(null);
//    $objDocumentoAPI->setDescricao(null);
//    $objDocumentoAPI->setNomeArquivo(null);
//    $objDocumentoAPI->setRemetente(null);
//    $objDocumentoAPI->setDestinatarios(array());
//    $objDocumentoAPI->setObservacao(null);
//    $objDocumentoAPI->setConteudo(base64_encode(utf8_encode(InfraUtil::filtrarISO88591($this->gerarConteudoCertidao($objItemSessaoJulgamentoDTO)))));
//    $objDocumentoAPI->setNivelAcesso(ProtocoloRN::$NA_PUBLICO);
//    $objDocumentoAPI->setSinBloqueado('N');
//
//    $objSeiRN->incluirDocumento($objDocumentoAPI);

    $objProtocoloDTO = new ProtocoloDTO();
    $objProtocoloDTO->setDblIdProtocolo(null);
    $objProtocoloDTO->setStrStaNivelAcessoLocal(ProtocoloRN::$NA_PUBLICO);
    $objProtocoloDTO->setStrDescricao(null);
    $objProtocoloDTO->setDtaGeracao(InfraData::getStrDataAtual());
    $objProtocoloDTO->setArrObjRelProtocoloAssuntoDTO(array());
    $objProtocoloDTO->setArrObjParticipanteDTO(array());
    $objProtocoloDTO->setArrObjObservacaoDTO(array());

    $objDocumentoDTO = new DocumentoDTO();
    $objDocumentoDTO->setDblIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
    $objDocumentoDTO->setDblIdDocumento(null);
    $objDocumentoDTO->setNumIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_ACORDAO));
    $objDocumentoDTO->setDblIdDocumentoEdoc(null);
    $objDocumentoDTO->setDblIdDocumentoEdocBase(null);
    $objDocumentoDTO->setNumIdUnidadeResponsavel($objItemSessaoJulgamentoDTO->getNumIdUnidadeRelatorAcordaoDistribuicao());
    $objDocumentoDTO->setStrNumero(null);
    $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_EDITOR_INTERNO);
    $arrSecaoConteudo=array();
//    $arrSecaoConteudo['Ementa']=$objItemSessaoJulgamentoDTO->getStrDispositivo();
    $arrSecaoConteudo['Corpo do Texto']='<p class="Texto_Centralizado_Maiusculas">AC�RD�O</p><p class="Texto_Justificado_Recuo_Primeira_Linha">'.$objItemSessaoJulgamentoDTO->getStrDispositivo().'</p>';
    $objDocumentoDTO->setArrSecaoConteudo($arrSecaoConteudo);

    $objDocumentoDTO->setObjProtocoloDTO($objProtocoloDTO);
    $objDocumentoDTO = $objDocumentoRN->cadastrarRN0003($objDocumentoDTO);

    $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objItemSessaoJulgamentoDTO2->setDblIdDocumentoJulgamento($objDocumentoDTO->getDblIdDocumento());
    $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
    $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTO2);

    return $objDocumentoDTO;
  }
  protected function gerarCertidaoCancelamentoControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {

    $objInfraParametro = new InfraParametro($this->inicializarObjInfraIBanco());
    $objDocumentoRN = new DocumentoRN();
    $bolFecharProcesso = false;
    $bolReabrirProcesso = true;

    $objSeiRN = new SeiRN();
    $objEntradaConsultarProcedimentoAPI = new EntradaConsultarProcedimentoAPI();
    $objEntradaConsultarProcedimentoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
    $objEntradaConsultarProcedimentoAPI->setSinRetornarUnidadesProcedimentoAberto('S');
    $objSaidaConsultarProcedimentoAPI = $objSeiRN->consultarProcedimento($objEntradaConsultarProcedimentoAPI);
    $arrObjUnidadeAPI = $objSaidaConsultarProcedimentoAPI->getUnidadesProcedimentoAberto();

    $numIdUnidadeAtual = SessaoSEI::getInstance()->getNumIdUnidadeAtual();
    foreach ($arrObjUnidadeAPI as $objUnidadeAPI) {
      if ($objUnidadeAPI->getUnidade()->getIdUnidade()==$numIdUnidadeAtual) {
        $bolReabrirProcesso = false;
        break;
      }
    }
    if ($bolReabrirProcesso) {
      $objEntradaReabrirProcessoAPI = new EntradaReabrirProcessoAPI();
      $objEntradaReabrirProcessoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
      $objSeiRN->reabrirProcesso($objEntradaReabrirProcessoAPI);
      $bolFecharProcesso = true;
    }
//TODO: converter para uso da API - falta permitir formul�rio_autom�tico
    $objProtocoloDTO = new ProtocoloDTO();
    $objProtocoloDTO->setDblIdProtocolo(null);
    $objProtocoloDTO->setStrStaNivelAcessoLocal(ProtocoloRN::$NA_PUBLICO);
    $objProtocoloDTO->setStrDescricao(null);
    $objProtocoloDTO->setDtaGeracao(InfraData::getStrDataAtual());
    $objProtocoloDTO->setArrObjRelProtocoloAssuntoDTO(array());
    $objProtocoloDTO->setArrObjParticipanteDTO(array());
    $objProtocoloDTO->setArrObjObservacaoDTO(array());

    $objDocumentoDTO = new DocumentoDTO();
    $objDocumentoDTO->setDblIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
    $objDocumentoDTO->setDblIdDocumento(null);
    $objDocumentoDTO->setNumIdSerie($objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_CERTIDAO_CANCELAMENTO_SESSAO));
    $objDocumentoDTO->setDblIdDocumentoEdoc(null);
    $objDocumentoDTO->setDblIdDocumentoEdocBase(null);
    $objDocumentoDTO->setNumIdUnidadeResponsavel(SessaoSEI::getInstance()->getNumIdUnidadeAtual());
    $objDocumentoDTO->setStrNumero(null);
    $objDocumentoDTO->setStrStaDocumento(DocumentoRN::$TD_FORMULARIO_AUTOMATICO);
    $objItemSessaoJulgamentoDTO2 = clone $objItemSessaoJulgamentoDTO;
    $objItemSessaoJulgamentoDTO2->setStrSinCancelamentoSessao('S');
    $objDocumentoDTO->setStrConteudo(InfraUtil::filtrarISO88591($this->gerarConteudoCertidao($objItemSessaoJulgamentoDTO2)));

    $objDocumentoDTO->setObjProtocoloDTO($objProtocoloDTO);
    $objDocumentoDTO = $objDocumentoRN->cadastrarRN0003($objDocumentoDTO);

    if ($bolFecharProcesso) {
      $objEntradaConcluirProcessoAPI = new EntradaConcluirProcessoAPI();
      $objEntradaConcluirProcessoAPI->setIdProcedimento($objItemSessaoJulgamentoDTO->getDblIdProcedimentoDistribuicao());
      $objSeiRN->concluirProcesso($objEntradaConcluirProcessoAPI);
    }

    $objItemSessaoJulgamentoDTO2 = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTO2->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objItemSessaoJulgamentoDTO2->setDblIdDocumentoJulgamento($objDocumentoDTO->getDblIdDocumento());
    $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
    $objItemSessaoJulgamentoBD->alterar($objItemSessaoJulgamentoDTO2);

    return $objDocumentoDTO;
  }

  protected function montarTextoRessalvasConectado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO): string
  {
    $objVotoParteRN = new VotoParteRN();
    $objVotoParteDTO = new VotoParteDTO();
    $objVotoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objVotoParteDTO->setStrRessalva(null, InfraDTO::$OPER_DIFERENTE);
    $objVotoParteDTO->retStrNomeUsuario();
    $objVotoParteDTO->retNumIdUsuario();
    $objVotoParteDTO->retStrRessalva();
    $objVotoParteDTO->retStrDescricaoJulgamentoParte();
    $objVotoParteDTO->retNumIdJulgamentoParte();
    $objVotoParteDTO->retNumIdColegiadoVersaoSessaoJulgamentoVoto();
    $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);
    $strRessalvas = '';
    if (InfraArray::contar($arrObjVotoParteDTO)>0) {
      $objColegiadoComposicaoDTO = new ColegiadoComposicaoDTO();
      $objColegiadoComposicaoRN = new ColegiadoComposicaoRN();
      $objColegiadoComposicaoDTO->setNumIdColegiadoVersao($arrObjVotoParteDTO[0]->getNumIdColegiadoVersaoSessaoJulgamentoVoto());
      $objColegiadoComposicaoDTO->setNumIdUsuario(InfraArray::converterArrInfraDTO($arrObjVotoParteDTO, 'IdUsuario'), InfraDTO::$OPER_IN);
      $objColegiadoComposicaoDTO->retNumIdUsuario();
      $objColegiadoComposicaoDTO->retStrExpressaoCargo();
      $objColegiadoComposicaoDTO->retStrNomeUsuario();
      $arrCargosUsuarios = InfraArray::converterArrInfraDTO($objColegiadoComposicaoRN->listar($objColegiadoComposicaoDTO), 'ExpressaoCargo', 'IdUsuario');

      $objJulgamentoParteDTO = new JulgamentoParteDTO();
      $objJulgamentoParteRN = new JulgamentoParteRN();
      $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
      $bolDetalhar = false;
      if ($objJulgamentoParteRN->contar($objJulgamentoParteDTO)>1) {
        $bolDetalhar = true;
      }
      $arrJulgamentoParte = InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO, 'IdJulgamentoParte', true);
      foreach ($arrJulgamentoParte as $arrObjVotoParteDTO) {
        if ($bolDetalhar) {
          $strRessalvas .= $arrObjVotoParteDTO[0]->getStrDescricaoJulgamentoParte() . "\n\n";
        }
        foreach ($arrObjVotoParteDTO as $objVotoParteDTO) {
          $strCargo = $arrCargosUsuarios[$objVotoParteDTO->getNumIdUsuario()];
          if ($strCargo!=null) {
            $strRessalvas .= $strCargo . ' ';
          }
          $strRessalvas .= $objVotoParteDTO->getStrNomeUsuario() . ': ' . $objVotoParteDTO->getStrRessalva() . "\n";
        }

      }

    }
    return $strRessalvas;
  }

  public function calcularVotos(ContagemVotosDTO $objContagemVotosDTO): ContagemVotosDTO
  {
    $idUsuarioRelator = $objContagemVotosDTO->getNumIdUsuarioRelator();
    $arrObjColegiadoComposicaoDTO = $objContagemVotosDTO->getArrObjColegiadoComposicaoDTO();
    $arrObjVotoParteDTO = InfraArray::indexarArrInfraDTO($objContagemVotosDTO->getArrObjVotoParteDTO(), 'IdUsuario');
    $arrIdUnidadesBloqueadas=$objContagemVotosDTO->getArrIdUnidadesBloqueadas();
    $numQuorumMinimo=$objContagemVotosDTO->getNumQuorumMinimo();

    $bolFaltamVotos = false;
    $bolPedidoVista = false;
    $bolPedidoVistaAnterior = false;
    $arrVotos = array();
    $arrVotos[$idUsuarioRelator] = array();

    $arrVotos[VotoParteRN::$STA_AUSENTE] = array();
    $arrVotos[VotoParteRN::$STA_ABSTENCAO] = array();
    $arrVotos[VotoParteRN::$STA_IMPEDIMENTO] = array();
    $arrVotos[VotoParteRN::$STA_PEDIDO_VISTA] = array();
    $arrVotos[VotoParteRN::$STA_AGUARDA_VISTA] = array();

    $arrRessalvas = array();
    $arrPresentes = array();

    $numVotosContagemQuorum=0;
    /** @var ColegiadoComposicaoDTO $objColegiadoComposicaoDTO */
    foreach ($arrObjColegiadoComposicaoDTO as $objColegiadoComposicaoDTO) {
      $idUsuario = $objColegiadoComposicaoDTO->getNumIdUsuario();
      if (isset($arrObjVotoParteDTO[$idUsuario])) {
        if ($arrObjVotoParteDTO[$idUsuario]->getStrRessalva()!=null) {
          $arrRessalvas[] = $objColegiadoComposicaoDTO->getStrNomeUsuario() . ' - ' . $arrObjVotoParteDTO[$idUsuario]->getStrRessalva() . "\n";
        }
        if($objColegiadoComposicaoDTO->getStrSinPresente()==='S') {
          $arrPresentes[$idUsuario] = 1;
        }
        $staVotoParte = $arrObjVotoParteDTO[$idUsuario]->getStrStaVotoParte();
        switch ($staVotoParte) {
          case VotoParteRN::$STA_AUSENTE:
            unset($arrPresentes[$idUsuario]);
            $arrVotos[$staVotoParte][] = $idUsuario;
            break;
          case VotoParteRN::$STA_ABSTENCAO:
          case VotoParteRN::$STA_IMPEDIMENTO:
            $arrVotos[$staVotoParte][] = $idUsuario;
//            unset($arrObjVotoParteDTO[$idUsuario]);
            break;
          case VotoParteRN::$STA_ACOMPANHA:
            $numVotosContagemQuorum++;
            $idRelacionado = $arrObjVotoParteDTO[$idUsuario]->getNumIdUsuarioVotoParte();
            if ($idRelacionado==null) {
              $idRelacionado = $idUsuarioRelator;
              $arrObjVotoParteDTO[$idUsuario]->setNumIdUsuarioVotoParte($idUsuarioRelator);
            }
            if (!isset($arrVotos[$idRelacionado])) {
              $arrVotos[$idRelacionado] = array($idUsuario);
            } else {
              $arrVotos[$idRelacionado][] = $idUsuario;
            }
            break;
          case VotoParteRN::$STA_DIVERGE:
            $numVotosContagemQuorum++;
            $arrObjVotoParteDTO[$idUsuario]->setNumIdUsuarioVotoParte($idUsuario);
            if (!isset($arrVotos[$idUsuario])) {
              $arrVotos[$idUsuario] = array($idUsuario);
            } else {
              $arrVotos[$idUsuario][] = $idUsuario;
            }
            break;
          case VotoParteRN::$STA_PEDIDO_VISTA:
            $numVotosContagemQuorum++;
            $arrVotos[$staVotoParte][] = $idUsuario;
            if ($arrObjVotoParteDTO[$idUsuario]->getNumIdSessaoVoto()!=$objContagemVotosDTO->getNumIdSessaoAtual()) {
              $bolPedidoVistaAnterior = true;
            }
            $bolPedidoVista = true;
            break;
          case VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO:
            $bolFaltamVotos = true;
            $numVotosContagemQuorum++;
            $arrVotos[$staVotoParte][] = $idUsuario;
            break;
          case VotoParteRN::$STA_AGUARDA_VISTA:
            $numVotosContagemQuorum++;
            $arrVotos[$staVotoParte][] = $idUsuario;
            break;
          default:
            throw new InfraException('Tipo de voto n�o previsto');
        }

      } else if ($objColegiadoComposicaoDTO->getStrSinPresente()==='S') {
        $arrPresentes[$idUsuario] = 1;
        if(!in_array($objColegiadoComposicaoDTO->getNumIdUnidade(),$arrIdUnidadesBloqueadas)) {
          $bolFaltamVotos = true;
          $numVotosContagemQuorum++;
        }
      }
    }

    //prepara resumo vota��o
    $arrObjResumoVotacaoDTO=array();

    //verifica voto vencedor/empate
    $arrCalculo = [];
    foreach ($arrVotos as $usuario => $arrIdUsuario) {
      $objResumoVotacaoDTO=new ResumoVotacaoDTO();
      $arrMembros=array();
      foreach ($arrIdUsuario as $idMembro) {
        if($idMembro!=$usuario) {
          $arrMembros[] = $arrObjVotoParteDTO[$idMembro]->getStrNomeUsuario();
        }
      }
      if (is_numeric($usuario) && isset($arrObjVotoParteDTO[$usuario])) {
        $arrCalculo[$usuario] = InfraArray::contar($arrIdUsuario);
        if($arrCalculo[$usuario]>0){
          $objResumoVotacaoDTO->setNumVotos($arrCalculo[$usuario]);
          $objResumoVotacaoDTO->setStrProvimento($arrObjVotoParteDTO[$usuario]->getStrConteudoProvimento());
          $objResumoVotacaoDTO->setStrComplemento($arrObjVotoParteDTO[$usuario]->getStrComplemento());
          $objResumoVotacaoDTO->setArrAcompanham($arrMembros);
          $objResumoVotacaoDTO->setArrMembros(array($arrObjVotoParteDTO[$usuario]->getStrNomeUsuario()));
          if($usuario==$idUsuarioRelator){
            $objResumoVotacaoDTO->setStrTipo(VotoParteRN::$STA_ACOMPANHA);
          } else {
            $objResumoVotacaoDTO->setStrTipo(VotoParteRN::$STA_DIVERGE);
          }
          $arrObjResumoVotacaoDTO[]=$objResumoVotacaoDTO;
        }
      } else if($usuario==VotoParteRN::$STA_PROVIMENTO_DISPONIBILIZADO){
        if($usuario==$idUsuarioRelator) {
          $objResumoVotacaoDTO->setNumVotos(1);
          $objResumoVotacaoDTO->setStrProvimento($arrObjVotoParteDTO[$usuario]->getStrConteudoProvimento());
          $objResumoVotacaoDTO->setStrComplemento($arrObjVotoParteDTO[$usuario]->getStrComplemento());
          $objResumoVotacaoDTO->setArrAcompanham($arrMembros);
          $objResumoVotacaoDTO->setArrMembros(array($arrObjVotoParteDTO[$usuario]->getStrNomeUsuario()));
          $objResumoVotacaoDTO->setStrTipo(VotoParteRN::$STA_ACOMPANHA);
          $arrObjResumoVotacaoDTO[] = $objResumoVotacaoDTO;
        }
      } else if(InfraArray::contar($arrIdUsuario)>0){
        $objResumoVotacaoDTO->setNumVotos(InfraArray::contar($arrIdUsuario));
        $objResumoVotacaoDTO->setStrProvimento('');
        $objResumoVotacaoDTO->setStrComplemento('');
        $objResumoVotacaoDTO->setArrAcompanham(array());

        $objResumoVotacaoDTO->setArrMembros($arrMembros);
        $objResumoVotacaoDTO->setStrTipo($usuario);
        $arrObjResumoVotacaoDTO[] = $objResumoVotacaoDTO;
      }
    }
    $objContagemVotosDTO->setArrObjResumoVotacaoDTO($arrObjResumoVotacaoDTO);
    arsort($arrCalculo);
    $maior = 0;
    $idUsuarioAcordao = null;
    $arrMaiores = [];
    $arrDivergem = [];
    foreach ($arrCalculo as $usuario => $numVotos) {
      if ($numVotos>$maior) {
        $maior = $numVotos;
        $arrMaiores[] = $usuario;
        $idUsuarioAcordao = $usuario;
      } else if ($numVotos==$maior) {
        $arrMaiores[] = $usuario;
      } else {
        foreach ($arrVotos[$usuario] as $usuario2) {
          $arrDivergem[$usuario2]=$usuario2;
        }
      }
    }

    if (!$bolPedidoVistaAnterior && InfraArray::contar($arrObjVotoParteDTO)>=$objContagemVotosDTO->getNumTitularesColegiado()) {
      $bolFaltamVotos = false;
    }
    //verifica novamente quorum com votos v�lidos
    if(!$bolFaltamVotos && $numVotosContagemQuorum<$numQuorumMinimo){
      $objContagemVotosDTO->setBolFaltaQuorum(true);
      $bolFaltamVotos=true;
    } else {
      $objContagemVotosDTO->setBolFaltaQuorum(false);
    }

    //se for unanime bolDivergencia=false, ou caso haja alguma divergencia =true
    $objContagemVotosDTO->setBolDivergencia(InfraArray::contar($arrCalculo)>1);

    if (InfraArray::contar($arrMaiores)>1) {
      $objContagemVotosDTO->setBolEmpate(true);
      $objContagemVotosDTO->setArrUsuariosEmpate($arrMaiores);
      $objContagemVotosDTO->setNumIdUsuarioAcordao(null);
      $objContagemVotosDTO->setArrAcompanham(null);
      $objContagemVotosDTO->setArrDivergem(array());
      if ($objContagemVotosDTO->isSetNumIdUsuarioDesempate() && $objContagemVotosDTO->getNumIdUsuarioDesempate()!=null) {
        $idUsuarioDesempate = $objContagemVotosDTO->getNumIdUsuarioDesempate();
        foreach ($arrMaiores as $idUsuario) {
          if ($idUsuario!=$idUsuarioDesempate) {
            foreach ($arrVotos[$idUsuario] as $usuario2) {
              $arrDivergem[$usuario2]=$usuario2;
            }
          }
        }
        $objContagemVotosDTO->setArrDivergem(array_values($arrDivergem));
        $arrAcompanham = [];
        if (isset($arrVotos[$idUsuarioDesempate])) {
          foreach ($arrVotos[$idUsuarioDesempate] as $idUsuario) {
            if ($idUsuario!=$idUsuarioDesempate) {
              $arrAcompanham[] = $idUsuario;
            }
          }
        }
        $objContagemVotosDTO->setArrAcompanham($arrAcompanham);
      }
    } else {
      $arrAcompanham = [];
      if (isset($arrVotos[$idUsuarioAcordao])) {
        foreach ($arrVotos[$idUsuarioAcordao] as $idUsuario) {
          if ($idUsuario!=$idUsuarioAcordao) {
            $arrAcompanham[] = $idUsuario;
          }
        }
      }
      $objContagemVotosDTO->setArrAcompanham($arrAcompanham);
      $objContagemVotosDTO->setArrDivergem($arrDivergem);
      $objContagemVotosDTO->setBolEmpate(false);
      $objContagemVotosDTO->setNumIdUsuarioAcordao($idUsuarioAcordao);
    }
    if ($bolFaltamVotos || $bolPedidoVista) {
      $objContagemVotosDTO->setBolEmpate(false);
    }
    $objContagemVotosDTO->setBolFaltamVotos($bolFaltamVotos);
    $objContagemVotosDTO->setBolPedidoVista($bolPedidoVista);
    $objContagemVotosDTO->setBolPedidoVistaAnterior($bolPedidoVistaAnterior);
    $objContagemVotosDTO->setArrVotos($arrVotos);
    $objContagemVotosDTO->setArrRessalvas($arrRessalvas);
    $objContagemVotosDTO->setArrPresentes($arrPresentes);
    return $objContagemVotosDTO;
  }

  protected function obterItensJulgamentoFracionadoConectado(SessaoJulgamentoDTO $objSessaoJulgamentoDTO):array
  {
    $objItemSessaoJulgamentoBD = new ItemSessaoJulgamentoBD($this->getObjInfraIBanco());
    return $objItemSessaoJulgamentoBD->obterItensJulgamentoFracionado($objSessaoJulgamentoDTO->getNumIdSessaoJulgamento());
  }

  protected function fracionarJulgamentoControlado(ItemSessaoJulgamentoDTO $objItemSessaoJulgamentoDTO)
  {
    $objInfraException = new InfraException();
    $numIdItemSessaoJulgamento = $objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento();
    $arrObjJulgamentoParteDTO = $objItemSessaoJulgamentoDTO->getArrObjJulgamentoParteDTO();
    if (InfraArray::contar($arrObjJulgamentoParteDTO)==0) {
      $objInfraException->lancarValidacao('Erro gravando fra��es de julgamento. Deve haver no m�nimo um item.');
    }
    $arrObjJulgamentoParteDTO = InfraArray::indexarArrInfraDTO($arrObjJulgamentoParteDTO, 'IdJulgamentoParte');

    $objJulgamentoParteDTO = new JulgamentoParteDTO();
    $objJulgamentoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    $objJulgamentoParteDTO->retNumIdItemSessaoJulgamento();
    $objJulgamentoParteDTO->retNumIdJulgamentoParte();
    $objJulgamentoParteDTO->retNumOrdem();
    $objJulgamentoParteDTO->retStrDescricao();
    $objJulgamentoParteRN = new JulgamentoParteRN();
    $arrObjJulgamentoParteDTOBanco = $objJulgamentoParteRN->listar($objJulgamentoParteDTO);
    $arrObjJulgamentoParteDTOBanco = InfraArray::indexarArrInfraDTO($arrObjJulgamentoParteDTOBanco, 'IdJulgamentoParte');

    foreach ($arrObjJulgamentoParteDTO as $id => $objJulgamentoParteDTO) {
      if (!is_numeric($id)) {
        $objJulgamentoParteRN->cadastrar($objJulgamentoParteDTO);
        continue;
      }
      if (!isset($arrObjJulgamentoParteDTOBanco[$id])) {
        $objInfraException->lancarValidacao('Parte de julgamento n�o encontrada para alterar.');
      }
      if ($arrObjJulgamentoParteDTOBanco[$id]->getStrDescricao()!=$objJulgamentoParteDTO->getStrDescricao()
      || $arrObjJulgamentoParteDTOBanco[$id]->getNumOrdem()!=$objJulgamentoParteDTO->getNumOrdem()) {
        $objJulgamentoParteRN->alterar($objJulgamentoParteDTO);
      }
      unset($arrObjJulgamentoParteDTOBanco[$id]);
    }
    $objVotoParteDTO = new VotoParteDTO();
    $objVotoParteDTO->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    $objVotoParteDTO->retNumIdVotoParte();
    $objVotoParteDTO->retStrNomeUsuario();
    $objVotoParteDTO->retNumIdUsuario();
    $objVotoParteDTO->retNumIdProvimento();
    $objVotoParteDTO->retStrStaVotoParte();
    $objVotoParteDTO->retNumIdJulgamentoParte();
    $objVotoParteDTO->retStrConteudoProvimento();
    $objVotoParteDTO->retStrComplemento();
    $objVotoParteDTO->retNumIdUsuarioVotoParte();
    $objVotoParteRN = new VotoParteRN();
    $arrObjVotoParteDTO = $objVotoParteRN->listar($objVotoParteDTO);

    $arrObjVotoParteDTO = InfraArray::indexarArrInfraDTO($arrObjVotoParteDTO, 'IdJulgamentoParte', true);

    $objItemSessaoJulgamentoDTOBanco = new ItemSessaoJulgamentoDTO();
    $objItemSessaoJulgamentoDTOBanco->setNumIdItemSessaoJulgamento($numIdItemSessaoJulgamento);
    $objItemSessaoJulgamentoDTOBanco->retNumIdUsuarioRelatorDistribuicao();
    $objItemSessaoJulgamentoDTOBanco->retNumIdSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retNumIdUnidadeResponsavelColegiado();
    $objItemSessaoJulgamentoDTOBanco->retStrStaSituacaoSessaoJulgamento();
    $objItemSessaoJulgamentoDTOBanco->retNumIdDistribuicao();

    $objItemSessaoJulgamentoDTOBanco = $this->consultar($objItemSessaoJulgamentoDTOBanco);

    $bolAdmin = false;
    if ($objItemSessaoJulgamentoDTOBanco->getStrStaSituacaoSessaoJulgamento()==SessaoJulgamentoRN::$ES_ABERTA &&
        SessaoSEI::getInstance()->verificarPermissao('sessao_julgamento_administrar') &&
        $objItemSessaoJulgamentoDTOBanco->getNumIdUnidadeResponsavelColegiado()==SessaoSEI::getInstance()->getNumIdUnidadeAtual()
    ) {
      $bolAdmin = true;
    }

    /** @var VotoParteDTO[][] $arrObjVotoParteDTO */
    foreach ($arrObjJulgamentoParteDTOBanco as $id => $objJulgamentoParteDTO) {
      if (!isset($arrObjJulgamentoParteDTO[$id])) {
        //exclus�o - com votos s� admin com sess�o aberta
        if (isset($arrObjVotoParteDTO[$id])) {

          if (!$bolAdmin && (InfraArray::contar($arrObjVotoParteDTO[$id])>1 || $arrObjVotoParteDTO[$id][0]->getNumIdUsuario()!=$objItemSessaoJulgamentoDTOBanco->getNumIdUsuarioRelatorDistribuicao())) {
            $objInfraException->lancarValidacao('N�o � poss�vel excluir partes de julgamento com votos.');
          }
          $objVotoParteRN->excluir($arrObjVotoParteDTO[$id]);
          //verificar se tem pedido de vista para excluir
          if (InfraArray::contar($arrObjVotoParteDTO[$id])>1) {
            foreach ($arrObjVotoParteDTO[$id] as $objVotoParteDTO) {
              if ($objVotoParteDTO->getStrStaVotoParte()==VotoParteRN::$STA_PEDIDO_VISTA) {
                //exclui pedidos de vista pendentes
                $objPedidoVistaDTO = new PedidoVistaDTO();
                $objPedidoVistaRN = new PedidoVistaRN();
                $objPedidoVistaDTO->setNumIdDistribuicao($objItemSessaoJulgamentoDTOBanco->getNumIdDistribuicao());
                $objPedidoVistaDTO->retNumIdPedidoVista();
                $objPedidoVistaDTO->setNumIdUsuario($objVotoParteDTO->getNumIdUsuario());
                $objPedidoVistaDTO->setStrSinPendente('S');
                $objPedidoVistaRN->excluir($objPedidoVistaRN->listar($objPedidoVistaDTO));
              }
            }

          }
        }
        $objJulgamentoParteRN->excluir(array($objJulgamentoParteDTO));
      }
    }
    $objEventoSessaoDTO=new EventoSessaoDTO();
    $objEventoSessaoRN=new EventoSessaoRN();
    $objEventoSessaoDTO->setNumIdSessaoJulgamento($objItemSessaoJulgamentoDTOBanco->getNumIdSessaoJulgamento());
    $objEventoSessaoDTO->setNumIdItemSessaoJulgamento($objItemSessaoJulgamentoDTO->getNumIdItemSessaoJulgamento());
    $objEventoSessaoDTO->setNumTipoEvento(EventoSessaoRN::$TE_ALTERACAO_STATUS_ITEM);
    $objEventoSessaoDTO->setStrDescricao('Fracionamento de Julgamento');
    $objEventoSessaoRN->lancar($objEventoSessaoDTO);

  }

  /**
   * @param $arrIdUsuario
   * @param $arrObjColegiadoComposicaoDTO
   * @param int|null $numIdUsuarioPresidente
   * @param int|null $numIdUsuarioRelator
   * @param bool $bolExibirNomeRelator
   * @return string
   */
  private function obterStrUsuarios($arrIdUsuario, $arrObjColegiadoComposicaoDTO, ?int $numIdUsuarioPresidente=null, ?int $numIdUsuarioRelator=null, bool $bolExibirNomeRelator=false): string
  {
    $numRegistros = InfraArray::contar($arrIdUsuario);
    $str = '';
    if ($arrIdUsuario>0) {

      for ($i = 0, $j = $numRegistros; $i<$numRegistros; $i ++) {

        $str.=$this->montarNomeMembro($arrObjColegiadoComposicaoDTO[$arrIdUsuario[$i]],$numIdUsuarioPresidente==$arrIdUsuario[$i],$numIdUsuarioRelator==$arrIdUsuario[$i],false,$bolExibirNomeRelator);

        if (-- $j>0) {
          if($j>1 && substr($str,-1)==',') {
            $str .= ' ';
          } else {
            $str .= $j>1 ? ', ' : ' e ';
          }
        }
      }

    }
    if(substr($str,-1)==','){
      $str=substr($str,0,-1);
    }
    return $str;
  }

  private function montarNomeMembro($objColegiadoComposicaoDTO,$bolPresidente=false,$bolRelator=false,$bolIncluirGenero=true,$bolIncluirNomeRelator=false): string
  {
    $ret='';
    $strStaGeneroCargo=$objColegiadoComposicaoDTO->getStrStaGeneroCargo();
    $strCargo = $objColegiadoComposicaoDTO->getStrExpressaoCargo();
    if ($strCargo!=null) {
      $strCargo .= ' ';
    } else {
      $strCargo = '';
    }
    $strNome=$strCargo . $objColegiadoComposicaoDTO->getStrNomeUsuario();

    if($bolIncluirGenero){
      $ret.= $this->obterStringGenero('o(a)', $strStaGeneroCargo).' ';
    }

    if($bolRelator){
      if($bolIncluirNomeRelator){
        $ret.=$strNome;
        $ret.=', ';
      }
      $ret.='Relator'.$this->obterStringGenero('(a)', $strStaGeneroCargo);
      if($bolPresidente){
        $ret.=' e Presidente';
      }
      $ret.=',';
    } else {
      $ret.=$strNome;
      if ($bolPresidente){
        $ret.=', Presidente,';
      }
    }

    return $ret;
  }
  public function obterStringGenero($strGenerica,$strStaGenero): string
  {
    if($strStaGenero!='M' && $strStaGenero!='F'){
      return $strGenerica;
    }
    $bolMasculino=$strStaGenero=='M';
    switch ($strGenerica){
      case 'o(a)':
        return $bolMasculino?'o':'a';
      case '(a)':
        return $bolMasculino?'':'a';
      case 'do(a) Relator(a)':
        return $bolMasculino?'do Relator':'da Relatora';
    }
    return $strGenerica;
  }

}

?>