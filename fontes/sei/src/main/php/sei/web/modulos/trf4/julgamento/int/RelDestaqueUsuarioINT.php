<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 14/04/2015 - criado por bcu
*
* Vers�o do Gerador de C�digo: 1.33.1
*
* Vers�o no CVS: $Id$
*/

require_once __DIR__.'/../../../../SEI.php';

class RelDestaqueUsuarioINT extends InfraINT {

  public static function montarSelectIdUsuario($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $numIdDestaque='', $numIdUsuario=''){
    $objRelDestaqueUsuarioDTO = new RelDestaqueUsuarioDTO();
    $objRelDestaqueUsuarioDTO->retNumIdDestaque();
    $objRelDestaqueUsuarioDTO->retNumIdUsuario();
    $objRelDestaqueUsuarioDTO->retNumIdUsuario();

    if ($numIdDestaque!==''){
      $objRelDestaqueUsuarioDTO->setNumIdDestaque($numIdDestaque);
    }

    if ($numIdUsuario!==''){
      $objRelDestaqueUsuarioDTO->setNumIdUsuario($numIdUsuario);
    }

    $objRelDestaqueUsuarioDTO->setOrdNumIdUsuario(InfraDTO::$TIPO_ORDENACAO_ASC);

    $objRelDestaqueUsuarioRN = new RelDestaqueUsuarioRN();
    $arrObjRelDestaqueUsuarioDTO = $objRelDestaqueUsuarioRN->listar($objRelDestaqueUsuarioDTO);

    return parent::montarSelectArrInfraDTO($strPrimeiroItemValor, $strPrimeiroItemDescricao, $strValorItemSelecionado, $arrObjRelDestaqueUsuarioDTO, 'IdUsuario', 'IdUsuario');
  }
}
?>