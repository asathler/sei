<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 31/01/2008 - criado por marcio_db
*
* Vers�o do Gerador de C�digo: 1.13.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  InfraDebug::getInstance()->setBolLigado(false);
  InfraDebug::getInstance()->setBolDebugInfra(true);
  InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  SessaoSEI::getInstance()->setArrParametrosRepasseLink(array('id_item_sessao_julgamento','id_sessao_julgamento','id_procedimento_sessao','arvore','id_colegiado','id_distribuicao'));


  $objSessaoJulgamentoDTO = new SessaoJulgamentoDTO();
  $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
  $objDistribuicaoRN = new DistribuicaoRN();

  $strTitulo = 'Sess�o de Julgamento';

  $strDesabilitar = '';
  $strParametros='';
  if (isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
  }
  if (isset($_GET['id_sessao_julgamento'])){
    $idSessaoJulgamento=$_GET['id_sessao_julgamento'];
  }

  if (isset($_GET['id_colegiado'])){
    $idColegiado=$_GET['id_colegiado'];
  } else {
    $idColegiado=null;
  }
  if (isset($_GET['id_distribuicao'])){
    $idDistribuicao=$_GET['id_distribuicao'];
  } else {
    $idDistribuicao=null;
  }


  $arrComandos = array();
  switch($_GET['acao']){
    
    case 'sessao_julgamento_alterar_presidente':
      $strTitulo = 'Alterar Presidente da Sess�o';

      if (isset($_POST['sbmSalvar'])){
        try{
        	
          $objSessaoJulgamentoDTO=new SessaoJulgamentoDTO();
          $objSessaoJulgamentoDTO->setNumIdUsuarioPresidente($_POST['selPresidente']);
          $objSessaoJulgamentoDTO->setNumIdSessaoJulgamento($idSessaoJulgamento);
          $objSessaoJulgamentoRN=new SessaoJulgamentoRN();
          $objSessaoJulgamentoRN->alterarPresidente($objSessaoJulgamentoDTO);


          header('Location: '.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].$strParametros));
          die;      		         
        }catch(Exception $e){
          PaginaSEI::getInstance()->processarExcecao($e);
        }
      }            
      $arrComandos[] = '<button type="submit" accesskey="S" name="sbmSalvar" id="sbmSalvar" value="Salvar" class="infraButton"><span class="infraTeclaAtalho">S</span>alvar</button>';     
     	$arrComandos[] = '<button type="button" accesskey="C" name="btnCancelar" value="Cancelar" onclick="location.href=\''.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.PaginaSEI::getInstance()->getAcaoRetorno().'&acao_origem='.$_GET['acao'].'&acao_destino='.$_GET['acao'].$strParametros).'\';" class="infraButton"><span class="infraTeclaAtalho">C</span>ancelar</button>';
      break;


    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

  $strItensSelPresidente = PresencaSessaoINT::montarSelectTrocaPresidente($idSessaoJulgamento,$_GET['id_usuario']);

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>
#lblPresidente {position:absolute;left:0%;top:0%;width:50%;}
#selPresidente {position:absolute;left:0%;top:6%;width:50%;}
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>


function inicializar(){
  
  document.getElementById('selPresidente').focus();
  

  infraEfeitoTabelas();
}

function OnSubmitForm() {
  return validarForm();
}

function validarForm(){
  if(infraTrim(document.getElementById('selPresidente').value)=='null'){
    alert('Informe o novo Presidente da Sess�o.');
    document.getElementById('selPresidente').focus();
    return false;
  }
  return true;
}
 
<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmAtividadeAtribuir" method="post" onsubmit="return OnSubmitForm();" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
<?
//PaginaSEI::getInstance()->montarBarraLocalizacao($strTitulo);
PaginaSEI::getInstance()->montarBarraComandosSuperior($arrComandos);
//PaginaSEI::getInstance()->montarAreaValidacao();
PaginaSEI::getInstance()->abrirAreaDados('30em');
?>

 	<label id="lblPresidente" for="selPresidente" class="infraLabelOpcional">Alterar a Presid�ncia da Sess�o para:</label>
  <select id="selPresidente" name="selPresidente" class="infraSelect" tabindex="<?=PaginaSEI::getInstance()->getProxTabDados()?>">
  <?=$strItensSelPresidente?>
  </select>
  
  <?
  PaginaSEI::getInstance()->fecharAreaDados();  
  //PaginaSEI::getInstance()->montarBarraComandosInferior($arrComandos);
  ?>
</form>
<?
PaginaSEI::getInstance()->montarAreaDebug();
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>