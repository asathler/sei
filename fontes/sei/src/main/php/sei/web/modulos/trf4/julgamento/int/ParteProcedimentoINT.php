<?
/**
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 26/10/2009 - criado por mga
 *
 * Vers�o do Gerador de C�digo: 1.29.1
 *
 * Vers�o no CVS: $Id$
 */

require_once __DIR__.'/../../../../SEI.php';

class ParteProcedimentoINT extends InfraINT {

  public static function montarHtmlPartes($arrObjParteProcedimentoDTO)
  {
    if(!is_array($arrObjParteProcedimentoDTO)){
      return '';
    }
    $strPartes = '';
    /** @var ParteProcedimentoDTO $objParteProcedimentoDTO */
    foreach($arrObjParteProcedimentoDTO as $objParteProcedimentoDTO){
      $strPartes .= '<div class="divItemCelula"><div class="divDiamante">&diams;&nbsp;&nbsp;</div><div>'.PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrNomeContato()).'&nbsp;('.PaginaSEI::tratarHTML($objParteProcedimentoDTO->getStrDescricaoQualificacaoParte()).')</div></div>';
    }

    return $strPartes;
  }
}
?>