<?
/**
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 31/01/2008 - criado por marcio_db
*
* Vers�o do Gerador de C�digo: 1.13.1
*
* Vers�o no CVS: $Id$
*/

try {
  require_once __DIR__.'/../../../SEI.php';

  session_start();

  //////////////////////////////////////////////////////////////////////////////
  //InfraDebug::getInstance()->setBolLigado(false);
  //InfraDebug::getInstance()->setBolDebugInfra(false);
  //InfraDebug::getInstance()->limpar();
  //////////////////////////////////////////////////////////////////////////////

  SessaoSEI::getInstance()->validarLink();

  SessaoSEI::getInstance()->validarPermissao($_GET['acao']);
  
  PaginaSEI::getInstance()->salvarCamposPost(array('hdnFiltroSerie'));
  
  $strParametros = '';
  if(isset($_GET['arvore'])){
    PaginaSEI::getInstance()->setBolArvore($_GET['arvore']);
    $strParametros .= '&arvore='.$_GET['arvore'];
  }

  if (isset($_GET['id_procedimento'])){
    $strParametros .= '&id_procedimento='.$_GET['id_procedimento'];
  }
  
  $objProcedimentoDTO = new ProcedimentoDTO();

  $strDesabilitar = '';
  $strDesabilitarCampo = '';

  $arrComandos = array();
  
  switch($_GET['acao']){
    
    case 'documento_escolher_tipo_julgamento':
      
      $strTitulo = 'Gerar Documento para Sess�o de Julgamento';
      
      $arrOpcoes = array();
      
      if (SessaoSEI::getInstance()->verificarPermissao('documento_gerar')){
        
        $objInfraParametro = new InfraParametro(BancoSEI::getInstance());

        $objSerieDTO = new SerieDTO();
        $objSerieDTO->retNumIdSerie();
        $objSerieDTO->retStrNome();

        $objSerieDTO->setNumIdSerie(array_filter(explode(',', $objInfraParametro->getValor(MdJulgarConfiguracaoRN::$PAR_SERIE_DOCUMENTOS_DISPONIBILIZAVEIS).',')), InfraDTO::$OPER_IN);

        $objSerieDTO->setOrdStrNome(InfraDTO::$TIPO_ORDENACAO_ASC);

        $objSerieRN = new SerieRN();
        $arrObjSerieDTO = $objSerieRN->listarRN0646($objSerieDTO);

        foreach($arrObjSerieDTO as $objSerieDTO){
          $arrOpcoes[] = array($objSerieDTO->getNumIdSerie(),$objSerieDTO->getStrNome());
        }
      }

      $strSumarioTabela = 'Tabela de Tipos de Documento.';
      
      $strResultado = '';
      $strResultado .= '<table id="tblSeries" class="infraTable" style="background-color:white;width:100%;" summary="'.$strSumarioTabela.'">'."\n";
      
	    $strResultado .= '<thead><tr style="display:none">';
      $strResultado .= '<th class="infraTh">'.PaginaSEI::getInstance()->getThCheck().'</th>'."\n";
	    $strResultado .= '</tr></thead><tbody>';
      

      InfraArray::ordenarArray($arrOpcoes,1,InfraArray::$TIPO_ORDENACAO_ASC);
      
      $numRegistros = InfraArray::contar($arrOpcoes);
      
      for($i=0;$i<$numRegistros;$i++){
        $strResultado .= '<tr  data-desc="' . strtolower(InfraString::excluirAcentos($arrOpcoes[$i][1])) . '">';
        $strResultado .= '<td style="padding-left:0px;padding-right:0px;">';
        
        $strResultado .= PaginaSEI::getInstance()->getTrCheck($i,$arrOpcoes[$i][0],$arrOpcoes[$i][1],'N','Infra','style="display:none;"');
        
        if ($arrOpcoes[$i][0] != -1){
          $strAcaoDestino = 'documento_gerar';
        }else{
          $strAcaoDestino = 'documento_receber';
        }
        
        $strResultado .= '<a style="width:100%;" href="'.SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$strAcaoDestino.'&acao_origem='.$_GET['acao'].'&acao_retorno='.$_GET['acao'].'&id_procedimento='.$_GET['id_procedimento'].'&id_serie='.$arrOpcoes[$i][0].$strParametros).'" tabindex="'.PaginaSEI::getInstance()->getProxTabTabela().'" class="ancoraOpcao">'.PaginaSEI::tratarHTML($arrOpcoes[$i][1]).'</a>'."\n";
        $strResultado .= '</td>';
        $strResultado .= '</tr>';
      }
      $strResultado .= '</tbody></table>';
		 
      break;

    default:
      throw new InfraException("A��o '".$_GET['acao']."' n�o reconhecida.");
  }

}catch(Exception $e){
  PaginaSEI::getInstance()->processarExcecao($e);
}

PaginaSEI::getInstance()->montarDocType();
PaginaSEI::getInstance()->abrirHtml();
PaginaSEI::getInstance()->abrirHead();
PaginaSEI::getInstance()->montarMeta();
PaginaSEI::getInstance()->montarTitle(PaginaSEI::getInstance()->getStrNomeSistema().' - '.$strTitulo);
PaginaSEI::getInstance()->montarStyle();
PaginaSEI::getInstance()->abrirStyle();
?>




  #tblSeries{
    border-spacing: 0px 5px;
    border-collapse: separate;
  }
  #tblSeries,
  #tblSeries td{
    border: none;
    border-radius: 5px;
    padding: 0px;

  }
  #tblSeries .ancoraOpcao{
    font-size: 14.5px;
    border-radius: 3px;
    padding:4px;
  }

  .spanRealce{
    font-size: 14.5px;
  }

  tr.infraTrSelecionada,
  tr.infraTrSelecionada td,
  td.infraTdSelecionada{
    background-color:unset !important;
  }

  #tblSeries td a:hover,#tblSeries td a:focus{
    background-color:#b0b0b0;
    color: black;
  }
  #imgExibirSeries{
    position: relative;
     top: 5px;
  }
<?
PaginaSEI::getInstance()->fecharStyle();
PaginaSEI::getInstance()->montarJavaScript();
PaginaSEI::getInstance()->abrirJavaScript();
?>

function inicializar(){
  infraEfeitoTabelas();
}  

function exibirSeries(tipo){
  document.getElementById('hdnFiltroSerie').value = tipo;
  document.getElementById('frmDocumentoEscolherTipo').submit();
}

<?
PaginaSEI::getInstance()->fecharJavaScript();
PaginaSEI::getInstance()->fecharHead();
PaginaSEI::getInstance()->abrirBody($strTitulo,'onload="inicializar();"');
?>
<form id="frmDocumentoEscolherTipo" method="post" onsubmit="return false;" action="<?=SessaoSEI::getInstance()->assinarLink('controlador.php?acao='.$_GET['acao'].'&acao_origem='.$_GET['acao'].$strParametros)?>">
  <div class="mx-auto w-50">
<br />
    <br />
  <label id="lblExibirSeries" class="infraLabelObrigatorio" style="font-size:1.6em;">Escolha o Tipo do Documento: </label>
<br />
<br />
<br />
  </div>
  <div class="mx-auto w-50">
<?
echo $strResultado;
?>
  </div>
<input type="hidden" id="hdnFiltroSerie" name="hdnFiltroSerie" value="" />
</form>
<?
PaginaSEI::getInstance()->montarAreaDebug();
PaginaSEI::getInstance()->fecharBody();
PaginaSEI::getInstance()->fecharHtml();
?>