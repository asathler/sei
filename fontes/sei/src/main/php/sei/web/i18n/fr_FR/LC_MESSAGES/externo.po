msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2020-02-28 11:42-0300\n"
"PO-Revision-Date: 2020-02-28 11:42-0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"X-Poedit-Basepath: ../../..\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Poedit-SourceCharset: iso-8859-1\n"
"X-Poedit-SearchPath-0: documento_validacao_externa.php\n"

#: documento_validacao_externa.php:40
msgid "Confer�ncia de Autenticidade de Documentos"
msgstr "V�rification d'Authenticit� des Documents"

#: documento_validacao_externa.php:52
msgid "C�digo de confirma��o inv�lido."
msgstr "Code de Confirmation invalide."

#: documento_validacao_externa.php:73
msgid "Documento n�o possui assinatura."
msgstr "Document n'a pas de signature."

#: documento_validacao_externa.php:76
msgid "Clique"
msgstr "Cliquez"

#: documento_validacao_externa.php:76 documento_validacao_externa.php:94
msgid "aqui"
msgstr "ici"

#: documento_validacao_externa.php:79
msgid "para visualizar o documento."
msgstr "pour voir le document."

#: documento_validacao_externa.php:81
msgid "para download do documento."
msgstr "t�l�charger le document."

#: documento_validacao_externa.php:94
msgid "Para valida��o da assinatura digital fa�a download do conte�do clicando "
msgstr "Pour valider la signature num�rique, t�l�charger le contenu "

#: documento_validacao_externa.php:97
msgid "Tabela de Assinaturas"
msgstr "Tableau des Signatures"

#: documento_validacao_externa.php:98
msgid "Lista de Assinaturas"
msgstr "Liste des Signatures"

#: documento_validacao_externa.php:98
msgid "registro"
msgid_plural "registros"
msgstr[0] "registre"
msgstr[1] "registres"

#: documento_validacao_externa.php:100
msgid "Assinante"
msgstr "Signateur"

#: documento_validacao_externa.php:101
msgid "Cargo/Fun��o"
msgstr "Charge/Fonction"

#: documento_validacao_externa.php:102
msgid "Data/Hora"
msgstr "Date/Heure"

#: documento_validacao_externa.php:103
msgid "Tipo"
msgstr "Type"

#: documento_validacao_externa.php:128
msgid "Certificado Digital"
msgstr "Certificat Num�rique"

#: documento_validacao_externa.php:128
msgid "Login/Senha"
msgstr "Connexion/Mot de Passe"

#: documento_validacao_externa.php:132
msgid "Download do arquivo PKCS #7 para valida��o da assinatura digital"
msgstr "T�l�chargement du fichier PKCS #7 pour validation de la signature num�rique"

#: documento_validacao_externa.php:157
msgid "Documento n�o encontrado."
msgstr "Document pas trouv�."

#: documento_validacao_externa.php:160
msgid "Link para download inv�lido."
msgstr "Lien pour t�l�chargement invalide."

#: documento_validacao_externa.php:176
msgid "Assinatura n�o encontrada."
msgstr "Signature pas trouv�e."

#: documento_validacao_externa.php:208
msgid "Documento n�o possui conte�do."
msgstr "Document n'a pas de contenu."

#: documento_validacao_externa.php:222
#, php-format
msgid "A��o '%s' n�o reconhecida."
msgstr "Action '%s' non reconnue."

#: documento_validacao_externa.php:280
msgid "Informe o C�digo Verificador."
msgstr "Informer le Code V�rificateur."

#: documento_validacao_externa.php:285
msgid "Informe o C�digo CRC."
msgstr "Informer le Code CRC."

#: documento_validacao_externa.php:290
msgid "Informe o C�digo de confirma��o."
msgstr "Informer le Code de Confirmation."

#: documento_validacao_externa.php:309
msgid "C�digo Verificador"
msgstr "Code V�rificateur"

#: documento_validacao_externa.php:312
msgid "C�digo CRC"
msgstr "Code CRC"

#: documento_validacao_externa.php:315
msgid "N�o foi poss�vel carregar a imagem de confirma��o"
msgstr "Impossible de charger l'image de confirmation"

#: documento_validacao_externa.php:318
msgid "Pesquisar"
msgstr "Rechercher"
