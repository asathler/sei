<? /** @noinspection ALL */
try{

  require_once __DIR__ .'/../web/Sip.php';

  if (!method_exists('InfraUtil','compararVersoes')){
    throw new InfraException('VERSAO DO FRAMEWORK PHP DEVE SER IGUAL OU SUPERIOR A 1.598.0 (REFERENTE A VERSAO SEI 4.0.0)');
  }

  class MdJulgarVersaoSipRN extends InfraScriptVersao {

    public function __construct(){
      parent::__construct();
    }

    protected function inicializarObjInfraIBanco(){
      return BancoSip::getInstance();
    }

    public function versao_1_0_0($strVersaoAtual){
      try{

        $numIdSistemaSei = ScriptSip::obterIdSistema('SEI');

        $numIdPerfilSeiBasico = ScriptSip::obterIdPerfil($numIdSistemaSei,'B�sico');

        $ret = ScriptSip::cadastrarPerfil($numIdSistemaSei, 'MD_Julgar_Administrador', 'Permite configura��o do m�dulo e administra��o dos cadastros (colegiados, motivos, tipos de mat�ria,...).', 'S', 'N');
        $numIdPerfilSeiSessaoJulgamentoAdm = $ret->getNumIdPerfil();

        $ret = ScriptSip::cadastrarPerfil($numIdSistemaSei, 'MD_Julgar_Secretaria', 'Distribui��o de processos e administra��o das sess�es de julgamento.', 'S', 'N');
        $numIdPerfilSeiSessaoJulgamentoSec = $ret->getNumIdPerfil();

        $ret = ScriptSip::cadastrarPerfil($numIdSistemaSei, 'MD_Julgar_B�sico', 'Acesso �s fun��es b�sicas do m�dulo como pautar processos, disponibilizar documentos e consultar as sess�es de julgamento.', 'S', 'N');
        $numIdPerfilSeiSessaoJulgamentoBas = $ret->getNumIdPerfil();

        $numIdMenuSei = ScriptSip::obterIdMenu($numIdSistemaSei, 'Principal');

        $numIdItemMenuSeiAdministracao = ScriptSip::obterIdItemMenu($numIdSistemaSei,$numIdMenuSei,'Administra��o');
        $numIdItemMenuSeiRelatorios = ScriptSip::obterIdItemMenu($numIdSistemaSei,$numIdMenuSei,'Relat�rios');

        //para auditoria
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, null, 'documento_sessao_consulta_externa');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiBasico, 'item_sessao_julgamento_listar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'andamento_sessao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'andamento_sessao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'andamento_sessao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'andamento_sessao_lancar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'andamento_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'andamento_sessao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'atributo_andamento_sessao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'atributo_andamento_sessao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'atributo_andamento_sessao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'atributo_andamento_sessao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'atributo_andamento_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'atributo_andamento_sessao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'ausencia_sessao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'ausencia_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_composicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_composicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_composicao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_versao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_versao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'colegiado_versao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_visualizar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'distribuicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'distribuicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'distribuicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'distribuicao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'documento_escolher_tipo_julgamento');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'impedimento_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'impedimento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_documento_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_documento_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_documento_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_documento_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_documento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_manual');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_ordenar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_retirar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'item_sessao_julgamento_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'doc_sessao_julgamento_disponibilizar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'doc_sessao_julgamento_indisponibilizar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'julgamento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'julgamento_parte_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'julgamento_parte_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'julgamento_parte_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'julgamento_parte_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'julgamento_parte_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'julgamento_parte_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_canc_distribuicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_canc_distribuicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_canc_distribuicao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_distribuicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_distribuicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_distribuicao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_ausencia_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_ausencia_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_ausencia_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_mesa_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_mesa_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'motivo_mesa_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'painel_distribuicao_detalhar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'painel_distribuicao_gerar_grafico');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'painel_distribuicao_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoBas,$numIdMenuSei,null,$objRecursoDTO->getNumIdRecurso(),'Painel de Distribui��o', 900);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'parte_procedimento_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'parte_procedimento_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'parte_procedimento_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'parte_procedimento_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'parte_procedimento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'parte_procedimento_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'pedido_vista_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'pedido_vista_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'pedido_vista_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'pedido_vista_devolver_secretaria');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'pedido_vista_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'pedido_vista_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'pedido_vista_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'presenca_sessao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'presenca_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'presenca_sessao_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'procedimento_autuacao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'procedimento_autuacao_gerenciar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'provimento_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'provimento_disponibilizado_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'provimento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'provimento_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'qualificacao_parte_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'qualificacao_parte_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'qualificacao_parte_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_destaque_usuario_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_destaque_usuario_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_destaque_usuario_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_destaque_usuario_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_destaque_usuario_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_destaque_usuario_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_colegiado_usuario_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_colegiado_usuario_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_proced_tipo_materia_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_proced_tipo_materia_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_proced_tipo_materia_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_proced_tipo_materia_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'resumo_pauta');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_julgamento_acompanhar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_julgamento_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_julgamento_historico');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_julgamento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_julgamento_pautar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sustentacao_oral_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sustentacao_oral_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sustentacao_oral_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_materia_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_materia_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_materia_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_membro_colegiado_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_membro_colegiado_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_parte_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_parte_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_parte_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_parte_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_parte_excluir');

        // recursos administrador do m�dulo
        $objItemMenuDTO=ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSeiAdministracao,null,'Sess�o de Julgamento', 0);
        $numIdItemMenuSessao=$objItemMenuDTO->getNumIdItemMenu();

        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'julgamento_configurar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,$objRecursoDTO->getNumIdRecurso(),'Configura��o', 10);

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'algoritmo_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'algoritmo_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'ausencia_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_composicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_composicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_composicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_composicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_composicao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_excluir');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,$objRecursoDTO->getNumIdRecurso(),'Colegiados', 20);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_reativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_versao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_versao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_versao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_versao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'colegiado_versao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'distribuicao_listar');

        $objItemMenuDTOMotivos = ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,null,'Motivos', 30);

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_ausencia_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_ausencia_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_ausencia_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_ausencia_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_ausencia_excluir');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_ausencia_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$objItemMenuDTOMotivos->getNumIdItemMenu(),$objRecursoDTO->getNumIdRecurso(),'Aus�ncia', 10);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_ausencia_reativar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_distribuicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_distribuicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_distribuicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_distribuicao_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_distribuicao_excluir');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_distribuicao_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$objItemMenuDTOMotivos->getNumIdItemMenu(),$objRecursoDTO->getNumIdRecurso(),'Distribui��o', 20);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_distribuicao_reativar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_canc_distribuicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_canc_distribuicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_canc_distribuicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_canc_distribuicao_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_canc_distribuicao_excluir');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_canc_distribuicao_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$objItemMenuDTOMotivos->getNumIdItemMenu(),$objRecursoDTO->getNumIdRecurso(),'Cancelamento de Distribui��o', 30);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_canc_distribuicao_reativar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_excluir');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$objItemMenuDTOMotivos->getNumIdItemMenu(),$objRecursoDTO->getNumIdRecurso(),'Mesa', 40);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'motivo_mesa_reativar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'impedimento_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'presenca_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'pedido_vista_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'provimento_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'provimento_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'provimento_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'provimento_excluir');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'provimento_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,$objRecursoDTO->getNumIdRecurso(),'Provimentos', 40);

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'qualificacao_parte_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'qualificacao_parte_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'qualificacao_parte_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'qualificacao_parte_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'qualificacao_parte_reativar');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'qualificacao_parte_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,$objRecursoDTO->getNumIdRecurso(),'Qualifica��es de Partes', 50);

        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'distribuicao_simular');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,$objRecursoDTO->getNumIdRecurso(),'Simular Distribui��o', 60);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_membro_colegiado_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_colegiado_usuario_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_colegiado_usuario_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_colegiado_usuario_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_colegiado_usuario_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_colegiado_usuario_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'sessao_julgamento_listar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_materia_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_materia_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_materia_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_materia_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_materia_reativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_materia_consultar');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_materia_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,$objRecursoDTO->getNumIdRecurso(),'Tipos de Mat�ria', 70);



        //recursos administrador das sess�es - secretarias
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'colegiado_versao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'algoritmo_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'algoritmo_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'algoritmo_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'algoritmo_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'ausencia_sessao_registrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'ausencia_sessao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'distribuicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'distribuicao_cancelar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'distribuicao_gerar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'impedimento_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'impedimento_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'impedimento_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_adiar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_adiamento_cancelar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_diligencia_cancelar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_diligenciar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_finalizar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_julgar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_manual');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_julgar_multiplo');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'item_sessao_julgamento_alterar_dispositivo');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'julgamento_parte_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'julgamento_parte_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'julgamento_parte_desempatar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'julgamento_parte_excluir');


        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'painel_distribuicao_global');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'pedido_vista_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'pedido_vista_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'pedido_vista_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'presenca_sessao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'presenca_sessao_registrar');

        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'relatorio_distribuicao');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoSec,$numIdMenuSei,$numIdItemMenuSeiRelatorios,$objRecursoDTO->getNumIdRecurso(),'Distribui��es', 0);

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_acompanhar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_administrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_alterar_presidente');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_cancelar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_encerrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_finalizar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_excluir');
        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoSec,$numIdMenuSei,null,$objRecursoDTO->getNumIdRecurso(),'Sess�es de Julgamento', 900);
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_pautar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sustentacao_oral_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sustentacao_oral_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sustentacao_oral_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'voto_parte_alterar_ressalva');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_reativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_alterar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_sessao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_sessao_listar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'tipo_sessao_bloco_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'tipo_sessao_bloco_listar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_bloco_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_bloco_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_bloco_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_bloco_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_bloco_consultar');

        ScriptSip::adicionarAuditoria($numIdSistemaSei,'Sess�o de Julgamento',array(
            'documento_sessao_consulta_externa',
            'colegiado_cadastrar',
            'colegiado_desativar',
            'colegiado_excluir',
            'colegiado_reativar',
            'colegiado_alterar',
            'item_sessao_julgamento_alterar_dispositivo',
            'julgamento_configurar',
            'julgamento_parte_alterar',
            'julgamento_parte_cadastrar',
            'julgamento_parte_excluir',
            'motivo_canc_distribuicao_alterar',
            'motivo_canc_distribuicao_cadastrar',
            'motivo_canc_distribuicao_desativar',
            'motivo_canc_distribuicao_excluir',
            'motivo_canc_distribuicao_reativar',
            'motivo_distribuicao_alterar',
            'motivo_distribuicao_cadastrar',
            'motivo_distribuicao_desativar',
            'motivo_distribuicao_excluir',
            'motivo_distribuicao_reativar',
            'motivo_ausencia_alterar',
            'motivo_ausencia_cadastrar',
            'motivo_ausencia_desativar',
            'motivo_ausencia_excluir',
            'motivo_ausencia_reativar',
            'motivo_mesa_alterar',
            'motivo_mesa_cadastrar',
            'motivo_mesa_desativar',
            'motivo_mesa_excluir',
            'motivo_mesa_reativar',
            'pedido_vista_devolver_secretaria',
            'presenca_sessao_excluir',
            'presenca_sessao_registrar',
            'provimento_alterar',
            'provimento_cadastrar',
            'provimento_excluir',
            'sessao_julgamento_alterar',
            'sessao_julgamento_cadastrar',
            'sessao_julgamento_excluir',
            'voto_parte_alterar_ressalva',
            'voto_parte_cadastrar',
            'voto_parte_excluir'
        ));

      }catch(Exception $e){
        throw new InfraException('Erro atualizando vers�o 1.0.0.', $e);
      }
    }
    public function versao_1_1_0($strVersaoAtual){
      try{

        $numIdSistemaSei = ScriptSip::obterIdSistema('SEI');

        $numIdPerfilSeiBasico = ScriptSip::obterIdPerfil($numIdSistemaSei,'B�sico');

        $numIdPerfilSeiSessaoJulgamentoAdm = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Administrador');
        $numIdPerfilSeiSessaoJulgamentoSec = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Secretaria');
        $numIdPerfilSeiSessaoJulgamentoBas = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_B�sico');

        $numIdMenuSei = ScriptSip::obterIdMenu($numIdSistemaSei, 'Principal');

        $numIdItemMenuSeiAdministracao = ScriptSip::obterIdItemMenu($numIdSistemaSei,$numIdMenuSei,'Administra��o');
        $numIdItemMenuSeiRelatorios = ScriptSip::obterIdItemMenu($numIdSistemaSei,$numIdMenuSei,'Relat�rios');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'algoritmo_excluir');


      }catch(Exception $e){
        throw new InfraException('Erro atualizando vers�o 1.1.0.', $e);
      }
    }
    public function versao_1_2_0($strVersaoAtual){
      try{

        $numIdSistemaSei = ScriptSip::obterIdSistema('SEI');

        $numIdPerfilSeiBasico = ScriptSip::obterIdPerfil($numIdSistemaSei,'B�sico');

        $numIdPerfilSeiSessaoJulgamentoAdm = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Administrador');
        $numIdPerfilSeiSessaoJulgamentoSec = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Secretaria');
        $numIdPerfilSeiSessaoJulgamentoBas = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_B�sico');

        $numIdMenuSei = ScriptSip::obterIdMenu($numIdSistemaSei, 'Principal');

        $numIdItemMenuSeiAdministracao = ScriptSip::obterIdItemMenu($numIdSistemaSei,$numIdMenuSei,'Administra��o');
        $numIdItemMenuSeiRelatorios = ScriptSip::obterIdItemMenu($numIdSistemaSei,$numIdMenuSei,'Relat�rios');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_marcar_leitura');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_desmarcar_leitura');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_motivo_distr_colegiado_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_motivo_distr_colegiado_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_motivo_distr_colegiado_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_motivo_distr_colegiado_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_motivo_distr_colegiado_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_motivo_distr_colegiado_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_motivo_distr_colegiado_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_motivo_distr_colegiado_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'rel_motivo_distr_colegiado_excluir');


      }catch(Exception $e){
        throw new InfraException('Erro atualizando vers�o 1.2.0.', $e);
      }
    }
    public function versao_1_3_0($strVersaoAtual){
      try{

        $numIdSistemaSei = ScriptSip::obterIdSistema('SEI');

        $numIdPerfilSeiBasico = ScriptSip::obterIdPerfil($numIdSistemaSei,'B�sico');

        $numIdPerfilSeiSessaoJulgamentoAdm = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Administrador');
        $numIdPerfilSeiSessaoJulgamentoSec = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Secretaria');
        $numIdPerfilSeiSessaoJulgamentoBas = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_B�sico');

        $numIdMenuSei = ScriptSip::obterIdMenu($numIdSistemaSei, 'Principal');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'bloqueio_item_sess_unidade_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'bloqueio_item_sess_unidade_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'bloqueio_item_sess_unidade_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'bloqueio_item_sess_unidade_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'bloqueio_item_sess_unidade_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'bloqueio_item_sess_unidade_selecionar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'unidade_selecionar_colegiado');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'destaque_marcar_leitura_todos');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'revisao_item_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'revisao_item_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'revisao_item_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'revisao_item_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'revisao_item_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'revisao_item_selecionar');


      }catch(Exception $e){
        throw new InfraException('Erro atualizando vers�o 1.3.0.', $e);
      }
    }
    public function versao_1_4_0($strVersaoAtual){
      try{

        $numIdSistemaSei = ScriptSip::obterIdSistema('SEI');

        $numIdPerfilSeiBasico = ScriptSip::obterIdPerfil($numIdSistemaSei,'B�sico');

        $numIdPerfilSeiSessaoJulgamentoAdm = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Administrador');
        $numIdPerfilSeiSessaoJulgamentoSec = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Secretaria');
        $numIdPerfilSeiSessaoJulgamentoBas = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_B�sico');

        $numIdMenuSei = ScriptSip::obterIdMenu($numIdSistemaSei, 'Principal');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_autuacao_tipo_materia_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_autuacao_tipo_materia_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_autuacao_tipo_materia_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_autuacao_tipo_materia_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_autuacao_tipo_materia_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'rel_autuacao_tipo_materia_selecionar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_julgamento_associar_doc_pauta');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'provimento_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'provimento_reativar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'eleicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'eleicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_clonar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'eleicao_votar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_liberar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_concluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_finalizar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'eleicao_anular');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'opcao_eleicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'opcao_eleicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'opcao_eleicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'opcao_eleicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'opcao_eleicao_excluir');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_eleicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_eleicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_eleicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_eleicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_eleicao_excluir');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_opcao_eleicao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_opcao_eleicao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_opcao_eleicao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_opcao_eleicao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'voto_opcao_eleicao_excluir');

        ScriptSip::adicionarAuditoria($numIdSistemaSei,'Sess�o de Julgamento',array(
            'bloqueio_item_sess_unidade_cadastrar',
            'bloqueio_item_sess_unidade_alterar',
            'bloqueio_item_sess_unidade_excluir',
            'provimento_desativar',
            'provimento_reativar',
            'eleicao_cadastrar',
            'eleicao_alterar',
            'eleicao_excluir',
            'eleicao_clonar',
            'eleicao_liberar',
            'eleicao_concluir',
            'eleicao_finalizar',
            'eleicao_anular'
        ));

        BancoSip::getInstance()->executarSql('update item_menu set icone=\'md_julgar_painel_distribuicao.svg\' where rotulo=\'Painel de Distribui��o\' and id_item_menu_pai is null and id_sistema='.$numIdSistemaSei);



      }catch(Exception $e){
        throw new InfraException('Erro atualizando vers�o 1.4.0.', $e);
      }
    }
    public function versao_2_0_0($strVersaoAtual){
      try{

        $numIdSistemaSei = ScriptSip::obterIdSistema('SEI');

        $numIdPerfilSeiBasico = ScriptSip::obterIdPerfil($numIdSistemaSei,'B�sico');

        $numIdPerfilSeiSessaoJulgamentoAdm = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Administrador');
        $numIdPerfilSeiSessaoJulgamentoSec = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_Secretaria');
        $numIdPerfilSeiSessaoJulgamentoBas = ScriptSip::obterIdPerfil($numIdSistemaSei,'MD_Julgar_B�sico');

        $numIdMenuSei = ScriptSip::obterIdMenu($numIdSistemaSei, 'Principal');
        $numIdItemMenuSessao = ScriptSip::obterIdItemMenu($numIdSistemaSei,$numIdMenuSei,'Sess�o de Julgamento');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_sessao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_excluir');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_desativar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_reativar');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_sessao_bloco_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'tipo_sessao_bloco_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_bloco_excluir');

        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_bloco_consultar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoBas, 'sessao_bloco_listar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_bloco_cadastrar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_bloco_alterar');
        ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoSec, 'sessao_bloco_excluir');

        $objRecursoDTO = ScriptSip::adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiSessaoJulgamentoAdm, 'tipo_sessao_listar');
        ScriptSip::adicionarItemMenu($numIdSistemaSei,$numIdPerfilSeiSessaoJulgamentoAdm,$numIdMenuSei,$numIdItemMenuSessao,$objRecursoDTO->getNumIdRecurso(),'Tipos de Sess�o', 80);

        ScriptSip::adicionarAuditoria($numIdSistemaSei,'Sess�o de Julgamento',array(
            'procedimento_autuacao_gerenciar',
          'tipo_sessao_cadastrar',
          'tipo_sessao_excluir',
          'tipo_sessao_alterar',
          'tipo_sessao_desativar',
          'tipo_sessao_reativar'
        ));
      }catch(Exception $e){
        throw new InfraException('Erro atualizando vers�o 2.0.0.', $e);
      }
    }
  }

  if (!isset($_SESSION)) {
    session_start();
  }

  SessaoSip::getInstance(false);
  BancoSip::getInstance()->setBolScript(true);

  $objMdJulgarVersaoSipRN = new MdJulgarVersaoSipRN();
  $objMdJulgarVersaoSipRN->setStrNome('MODULO SEI JULGAR');
  $objMdJulgarVersaoSipRN->setStrVersaoAtual('2.0.3');
  $objMdJulgarVersaoSipRN->setStrParametroVersao('MD_JULGAR_VERSAO');
  $objMdJulgarVersaoSipRN->setArrVersoes(array('1.0.*' => 'versao_1_0_0','1.1.*'=>'versao_1_1_0','1.2.*'=>'versao_1_2_0','1.3.*'=>'versao_1_3_0','1.4.*'=>'versao_1_4_0','2.0.*'=>'versao_2_0_0'));
  $objMdJulgarVersaoSipRN->setStrVersaoInfra('1.598.0');
  $objMdJulgarVersaoSipRN->setBolMySql(true);
  $objMdJulgarVersaoSipRN->setBolOracle(true);
  $objMdJulgarVersaoSipRN->setBolSqlServer(true);
  if(method_exists($objMdJulgarVersaoSipRN,'setBolPostgreSql')){
    $objMdJulgarVersaoSipRN->setBolPostgreSql(true);
  }
  $objMdJulgarVersaoSipRN->setBolErroVersaoInexistente(false);

//  if (InfraUtil::compararVersoes(SIP_VERSAO, '<', '2.1.1')){
//    $objMdJulgarVersaoSipRN->processarErro('VERSAO DO SIP DEVE SER IGUAL OU SUPERIOR A 2.1.1 (REFERENTE A VERSAO SEI 3.1.1)');
//  }

  $objMdJulgarVersaoSipRN->atualizarVersao();

}catch(Exception $e){
  echo(InfraException::inspecionar($e));
  try{LogSip::getInstance()->gravar(InfraException::inspecionar($e));	}catch (Exception $e){}
  exit(1);
}
?>