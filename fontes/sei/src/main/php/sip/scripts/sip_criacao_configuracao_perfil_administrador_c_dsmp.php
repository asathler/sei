<?
try {

    require_once dirname(__FILE__).'/../web/SEI.php';

    class MdUtlPerfilRN extends InfraRN
    {

        private $numSeg = 0;

        public function __construct()
        {
            parent::__construct();
        }

        protected function inicializarObjInfraIBanco()
        {
            return BancoSEI::getInstance();
        }


        private function inicializar($strTitulo)
        {
            ini_set('max_execution_time', '0');
            ini_set('memory_limit', '-1');

            try {
                @ini_set('zlib.output_compression', '0');
                @ini_set('implicit_flush', '1');
            } catch (Exception $e) {
            }

            ob_implicit_flush();

            InfraDebug::getInstance()->setBolLigado(true);
            InfraDebug::getInstance()->setBolDebugInfra(true);
            InfraDebug::getInstance()->setBolEcho(true);
            InfraDebug::getInstance()->limpar();

            $this->numSeg = InfraUtil::verificarTempoProcessamento();

            $this->logar($strTitulo);
        }

        private function logar($strMsg)
        {
            InfraDebug::getInstance()->gravar($strMsg);
            flush();
        }

        private function finalizar($strMsg = null, $bolErro)
        {
            if (!$bolErro) {
                $this->numSeg = InfraUtil::verificarTempoProcessamento($this->numSeg);
                $this->logar('TEMPO TOTAL DE EXECUO: ' . $this->numSeg . ' s');
            } else {
                $strMsg = 'ERRO: ' . $strMsg;
            }

            if ($strMsg != null) {
                $this->logar($strMsg);
            }

            InfraDebug::getInstance()->setBolLigado(false);
            InfraDebug::getInstance()->setBolDebugInfra(false);
            InfraDebug::getInstance()->setBolEcho(false);
            $this->numSeg = 0;
            die;
        }

        protected function realizarCorrecaoConectado()
        {
            try {
                $this->inicializar('INICIANDO');

                $this->logar('Criando perfil');
                $this->cadastrarPerfilAdministradorControleDesempenho();
                $this->logar('FIM da Criando perfil');

                $this->logar('Vinculando recursos');
                $this->configurarPerfilAdministradorControleDesempenho();
                $this->logar('FIM Vinculando recursos');

                $this->finalizar('FIM', false);

                InfraDebug::getInstance()->setBolLigado(false);
                InfraDebug::getInstance()->setBolDebugInfra(false);
                InfraDebug::getInstance()->setBolEcho(false);

            } catch (Exception $e) {

                var_dump($e);
                InfraDebug::getInstance()->setBolLigado(true);
                InfraDebug::getInstance()->setBolDebugInfra(true);
                InfraDebug::getInstance()->setBolEcho(true);
                $this->logar($e->getTraceAsString());
                $this->finalizar('FIM', true);
                print_r($e);
                die;
                throw new InfraException('Erro instalando/atualizando verso.', $e);
            }
        }

        protected function cadastrarPerfilAdministradorControleDesempenho()
        {
            $numIdSistemaSei = $this->_getIdSistema();

            $objPerfilRN = new PerfilRN();
            $objPerfilDTO = new PerfilDTO();
            $objPerfilDTO->retNumIdPerfil();
            $objPerfilDTO->setNumIdSistema($numIdSistemaSei);
            $objPerfilDTO->setStrNome($this->nomeAdministradorControleDesempenho);
            $objPerfilDTO = $objPerfilRN->consultar($objPerfilDTO);

            if (!$objPerfilDTO) {
                $objPerfilDTO = new PerfilDTO();
                $objPerfilDTO->setNumIdSistema($numIdSistemaSei);
                $objPerfilDTO->setStrNome('Administrador - Controle de Desempenho');
                $objPerfilDTO->setStrDescricao('Acesso aos recursos de Administra��o do M�dulo de Controle de Desempenho.');
                $objPerfilDTO->setStrSinCoordenado('N');
                $objPerfilDTO->setStrSinAtivo('S');
                $objPerfilRN->cadastrar($objPerfilDTO);
            }
        }

        protected function configurarPerfilAdministradorControleDesempenho()
        {
            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Par�metriza��o de Tipo de Controle em Administrador de controle de desempenho');
            $numIdSistemaSei = $this->_getIdSistema();
            $numIdPerfilSeiAdministradorControleDsmpUtl = $this->_getIdPerfil($numIdSistemaSei, $this->nomeAdministradorControleDesempenho);
            $numIdItemMenuSeiContr = $this->_getIdItemMenuControleDesempenho($numIdSistemaSei, true);
            $numIdPerfilSeiBasico = $this->_getIdPerfil($numIdSistemaSei, 'B�sico');
            $numIdPerfilSeiAdmin = $this->_getIdPerfil($numIdSistemaSei);
            $numIdMenuSei = $this->_getIdMenu($numIdSistemaSei);
            $numIdItemMenuSeiAdmin = $this->_getIdItemMenu($numIdSistemaSei);

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Par�metriza��o de Tipo de Controle em Gestor');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_gr_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_gr_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_gr_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_gr_usu_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_gr_usu_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_gr_usu_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_gr_proc_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_gr_proc_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_gr_proc_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_gr_proc_selecionar');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Tipo de Aus�ncia em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ausencia_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ausencia_consultar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ausencia_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ausencia_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ausencia_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ausencia_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Fila em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_selecionar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_prm_gr_usu_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_fila_prm_gr_usu_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Ajuste de Jornada em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_jornada_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_jornada_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_jornada_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_jornada_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_jornada_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_jornada_usu_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_jornada_usu_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Tipo de Justificativa em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_just_revisao_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_just_revisao_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_just_revisao_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_just_revisao_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_just_revisao_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Tipo de Produto em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_produto_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_produto_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_produto_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_produto_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_produto_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Tipo de Atividade em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atividade_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atividade_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atividade_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atividade_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atividade_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atv_serie_prod_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atv_serie_prod_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_atv_serie_prod_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Grupo de Atividade em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Grupo/Fila em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Grupo/Processo em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_proc_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_proc_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fila_proc_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Grupo/Processo/Atividade em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fl_proc_atv_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fl_proc_atv_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_grp_fl_proc_atv_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Tipo de Revis�o em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_revisao_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_revisao_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_revisao_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_revisao_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_revisao_reativar');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Gestao de Solicitao em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_gestao_ajust_prazo_aprovar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_gestao_ajust_prazo_reprovar');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Hist�rico de Parametriza��o em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_hist_prm_gr_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_hist_prm_gr_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_hist_prm_gr_consultar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_hist_prm_gr_listar');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Parametriza��o de Contesta��o em Usu�rio Administrador');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_contest_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_contest_alterar');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Justificativa de Contesta��o em Usu�rio Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_just_contest_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_just_contest_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_just_contest_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_just_contest_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_just_contest_excluir');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Parametriza��o de Distribui��o em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_ds_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_ds_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_prm_ds_excluir');

            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_fila_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_fila_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_fila_alterar');

            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_aten_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_aten_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_aten_alterar');

            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_ativ_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_ativ_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_ativ_alterar');

            $this->logar('CRIANDO E VINCULANDO RECURSO A PERFIL - Parametriza��o de Distribui��o em Administrador de controle de desempenho');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_proc_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_proc_excluir');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_rel_prm_ds_proc_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_controle_dsmp_detalhar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_controle_dsmp_alterar');

            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ctrl_desemp_listar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ausencia_listar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ctrl_desemp_cadastrar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ctrl_desemp_alterar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ctrl_desemp_desativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ctrl_desemp_reativar');
            $objRecursoDTO = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ctrl_desemp_excluir');


            //////////////////////////////////////////////////////// MENUS ///////////////////////////////////////////////////////
            $this->logar('CRIANDO e VINCULANDO ITEM MENU PRINCIPAL DO M�DULO A PERFIL Administra��o > Controle de Desempenho');
            $objItemMenuDTOCtrlDesempenho = $this->adicionarItemMenu($numIdSistemaSei, $numIdPerfilSeiAdmin, $numIdMenuSei, $numIdItemMenuSeiAdmin, null, 'Controle de Desempenho', 0);

            $this->logar('CRIANDO e VINCULANDO ITEM MENU A PERFIL Administra��o > Controle de Desempenho > Tipo de Aus�ncia em Gestor');
            $objMdUtlTpAusenciaListar = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdmin, 'md_utl_adm_tp_ausencia_listar');
            $this->adicionarItemMenu($numIdSistemaSei,
                $numIdPerfilSeiAdministradorControleDsmpUtl,
                $numIdMenuSei,
                $objItemMenuDTOCtrlDesempenho->getNumIdItemMenu(),
                $objMdUtlTpAusenciaListar->getNumIdRecurso(),
                'Tipos de Aus�ncia',
                20);

            $objMdUtlTpControleListar = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_adm_tp_ctrl_desemp_listar');
            $this->adicionarItemMenu($numIdSistemaSei,
                $numIdPerfilSeiAdministradorControleDsmpUtl,
                $numIdMenuSei,
                $objItemMenuDTOCtrlDesempenho->getNumIdItemMenu(),
                $objMdUtlTpControleListar->getNumIdRecurso(),
                'Tipos de Controle de Desempenho',
                10);

            $this->logar('CRIANDO e VINCULANDO ITEM MENU A PERFIL Administra��o >  Controle de Desempenho > Ajuste de Jornada em Gestor');
            $objMdUtlJornadaListar = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiBasico, 'md_utl_adm_jornada_listar');
            $this->adicionarItemMenu($numIdSistemaSei,
                $numIdPerfilSeiAdministradorControleDsmpUtl,
                $numIdMenuSei,
                $objItemMenuDTOCtrlDesempenho->getNumIdItemMenu(),
                $objMdUtlJornadaListar->getNumIdRecurso(),
                'Ajuste de Jornada',
                30);
            $this->logar('CRIANDO RECURSOS QUE SER�O CHAMADOS VIA MENU');
            $objMdUtlGestaoSolicitacaoListar = $this->adicionarRecursoPerfil($numIdSistemaSei, $numIdPerfilSeiAdministradorControleDsmpUtl, 'md_utl_gestao_solicitacoes_listar');

            $this->adicionarItemMenu($numIdSistemaSei,
                $numIdPerfilSeiAdministradorControleDsmpUtl,
                $numIdMenuSei,
                $numIdItemMenuSeiContr,
                $objMdUtlGestaoSolicitacaoListar->getNumIdRecurso(),
                'Gest�o de Solicita��es',
                40);
        }
    }

    SessaoSEI::getInstance(false);
    BancoSEI::getInstance()->setBolScript(true);

    $objCorrecaoScriptRN = new MdUtlAvaliacaoRN();
    $objCorrecaoScriptRN->realizarCorrecao();
    exit;

}catch(Exception $e){
    echo(InfraException::inspecionar($e));
    try{LogSEI::getInstance()->gravar(InfraException::inspecionar($e));	}catch (Exception $e){}
    exit(1);
}
?>
