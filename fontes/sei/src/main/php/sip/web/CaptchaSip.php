<?
/*
 * TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
 *
 * 14/06/2006 - criado por MGA
 *
 */

require_once dirname(__FILE__).'/Sip.php';

class CaptchaSip extends InfraCaptcha {

  private static $instance = null;

  public static function getInstance()
  {
    if (CaptchaSip::$instance == null) {
      CaptchaSip::$instance = new CaptchaSip(BancoSip::getInstance());
    }
    return CaptchaSip::$instance;
  }

  public function configurarCaptcha($strIdentificacao){
    try {

      $objInfraParametro = new InfraParametro(BancoSip::getInstance());
      $tipoCaptcha = $objInfraParametro->getValor('SIP_TIPO_CAPTCHA', false);

      if ($tipoCaptcha == null) {
        $tipoCaptcha = CaptchaSip::$TIPO_INFRA;
      }

      switch ($tipoCaptcha) {
        case CaptchaSip::$TIPO_INFRA:
          $this->configurarInfra($strIdentificacao);
          break;

        case CaptchaSip::$TIPO_INFRA_V2:
          $this->configurarInfraV2($strIdentificacao);
          break;

        default:
          throw new InfraException('Tipo de Captcha inv�lido.');
      }

    }catch(Exception $e){
      throw new InfraException('Erro configurando captcha.', $e);
    }
  }
}
?>