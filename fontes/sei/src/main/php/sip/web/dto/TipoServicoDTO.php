<?
/*
* TRIBUNAL REGIONAL FEDERAL DA 4� REGI�O
*
* 11/12/2006 - criado por mga
*
*
*/

require_once dirname(__FILE__).'/../Sip.php';

class TipoServicoDTO extends InfraDTO {

  public function getStrNomeTabela() {
  	 return null;
  }

  public function montar() {
  	 $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'StaServico');
  	 $this->adicionarAtributo(InfraDTO::$PREFIXO_STR, 'Descricao');
  }
}
?>