# Ambiente de Desenvolvimento - Docker Compose

O objetivo dessa documentação é descrever os procedimentos para preparar um ambiente de desenvolvimento ou testes do SEI de forma rápida e padronizada através do [Docker](https://www.docker.com). Essas ferramentas permitem que seja provisionado automaticamente todo um ambiente funcional na máquina de trabalho do desenvolvedor, sem que seja necessário a instalação de servidores de aplicação e banco de dados pelo mesmo. Outra vantagem é que esse ambiente é completamente provisionado utilizando máquinas virtuais, necessitando que o desenvolvedor apenas configure sua IDE ou editor de texto preferido.

Antes, gostariamos de reforçar a necessidade que todo o desenvolvimento esteja alinhado com as diretivas e padrões de interface, codificação php e modelagem de banco de dados utilizados pelo SEI. A documentação dos padrões estão disponíveis na comunidade do SEI:

* [Documentação Técnica do SEI](https://www.gov.br/economia/pt-br/assuntos/processo-eletronico-nacional/destaques/material-de-apoio-2/material-de-apoio-sei "Clique para acessar")

Voltando para à configuração do ambiente de desenvolvimento, as tecnologias utilizadas nesse trabalho foram as listadas abaixo. Sugerimos uma breve leitura de suas documentações para melhor entendimento.

* Docker - https://www.docker.com/
* Docker Compose - https://docs.docker.com/compose/
* Git - https://git-scm.com/

O provisionamento dos componentes do sistema é implementado através da plataforma de containers do [Docker](https://www.docker.com), portanto, as imagens dos containers podem ser reutilizados isoladamente em outras configurações de infraestrutura para testes, sendo que todos os containers utilizados neste projeto estão publicados no [DockerHub](https://hub.docker.com/u/processoeletronico). 

O Docker utiliza recursos do Kernel do Linux para gerenciar o isolamento provido pelos container, o que obriga a utilização deste sistema operacional para uso da "virtualização". Para resolver esta limitação no Windows, será utilizado o Docker Desktop, responsável por criar uma virtualização para servir de host para o Docker e permitir sua utilização em outros Sistemas Operacionais, como Windows e MacOS.



## CONFIGURAÇÃO DO AMBIENTE

### 1) Instalação das ferramentas de desenvolvimento

Para configurar o ambiente, será necessário a instalação dos seguintes pré-requisitos:

* **Docker Desktop** Download: https://www.docker.com/products/docker-desktop/
* **Git** Download: https://git-scm.com/downloads

#### 1.2 - Instalação do Docker Desktop

O primeiro passo é instalar o Docker Desktop, ferramentas de gerenciamento de containers para Windows que será utilizado para virtualizar cada um dos componentes da aplicação. 

Página de download: https://www.docker.com/products/docker-desktop/

Recomentado selecionar a opção "Use WSL2 instead of Hyper-V" para melhor integração e desempenho com as demais ferramentas que serão utilizadas.

#### 1.3 - Instalação do Kernel do Linux para o WSL2

As novas versões do kernel do Linux para o recurso WSL2 do Windows são feitos separadamente através de pacotes de atualização. Acesse o link a seguir para maiores informações de como instalar este componente: https://aka.ms/wsl2kernel


#### 1.4 - Instalação do Git

Para possibilitar o acesso ao repositório do projeto no Github, necessário instalar o GIT ou uma das alternativas na máquina local de desenvolvimento.

> GIT - https://git-scm.com/
> Github Desktop - https://desktop.github.com/
> TortoiseGit - https://tortoisegit.org/


### 2) Habilitação do mecanismo de virtualização nas configurações de Boot (UEFI ou BIOS)

É necessário que a configuração de Boot (UEFI ou BIOS) da máquina localhost (desktop ou notebook) onde vai ser instalado o ambiente padrão de desenvolvimento esteja com a opção para aceitar virtualização habilitada.


### 3) - Clonar repositório git do projeto SEI

Clonar o repositório do projeto na máquina local de desenvolvimento.

O diretório da aplicação estará organizado da seguinte forma:

/fontes
   /sei
      /ops
         ...
      /src
         /main
            /php
               /sei
               /sip
               /infra

A pasta /ops estatá localizado os arquivos de configuração do ambiente de desenvolvimento local para o SEI, Já no diretório /src/main/php/ estará os arquivos da aplicação conforme disponibilizado originalmente (pastas /sei, /sip e /infra). 

Esses arquivos serão compartilhados para dentro das máquinas virtuais criadas pelo Docker para ativação do sistema. Com isso, as alterações feitas diretamente nos arquivos php durante do desenvolvimento refletirão de forma automática no ambiente que estará disponível em http://localhost:8000/sei


### 4) Definir qual banco de dados será utilizado para o ambiente local

Atualmente, existem 3 diferentes bases de dados que podem ser utilizadas no ambiente de desenvolvimento local: MySQL, Oracle e SQLServer.

Para definir qual base de dados será utilizada, é necessário que o arquivo de variáveis correspondente à base desejada (mysql.env, oracle.env, sqlserver.env) seja copiado da pasta fontes/sei/ops/envs para diretório do arquivo de configuração docker-compose.yml (fontes/sei/ops) com o nome ".env".

Outra alternativa para configuração pode ambiente pode ser realizada pelos scripts utilitários criados no ambiente colaborativo. Vide seção INFORMAÇÕES ADICIONAIS >> 2) Scritps utilitários .bat para Windows ou comandos make para Linux.

### 5) Iniciar o Ambiente de Desenvolvimento Virtualizado

Definido os detalhes da configuração do ambiente, será necessário navegar para o diretório /fontes/sei/ops e executar os seguintes comando para operar o ambiente virtualizado:

  # Inicializa o ambiente de desenvolvimento local
  docker compose up


Esse comando irá iniciar a construção do ambiente de desenvolvimento começando pelo download das imagens do Docker contendo todo o ambiente preparado para o desenvolvimento.

É normal que a primeira execução desse comando demore vários minutos para ser concluído, pois as imagens serão baixada para a máquina de desenvolvimento. Após o fim da transferência, o ambiente estará disponível em questão de minutos.

É possível indicar qual base de dados será utilizada no ambiente local, as opções disponíveis são Oracle, SQLServer e MySQL (padrão). Caso deseja selecionar outro banco de dados, copie o arquivo de variáveis de ambiente do diretório /fontes/sei/ops/envs para o diretório do docker compose (/fontes/sei/ops), mantendo o nome padrão .env.

Após a conclusão do primeiro provisionamento, o ambiente poderá ser destruído e recriado rapidamente.

Ao final da inicialização do ambiente de desenvolvimento, será apresentada a mensagem abaixo, indicando que todos os serviços do SEI já estão em operação na máquina de desenvolvimento:

    ===> default: Starting smtp
    ===> default: Starting jod
    ===> default: Starting mysql
    ===> default: Starting solr
    ===> default: Starting memcached    
    ===> default: Starting http

### 6) Testar a Aplicação

#### SEI
Após a finalização do provisionamento do ambiente e a apresentação das mensagens acima, o SEI estará disponível para testes na máquina local de desenvolvimento através do acesso ao endereço http://localhost:8000/sei. O usuário para acesso será o login: teste / senha: teste, o mesmo configurado na base inicial do sistema.

Importante mencionar que o sistema que está rodando nesse endereço se baseia exatamente no código-fonte do SEI. A alteração feita no código-fonte do sistema poderá ser visto instantaneamente no sei através de um simples Refresh no browser do desenvolvedor.

#### SIP
Da mesma forma como descrito anteriormente, o sip estará disponível no endereço http://localhost:8000/sip e o usuário de acesso será o login: teste / senha: teste, o mesmo configurado na base inicial do sistema.

#### BANCOS DE DADOS
O componente chamado db, apresentado logo após o provisionamento do ambiente, se refere ao serviço de banco de dados escolhido durante o provisionamento do ambiente, podendo ser o MySQL (padrão, Oracle ou SQLServer. A base poderá ser acesso por qualquer utilitário de conexão á banco de dados. Este serviço estará com os 2 bancos de dados utilizados pelo SEI (sei e sip) e poderá ser acessados com os seguintes usuários:

##### MySQL
    Usuário Root do MySQL: login:root / senha:P@ssword
    Usuário da Base de Dados do SEI: login: sei_user / senha: sei_user
    Usuário da Base de Dados do SIP: login: sip_user / senha: sip_user

    Ex: mysql -h 127.0.0.1 -u root -p sei

##### Oracle
    Usuário Root do Oracle: login:sys / senha:P@ssword
    Usuário de sistema do Oracle: login:system / senha:P@ssword
    Usuário da Base de Dados do SEI: login: sei_user / senha: sei_user
    Usuário da Base de Dados do SIP: login: sip_user / senha: sip_user

    Ex: sqlplus sys/P@ssword as sysdba

##### SQLServer
    Usuário Root do SQLServer: login:sa / senha:P@ssword
    Usuário da Base de Dados do SEI: login: sei_user / senha: sei_user
    Usuário da Base de Dados do SIP: login: sip_user / senha: sip_user

    Ex: tsql -S 127.0.0.1 -U sa -P P@ssword 


* **Apache Solr** O Apache Solr também estará disponível para testes e poderá ser acessado pelo endereço: http://localhost:8983/solr
* **Memcached** Servidor de cache utilizado pela aplicação SEI e SIP http://localhost:11211
* **Serviço SMTP para visualizar e-mails enviados** O ambiente de desenvolvimento possui um serviço SMTP próprio para disparar os e-mails do sistema. Para visualizar os e-mails enviados acesse: http://localhost:1080

## INFORMAÇÕES ADICIONAIS

A operação do ambiente de desenvolvimento virtualizado poderá ser feito de 2 formas:

#### 1) Comandos Básicos do Docker
Os seguintes comandos poderão ser executados no diretório onde está localizado o código-fonte do SEI para ligar e desligar o ambiente:

    docker compose up

Responsável por inicializar os containers docker e gerenciar o redirecionamento dos serviços para à máquina local de desenvolvimento. Caso não exista nenhum ambiente previamente configurado na máquina local, ele também se encarregará de fazer toda a instalação e provisionamento da ambiente virtual.

    docker compose down

Responsável por desligar os containers de todos os serviços em execução.

    docker compose restart

Reinicia todo o ambiente de desenvolvimento e seus containers.

    docker compose down --volumes

Destrói todo o ambiente de desenvolvimento utilizado até o momento, apagando todas as informações persistidas no banco de dados. A simples execução do comando `docker compose up -d` reconstrói um novo ambiente limpo para o sistema SEI.

    docker compose pull

Atualizar as imagens dos containers docker da aplicação para a última versão disponível no repositório DockerHub:


#### 2) Scritps utilitários .bat

Para simplificar a operação dos containers Docker em ambiente Windows, os seguintes scripts .bat foram disponibilizados no diretório /fontes/sei/ops:
----------
    provisionar_Dev_Localhost_SEI.bat

Responsável por criar a configuração inicial do ambiente de desenvolvimento do SEI utilizando os arquivos de configuração para a base de dados MySQL. 
----------
    iniciar_Dev_Localhost_SEI.bat

Responsável por inicializar os containers e gerenciar o redirecionamento dos serviços para à máquina local de desenvolvimento. Caso não exista nenhum ambiente previamente configurado na máquina local, ele também se encarregará de fazer toda a instalação e provisionamento da máquina virtual. Semelhante ao comando `docker compose up`
----------
    parar_Dev_Localhost_SEI.bat

Responsável por desligar os containers de todos os serviços em execução. Semelhante ao comando `docker compose down`
----------
    destruir_Dev_Localhost_SEI.bat

Destrói todo o ambiente de desenvolvimento utilizado até o momento, apagando todas as informações persistidas no banco de dados. A simples execução do comando `docker compose up -d` reconstrói um novo ambiente limpo para o sistema SEI. Semelhante ao comando `docker compose down --volumes`.

CONFIGURAÇÕES ADICIONAIS
### Proxy para acesso a internet

Se a máquina localhost (desktop ou notebook) onde vai ser instalado o ambiente padrão de desenvolvimento depender de um proxy para acesso à Internet, é necessário antes configurar o proxy como variável de ambiente conforme abaixo:

    set http_proxy=http://proxy.anp.net
    set https_proxy=http://proxy.anp.net

Se o proxy exigir autenticação é necessário incluir o nome e senha do usuário, conforme este segundo exemplo:

    set http_proxy=http://<usuario>:<senha>@proxy.anp.net
    set https_proxy=http://<usuario>:<senha>@proxy.anp.net


OUTRAS INFORMAÇÔES

Os arquivos de configuração do docker-compose utilizados como referência para a configuração deste projeto são públicos e estão disponíveis nos endereços abaixo:

* docker-compose.yml - https://gist.github.com/guilhermeadc/067412207cb432cbfc9bf4617c1f34b0
* mysql.env - https://gist.github.com/guilhermeadc/548753472d138da940a68a70bcc7d6fc
* oracle.env - https://gist.github.com/guilhermeadc/1d56b08b1245b79953a40ca02f09750e
* sqlserver.env - https://gist.github.com/guilhermeadc/946a0f396a3f743ac3e9d072d87e6e63
